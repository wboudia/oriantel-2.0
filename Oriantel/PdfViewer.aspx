﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PdfViewer.aspx.vb" Inherits="Oriantel.PdfViewer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link rel="icon" type="image/ico" href="images/icones/favicon.ico" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <script type="text/javascript" src="js/detectPdfReader.js"></script>

    <title>Oria Extranet</title>

    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

    <script type="text/javascript">
        function AffecterHauteur() {
            var ihauteurFenetre;
            if (document.all) {
                ihauteurFenetre = document.documentElement.clientHeight;
            }
            else {
                ihauteurFenetre = window.innerHeight;
            }
            document.getElementById("pdfembed").height = ihauteurFenetre - 20;
            $('#divVisibilite').css('display', '');
        }

        $(window).on('resize', function () {
            AffecterHauteur();
        });

    </script>

</head>
<%--<body onload="AffecterHauteur()">--%>
<body>

    <form id="form1" runat="server">
        <asp:HiddenField runat="server" ID="cheminFichier" Value="" />
        <asp:HiddenField runat="server" ID="testReader" Value="" />

        <div id="divVisibilite" style="display:none;">
            <asp:PlaceHolder ID="PlaceHolder_PdfViewer" runat="server"></asp:PlaceHolder>
        </div>
    </form>

    <script type="text/javascript">
        var hasReader = false;
        var testReader = '0';
        var cheminFichier = document.getElementById("cheminFichier");

        testReader = document.getElementById("testReader").value
        if (testReader == '1') {
            var info = getAcrobatInfo();
            if (info.acrobat == 'installed') {
                hasReader = 'true';
            }

            window.location = document.URL + '&hasReader=' + hasReader;
        }
        else {
            AffecterHauteur();
        }
       
   </script>
</body>
</html>
