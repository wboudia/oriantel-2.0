﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="Oriantel._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="fr-FR">
<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title>ORIA &#8211; Conseil et Stratégie en Systèmes d&#039;Information</title>

    <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64 128x128" href="images/icones/favicon.ico" />

    <link href="css/reset.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="screen" />
    
    <link rel="stylesheet" href="css/superfish.css" type="text/css" />
    <link rel='stylesheet' id='uds-billboard-css' href='css/billboard.min.css?ver=3.4.1' type='text/css' media='screen' />
    <link rel="stylesheet" href="css/style2.css" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css' />

	<script src="js/jquery.js?ver=1.7.2" type="text/javascript"></script>
    <script src="js/scripts.js?ver=3.2.1" type="text/javascript"></script>

    <script src="js/billboard.min.js?ver=3.0" type="text/javascript"></script>
    <script src="js/superfish.js" type="text/javascript"></script>
    
    <!--[if lt IE 9]> <link href="css/ie.css" rel="stylesheet" type="text/css" media="screen"> <![endif]-->
    <!--[if IE 6]> <link href="css/ie6.css" rel="stylesheet" type="text/css" media="screen"> <![endif]-->

</head>

<body>
    <form runat="server">
        <div id="conteneur">

            <div id="header2">
	            <div class="conteneur2">

		            <h1>
			            <a href="http://oriantel.net/">ORIANTEL</a>
		            </h1>
		            <div class="description2">
		            </div>

		            <div id="mainmenu2">
			            <div class="menu-menu-principal-container">
				            <ul id="menu-menu-principal" class="sf-menu sf-js-enabled sf-shadow">
					            <li id="menu-item-5354" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4558 current_page_item menu-item-5354">
						            <a>&nbsp;</a>
					            </li>
				            </ul>
			            </div>
		            </div>

	            </div>
            </div>

            <div id="modules">

                <div id="content">
                    <ul id="home-menu">
                        <asp:PlaceHolder ID="PlaceHolderModules" runat="server"></asp:PlaceHolder>
                    </ul>
                </div>

                <div id="sidebar">
                    <div id="logo-partner-sommaire">
                        <asp:Image ID="logoClientSommaire" runat="server" />
                    </div>
                    <div style="height:17Px;">
                        &nbsp;
                    </div>
                    <div id="sidebar-menu">
                        <ul id="homemenu">
                            <asp:PlaceHolder ID="PlaceHolderInfosUtilisateur" runat="server"></asp:PlaceHolder>
                            <asp:PlaceHolder ID="PlaceHolderDocumentsUtilisateur" runat="server"></asp:PlaceHolder>
                        </ul>

                        <asp:LinkButton class="close" ID="lnkClose" runat="server" Text="Fermer la session" ></asp:LinkButton>
                    </div>

                </div>

            </div>
       </div>
    </form>

    <script type='text/javascript'>
        //<![CDATA[
        jQuery(document).ready(function ($) {

            $('#uds-bb-0').show().uBillboard({
                width: '600px',
                height: '90px',
                squareSize: '100px',
                autoplay: true,
                pauseOnVideo: true,
                showControls: false,
                showPause: false,
                showPaginator: false,
                showThumbnails: false,
                showTimer: false,
                thumbnailHoverColor: "#FFFFFF"
            });

        });
        //]]>
    </script>
    <div id="footer2">
	        <div class="conteneur2">
		        <div class="menu-menu-pied-de-page-container">
			        <ul id="menu-menu-pied-de-page" class="menufooter">
				        <li id="menu-item-5352" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4558 current_page_item menu-item-5352">
					        <a href="http://oriantel.net/">Oriantel</a>
				        </li>
			        </ul>
		        </div> 
                <div id="recommander"> 		
			        <a href="http://oriantel.net/feed/" title="Suivre par RSS." target="_blank"> 
		            <img src="img/icon-rss.png" alt="Suivre par Rss" /></a>
	            </div><!-- #recommander -->
	        </div>
        </div>
</body>
</html>