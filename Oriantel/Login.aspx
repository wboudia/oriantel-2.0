﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="Oriantel.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"  dir="ltr" lang="fr-FR">
<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title>ORIA &#8211; Conseil et Stratégie en Systèmes d&#039;Information</title>

    <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64 128x128" href="images/icones/favicon.ico" />

    <link href="css/reset.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/style2.css" rel="stylesheet" type="text/css" />
    <link href='css/billboard.min.css?ver=3.4.1' rel='stylesheet' id='uds-billboard-css' type='text/css' media='screen' />
    <link href="css/superfish.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
		<script type="text/javascript" src="js/jquery.placeholder.js"></script>
		<script type="text/javascript">
		    $(function () {
		        $('input, textarea').placeholder();
		    });
		</script>
    <script src="js/superfish.js" type="text/javascript"></script>
    <script src="js/jquery.js?ver=1.7.2" type="text/javascript"></script>
    <script type="text/javascript">
        function getResEcran() {
            $('#idResolutionEcran').attr('value', screen.width + 'x' + screen.height);
        }
    </script>
    

    <!--[if lt IE 9]> <link href="css/ie.css" rel="stylesheet" type="text/css" media="screen"> <![endif]-->
    <!--[if IE 6]> <link href="css/ie6.css" rel="stylesheet" type="text/css" media="screen"> <![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie_2.css" type="text/css" /><![endif]-->
</head>

<body class="homepage" onload="getResEcran();">
    <div id="container">
        <!--HEADER-->
        <div id="header2">
	        <div class="conteneur2">

		        <h1>
			        <a href="http://oriantel.net/">ORIANTEL</a>
		        </h1>
		        <div class="description2">
		        </div>

		        <div id="mainmenu2">
			        <div class="menu-menu-principal-container">
				        <ul id="menu-menu-principal" class="sf-menu sf-js-enabled sf-shadow">
					        <li id="menu-item-5354" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4558 current_page_item menu-item-5354">
						        <a>&nbsp;</a>
					        </li>
				        </ul>
			        </div>
		        </div>

	        </div>
        </div>

        <div id="contentLogin">
            <div class="conteneur">

                <div id="main">
                    <form id="form1" runat="server">
                        <asp:HiddenField ID="idResolutionEcran" runat="server" />

                        <div id="post">

                            <div id="login" style="margin: 1em;width: 450Px;">
				                <p>
                                    <asp:TextBox ID="ES_Login" runat="server" placeholder="Identifiant"></asp:TextBox>
				                </p>
                                    
				                <p>
                                    <asp:TextBox ID="ES_Pass" runat="server" TextMode="Password" placeholder="Mot de passe"></asp:TextBox>
				                </p>

				                <p>
                                    <asp:Button ID="BP_Valider" runat="server" Text="Valider"></asp:Button>
                                    </p>

				                <p>
                                    <span class="lienLogin"><a href="#" style="text-decoration:underline;color:white;">Mot de passe oublié?</a></span>
				                </p>

                                <p>
                                    <asp:Label ID="STV_Erreur" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Small" ForeColor="Red" Height="17px" Style="z-index: 103;" Visible="False" Width="152px">Identification invalide !</asp:Label>
                                </p>
		                    </div>

                            <div>
                                <asp:Literal ID="ltlVersionIE" runat="server"></asp:Literal>
                            </div>

                            <div class="wpcf7" id="wpcf7-f4-p42-o1">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="4" /><br />
                                    <input type="hidden" name="_wpcf7_version" value="3.2.1" /><br />
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4-p42-o1" /><br />
                                    <input type="hidden" name="_wpnonce" value="df3bf6b2e0" />
                                </div>


                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                            </div>

                        </div>
                    </form>
                    <!--Wrapper-->
                </div>
                <!-- main -->

                <!--Wrapper-->
                <div class="clear">
                    <hr />
                </div>
            </div>
            <!-- .conteneur -->
        </div>
        <!-- content -->


        <!--Wrapper-->
        <div class="clear">
            <hr />
        </div>
        <!--Wrapper-->
        <div class="clear">
            <hr />
        </div>
        <!--Wrapper-->
        <div class="clear">
            <hr />
        </div>

        <div id="footer2">
	        <div class="conteneur2">
		        <div class="menu-menu-pied-de-page-container">
			        <ul id="menu-menu-pied-de-page" class="menufooter">
				        <li id="menu-item-5352" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4558 current_page_item menu-item-5352">
					        <a href="http://oriantel.net/">Oriantel</a>
				        </li>
			        </ul>
		        </div> 
                <div id="recommander"> 		
			        <a href="http://oriantel.net/feed/" title="Suivre par RSS." target="_blank"> 
		            <img src="img/icon-rss.png" alt="Suivre par Rss" /></a>
	            </div><!-- #recommander -->
	        </div>
        </div>
        <!-- #footer -->

        <br />
        <br />
        <script type='text/javascript'>
            //<![CDATA[
            jQuery(document).ready(function ($) {

                $('#uds-bb-0').show().uBillboard({
                    width: '600px',
                    height: '90px',
                    squareSize: '100px',
                    autoplay: true,
                    pauseOnVideo: false,
                    showControls: true,
                    showPause: false,
                    showPaginator: false,
                    showThumbnails: false,
                    showTimer: false,
                    thumbnailHoverColor: "#000"
                });

            });
            //]]>
        </script>

        <script src="js/scripts.js?ver=3.2.1" type="text/javascript"></script>
        <script src="js/billboard.min.js?ver=3.0" type="text/javascript"></script>

    </div>

</body>
</html>