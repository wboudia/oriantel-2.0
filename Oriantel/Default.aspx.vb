﻿Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '---Ici coder gestion des raccourcis directs

        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
            Exit Sub
        End If

        chargerTabConnexion()
        chargerTabModules()

        Dim oNavigation As New Navigation
        oNavigation.effacerNavigation()
    End Sub

    Private Sub chargerTabConnexion()
        Dim oLiteral As New Literal
        Dim oUtilisateur As New Utilisateur()
        Dim oUtilisateurSecondaire As New UtilisateurSecondaire()
        Dim oLogs As New Logs()
        Dim sLibelleDerniereConnxion As String = ""

        Try
            oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))
            oLogs.oUtilisateur = oUtilisateur
            If Not Session.Item("idUtilisateurSecondaire") Is Nothing And CStr(Session.Item("idUtilisateurSecondaire")) <> "0" Then
                oUtilisateurSecondaire.Req_Infos_Utilisateur_Secondaire(CInt(Session.Item("idUtilisateurSecondaire")), CInt(User.Identity.Name.Split(CChar("_"))(1)))
                oLogs.oUtilisateurSecondaire = oUtilisateurSecondaire
                oLogs.recuperationDerniereConnexionPourUtilisateurSecondaire()
            Else
                oLogs.recuperationDerniereConnexion()
            End If



            If oUtilisateur.oClient.sLangueClient = "US" Then
                sLibelleDerniereConnxion = "Last connection"
                Me.lnkClose.Text = "Log out"
            Else
                sLibelleDerniereConnxion = "Dernière connexion"
            End If


            ' Récupération des informations utilisateurs pour l'accueil
            Dim nomUtilisateur As String = ""
            If Not Session.Item("idUtilisateurSecondaire") Is Nothing And CStr(Session.Item("idUtilisateurSecondaire")) <> "0" Then
                nomUtilisateur = oUtilisateurSecondaire.sCivilite + " " + oUtilisateurSecondaire.sNom + " " + oUtilisateurSecondaire.sPrenom
            Else
                nomUtilisateur = oUtilisateur.sCivilite + " " + oUtilisateur.sNom + " " + oUtilisateur.sPrenom
            End If

            Me.logoClientSommaire.ImageUrl = "images/logo/" & Toolbox.ReplaceQuote(oUtilisateur.oClient.sLogo)
            Me.logoClientSommaire.Width = New Unit(120, UnitType.Pixel)
            Me.logoClientSommaire.Height = New Unit(120, UnitType.Pixel)
            Dim codeHtml As String = "<li>" & oUtilisateur.oClient.sPrefixeClient & " " & oUtilisateur.oClient.sNomClient & "</li>"
            codeHtml &= "<li>" & nomUtilisateur & "</li>"
            codeHtml &= "<li class=""lastconnection""><strong>" & sLibelleDerniereConnxion & "</strong><br>" & oLogs.dDateHeure.ToString("dd/MM/yy H:mm:ss") & "</li>" ' TRADUCTION

            ' Récupération de la liste des documents disponibles pour ce client
            Dim oDocuments As New Documents()
            oDocuments.chargerListeDocument(oUtilisateur)
            For i = 0 To (oDocuments.Count - 1) Step 1
                codeHtml &= "<li><a href=""" & oDocuments.Item(i).sLien & """ class=""" & oDocuments.Item(i).slibelleClasse & """>" & oDocuments.Item(i).sLibelle & "</a></li>"
            Next

            oLiteral.Text = codeHtml
            Me.PlaceHolderInfosUtilisateur.Controls.Add(oLiteral)
            oLiteral = Nothing
        Catch ex As Exception
            'Email.envoiEmail("Extranet", "CreationSessionUtilisateur", ex.Message)
            Throw ex
        End Try

    End Sub

    Private Sub chargerTabModules()
        Dim oLiteral As New Literal
        Dim codeHtml As String = ""
        Dim idUtilisateur As Integer = CInt(User.Identity.Name.Split(CChar("_"))(0))

        Dim oModules As New Moduls()

        Try
            oModules.chargerListeModulesUnUtilisateur(idUtilisateur, CInt(User.Identity.Name.Split(CChar("_"))(1)))

            For i = 0 To (oModules.Count - 1) Step 1
                codeHtml &= "<li>"
                codeHtml &= "<a href=""" & oModules.Item(i).sLien & "?module=" & oModules.Item(i).nIdModule & """ class=""" & oModules.Item(i).sLibelleClasse & """>"
                codeHtml &= "<span class=""image""></span>"
                codeHtml &= "<span class=""title""><span>" & oModules.Item(i).sTitre & "</span></span>"
                codeHtml &= "<span class=""desc"">" & oModules.Item(i).sDetail & "</span>"
                codeHtml &= "</a>"
                codeHtml &= "</li>"
            Next
            oLiteral.Text = codeHtml
            Me.PlaceHolderModules.Controls.Add(oLiteral)
            oLiteral = Nothing
        Catch ex As Exception
            Throw ex
            ' VOIR LA GESTION DES MESSAGES D'ERREUR A FAIRE
        End Try

    End Sub

    Protected Sub lnkClose_Click(sender As Object, e As EventArgs) Handles lnkClose.Click
        FormsAuthentication.SignOut()
        Response.Redirect("login.aspx")
    End Sub
End Class
