﻿Imports Microsoft.Office.Interop
Imports System.IO
Public Class GestionParcCNES
    Inherits System.Web.UI.Page

    Private Property oLigne() As Ligne
        Get
            Return CType(Session("oLigne"), Ligne)
        End Get
        Set(ByVal Value As Ligne)
            Session("oLigne") = Value
        End Set
    End Property
    Private Property oProfil() As Profil
        Get
            Return CType(Session("oProfil"), Profil)
        End Get
        Set(ByVal Value As Profil)
            Session("oProfil") = Value
        End Set
    End Property
    Private Property sTypeModif() As String
        Get
            Return CType(Session("sTypeModif"), String)
        End Get
        Set(ByVal Value As String)
            Session("sTypeModif") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If

        Dim nIdLigne As Integer = 0
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim oAffectations As New Affectations
        Dim oLignes As New Lignes
        Dim oFonctions As New Fonctionss
        Dim oCompteFacturants As New CompteFacturants
        Dim oProfils As New Profils
        Dim oDiscriminations As New Discriminations
        Dim oOptionsProfils As New OptionsProfils
        Dim oProfil As New Profil

        If Request.Item("idLigne") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            nIdLigne = CInt(Request.Item("idLigne").ToString)
        End If

        If Request.Item("typeModif") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            sTypeModif = Request.Item("typeModif").ToString
        End If

        Me.TBAffect.Visible = False
        Me.TBNom.Visible = False

        ChargerImageAction(sTypeModif)

        oFonctions.chargerFonctions(nIdClient)
        oCompteFacturants.chargerCompteFacturants(nIdClient)
        oAffectations.chargerAffectation(nIdClient)
        oProfils.ChargerProfil(nIdClient)

        ChargerAffectation(oAffectations, nIdClient)
        ChargerListeProfil(oProfils, nIdClient)
        ChargerListeFonction(oFonctions, nIdClient)
        ChargerListeCompteFacturant(oCompteFacturants, nIdClient)

        TBDateModification.Text = Date.Today.ToString("dd/MM/yyyy")

        If Not Page.IsPostBack Then

            oLigne = New Ligne
            oProfil = New Profil

            If oLigne.oOptionsUtilisateur Is Nothing Then
                oLigne.oOptionsUtilisateur = New OptionsUtilisateur
            End If

            If oLigne.oStocks Is Nothing Then
                oLigne.oStocks = New Stocks
            End If

            If nIdLigne <> 0 Then
                oLigne.chargerLigneParId(nIdLigne)
            End If

            If oProfil.oOptionsProfils Is Nothing Then
                oProfil.oOptionsProfils = New OptionsProfils
            End If


            Dim oHierarchies As New Hierarchies
            Dim oForfaits As New Forfaits

            Dim oOptionsUtilisateur As New OptionsUtilisateur
            Dim oStocks As New Stocks

            oHierarchies.chargerHierarchies(nIdClient)
            oForfaits.chargerForfaits(nIdClient)
            oOptionsUtilisateur.chargerOptionsClient(nIdClient)
            oStocks.chargerStocksLibres(nIdClient)
            oOptionsProfils.chargerOptionsProfil(oProfil.sNomProfil, nIdClient)

            oStocks = FiltrerStocksAttribues(oStocks)

            ChargerListeTypeForfait(oForfaits, nIdClient)
            ChargerListeForfait(oForfaits, nIdClient)

            ChargerListeTypeMateriel(oStocks)
            ChargerListeMarque(oStocks)
            ChargerListeModele(oStocks)
            ChargerListeCodeReference(oStocks)
            ChargerListeCodeEmei(oStocks)
            ChargerListeNumSerie(oStocks)
            ChargerIdStock(oStocks)

            oLignes.chargerLignesGestionParc(nIdClient, DDLAffect.SelectedItem.Text.ToString)
            ChargerLignesGestionParc(oLignes, nIdClient, DDLAffect.SelectedItem.Text.ToString)

            If Not oLigne.oOptionsUtilisateur Is Nothing Then chargerOptionsAffectees()
            chargerOptionsLibres(oOptionsUtilisateur)
            If Not oProfil.oOptionsProfils Is Nothing Then ChargerOptionsProfil()

            LAncienProfil.Text = DDLProfil.Text

            If Not oLigne.oStocks Is Nothing Then chargerStocksAffectes()

            If DDLAffect.Text = "Pool" Then
                Label12.Visible = True
                TBUsage.Visible = True
                DDLNom.Text = "Structure : "
                Label11.Visible = True
                TxtNom.Visible = True
                TxtPrenom.Visible = True

            Else
                Label12.Visible = False
                TBUsage.Visible = False
                DDLNom.Text = "Nom : "
            End If

            Select Case sTypeModif
                    Case "insert"
                        Me.LblTitre.Text = "Ajouter une ligne"
                        Me.DivActionInsert.Style.Item("display") = "block"
                        Me.TxtDateActivation.Text = (Now.AddDays(2)).ToString("dd/MM/yyyy")
                        Me.TxtDateActivation.Enabled = True
                        Me.TxtNumeroGSM.Text = "-"
                        Dim oStocksCarte = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle.ToUpper = "CARTE"
                        If oStocksCarte.Count > 0 Then
                            Me.DdlTypeMateriel.SelectedValue = oStocksCarte(0).oArticle.oTypeMateriel.sLibelle
                            ChargerListeMarque(oStocks)
                            ChargerListeModele(oStocks)
                            ChargerListeCodeReference(oStocks)
                            ChargerListeCodeEmei(oStocks)
                            ChargerListeNumSerie(oStocks)
                            ChargerIdStock(oStocks)
                        End If
                    Case "edit"
                        oLigne.chargerLigneParId(nIdLigne)
                        Me.LblTitre.Text = "Modifier une ligne"
                        oLigne.RecupererAffectation()
                        Me.TBAffect.Text = oLigne.sAffectation
                        Me.TBAffect.Visible = True
                        Me.TBNom.Text = oLigne.sNom & " " & oLigne.sPrenom
                        Me.TBNom.Visible = True
                        Me.TxtNumeroGSM.Text = oLigne.sNumeroGSM
                        Me.TxtNumeroData.Text = oLigne.sNumeroData
                        Me.TBStructure.Text = oLigne.sStructure
                        Me.TBMatricule.Text = oLigne.sMatricule
                        Me.TBSite.Text = oLigne.sSite
                        Me.TBMail.Text = oLigne.sMail
                        Me.TBFixe.Text = oLigne.sFixe
                        Me.TBBureau.Text = oLigne.sBureau
                        Me.TBDirection.Text = oLigne.sDirection
                        Me.TBSociété.Text = oLigne.sSociete

                        If Not oLigne.dDateActivation = Nothing Then
                            Me.TxtDateActivation.Text = oLigne.dDateActivation.ToString("dd/MM/yyyy")
                        Else
                            Me.TxtDateActivation.Text = Date.Today.ToString("dd/MM/yyyy")
                        End If

                        Me.DivActionEdition.Style.Item("display") = "block"
                    Case "delete"
                        Me.LblTitre.Text = "Supprimer une ligne"
                        Me.DDLNom.Text = oLigne.sNom & " " & oLigne.sPrenom
                        Me.TxtNumeroGSM.Text = oLigne.sNumeroGSM
                        Me.TxtNumeroData.Text = oLigne.sNumeroData
                        If Not oLigne.dDateActivation = Nothing Then Me.TxtDateActivation.Text = oLigne.dDateActivation.ToString("dd/MM/yyyy")
                        VerrouillerControles()
                        Me.DivActionDelete.Style.Item("display") = "block"
                    Case "visu"
                        Me.LblTitre.Text = "Détail de la ligne"
                        Me.LblDateDesactivation.Visible = True
                        Me.TxtDateDesactivation.Visible = True
                        Me.DDLNom.Text = oLigne.sNom & " " & oLigne.sPrenom
                        Me.TxtNumeroGSM.Text = oLigne.sNumeroGSM
                        Me.TxtNumeroData.Text = oLigne.sNumeroData
                        Me.DivAction.Visible = False
                        VerrouillerControles()
                    Case "visucomplet"
                        Me.LblTitre.Text = "Détail de la ligne"
                        Me.DDLNom.Text = oLigne.sNom & " " & oLigne.sPrenom
                        Me.TxtNumeroGSM.Text = oLigne.sNumeroGSM
                        Me.TxtNumeroData.Text = oLigne.sNumeroData
                        If Not oLigne.dDateActivation = Nothing Then Me.TxtDateActivation.Text = oLigne.dDateActivation.ToString("dd/MM/yyyy")
                        If Not oLigne.dDateDesactivation = Nothing Then Me.TxtDateDesactivation.Text = oLigne.dDateDesactivation.ToString("dd/MM/yyyy")
                        Me.DivAction.Visible = False
                        VerrouillerControles()
                End Select
                If sTypeModif = "visu" Or sTypeModif = "delete" Or sTypeModif = "visucomplet" Then
                    Me.GvOptionsDisponibles.Visible = False
                    'Me.LblOptionsDisponibles.Visible = False
                End If

            End If

    End Sub
    Private Sub ChargerAffectation(ByVal oAffectations As Affectations, ByVal nIdClient As Integer)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("Affectation")
        oDataTable.Rows.Add("Pool")
        For Each oAffectation In oAffectations
            oDataTable.Rows.Add(oAffectation.sAffectation)
        Next
        Me.DDLAffect.DataSource = oDataTable
        Me.DDLAffect.DataValueField = "Affectation"
        Me.DDLAffect.DataTextField = "Affectation"
        Me.DDLAffect.DataBind()

        Dim oControle As DropDownList = Me.DDLAffect
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If
    End Sub
    Private Sub ChargerLignesGestionParc(ByVal oLignes As Lignes, ByVal nIdClient As Integer, ByVal sTypeAffectation As String)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idLigne")
        oDataTable.Columns.Add("Nom")
        For Each oLigne As Ligne In oLignes
            oDataTable.Rows.Add(oLigne.nIdLigne, oLigne.sNom & " " & oLigne.sPrenom)
        Next
        Me.DDLNom.DataSource = oDataTable
        Me.DDLNom.DataValueField = "idLigne"
        Me.DDLNom.DataTextField = CStr("Nom")
        Me.DDLNom.DataBind()

        Dim oControle As DropDownList = Me.DDLNom
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeProfil(ByVal oProfils As Profils, ByVal nIdClient As Integer)

        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idProfil")
        oDataTable.Columns.Add("NomProfil")
        'oDataTable.Rows.Add("0", "Sans Profil")

        For Each oProfil As Profil In oProfils
            oDataTable.Rows.Add(oProfil.nIdProfil, oProfil.sNomProfil)
        Next

        Me.DDLProfil.DataSource = oDataTable
        Me.DDLProfil.DataValueField = "idProfil"
        Me.DDLProfil.DataTextField = "NomProfil"
        Me.DDLProfil.DataBind()

        Dim oControle As DropDownList = Me.DDLProfil
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeTypeForfait(ByVal oForfaits As Forfaits, ByVal nIdClient As Integer)
        Dim sListeTypeForfait = From oForfait In oForfaits Group oForfait By oForfait.oTypeForfait.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeTypeForfait
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlTypeForfait.DataSource = oDataTable
        Me.DdlTypeForfait.DataValueField = "libelle"
        Me.DdlTypeForfait.DataTextField = "libelle"
        Me.DdlTypeForfait.DataBind()

        Dim oControle As DropDownList = Me.DdlTypeForfait
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeForfait(ByVal oForfaits As Forfaits, ByVal nIdClient As Integer)
        Dim sListeForfait = From oForfait In oForfaits Where oForfait.oTypeForfait.sLibelle = Me.DdlTypeForfait.SelectedValue Group oForfait By oForfait.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeForfait
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlForfait.DataSource = oDataTable
        Me.DdlForfait.DataValueField = "libelle"
        Me.DdlForfait.DataTextField = "libelle"
        Me.DdlForfait.DataBind()

        Dim oControle As DropDownList = Me.DdlForfait
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerStructuresGestionParc(ByVal oLignes As Lignes, ByVal nIdClient As Integer, ByVal sTypeAffectation As String)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idStructure")
        oDataTable.Columns.Add("structure")
        For Each oLigne As Ligne In oLignes
            oDataTable.Rows.Add(oLigne.nIdLigne, oLigne.sNom)
        Next
        Me.DDLNom.DataSource = oDataTable
        Me.DDLNom.DataValueField = "idStructure"
        Me.DDLNom.DataTextField = "structure"
        Me.DDLNom.DataBind()

        Dim oControle As DropDownList = Me.DDLNom
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If
    End Sub
    Private Sub DDLAffect_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLAffect.SelectedIndexChanged

        Dim oLignes As New Lignes
        Dim nIdclient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim sTypeAffectation As String = DDLAffect.SelectedItem.Text.ToString

        If DDLAffect.Text = "Pool" Then

            Label12.Visible = True
            TBUsage.Visible = True
            Label2.Text = "Structure : "
            LblDateActivation.Text = "Date activation : "
            LblDateDesactivation.Text = "Date désactivation : "
            LblDateDesactivation.Visible = False
            TxtDateDesactivation.Visible = False
            TxtDateDesactivation.Enabled = False

            oLignes.chargerStructuresGestionParc(nIdclient, sTypeAffectation)
            ChargerStructuresGestionParc(oLignes, nIdclient, sTypeAffectation)

            cDal.chargerLigneStructureParId(oLigne, oLigne.nIdLigne)
            cDal.RecupererInfosStructureCNES(nIdclient, oLigne)

            Label11.Visible = True
            TxtNom.Visible = True
            TxtPrenom.Visible = True

            TxtNom.Text = oLigne.sNom
            TxtPrenom.Text = oLigne.sPrenom
        Else

            If DDLAffect.Text = "Prêt" Then
                LblDateActivation.Text = "Date début prêt : "
                LblDateDesactivation.Text = "Date fin prêt : "
                LblDateDesactivation.Visible = True
                TxtDateDesactivation.Visible = True
                TxtDateDesactivation.Enabled = True
            Else
                LblDateActivation.Text = "Date activation : "
                LblDateDesactivation.Text = "Date désactivation : "
                LblDateDesactivation.Visible = False
                TxtDateDesactivation.Visible = False
                TxtDateDesactivation.Enabled = False
            End If

            oLignes.chargerLignesGestionParc(nIdclient, sTypeAffectation)
            ChargerLignesGestionParc(oLignes, nIdclient, sTypeAffectation)

            cDal.RecupererInfosLigneCNES(nIdclient, oLigne)

            TxtNom.Text = oLigne.sNom
            TxtPrenom.Text = oLigne.sPrenom

            Label11.Visible = False
            TxtNom.Visible = False
            TxtPrenom.Visible = False

            Label12.Visible = False
            TBUsage.Visible = False
            Label2.Text = "Nom : "

        End If

    End Sub
    Private Sub DDLNom_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLNom.SelectedIndexChanged

        Dim oLigne As New Ligne
        Dim nIdclient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))

        oLigne.nIdLigne = CInt(DDLNom.SelectedValue)

        If Label2.Text = "Structure : " Then

            cDal.chargerLigneStructureParId(oLigne, oLigne.nIdLigne)
            cDal.RecupererInfosStructureCNES(nIdclient, oLigne)
            'cDal.RecupererInfosLigneCNES(nIdclient, oLigne)

            'TxtNom.Text = oLigne.sNom
            'TxtPrenom.Text = oLigne.sPrenom

        Else

            cDal.RecupererInfosLigneCNES(nIdclient, oLigne)


        End If

        TxtNumeroGSM.Text = oLigne.sNumeroGSM
            TxtNumeroData.Text = oLigne.sNumeroData
            TxtDateActivation.Text = oLigne.dDateActivation.ToString("dd/MM/yyyy")
            TxtDateDesactivation.Text = oLigne.dDateDesactivation.ToString("dd/MM/yyyy")
            TBStructure.Text = oLigne.sStructure
            TBMatricule.Text = oLigne.sMatricule
            TBSite.Text = oLigne.sSite
            TBMail.Text = oLigne.sMail
        TBFixe.Text = oLigne.sFixe
        TxtNom.Text = oLigne.sNom
        TxtPrenom.Text = oLigne.sPrenom


    End Sub
    Private Sub ChargerImageAction(ByVal sTypeAction As String)
        Dim strFile As System.IO.FileInfo
        Dim sNomFichier As String = ""

        Select Case sTypeAction
            Case "insert"
                sNomFichier = "ico_Ajouter.png"
            Case "edit"
                sNomFichier = "ico_Modifier.png"
            Case "visu", "visucomplet"
                sNomFichier = "ico_Loupe.png"
            Case "delete"
                sNomFichier = "ico_supprimer.png"
        End Select

        strFile = New System.IO.FileInfo(Server.MapPath("images/icones/" & sNomFichier))
        If strFile.Exists Then
            Me.ImageAction.ImageUrl = "images/icones/" & sNomFichier
        End If
        ImageAction.Visible = True
    End Sub
    Private Sub ChargerListeFonction(ByVal oFonctions As Fonctionss, ByVal nIdClient As Integer)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idFonction")
        oDataTable.Columns.Add("libelle")
        For Each oFonction In oFonctions
            oDataTable.Rows.Add(oFonction.nIdFonction, oFonction.sLibelle)
        Next
        Me.DdlFonction.DataSource = oDataTable
        Me.DdlFonction.DataValueField = "idFonction"
        Me.DdlFonction.DataTextField = "libelle"
        Me.DdlFonction.DataBind()

        Dim oControle As DropDownList = Me.DdlFonction
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If
    End Sub
    Private Sub ChargerListeCompteFacturant(ByVal oCompteFacturants As CompteFacturants, ByVal nIdClient As Integer)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idCompteFacturant")
        oDataTable.Columns.Add("libelle")
        For Each oCompteFacturant In oCompteFacturants
            oDataTable.Rows.Add(oCompteFacturant.nIdCompteFacturant, oCompteFacturant.sLibelle)
        Next
        Me.DdlCompteFacturant.DataSource = oDataTable
        Me.DdlCompteFacturant.DataValueField = "idCompteFacturant"
        Me.DdlCompteFacturant.DataTextField = "libelle"
        Me.DdlCompteFacturant.DataBind()

        Dim oControle As DropDownList = Me.DdlCompteFacturant
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If
    End Sub
    Private Sub chargerOptionsAffectees()
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idOption")
        oDataTable.Columns.Add("categorie")
        oDataTable.Columns.Add("libelle")
        For Each oOptionUtilisateur In oLigne.oOptionsUtilisateur
            oDataTable.Rows.Add(oOptionUtilisateur.nIdOption, oOptionUtilisateur.oCategorieOptionUtilisateur.sLibelle, oOptionUtilisateur.sLibelle)
        Next
        Me.GvOptionsAffectees.DataSource = oDataTable
        Me.GvOptionsAffectees.DataBind()
        If sTypeModif = "visu" Or sTypeModif = "visucomplet" Or sTypeModif = "delete" Then
            Me.GvOptionsAffectees.Columns(Me.GvOptionsAffectees.Columns.Count - 1).Visible = False
        End If
    End Sub
    Private Sub ChargerOptionsProfil()
        '    Dim oDataTable As New DataTable
        '    oDataTable.Columns.Add("idOption")
        '    oDataTable.Columns.Add("categorie")
        '    oDataTable.Columns.Add("libelle")
        '    For Each oOptionProfil In oProfil.oOptionsProfils
        '        oDataTable.Rows.Add(oOptionProfil.nIdOptionProfil, oOptionProfil.oCategorieOptionUtilisateur.sLibelle, oOptionProfil.sLibelle)
        '    Next

        '    Me.GVProfil.DataSource = oDataTable
        '    Me.GVProfil.DataBind()
        '    If sTypeModif = "visu" Or sTypeModif = "visucomplet" Or sTypeModif = "delete" Then
        '        Me.GVProfil.Columns(Me.GVProfil.Columns.Count - 1).Visible = False
        '    End If
    End Sub
    Private Sub chargerOptionsLibres(ByVal oOptionsUtilisateur As OptionsUtilisateur)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idOption")
        oDataTable.Columns.Add("categorie")
        oDataTable.Columns.Add("libelle")
        For Each oOptionUtilisateur In oOptionsUtilisateur
            If Not oLigne.oOptionsUtilisateur Is Nothing Then
                Dim oListeFiltree = From oOptionUtilisateurTri In oLigne.oOptionsUtilisateur Where oOptionUtilisateur.nIdOption = oOptionUtilisateurTri.nIdOption
                If oListeFiltree.Count() = 0 Then
                    oDataTable.Rows.Add(oOptionUtilisateur.nIdOption, oOptionUtilisateur.oCategorieOptionUtilisateur.sLibelle, oOptionUtilisateur.sLibelle)
                End If
            Else
                oDataTable.Rows.Add(oOptionUtilisateur.nIdOption, oOptionUtilisateur.oCategorieOptionUtilisateur.sLibelle, oOptionUtilisateur.sLibelle)
            End If
        Next
        Me.GvOptionsDisponibles.DataSource = oDataTable
        Me.GvOptionsDisponibles.DataBind()
        If sTypeModif = "visu" Or sTypeModif = "visucomplet" Or sTypeModif = "delete" Then
            Me.GvOptionsDisponibles.Columns(Me.GvOptionsDisponibles.Columns.Count - 1).Visible = False
        End If
    End Sub
    'Private Sub GvProfil_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GvOptionsAffectees.RowCommand
    'If e.CommandName = "enlever" Then
    '    Dim nRowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
    '    Dim oOptionsProfils As New OptionsProfils
    '    Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
    '    oOptionsProfils.chargerOptionsProfil(oProfil.sNomProfil, nIdClient)
    '    'Dim oListeFiltree = From oOptionUtilisateur In oLigne.oOptionsUtilisateur Where oOptionUtilisateur.nIdOption = CInt(GVProfil.DataKeys(nRowIndex).Value)
    '    oLigne.oOptionsUtilisateur.Remove(CType(oListeFiltree(0), OptionUtilisateur))
    '    ChargerOptionsProfil()
    'End If
    'End Sub
    Private Sub GvOptionsAffectees_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GvOptionsAffectees.RowCommand
        If e.CommandName = "enlever" Then
            Dim nRowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim oOptionsUtilisateur As New OptionsUtilisateur
            Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
            oOptionsUtilisateur.chargerOptionsClient(nIdClient)
            Dim oListeFiltree = From oOptionUtilisateur In oLigne.oOptionsUtilisateur Where oOptionUtilisateur.nIdOption = CInt(GvOptionsAffectees.DataKeys(nRowIndex).Value)
            oLigne.oOptionsUtilisateur.Remove(CType(oListeFiltree(0), OptionUtilisateur))
            chargerOptionsAffectees()
            chargerOptionsLibres(oOptionsUtilisateur)
        End If
    End Sub
    Private Sub GvOptionsDisponibles_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GvOptionsDisponibles.RowCommand
        If e.CommandName = "ajouter" Then
            Dim nRowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim oOptionUtilisateur As New OptionUtilisateur
            Dim oOptionsUtilisateur As New OptionsUtilisateur
            Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
            oOptionsUtilisateur.chargerOptionsClient(nIdClient)
            oOptionUtilisateur.chargerOptionParId(CInt(GvOptionsDisponibles.DataKeys(nRowIndex).Value))
            oLigne.oOptionsUtilisateur.Add(oOptionUtilisateur)
            chargerOptionsAffectees()
            chargerOptionsLibres(oOptionsUtilisateur)
        End If
    End Sub
    Private Sub BtnAjouterMateriel_Click(sender As Object, e As EventArgs) Handles BtnAjouterMateriel.Click

        Dim oStockAAjouter As New Stock
        Dim oOptionsUtilisateur As New OptionsUtilisateur
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oStockAAjouter.chargerStockParId(CInt(Me.TxtIdStock.Value))
        oStockAAjouter.dDateAttribution = Now
        Select Case oStockAAjouter.oArticle.oTypeMateriel.sLibelle
            Case "Terminaux Données", "Terminaux Voix et GPRS"
                Dim CompteurTest = From oStock In oLigne.oStocks Where oStock.oArticle.oTypeMateriel.sLibelle.StartsWith("Terminaux") And oStock.sTypeLiaison = "Principal"
                If CompteurTest.Count() = 0 Then
                    oStockAAjouter.sTypeLiaison = "Principal"
                Else
                    oStockAAjouter.sTypeLiaison = "Secondaire"
                End If
                oLigne.oStocks.Add(oStockAAjouter)
            Case "Carte"
                Dim CompteurTest = From oStock In oLigne.oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = "Carte" And oStock.sTypeLiaison = "Principale"
                If CompteurTest.Count() = 0 Then
                    oStockAAjouter.sTypeLiaison = "Principale"
                Else
                    oStockAAjouter.sTypeLiaison = "Jumelle"
                End If
                oLigne.oStocks.Add(oStockAAjouter)
            Case "Accessoires"
                oStockAAjouter.sTypeLiaison = ""
                oLigne.oStocks.Add(oStockAAjouter)
        End Select


        Dim oStocks As New Stocks
        oStocks.chargerStocksLibres(nIdClient)
        oStocks = FiltrerStocksAttribuesCNES(oStocks)
        ChargerListeTypeMaterielCNES(oStocks)
        ChargerListeMarqueCNES(oStocks)
        ChargerListeModeleCNES(oStocks)
        ChargerListeCodeReferenceCNES(oStocks)
        ChargerListeCodeEmeiCNES(oStocks)
        ChargerListeNumSerieCNES(oStocks)
        ChargerIdStockCNES(oStocks)

        chargerStocksAffectesCNES()

    End Sub
    Private Function FiltrerStocksAttribuesCNES(ByRef oStocks As Stocks) As Stocks
        Dim oStocksLibre As New Stocks
        Dim oListeFiltree = oStocks.Where(Function(a) oLigne.oStocks.Contains(a) = False)

        For Each oStock In oLigne.oStocks
            oListeFiltree = From oStockTest In oStocks Where oStockTest.nIdArticleStock <> oStock.nIdArticleStock
        Next

        For Each oStock In oListeFiltree
            oStocksLibre.Add(oStock)
        Next
        Return oStocksLibre
    End Function
    Private Sub ChargerListeTypeMaterielCNES(ByVal oStocks As Stocks)

        Dim sListeStock = From oStock In oStocks Group oStock.oArticle By oStock.oArticle.oTypeMateriel.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")

        For Each sTexte In sListeStock
            Select Case sTexte
                Case "Terminaux Données", "Terminaux Voix et GPRS"
                    Dim CompteurTest = From oStock In oLigne.oStocks Where oStock.oArticle.oTypeMateriel.sLibelle.StartsWith("Terminaux")
                    If CompteurTest.Count() <> 2 Then
                        oDataTable.Rows.Add(sTexte)
                    End If
                Case "Carte"
                    Dim CompteurTest = From oStock In oLigne.oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = "Carte"
                    If CompteurTest.Count() <> 2 Then
                        oDataTable.Rows.Add(sTexte)
                    End If
                Case "Accessoires"
                    oDataTable.Rows.Add(sTexte)
            End Select

        Next
        Me.DdlTypeMateriel.DataSource = oDataTable
        Me.DdlTypeMateriel.DataValueField = "libelle"
        Me.DdlTypeMateriel.DataTextField = "libelle"
        Me.DdlTypeMateriel.DataBind()

        Dim oControle As DropDownList = Me.DdlTypeMateriel
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If


    End Sub
    Private Sub ChargerListeMarqueCNES(ByVal oStocks As Stocks)
        Dim sListeMarque = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue Group oStock.oArticle By oStock.oArticle.oMarque.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeMarque
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlMarque.DataSource = oDataTable
        Me.DdlMarque.DataValueField = "libelle"
        Me.DdlMarque.DataTextField = "libelle"
        Me.DdlMarque.DataBind()

        Dim oControle As DropDownList = Me.DdlMarque
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeModeleCNES(ByVal oStocks As Stocks)
        Dim sListeModele = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue Group oStock.oArticle By oStock.oArticle.sModele Into TupleGroup = Group Select sModele Order By sModele
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeModele
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlModele.DataSource = oDataTable
        Me.DdlModele.DataValueField = "libelle"
        Me.DdlModele.DataTextField = "libelle"
        Me.DdlModele.DataBind()

        Dim oControle As DropDownList = Me.DdlModele
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeCodeReferenceCNES(ByVal oStocks As Stocks)
        Dim sListeCodeReference = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue Group oStock.oArticle By oStock.sCodeReference Into TupleGroup = Group Select sCodeReference Order By sCodeReference
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeCodeReference
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlCodeReference.DataSource = oDataTable
        Me.DdlCodeReference.DataValueField = "libelle"
        Me.DdlCodeReference.DataTextField = "libelle"
        Me.DdlCodeReference.DataBind()

        Dim oControle As DropDownList = Me.DdlCodeReference
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub

    Private Sub ChargerListeCodeEmeiCNES(ByVal oStocks As Stocks)
        Dim sListeCodeEmei = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue Group oStock.oArticle By oStock.sCodeEmei Into TupleGroup = Group Select sCodeEmei Order By sCodeEmei
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeCodeEmei
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlCodeEmei.DataSource = oDataTable
        Me.DdlCodeEmei.DataValueField = "libelle"
        Me.DdlCodeEmei.DataTextField = "libelle"
        Me.DdlCodeEmei.DataBind()

        Dim oControle As DropDownList = Me.DdlCodeEmei
        Dim sListeCodeEmeiSelectionne = From oStock In oStocks Where oStock.sCodeReference = Me.DdlCodeReference.SelectedValue Group oStock.oArticle By oStock.sCodeEmei Into TupleGroup = Group Select sCodeEmei Order By sCodeEmei

        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            If sListeCodeEmeiSelectionne.Count() <> 0 Then
                Try
                    oControle.SelectedValue = sListeCodeEmeiSelectionne(0)
                Catch

                End Try
            End If
        End If
    End Sub
    Private Sub ChargerListeNumSerieCNES(ByVal oStocks As Stocks)
        Dim sListeNumSerie = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue Group oStock.oArticle By oStock.sNumSerie Into TupleGroup = Group Select sNumSerie Order By sNumSerie
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeNumSerie
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlNumSerie.DataSource = oDataTable
        Me.DdlNumSerie.DataValueField = "libelle"
        Me.DdlNumSerie.DataTextField = "libelle"
        Me.DdlNumSerie.DataBind()

        Dim oControle As DropDownList = Me.DdlNumSerie
        Dim sListeNumSerieSelectionne = From oStock In oStocks Where oStock.sCodeReference = Me.DdlCodeReference.SelectedValue Group oStock.oArticle By oStock.sNumSerie Into TupleGroup = Group Select sNumSerie Order By sNumSerie

        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            If sListeNumSerieSelectionne.Count() <> 0 Then
                Try
                    oControle.SelectedValue = sListeNumSerieSelectionne(0)
                Catch

                End Try
            End If
        End If
    End Sub
    Private Sub ChargerIdStockCNES(ByVal oStocks As Stocks)
        Dim sListeCodeReference = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue And oStock.sCodeReference = Me.DdlCodeReference.SelectedValue Group oStock By oStock.nIdArticleStock Into TupleGroup = Group Select nIdArticleStock Order By nIdArticleStock

        For Each sTexte In sListeCodeReference
            Me.TxtIdStock.Value = CStr(sTexte)
        Next

    End Sub
    Private Sub chargerStocksAffectesCNES()
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idArticleStock")
        oDataTable.Columns.Add("image")
        oDataTable.Columns.Add("libelleTypeMateriel")
        oDataTable.Columns.Add("libelleMarque")
        oDataTable.Columns.Add("modele")
        oDataTable.Columns.Add("typeLiaison")
        oDataTable.Columns.Add("codeReference")
        oDataTable.Columns.Add("dateAttribution")
        oDataTable.Columns.Add("lien")
        Dim sTexteImage As String = ""
        Dim sLien As String = Nothing

        For Each oStock In oLigne.oStocks
            Select Case sTypeModif
                Case "insert", "edit"
                    sLien = "<span onclick=""EditerStockCNES('" + oStock.nIdArticleStock.ToString + "','edit');""><img class=""imageDetail"" src=""images/icones/ico_loupe.png"" title=""Afficher le détail"" alt=""Afficher le détail""></span>"
                Case "delete", "visu", "visucomplet"
                    sLien = "<span onclick=""EditerStockCNES('" + oStock.nIdArticleStock.ToString + "','visu');""><img class=""imageDetail"" src=""images/icones/ico_loupe.png"" title=""Afficher le détail"" alt=""Afficher le détail""></span>"
            End Select

            Select Case oStock.oArticle.oTypeMateriel.sLibelle
                Case "Accessoires"
                    sTexteImage = "<img src=""images/icones/ico_Accessoire.png"" class=""imageDataGridView"">"
                Case "Carte"
                    sTexteImage = "<img src=""images/icones/ico_Carte.png"" class=""imageDataGridView"">"
                Case "Terminaux Données", "Terminaux Voix et GPRS"
                    sTexteImage = "<img src=""images/icones/ico_Terminal.png"" class=""imageDataGridView"">"
            End Select
            oDataTable.Rows.Add(oStock.nIdArticleStock, sTexteImage, oStock.oArticle.oTypeMateriel.sLibelle, oStock.oArticle.oMarque.sLibelle, oStock.oArticle.sModele, oStock.sTypeLiaison, oStock.sCodeReference, oStock.dDateAttribution.ToString("dd/MM/yyyy"), sLien)
        Next
        Me.GvMaterielAffectes.DataSource = oDataTable
        Me.GvMaterielAffectes.DataBind()
        If sTypeModif = "visu" Or sTypeModif = "visucomplet" Or sTypeModif = "delete" Then
            Me.GvMaterielAffectes.Columns(Me.GvMaterielAffectes.Columns.Count - 1).Visible = False
        End If
    End Sub
    Private Sub DDLProfil_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLProfil.SelectedIndexChanged
        MsgBox("Ca passe")
    End Sub
    Private Function FiltrerStocksAttribues(ByRef oStocks As Stocks) As Stocks
        Dim oStocksLibre As New Stocks
        Dim oListeFiltree = oStocks.Where(Function(a) oLigne.oStocks.Contains(a) = False)

        For Each oStock In oLigne.oStocks
            oListeFiltree = From oStockTest In oStocks Where oStockTest.nIdArticleStock <> oStock.nIdArticleStock
        Next

        For Each oStock In oListeFiltree
            oStocksLibre.Add(oStock)
        Next
        Return oStocksLibre
    End Function
    Private Sub ChargerListeTypeMateriel(ByVal oStocks As Stocks)

        Dim sListeStock = From oStock In oStocks Group oStock.oArticle By oStock.oArticle.oTypeMateriel.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")

        For Each sTexte In sListeStock
            Select Case sTexte
                Case "Terminaux Données", "Terminaux Voix et GPRS"
                    Dim CompteurTest = From oStock In oLigne.oStocks Where oStock.oArticle.oTypeMateriel.sLibelle.StartsWith("Terminaux")
                    If CompteurTest.Count() <> 2 Then
                        oDataTable.Rows.Add(sTexte)
                    End If
                Case "Carte"
                    Dim CompteurTest = From oStock In oLigne.oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = "Carte"
                    If CompteurTest.Count() <> 2 Then
                        oDataTable.Rows.Add(sTexte)
                    End If
                Case "Accessoires"
                    oDataTable.Rows.Add(sTexte)
            End Select

        Next
        Me.DdlTypeMateriel.DataSource = oDataTable
        Me.DdlTypeMateriel.DataValueField = "libelle"
        Me.DdlTypeMateriel.DataTextField = "libelle"
        Me.DdlTypeMateriel.DataBind()

        Dim oControle As DropDownList = Me.DdlTypeMateriel
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If


    End Sub

    Private Sub ChargerListeMarque(ByVal oStocks As Stocks)
        Dim sListeMarque = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue Group oStock.oArticle By oStock.oArticle.oMarque.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeMarque
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlMarque.DataSource = oDataTable
        Me.DdlMarque.DataValueField = "libelle"
        Me.DdlMarque.DataTextField = "libelle"
        Me.DdlMarque.DataBind()

        Dim oControle As DropDownList = Me.DdlMarque
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeModele(ByVal oStocks As Stocks)
        Dim sListeModele = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue Group oStock.oArticle By oStock.oArticle.sModele Into TupleGroup = Group Select sModele Order By sModele
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeModele
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlModele.DataSource = oDataTable
        Me.DdlModele.DataValueField = "libelle"
        Me.DdlModele.DataTextField = "libelle"
        Me.DdlModele.DataBind()

        Dim oControle As DropDownList = Me.DdlModele
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeCodeReference(ByVal oStocks As Stocks)
        Dim sListeCodeReference = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue Group oStock.oArticle By oStock.sCodeReference Into TupleGroup = Group Select sCodeReference Order By sCodeReference
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeCodeReference
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlCodeReference.DataSource = oDataTable
        Me.DdlCodeReference.DataValueField = "libelle"
        Me.DdlCodeReference.DataTextField = "libelle"
        Me.DdlCodeReference.DataBind()

        Dim oControle As DropDownList = Me.DdlCodeReference
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub

    Private Sub ChargerListeCodeEmei(ByVal oStocks As Stocks)
        Dim sListeCodeEmei = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue Group oStock.oArticle By oStock.sCodeEmei Into TupleGroup = Group Select sCodeEmei Order By sCodeEmei
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeCodeEmei
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlCodeEmei.DataSource = oDataTable
        Me.DdlCodeEmei.DataValueField = "libelle"
        Me.DdlCodeEmei.DataTextField = "libelle"
        Me.DdlCodeEmei.DataBind()

        Dim oControle As DropDownList = Me.DdlCodeEmei
        Dim sListeCodeEmeiSelectionne = From oStock In oStocks Where oStock.sCodeReference = Me.DdlCodeReference.SelectedValue Group oStock.oArticle By oStock.sCodeEmei Into TupleGroup = Group Select sCodeEmei Order By sCodeEmei

        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            If sListeCodeEmeiSelectionne.Count() <> 0 Then
                Try
                    oControle.SelectedValue = sListeCodeEmeiSelectionne(0)
                Catch

                End Try
            End If
        End If
    End Sub

    Private Sub ChargerListeNumSerie(ByVal oStocks As Stocks)
        Dim sListeNumSerie = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue Group oStock.oArticle By oStock.sNumSerie Into TupleGroup = Group Select sNumSerie Order By sNumSerie
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeNumSerie
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlNumSerie.DataSource = oDataTable
        Me.DdlNumSerie.DataValueField = "libelle"
        Me.DdlNumSerie.DataTextField = "libelle"
        Me.DdlNumSerie.DataBind()

        Dim oControle As DropDownList = Me.DdlNumSerie
        Dim sListeNumSerieSelectionne = From oStock In oStocks Where oStock.sCodeReference = Me.DdlCodeReference.SelectedValue Group oStock.oArticle By oStock.sNumSerie Into TupleGroup = Group Select sNumSerie Order By sNumSerie

        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            If sListeNumSerieSelectionne.Count() <> 0 Then
                Try
                    oControle.SelectedValue = sListeNumSerieSelectionne(0)
                Catch

                End Try
            End If
        End If
    End Sub
    Private Sub ChargerIdStock(ByVal oStocks As Stocks)
        Dim sListeCodeReference = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue And oStock.sCodeReference = Me.DdlCodeReference.SelectedValue Group oStock By oStock.nIdArticleStock Into TupleGroup = Group Select nIdArticleStock Order By nIdArticleStock

        For Each sTexte In sListeCodeReference
            Me.TxtIdStock.Value = CStr(sTexte)
        Next

    End Sub
    Private Sub chargerStocksAffectes()
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idArticleStock")
        oDataTable.Columns.Add("image")
        oDataTable.Columns.Add("libelleTypeMateriel")
        oDataTable.Columns.Add("libelleMarque")
        oDataTable.Columns.Add("modele")
        oDataTable.Columns.Add("typeLiaison")
        oDataTable.Columns.Add("codeReference")
        oDataTable.Columns.Add("dateAttribution")
        oDataTable.Columns.Add("lien")
        Dim sTexteImage As String = ""
        Dim sLien As String = Nothing

        For Each oStock In oLigne.oStocks
            Select Case sTypeModif
                Case "insert", "edit"
                    If CInt(User.Identity.Name.Split(CChar("_"))(1)) = 118 Then
                        sLien = "<span onclick=""EditerStockCNES('" + oStock.nIdArticleStock.ToString + "','edit');""><img class=""imageDetail"" src=""images/icones/ico_loupe.png"" title=""Afficher le détail"" alt=""Afficher le détail""></span>"
                    Else
                        sLien = "<span onclick=""EditerStock('" + oStock.nIdArticleStock.ToString + "','edit');""><img class=""imageDetail"" src=""images/icones/ico_loupe.png"" title=""Afficher le détail"" alt=""Afficher le détail""></span>"
                    End If
                Case "delete", "visu", "visucomplet"
                    If CInt(User.Identity.Name.Split(CChar("_"))(1)) = 118 Then
                        sLien = "<span onclick=""EditerStockCNES('" + oStock.nIdArticleStock.ToString + "','visu');""><img class=""imageDetail"" src=""images/icones/ico_loupe.png"" title=""Afficher le détail"" alt=""Afficher le détail""></span>"
                    Else
                        sLien = "<span onclick=""EditerStock('" + oStock.nIdArticleStock.ToString + "','visu');""><img class=""imageDetail"" src=""images/icones/ico_loupe.png"" title=""Afficher le détail"" alt=""Afficher le détail""></span>"
                    End If
            End Select

            Select Case oStock.oArticle.oTypeMateriel.sLibelle
                Case "Accessoires"
                    sTexteImage = "<img src=""images/icones/ico_Accessoire.png"" class=""imageDataGridView"">"
                Case "Carte"
                    sTexteImage = "<img src=""images/icones/ico_Carte.png"" class=""imageDataGridView"">"
                Case "Terminaux Données", "Terminaux Voix et GPRS"
                    sTexteImage = "<img src=""images/icones/ico_Terminal.png"" class=""imageDataGridView"">"
            End Select
            oDataTable.Rows.Add(oStock.nIdArticleStock, sTexteImage, oStock.oArticle.oTypeMateriel.sLibelle, oStock.oArticle.oMarque.sLibelle, oStock.oArticle.sModele, oStock.sTypeLiaison, oStock.sCodeReference, oStock.dDateAttribution.ToString("dd/MM/yyyy"), sLien)
        Next
        Me.GvMaterielAffectes.DataSource = oDataTable
        Me.GvMaterielAffectes.DataBind()
        If sTypeModif = "visu" Or sTypeModif = "visucomplet" Or sTypeModif = "delete" Then
            Me.GvMaterielAffectes.Columns(Me.GvMaterielAffectes.Columns.Count - 1).Visible = False
        End If
    End Sub
    Private Sub DdlTypeForfait_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlTypeForfait.SelectedIndexChanged
        Dim oForfaits As New Forfaits
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oForfaits.chargerForfaits(nIdClient)
        ChargerListeForfait(oForfaits, nIdClient)
    End Sub
    Private Sub VerrouillerControles()
        Me.DDLNom.Enabled = False
        Me.TxtNumeroGSM.Enabled = False
        Me.TxtNumeroData.Enabled = False
        Me.TxtDateActivation.Enabled = False
        Me.DdlTypeMateriel.Enabled = False
        Me.DdlMarque.Enabled = False
        Me.DdlModele.Enabled = False
        Me.DdlCodeReference.Enabled = False
        Me.BtnAjouterMateriel.Enabled = False
        'Me.imgParamFonction.visible = False
        'Me.imgParamCompteFacturant.visible = False
        'Me.imgParamDiscrimination.Visible = False
        Me.DivMaterielDisponible.Style.Item("display") = "none"
    End Sub

    Private Sub GestionParcCNES_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete

        If Not IsNothing(DDLProfil.SelectedItem) Then
            Console.WriteLine("InitComplete --> Item : " & DDLProfil.SelectedItem.ToString & " Value : " & DDLProfil.SelectedValue.ToString & "Index : " & DDLProfil.SelectedIndex.ToString)
        End If

    End Sub

    Private Sub GestionParcCNES_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad

        If Not IsNothing(DDLProfil.SelectedItem) Then
            Console.WriteLine("Preload --> Item : " & DDLProfil.SelectedItem.ToString & " Value : " & DDLProfil.SelectedValue.ToString & "Index : " & DDLProfil.SelectedIndex.ToString)
        End If

    End Sub

    Private Sub BtnInsertValider_Click(sender As Object, e As EventArgs) Handles BtnInsertValider.Click

        Dim oFonction As New Fonction
        Dim oCompteFacturant As New CompteFacturant
        Dim oDiscrimination As New Discrimination
        Dim oForfait As New Forfait
        Dim oHierarchie As New Hierarchie
        Dim oCommande As New Commande
        oCommande.oOptionsAjoutees = New OptionsUtilisateur
        oCommande.oOptionsRetirees = New OptionsUtilisateur
        oCommande.oStockAjoutes = New Stocks
        oCommande.oStockRetires = New Stocks

        If Not IsDate(Me.TxtDateActivation.Text) Then
            Me.LblInformation.Visible = True
            Me.LblInformation.Text = "Vous devez saisir une date d'activation valide."
            Exit Sub
        End If

        If Me.TxtNom.Text = "" And Me.TxtPrenom.Text = "" Then
            Me.LblInformation.Visible = True
            Me.LblInformation.Text = "Vous devez saisir un nom ou un prénom avant de pouvoir valider votre opération"
            Exit Sub
        End If

        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oFonction.chargerFonctionParId(CInt(Me.DdlFonction.SelectedValue))
        oCompteFacturant.chargerCompteFacturantParId(CInt(Me.DdlCompteFacturant.SelectedValue))
        oForfait.chargerForfaitParId(RecupererIdForfait)
        oHierarchie.chargerHierarchieParId(RecupererIdHierarchie)

        oLigne.sNumeroGSM = Me.TxtNumeroGSM.Text
        oLigne.sNumeroData = Me.TxtNumeroData.Text
        oLigne.sNom = Me.TxtNom.Text
        oLigne.sPrenom = Me.TxtPrenom.Text
        oLigne.bEstActif = True
        oLigne.dDateActivation = CDate(Me.TxtDateActivation.Text)

        oLigne.nIdClient = nIdClient
        oLigne.oFonction = oFonction
        oLigne.oCompteFacturant = oCompteFacturant
        oLigne.oDiscrimination = oDiscrimination
        oLigne.oForfait = oForfait
        oLigne.oHierarchie = oHierarchie

        oLigne.nIdLigne = CInt(DDLNom.SelectedValue)
        'cDal.chargerLigneStructureParId(oLigne, oLigne.nIdLigne)

        oForfait.chargerForfaitParId(RecupererIdForfait)

        'oDiscrimination.chargerDiscriminationParId(CInt(Me.DdlDiscrimination.SelectedValue))

        oLigne.insererLigne()

        If Label2.Text = "Structure : " Then

            cDal.RecupererInfosStructureCNES(nIdClient, oLigne)
            cDal.RecupererInfosLigneCNES(nIdClient, oLigne)

            oLigne.sNom = TxtNom.Text
            oLigne.sPrenom = TxtPrenom.Text

        Else

            cDal.RecupererInfosLigneCNES(nIdClient, oLigne)

        End If

        oCommande.oNouvelleLigne = oLigne
        oCommande.sTypeCommande = "creation"
        oCommande.bEstRealiseFacture = False
        oCommande.bEstAffichable = True
        oCommande.bEstTelecharge = False

        '---TestMAteriel
        For Each oStock In oLigne.oStocks
            Dim oStockTest As New Stock
            oStockTest.chargerStockParId(oStock.nIdArticleStock)
            If oStockTest.sEtat <> "Non Attribué" Then
                Me.LblInformation.Visible = True
                Me.LblInformation.Text = "le matériel " & oStockTest.sCodeReference & " est indisponible car un autre utilisateur vient de l'attribuer."
                Exit Sub
            End If
        Next

        For Each oOptionUtilisateur In oLigne.oOptionsUtilisateur
            oLigne.ajouterOption(oOptionUtilisateur)
            oCommande.oOptionsAjoutees.Add(oOptionUtilisateur)
        Next

        For Each oStock In oLigne.oStocks
            oLigne.ajouterStock(oStock)
            oCommande.oStockAjoutes.Add(oStock)
        Next

        If User.Identity.Name.Split(CChar("_"))(2) <> "" And User.Identity.Name.Split(CChar("_"))(2) <> "0" Then
            oCommande.nIdUtilisateurPrincipal = 0
            oCommande.nIdUtilisateurSecondaire = CInt(User.Identity.Name.Split(CChar("_"))(2))
        Else
            oCommande.nIdUtilisateurPrincipal = CInt(User.Identity.Name.Split(CChar("_"))(0))
            oCommande.nIdUtilisateurSecondaire = 1
        End If

        oCommande.ajouterCommande()

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)

    End Sub
    Private Function RecupererIdForfait() As Integer
        Dim oForfaits As New Forfaits
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim Resultat As Integer = 0
        oForfaits.chargerForfaits(nIdClient)

        Dim oListeFiltree = From oForfait In oForfaits Where oForfait.oTypeForfait.sLibelle = Me.DdlTypeForfait.SelectedValue And oForfait.sLibelle = Me.DdlForfait.SelectedValue

        For Each oForfait In oListeFiltree
            Resultat = oForfait.nIdForfait
        Next
        Return Resultat
    End Function
    Private Function RecupererIdHierarchie() As Integer
        Dim oHierarchies As New Hierarchies
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim Resultat As Integer = 0
        oHierarchies.chargerHierarchies(nIdClient)
        Dim oListeFiltree = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = Me.DDLAffect.Text
        For Each oHierarchie In oListeFiltree
            Resultat = oHierarchie.nIdHierarchie
        Next
        Return Resultat
    End Function
End Class