﻿Public Class GestionParcFixeActivation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If


        Dim oclient As New Client(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        Dim oLigneFixe As New LigneFixe

        If Request.Item("idLigne") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            oLigneFixe.nIdLigneFixe = CInt(Request.Item("idLigne").ToString)
        End If

        oLigneFixe.chargerLigneFixeParId()
        If Not Page.IsPostBack Then
            If oLigneFixe.dDateActivation = Nothing Then
                Me.TxtDateActivation.Text = Now.ToString("dd/MM/yyyy")
            Else
                Me.TxtDateActivation.Text = oLigneFixe.dDateActivation.ToString("dd/MM/yyyy")
            End If
        End If

    End Sub

    Private Sub BtnActiverValider_Click(sender As Object, e As EventArgs) Handles BtnActiverValider.Click

        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If


        Dim oclient As New Client(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        Dim oLigneFixe As New LigneFixe

        If Request.Item("idLigne") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            oLigneFixe.nIdLigneFixe = CInt(Request.Item("idLigne").ToString)
        End If
        oLigneFixe.chargerLigneFixeParId()


        Dim regex As Regex = New Regex("^(01|02|03|04|05|08|09)[0-9]{8}$")
        Dim match As Match = regex.Match(Me.TxtNumeroFixe.Text)
        If Not match.Success And oLigneFixe.oTypeLigneFixe.sLibelleTypeLigne <> "Internet" Then
            Me.LblInformation.Text = "Le format du numéro n'est pas valide"
            Exit Sub
        Else
            Me.LblInformation.Text = ""
        End If

        If cDal.TesterSiNumeroExisteFixe(oLigneFixe) = True Then
            Me.LblInformation.Text = "Le numéro saisi existe déjà dans le parc."
            Exit Sub
        Else
            Me.LblInformation.Text = ""
        End If

        If Not IsDate(Me.TxtDateActivation.Text) Then
            Me.LblInformation.Text = "La date n'est pas valide"
            Exit Sub
        Else
            Me.LblInformation.Text = ""
        End If

        oLigneFixe.sNumeroLigneFixe = Me.TxtNumeroFixe.Text
        oLigneFixe.dDateActivation = CDate(Me.TxtDateActivation.Text)

        oLigneFixe.activerLigneFixe()

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub
    Private Sub BtnActiverAnnuler_Click(sender As Object, e As EventArgs) Handles BtnActiverAnnuler.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub

End Class