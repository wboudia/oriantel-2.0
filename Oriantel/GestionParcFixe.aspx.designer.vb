﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class GestionParcFixe
    
    '''<summary>
    '''Contrôle form1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''Contrôle sc1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents sc1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''Contrôle idArticle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents idArticle As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle MemoireOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MemoireOnglets As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle MasquageOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MasquageOnglets As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle TextePopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TextePopup As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle UpdatePanelPostback.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelPostback As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle checkboxpostback.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents checkboxpostback As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''Contrôle ImageAction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ImageAction As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''Contrôle LblTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LblTitre As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle lblBoiteGeneral.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents lblBoiteGeneral As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Label1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DdlTypeLigne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DdlTypeLigne As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Contrôle Label2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Label2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DdlUsage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DdlUsage As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Contrôle Label3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Label3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TxtNumeroGSM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TxtNumeroGSM As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle LblDateActivation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LblDateActivation As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TxtDateActivation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TxtDateActivation As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle UpdatePanel1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle PlaceHolder1_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder1_1 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder1_2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder1_2 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder1_3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder1_3 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder1_4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder1_4 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder1_5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder1_5 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder1_6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder1_6 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder1_7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder1_7 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder1_8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder1_8 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder1_9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder1_9 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle UpdateProgress1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateProgress1 As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''Contrôle tabs2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents tabs2 As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Contrôle UpdatePanel2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle PlaceHolder2_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder2_1 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder2_2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder2_2 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder2_3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder2_3 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder2_4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder2_4 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder2_5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder2_5 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder2_6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder2_6 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder2_7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder2_7 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder2_8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder2_8 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle UpdateProgress2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateProgress2 As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''Contrôle UpdatePanel3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanel3 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle PlaceHolder3_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder3_1 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder3_2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder3_2 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder3_3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder3_3 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder3_4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder3_4 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder3_5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder3_5 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder3_6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder3_6 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder3_7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder3_7 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder3_8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder3_8 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder3_9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder3_9 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder3_10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder3_10 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle UpdateProgress3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateProgress3 As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''Contrôle tabs4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents tabs4 As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Contrôle UpdatePanel4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanel4 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle PlaceHolder4_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder4_1 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder4_2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder4_2 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle UpdateProgress4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateProgress4 As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''Contrôle tabs5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents tabs5 As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Contrôle UpdatePanel5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanel5 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle PlaceHolder5_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder5_1 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder5_2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder5_2 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder5_3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder5_3 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder5_4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder5_4 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder5_5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder5_5 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder5_6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder5_6 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder5_7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder5_7 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder5_8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder5_8 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder5_9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder5_9 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle UpdateProgress5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateProgress5 As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''Contrôle tabs6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents tabs6 As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Contrôle UpdatePanel6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanel6 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle PlaceHolder6_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder6_1 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolder6_2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolder6_2 As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle UpdateProgress6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateProgress6 As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''Contrôle DivAction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DivAction As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Contrôle lblBoiteAction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents lblBoiteAction As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DivActionInsert.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DivActionInsert As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Contrôle BtnInsertValider.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnInsertValider As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle BtnInsertAnnuler.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnInsertAnnuler As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle DivInformation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DivInformation As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Contrôle LblInformation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LblInformation As Global.System.Web.UI.WebControls.Label
End Class
