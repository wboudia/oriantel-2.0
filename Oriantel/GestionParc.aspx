﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GestionParc.aspx.vb" Inherits="Oriantel.GestionParc" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html>

<%--<html xmlns="http://www.w3.org/1999/xhtml">--%>
<!--[if lt IE 7 ]> <html class="ie6" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<!--<![endif]-->

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64 128x128" href="../images/icones/favicon.ico" />
    
    <title>Oria Extranet</title>
    
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css' />
        <link href="css/onglets/jquery-ui-1.10.3.customParc.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/colorbox.css" rel="stylesheet" type="text/css" media="screen" />

    <%--  <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>--%>
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-fr.js"></script>
    <script type="text/javascript" src="js/jquery.formalize.min.js"></script>
    <script type="text/javascript" src="js/jquery.colorbox-min.js"></script>

    <%-- <link href="js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.css" rel="stylesheet" />--%>
    
    <link href="css/gestionParc.css" rel="stylesheet" type="text/css" media="screen" />
    
        <script>
        $(function () {
            if (document.getElementById("MemoireOnglets").value != "") {
                $("#tabs").tabs({ active: document.getElementById("MemoireOnglets").value });
            }
            else {
                $("#tabs").tabs();
            }
            $("#TxtDateActivation").datepicker($.datepicker.regional["fr"]);
        });

        function GererMemoireOnglets(idOnglet) {
            document.getElementById("MemoireOnglets").value = idOnglet;
        }

        function OuvrirPopup(Lien, largeur, hauteur, raffraichissement) {

            if (typeof (raffraichissement) == 'undefined') {
                raffraichissement = 0;
            }

            $.colorbox.settings.opacity = 0.7;

            jQuery.extend(jQuery.colorbox.settings, {
                current: "image {current} sur {total}",
                previous: "pr&eacute;c&eacute;dente",
                next: "suivante",
                close: "<span style='color:#206797;'><b style='position:relative;'>x&nbsp;</b>Fermer</span>",
                xhrError: "Impossible de charger ce contenu.",
                imgError: "Impossible de charger cette image.",
                slideshowStart: "d&eacute;marrer la pr&eacute;sentation",
                slideshowStop: "arr&ecirc;ter la pr&eacute;sentation",
                onClosed: function () { if (raffraichissement == 1) { __doPostBack('checkboxpostback', '') } }
            });

            $.colorbox({
                innerWidth: largeur + "px",
                innerHeight: hauteur + "px",
                iframe: true,
                href: Lien
            });
                    }

        function EditerStock(idArticle, typemodif, filtreTypeMateriel) {
            if (typeof (filtreTypeMateriel) == 'undefined') {
                filtreTypeMateriel = 'aucun';
            }
            OuvrirPopup('GestionStock.aspx?idArticleStock=' + idArticle + '&typemodif=' + typemodif + '&filtreTypeMateriel=' + filtreTypeMateriel, '595', '460', 1);
            //OuvrirPopup('GestionStockCNES.aspx?idArticleStock=' + idArticle + '&typemodif=' + typemodif + '&filtreTypeMateriel=' + filtreTypeMateriel, '595', '460', 1);
        }

    </script>

</head>
<body style="width: 680px; height: 400px">
    <form id="form1" runat="server">

        <asp:ScriptManager ID="sc1" runat="server" EnablePartialRendering="true" EnableViewState="false"></asp:ScriptManager>

        <asp:HiddenField ID="idArticle" runat="server" />
        <asp:HiddenField ID="MemoireOnglets" runat="server" />
        
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Always" runat="server">
            <ContentTemplate>
                <asp:CheckBox ID="checkboxpostback" runat="server" Visible="false" AutoPostBack="true" CausesValidation="true" />
                            </ContentTemplate>
        </asp:UpdatePanel>

        <div style="text-align: center; width: 680px; height: 20px; float: left">
            <span style="padding-top: 10px; font-size: 14pt; text-transform: uppercase; position: relative;">
                <asp:Image ID="ImageAction" Visible="false" runat="server" Style="height: 22px; width: 22px; position: absolute; left: -27Px;" />
                <asp:Label ID="LblTitre" runat="server" Text=""></asp:Label>
            </span>
        </div>

        <div id="Principal" style="position: absolute; top: 40px; left: 12px; height: 110px; width: 680px;">
            <img src="images/GestionParc/cadre_general.png" style="z-index: 99; left: 0px; position: absolute; top: 0px; height: 105px; width: 680px; right: 8px;" />
            <asp:Label ID="lblBoiteGeneral" runat="server" Height="16px" Style="left: 20px; position: absolute; top: 8px; z-index: 102; right: 919px;" Width="250px">Informations générales : </asp:Label>
            <p>
                <asp:Label ID="Label1" runat="server" Text="Nom :" Width="60px" Height="16px" Style="position: absolute; left: 20px; top: 50px; z-index: 100"></asp:Label>
                <asp:TextBox ID="TxtNom" Class="champs" runat="server" Height="16px" Width="190px" Style="position: absolute; left: 70px; top: 50px; z-index: 100"></asp:TextBox>
            </p>
            <p>
                <asp:Label ID="Label2" runat="server" Text="Prénom :" Height="16px" Width="60px" Style="position: absolute; left: 20px; top: 70px; z-index: 100"></asp:Label>
                <asp:TextBox ID="TxtPrenom" Class="champs" runat="server" Width="190px" Style="position: absolute; left: 70px; top: 70px; z-index: 100"></asp:TextBox>
            </p>

                        <p>
                <asp:Label ID="Label3" runat="server" Text="Numéro GSM :" Height="16px" Width="115px" Style="position: absolute; left: 280px; top: 50px; z-index: 100"></asp:Label>
                <asp:TextBox ID="TxtNumeroGSM" Enabled="false" Class="champs" runat="server" Width="80px" Style="position: absolute; height: 15px; left: 360px; top: 50px; z-index: 100"></asp:TextBox>
            </p>
            <p>
                <asp:Label ID="Label4" runat="server" Text="Numéro Data :" Height="16px" Width="115px" Style="position: absolute; left: 280px; top: 70px; z-index: 100"></asp:Label>
                <asp:TextBox ID="TxtNumeroData" Class="champs" runat="server" Width="80px" Style="position: absolute; left: 360px; top: 70px; z-index: 100"></asp:TextBox>
            </p>
            <p>
                <asp:Label ID="LblDateActivation" runat="server" Text="Date activation :" Height="16px" Width="115px" Style="position: absolute; left: 470px; top: 50px; z-index: 100"></asp:Label>
                <asp:TextBox ID="TxtDateActivation" Enabled="false" Class="champs" runat="server" Width="70px" Height="15px" Style="position: absolute; left: 575px; top: 50px; z-index: 100"></asp:TextBox>
            </p>
            <p>
                <asp:Label ID="LblDateDesactivation" Visible="false" runat="server" Text="Date désactivation :" Height="16px" Width="115px" Style="position: absolute; left: 470px; top: 70px; z-index: 100"></asp:Label>
                <asp:TextBox ID="TxtDateDesactivation" Visible="false" Enabled="false" Class="champs" runat="server" Width="70px" Style="position: absolute; left: 575px; top: 70px; z-index: 100"></asp:TextBox>
            </p>
        </div>
        
        <div id="tabs" style="position: absolute; left: 12px; top: 153px; width: 680px">
            <ul class="ulonglets">
                <li><a href="#tabs1" onclick="GererMemoireOnglets(0);">Administratif</a> </li>
                <li><a href="#tabs2" onclick="GererMemoireOnglets(1);">Abonnement</a></li>
                <li runat="server" id="LiMateriel" style="display: block;"><a href="#tabs3" onclick="GererMemoireOnglets(2);">Matériel</a></li>
            </ul>
            <div id="tabs1" style="border: 1px solid white; height: 290px">
                <asp:UpdatePanel ID="UpdatePanelAdministratif" UpdateMode="Always" runat="server">
                    <ContentTemplate>
                        <p style="width: 550px">
                            <asp:Label ID="Label6" runat="server" Class="champs" Text="Fonction :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 50px"></asp:Label>
                            <asp:DropDownList ID="DdlFonction" runat="server" AutoPostBack="True" Style="position: absolute; width: 300px; height: 21px; left: 130px; top: 50px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                            <img id="imgParamFonction" runat="server" alt="" src="images/icones/ico_parametrage.png" style="position: absolute; cursor: pointer; width: 15px; height: 15px; left: 440px; top: 52px" onclick="OuvrirPopup('Report_1.aspx?module=9&rapport=8&masquer=false', '465', '400', 1);" />

                        </p>
                        <p style="width: 550px">
                            <asp:Label ID="Label7" runat="server" class="champs" Text="Compte Facturant :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 80px"></asp:Label>
                            <asp:DropDownList ID="DdlCompteFacturant" runat="server" AutoPostBack="True" Style="position: absolute; width: 300px; height: 21px; left: 130px; top: 80px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                            <img id="imgParamCompteFacturant" runat="server" alt="" src="images/icones/ico_parametrage.png" style="position: absolute; cursor: pointer; width: 15px; height: 15px; left: 440px; top: 82px" onclick="OuvrirPopup('Report_1.aspx?module=9&rapport=9&masquer=false', '350', '400', 1);" />
                        </p>
                        <p style="width: 550px">
                            <asp:Label ID="Label8" runat="server" class="champs" Text="Direction générale:" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 110px"></asp:Label>
                            <asp:DropDownList ID="DdlHierarchie1" runat="server" AutoPostBack="True" Style="position: absolute; width: 300px; height: 21px; left: 130px; top: 110px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                        </p>
                        <p style="width: 550px">
                            <asp:Label ID="Label9" runat="server" class="champs" Text="Direction :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 130px"></asp:Label>
                            <asp:DropDownList ID="DdlHierarchie2" runat="server" AutoPostBack="True" Style="position: absolute; width: 300px; height: 21px; left: 130px; top: 130px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                        </p>
                        <p style="width: 550px">
                            <asp:Label ID="Label10" runat="server" class="champs" Text="Direction adjointe :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 150px"></asp:Label>
                            <asp:DropDownList ID="DdlHierarchie3" runat="server" AutoPostBack="True" Style="position: absolute; width: 300px; height: 21px; left: 130px; top: 150px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                        </p>
                        <p style="width: 550px">
                            <asp:Label ID="Label11" runat="server" class="champs" Text="Groupement service :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 170px"></asp:Label>
                            <asp:DropDownList ID="DdlHierarchie4" runat="server" AutoPostBack="True" Style="position: absolute; width: 300px; height: 21px; left: 130px; top: 170px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                        </p>
                        <p style="width: 550px">
                            <asp:Label ID="Label12" runat="server" class="champs" Text="Service :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 190px"></asp:Label>
                            <asp:DropDownList ID="DdlHierarchie5" runat="server" AutoPostBack="True" Style="position: absolute; width: 300px; height: 21px; left: 130px; top: 190px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                        </p>
                        <p style="width: 550px">
                            <asp:Label ID="Label13" runat="server" class="champs" Text="Code site :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 210px"></asp:Label>
                            <asp:DropDownList ID="DdlHierarchie6" runat="server" AutoPostBack="True" Style="position: absolute; width: 300px; height: 21px; left: 130px; top: 210px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                        </p>

                        <asp:HiddenField ID="TxtIdHierarchie" runat="server" />
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanelAdministratif"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img src="images/Chargement.gif" alt="">
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <p style="width: 550px">
                    <asp:LinkButton CssClass="BpBordereau" ID="BpBordereauMateriel" runat="server" Style="color: #206797; position: absolute; height: 16px; width: 190px; left: 20px; top: 310px" CausesValidation="false">Bordereau de remise de matériel</asp:LinkButton>
                </p>
            </div>
            <div id="tabs2" style="border: 1px solid white; height: 290px">
                <asp:UpdatePanel ID="UpdatePanelAbonnement" UpdateMode="Always" runat="server">
                    <ContentTemplate>
                        <p style="width: 550px">
                            <asp:Label ID="Label14" runat="server" Class="champs" Text="Opérateur :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 50px"></asp:Label>
                            <asp:DropDownList ID="DdlOperateur" runat="server" AutoPostBack="True" Style="position: absolute; width: 150px; height: 21px; left: 130px; top: 50px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                        </p>
                        <p style="width: 550px">
                            <asp:Label ID="Label15" class="champs" runat="server" Text="Type forfait :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 70px"></asp:Label>
                            <asp:DropDownList ID="DdlTypeForfait" runat="server" AutoPostBack="True" Style="position: absolute; width: 150px; height: 21px; left: 130px; top: 70px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                        </p>
                        <p style="width: 550px">
                            <asp:Label ID="Label16" runat="server" Class="champs" Text="Forfait :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 90px"></asp:Label>
                            <asp:DropDownList ID="DdlForfait" runat="server" AutoPostBack="True" Style="position: absolute; width: 150px; height: 21px; left: 130px; top: 90px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                        </p>
                        <p style="width: 550px">
                            <asp:Label ID="Label17" runat="server" class="champs" Text="Prix Abonnement :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 110px"></asp:Label>
                            <asp:TextBox ID="TxtPrixAbonnement" class="champs" Enabled="false" runat="server" Style="position: absolute; width: 146px; left: 130px; top: 110px"></asp:TextBox>
                        </p>
                        <p style="width: 550px">
                            <asp:Label ID="Label18" runat="server" class="champs" Text="Discrimination :" Style="position: absolute; height: 16px; width: 140px; left: 350px; top: 50px"></asp:Label>
                            <asp:DropDownList ID="DdlDiscrimination" runat="server" AutoPostBack="True" Style="position: absolute; width: 150px; height: 20px; left: 450px; top: 50px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                            <img id="imgParamDiscrimination" runat="server" alt="" src="images/icones/ico_parametrage.png" style="position: absolute; cursor: pointer; width: 15px; height: 15px; left: 610px; top: 52px" onclick="OuvrirPopup('Report_1.aspx?module=9&rapport=4&masquer=false', '295', '400', 1);" />
                        </p>
                        <p style="width: 275px">
                            <asp:Label ID="Label26" runat="server" class="champs" Text="Options affectées:" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 150px"></asp:Label>
                            <asp:Label ID="LblOptionsDisponibles" runat="server" class="champs" Text="Options disponibles:" Style="position: absolute; height: 16px; width: 140px; left: 350px; top: 150px"></asp:Label>
                        </p>

                        <div style="position: absolute; height: 150px; left: 20px; top: 170px" class="DivGridOptions">
                            <asp:GridView ID="GvOptionsAffectees" runat="server" AutoGenerateColumns="False" class="Grid" DataKeyNames="idOption">
                                <Columns>
                                    <asp:BoundField HeaderText="idOption" DataField="idOption" Visible="false" />
                                    <asp:BoundField HeaderText="Catégorie" DataField="categorie" ControlStyle-Width="80px" />
                                    <asp:BoundField HeaderText="Option" DataField="libelle" ControlStyle-Width="80px" />
                                    <asp:ButtonField CausesValidation="True" CommandName="enlever" ButtonType="Image" ImageUrl="images/icones/ico_enlever.png" ControlStyle-CssClass="actionEnlever" HeaderText="Action" />
                                </Columns>
                            </asp:GridView>
                        </div>

                        <div style="position: absolute; height: 150px; left: 350px; top: 170px" class="DivGridOptions">
                            <asp:GridView ID="GvOptionsDisponibles" runat="server" AutoGenerateColumns="False" class="Grid" DataKeyNames="idOption">
                                <Columns>
                                    <asp:BoundField HeaderText="idOption" DataField="idOption" Visible="false" />
                                    <asp:BoundField HeaderText="Catégorie" DataField="categorie" />
                                    <asp:BoundField HeaderText="Option" DataField="libelle" />
                                    <asp:ButtonField CausesValidation="True" CommandName="ajouter" ButtonType="Image" ImageUrl="images/icones/ico_ajouter.png" ControlStyle-CssClass="actionAjouter" HeaderText="Action" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <asp:UpdateProgress ID="UpdateProgress3" runat="server" AssociatedUpdatePanelID="UpdatePanelAbonnement"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img src="images/Chargement.gif" alt="">
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="tabs3" runat="server" style="display: block; border: 1px solid white; height: 290px">
                <asp:UpdatePanel ID="UpdatePanelMateriel" UpdateMode="Always" runat="server">
                    <ContentTemplate>

                        <p style="width: 550px">
                            <asp:Label ID="Label21" runat="server" Class="champs" Text="Matériels affectées:" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 40px"></asp:Label>
                        </p>
                        <div style="position: absolute; height: 140px; left: 20px; top: 60px" class="DivGridMateriel">
                            <asp:GridView ID="GvMaterielAffectes" runat="server" AutoGenerateColumns="False" class="Grid" DataKeyNames="idArticleStock">
                                <Columns>
                                    <asp:BoundField HeaderText="idStock" DataField="idArticleStock" Visible="false" />
                                    <asp:BoundField HeaderText="Détail" DataField="image" HtmlEncode="false" />
                                    <asp:BoundField HeaderText="Type" DataField="libelleTypeMateriel" />
                                    <asp:BoundField HeaderText="Marque" DataField="libelleMarque" />
                                    <asp:BoundField HeaderText="Modèle" DataField="modele" />
                                    <asp:BoundField HeaderText="Type" DataField="typeLiaison" />
                                    <asp:BoundField HeaderText="Code référence" DataField="codeReference" />
                                    <asp:BoundField HeaderText="Date attribution" DataField="dateAttribution" />
                                    <asp:BoundField HeaderText="" DataField="lien" HtmlEncode="false" />
                                    <asp:ButtonField CausesValidation="True" CommandName="enlever" ButtonType="Image" ImageUrl="images/icones/ico_enlever.png" ControlStyle-CssClass="actionEnlever" HeaderText="Action" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div id="DivMaterielDisponible" style="display: block;" runat="server">
                            <p style="width: 550px">
                                <asp:Label ID="Label5" runat="server" Class="champs" Text="Matériels disponibles:" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 210px"></asp:Label>
                            </p>

                            <p style="width: 550px">
                                <asp:Label ID="Label22" class="champs" runat="server" Text="Type Matériel :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 240px"></asp:Label>
                                <asp:DropDownList ID="DdlTypeMateriel" runat="server" AutoPostBack="True" CausesValidation="true" Style="position: absolute; width: 240px; height: 21px; left: 110px; top: 240px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                            </p>
                            <p style="width: 550px">
                                <asp:Label ID="Label23" runat="server" class="champs" Text="Marque :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 260px"></asp:Label>
                                <asp:DropDownList ID="DdlMarque" runat="server" AutoPostBack="True" CausesValidation="true" Style="position: absolute; width: 240px; height: 21px; left: 110px; top: 260px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                            </p>
                            <p style="width: 550px">
                                <asp:Label ID="Label24" runat="server" Class="champs" Text="Modèle :" Style="position: absolute; height: 16px; width: 140px; left: 20px; top: 280px"></asp:Label>
                                <asp:DropDownList ID="DdlModele" runat="server" AutoPostBack="True" CausesValidation="true" Style="position: absolute; width: 240px; height: 21px; left: 110px; top: 280px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                            </p>
                            <p style="width: 550px">
                                <asp:Label ID="Label25" runat="server" Class="champs" Text="Code référence :" Style="position: absolute; height: 16px; width: 140px; left: 360px; top: 240px"></asp:Label>
                                <asp:DropDownList ID="DdlCodeReference" runat="server" AutoPostBack="True" Style="position: absolute; width: 200px; height: 21px; left: 460px; top: 240px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                            </p>
                            <p style="width: 550px">
                                <asp:Label ID="Label19" runat="server" Class="champs" Text="Code Emei :" Style="position: absolute; height: 16px; width: 140px; left: 360px; top: 260px"></asp:Label>
                                <asp:DropDownList ID="DdlCodeEmei" runat="server" AutoPostBack="True" Style="position: absolute; width: 200px; height: 21px; left: 460px; top: 260px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                            </p>
                            <p style="width: 550px">
                                <asp:Label ID="Label20" runat="server" Class="champs" Text="Numéro série :" Style="position: absolute; height: 16px; width: 140px; left: 360px; top: 280px"></asp:Label>
                                <asp:DropDownList ID="DdlNumSerie" runat="server" AutoPostBack="True" Style="position: absolute; width: 200px; height: 21px; left: 460px; top: 280px" Font-Size="12px" ForeColor="#206797"></asp:DropDownList>
                            </p>
                            <p style="width: 550px">
                                <asp:Button ID="BtnAjouterMateriel" class="champs" runat="server" Text="Ajouter" Style="position: absolute; left: 320px; top: 305px" />
                            </p>
                        </div>
                        <asp:HiddenField ID="TxtIdStock" runat="server" />

                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanelMateriel"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img src="images/Chargement.gif" alt="">
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </div>

        <div id="DivAction" runat="server" style="display: block; position: absolute; top: 512px; left: 12px; height: 60px; width: 680px;">
            <img src="images/GestionParc/cadre_action.png" style="z-index: 99; left: 0px; position: absolute; top: 0px; height: 60px; width: 680px; right: -180px;" />
            <asp:Label ID="lblBoiteAction" runat="server" Height="16px" Style="left: 20px; position: absolute; top: 8px; z-index: 102; right: 919px;" Width="250px">Actions : </asp:Label>

            <div id="DivActionInsert" style="display: none; position: absolute; z-index: 100; top: 30px; left: 0px" runat="server">
                <asp:Button ID="BtnInsertValider" class="champs" runat="server" Text="Valider" Style="position: absolute; z-index: 101; width: 80px; top: 3px; left: 245px;" />
                <asp:Button ID="BtnInsertAnnuler" class="champs" runat="server" Text="Annuler" Style="position: absolute; z-index: 101; width: 80px; top: 3px; left: 355px;" />
            </div>

            <div id="DivActionEdition" style="display: none; position: absolute; z-index: 100; top: 30px; left: 0px" runat="server">
                <asp:Button ID="BtnEditionValider" class="champs" runat="server" Text="Valider" Style="position: absolute; z-index: 101; width: 80px; top: 3px; left: 245px;" />
                <asp:Button ID="BtnEditionAnnuler" class="champs" runat="server" Text="Annuler" Style="position: absolute; z-index: 101; width: 80px; top: 3px; left: 355px;" />
            </div>

            <div id="DivActionDelete" style="display: none; position: absolute; z-index: 100; top: 30px; left: 0px" runat="server">
                <asp:Button ID="BtnDeleteValider" class="champs" runat="server" Text="Valider" Style="position: absolute; z-index: 101; width: 80px; top: 3px; left: 245px;" />
                <asp:Button ID="BtnDeleteAnnuler" Class="champs" runat="server" Text="Annuler" Style="position: absolute; z-index: 101; width: 80px; top: 3px; left: 355px;" />
            </div>
        </div>


        <div id="DivInformation" runat="server" style="position: absolute; top: 576px; left: 12px; height: 20px; width: 680px;">
            <p style="width: 550px">
                <asp:Label ID="LblInformation" Visible="false" runat="server" Text="" Style="position: absolute; left: 1px; top: 0px"></asp:Label>
            </p>
        </div>

    </form>
</body>
</html>
