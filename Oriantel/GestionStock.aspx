﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GestionStock.aspx.vb" Inherits="Oriantel.GestionStock" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html>

<%--<html xmlns="http://www.w3.org/1999/xhtml">--%>
<!--[if lt IE 7 ]> <html class="ie6" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64 128x128" href="../images/icones/favicon.ico" />

    <title>Oria Extranet</title>

    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css' />
    <link href="css/gestionParc.css" rel="stylesheet" type="text/css" media="screen" />

</head>
<body style="width: 525px; height: 400px">
    <form id="form1" runat="server">
        <asp:Panel ID="pnlDefaultButton" runat="server" DefaultButton="BtnEditionValider">
            <asp:HiddenField ID="idArticle" runat="server" />

            <div style="width: 525px; height: 20px; float: left">
                <h1 style="text-align: center; width: 111%;">
                    <span style="font-size: 14pt; text-transform: uppercase; position: relative;">
                        <asp:Image ID="ImageAction" Visible="false" runat="server" Style="height: 22px; width: 22px; position: absolute; top: 0Px; left: -27Px;" />
                        <asp:Label ID="LblTitre" runat="server" Text=""></asp:Label>
                    </span>
                    <span style="font-size: 7pt">&nbsp;</span><br>
                    <asp:Image ID="Image1" runat="server" Height="1px" ImageUrl="Images/GestionParc/hb.png" Width="320px" /></h1>

                <asp:Image ID="ImageTerminal" Visible="false" runat="server" Style="position: absolute; top: 5px; right: 20px; height: 100px" />

            </div>

            <div id="DivArticle" style="position: absolute; top: 60px; left: 5px; height: 150px; width: 280px;">
                <img src="images/GestionParc/cadre_info.png" style="z-index: 99; left: 5px; position: absolute; top: 60px; height: 150px; width: 280px; right: -104px;" />
                <asp:Label ID="lblBoiteArticle" runat="server" Height="16px" Style="left: 10px; position: absolute; top: 65px; z-index: 102; right: 919px;" Width="250px">Informations générales : </asp:Label>
                <p align="left">
                    <asp:Label ID="Label1" runat="server" Text="Type" Height="16px" Width="115px" Style="left: 10px; position: absolute; top: 100px; z-index: 102;"></asp:Label>
                    <asp:DropDownList ID="DdlType" runat="server" Class="champs" AutoPostBack="True" Width="170px" Style="left: 105px; height: 21px; position: absolute; top: 100px; z-index: 103;">
                    </asp:DropDownList>
                </p>
                <p align="left">
                    <asp:Label ID="Label2" runat="server" Text="Marque" Height="16px" Width="115px" Style="left: 10px; position: absolute; top: 120px; z-index: 102;"></asp:Label>
                    <asp:DropDownList ID="DdlMarque" runat="server" Class="champs" AutoPostBack="True" Width="170px" Style="left: 105px; height: 21px; position: absolute; top: 120px; z-index: 103;"></asp:DropDownList>
                </p>
                <p align="left">
                    <asp:Label ID="Label3" runat="server" Text="Modele" Height="16px" Width="115px" Style="left: 10px; position: absolute; top: 140px; z-index: 102;"></asp:Label>
                    <asp:DropDownList ID="DdlModele" runat="server" Class="champs" AutoPostBack="True" Width="170px" Style="left: 105px; height: 21px; position: absolute; top: 140px; z-index: 103;"></asp:DropDownList>
                </p>
                <p align="left">
                    <asp:Label ID="Label4" runat="server" Text="Prix" Height="16px" Width="115px" Style="left: 10px; position: absolute; top: 160px; z-index: 102;"></asp:Label>
                    <asp:DropDownList ID="DdlPrix" runat="server" Class="champs" AutoPostBack="True" Width="170px" Style="left: 105px; height: 21px; position: absolute; top: 160px; z-index: 103;"></asp:DropDownList>
                </p>
                <p align="left">
                    <asp:Label ID="Label16" runat="server" Text="Code Référence" Height="16px" Width="115px" Style="left: 10px; position: absolute; top: 180px; z-index: 102;"></asp:Label>
                    <asp:Label ID="LblCodeReference" runat="server" Class="champs" Text="a" Width="120px" Style="left: 105px; position: absolute; top: 180px; z-index: 103;"></asp:Label>
                </p>
            </div>



            <div id="DivCadreSerie" runat="server" style="display: none; position: absolute; top: 60px; left: 150px; height: 150px; width: 325px;">
                <img src="images/GestionParc/cadre_serie.png" style="z-index: 99; left: 150px; position: absolute; top: 60px; height: 150px; width: 280px; right: -180px;" />
                <asp:Label ID="lblBoiteSerie" runat="server" Height="16px" Style="left: 155px; position: absolute; top: 65px; z-index: 102; right: 919px;" Width="250px">Informations relatives à l'accessoire : </asp:Label>
            </div>

            <div id="DivSerie" runat="server" style="display: none; position: absolute; top: 60px; left: 150px; height: 150px; width: 325px;">
                <p align="left">
                    <asp:Label ID="Label9" runat="server" Text="Numéro série" Height="16px" Width="115px" Style="left: 158px; position: absolute; top: 100px; z-index: 102;"></asp:Label>
                    <asp:TextBox ID="TbNumeroSerie" runat="server" Class="champs" Width="160px" Style="left: 253px; height: 15px; position: absolute; top: 100px; z-index: 103;"></asp:TextBox>
                </p>
            </div>

            <div id="DivCarte" runat="server" style="display: none; position: absolute; top: 60px; left: 150px; height: 150px; width: 280px;">
                <img src="images/GestionParc/cadre_carte.png" style="z-index: 99; left: 150px; position: absolute; top: 60px; height: 150px; width: 280px; right: -104px;" />
                <asp:Label ID="lblBoiteCarte" runat="server" Height="16px" Style="left: 155px; position: absolute; top: 65px; z-index: 102; right: 919px;" Width="250px">Informations relatives à la carte : </asp:Label>
                <p>
                    <asp:Label ID="Label10" runat="server" Text="Code Pin" Height="16px" Width="115px" Style="left: 158px; position: absolute; top: 120px; z-index: 102;"></asp:Label>
                    <asp:TextBox ID="TbCodePin1" runat="server" Class="champs" Width="160px" Style="left: 253px; height: 15px; position: absolute; top: 120px; z-index: 103;"></asp:TextBox>
                </p>

                <p>
                    <asp:Label ID="Label12" runat="server" Text="Code Puk" Height="16px" Width="115px" Style="left: 158px; position: absolute; top: 140px; z-index: 102;"></asp:Label>
                    <asp:TextBox ID="TbCodePuk1" runat="server" Class="champs" Width="160px" Style="left: 253px; height: 15px; position: absolute; top: 140px; z-index: 103;"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="Label13" runat="server" Text="Code Puk 2" Height="16px" Width="115px" Style="left: 158px; position: absolute; top: 160px; z-index: 102;"></asp:Label>
                    <asp:TextBox ID="TbCodePuk2" runat="server" Class="champs" Width="160px" Style="left: 253px; height: 15px; height: 15px; position: absolute; top: 160px; z-index: 103;"></asp:TextBox>
                </p>
            </div>

            <div id="DivTerminal" runat="server" style="display: none; position: absolute; top: 60px; left: 150px; height: 120px; width: 325px;">
                <img src="images/GestionParc/cadre_terminal.png" style="z-index: 99; left: 150px; position: absolute; top: 60px; height: 150px; width: 280px; right: -180px;" />
                <asp:Label ID="lblBoiteTerminal" runat="server" Height="16px" Style="left: 155px; position: absolute; top: 65px; z-index: 102; right: 919px;" Width="250px">Informations relatives au terminal : </asp:Label>
                <p>
                    <asp:Label ID="Label5" runat="server" Text="Code Emei" Height="16px" Width="170px" Style="left: 158px; position: absolute; top: 100px; z-index: 102;"></asp:Label>
                    <asp:TextBox ID="TbCodeEmei" runat="server" Class="champs" Width="130px" Style="left: 283px; height: 15px; position: absolute; top: 100px; z-index: 103;"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="Label6" runat="server" Text="Code Desimlockage" Height="16px" Width="170px" Style="left: 158px; position: absolute; top: 120px; z-index: 102;"></asp:Label>
                    <asp:TextBox ID="TbCodeDesimlockage1" Class="champs" runat="server" Width="130px" Style="left: 283px; height: 15px; position: absolute; top: 120px; z-index: 103;"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="Label7" runat="server" Text="Etiquette Inventaire" Height="16px" Width="170px" Style="left: 158px; position: absolute; top: 140px; z-index: 102;"></asp:Label>
                    <asp:TextBox ID="TbEtiquetteInventaire" Class="champs" runat="server" Width="130px" Style="left: 283px; height: 15px; position: absolute; top: 140px; z-index: 103;"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="Label14" runat="server" Text="Code Verrouillage" Height="16px" Width="170px" Style="left: 158px; position: absolute; top: 160px; z-index: 102;"></asp:Label>
                    <asp:TextBox ID="TbCodeVerrouillage" Class="champs" runat="server" Width="130px" Style="left: 283px; height: 15px; position: absolute; top: 160px; z-index: 103;"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="Label8" runat="server" Text="Est Desimlocke" Height="16px" Width="170px" Style="left: 158px; position: absolute; top: 180px; z-index: 102;"></asp:Label>
                    <asp:CheckBox ID="CbEstDesimlocke" runat="server" Width="160px" Style="left: 253px; position: absolute; top: 180px; z-index: 103;" />
                </p>
                <p>
                    <asp:Label ID="Label15" runat="server" Text="Est Enrolé" Height="16px" Width="110px" Style="left: 330px; position: absolute; top: 180px; z-index: 102;"></asp:Label>
                    <asp:CheckBox ID="CbEstEnrole" runat="server" Width="10px" Style="left: 400px; position: absolute; top: 180px; z-index: 103;" />
                </p>
            </div>

            <div id="DivEtat" runat="server" style="display: block; position: absolute; top: 140px; left: 5px; height: 60px; width: 280px;">
                <img src="images/GestionParc/cadre_etat.png" style="z-index: 99; left: 5px; position: absolute; top: 140px; height: 60px; width: 280px; right: -180px;" />
                <asp:Label ID="lblBoiteEtat" runat="server" Height="16px" Style="left: 10px; position: absolute; top: 145px; z-index: 102; right: 919px;" Width="250px">Informations relatives à l'état : </asp:Label>
                <asp:DropDownList ID="DdlEtat" runat="server" Class="champs" Width="190px" Style="left: 10px; height: 21px; position: absolute; top: 175px; z-index: 103;"></asp:DropDownList>
            </div>

            <div id="DivDernierDetenteur" runat="server" style="display: block; position: absolute; top: 140px; left: 150px; height: 60px; width: 280px;">
                <img src="images/GestionParc/cadre_dernierdetenteur.png" style="z-index: 99; left: 150px; position: absolute; top: 140px; height: 60px; width: 280px; right: -180px;" />
                <asp:Label ID="lblBoiteDernierDetenteur" runat="server" Height="16px" Style="left: 155px; position: absolute; top: 145px; z-index: 102; right: 919px;" Width="250px">Dernier détenteur : </asp:Label>
                <p>
                    <asp:Label ID="LblDernierDetenteur" runat="server" Text="" Height="16px" Width="220px" Style="left: 158px; position: absolute; top: 175px; z-index: 103;"></asp:Label>
                    <img id="imgDetailDernierUtilisateur" alt="Afficher la ligne" title="Afficher la ligne" runat="server" visible="false" src="images/icones/ico_loupe.png" style="z-index: 99; cursor: pointer; width: 15px; height: 15px; left: 400px; position: absolute; top: 175px;" />
                </p>
            </div>

            <div id="DivAction" runat="server" style="display: block; position: absolute; top: 175px; left: 67px; height: 60px; width: 325px;">
                <img src="images/GestionParc/cadre_action.png" style="z-index: 99; left: 67px; position: absolute; top: 180px; height: 60px; width: 325px; right: -180px;" />
                <asp:Label ID="lblBoiteAction" runat="server" Height="16px" Style="left: 77px; position: absolute; top: 185px; z-index: 102; right: 919px;" Width="250px">Actions : </asp:Label>




                <div id="DivActionDelete" style="display: none;" runat="server">
                    <asp:Button ID="BtnDeleteValider" runat="server" Text="Valider" Style="left: 155px; position: absolute; top: 214px; z-index: 102;" />
                    <asp:Button ID="BtnDeleteAnnuler" runat="server" Text="Annuler" Style="left: 247px; position: absolute; top: 214px; z-index: 102;" />
                </div>
                <div id="DivActionInsert" style="display: none;" runat="server">
                    <asp:Button ID="BtnInsertValider" runat="server" Text="Valider" Style="left: 155px; position: absolute; top: 214px; z-index: 102;" />
                    <asp:Button ID="BtnInsertAnnuler" runat="server" Text="Annuler" Style="left: 247px; position: absolute; top: 214px; z-index: 102;" />
                </div>
                <div id="DivActionEdition" style="display: none;" runat="server">
                    <asp:Button ID="BtnEditionValider" runat="server" Text="Valider" Style="left: 155px; position: absolute; top: 214px; z-index: 102;" />
                    <asp:Button ID="BtnEditionAnnuler" runat="server" Text="Annuler" Style="left: 247px; position: absolute; top: 214px; z-index: 102;" />
                </div>

            </div>


            <div id="DivInformation" style="display: block;" runat="server">
                <div style="width: 302px">
                    <asp:Label ID="LblInformation" runat="server" Text="" Style="position: absolute; top: 425px; left: 20px;"></asp:Label>
                </div>

            </div>
        </asp:Panel>
    </form>
</body>
</html>
