﻿Imports System.Web.Security
Imports System.Data.SqlClient

Public Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Ecrasement de toute session existante
        Session.Clear()
        FormsAuthentication.SignOut()

        ' Déclaration de variable
        Dim password As String
        Dim login As String

        ' Récupération des paramétres GET pour un accès simplifié
        login = Request.Item("1")
        password = Request.Item("2")


        'If Not String.IsNullOrEmpty(Request.Item("base")) Then
        '    Session("base") = Request.Item("base")
        'End If

        'If Not String.IsNullOrEmpty(Request.Item("numReporting")) Then
        '    Session("numReporting") = Request.Item("numReporting")
        'End If

        ' Si les champs ne sont pas renseignés on les initialise à chaine vide
        If IsNothing(login) Then login = ""
        If IsNothing(password) Then password = ""

        ' Vérification du login et du password
        If Not login.Equals("") And Not password.Equals("") Then
            If verifierIdentite(login.Replace("'", "''"), password.Replace("'", "''")) = True Then
                ' Redirection vers la liste des modules disponibles si l'identification est correcte
                FormsAuthentication.RedirectFromLoginPage(CStr(Session.Item("idUtilisateur")) & "_" & CStr(Session.Item("idClient")) & "_" & CStr(Session.Item("idUtilisateurSecondaire")), False)
            Else

                ' Affichage de l'erreur
                STV_Erreur.Visible = True

            End If
        Else
            If Not login.Equals("") Then ES_Login.Text = login
            ' On met le focus sur le champ Login
            donnerFocus(ES_Login)

        End If
    End Sub

    '*********************************************************************************************************
    ' Vérification de l'identité d'un utilisateur qui s'identifie et insertion d' information dans les logs
    '*********************************************************************************************************
    Public Function verifierIdentite(ByVal pLogin As String, ByVal pPass As String) As Boolean

        ' Déclaration de variables
        Dim existe As Boolean = False
        Dim sBrowserName As String = ""
        Dim nBrowserVersion As String = ""
        Dim sResolution As String = ""
        Dim sIp As String
        Dim oModule As New Modul
        Dim oLogs As New Logs


        Try
            ' Si un des deux champs est vide alors on renvoie false
            If pLogin = Nothing OrElse pPass = Nothing OrElse Trim(pLogin) = "" OrElse Trim(pPass) = "" Then
                Return False
            End If

            ' Cryptage du mot de passe
            pPass = Toolbox.cryptageMD5(pPass)

            ' Création Session Utilisateur
            cDal.CreationSessionUtilisateur(existe, pLogin, pPass)

            ' *** Récupération des informations du navigateur *** '

            With Request.Browser
                sBrowserName = .Browser
                nBrowserVersion = .Version
                nBrowserVersion = nBrowserVersion.Split(CChar("."))(0)
            End With
            sIp = Request.ServerVariables("REMOTE_ADDR")
            sResolution = Me.idResolutionEcran.Value

            oModule.createModuleConnexion()
            If Not Session.Item("idUtilisateur") Is Nothing Then
                oLogs.insertionInformationsUtilisateurDansLog(CInt(Session.Item("idClient")), CInt(Session.Item("idUtilisateur")), oModule.nIdModule, sBrowserName, CUShort(nBrowserVersion), sResolution, sIp, CInt(Session.Item("idUtilisateurSecondaire")))
            End If
        Catch ex As Exception
            'Email.envoiEmail("Login Extranet", verifierIdentite, ex.Message)
            Throw ex
        Finally
            oModule = Nothing
        End Try

        Return existe

    End Function


    '***********************************************************************************************
    ' Méthode permettant de donner le focus à un champs de type WebControl
    '***********************************************************************************************
    Public Sub donnerFocus(ByVal champs As System.Web.UI.WebControls.WebControl)
        'Champ est le webcontrol sur lequel on veut mettre le focus
        Dim s As String
        s = "<script type= ""text/javascript"">document.getElementById('" + _
        champs.ClientID + "').focus()</script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "focus", s)
    End Sub


    '***********************************************************************************************
    ' Evenement lors de l'appui sur le bouton Valider : Vérification des identifiants
    '***********************************************************************************************
    Protected Sub BP_Valider_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BP_Valider.Click
        If verifierIdentite(ES_Login.Text.Replace("'", "''"), ES_Pass.Text.Replace("'", "''")) = True Then
            ' Redirection vers la liste des modules disponibles si l'identification est correcte
            FormsAuthentication.SetAuthCookie(CStr(Session.Item("idUtilisateur")) & "_" & CStr(Session.Item("idClient")) & "_" & CStr(Session.Item("idUtilisateurSecondaire")), True)
            FormsAuthentication.RedirectFromLoginPage(CStr(Session.Item("idUtilisateur")) & "_" & CStr(Session.Item("idClient")) & "_" & CStr(Session.Item("idUtilisateurSecondaire")), True)
        Else
            ' Affichage de l'erreur
            STV_Erreur.Visible = True
        End If
    End Sub

End Class