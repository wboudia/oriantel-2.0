﻿Imports System.IO

Public Class Detail_Facturation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If

        Dim oUtilisateur As New Utilisateur()
        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))

        Dim sIdFacture As String = ""
        If Request.Item("facture") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            sIdFacture = Request.Item("facture").ToString
        End If

        Dim oDetailFaturation As New DetailFacturation
        oDetailFaturation.ChargerDetailFactureParId(CInt(sIdFacture), oUtilisateur)

        If Request.Item("Redirection") = Nothing Then
            Me.boutonretour.Visible = False
        End If

        '---Gestion du titre
        Dim oSpanTitre As New HtmlGenericControl("SPAN")
        Select Case oDetailFaturation.sLibelleTypeFacture.ToUpper
            Case "AVOIR"
                oSpanTitre.InnerText = "Détail de l'avoir n° " + oDetailFaturation.sNumeroFacture
            Case "FACTURE"
                oSpanTitre.InnerText = "Détail de la facture n° " + oDetailFaturation.sNumeroFacture
            Case Else
                oSpanTitre.InnerText = "Détail de la facture n° " + oDetailFaturation.sNumeroFacture
        End Select
        Me.PlaceHolderTitre.Controls.Add(oSpanTitre)

        '---Gestion informations

        Dim sDevise As String = ""
        Select Case oUtilisateur.oClient.sMonnaieClient
            Case "euro"
                sDevise = " €"
            Case "dollar"
                sDevise = " $"
        End Select



        Me.STV_Operateur.Text = oDetailFaturation.sLibelleOperateur
        Me.STV_MontantHT.Text = Format(oDetailFaturation.nMontantHT, "# ### ##0.00") + sDevise
        Me.STV_MontantTTC.Text = Format(oDetailFaturation.nMontantTTC, "# ### ##0.00") + sDevise
        Me.STV_MontantPaye.Text = Format(oDetailFaturation.nMontantPayeHT, "# ### ##0.00") + sDevise
        Me.STV_Statut.Text = oDetailFaturation.sLibelleStatut
        Me.STV_DateFacture.Text = Format(oDetailFaturation.dDateFacture, "dddd dd MMM yyyy")

        If oDetailFaturation.sLibelleMandat <> "" Then
            Me.STV_Statut.Text += "   (" + oDetailFaturation.sLibelleMandat + ")"
        End If


        Me.ImgStatut.Attributes.Item("src") = oDetailFaturation.sLienImageStatut

        '---Gestion Factures Liees
        Dim oTableFacturesDocumentsLiees As New Table

        For Each oDetailFacturationLiee In oDetailFaturation.oListeFacturesLiees
            Dim oRow As New TableRow
            Dim oCelluleLien As New TableCell
            oCelluleLien.Text = oDetailFacturationLiee.sLibelleTypeFacture + " n° " + oDetailFacturationLiee.sNumeroFacture

            oRow.Cells.Add(oCelluleLien)


            Select Case oUtilisateur.oClient.sMonnaieClient
                Case "euro"
                    sDevise = " € HT"
                Case "dollar"
                    sDevise = " $"
            End Select


            Dim oCelluleMontant As New TableCell
            oCelluleMontant.Text = Format(oDetailFacturationLiee.nMontantHT, "# ### ##0.00") + sDevise
            oCelluleMontant.Attributes.Add("style", "padding-left: 10px;")
            oRow.Cells.Add(oCelluleMontant)

            Dim oCelluleImage As New TableCell
            Dim oImage As New Literal
            oImage.Text = "<IMG style=""width:15px;height:15px"" src='" + oDetailFacturationLiee.sLienImageStatut + "' title='" + oDetailFacturationLiee.sLibelleStatut + "'"
            oCelluleImage.Controls.Add(oImage)
            oCelluleImage.Attributes.Add("style", "padding-left: 10px;")
            oRow.Cells.Add(oCelluleImage)

            Dim oCelluleLoupe As New TableCell
            Dim oLiteral As New Literal
            oLiteral.Text = "<a href=""" + "Detail_Facturation.aspx?facture=" + CStr(oDetailFacturationLiee.nIdFacture) + "&Redirection=ok" + """><IMG style=""width:15px;height:15px"" src='images/icones/ico_loupe.png' title='Afficher' alt='Afficher' oncick="""" ></a>"
            oCelluleLoupe.Controls.Add(oLiteral)
            oCelluleLoupe.Attributes.Add("style", "padding-left: 10px;")
            oRow.Cells.Add(oCelluleLoupe)

            Dim oCelluleCommentaire As New TableCell
            oCelluleCommentaire.Text = oDetailFacturationLiee.sCommentaireLiaison
            oCelluleCommentaire.Attributes.Add("style", "padding-left: 10px;")
            oRow.Cells.Add(oCelluleCommentaire)

            oTableFacturesDocumentsLiees.Rows.Add(oRow)
        Next

        For Each oDetailDocumentLiee In oDetailFaturation.oListeDocuments
            Dim oRow As New TableRow
            Dim oCelluleLien As New TableCell
            oCelluleLien.Text = oDetailDocumentLiee.sTitreDocument

            oRow.Cells.Add(oCelluleLien)

            Dim oCelluleDate As New TableCell
            oCelluleDate.Text = Format(oDetailDocumentLiee.dDateDocument, "dd/MM/yyyy")
            oCelluleDate.Attributes.Add("style", "padding-left: 10px;")
            oRow.Cells.Add(oCelluleDate)

            Dim oCelluleImage As New TableCell
            Dim oImage As New Literal
            oImage.Text = " "
            oCelluleImage.Controls.Add(oImage)
            oCelluleImage.Attributes.Add("style", "padding-left: 10px;")
            oRow.Cells.Add(oCelluleImage)

            Dim oCelluleLoupe As New TableCell
            Dim oLiteral As New Literal
            Dim sRepertoireDocument As String = "documents/" + oUtilisateur.oClient.sNomClient + "/Factures Client/" + CStr(oDetailFaturation.nAnnee) + "/" + CStr(oDetailFaturation.nMois) + "/"
            oLiteral.Text = "<a href=""" + "PdfViewer.aspx?fichier=" + sRepertoireDocument + oDetailDocumentLiee.sNomFichier + """ target=""_blank""><IMG style=""width:15px;height:15px"" src='images/icones/ico_loupe.png' title='Afficher' alt='Afficher' oncick="""" ></a>"
            oCelluleLoupe.Controls.Add(oLiteral)
            oCelluleLoupe.Attributes.Add("style", "padding-left: 10px;")
            oRow.Cells.Add(oCelluleLoupe)

            Dim oCelluleCommentaire As New TableCell
            oCelluleCommentaire.Text = oDetailDocumentLiee.sCommentaireDocument
            oCelluleCommentaire.Attributes.Add("style", "padding-left: 10px;")
            oRow.Cells.Add(oCelluleCommentaire)

            oTableFacturesDocumentsLiees.Rows.Add(oRow)
        Next

        If oTableFacturesDocumentsLiees.Rows.Count > 0 Then
            PlaceHolderFacturesLiees.Controls.Add(oTableFacturesDocumentsLiees)
        Else
            Me.divFacturesLiees.Visible = False
        End If

        '---Gestion dates
        Dim oTableDatesFacture As New Table
        For Each oDetailFaturationDates In oDetailFaturation.oListeDatesFactures
            Dim oRow As New TableRow
            Dim oCelluleLibelleDate As New TableCell
            oCelluleLibelleDate.Text = oDetailFaturationDates.sLibelleDateType + " : "
            oRow.Cells.Add(oCelluleLibelleDate)

            Dim oCelluleDate As New TableCell
            oCelluleDate.Text = Format(oDetailFaturationDates.dDateFacture, "dddd dd MMM yyyy")
            oCelluleDate.Attributes.Add("style", "padding-left: 10px")
            oRow.Cells.Add(oCelluleDate)

            oTableDatesFacture.Rows.Add(oRow)
        Next
        If oTableDatesFacture.Rows.Count > 0 Then
            PlaceHolderFacturesDates.Controls.Add(oTableDatesFacture)
        End If

        '---Gestion Fichiers

        Dim sRepertoire As String = "documents/" + oUtilisateur.oClient.sNomClient + "/Factures Client/"
        Dim sNomFichier As String = ""
        If oUtilisateur.oClient.sNomClient.ToUpper <> "ORIA" Then
            sRepertoire += CStr(oDetailFaturation.nAnnee) + "/" + CStr(oDetailFaturation.nMois) + "/"
            sNomFichier = oDetailFaturation.sLibelleTypeFacture & "_" & oDetailFaturation.sLibelleCompteFacturant & "_" & oDetailFaturation.sNumeroFacture + ".pdf"
        Else
            sNomFichier = "Facture_Exemple.pdf"
        End If



        If Not File.Exists(Server.MapPath(sRepertoire + sNomFichier)) Then
            LienFacture.Visible = False
            lblFacturePDF.Visible = False
        Else
            LienFacture.Visible = True
            lblFacturePDF.Visible = True
        End If
        If Not File.Exists(Server.MapPath(sRepertoire + sNomFichier.Replace(".pdf", ".xls"))) Then
            BP_Annexe.Visible = False
            lblAnnexePapier.Visible = False
        Else
            BP_Annexe.Visible = True
            lblAnnexePapier.Visible = True
        End If




        LienFacture.Attributes.Item("href") = "PdfViewer.aspx?fichier=" + sRepertoire + sNomFichier

    End Sub

    Protected Sub BP_Annexe_Click(sender As Object, e As ImageClickEventArgs) Handles BP_Annexe.Click

        Dim oUtilisateur As New Utilisateur()
        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))

        Dim sIdFacture As String = ""
        If Request.Item("facture") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            sIdFacture = Request.Item("facture").ToString
        End If

        Dim oDetailFaturation As New DetailFacturation
        oDetailFaturation.ChargerDetailFactureParId(CInt(sIdFacture), oUtilisateur)

        Dim sRepertoire As String = "documents\" + oUtilisateur.oClient.sNomClient + "\Factures Client\"

        Dim sNomFichier As String = ""
        If oUtilisateur.oClient.sNomClient.ToUpper <> "ORIA" Then
            sRepertoire += CStr(oDetailFaturation.nAnnee) + "\" + CStr(oDetailFaturation.nMois) + "\"
            sNomFichier = oDetailFaturation.sLibelleTypeFacture & "_" & oDetailFaturation.sLibelleCompteFacturant & "_" & oDetailFaturation.sNumeroFacture + ".xls"
        Else
            sNomFichier = "Facture_Exemple.xls"
        End If


        ' Proposition du téléchargement du fichier
        Dim strFile As FileInfo = New FileInfo(Server.MapPath(sRepertoire & sNomFichier))
        If strFile.Exists = True Then
            Response.Clear()
            Response.AppendHeader("Content-Disposition", "attachment; filename=" & sNomFichier)
            Response.AppendHeader("Content-Length", strFile.Length.ToString())
            Response.WriteFile(strFile.FullName)
            Response.End()
        End If
    End Sub
End Class