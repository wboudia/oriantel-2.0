﻿Public Class PdfViewer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If

        Try
            If Not Request.Item("fichier") = Nothing Then
                Me.cheminFichier.Value = Server.UrlEncode(Request.Item("fichier"))
                Me.testReader.Value = "1"
                If Not Request.Item("hasReader") = Nothing Then
                    Me.testReader.Value = "0"

                    If CStr(Request.Item("hasReader")) = "true" Then
                        Dim oLiteral As New Literal
                        oLiteral.Text = "<embed id=""pdfembed"" src=""" & Request.Item("fichier").ToString & """ width=100% height=100 alt=""pdf"" pluginspage=""https://www.adobe.com/products/acrobat/readstep2.html"">"
                        Me.PlaceHolder_PdfViewer.Controls.Add(oLiteral)
                    ElseIf CStr(Request.Item("hasReader")) = "false" Then
                        Dim sNomFic As String = Server.UrlDecode(Request.Item("Fichier"))
                        Dim nPosLastOccur As Integer = sNomFic.LastIndexOf("/")
                        Dim sNomFichier As String = "Export.pdf"
                        If nPosLastOccur >= 0 Then
                            sNomFichier = sNomFic.Substring(nPosLastOccur + 1)
                        End If

                        Dim strFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath((sNomFic)))
                        If strFile.Exists Then
                            ForceDownload(strFile)
                        End If
                    End If
                End If
            Else
                Response.Write("<script>window.open('Default.aspx','_parent');<" & Chr(47) & "script>")
            End If
        Catch ex As Exception
            Response.Write("<script>window.open('Default.aspx','_parent');<" & Chr(47) & "script>")
        End Try

    End Sub

    Private Sub ForceDownload(ByVal virtualPath As System.IO.FileInfo)
        'Response.Clear()
        'Response.AddHeader("content-disposition", "attachment; filename=" & fileName)
        'Response.WriteFile(virtualPath)
        'Response.ContentType = ""
        'Response.End()

        Response.Clear()
        Response.AddHeader("Content-Disposition", "attachment; filename=" & Server.UrlPathEncode(virtualPath.Name))
        Response.AddHeader("Content-Length", virtualPath.Length.ToString())
        Response.WriteFile(virtualPath.FullName)
        Response.ContentType = "application/pdf"
        Response.End()
    End Sub

End Class