﻿Imports Subgurim.Controles
Imports System.Drawing

Public Class maps
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim latLng As GLatLng = New GLatLng(50, 10)
        GMap1.setCenter(latLng, 4)
        Dim icon As GIcon = New GIcon
        icon.markerIconOptions = New MarkerIconOptions(50, 50, Color.Blue)
        Dim marker As GMarker = New GMarker(latLng, icon)
        Dim window As GInfoWindow = New GInfoWindow(marker, "You can use the Map Icon Maker as any other marker")
        GMap1.Add(window)
        Dim icon2 As GIcon = New GIcon
        icon2.labeledMarkerIconOptions = New LabeledMarkerIconOptions(Color.Gold, Color.White, "A", Color.Green)
        Dim marker2 As GMarker = New GMarker((latLng + New GLatLng(3, 3)), icon2)
        Dim window2 As GInfoWindow = New GInfoWindow(marker2, "You can use the Map Icon Maker as any other marker")
        GMap1.Add(window2)
        Dim icon3 As GIcon = New GIcon
        icon3.flatIconOptions = New FlatIconOptions(25, 25, Color.Red, Color.Black, "B", Color.White, 15, FlatIconOptions.flatIconShapeEnum.roundedrect)
        Dim marker3 As GMarker = New GMarker((latLng + New GLatLng(-3, -3)), icon3)
        Dim window3 As GInfoWindow = New GInfoWindow(marker3, "You can use the Map Icon Maker as any other marker")
        GMap1.Add(window3)
    End Sub

End Class