﻿Public Class PostIt
    Inherits System.Web.UI.Page

    Private _oPostItLigne As New PostItLigne
    Public Property oPostItLigne() As PostItLigne
        Get
            Return _oPostItLigne
        End Get
        Set(ByVal value As PostItLigne)
            _oPostItLigne = value
        End Set
    End Property

    Private _oClient As New Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If

        oClient.chargerClientParId((CInt(User.Identity.Name.Split(CChar("_"))(1))))
        If (Request.Item("numLigne") = Nothing) Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            oPostItLigne.sNumLigne = Request.Item("numLigne")
            Me.lblNumLigne.Text = oPostItLigne.sNumLigne
        End If

        oPostItLigne.chargerPostItLigne(oClient)
        If (Not Page.IsPostBack) AndAlso (oPostItLigne.nIdPostIt > 0) Then
            idTextAreaInfoPostIt.Text = oPostItLigne.sInfoPostIt
        End If

    End Sub

    Protected Sub btnValider_Click(sender As Object, e As EventArgs) Handles btnValider.Click
        If Trim(Me.idTextAreaInfoPostIt.Text).Length > 1000 Then
            Me.idTextAreaInfoPostIt.Text = Left(Trim(Me.idTextAreaInfoPostIt.Text), 1000)
        End If
        oPostItLigne.sInfoPostIt = Trim(Me.idTextAreaInfoPostIt.Text)
        If oPostItLigne.nIdPostIt > 0 Then 'Modification
            oPostItLigne.updatePostItLigne(oClient)
        Else 'Ajout
            oPostItLigne.InsertionPostItLigne(oClient)
        End If
        ' ************************************************************************* '
        ' UPDATE DES LIGNES DE TABLERAPPORT POUR MISE A JOUR SUR LE 31.31 EN DIRECT '
        ' ************************************************************************* '
        Dim oListeTableRapport As New List(Of String)
        PostItLigne.recupererListeTablePostItSelonClient(oListeTableRapport, oClient)
        'oPostItLigne.updatePostItLigneSurTableRapportPourRepercussionDirecte(oListeTableRapport, oClient)

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub
End Class