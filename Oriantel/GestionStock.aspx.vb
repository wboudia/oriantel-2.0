﻿Public Class GestionStock
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If

        Dim nIdArticleStock As Integer
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim sTypeModif As String
        Dim sfiltreTypeMateriel As String

        If Request.Item("idArticleStock") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            nIdArticleStock = CInt(Request.Item("idArticleStock").ToString)
        End If

        If Request.Item("typeModif") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            sTypeModif = Request.Item("typeModif").ToString
        End If

        If Request.Item("filtreTypeMateriel") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            sfiltreTypeMateriel = Request.Item("filtreTypeMateriel").ToString
        End If

        Dim oArticles As New Articles
        oArticles.chargerArticles(nIdClient)

        ChargerImageAction(sTypeModif)

        Select Case sTypeModif
            Case "insert"
                Me.pnlDefaultButton.DefaultButton = "BtnInsertValider"
                If Page.IsPostBack = False Then
                    Me.LblTitre.Text = "Ajouter un produit"
                    ChargerListeMateriel(sfiltreTypeMateriel, oArticles)
                    ChargerListeMarque(oArticles)
                    ChargerListeModele(oArticles)
                    ChargerListePrix(oArticles)
                    ChargerImage(oArticles)
                    GenererCodeReference(nIdClient)
                    Me.DdlEtat.Items.Add("Non Attribué")
                    Me.DdlEtat.SelectedValue = "Non Attribué"
                    Me.DivDernierDetenteur.Style.Item("display") = "none"
                    GererAffichage()
                    Me.DivActionInsert.Style.Item("display") = "block"
                    Me.DivActionDelete.Style.Item("display") = "none"
                    Me.DivActionEdition.Style.Item("display") = "none"
                End If
            Case "edit"
                Me.pnlDefaultButton.DefaultButton = "BtnEditionValider"
                If Page.IsPostBack = False Then
                    Me.LblTitre.Text = "Modifier un produit"
                    Dim oStock As New Stock
                    oStock.chargerStockParId(nIdArticleStock)

                    ChargerListeMateriel("aucun", oArticles, oStock)
                    ChargerListeMarque(oArticles, oStock)
                    ChargerListeModele(oArticles, oStock)
                    ChargerListePrix(oArticles, oStock)
                    ChargerImage(oArticles, oStock)
                    Me.DdlType.Enabled = False
                    Me.LblCodeReference.Text = oStock.sCodeReference
                    Me.TbCodeEmei.Text = oStock.sCodeEmei
                    Me.TbCodeDesimlockage1.Text = oStock.sCodeDesimlockage1
                    Me.TbEtiquetteInventaire.Text = oStock.sEtiquetteInventaire
                    Me.CbEstDesimlocke.Checked = oStock.bEstDesimlocke
                    Me.CbEstEnrole.Checked = oStock.bEstEnrole
                    Me.TbNumeroSerie.Text = oStock.sNumSerie
                    Me.TbCodePin1.Text = oStock.sCodePin1
                    Me.TbCodePuk1.Text = oStock.sCodePuk1
                    Me.TbCodePuk2.Text = oStock.sCodePuk2
                    Me.TbCodeVerrouillage.Text = oStock.sCodeVerrouillage
                    If oStock.sEtat = "Attribué" Then
                        Me.DdlEtat.Items.Add("Attribué")
                        Me.DdlEtat.SelectedValue = oStock.sEtat
                    Else
                        Me.DdlEtat.Items.Add("Non Actif")
                        Me.DdlEtat.Items.Add("Non Attribué")
                        Me.DdlEtat.Items.Add("SAV")
                        If Me.DdlEtat.Items.Contains(New ListItem(oStock.sEtat)) Then
                            Me.DdlEtat.SelectedValue = oStock.sEtat
                        End If
                    End If
                    Me.LblDernierDetenteur.Text = oStock.sDernierDetenteur
                    GererAffichage()
                    Me.DivActionEdition.Style.Item("display") = "block"
                    Me.DivActionInsert.Style.Item("display") = "none"
                    Me.DivActionDelete.Style.Item("display") = "none"
                End If
            Case "visu"
                Me.pnlDefaultButton.DefaultButton = "BtnInsertAnnuler"
                If Page.IsPostBack = False Then
                    Me.LblTitre.Text = "Détail du produit"
                    Dim oStock As New Stock
                    oStock.chargerStockParId(nIdArticleStock)

                    ChargerListeMateriel("aucun", oArticles, oStock)
                    ChargerListeMarque(oArticles, oStock)
                    ChargerListeModele(oArticles, oStock)
                    ChargerListePrix(oArticles, oStock)
                    ChargerImage(oArticles, oStock)
                    Me.DdlType.Enabled = False
                    Me.LblCodeReference.Text = oStock.sCodeReference
                    Me.TbCodeEmei.Text = oStock.sCodeEmei
                    Me.TbCodeDesimlockage1.Text = oStock.sCodeDesimlockage1
                    Me.TbEtiquetteInventaire.Text = oStock.sEtiquetteInventaire
                    Me.CbEstDesimlocke.Checked = oStock.bEstDesimlocke
                    Me.CbEstEnrole.Checked = oStock.bEstEnrole
                    Me.TbNumeroSerie.Text = oStock.sNumSerie
                    Me.TbCodePin1.Text = oStock.sCodePin1
                    Me.TbCodePuk1.Text = oStock.sCodePuk1
                    Me.TbCodePuk2.Text = oStock.sCodePuk2
                    Me.TbCodeVerrouillage.Text = oStock.sCodeVerrouillage
                    If oStock.sEtat = "Attribué" Then
                        Me.DdlEtat.Items.Add("Attribué")
                        Me.DdlEtat.SelectedValue = oStock.sEtat
                    Else
                        Me.DdlEtat.Items.Add("Non Actif")
                        Me.DdlEtat.Items.Add("Non Attribué")
                        Me.DdlEtat.Items.Add("SAV")
                        If Me.DdlEtat.Items.Contains(New ListItem(oStock.sEtat)) Then
                            Me.DdlEtat.SelectedValue = oStock.sEtat
                        End If
                    End If

                    Me.LblDernierDetenteur.Text = oStock.sDernierDetenteur
                    If oStock.sEtat = "Attribué" Then
                        Me.imgDetailDernierUtilisateur.Visible = True
                        Dim oLigne As New Ligne
                        oLigne = oStock.RecupererLigneAffecteStock()
                        If Not oLigne Is Nothing Then
                            If CInt(User.Identity.Name.Split(CChar("_"))(1)) = 118 Then
                                Me.imgDetailDernierUtilisateur.Attributes.Add("onclick", "parent.EditerLigneParcRetourMaterielCNES('" & oLigne.nIdLigne.ToString & "','visucomplet','" & oStock.nIdArticleStock.ToString & "','visu','" & oStock.oArticle.oTypeMateriel.sLibelle & "')")
                            Else
                                Me.imgDetailDernierUtilisateur.Attributes.Add("onclick", "parent.EditerLigneParcRetourMateriel('" & oLigne.nIdLigne.ToString & "','visucomplet','" & oStock.nIdArticleStock.ToString & "','visu','" & oStock.oArticle.oTypeMateriel.sLibelle & "')")
                            End If
                        End If
                    End If


                    If oStock.sEtat = "Attribué" Then
                        Dim oLigne As New Ligne
                        oLigne = oStock.RecupererLigneAffecteStock()
                        If Not oLigne Is Nothing Then
                            If CInt(User.Identity.Name.Split(CChar("_"))(1)) = 118 Then
                                Me.imgDetailDernierUtilisateur.Attributes.Add("onclick", "parent.EditerLigneParcRetourMaterielCNES('" & oLigne.nIdLigne.ToString & "','edit','" & oStock.nIdArticleStock.ToString & "','delete','" & oStock.oArticle.oTypeMateriel.sLibelle & "')")
                            Else
                                Me.imgDetailDernierUtilisateur.Attributes.Add("onclick", "parent.EditerLigneParcRetourMateriel('" & oLigne.nIdLigne.ToString & "','edit','" & oStock.nIdArticleStock.ToString & "','delete','" & oStock.oArticle.oTypeMateriel.sLibelle & "')")
                            End If
                        End If
                    End If


                    GererAffichage()
                    VerrouillerControles()
                    Me.DivAction.Visible = False
                End If
            Case "delete"
                Me.pnlDefaultButton.DefaultButton = "BtnDeleteValider"
                If Page.IsPostBack = False Then

                    Me.LblTitre.Text = "Supprimer un produit"
                    Dim oStock As New Stock
                    oStock.chargerStockParId(nIdArticleStock)

                    ChargerListeMateriel("aucun", oArticles, oStock)
                    ChargerListeMarque(oArticles, oStock)
                    ChargerListeModele(oArticles, oStock)
                    ChargerListePrix(oArticles, oStock)
                    ChargerImage(oArticles, oStock)
                    Me.LblCodeReference.Text = oStock.sCodeReference
                    Me.TbCodeEmei.Text = oStock.sCodeEmei
                    Me.TbCodeDesimlockage1.Text = oStock.sCodeDesimlockage1
                    Me.TbEtiquetteInventaire.Text = oStock.sEtiquetteInventaire
                    Me.CbEstDesimlocke.Checked = oStock.bEstDesimlocke
                    Me.CbEstEnrole.Checked = oStock.bEstEnrole
                    Me.TbNumeroSerie.Text = oStock.sNumSerie
                    Me.TbCodePin1.Text = oStock.sCodePin1
                    Me.TbCodePuk1.Text = oStock.sCodePuk1
                    Me.TbCodePuk2.Text = oStock.sCodePuk2
                    Me.TbCodeVerrouillage.Text = oStock.sCodeVerrouillage
                    If oStock.sEtat = "Attribué" Then
                        Me.DdlEtat.Items.Add("Attribué")
                        Me.DdlEtat.SelectedValue = oStock.sEtat
                    Else
                        Me.DdlEtat.Items.Add("Non Actif")
                        Me.DdlEtat.Items.Add("Non Attribué")
                        Me.DdlEtat.Items.Add("SAV")
                        If Me.DdlEtat.Items.Contains(New ListItem(oStock.sEtat)) Then
                            Me.DdlEtat.SelectedValue = oStock.sEtat
                        End If
                    End If

                    Me.LblDernierDetenteur.Text = oStock.sDernierDetenteur
                    Me.imgDetailDernierUtilisateur.Visible = True
                    If oStock.sEtat = "Attribué" Then
                        Dim oLigne As New Ligne
                        oLigne = oStock.RecupererLigneAffecteStock()
                        If Not oLigne Is Nothing Then
                            If CInt(User.Identity.Name.Split(CChar("_"))(1)) = 118 Then
                                Me.imgDetailDernierUtilisateur.Attributes.Add("onclick", "parent.EditerLigneParcRetourMaterielCNES('" & oLigne.nIdLigne.ToString & "','edit','" & oStock.nIdArticleStock.ToString & "','delete','" & oStock.oArticle.oTypeMateriel.sLibelle & "')")
                            Else
                                Me.imgDetailDernierUtilisateur.Attributes.Add("onclick", "parent.EditerLigneParcRetourMateriel('" & oLigne.nIdLigne.ToString & "','edit','" & oStock.nIdArticleStock.ToString & "','delete','" & oStock.oArticle.oTypeMateriel.sLibelle & "')")
                            End If
                        End If
                    End If

                    GererAffichage()
                    VerrouillerControles()
                    Me.DivActionDelete.Style.Item("display") = "block"
                    Me.DivActionEdition.Style.Item("display") = "none"
                    Me.DivActionInsert.Style.Item("display") = "none"

                End If
        End Select
    End Sub

    Private Sub ChargerImageAction(ByVal sTypeAction As String)
        Dim strFile As System.IO.FileInfo
        Dim sNomFichier As String = ""

        Select Case sTypeAction
            Case "insert"
                sNomFichier = "ico_Ajouter.png"
            Case "edit"
                sNomFichier = "ico_Modifier.png"
            Case "visu"
                sNomFichier = "ico_Loupe.png"
            Case "delete"
                sNomFichier = "ico_supprimer.png"
        End Select

        strFile = New System.IO.FileInfo(Server.MapPath("images/icones/" & sNomFichier))
        If strFile.Exists Then
            Me.ImageAction.ImageUrl = "images/icones/" & sNomFichier
        End If
        ImageAction.Visible = True
    End Sub

    Private Sub DdlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlType.SelectedIndexChanged
        Dim oArticles As New Articles
        Dim oStock As New Stock
        ChargerArticlesEtStock(oArticles, oStock)
        ChargerListeMarque(oArticles, oStock)
        GererAffichage()
    End Sub
    Private Sub DdlMarque_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlMarque.SelectedIndexChanged
        Dim oArticles As New Articles
        Dim oStock As New Stock
        ChargerArticlesEtStock(oArticles, oStock)
        ChargerListeModele(oArticles, oStock)
        ChargerImage(oArticles, oStock)
    End Sub
    Private Sub DdlModele_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlModele.SelectedIndexChanged
        Dim oArticles As New Articles
        Dim oStock As New Stock
        ChargerArticlesEtStock(oArticles, oStock)
        ChargerListePrix(oArticles, oStock)
        ChargerImage(oArticles, oStock)
    End Sub

    Private Sub ChargerListeMateriel(ByVal sFiltreType As String, ByVal oArticles As Articles, Optional oStock As Stock = Nothing)
        Dim sListeTypeMateriel = From oArticle In oArticles Group oArticle By oArticle.oTypeMateriel.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle

        For Each sTexte In sListeTypeMateriel
            If sFiltreType.ToUpper = "AUCUN" Or sTexte.ToUpper.StartsWith(sFiltreType.ToUpper) Then
                Me.DdlType.Items.Add(sTexte)
            End If
        Next

        If Not oStock Is Nothing AndAlso Not oStock.oArticle Is Nothing Then
            If Me.DdlType.Items.Contains(New ListItem(oStock.oArticle.oTypeMateriel.sLibelle)) Then
                Me.DdlType.SelectedValue = oStock.oArticle.oTypeMateriel.sLibelle
            End If
        End If
        EcrireIdArticle(oArticles)
    End Sub

    Private Sub ChargerListeMarque(ByVal oArticles As Articles, Optional oStock As Stock = Nothing)
        Me.DdlMarque.Items.Clear()
        Dim sListeMarque = From oArticle In oArticles Where oArticle.oTypeMateriel.sLibelle = Me.DdlType.SelectedValue Group oArticle By oArticle.oMarque.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        For Each sTexte In sListeMarque
            Me.DdlMarque.Items.Add(sTexte)
        Next

        If Not oStock Is Nothing AndAlso Not oStock.oArticle Is Nothing Then
            If Me.DdlMarque.Items.Contains(New ListItem(oStock.oArticle.oMarque.sLibelle)) Then
                Me.DdlMarque.SelectedValue = oStock.oArticle.oMarque.sLibelle
            End If
        End If
        EcrireIdArticle(oArticles)
    End Sub
    Private Sub ChargerListeModele(ByVal oArticles As Articles, Optional oStock As Stock = Nothing)
        Me.DdlModele.Items.Clear()
        Dim sListeModele = From oArticle In oArticles Where oArticle.oTypeMateriel.sLibelle = Me.DdlType.SelectedValue And oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue Group oArticle By oArticle.sModele Into TupleGroup = Group Select sModele Order By sModele
        For Each sTexte In sListeModele
            Me.DdlModele.Items.Add(sTexte)
        Next

        If Not oStock Is Nothing AndAlso Not oStock.oArticle Is Nothing Then
            If Me.DdlModele.Items.Contains(New ListItem(oStock.oArticle.sModele)) Then
                Me.DdlModele.SelectedValue = oStock.oArticle.sModele
            End If
        End If
        EcrireIdArticle(oArticles)
    End Sub
    Private Sub ChargerListePrix(ByVal oArticles As Articles, Optional oStock As Stock = Nothing)
        Me.DdlPrix.Items.Clear()
        Dim sListePrix1 = From oArticle In oArticles Where oArticle.oTypeMateriel.sLibelle = Me.DdlType.SelectedValue And oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oArticle.sModele = Me.DdlModele.SelectedValue Group oArticle By oArticle.nPrix1 Into TupleGroup = Group Select nPrix1 Order By nPrix1
        Dim sListePrix2 = From oArticle In oArticles Where oArticle.oTypeMateriel.sLibelle = Me.DdlType.SelectedValue And oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oArticle.sModele = Me.DdlModele.SelectedValue Group oArticle By oArticle.nPrix2 Into TupleGroup = Group Select nPrix2 Order By nPrix2

        Me.DdlPrix.Items.Add(CStr(sListePrix1(0)))
        Me.DdlPrix.Items.Add(CStr(sListePrix2(0)))

        If Not oStock Is Nothing AndAlso Not oStock.oArticle Is Nothing Then
            If Me.DdlPrix.Items.Contains(New ListItem(oStock.nPrix.ToString)) Then
                Me.DdlPrix.SelectedValue = oStock.nPrix.ToString
            End If
        End If
        EcrireIdArticle(oArticles)
    End Sub

    Private Sub ChargerImage(ByVal oArticles As Articles, Optional oStock As Stock = Nothing)

        Dim sModeleTrie = From oArticle In oArticles Where oArticle.oTypeMateriel.sLibelle = Me.DdlType.SelectedValue And oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oArticle.sModele = Me.DdlModele.SelectedValue Group oArticle By oArticle.sModele Into TupleGroup = Group Select sModele Order By sModele
        Dim sMarqueTrie = From oArticle In oArticles Where oArticle.oTypeMateriel.sLibelle = Me.DdlType.SelectedValue And oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oArticle.sModele = Me.DdlModele.SelectedValue Group oArticle By oArticle.oMarque.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle

        Dim sNomFichier As String = Nothing
        If Me.DdlType.SelectedValue.ToString.ToUpper.StartsWith("TERMI") Then
            sNomFichier = sMarqueTrie(0).Replace(" ", "").ToUpper & "_" & sModeleTrie(0).Replace(" ", "").ToUpper & ".png"
            ImageTerminal.Visible = True
        ElseIf Me.DdlType.SelectedValue.ToString.ToUpper = "CARTE" Then
            sNomFichier = "carte.png"
            ImageTerminal.Visible = True
        ElseIf Me.DdlType.SelectedValue.ToString.ToUpper = "ACCESSOIRES" Then
            sNomFichier = "accessoire.png"
        End If

        Dim strFile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("documents/Terminaux/" & sNomFichier))
        If strFile.Exists Then
            Me.ImageTerminal.ImageUrl = "documents/Terminaux/" & sNomFichier
        Else
            Me.ImageTerminal.ImageUrl = "documents/Terminaux/Default.png"
        End If



        'ImageTerminal.Visible = False

    End Sub

    Private Sub ChargerArticlesEtStock(ByRef oArticles As Articles, ByRef oStock As Stock)
        Dim nIdArticleStock As Integer
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        If Request.Item("idArticleStock") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            nIdArticleStock = CInt(Request.Item("idArticleStock").ToString)
        End If
        oArticles.chargerArticles(nIdClient)
        oStock.chargerStockParId(nIdArticleStock)
    End Sub

    Private Sub EcrireIdArticle(ByRef oArticles As Articles)
        If Me.DdlType.Items.Count > 0 And Me.DdlMarque.Items.Count > 0 And Me.DdlModele.Items.Count > 0 Then
            Dim sListeidArticle = From oArticle In oArticles Where oArticle.oTypeMateriel.sLibelle = Me.DdlType.SelectedValue And oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oArticle.sModele = Me.DdlModele.SelectedValue Select oArticle.nIdArticle
            If sListeidArticle.Count > 0 Then
                Me.idArticle.Value = sListeidArticle(0).ToString
            End If
        End If
    End Sub


    Private Sub BtnEditionValider_Click(sender As Object, e As EventArgs) Handles BtnEditionValider.Click
        Dim oStock As New Stock
        Dim oNouveauStock As New Stock
        Dim oNouveauArticle As New Article
        Dim nIdArticleStock As Integer
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))

        If Request.Item("idArticleStock") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            nIdArticleStock = CInt(Request.Item("idArticleStock").ToString)
        End If

        oStock.chargerStockParId(nIdArticleStock)
        oNouveauArticle.chargerArticleParId(CInt(Me.idArticle.Value))

        oNouveauStock.oArticle = oNouveauArticle
        oNouveauStock.bEstDesimlocke = Me.CbEstDesimlocke.Checked
        oNouveauStock.dDateRentree = oStock.dDateRentree
        oNouveauStock.dDateSortie = oStock.dDateSortie
        oNouveauStock.nIdArticleStock = oStock.nIdArticleStock
        If Me.DdlPrix.SelectedValue <> "" Then
            oNouveauStock.nPrix = CDbl(Me.DdlPrix.SelectedValue)
        End If
        oNouveauStock.sCodeDesimlockage1 = Me.TbCodeDesimlockage1.Text
        oNouveauStock.sEtiquetteInventaire = Me.TbEtiquetteInventaire.Text
        oNouveauStock.sCodeEmei = Me.TbCodeEmei.Text
        oNouveauStock.sCodePin1 = Me.TbCodePin1.Text
        oNouveauStock.sCodePuk1 = Me.TbCodePuk1.Text
        oNouveauStock.sCodePuk2 = Me.TbCodePuk2.Text
        oNouveauStock.sCodeReference = Me.LblCodeReference.Text
        oNouveauStock.sCodeVerrouillage = Me.TbCodeVerrouillage.Text
        oNouveauStock.bEstEnrole = Me.CbEstEnrole.Checked
        oNouveauStock.sDernierDetenteur = oStock.sDernierDetenteur
        oNouveauStock.sEtat = Me.DdlEtat.SelectedValue
        If Me.DdlEtat.SelectedValue = "Non Actif" Then
            oNouveauStock.dDateSortie = Now
        End If
        oNouveauStock.sNumSerie = Me.TbNumeroSerie.Text

        oNouveauStock.mettreAJourStock()

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)

    End Sub

    Private Sub BtnEditionAnnuler_Click(sender As Object, e As EventArgs) Handles BtnEditionAnnuler.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub

    Private Sub BtnDeleteValider_Click(sender As Object, e As EventArgs) Handles BtnDeleteValider.Click
        Dim oStock As New Stock
        Dim nIdArticleStock As Integer
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))

        If Request.Item("idArticleStock") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            nIdArticleStock = CInt(Request.Item("idArticleStock").ToString)
        End If

        oStock.chargerStockParId(nIdArticleStock)

        If oStock.verifierPresenceTableLiaison = True Then
            Me.DivInformation.Style.Item("display") = "block"
            Me.LblInformation.Text = "Vous ne pouvez pas supprimer ce produit car il est associé à un utilisateur."
        Else
            oStock.supprimerStock()
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
        End If



    End Sub

    Private Sub BtnDeleteAnnuler_Click(sender As Object, e As EventArgs) Handles BtnDeleteAnnuler.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub

    Private Sub GererAffichage()
        If Me.DdlType.SelectedValue.ToUpper.StartsWith("TERMI") Then
            Me.DivTerminal.Style.Item("display") = "block"
        Else
            Me.DivTerminal.Style.Item("display") = "none"
            Me.TbCodeEmei.Text = ""
            Me.TbCodeDesimlockage1.Text = ""
            Me.TbEtiquetteInventaire.Text = ""
            Me.CbEstDesimlocke.Checked = False
        End If

        If Me.DdlType.SelectedValue.ToUpper.StartsWith("CARTE") Or Me.DdlType.SelectedValue.ToUpper.StartsWith("ACCESS") Then
            Me.DivSerie.Style.Item("display") = "block"
            Me.DivCadreSerie.Style.Item("display") = "block"
        Else
            Me.DivSerie.Style.Item("display") = "none"
            Me.DivCadreSerie.Style.Item("display") = "none"
            Me.TbNumeroSerie.Text = ""
        End If

        If Me.DdlType.SelectedValue.ToUpper.StartsWith("CARTE") Then
            Me.DivCarte.Style.Item("display") = "block"
            Me.DivCadreSerie.Style.Item("display") = "none"
            Me.DivSerie.Style.Item("display") = "block"
        Else
            Me.DivCarte.Style.Item("display") = "none"
            Me.TbCodePin1.Text = ""
            Me.TbCodePuk1.Text = ""
            Me.TbCodePuk2.Text = ""
        End If
    End Sub

    Private Sub VerrouillerControles()
        Me.DdlType.Enabled = False
        Me.DdlMarque.Enabled = False
        Me.DdlModele.Enabled = False
        Me.DdlPrix.Enabled = False
        Me.TbCodeEmei.Enabled = False
        Me.TbCodeDesimlockage1.Enabled = False
        Me.TbEtiquetteInventaire.Enabled = False
        Me.CbEstDesimlocke.Enabled = False
        Me.TbNumeroSerie.Enabled = False
        Me.TbCodePin1.Enabled = False
        Me.TbCodePuk1.Enabled = False
        Me.TbCodePuk2.Enabled = False
        Me.DdlEtat.Enabled = False
        Me.CbEstEnrole.Enabled = False
        Me.TbCodeVerrouillage.Enabled = False
    End Sub

    Private Sub BtnInsertAnnuler_Click(sender As Object, e As EventArgs) Handles BtnInsertAnnuler.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub

    Private Sub BtnInsertValider_Click(sender As Object, e As EventArgs) Handles BtnInsertValider.Click

        Dim oNouveauStock As New Stock
        Dim oNouveauArticle As New Article

        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))

        oNouveauArticle.chargerArticleParId(CInt(Me.idArticle.Value))
        oNouveauStock.oArticle = oNouveauArticle
        oNouveauStock.bEstDesimlocke = Me.CbEstDesimlocke.Checked
        oNouveauStock.dDateRentree = Now
        If Me.DdlPrix.SelectedValue <> "" Then
            oNouveauStock.nPrix = CDbl(Me.DdlPrix.SelectedValue)
        End If
        oNouveauStock.sCodeDesimlockage1 = Me.TbCodeDesimlockage1.Text
        oNouveauStock.sEtiquetteInventaire = Me.TbEtiquetteInventaire.Text
        oNouveauStock.sCodeEmei = Me.TbCodeEmei.Text
        oNouveauStock.sCodePin1 = Me.TbCodePin1.Text
        oNouveauStock.sCodePuk1 = Me.TbCodePuk1.Text
        oNouveauStock.sCodePuk2 = Me.TbCodePuk2.Text
        oNouveauStock.sCodeReference = Me.LblCodeReference.Text
        oNouveauStock.sEtat = Me.DdlEtat.SelectedValue
        oNouveauStock.sNumSerie = Me.TbNumeroSerie.Text
        oNouveauStock.sCodeVerrouillage = Me.TbCodeVerrouillage.Text
        oNouveauStock.bEstEnrole = Me.CbEstEnrole.Checked
        oNouveauStock.nIdClient = nIdClient
        oNouveauStock.insererStock()

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub

    Private Sub GenererCodeReference(ByVal nIdClient As Integer)
        Dim sCodeReference As String
        Dim nCodeReference As Long

        If Me.DdlType.SelectedValue.ToUpper.StartsWith("TERM") Then
            sCodeReference = "POR"
        Else
            sCodeReference = "ACC"
        End If

        nCodeReference = cDal.RecupererDernierCodeReference(sCodeReference, nIdClient)

        For i = Len(CStr(nCodeReference)) To 3
            sCodeReference += "0"
        Next
        sCodeReference += CStr(nCodeReference)

        Me.LblCodeReference.Text = sCodeReference
    End Sub

End Class