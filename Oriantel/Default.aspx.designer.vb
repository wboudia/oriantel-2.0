﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class _Default

    '''<summary>
    '''Contrôle PlaceHolderModules.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderModules As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''Contrôle logoClientSommaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents logoClientSommaire As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''Contrôle PlaceHolderInfosUtilisateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderInfosUtilisateur As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''Contrôle PlaceHolderDocumentsUtilisateur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderDocumentsUtilisateur As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''Contrôle lnkClose.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents lnkClose As Global.System.Web.UI.WebControls.LinkButton
End Class
