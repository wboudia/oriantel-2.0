﻿Imports Microsoft.Office.Interop
Imports System.IO

Public Class GestionParc
    Inherits System.Web.UI.Page

    Private Property oLigne() As Ligne
        Get
            Return CType(Session("oLigne"), Ligne)
        End Get
        Set(ByVal Value As Ligne)
            Session("oLigne") = Value
        End Set
    End Property

    Private Property sTypeModif() As String
        Get
            Return CType(Session("sTypeModif"), String)
        End Get
        Set(ByVal Value As String)
            Session("sTypeModif") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If

        Dim nIdLigne As Integer = 0
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))

        If Request.Item("idLigne") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            nIdLigne = CInt(Request.Item("idLigne").ToString)
        End If

        If Request.Item("typeModif") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            sTypeModif = Request.Item("typeModif").ToString
        End If

        ChargerImageAction(sTypeModif)

        Dim oFonctions As New Fonctionss
        Dim oCompteFacturants As New CompteFacturants
        Dim oDiscriminations As New Discriminations
        oFonctions.chargerFonctions(nIdClient)
        oCompteFacturants.chargerCompteFacturants(nIdClient)
        oDiscriminations.chargerDiscriminations(nIdClient)

        If Not Page.IsPostBack Then

            oLigne = New Ligne
            If oLigne.oOptionsUtilisateur Is Nothing Then
                oLigne.oOptionsUtilisateur = New OptionsUtilisateur
            End If

            If oLigne.oStocks Is Nothing Then
                oLigne.oStocks = New Stocks
            End If

            If nIdLigne <> 0 Then
                oLigne.chargerLigneParId(nIdLigne)
            End If

            Dim oHierarchies As New Hierarchies
            Dim oForfaits As New Forfaits

            Dim oOptionsUtilisateur As New OptionsUtilisateur
            Dim oStocks As New Stocks

            oHierarchies.chargerHierarchies(nIdClient)
            oForfaits.chargerForfaits(nIdClient)

            oOptionsUtilisateur.chargerOptionsClient(nIdClient)
            oStocks.chargerStocksLibres(nIdClient)
            oStocks = FiltrerStocksAttribues(oStocks)

            ChargerListeFonction(oFonctions, nIdClient)
            ChargerListeCompteFacturant(oCompteFacturants, nIdClient)

            ChargerListeHierarchie1(oHierarchies, nIdClient)
            ChargerListeHierarchie2(oHierarchies, nIdClient)
            ChargerListeHierarchie3(oHierarchies, nIdClient)
            ChargerListeHierarchie4(oHierarchies, nIdClient)
            ChargerListeHierarchie5(oHierarchies, nIdClient)
            ChargerListeHierarchie6(oHierarchies, nIdClient)

            ChargerListeOperateurs(oForfaits, nIdClient)
            ChargerListeTypeForfait(oForfaits, nIdClient)
            ChargerListeForfait(oForfaits, nIdClient)

            ChargerListeDiscrimination(oDiscriminations, nIdClient)



            ChargerListeTypeMateriel(oStocks)
            ChargerListeMarque(oStocks)
            ChargerListeModele(oStocks)
            ChargerListeCodeReference(oStocks)
            ChargerListeCodeEmei(oStocks)
            ChargerListeNumSerie(oStocks)
            ChargerIdStock(oStocks)


            If Not oLigne.oOptionsUtilisateur Is Nothing Then chargerOptionsAffectees()
            chargerOptionsLibres(oOptionsUtilisateur)

            If Not oLigne.oStocks Is Nothing Then chargerStocksAffectes()

            Select Case sTypeModif
                Case "insert"
                    Me.LblTitre.Text = "Ajouter une ligne"
                    Me.DivActionInsert.Style.Item("display") = "block"
                    Me.TxtDateActivation.Text = (Now.AddDays(2)).ToString("dd/MM/yyyy")
                    Me.TxtDateActivation.Enabled = True
                    Me.TxtNumeroGSM.Text = "-"
                    Me.LblDateActivation.Text = "Date prévisionelle :"
                    Dim oStocksCarte = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle.ToUpper = "CARTE"
                    If oStocksCarte.Count > 0 Then
                        Me.DdlTypeMateriel.SelectedValue = oStocksCarte(0).oArticle.oTypeMateriel.sLibelle
                        ChargerListeMarque(oStocks)
                        ChargerListeModele(oStocks)
                        ChargerListeCodeReference(oStocks)
                        ChargerListeCodeEmei(oStocks)
                        ChargerListeNumSerie(oStocks)
                        ChargerIdStock(oStocks)
                    End If

                Case "edit"
                    Me.LblTitre.Text = "Modifier une ligne"
                    Me.TxtNom.Text = oLigne.sNom
                    Me.TxtPrenom.Text = oLigne.sPrenom
                    Me.TxtNumeroGSM.Text = oLigne.sNumeroGSM
                    Me.TxtNumeroData.Text = oLigne.sNumeroData
                    If Not oLigne.dDateActivation = Nothing Then Me.TxtDateActivation.Text = oLigne.dDateActivation.ToString("dd/MM/yyyy")
                    Me.DdlFonction.SelectedValue = oLigne.oFonction.nIdFonction.ToString
                    Me.DdlCompteFacturant.SelectedValue = oLigne.oCompteFacturant.nIdCompteFacturant.ToString
                    Me.DdlHierarchie1.SelectedValue = oLigne.oHierarchie.sHierarchie1
                    ChargerListeHierarchie2(oHierarchies, nIdClient)
                    Me.DdlHierarchie2.SelectedValue = oLigne.oHierarchie.sHierarchie2
                    ChargerListeHierarchie3(oHierarchies, nIdClient)
                    Me.DdlHierarchie3.SelectedValue = oLigne.oHierarchie.sHierarchie3
                    ChargerListeHierarchie4(oHierarchies, nIdClient)
                    Me.DdlHierarchie4.SelectedValue = oLigne.oHierarchie.sHierarchie4
                    ChargerListeHierarchie5(oHierarchies, nIdClient)
                    Me.DdlHierarchie5.SelectedValue = oLigne.oHierarchie.sHierarchie5
                    ChargerListeHierarchie6(oHierarchies, nIdClient)
                    Me.DdlHierarchie6.SelectedValue = oLigne.oHierarchie.sHierarchie6
                    Me.DdlOperateur.SelectedValue = oLigne.oForfait.oOperateur.sLibelle
                    ChargerListeTypeForfait(oForfaits, nIdClient)
                    Me.DdlTypeForfait.SelectedValue = oLigne.oForfait.oTypeForfait.sLibelle
                    ChargerListeForfait(oForfaits, nIdClient)
                    Me.DdlForfait.SelectedValue = oLigne.oForfait.sLibelle
                    Me.TxtPrixAbonnement.Text = oLigne.oForfait.nPrixAbonnement.ToString
                    Me.DdlDiscrimination.SelectedValue = oLigne.oDiscrimination.nIdDiscrimination.ToString
                    Me.DivActionEdition.Style.Item("display") = "block"
                Case "delete"
                    Me.LblTitre.Text = "Supprimer une ligne"
                    Me.TxtNom.Text = oLigne.sNom
                    Me.TxtPrenom.Text = oLigne.sPrenom
                    Me.TxtNumeroGSM.Text = oLigne.sNumeroGSM
                    Me.TxtNumeroData.Text = oLigne.sNumeroData
                    If Not oLigne.dDateActivation = Nothing Then Me.TxtDateActivation.Text = oLigne.dDateActivation.ToString("dd/MM/yyyy")
                    Me.DdlFonction.SelectedValue = oLigne.oFonction.nIdFonction.ToString
                    Me.DdlCompteFacturant.SelectedValue = oLigne.oCompteFacturant.nIdCompteFacturant.ToString
                    Me.DdlHierarchie1.SelectedValue = oLigne.oHierarchie.sHierarchie1
                    ChargerListeHierarchie2(oHierarchies, nIdClient)
                    Me.DdlHierarchie2.SelectedValue = oLigne.oHierarchie.sHierarchie2
                    ChargerListeHierarchie3(oHierarchies, nIdClient)
                    Me.DdlHierarchie3.SelectedValue = oLigne.oHierarchie.sHierarchie3
                    ChargerListeHierarchie4(oHierarchies, nIdClient)
                    Me.DdlHierarchie4.SelectedValue = oLigne.oHierarchie.sHierarchie4
                    ChargerListeHierarchie5(oHierarchies, nIdClient)
                    Me.DdlHierarchie5.SelectedValue = oLigne.oHierarchie.sHierarchie5
                    ChargerListeHierarchie6(oHierarchies, nIdClient)
                    Me.DdlHierarchie6.SelectedValue = oLigne.oHierarchie.sHierarchie6
                    Me.DdlOperateur.SelectedValue = oLigne.oForfait.oOperateur.sLibelle
                    ChargerListeTypeForfait(oForfaits, nIdClient)
                    Me.DdlTypeForfait.SelectedValue = oLigne.oForfait.oTypeForfait.sLibelle
                    ChargerListeForfait(oForfaits, nIdClient)
                    Me.DdlForfait.SelectedValue = oLigne.oForfait.sLibelle
                    Me.TxtPrixAbonnement.Text = oLigne.oForfait.nPrixAbonnement.ToString
                    Me.DdlDiscrimination.SelectedValue = oLigne.oDiscrimination.nIdDiscrimination.ToString
                    Me.BpBordereauMateriel.Visible = False
                    VerrouillerControles()
                    Me.DivActionDelete.Style.Item("display") = "block"
                Case "visu"
                    Me.LblTitre.Text = "Détail de la ligne"
                    Me.LblDateDesactivation.Visible = True
                    Me.TxtDateDesactivation.Visible = True
                    Me.tabs3.Style.Item("display") = "none"
                    Me.LiMateriel.Style.Item("display") = "none"
                    Me.TxtNom.Text = oLigne.sNom
                    Me.TxtPrenom.Text = oLigne.sPrenom
                    Me.TxtNumeroGSM.Text = oLigne.sNumeroGSM
                    Me.TxtNumeroData.Text = oLigne.sNumeroData
                    If Not oLigne.dDateActivation = Nothing Then Me.TxtDateActivation.Text = oLigne.dDateActivation.ToString("dd/MM/yyyy")
                    If Not oLigne.dDateDesactivation = Nothing Then Me.TxtDateDesactivation.Text = oLigne.dDateDesactivation.ToString("dd/MM/yyyy")
                    Me.DdlFonction.SelectedValue = oLigne.oFonction.nIdFonction.ToString
                    Me.DdlCompteFacturant.SelectedValue = oLigne.oCompteFacturant.nIdCompteFacturant.ToString
                    Me.DdlHierarchie1.SelectedValue = oLigne.oHierarchie.sHierarchie1
                    ChargerListeHierarchie2(oHierarchies, nIdClient)
                    Me.DdlHierarchie2.SelectedValue = oLigne.oHierarchie.sHierarchie2
                    ChargerListeHierarchie3(oHierarchies, nIdClient)
                    Me.DdlHierarchie3.SelectedValue = oLigne.oHierarchie.sHierarchie3
                    ChargerListeHierarchie4(oHierarchies, nIdClient)
                    Me.DdlHierarchie4.SelectedValue = oLigne.oHierarchie.sHierarchie4
                    ChargerListeHierarchie5(oHierarchies, nIdClient)
                    Me.DdlHierarchie5.SelectedValue = oLigne.oHierarchie.sHierarchie5
                    ChargerListeHierarchie6(oHierarchies, nIdClient)
                    Me.DdlHierarchie6.SelectedValue = oLigne.oHierarchie.sHierarchie6
                    Me.DdlOperateur.SelectedValue = oLigne.oForfait.oOperateur.sLibelle
                    ChargerListeTypeForfait(oForfaits, nIdClient)
                    Me.DdlTypeForfait.SelectedValue = oLigne.oForfait.oTypeForfait.sLibelle
                    ChargerListeForfait(oForfaits, nIdClient)
                    Me.DdlForfait.SelectedValue = oLigne.oForfait.sLibelle
                    Me.TxtPrixAbonnement.Text = oLigne.oForfait.nPrixAbonnement.ToString
                    Me.DdlDiscrimination.SelectedValue = oLigne.oDiscrimination.nIdDiscrimination.ToString
                    Me.DivAction.Visible = False
                    Me.BpBordereauMateriel.Visible = False
                    VerrouillerControles()
                Case "visucomplet"
                    Me.LblTitre.Text = "Détail de la ligne"
                    Me.TxtNom.Text = oLigne.sNom
                    Me.TxtPrenom.Text = oLigne.sPrenom
                    Me.TxtNumeroGSM.Text = oLigne.sNumeroGSM
                    Me.TxtNumeroData.Text = oLigne.sNumeroData
                    If Not oLigne.dDateActivation = Nothing Then Me.TxtDateActivation.Text = oLigne.dDateActivation.ToString("dd/MM/yyyy")
                    If Not oLigne.dDateDesactivation = Nothing Then Me.TxtDateDesactivation.Text = oLigne.dDateDesactivation.ToString("dd/MM/yyyy")
                    Me.DdlFonction.SelectedValue = oLigne.oFonction.nIdFonction.ToString
                    Me.DdlCompteFacturant.SelectedValue = oLigne.oCompteFacturant.nIdCompteFacturant.ToString
                    Me.DdlHierarchie1.SelectedValue = oLigne.oHierarchie.sHierarchie1
                    ChargerListeHierarchie2(oHierarchies, nIdClient)
                    Me.DdlHierarchie2.SelectedValue = oLigne.oHierarchie.sHierarchie2
                    ChargerListeHierarchie3(oHierarchies, nIdClient)
                    Me.DdlHierarchie3.SelectedValue = oLigne.oHierarchie.sHierarchie3
                    ChargerListeHierarchie4(oHierarchies, nIdClient)
                    Me.DdlHierarchie4.SelectedValue = oLigne.oHierarchie.sHierarchie4
                    ChargerListeHierarchie5(oHierarchies, nIdClient)
                    Me.DdlHierarchie5.SelectedValue = oLigne.oHierarchie.sHierarchie5
                    ChargerListeHierarchie6(oHierarchies, nIdClient)
                    Me.DdlHierarchie6.SelectedValue = oLigne.oHierarchie.sHierarchie6
                    Me.DdlOperateur.SelectedValue = oLigne.oForfait.oOperateur.sLibelle
                    ChargerListeTypeForfait(oForfaits, nIdClient)
                    Me.DdlTypeForfait.SelectedValue = oLigne.oForfait.oTypeForfait.sLibelle
                    ChargerListeForfait(oForfaits, nIdClient)
                    Me.DdlForfait.SelectedValue = oLigne.oForfait.sLibelle
                    Me.TxtPrixAbonnement.Text = oLigne.oForfait.nPrixAbonnement.ToString
                    Me.DdlDiscrimination.SelectedValue = oLigne.oDiscrimination.nIdDiscrimination.ToString
                    Me.DivAction.Visible = False
                    VerrouillerControles()
            End Select
            If sTypeModif = "visu" Or sTypeModif = "delete" Or sTypeModif = "visucomplet" Then
                Me.GvOptionsDisponibles.Visible = False
                Me.LblOptionsDisponibles.Visible = False
            End If
        End If
    End Sub


    Private Sub ChargerImageAction(ByVal sTypeAction As String)
        Dim strFile As System.IO.FileInfo
        Dim sNomFichier As String = ""

        Select Case sTypeAction
            Case "insert"
                sNomFichier = "ico_Ajouter.png"
            Case "edit"
                sNomFichier = "ico_Modifier.png"
            Case "visu", "visucomplet"
                sNomFichier = "ico_Loupe.png"
            Case "delete"
                sNomFichier = "ico_supprimer.png"
        End Select

        strFile = New System.IO.FileInfo(Server.MapPath("images/icones/" & sNomFichier))
        If strFile.Exists Then
            Me.ImageAction.ImageUrl = "images/icones/" & sNomFichier
        End If
        ImageAction.Visible = True
    End Sub


    Private Sub ChargerListeFonction(ByVal oFonctions As Fonctionss, ByVal nIdClient As Integer)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idFonction")
        oDataTable.Columns.Add("libelle")
        For Each oFonction In oFonctions
            oDataTable.Rows.Add(oFonction.nIdFonction, oFonction.sLibelle)
        Next
        Me.DdlFonction.DataSource = oDataTable
        Me.DdlFonction.DataValueField = "idFonction"
        Me.DdlFonction.DataTextField = "libelle"
        Me.DdlFonction.DataBind()

        Dim oControle As DropDownList = Me.DdlFonction
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If
    End Sub
    Private Sub ChargerListeCompteFacturant(ByVal oCompteFacturants As CompteFacturants, ByVal nIdClient As Integer)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idCompteFacturant")
        oDataTable.Columns.Add("libelle")
        For Each oCompteFacturant In oCompteFacturants
            oDataTable.Rows.Add(oCompteFacturant.nIdCompteFacturant, oCompteFacturant.sLibelle)
        Next
        Me.DdlCompteFacturant.DataSource = oDataTable
        Me.DdlCompteFacturant.DataValueField = "idCompteFacturant"
        Me.DdlCompteFacturant.DataTextField = "libelle"
        Me.DdlCompteFacturant.DataBind()

        Dim oControle As DropDownList = Me.DdlCompteFacturant
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If
    End Sub
    Private Sub ChargerListeHierarchie1(ByVal oHierarchies As Hierarchies, ByVal nIdClient As Integer)
        Dim sListeHierarchie1 = From oHierarchie In oHierarchies Group oHierarchie By oHierarchie.sHierarchie1 Into TupleGroup = Group Select sHierarchie1 Order By sHierarchie1
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie1
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlHierarchie1.DataSource = oDataTable
        Me.DdlHierarchie1.DataValueField = "libelle"
        Me.DdlHierarchie1.DataTextField = "libelle"
        Me.DdlHierarchie1.DataBind()

        Dim oControle As DropDownList = Me.DdlHierarchie1
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub

    Private Sub ChargerListeHierarchie2(ByVal oHierarchies As Hierarchies, ByVal nIdClient As Integer)
        Dim sListeHierarchie2 = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = Me.DdlHierarchie1.Text Group oHierarchie By oHierarchie.sHierarchie2 Into TupleGroup = Group Select sHierarchie2 Order By sHierarchie2
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie2
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlHierarchie2.DataSource = oDataTable
        Me.DdlHierarchie2.DataValueField = "libelle"
        Me.DdlHierarchie2.DataTextField = "libelle"
        Me.DdlHierarchie2.DataBind()

        Dim oControle As DropDownList = Me.DdlHierarchie2
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeHierarchie3(ByVal oHierarchies As Hierarchies, ByVal nIdClient As Integer)
        Dim sListeHierarchie3 = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = Me.DdlHierarchie1.Text And oHierarchie.sHierarchie2 = Me.DdlHierarchie2.Text Group oHierarchie By oHierarchie.sHierarchie3 Into TupleGroup = Group Select sHierarchie3 Order By sHierarchie3
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie3
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlHierarchie3.DataSource = oDataTable
        Me.DdlHierarchie3.DataValueField = "libelle"
        Me.DdlHierarchie3.DataTextField = "libelle"
        Me.DdlHierarchie3.DataBind()

        Dim oControle As DropDownList = Me.DdlHierarchie3
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeHierarchie4(ByVal oHierarchies As Hierarchies, ByVal nIdClient As Integer)
        Dim sListeHierarchie4 = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = Me.DdlHierarchie1.Text And oHierarchie.sHierarchie2 = Me.DdlHierarchie2.Text And oHierarchie.sHierarchie3 = Me.DdlHierarchie3.Text Group oHierarchie By oHierarchie.sHierarchie4 Into TupleGroup = Group Select sHierarchie4 Order By sHierarchie4
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie4
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlHierarchie4.DataSource = oDataTable
        Me.DdlHierarchie4.DataValueField = "libelle"
        Me.DdlHierarchie4.DataTextField = "libelle"
        Me.DdlHierarchie4.DataBind()

        Dim oControle As DropDownList = Me.DdlHierarchie4
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeHierarchie5(ByVal oHierarchies As Hierarchies, ByVal nIdClient As Integer)
        Dim sListeHierarchie5 = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = Me.DdlHierarchie1.Text And oHierarchie.sHierarchie2 = Me.DdlHierarchie2.Text And oHierarchie.sHierarchie3 = Me.DdlHierarchie3.Text And oHierarchie.sHierarchie4 = Me.DdlHierarchie4.Text Group oHierarchie By oHierarchie.sHierarchie5 Into TupleGroup = Group Select sHierarchie5 Order By sHierarchie5
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie5
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlHierarchie5.DataSource = oDataTable
        Me.DdlHierarchie5.DataValueField = "libelle"
        Me.DdlHierarchie5.DataTextField = "libelle"
        Me.DdlHierarchie5.DataBind()

        Dim oControle As DropDownList = Me.DdlHierarchie5
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeHierarchie6(ByVal oHierarchies As Hierarchies, ByVal nIdClient As Integer)
        Dim sListeHierarchie6 = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = Me.DdlHierarchie1.Text And oHierarchie.sHierarchie2 = Me.DdlHierarchie2.Text And oHierarchie.sHierarchie3 = Me.DdlHierarchie3.Text And oHierarchie.sHierarchie4 = Me.DdlHierarchie4.Text And oHierarchie.sHierarchie5 = Me.DdlHierarchie5.Text Group oHierarchie By oHierarchie.sHierarchie6 Into TupleGroup = Group Select sHierarchie6 Order By sHierarchie6
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie6
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlHierarchie6.DataSource = oDataTable
        Me.DdlHierarchie6.DataValueField = "libelle"
        Me.DdlHierarchie6.DataTextField = "libelle"
        Me.DdlHierarchie6.DataBind()

        Dim oControle As DropDownList = Me.DdlHierarchie6
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If


    End Sub
    Private Sub ChargerListeOperateurs(ByVal oForfaits As Forfaits, ByVal nIdClient As Integer)
        Dim sListeOperateur = From oForfait In oForfaits Group oForfait By oForfait.oOperateur.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeOperateur
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlOperateur.DataSource = oDataTable
        Me.DdlOperateur.DataValueField = "libelle"
        Me.DdlOperateur.DataTextField = "libelle"
        Me.DdlOperateur.DataBind()

        Dim oControle As DropDownList = Me.DdlOperateur
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        Else
            Dim sListeOperateurOrange = From oForfait In oForfaits Where oForfait.oOperateur.sLibelle.ToUpper = "ORANGE"
            If sListeOperateurOrange.Count > 0 Then
                oControle.SelectedValue = "Orange"
            End If
        End If

    End Sub
    Private Sub ChargerListeTypeForfait(ByVal oForfaits As Forfaits, ByVal nIdClient As Integer)
        Dim sListeTypeForfait = From oForfait In oForfaits Where oForfait.oOperateur.sLibelle = Me.DdlOperateur.SelectedValue Group oForfait By oForfait.oTypeForfait.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeTypeForfait
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlTypeForfait.DataSource = oDataTable
        Me.DdlTypeForfait.DataValueField = "libelle"
        Me.DdlTypeForfait.DataTextField = "libelle"
        Me.DdlTypeForfait.DataBind()

        Dim oControle As DropDownList = Me.DdlTypeForfait
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        Else
            If Me.DdlOperateur.SelectedValue.ToUpper = "ORANGE" Then
                Dim sListeOperateurOrangeTypeVoix = From oForfait In oForfaits Where oForfait.oOperateur.sLibelle.ToUpper = "ORANGE" And oForfait.oTypeForfait.sLibelle.ToUpper = "VOIX"
                If sListeOperateurOrangeTypeVoix.Count > 0 Then
                    oControle.SelectedValue = "Voix"
                End If
            End If
            End If


    End Sub
    Private Sub ChargerListeForfait(ByVal oForfaits As Forfaits, ByVal nIdClient As Integer)
        Dim sListeForfait = From oForfait In oForfaits Where oForfait.oOperateur.sLibelle = Me.DdlOperateur.SelectedValue And oForfait.oTypeForfait.sLibelle = Me.DdlTypeForfait.SelectedValue Group oForfait By oForfait.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeForfait
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlForfait.DataSource = oDataTable
        Me.DdlForfait.DataValueField = "libelle"
        Me.DdlForfait.DataTextField = "libelle"
        Me.DdlForfait.DataBind()

        Dim oControle As DropDownList = Me.DdlForfait
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub

    Private Sub ChargerPrixAbonnement(ByVal oForfaits As Forfaits, ByVal nIdClient As Integer)
        Dim sListePrix = From oForfait In oForfaits Where oForfait.oOperateur.sLibelle = Me.DdlOperateur.SelectedValue And oForfait.oTypeForfait.sLibelle = Me.DdlTypeForfait.SelectedValue And oForfait.sLibelle = Me.DdlForfait.SelectedValue Group oForfait By oForfait.nPrixAbonnement Into TupleGroup = Group Select nPrixAbonnement Order By nPrixAbonnement
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        Me.TxtPrixAbonnement.Text = sListePrix(0).ToString
    End Sub
    Private Sub ChargerListeDiscrimination(ByVal oDiscriminations As Discriminations, ByVal nIdClient As Integer)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idDiscrimination")
        oDataTable.Columns.Add("libelle")
        For Each oDiscrimination In oDiscriminations
            oDataTable.Rows.Add(oDiscrimination.nIdDiscrimination, oDiscrimination.sLibelle)
        Next
        Me.DdlDiscrimination.DataSource = oDataTable
        Me.DdlDiscrimination.DataValueField = "idDiscrimination"
        Me.DdlDiscrimination.DataTextField = "libelle"
        Me.DdlDiscrimination.DataBind()

        Dim oControle As DropDownList = Me.DdlDiscrimination
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub

    Private Sub DdlHierarchie1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlHierarchie1.SelectedIndexChanged
        Dim oHierarchies As New Hierarchies
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oHierarchies.chargerHierarchies(nIdClient)
        ChargerListeHierarchie2(oHierarchies, nIdClient)
        ChargerListeHierarchie3(oHierarchies, nIdClient)
        ChargerListeHierarchie4(oHierarchies, nIdClient)
        ChargerListeHierarchie5(oHierarchies, nIdClient)
        ChargerListeHierarchie6(oHierarchies, nIdClient)
    End Sub
    Private Sub DdlHierarchie2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlHierarchie2.SelectedIndexChanged
        Dim oHierarchies As New Hierarchies
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oHierarchies.chargerHierarchies(nIdClient)
        ChargerListeHierarchie3(oHierarchies, nIdClient)
        ChargerListeHierarchie4(oHierarchies, nIdClient)
        ChargerListeHierarchie5(oHierarchies, nIdClient)
        ChargerListeHierarchie6(oHierarchies, nIdClient)
    End Sub
    Private Sub DdlHierarchie3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlHierarchie3.SelectedIndexChanged
        Dim oHierarchies As New Hierarchies
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oHierarchies.chargerHierarchies(nIdClient)
        ChargerListeHierarchie4(oHierarchies, nIdClient)
        ChargerListeHierarchie5(oHierarchies, nIdClient)
        ChargerListeHierarchie6(oHierarchies, nIdClient)
    End Sub
    Private Sub DdlHierarchie4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlHierarchie4.SelectedIndexChanged
        Dim oHierarchies As New Hierarchies
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oHierarchies.chargerHierarchies(nIdClient)
        ChargerListeHierarchie5(oHierarchies, nIdClient)
        ChargerListeHierarchie6(oHierarchies, nIdClient)
    End Sub
    Private Sub DdlHierarchie5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlHierarchie5.SelectedIndexChanged
        Dim oHierarchies As New Hierarchies
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oHierarchies.chargerHierarchies(nIdClient)
        ChargerListeHierarchie6(oHierarchies, nIdClient)
    End Sub
    Private Sub DdlOperateur_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlOperateur.SelectedIndexChanged
        Dim oForfaits As New Forfaits
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oForfaits.chargerForfaits(nIdClient)
        ChargerListeTypeForfait(oForfaits, nIdClient)
        ChargerListeForfait(oForfaits, nIdClient)
        ChargerPrixAbonnement(oForfaits, nIdClient)
    End Sub

    Private Sub DdlTypeForfait_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlTypeForfait.SelectedIndexChanged
        Dim oForfaits As New Forfaits
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oForfaits.chargerForfaits(nIdClient)
        ChargerListeForfait(oForfaits, nIdClient)
        ChargerPrixAbonnement(oForfaits, nIdClient)
    End Sub
    Private Sub DdlForfait_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlForfait.SelectedIndexChanged
        Dim oForfaits As New Forfaits
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oForfaits.chargerForfaits(nIdClient)
        ChargerPrixAbonnement(oForfaits, nIdClient)
    End Sub

    Private Sub VerrouillerControles()
        Me.TxtNom.Enabled = False
        Me.TxtPrenom.Enabled = False
        Me.TxtNumeroGSM.Enabled = False
        Me.TxtNumeroData.Enabled = False
        Me.TxtDateActivation.Enabled = False
        Me.DdlFonction.Enabled = False
        Me.DdlCompteFacturant.Enabled = False
        Me.DdlHierarchie1.Enabled = False
        Me.DdlHierarchie2.Enabled = False
        Me.DdlHierarchie3.Enabled = False
        Me.DdlHierarchie4.Enabled = False
        Me.DdlHierarchie5.Enabled = False
        Me.DdlHierarchie6.Enabled = False
        Me.DdlOperateur.Enabled = False
        Me.DdlTypeForfait.Enabled = False
        Me.DdlForfait.Enabled = False
        Me.DdlDiscrimination.Enabled = False
        Me.DdlTypeMateriel.Enabled = False
        Me.DdlMarque.Enabled = False
        Me.DdlModele.Enabled = False
        Me.DdlCodeReference.Enabled = False
        Me.BtnAjouterMateriel.Enabled = False
        Me.imgParamFonction.visible = False
        Me.imgParamCompteFacturant.visible = False
        Me.imgParamDiscrimination.Visible = False
        Me.DivMaterielDisponible.Style.Item("display") = "none"

    End Sub

    Private Function RecupererIdForfait() As Integer
        Dim oForfaits As New Forfaits
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim Resultat As Integer = 0
        oForfaits.chargerForfaits(nIdClient)

        Dim oListeFiltree = From oForfait In oForfaits Where oForfait.oOperateur.sLibelle = Me.DdlOperateur.SelectedValue And oForfait.oTypeForfait.sLibelle = Me.DdlTypeForfait.SelectedValue And oForfait.sLibelle = Me.DdlForfait.SelectedValue

        For Each oForfait In oListeFiltree
            Resultat = oForfait.nIdForfait
        Next
        Return Resultat
    End Function

    Private Function RecupererIdHierarchie() As Integer
        Dim oHierarchies As New Hierarchies
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim Resultat As Integer = 0
        oHierarchies.chargerHierarchies(nIdClient)
        Dim oListeFiltree = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = Me.DdlHierarchie1.Text And oHierarchie.sHierarchie2 = Me.DdlHierarchie2.Text And oHierarchie.sHierarchie3 = Me.DdlHierarchie3.Text And oHierarchie.sHierarchie4 = Me.DdlHierarchie4.Text And oHierarchie.sHierarchie5 = Me.DdlHierarchie5.Text And oHierarchie.sHierarchie6 = Me.DdlHierarchie6.Text
        For Each oHierarchie In oListeFiltree
            Resultat = oHierarchie.nIdHierarchie
        Next
        Return Resultat
    End Function

    Private Sub chargerOptionsAffectees()
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idOption")
        oDataTable.Columns.Add("categorie")
        oDataTable.Columns.Add("libelle")
        For Each oOptionUtilisateur In oLigne.oOptionsUtilisateur
            oDataTable.Rows.Add(oOptionUtilisateur.nIdOption, oOptionUtilisateur.oCategorieOptionUtilisateur.sLibelle, oOptionUtilisateur.sLibelle)
        Next
        Me.GvOptionsAffectees.DataSource = oDataTable
        Me.GvOptionsAffectees.DataBind()
        If sTypeModif = "visu" Or sTypeModif = "visucomplet" Or sTypeModif = "delete" Then
            Me.GvOptionsAffectees.Columns(Me.GvOptionsAffectees.Columns.Count - 1).Visible = False
        End If
    End Sub

    Private Sub chargerOptionsLibres(ByVal oOptionsUtilisateur As OptionsUtilisateur)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idOption")
        oDataTable.Columns.Add("categorie")
        oDataTable.Columns.Add("libelle")
        For Each oOptionUtilisateur In oOptionsUtilisateur
            If Not oLigne.oOptionsUtilisateur Is Nothing Then
                Dim oListeFiltree = From oOptionUtilisateurTri In oLigne.oOptionsUtilisateur Where oOptionUtilisateur.nIdOption = oOptionUtilisateurTri.nIdOption
                If oListeFiltree.Count() = 0 Then
                    oDataTable.Rows.Add(oOptionUtilisateur.nIdOption, oOptionUtilisateur.oCategorieOptionUtilisateur.sLibelle, oOptionUtilisateur.sLibelle)
                End If
            Else
                oDataTable.Rows.Add(oOptionUtilisateur.nIdOption, oOptionUtilisateur.oCategorieOptionUtilisateur.sLibelle, oOptionUtilisateur.sLibelle)
            End If
        Next
        Me.GvOptionsDisponibles.DataSource = oDataTable
        Me.GvOptionsDisponibles.DataBind()
        If sTypeModif = "visu" Or sTypeModif = "visucomplet" Or sTypeModif = "delete" Then
            Me.GvOptionsDisponibles.Columns(Me.GvOptionsDisponibles.Columns.Count - 1).Visible = False
        End If
    End Sub


    Private Sub GvOptionsAffectees_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GvOptionsAffectees.RowCommand
        If e.CommandName = "enlever" Then
            Dim nRowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim oOptionsUtilisateur As New OptionsUtilisateur
            Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
            oOptionsUtilisateur.chargerOptionsClient(nIdClient)
            Dim oListeFiltree = From oOptionUtilisateur In oLigne.oOptionsUtilisateur Where oOptionUtilisateur.nIdOption = CInt(GvOptionsAffectees.DataKeys(nRowIndex).Value)
            oLigne.oOptionsUtilisateur.Remove(CType(oListeFiltree(0), OptionUtilisateur))
            chargerOptionsAffectees()
            chargerOptionsLibres(oOptionsUtilisateur)
        End If
    End Sub
    Private Sub GvOptionsDisponibles_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GvOptionsDisponibles.RowCommand
        If e.CommandName = "ajouter" Then
            Dim nRowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim oOptionUtilisateur As New OptionUtilisateur
            Dim oOptionsUtilisateur As New OptionsUtilisateur
            Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
            oOptionsUtilisateur.chargerOptionsClient(nIdClient)
            oOptionUtilisateur.chargerOptionParId(CInt(GvOptionsDisponibles.DataKeys(nRowIndex).Value))
            oLigne.oOptionsUtilisateur.Add(oOptionUtilisateur)
            chargerOptionsAffectees()
            chargerOptionsLibres(oOptionsUtilisateur)
        End If
    End Sub

    Private Sub chargerStocksAffectes()
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idArticleStock")
        oDataTable.Columns.Add("image")
        oDataTable.Columns.Add("libelleTypeMateriel")
        oDataTable.Columns.Add("libelleMarque")
        oDataTable.Columns.Add("modele")
        oDataTable.Columns.Add("typeLiaison")
        oDataTable.Columns.Add("codeReference")
        oDataTable.Columns.Add("dateAttribution")
        oDataTable.Columns.Add("lien")
        Dim sTexteImage As String = ""
        Dim sLien As String = Nothing

        For Each oStock In oLigne.oStocks
            Select Case sTypeModif
                Case "insert", "edit"
                    If CInt(User.Identity.Name.Split(CChar("_"))(1)) = 118 Then
                        sLien = "<span onclick=""EditerStockCNES('" + oStock.nIdArticleStock.ToString + "','edit');""><img class=""imageDetail"" src=""images/icones/ico_loupe.png"" title=""Afficher le détail"" alt=""Afficher le détail""></span>"
                    Else
                        sLien = "<span onclick=""EditerStock('" + oStock.nIdArticleStock.ToString + "','edit');""><img class=""imageDetail"" src=""images/icones/ico_loupe.png"" title=""Afficher le détail"" alt=""Afficher le détail""></span>"
                    End If
                Case "delete", "visu", "visucomplet"
                    If CInt(User.Identity.Name.Split(CChar("_"))(1)) = 118 Then
                        sLien = "<span onclick=""EditerStockCNES('" + oStock.nIdArticleStock.ToString + "','visu');""><img class=""imageDetail"" src=""images/icones/ico_loupe.png"" title=""Afficher le détail"" alt=""Afficher le détail""></span>"
                    Else
                        sLien = "<span onclick=""EditerStock('" + oStock.nIdArticleStock.ToString + "','visu');""><img class=""imageDetail"" src=""images/icones/ico_loupe.png"" title=""Afficher le détail"" alt=""Afficher le détail""></span>"
                    End If
            End Select

            Select Case oStock.oArticle.oTypeMateriel.sLibelle
                Case "Accessoires"
                    sTexteImage = "<img src=""images/icones/ico_Accessoire.png"" class=""imageDataGridView"">"
                Case "Carte"
                    sTexteImage = "<img src=""images/icones/ico_Carte.png"" class=""imageDataGridView"">"
                Case "Terminaux Données", "Terminaux Voix et GPRS"
                    sTexteImage = "<img src=""images/icones/ico_Terminal.png"" class=""imageDataGridView"">"
            End Select
            oDataTable.Rows.Add(oStock.nIdArticleStock, sTexteImage, oStock.oArticle.oTypeMateriel.sLibelle, oStock.oArticle.oMarque.sLibelle, oStock.oArticle.sModele, oStock.sTypeLiaison, oStock.sCodeReference, oStock.dDateAttribution.ToString("dd/MM/yyyy"), sLien)
        Next
        Me.GvMaterielAffectes.DataSource = oDataTable
        Me.GvMaterielAffectes.DataBind()
        If sTypeModif = "visu" Or sTypeModif = "visucomplet" Or sTypeModif = "delete" Then
            Me.GvMaterielAffectes.Columns(Me.GvMaterielAffectes.Columns.Count - 1).Visible = False
        End If
    End Sub

    Private Sub ChargerListeTypeMateriel(ByVal oStocks As Stocks)

        Dim sListeStock = From oStock In oStocks Group oStock.oArticle By oStock.oArticle.oTypeMateriel.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")

        For Each sTexte In sListeStock
            Select Case sTexte
                Case "Terminaux Données", "Terminaux Voix et GPRS"
                    Dim CompteurTest = From oStock In oLigne.oStocks Where oStock.oArticle.oTypeMateriel.sLibelle.StartsWith("Terminaux")
                    If CompteurTest.Count() <> 2 Then
                        oDataTable.Rows.Add(sTexte)
                    End If
                Case "Carte"
                    Dim CompteurTest = From oStock In oLigne.oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = "Carte"
                    If CompteurTest.Count() <> 2 Then
                        oDataTable.Rows.Add(sTexte)
                    End If
                Case "Accessoires"
                    oDataTable.Rows.Add(sTexte)
            End Select

        Next
        Me.DdlTypeMateriel.DataSource = oDataTable
        Me.DdlTypeMateriel.DataValueField = "libelle"
        Me.DdlTypeMateriel.DataTextField = "libelle"
        Me.DdlTypeMateriel.DataBind()

        Dim oControle As DropDownList = Me.DdlTypeMateriel
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If


    End Sub

    Private Sub ChargerListeMarque(ByVal oStocks As Stocks)
        Dim sListeMarque = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue Group oStock.oArticle By oStock.oArticle.oMarque.sLibelle Into TupleGroup = Group Select sLibelle Order By sLibelle
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeMarque
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlMarque.DataSource = oDataTable
        Me.DdlMarque.DataValueField = "libelle"
        Me.DdlMarque.DataTextField = "libelle"
        Me.DdlMarque.DataBind()

        Dim oControle As DropDownList = Me.DdlMarque
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeModele(ByVal oStocks As Stocks)
        Dim sListeModele = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue Group oStock.oArticle By oStock.oArticle.sModele Into TupleGroup = Group Select sModele Order By sModele
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeModele
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlModele.DataSource = oDataTable
        Me.DdlModele.DataValueField = "libelle"
        Me.DdlModele.DataTextField = "libelle"
        Me.DdlModele.DataBind()

        Dim oControle As DropDownList = Me.DdlModele
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeCodeReference(ByVal oStocks As Stocks)
        Dim sListeCodeReference = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue Group oStock.oArticle By oStock.sCodeReference Into TupleGroup = Group Select sCodeReference Order By sCodeReference
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeCodeReference
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlCodeReference.DataSource = oDataTable
        Me.DdlCodeReference.DataValueField = "libelle"
        Me.DdlCodeReference.DataTextField = "libelle"
        Me.DdlCodeReference.DataBind()

        Dim oControle As DropDownList = Me.DdlCodeReference
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        End If

    End Sub

    Private Sub ChargerListeCodeEmei(ByVal oStocks As Stocks)
        Dim sListeCodeEmei = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue Group oStock.oArticle By oStock.sCodeEmei Into TupleGroup = Group Select sCodeEmei Order By sCodeEmei
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeCodeEmei
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlCodeEmei.DataSource = oDataTable
        Me.DdlCodeEmei.DataValueField = "libelle"
        Me.DdlCodeEmei.DataTextField = "libelle"
        Me.DdlCodeEmei.DataBind()

        Dim oControle As DropDownList = Me.DdlCodeEmei
        Dim sListeCodeEmeiSelectionne = From oStock In oStocks Where oStock.sCodeReference = Me.DdlCodeReference.SelectedValue Group oStock.oArticle By oStock.sCodeEmei Into TupleGroup = Group Select sCodeEmei Order By sCodeEmei

        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            If sListeCodeEmeiSelectionne.Count() <> 0 Then
                Try
                    oControle.SelectedValue = sListeCodeEmeiSelectionne(0)
                Catch

                End Try
            End If
        End If
    End Sub

    Private Sub ChargerListeNumSerie(ByVal oStocks As Stocks)
        Dim sListeNumSerie = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue Group oStock.oArticle By oStock.sNumSerie Into TupleGroup = Group Select sNumSerie Order By sNumSerie
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeNumSerie
            oDataTable.Rows.Add(sTexte)
        Next
        Me.DdlNumSerie.DataSource = oDataTable
        Me.DdlNumSerie.DataValueField = "libelle"
        Me.DdlNumSerie.DataTextField = "libelle"
        Me.DdlNumSerie.DataBind()

        Dim oControle As DropDownList = Me.DdlNumSerie
        Dim sListeNumSerieSelectionne = From oStock In oStocks Where oStock.sCodeReference = Me.DdlCodeReference.SelectedValue Group oStock.oArticle By oStock.sNumSerie Into TupleGroup = Group Select sNumSerie Order By sNumSerie

        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            If sListeNumSerieSelectionne.Count() <> 0 Then
                Try
                    oControle.SelectedValue = sListeNumSerieSelectionne(0)
                Catch

                End Try
            End If
        End If
    End Sub
    Private Sub ChargerIdStock(ByVal oStocks As Stocks)
        Dim sListeCodeReference = From oStock In oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = Me.DdlTypeMateriel.SelectedValue And oStock.oArticle.oMarque.sLibelle = Me.DdlMarque.SelectedValue And oStock.oArticle.sModele = Me.DdlModele.SelectedValue And oStock.sCodeReference = Me.DdlCodeReference.SelectedValue Group oStock By oStock.nIdArticleStock Into TupleGroup = Group Select nIdArticleStock Order By nIdArticleStock

        For Each sTexte In sListeCodeReference
            Me.TxtIdStock.Value = CStr(sTexte)
        Next

    End Sub
    Private Sub DdlTypeMateriel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlTypeMateriel.SelectedIndexChanged
        Dim oStocks As New Stocks
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oStocks.chargerStocksLibres(nIdClient)
        oStocks = FiltrerStocksAttribues(oStocks)

        ChargerListeMarque(oStocks)
        ChargerListeModele(oStocks)
        ChargerListeCodeReference(oStocks)
        ChargerListeCodeEmei(oStocks)
        ChargerListeNumSerie(oStocks)
        ChargerIdStock(oStocks)
    End Sub
    Private Sub DdlMarque_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlMarque.SelectedIndexChanged
        Dim oStocks As New Stocks
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oStocks.chargerStocksLibres(nIdClient)
        oStocks = FiltrerStocksAttribues(oStocks)

        ChargerListeModele(oStocks)
        ChargerListeCodeReference(oStocks)
        ChargerListeCodeEmei(oStocks)
        ChargerListeNumSerie(oStocks)
        ChargerIdStock(oStocks)
    End Sub
    Private Sub DdlModele_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlModele.SelectedIndexChanged
        Dim oStocks As New Stocks
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oStocks.chargerStocksLibres(nIdClient)
        oStocks = FiltrerStocksAttribues(oStocks)

        ChargerListeCodeReference(oStocks)
        ChargerListeCodeEmei(oStocks)
        ChargerListeNumSerie(oStocks)
        ChargerIdStock(oStocks)
    End Sub
    Private Sub DdlCodeReference_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlCodeReference.SelectedIndexChanged
        Dim oStocks As New Stocks
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oStocks.chargerStocksLibres(nIdClient)
        oStocks = FiltrerStocksAttribues(oStocks)

        ChargerListeCodeEmei(oStocks)
        ChargerListeNumSerie(oStocks)

        ChargerIdStock(oStocks)
    End Sub
    Private Sub DdlCodeEmei_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlCodeEmei.SelectedIndexChanged
        Dim oStocks As New Stocks
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oStocks.chargerStocksLibres(nIdClient)
        oStocks = FiltrerStocksAttribues(oStocks)

        RemoveHandler DdlCodeReference.SelectedIndexChanged, AddressOf DdlCodeReference_SelectedIndexChanged
        RemoveHandler DdlNumSerie.SelectedIndexChanged, AddressOf DdlNumSerie_SelectedIndexChanged

        Dim oControle As DropDownList = Me.DdlCodeReference
        Dim sListeCodeReference = From oStock In oStocks Where oStock.sCodeEmei = Me.DdlCodeEmei.SelectedValue Group oStock.oArticle By oStock.sCodeReference Into TupleGroup = Group Select sCodeReference Order By sCodeReference

        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            If sListeCodeReference.Count() <> 0 Then
                Try
                    oControle.SelectedValue = sListeCodeReference(0)
                Catch

                End Try
            End If
        End If

        ChargerListeNumSerie(oStocks)

        AddHandler DdlCodeReference.SelectedIndexChanged, AddressOf DdlCodeReference_SelectedIndexChanged
        AddHandler DdlNumSerie.SelectedIndexChanged, AddressOf DdlNumSerie_SelectedIndexChanged

        ChargerIdStock(oStocks)

    End Sub
    Private Sub DdlNumSerie_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlNumSerie.SelectedIndexChanged
        Dim oStocks As New Stocks
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oStocks.chargerStocksLibres(nIdClient)
        oStocks = FiltrerStocksAttribues(oStocks)

        RemoveHandler DdlCodeReference.SelectedIndexChanged, AddressOf DdlCodeReference_SelectedIndexChanged
        RemoveHandler DdlCodeEmei.SelectedIndexChanged, AddressOf DdlCodeEmei_SelectedIndexChanged

        Dim oControle As DropDownList = Me.DdlCodeReference
        Dim sListeCodeReference = From oStock In oStocks Where oStock.sNumSerie = Me.DdlNumSerie.SelectedValue Group oStock.oArticle By oStock.sCodeReference Into TupleGroup = Group Select sCodeReference Order By sCodeReference

        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            If sListeCodeReference.Count() <> 0 Then
                Try
                    oControle.SelectedValue = sListeCodeReference(0)
                Catch

                End Try
            End If
        End If

        ChargerListeCodeEmei(oStocks)

        AddHandler DdlCodeReference.SelectedIndexChanged, AddressOf DdlCodeReference_SelectedIndexChanged
        AddHandler DdlCodeEmei.SelectedIndexChanged, AddressOf DdlCodeEmei_SelectedIndexChanged

        ChargerIdStock(oStocks)

    End Sub
    Private Sub BtnAjouterMateriel_Click(sender As Object, e As EventArgs) Handles BtnAjouterMateriel.Click

        Dim oStockAAjouter As New Stock
        Dim oOptionsUtilisateur As New OptionsUtilisateur
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oStockAAjouter.chargerStockParId(CInt(Me.TxtIdStock.Value))
        oStockAAjouter.dDateAttribution = Now
        Select Case oStockAAjouter.oArticle.oTypeMateriel.sLibelle
            Case "Terminaux Données", "Terminaux Voix et GPRS"
                Dim CompteurTest = From oStock In oLigne.oStocks Where oStock.oArticle.oTypeMateriel.sLibelle.StartsWith("Terminaux") And oStock.sTypeLiaison = "Principal"
                If CompteurTest.Count() = 0 Then
                    oStockAAjouter.sTypeLiaison = "Principal"
                Else
                    oStockAAjouter.sTypeLiaison = "Secondaire"
                End If
                oLigne.oStocks.Add(oStockAAjouter)
            Case "Carte"
                Dim CompteurTest = From oStock In oLigne.oStocks Where oStock.oArticle.oTypeMateriel.sLibelle = "Carte" And oStock.sTypeLiaison = "Principale"
                If CompteurTest.Count() = 0 Then
                    oStockAAjouter.sTypeLiaison = "Principale"
                Else
                    oStockAAjouter.sTypeLiaison = "Jumelle"
                End If
                oLigne.oStocks.Add(oStockAAjouter)
            Case "Accessoires"
                oStockAAjouter.sTypeLiaison = ""
                oLigne.oStocks.Add(oStockAAjouter)
        End Select


        Dim oStocks As New Stocks
        oStocks.chargerStocksLibres(nIdClient)
        oStocks = FiltrerStocksAttribues(oStocks)
        ChargerListeTypeMateriel(oStocks)
        ChargerListeMarque(oStocks)
        ChargerListeModele(oStocks)
        ChargerListeCodeReference(oStocks)
        ChargerListeCodeEmei(oStocks)
        ChargerListeNumSerie(oStocks)
        ChargerIdStock(oStocks)

        chargerStocksAffectes()

    End Sub

    Private Sub GvMaterielAffectes_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GvMaterielAffectes.RowCommand
        If e.CommandName = "enlever" Then
            Dim nRowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim oStocks As New Stocks
            Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
            'oStocks.chargerStocks(nIdClient)
            Dim oListeFiltree = From oStock In oLigne.oStocks Where oStock.nIdArticleStock = CInt(GvMaterielAffectes.DataKeys(nRowIndex).Value)
            oLigne.oStocks.Remove(CType(oListeFiltree(0), Stock))


            oStocks = New Stocks
            oStocks.chargerStocksLibres(nIdClient)
            oStocks = FiltrerStocksAttribues(oStocks)

            ChargerListeTypeMateriel(oStocks)
            ChargerListeMarque(oStocks)
            ChargerListeModele(oStocks)
            ChargerListeCodeReference(oStocks)
            ChargerListeCodeEmei(oStocks)
            ChargerListeNumSerie(oStocks)
            ChargerIdStock(oStocks)

            chargerStocksAffectes()

        End If
    End Sub

    Private Function FiltrerStocksAttribues(ByRef oStocks As Stocks) As Stocks
        Dim oStocksLibre As New Stocks



        Dim oListeFiltree = oStocks.Where(Function(a) oLigne.oStocks.Contains(a) = False)


        For Each oStock In oLigne.oStocks
            oListeFiltree = From oStockTest In oStocks Where oStockTest.nIdArticleStock <> oStock.nIdArticleStock
        Next

        For Each oStock In oListeFiltree
            oStocksLibre.Add(oStock)
        Next
        Return oStocksLibre
    End Function


    Private Sub BtnEditionValider_Click(sender As Object, e As EventArgs) Handles BtnEditionValider.Click

        Dim oFonction As New Fonction
        Dim oCompteFacturant As New CompteFacturant
        Dim oDiscrimination As New Discrimination
        Dim oForfait As New Forfait
        Dim oHierarchie As New Hierarchie

        If Not IsDate(Me.TxtDateActivation.Text) Then
            Me.LblInformation.Visible = True
            Me.LblInformation.Text = "Vous devez saisir une date d'activation valide."
            Exit Sub
        End If

        If Me.TxtNom.Text = "" And Me.TxtPrenom.Text = "" Then
            Me.LblInformation.Visible = True
            Me.LblInformation.Text = "Vous devez saisir un nom ou un prénom avant de pouvoir valider votre opération"
            Exit Sub
        End If

        oFonction.chargerFonctionParId(CInt(Me.DdlFonction.SelectedValue))
        oCompteFacturant.chargerCompteFacturantParId(CInt(Me.DdlCompteFacturant.SelectedValue))
        oDiscrimination.chargerDiscriminationParId(CInt(Me.DdlDiscrimination.SelectedValue))
        oForfait.chargerForfaitParId(RecupererIdForfait)
        oHierarchie.chargerHierarchieParId(RecupererIdHierarchie)

        oLigne.sNumeroGSM = Me.TxtNumeroGSM.Text
        oLigne.sNumeroData = Me.TxtNumeroData.Text
        oLigne.sNom = Me.TxtNom.Text
        oLigne.sPrenom = Me.TxtPrenom.Text
        oLigne.bEstActif = True
        oLigne.dDateActivation = CDate(Me.TxtDateActivation.Text)
        oLigne.oFonction = oFonction
        oLigne.oCompteFacturant = oCompteFacturant
        oLigne.oDiscrimination = oDiscrimination
        oLigne.oForfait = oForfait
        oLigne.oHierarchie = oHierarchie

        Dim oAncienneLigne As New Ligne
        oAncienneLigne.chargerLigneParId(oLigne.nIdLigne)


        Dim oCommande As New Commande
        oCommande.oOptionsAjoutees = New OptionsUtilisateur
        oCommande.oOptionsRetirees = New OptionsUtilisateur
        oCommande.oStockAjoutes = New Stocks
        oCommande.oStockRetires = New Stocks
        oCommande.oAncienneLigne = oAncienneLigne
        oCommande.oNouvelleLigne = oLigne
        oCommande.sTypeCommande = "modification"
        oCommande.bEstRealiseFacture = False
        oCommande.bEstAffichable = False
        oCommande.bEstTelecharge = False

        If oLigne.oCompteFacturant.nIdCompteFacturant <> oAncienneLigne.oCompteFacturant.nIdCompteFacturant Then oCommande.bEstAffichable = True
        If oLigne.oForfait.nIdForfait <> oAncienneLigne.oForfait.nIdForfait Then oCommande.bEstAffichable = True
        If oLigne.oDiscrimination.nIdDiscrimination <> oAncienneLigne.oDiscrimination.nIdDiscrimination Then oCommande.bEstAffichable = True
        If oLigne.sNom <> oAncienneLigne.sNom Then oCommande.bEstAffichable = True
        If oLigne.sPrenom <> oAncienneLigne.sPrenom Then oCommande.bEstAffichable = True

        '---TestMAteriel
        For Each oStock In oLigne.oStocks
            Dim oStocksCompteur = From oSTockTest In oAncienneLigne.oStocks Where oSTockTest.nIdArticleStock = oStock.nIdArticleStock And oSTockTest.sTypeLiaison = oStock.sTypeLiaison
            If oStocksCompteur.Count() = 0 Then
                Dim oStockTest As New Stock
                oStockTest.chargerStockParId(oStock.nIdArticleStock)
                If oStockTest.sEtat <> "Non Attribué" Then
                    Me.LblInformation.Visible = True
                    Me.LblInformation.Text = "le matériel " & oStockTest.sCodeReference & " est indisponible car un autre utilisateur vient de l'attribuer."
                    Exit Sub
                End If
            End If
        Next




        For Each oOptionUtilisateur In oAncienneLigne.oOptionsUtilisateur
            Dim oOptionUtilisateurCompteur = From oOptionUtilisateurTest In oLigne.oOptionsUtilisateur Where oOptionUtilisateurTest.nIdOption = oOptionUtilisateur.nIdOption
            If oOptionUtilisateurCompteur.Count() = 0 Then
                oLigne.enleverOption(oOptionUtilisateur)
                oCommande.oOptionsRetirees.Add(oOptionUtilisateur)
                oCommande.bEstAffichable = True
            End If
        Next
        For Each oOptionUtilisateur In oLigne.oOptionsUtilisateur
            Dim oOptionUtilisateurCompteur = From oOptionUtilisateurTest In oAncienneLigne.oOptionsUtilisateur Where oOptionUtilisateurTest.nIdOption = oOptionUtilisateur.nIdOption
            If oOptionUtilisateurCompteur.Count() = 0 Then
                oLigne.ajouterOption(oOptionUtilisateur)
                oCommande.oOptionsAjoutees.Add(oOptionUtilisateur)
                oCommande.bEstAffichable = True
            End If
        Next


        For Each oStock In oAncienneLigne.oStocks
            Dim oStocksCompteur = From oSTockTest In oLigne.oStocks Where oSTockTest.nIdArticleStock = oStock.nIdArticleStock And oSTockTest.sTypeLiaison = oStock.sTypeLiaison
            If oStocksCompteur.Count() = 0 Then
                oLigne.enleverStock(oStock)
                oCommande.oStockRetires.Add(oStock)
            End If
        Next
        For Each oStock In oLigne.oStocks
            Dim oStocksCompteur = From oSTockTest In oAncienneLigne.oStocks Where oSTockTest.nIdArticleStock = oStock.nIdArticleStock And oSTockTest.sTypeLiaison = oStock.sTypeLiaison
            If oStocksCompteur.Count() = 0 Then
                oLigne.ajouterStock(oStock)
                oCommande.oStockAjoutes.Add(oStock)
            End If
        Next

        If User.Identity.Name.Split(CChar("_"))(2) <> "" And User.Identity.Name.Split(CChar("_"))(2) <> "0" Then
            oCommande.nIdUtilisateurPrincipal = 0
            oCommande.nIdUtilisateurSecondaire = CInt(User.Identity.Name.Split(CChar("_"))(2))
        Else
            oCommande.nIdUtilisateurPrincipal = CInt(User.Identity.Name.Split(CChar("_"))(0))
            oCommande.nIdUtilisateurSecondaire = 0
        End If

        oLigne.editerLigne()
        oCommande.ajouterCommande()
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)

    End Sub

    Private Sub BtnEditionAnnuler_Click(sender As Object, e As EventArgs) Handles BtnEditionAnnuler.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub


    Private Sub BtnDeleteValider_Click(sender As Object, e As EventArgs) Handles BtnDeleteValider.Click
        If oLigne.oStocks.Count > 0 Then
            Me.LblInformation.Visible = True
            Me.LblInformation.Text = "Vous ne pouvez pas supprimer cette ligne car du matériel y est affecté."
            Exit Sub
        Else
            Me.LblInformation.Visible = False
        End If

        Dim oCommande As New Commande
        oCommande.oOptionsAjoutees = New OptionsUtilisateur
        oCommande.oOptionsRetirees = New OptionsUtilisateur
        oCommande.oStockAjoutes = New Stocks
        oCommande.oStockRetires = New Stocks
        oCommande.oAncienneLigne = oLigne
        oCommande.sTypeCommande = "suppression"
        oCommande.bEstRealiseFacture = False
        oCommande.bEstAffichable = True
        oCommande.bEstTelecharge = False

        oLigne.supprimerLigne()


         If User.Identity.Name.Split(CChar("_"))(2) <> "" And User.Identity.Name.Split(CChar("_"))(2) <> "0" Then
            oCommande.nIdUtilisateurPrincipal = 0
            oCommande.nIdUtilisateurSecondaire = CInt(User.Identity.Name.Split(CChar("_"))(2))
        Else
            oCommande.nIdUtilisateurPrincipal = CInt(User.Identity.Name.Split(CChar("_"))(0))
            oCommande.nIdUtilisateurSecondaire = 0
        End If

        oCommande.ajouterCommande()

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub

    Private Sub BtnDeleteAnnuler_Click(sender As Object, e As EventArgs) Handles BtnDeleteAnnuler.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub

    Private Sub BtnInsertAnnuler_Click(sender As Object, e As EventArgs) Handles BtnInsertAnnuler.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub


    Private Sub BtnInsertValider_Click(sender As Object, e As EventArgs) Handles BtnInsertValider.Click

        Dim oFonction As New Fonction
        Dim oCompteFacturant As New CompteFacturant
        Dim oDiscrimination As New Discrimination
        Dim oForfait As New Forfait
        Dim oHierarchie As New Hierarchie
        Dim oCommande As New Commande
        oCommande.oOptionsAjoutees = New OptionsUtilisateur
        oCommande.oOptionsRetirees = New OptionsUtilisateur
        oCommande.oStockAjoutes = New Stocks
        oCommande.oStockRetires = New Stocks


        If Not IsDate(Me.TxtDateActivation.Text) Then
            Me.LblInformation.Visible = True
            Me.LblInformation.Text = "Vous devez saisir une date d'activation valide."
            Exit Sub
        End If

        If Me.TxtNom.Text = "" And Me.TxtPrenom.Text = "" Then
            Me.LblInformation.Visible = True
            Me.LblInformation.Text = "Vous devez saisir un nom ou un prénom avant de pouvoir valider votre opération"
            Exit Sub
        End If



        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        oFonction.chargerFonctionParId(CInt(Me.DdlFonction.SelectedValue))
        oCompteFacturant.chargerCompteFacturantParId(CInt(Me.DdlCompteFacturant.SelectedValue))
        oDiscrimination.chargerDiscriminationParId(CInt(Me.DdlDiscrimination.SelectedValue))
        oForfait.chargerForfaitParId(RecupererIdForfait)
        oHierarchie.chargerHierarchieParId(RecupererIdHierarchie)

        oLigne.sNumeroGSM = Me.TxtNumeroGSM.Text
        oLigne.sNumeroData = Me.TxtNumeroData.Text
        oLigne.sNom = Me.TxtNom.Text
        oLigne.sPrenom = Me.TxtPrenom.Text
        oLigne.bEstActif = True
        oLigne.dDateActivation = CDate(Me.TxtDateActivation.Text)
        oLigne.oFonction = oFonction
        oLigne.oCompteFacturant = oCompteFacturant
        oLigne.oDiscrimination = oDiscrimination
        oLigne.oForfait = oForfait
        oLigne.oHierarchie = oHierarchie
        oLigne.nIdClient = nIdClient

        oLigne.insererLigne()

        oCommande.oNouvelleLigne = oLigne
        oCommande.sTypeCommande = "creation"
        oCommande.bEstRealiseFacture = False
        oCommande.bEstAffichable = True
        oCommande.bEstTelecharge = False

        '---TestMAteriel
        For Each oStock In oLigne.oStocks
            Dim oStockTest As New Stock
            oStockTest.chargerStockParId(oStock.nIdArticleStock)
            If oStockTest.sEtat <> "Non Attribué" Then
                Me.LblInformation.Visible = True
                Me.LblInformation.Text = "le matériel " & oStockTest.sCodeReference & " est indisponible car un autre utilisateur vient de l'attribuer."
                Exit Sub
            End If
        Next

        For Each oOptionUtilisateur In oLigne.oOptionsUtilisateur
            oLigne.ajouterOption(oOptionUtilisateur)
            oCommande.oOptionsAjoutees.Add(oOptionUtilisateur)
        Next

        For Each oStock In oLigne.oStocks
            oLigne.ajouterStock(oStock)
            oCommande.oStockAjoutes.Add(oStock)
        Next

        If User.Identity.Name.Split(CChar("_"))(2) <> "" And User.Identity.Name.Split(CChar("_"))(2) <> "0" Then
            oCommande.nIdUtilisateurPrincipal = 0
            oCommande.nIdUtilisateurSecondaire = CInt(User.Identity.Name.Split(CChar("_"))(2))
        Else
            oCommande.nIdUtilisateurPrincipal = CInt(User.Identity.Name.Split(CChar("_"))(0))
            oCommande.nIdUtilisateurSecondaire = 0
        End If

        oCommande.ajouterCommande()

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub

    Private Sub SendFile(ByVal strPath As System.String, ByVal strSuggestedName As System.String)
        Try
            Dim objSourceFileInfo As System.IO.FileInfo
            objSourceFileInfo = New System.IO.FileInfo(strPath)
            If objSourceFileInfo.Exists Then

                With Me.Response

                    .ContentType = "application/octet-stream"
                    .AddHeader("Content-Disposition", "attachment; filename=" & Replace(strSuggestedName, " ", "_"))
                    .AddHeader("Content-Length", objSourceFileInfo.Length.ToString)
                    .WriteFile(objSourceFileInfo.FullName)
                    .Flush()

                    .End()

                End With
            Else


            End If

        Catch e As Exception
            Dim a As String = ""

        End Try
    End Sub

    Private Sub BpBordereauMateriel_Click(sender As Object, e As EventArgs) Handles BpBordereauMateriel.Click
        Dim oUtilisateur As New Utilisateur()
        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))


        Dim sCheminFichierModele As String = Server.MapPath("\documents\" & oUtilisateur.oClient.sNomClient & "\GestionParc\Modeles\Modele Remise Materiel.xls")
        Dim sCheminFichierEnregistrement = Server.MapPath("\documents\" & oUtilisateur.oClient.sNomClient & "\GestionParc\BordereauRemiseMateriel\Remise Materiel " & oLigne.sNumeroGSM & ".xls")

        Dim Process1() As Process = Process.GetProcesses()
        Dim oExcel As New Excel.Application
        Dim oClasseur As Excel.Workbook = oExcel.Workbooks.Open(sCheminFichierModele)
        Dim oFichier As FileInfo
        oFichier = New FileInfo(sCheminFichierEnregistrement)
        If oFichier.Exists Then
            oFichier.Delete()
        End If

        oClasseur.SaveAs(sCheminFichierEnregistrement)
        Dim oFeuille As Excel.Worksheet = CType(oClasseur.Sheets(1), Excel.Worksheet)
        Dim idProcExcel As Integer
        Dim Process2() As Process = Process.GetProcesses()
        ' -------- Recherche de mon Id de processus
        Dim bMonProcessXL As Boolean
        For j = 0 To Process2.GetUpperBound(0)
            If Process2(j).ProcessName = "EXCEL" Then
                bMonProcessXL = True
                ' Parcours des processus avant le mien
                For i = 0 To Process1.GetUpperBound(0)
                    If Process1(i).ProcessName = "EXCEL" Then
                        If Process2(j).Id = Process1(i).Id Then
                            ' S'il existait avant, c
                            bMonProcessXL = False
                            Exit For
                        End If
                    End If
                Next i
                If bMonProcessXL = True Then
                    idProcExcel = Process2(j).Id
                    Exit For
                End If
            End If
        Next j
        oExcel.Visible = False : oExcel.DisplayAlerts = False


        oFeuille.Range("C3").Value = "Date d'édition : " + Now.ToString("dd MMM yyyy")

        oFeuille.Range("C6").Value = oLigne.sNom
        oFeuille.Range("C7").Value = oLigne.sPrenom
        oFeuille.Range("C8").Value = oLigne.oHierarchie.sHierarchie1
        oFeuille.Range("C9").Value = oLigne.oHierarchie.sHierarchie2
        oFeuille.Range("C10").Value = oLigne.oHierarchie.sHierarchie3
        oFeuille.Range("C11").Value = oLigne.oHierarchie.sHierarchie4
        oFeuille.Range("C12").Value = oLigne.oHierarchie.sHierarchie5

        oFeuille.Range("C15").Value = "'" & oLigne.sNumeroGSM
        For Each oStock In oLigne.oStocks
            If oStock.sTypeLiaison.ToUpper = "PRINCIPAL" Then
                oFeuille.Range("C16").Value = oStock.oArticle.oMarque.sLibelle
                oFeuille.Range("C17").Value = oStock.oArticle.sModele
                oFeuille.Range("C18").Value = oStock.sCodeEmei
            End If
        Next
        For Each oStock In oLigne.oStocks
            If oStock.sTypeLiaison.ToUpper = "PRINCIPALE" Then
                oFeuille.Range("C19").Value = oStock.sNumSerie
                oFeuille.Range("C20").Value = oStock.sCodePin1
            End If
        Next


        oClasseur.Save()
        oClasseur.Close()
        oFeuille = Nothing
        oClasseur = Nothing
        oExcel = Nothing

        Try
            If idProcExcel <> 0 Then
                Process.GetProcessById(idProcExcel).Kill()
            Else
                For Each el In Process.GetProcessesByName("EXCEL")
                    el.Kill()
                Next
            End If
        Catch ex As Exception
            For Each el In Process.GetProcessesByName("EXCEL")
                el.Kill()
            Next
        End Try



        Try
            SendFile(sCheminFichierEnregistrement, sCheminFichierEnregistrement.Split(CChar("\"))(sCheminFichierEnregistrement.Split(CChar("\")).Length - 1))
        Catch ex As Exception
        Finally
            
        End Try
    End Sub

   
End Class