﻿Imports System.Reflection

Public Class GestionParcFixe
    Inherits System.Web.UI.Page

    Private Property oLigneFixe() As LigneFixe
        Get
            Return CType(Session("oLigneFixe"), LigneFixe)
        End Get
        Set(ByVal Value As LigneFixe)
            Session("oLigneFixe") = Value
        End Set
    End Property

    Private Property sTypeModifFixe() As String
        Get
            Return CType(Session("sTypeModifFixe"), String)
        End Get
        Set(ByVal Value As String)
            Session("sTypeModifFixe") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If

        Dim oClient As New Client(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        Dim sNumero As String = ""
        Dim oHierarchies As New HierarchiesFixe

        If Request.Item("numeroLigne") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            sNumero = Request.Item("numeroLigne").ToString
        End If

        If Request.Item("typeModif") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            sTypeModifFixe = Request.Item("typeModif").ToString
        End If


        If Not Page.IsPostBack Then
            oLigneFixe = New LigneFixe
            If oLigneFixe.oHierarchieFixe Is Nothing Then
                oLigneFixe.oHierarchieFixe = New HierarchieFixe
            End If
            If oLigneFixe.oCompteFacturantFixe Is Nothing Then
                oLigneFixe.oCompteFacturantFixe = New CompteFacturantFixe
            End If
            If oLigneFixe.oUsageFixe Is Nothing Then
                oLigneFixe.oUsageFixe = New UsageFixe
            End If
            If oLigneFixe.oTypeLigneFixe Is Nothing Then
                oLigneFixe.oTypeLigneFixe = New TypeLigneFixe
            End If
            If oLigneFixe.oContratFixe Is Nothing Then
                oLigneFixe.oContratFixe = New ContratFixe
            End If
            If oLigneFixe.oChoixListeFixe Is Nothing Then
                oLigneFixe.oChoixListeFixe = New ChoixListeFixe
            End If
            If oLigneFixe.oTypeSelectionFixe Is Nothing Then
                oLigneFixe.oTypeSelectionFixe = New TypeSelectionFixe
            End If
            If oLigneFixe.oTypeMail Is Nothing Then
                oLigneFixe.oTypeMail = New TypeMail
            End If

            If sNumero <> "" Then
                oLigneFixe.chargerLigneFixeParNumero(sNumero, oClient)
            End If

            Dim oTypesLignes As New TypesLignesFixe
            oTypesLignes.chargerTypesLignesFixe(oClient)
            ChargerListeTypeLigne(oTypesLignes)

            Dim oUsages As New UsagesFixe
            oUsages.chargerUsagesFixe(oClient)
            ChargerListeUsage(oUsages, oClient)







        End If



        ChargerControles()


        If Not Page.IsPostBack Then

            ChargerListes()

        End If


        ChargerImageAction(sTypeModifFixe)

        Me.MasquageOnglets.Value = ""
        Select Case sTypeModifFixe
            Case "insert"
                Me.LblTitre.Text = "Ajouter un accès"
                Me.TxtDateActivation.Text = (Now.AddDays(7)).ToString("dd/MM/yyyy")
                Me.TxtDateActivation.Enabled = True
                Me.TxtNumeroGSM.Text = "-"
                Me.LblDateActivation.Text = "Date prévisionelle :"
                Me.TxtNumeroGSM.Enabled = False
            Case "edit"
                Me.LblTitre.Text = "Modifier un accès"
                Me.MasquageOnglets.Value = ""
                Me.DdlTypeLigne.Enabled = False
                Me.TxtNumeroGSM.Enabled = False
                Me.TxtDateActivation.Enabled = False
            Case "delete"
                Me.LblTitre.Text = "Supprimer un accès"
                Me.MasquageOnglets.Value = "4"
                Me.DdlTypeLigne.Enabled = False
                Me.TxtNumeroGSM.Enabled = False
                Me.TxtDateActivation.Enabled = False
                Me.DdlUsage.Enabled = False
            Case "visu"
                Me.LblTitre.Text = "Détail de l'accès"
                Me.MasquageOnglets.Value = "2;3;5"
                Me.DdlTypeLigne.Enabled = False
                Me.TxtNumeroGSM.Enabled = False
                Me.TxtDateActivation.Enabled = False
                Me.DdlUsage.Enabled = False
            Case "visucomplet"
                Me.LblTitre.Text = "Détail de l'accès"
                Me.MasquageOnglets.Value = "2;3;5"
                Me.DdlTypeLigne.Enabled = False
                Me.TxtNumeroGSM.Enabled = False
                Me.TxtDateActivation.Enabled = False
                Me.DdlUsage.Enabled = False
        End Select

        If Not Page.Request("DdlTypeLigne") Is Nothing AndAlso Page.Request("DdlTypeLigne").ToString <> "" Then
            Dim oTypeLigne As New TypeLigneFixe
            oTypeLigne.chargerTypeLigneFixeParId(CInt(Page.Request("DdlTypeLigne").ToString))

            If oTypeLigne.sLibelleTypeLigne <> "Ligne Analogique" And sTypeModifFixe <> "insert" And oTypeLigne.sLibelleTypeLigne <> "Internet" Then
                If Me.MasquageOnglets.Value = "" Then
                    Me.MasquageOnglets.Value = "6"
                Else
                    Me.MasquageOnglets.Value += ";6"
                End If
            End If

            If oTypeLigne.sLibelleTypeLigne = "Internet" Then
                If Me.MasquageOnglets.Value = "" Then
                    Me.MasquageOnglets.Value = "0;4"
                Else
                    Me.MasquageOnglets.Value += ";0;4"
                End If
            End If


        Else
            If Me.DdlTypeLigne.SelectedItem.ToString <> "Ligne Analogique" And sTypeModifFixe <> "insert" And Me.DdlTypeLigne.SelectedItem.ToString <> "Internet" Then
                If Me.MasquageOnglets.Value = "" Then
                    Me.MasquageOnglets.Value = "6"
                Else
                    Me.MasquageOnglets.Value += ";6"
                End If
            End If

            If Me.DdlTypeLigne.SelectedItem.ToString = "Internet" Then
                If Me.MasquageOnglets.Value = "" Then
                    Me.MasquageOnglets.Value = "0;4"
                Else
                    Me.MasquageOnglets.Value += ";0;4"
                End If
            End If

        End If




    End Sub

    Private Sub ChargerListes()
        Dim oClient As New Client(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        Dim oHierarchies As New HierarchiesFixe
        Dim sNumero As String = ""

        If Request.Item("numeroLigne") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            sNumero = Request.Item("numeroLigne").ToString
        End If

        If Not Page.FindControl("DdlTypeSelectionFixe") Is Nothing Then ChargerListeTypeSelectionPermanente()
        If Not Page.FindControl("DdlTypeMail") Is Nothing Then ChargerListeTypeMail()
        If Not Page.FindControl("DdlChoixListeFixe") Is Nothing Then ChargerListeChoixListe()
        If Not Page.FindControl("LstLignesSecondaires") Is Nothing Then ChargerListeLignesSecondaires()

        Dim oComptesFacturantsFixe As New CompteFacturantsFixe
        oComptesFacturantsFixe.chargerCompteFacturantsFixe(oClient)
        ChargerListeCompteFacturant(oComptesFacturantsFixe, oClient)

        ChargerCentreFrais()


        Dim oContactInstallationsFixes As New ContactInstallationFixes()
        oContactInstallationsFixes.chargerContactInstallationFixes(oClient)
        chargerListeContactInstallation(oContactInstallationsFixes, oClient)

        Dim oAdresseInstallationFixes As New AdresseInstallationFixes()
        oAdresseInstallationFixes.chargerAdresseInstallationFixes(oClient)


        ChargerListeContrat()

        oHierarchies.chargerHierarchiesFixe(oClient)
        ChargerListeHierarchie1(oHierarchies)
        If oLigneFixe.oHierarchieFixe.nIdHierarchie <> 0 Then CType(Page.FindControl("DdlHierarchie1"), DropDownList).SelectedValue = oLigneFixe.oHierarchieFixe.sHierarchie1
        ChargerListeHierarchie2(oHierarchies)
        If oLigneFixe.oHierarchieFixe.nIdHierarchie <> 0 Then CType(Page.FindControl("DdlHierarchie2"), DropDownList).SelectedValue = oLigneFixe.oHierarchieFixe.sHierarchie2
        ChargerListeHierarchie3(oHierarchies)
        If oLigneFixe.oHierarchieFixe.nIdHierarchie <> 0 Then CType(Page.FindControl("DdlHierarchie3"), DropDownList).SelectedValue = oLigneFixe.oHierarchieFixe.sHierarchie3
        ChargerListeHierarchie4(oHierarchies)
        If oLigneFixe.oHierarchieFixe.nIdHierarchie <> 0 Then CType(Page.FindControl("DdlHierarchie4"), DropDownList).SelectedValue = oLigneFixe.oHierarchieFixe.sHierarchie4
        ChargerListeHierarchie5(oHierarchies)
        If oLigneFixe.oHierarchieFixe.nIdHierarchie <> 0 Then CType(Page.FindControl("DdlHierarchie5"), DropDownList).SelectedValue = oLigneFixe.oHierarchieFixe.sHierarchie5
        ChargerListeHierarchie6(oHierarchies)
        If oLigneFixe.oHierarchieFixe.nIdHierarchie <> 0 Then CType(Page.FindControl("DdlHierarchie6"), DropDownList).SelectedValue = oLigneFixe.oHierarchieFixe.sHierarchie6

        If sNumero <> "" And sNumero <> "0" Then
            ChargerValeurs()
        End If

        ValoriserInformationsAdresse()
        ValoriserInformationsContact()

        Me.TxtDateActivation.Text = oLigneFixe.dDateActivation.ToString("dd/MM/yyyy")
        Me.TxtNumeroGSM.Text = oLigneFixe.sNumeroLigneFixe
    End Sub

    Private Sub ChargerValeurs()
        Dim oParametragesControlesFixe As New ParametragesControlesFixe
        oParametragesControlesFixe.chargerParametragesControlesFixe(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        Dim oListeFiltree = From oParametrageControleFixe In oParametragesControlesFixe Where oParametrageControleFixe.sTypeLigne = Me.DdlTypeLigne.SelectedItem.Text And oParametrageControleFixe.sCas = sTypeModifFixe And oParametrageControleFixe.sNomMembre <> ""
        For Each oParametrageControle In oListeFiltree
            Select Case oParametrageControle.sTypeControle
                Case "Checkbox"
                    Dim oControle As CheckBox
                    oControle = CType(Page.FindControl("Chk" & oParametrageControle.sIdControle), CheckBox)
                    oControle.Checked = CBool(Toolbox.GetPropertyValue(oLigneFixe, oParametrageControle.sNomMembre))
                Case "Textbox", "Commentaire"
                    Dim oControle As TextBox
                    oControle = CType(Page.FindControl("Txt" & oParametrageControle.sIdControle), TextBox)
                    oControle.Text = CStr(Toolbox.GetPropertyValue(oLigneFixe, oParametrageControle.sNomMembre))
                Case "DropdownList"
                    Dim oControle As DropDownList
                    oControle = CType(Page.FindControl("Ddl" & oParametrageControle.sIdControle), DropDownList)
                    oControle.Text = CStr(Toolbox.GetPropertyValue(oLigneFixe, oParametrageControle.sNomMembre))
            End Select
        Next

    End Sub

    Private Sub ChargerListeTypeLigne(ByVal oTypesLignes As TypesLignesFixe)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idTypeLigne")
        oDataTable.Columns.Add("libelleTypeLigne")
        For Each oTypeLigne In oTypesLignes
            oDataTable.Rows.Add(oTypeLigne.nIdTypeLigne, oTypeLigne.sLibelleTypeLigne)
        Next
        Me.DdlTypeLigne.DataSource = oDataTable
        Me.DdlTypeLigne.DataValueField = "idTypeLigne"
        Me.DdlTypeLigne.DataTextField = "libelleTypeLigne"
        Me.DdlTypeLigne.DataBind()

        Dim oControle As DropDownList = Me.DdlTypeLigne
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        Else
            If oLigneFixe.oTypeLigneFixe.nIdTypeLigne <> 0 Then
                oControle.SelectedValue = oLigneFixe.oTypeLigneFixe.nIdTypeLigne.ToString
            End If
        End If



    End Sub

    Private Sub ChargerListeUsage(ByVal oUsages As UsagesFixe, ByRef oClient As Client)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idUsage")
        oDataTable.Columns.Add("libelleUsage")
        For Each oUsage In oUsages
            oDataTable.Rows.Add(oUsage.nIdUsage, oUsage.sLibelleUsage)
        Next
        Me.DdlUsage.DataSource = oDataTable
        Me.DdlUsage.DataValueField = "idUsage"
        Me.DdlUsage.DataTextField = "libelleUsage"
        Me.DdlUsage.DataBind()

        Dim oControle As DropDownList = Me.DdlUsage
        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        Else
            If oLigneFixe.oUsageFixe.nIdUsage <> 0 Then
                oControle.SelectedValue = oLigneFixe.oUsageFixe.nIdUsage.ToString
            End If
        End If

    End Sub

    Private Sub ChargerListeContrat()
        Dim oControle As DropDownList = CType(Page.FindControl("DdlContrat"), DropDownList)
        Dim oContratFixes As New ContratFixes()
        Dim oClient As New Client(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        Dim oTypeLigneFixe As New TypeLigneFixe()

        oTypeLigneFixe.nIdTypeLigne = CInt(Me.DdlTypeLigne.SelectedItem.Value)
        oTypeLigneFixe.sLibelleTypeLigne = Me.DdlTypeLigne.SelectedItem.Text
        oTypeLigneFixe.oClient = oClient

        oContratFixes.chargerContratFixesParTypeLigne(oTypeLigneFixe)

        If oControle Is Nothing Then Exit Sub

        oControle.DataSource = oContratFixes
        oControle.DataValueField = "nIdContrat"
        oControle.DataTextField = "sLibelleContrat"
        oControle.DataBind()

        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
            Try
                oControle.SelectedValue = Page.Request(oControle.ID).ToString
            Catch
            End Try
        Else
            If oLigneFixe.oContratFixe.nIdContrat <> 0 Then
                oControle.SelectedValue = oLigneFixe.oContratFixe.nIdContrat.ToString
            End If
        End If

    End Sub

    Private Sub ChargerListeTypeSelectionPermanente()
        Dim oControle As DropDownList = CType(Page.FindControl("DdlTypeSelectionFixe"), DropDownList)
        Dim oTypeSelectionFixes As New TypeSelectionFixes()
        Dim oClient As New Client(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        oTypeSelectionFixes.chargerTypeSelectionFixes(oClient)

        If Not IsNothing(oControle) Then

            oControle.DataSource = oTypeSelectionFixes
            oControle.DataValueField = "nIdTypeSelection"
            oControle.DataTextField = "sLibelleTypeSelection"
            oControle.DataBind()

            If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
                Try
                    oControle.SelectedValue = Page.Request(oControle.ID).ToString
                Catch
                End Try
            Else
                If oLigneFixe.oTypeSelectionFixe.nIdTypeSelection <> 0 Then
                    oControle.SelectedValue = oLigneFixe.oTypeSelectionFixe.nIdTypeSelection.ToString
                End If
            End If
        End If
    End Sub

    Private Sub ChargerListeTypeMail()
        Dim oControle As DropDownList = CType(Page.FindControl("DdlTypeMail"), DropDownList)
        Dim oTypeMails As New TypeMails()
        Dim oClient As New Client(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        oTypeMails.chargerTypeMails(oClient)

        If Not IsNothing(oControle) Then
            oControle.DataSource = oTypeMails
            oControle.DataValueField = "nIdTypeMail"
            oControle.DataTextField = "sLibelleTypeMail"
            oControle.DataBind()

            If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
                Try
                    oControle.SelectedValue = Page.Request(oControle.ID).ToString
                Catch
                End Try
            Else
                If oLigneFixe.oTypeMail.nIdTypeMail <> 0 Then
                    oControle.SelectedValue = oLigneFixe.oTypeMail.nIdTypeMail.ToString
                End If
            End If
        End If
    End Sub

    Private Sub ChargerListeChoixListe()
        Dim oControle As DropDownList = CType(Page.FindControl("DdlChoixListeFixe"), DropDownList)

        Dim oClient As New Client(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        Dim oChoixListeFixes As New ChoixListeFixes()
        oChoixListeFixes.chargerChoixListeFixes(oClient)

        If Not IsNothing(oControle) Then

            oControle.DataSource = oChoixListeFixes
            oControle.DataValueField = "nIdChoixListe"
            oControle.DataTextField = "sLibelleChoixListe"
            oControle.DataBind()

            If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
                Try
                    oControle.SelectedValue = Page.Request(oControle.ID).ToString
                Catch
                End Try
            Else
                If oLigneFixe.oChoixListeFixe.nIdChoixListe <> 0 Then
                    oControle.SelectedValue = oLigneFixe.oChoixListeFixe.nIdChoixListe.ToString
                End If
            End If
        End If
    End Sub

    Private Sub ChargerListeLignesSecondaires()
        Dim oControle As ListBox = CType(Page.FindControl("LstLignesSecondaires"), ListBox)

        Dim oClient As New Client(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        Dim oLigneFixes As New LigneFixes()
        oLigneFixes.ChargerLignesSecondairesSelonIdLigne(oLigneFixe)

        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idLigneFixe")
        oDataTable.Columns.Add("numreroFixe")
        For Each oLigneSecondaire In oLigneFixes
            oDataTable.Rows.Add(oLigneSecondaire.nIdLigneFixe, oLigneSecondaire.sNumeroLigneFixe)
        Next

        If Not IsNothing(oControle) Then
            oControle.DataSource = oDataTable
            oControle.DataValueField = "idLigneFixe"
            oControle.DataTextField = "numreroFixe"
            oControle.DataBind()
        End If

    End Sub

    Private Sub ChargerListeCompteFacturant(ByVal oComptesFacturants As CompteFacturantsFixe, ByRef oClient As Client)
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("idCompteFacturant")
        oDataTable.Columns.Add("libelleCompteFacturant")
        For Each oCompteFacturant In oComptesFacturants
            oDataTable.Rows.Add(oCompteFacturant.nIdCompteFacturant, oCompteFacturant.sLibelleCompteFacturant)
        Next

        Dim oControle As DropDownList = CType(Page.FindControl("DdlCompteFacturant"), DropDownList)

        If Not oControle Is Nothing Then

            oControle.DataSource = oDataTable
            oControle.DataValueField = "idCompteFacturant"
            oControle.DataTextField = "libelleCompteFacturant"
            oControle.DataBind()

            If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
                Try
                    oControle.SelectedValue = Page.Request(oControle.ID).ToString
                Catch
                End Try
            Else
                If oLigneFixe.oCompteFacturantFixe.nIdCompteFacturant <> 0 Then
                    oControle.SelectedValue = oLigneFixe.oCompteFacturantFixe.nIdCompteFacturant.ToString
                End If
            End If
        End If
    End Sub


    'Private Sub ChargerListeCompteFacturantInternet(ByVal oComptesFacturants As CompteFacturantsFixe, ByRef oClient As Client)
    '    Dim oDataTable As New DataTable
    '    oDataTable.Columns.Add("idCompteFacturant")
    '    oDataTable.Columns.Add("libelleCompteFacturant")
    '    For Each oCompteFacturant In oComptesFacturants
    '        oDataTable.Rows.Add(oCompteFacturant.nIdCompteFacturant, oCompteFacturant.sLibelleCompteFacturant)
    '    Next

    '    Dim oControle As DropDownList = CType(Page.FindControl("DdlCompteFacturantInternet"), DropDownList)

    '    If Not oControle Is Nothing Then

    '        oControle.DataSource = oDataTable
    '        oControle.DataValueField = "idCompteFacturant"
    '        oControle.DataTextField = "libelleCompteFacturant"
    '        oControle.DataBind()

    '        If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
    '            Try
    '                oControle.SelectedValue = Page.Request(oControle.ID).ToString
    '            Catch
    '            End Try
    '        Else
    '            If oLigneFixe.oCompteFacturantFixe.nIdCompteFacturant <> 0 Then
    '                oControle.SelectedValue = oLigneFixe.oCompteFacturantFixe.nIdCompteFacturant.ToString
    '            End If
    '        End If
    '    End If
    'End Sub




    Private Sub chargerListeContactInstallation(ByRef oContactInstallationsFixes As ContactInstallationFixes, ByRef oClient As Client)
        If Not Page.FindControl("DdlNom") Is Nothing Then
            Dim oControle As DropDownList = CType(Page.FindControl("DdlNom"), DropDownList)

            oControle.DataSource = oContactInstallationsFixes
            oControle.DataValueField = "nIdContact"
            oControle.DataTextField = "sNomContact"
            oControle.DataBind()

            DdlNom_SelectedIndexChanged(Me, EventArgs.Empty)

            If Not Page.Request(oControle.ID) Is Nothing AndAlso Page.Request(oControle.ID).ToString <> "" Then
                Try
                    oControle.SelectedValue = Page.Request(oControle.ID).ToString
                Catch
                End Try
            End If
        End If
    End Sub

    Private Sub DdlNom_SelectedIndexChanged(sender As Object, e As EventArgs)
        ValoriserInformationsContact()
    End Sub

    Private Sub ValoriserInformationsContact()
        Dim oDdlNom As DropDownList = CType(Page.FindControl("DdlNom"), DropDownList)
        Dim oTxtMobile As TextBox = CType(Page.FindControl("TxtMobile"), TextBox)
        Dim oTxtFixe As TextBox = CType(Page.FindControl("TxtFixe"), TextBox)
        Dim oTxtEmail As TextBox = CType(Page.FindControl("TxtEmail"), TextBox)

        If Not oDdlNom Is Nothing Then
            Dim oContactInstallationFixe As New ContactInstallationFixe()
            If Not oDdlNom.SelectedValue Is String.Empty Then
                oContactInstallationFixe.nIdContact = CInt(oDdlNom.SelectedValue)
                oContactInstallationFixe.chargerContactInstallationFixe()

                oTxtMobile.Text = Trim(oContactInstallationFixe.sMobileContact)
                oTxtFixe.Text = Trim(oContactInstallationFixe.sFixeContact)
                oTxtEmail.Text = Trim(oContactInstallationFixe.sEmailContact)
            Else
                oTxtMobile.Text = ""
                oTxtFixe.Text = ""
                oTxtEmail.Text = ""
            End If
        End If
    End Sub

    Private Sub DdlHierarchie6_SelectedIndexChanged(sender As Object, e As EventArgs)
        ValoriserInformationsAdresse()
    End Sub

    Private Sub ValoriserInformationsAdresse()
        Dim DdlHierarchie6 As DropDownList = CType(Page.FindControl("DdlHierarchie6"), DropDownList)
        Dim oTxtNomEtablissement As TextBox = CType(Page.FindControl("TxtNomEtablissement"), TextBox)
        Dim oTxtAdresse As TextBox = CType(Page.FindControl("TxtAdresse"), TextBox)
        Dim oTxtcodePostal As TextBox = CType(Page.FindControl("TxtcodePostal"), TextBox)
        Dim oTxtlocalite As TextBox = CType(Page.FindControl("Txtlocalite"), TextBox)

        If Not oTxtNomEtablissement Is Nothing Then
            Dim oAdresseInstallationFixe As New AdresseInstallationFixe()
            If Not oTxtNomEtablissement Is Nothing Then
                If Not DdlHierarchie6.SelectedValue Is String.Empty Then
                    oAdresseInstallationFixe.sCodeSite = CStr(DdlHierarchie6.SelectedValue)
                    oAdresseInstallationFixe.chargerAdresseInstallationFixeParCodeSite()

                    oTxtNomEtablissement.Text = Trim(oAdresseInstallationFixe.sNomSite)
                    oTxtAdresse.Text = Trim(oAdresseInstallationFixe.sAdresseSite)
                    oTxtcodePostal.Text = Trim(oAdresseInstallationFixe.sCodePostal)
                    oTxtlocalite.Text = Trim(oAdresseInstallationFixe.sLocalite)
                Else
                    oTxtNomEtablissement.Text = ""
                    oTxtAdresse.Text = ""
                    oTxtcodePostal.Text = ""
                    oTxtlocalite.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub ChargerImageAction(ByVal sTypeAction As String)
        Dim strFile As System.IO.FileInfo
        Dim sNomFichier As String = ""

        Select Case sTypeAction
            Case "insert"
                sNomFichier = "ico_Ajouter.png"
            Case "edit"
                sNomFichier = "ico_Modifier.png"
            Case "visu", "visucomplet"
                sNomFichier = "ico_Loupe.png"
            Case "delete"
                sNomFichier = "ico_supprimer.png"
        End Select

        strFile = New System.IO.FileInfo(Server.MapPath("images/icones/" & sNomFichier))
        If strFile.Exists Then
            Me.ImageAction.ImageUrl = "images/icones/" & sNomFichier
        End If
        ImageAction.Visible = True
    End Sub

    Private Sub DdlTypeLigne_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DdlTypeLigne.SelectedIndexChanged

        ChargerListes()

        'ChargerListeContrat()
        'ChargerListeTypeSelectionPermanente()



        'If Not Page.FindControl("DdlTypeMail") Is Nothing Then ChargerListeTypeMail()
        'If Not Page.FindControl("DdlCompteFacturantInternet") Is Nothing Then
        '    Dim oClient As New Client(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        '    Dim oComptesFacturantsFixe As New CompteFacturantsFixe
        '    oComptesFacturantsFixe.chargerCompteFacturantsFixe(oClient)
        '    ChargerListeCompteFacturantInternet(oComptesFacturantsFixe, oClient)
        '    ChargerCentreFraisInternet()
        'End If

    End Sub

    Private Sub ChargerControles()
        Dim oDropDownList As New DropDownList()
        Dim oTextBox As New TextBox()

        Dim oParametragesControlesFixe As New ParametragesControlesFixe
        oParametragesControlesFixe.chargerParametragesControlesFixe(CInt(User.Identity.Name.Split(CChar("_"))(1)))

        Dim oListeFiltree = From oParametrageControleFixe In oParametragesControlesFixe Where oParametrageControleFixe.sTypeLigne = Me.DdlTypeLigne.SelectedItem.Text And oParametrageControleFixe.sCas = sTypeModifFixe

        For Each oParametrageControle In oListeFiltree
            Select Case oParametrageControle.sTypeControle
                Case "DropdownList"
                    AjouterDropDownList(oParametrageControle)
                Case "Textbox"
                    AjouterTextbox(oParametrageControle)
                Case "Commentaire"
                    AjouterCommentaire(oParametrageControle)
                Case "Label"
                    AjouterLabelSeul(oParametrageControle)
                Case "Checkbox"
                    AjouterCheckbox(oParametrageControle)
                Case "ListBox"
                    AjouterListBox(oParametrageControle)
            End Select
        Next
    End Sub

    Private Sub AjouterListBox(ByRef oParametrageControleFixe As ParametrageControleFixe)
        Dim oPlaceHolder As PlaceHolder = CType(Page.FindControl(oParametrageControleFixe.sPlaceholder), PlaceHolder)

        Dim oLabelTemp As New Label
        oLabelTemp.ID = "Lbl" & oParametrageControleFixe.sIdControle
        oLabelTemp.CssClass = "champs"
        oLabelTemp.Text = oParametrageControleFixe.sTextLabel
        oLabelTemp.Attributes.Add("style", "position: absolute; height: " & oParametrageControleFixe.sHeightLabel & "px; width: " & oParametrageControleFixe.sWidthLabel & "px; left: " & oParametrageControleFixe.sLeftLabel & "px; top: " & oParametrageControleFixe.sTopControle & "px")

        If Not Page.FindControl(oLabelTemp.ID) Is Nothing Then Page.Controls.Remove(Page.FindControl(oLabelTemp.ID))
        oPlaceHolder.Controls.Add(oLabelTemp)

        Dim oListeView As New ListBox()
        oListeView.ID = "Lst" & oParametrageControleFixe.sIdControle
        oListeView.Attributes.Add("style", "Font-Size:12px;Color:#206797;position: absolute; width: " & oParametrageControleFixe.sWidthControle & "px; height: " & oParametrageControleFixe.sHeightControle & "px; left: " & oParametrageControleFixe.sLeftControle & "px; top: " & oParametrageControleFixe.sTopControle & "px")

        If (Not oParametrageControleFixe.bVerrouille = Nothing) AndAlso (oParametrageControleFixe.bVerrouille = True) Then
            oListeView.Enabled = False
        End If

        If Not Page.FindControl(oListeView.ID) Is Nothing Then Page.Controls.Remove(Page.FindControl(oListeView.ID))
        oPlaceHolder.Controls.Add(oListeView)

    End Sub

    Private Sub AjouterDropDownList(ByRef oParametrageControleFixe As ParametrageControleFixe)
        Dim oPlaceHolder As PlaceHolder = CType(Page.FindControl(oParametrageControleFixe.sPlaceholder), PlaceHolder)

        Dim oLabelTemp As New Label
        oLabelTemp.ID = "Lbl" & oParametrageControleFixe.sIdControle
        oLabelTemp.CssClass = "champs"
        oLabelTemp.Text = oParametrageControleFixe.sTextLabel
        oLabelTemp.Attributes.Add("style", "position: absolute; height: " & oParametrageControleFixe.sHeightLabel & "px; width: " & oParametrageControleFixe.sWidthLabel & "px; left: " & oParametrageControleFixe.sLeftLabel & "px; top: " & oParametrageControleFixe.sTopControle & "px")

        If Not Page.FindControl(oLabelTemp.ID) Is Nothing Then Page.Controls.Remove(Page.FindControl(oLabelTemp.ID))
        oPlaceHolder.Controls.Add(oLabelTemp)

        Dim oDropDownList As New DropDownList
        oDropDownList.ID = "Ddl" & oParametrageControleFixe.sIdControle
        oDropDownList.AutoPostBack = oParametrageControleFixe.bAutoPostBack
        oDropDownList.Attributes.Add("style", "Font-Size:12px;Color:#206797;position: absolute; width: " & oParametrageControleFixe.sWidthControle & "px; height: " & oParametrageControleFixe.sHeightControle & "px; left: " & oParametrageControleFixe.sLeftControle & "px; top: " & oParametrageControleFixe.sTopControle & "px")

        If (Not oParametrageControleFixe.bVerrouille = Nothing) AndAlso (oParametrageControleFixe.bVerrouille = True) Then
            oDropDownList.Enabled = False
        End If

        If Not Page.FindControl(oDropDownList.ID) Is Nothing Then Page.Controls.Remove(Page.FindControl(oDropDownList.ID))
        oPlaceHolder.Controls.Add(oDropDownList)

        If oDropDownList.ID = "DdlHierarchie1" Then AddHandler oDropDownList.SelectedIndexChanged, AddressOf DdlHierarchie1_SelectedIndexChanged
        If oDropDownList.ID = "DdlHierarchie2" Then AddHandler oDropDownList.SelectedIndexChanged, AddressOf DdlHierarchie2_SelectedIndexChanged
        If oDropDownList.ID = "DdlHierarchie3" Then AddHandler oDropDownList.SelectedIndexChanged, AddressOf DdlHierarchie3_SelectedIndexChanged
        If oDropDownList.ID = "DdlHierarchie4" Then AddHandler oDropDownList.SelectedIndexChanged, AddressOf DdlHierarchie4_SelectedIndexChanged
        If oDropDownList.ID = "DdlHierarchie5" Then AddHandler oDropDownList.SelectedIndexChanged, AddressOf DdlHierarchie5_SelectedIndexChanged
        If oDropDownList.ID = "DdlCompteFacturant" Then AddHandler oDropDownList.SelectedIndexChanged, AddressOf DdlCompteFacturant_SelectedIndexChanged

        If oDropDownList.ID = "DdlNom" Then AddHandler oDropDownList.SelectedIndexChanged, AddressOf DdlNom_SelectedIndexChanged
        If oDropDownList.ID = "DdlHierarchie6" Then AddHandler oDropDownList.SelectedIndexChanged, AddressOf DdlHierarchie6_SelectedIndexChanged

    End Sub

    Private Sub AjouterTextbox(ByRef oParametrageControleFixe As ParametrageControleFixe)
        Dim oPlaceHolder As PlaceHolder = CType(Page.FindControl(oParametrageControleFixe.sPlaceholder), PlaceHolder)

        Dim oLabelTemp As New Label
        oLabelTemp.ID = "Lbl" & oParametrageControleFixe.sIdControle
        oLabelTemp.CssClass = "champs"
        oLabelTemp.Text = oParametrageControleFixe.sTextLabel
        oLabelTemp.Attributes.Add("style", "position: absolute; height: " & oParametrageControleFixe.sHeightLabel & "px; width: " & oParametrageControleFixe.sWidthLabel & "px; left: " & oParametrageControleFixe.sLeftLabel & "px; top: " & oParametrageControleFixe.sTopControle & "px")


        If Not Page.FindControl(oLabelTemp.ID) Is Nothing Then Page.Controls.Remove(Page.FindControl(oLabelTemp.ID))
        oPlaceHolder.Controls.Add(oLabelTemp)

        Dim oTextBox As New TextBox
        oTextBox.ID = "Txt" & oParametrageControleFixe.sIdControle
        oTextBox.AutoPostBack = False
        oTextBox.Attributes.Add("style", "Font-Size:12px;Color:#206797;position: absolute; width: " & oParametrageControleFixe.sWidthControle & "px; height: " & oParametrageControleFixe.sHeightControle & "px; left: " & oParametrageControleFixe.sLeftControle & "px; top: " & oParametrageControleFixe.sTopControle & "px")

        If oParametrageControleFixe.sTypeAttendu = "Nombre" Then
            oTextBox.Attributes("type") = "number"
            oTextBox.Text = "0"
        End If

        If oParametrageControleFixe.sIdControle = "NbAcces" Then oTextBox.Text = "1"


        If (Not oParametrageControleFixe.bVerrouille = Nothing) AndAlso (oParametrageControleFixe.bVerrouille = True) Then
            oTextBox.Enabled = False
        End If

        If Not Page.FindControl(oTextBox.ID) Is Nothing Then Page.Controls.Remove(Page.FindControl(oTextBox.ID))
        oPlaceHolder.Controls.Add(oTextBox)
    End Sub

    Private Sub AjouterCommentaire(ByRef oParametrageControleFixe As ParametrageControleFixe)
        Dim oPlaceHolder As PlaceHolder = CType(Page.FindControl(oParametrageControleFixe.sPlaceholder), PlaceHolder)

        Dim oLabelTemp As New Label
        oLabelTemp.ID = "Lbl" & oParametrageControleFixe.sIdControle
        oLabelTemp.CssClass = "champs"
        oLabelTemp.Text = oParametrageControleFixe.sTextLabel
        oLabelTemp.Attributes.Add("style", "position: absolute; height: " & oParametrageControleFixe.sHeightLabel & "px; width: " & oParametrageControleFixe.sWidthLabel & "px; left: " & oParametrageControleFixe.sLeftLabel & "px; top: " & oParametrageControleFixe.sTopControle & "px")

        If Not Page.FindControl(oLabelTemp.ID) Is Nothing Then Page.Controls.Remove(Page.FindControl(oLabelTemp.ID))
        oPlaceHolder.Controls.Add(oLabelTemp)

        Dim oTextBox As New TextBox
        oTextBox.ID = "Txt" & oParametrageControleFixe.sIdControle
        oTextBox.TextMode = TextBoxMode.MultiLine
        oTextBox.AutoPostBack = False
        oTextBox.CausesValidation = False
        oTextBox.Attributes.Add("style", "Font-Size:12px;Color:#206797;position: absolute; width: " & oParametrageControleFixe.sWidthControle & "px; height: " & oParametrageControleFixe.sHeightControle & "px; left: " & oParametrageControleFixe.sLeftControle & "px; top: " & oParametrageControleFixe.sTopControle & "px")

        If (Not oParametrageControleFixe.bVerrouille = Nothing) AndAlso (oParametrageControleFixe.bVerrouille = True) Then
            oTextBox.Enabled = False
        End If

        If Not Page.FindControl(oTextBox.ID) Is Nothing Then Page.Controls.Remove(Page.FindControl(oTextBox.ID))
        oPlaceHolder.Controls.Add(oTextBox)
    End Sub

    Private Sub AjouterCheckbox(ByRef oParametrageControleFixe As ParametrageControleFixe)
        Dim oPlaceHolder As PlaceHolder = CType(Page.FindControl(oParametrageControleFixe.sPlaceholder), PlaceHolder)

        Dim oLabelTemp As New Label
        oLabelTemp.ID = "Lbl" & oParametrageControleFixe.sIdControle
        oLabelTemp.CssClass = "champs"
        oLabelTemp.Text = oParametrageControleFixe.sTextLabel
        oLabelTemp.Attributes.Add("style", "position: absolute; height: " & oParametrageControleFixe.sHeightLabel & "px; width: " & oParametrageControleFixe.sWidthLabel & "px; left: " & oParametrageControleFixe.sLeftLabel & "px; top: " & oParametrageControleFixe.sTopControle & "px")

        If Not Page.FindControl(oLabelTemp.ID) Is Nothing Then Page.Controls.Remove(Page.FindControl(oLabelTemp.ID))
        oPlaceHolder.Controls.Add(oLabelTemp)

        Dim oCheckBox As New CheckBox
        oCheckBox.ID = "Chk" & oParametrageControleFixe.sIdControle
        oCheckBox.AutoPostBack = False
        oCheckBox.Attributes.Add("style", "position: absolute; height: " & oParametrageControleFixe.sHeightControle & "px; left: " & oParametrageControleFixe.sLeftControle & "px; top: " & oParametrageControleFixe.sTopControle & "px")
        oCheckBox.Attributes.Add("Font-Size", "12px")
        oCheckBox.Attributes.Add("ForeColor", "#206797")

        If (Not oParametrageControleFixe.bVerrouille = Nothing) AndAlso (oParametrageControleFixe.bVerrouille = True) Then
            oCheckBox.Enabled = False
        End If

        If Not Page.FindControl(oCheckBox.ID) Is Nothing Then Page.Controls.Remove(Page.FindControl(oCheckBox.ID))
        oPlaceHolder.Controls.Add(oCheckBox)
    End Sub

    Private Sub AjouterLabelSeul(ByRef oParametrageControleFixe As ParametrageControleFixe)
        Dim oPlaceHolder As PlaceHolder = CType(Page.FindControl(oParametrageControleFixe.sPlaceholder), PlaceHolder)

        Dim oLabelTemp As New Label
        oLabelTemp.ID = "Lbl" & oParametrageControleFixe.sIdControle
        oLabelTemp.CssClass = "champs"
        oLabelTemp.Text = oParametrageControleFixe.sTextLabel
        oLabelTemp.Attributes.Add("style", "position: absolute;text-decoration:underline; height: " & oParametrageControleFixe.sHeightLabel & "px; width: " & oParametrageControleFixe.sWidthLabel & "px; left: " & oParametrageControleFixe.sLeftLabel & "px; top: " & oParametrageControleFixe.sTopControle & "px")

        If Not Page.FindControl(oLabelTemp.ID) Is Nothing Then Page.Controls.Remove(Page.FindControl(oLabelTemp.ID))
        oPlaceHolder.Controls.Add(oLabelTemp)
    End Sub


    Private Sub ChargerListeHierarchie1(ByVal oHierarchies As HierarchiesFixe)

        Dim DdlHierarchie1 As DropDownList = CType(Page.FindControl("DdlHierarchie1"), DropDownList)
        If DdlHierarchie1 Is Nothing Then Exit Sub

        Dim sListeHierarchie1 = From oHierarchie In oHierarchies Group oHierarchie By oHierarchie.sHierarchie1 Into TupleGroup = Group Select sHierarchie1 Order By sHierarchie1
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie1
            oDataTable.Rows.Add(sTexte)
        Next
        DdlHierarchie1.DataSource = oDataTable
        DdlHierarchie1.DataValueField = "libelle"
        DdlHierarchie1.DataTextField = "libelle"
        DdlHierarchie1.DataBind()

        If Not Page.Request(DdlHierarchie1.ID) Is Nothing AndAlso Page.Request(DdlHierarchie1.ID).ToString <> "" Then
            Try
                DdlHierarchie1.SelectedValue = Page.Request(DdlHierarchie1.ID).ToString
            Catch
            End Try
        End If

    End Sub

    Private Sub ChargerListeHierarchie2(ByVal oHierarchies As HierarchiesFixe)

        Dim DdlHierarchie1 As DropDownList = CType(Page.FindControl("DdlHierarchie1"), DropDownList)
        If DdlHierarchie1 Is Nothing Then Exit Sub
        Dim DdlHierarchie2 As DropDownList = CType(Page.FindControl("DdlHierarchie2"), DropDownList)
        If DdlHierarchie2 Is Nothing Then Exit Sub

        Dim sListeHierarchie2 = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = DdlHierarchie1.Text Group oHierarchie By oHierarchie.sHierarchie2 Into TupleGroup = Group Select sHierarchie2 Order By sHierarchie2
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie2
            oDataTable.Rows.Add(sTexte)
        Next
        DdlHierarchie2.DataSource = oDataTable
        DdlHierarchie2.DataValueField = "libelle"
        DdlHierarchie2.DataTextField = "libelle"
        DdlHierarchie2.DataBind()

        If Not Page.Request(DdlHierarchie2.ID) Is Nothing AndAlso Page.Request(DdlHierarchie2.ID).ToString <> "" Then
            Try
                DdlHierarchie2.SelectedValue = Page.Request(DdlHierarchie2.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeHierarchie3(ByVal oHierarchies As HierarchiesFixe)

        Dim DdlHierarchie1 As DropDownList = CType(Page.FindControl("DdlHierarchie1"), DropDownList)
        If DdlHierarchie1 Is Nothing Then Exit Sub
        Dim DdlHierarchie2 As DropDownList = CType(Page.FindControl("DdlHierarchie2"), DropDownList)
        If DdlHierarchie2 Is Nothing Then Exit Sub
        Dim DdlHierarchie3 As DropDownList = CType(Page.FindControl("DdlHierarchie3"), DropDownList)
        If DdlHierarchie3 Is Nothing Then Exit Sub


        Dim sListeHierarchie3 = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = DdlHierarchie1.Text And oHierarchie.sHierarchie2 = DdlHierarchie2.Text Group oHierarchie By oHierarchie.sHierarchie3 Into TupleGroup = Group Select sHierarchie3 Order By sHierarchie3
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie3
            oDataTable.Rows.Add(sTexte)
        Next
        DdlHierarchie3.DataSource = oDataTable
        DdlHierarchie3.DataValueField = "libelle"
        DdlHierarchie3.DataTextField = "libelle"
        DdlHierarchie3.DataBind()

        If Not Page.Request(DdlHierarchie3.ID) Is Nothing AndAlso Page.Request(DdlHierarchie3.ID).ToString <> "" Then
            Try
                DdlHierarchie3.SelectedValue = Page.Request(DdlHierarchie3.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeHierarchie4(ByVal oHierarchies As HierarchiesFixe)

        Dim DdlHierarchie1 As DropDownList = CType(Page.FindControl("DdlHierarchie1"), DropDownList)
        If DdlHierarchie1 Is Nothing Then Exit Sub
        Dim DdlHierarchie2 As DropDownList = CType(Page.FindControl("DdlHierarchie2"), DropDownList)
        If DdlHierarchie2 Is Nothing Then Exit Sub
        Dim DdlHierarchie3 As DropDownList = CType(Page.FindControl("DdlHierarchie3"), DropDownList)
        If DdlHierarchie3 Is Nothing Then Exit Sub
        Dim DdlHierarchie4 As DropDownList = CType(Page.FindControl("DdlHierarchie4"), DropDownList)
        If DdlHierarchie4 Is Nothing Then Exit Sub

        Dim sListeHierarchie4 = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = DdlHierarchie1.Text And oHierarchie.sHierarchie2 = DdlHierarchie2.Text And oHierarchie.sHierarchie3 = DdlHierarchie3.Text Group oHierarchie By oHierarchie.sHierarchie4 Into TupleGroup = Group Select sHierarchie4 Order By sHierarchie4
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie4
            oDataTable.Rows.Add(sTexte)
        Next
        DdlHierarchie4.DataSource = oDataTable
        DdlHierarchie4.DataValueField = "libelle"
        DdlHierarchie4.DataTextField = "libelle"
        DdlHierarchie4.DataBind()

        If Not Page.Request(DdlHierarchie4.ID) Is Nothing AndAlso Page.Request(DdlHierarchie4.ID).ToString <> "" Then
            Try
                DdlHierarchie4.SelectedValue = Page.Request(DdlHierarchie4.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeHierarchie5(ByVal oHierarchies As HierarchiesFixe)

        Dim DdlHierarchie1 As DropDownList = CType(Page.FindControl("DdlHierarchie1"), DropDownList)
        If DdlHierarchie1 Is Nothing Then Exit Sub
        Dim DdlHierarchie2 As DropDownList = CType(Page.FindControl("DdlHierarchie2"), DropDownList)
        If DdlHierarchie2 Is Nothing Then Exit Sub
        Dim DdlHierarchie3 As DropDownList = CType(Page.FindControl("DdlHierarchie3"), DropDownList)
        If DdlHierarchie3 Is Nothing Then Exit Sub
        Dim DdlHierarchie4 As DropDownList = CType(Page.FindControl("DdlHierarchie4"), DropDownList)
        If DdlHierarchie4 Is Nothing Then Exit Sub
        Dim DdlHierarchie5 As DropDownList = CType(Page.FindControl("DdlHierarchie5"), DropDownList)
        If DdlHierarchie5 Is Nothing Then Exit Sub

        Dim sListeHierarchie5 = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = DdlHierarchie1.Text And oHierarchie.sHierarchie2 = DdlHierarchie2.Text And oHierarchie.sHierarchie3 = DdlHierarchie3.Text And oHierarchie.sHierarchie4 = DdlHierarchie4.Text Group oHierarchie By oHierarchie.sHierarchie5 Into TupleGroup = Group Select sHierarchie5 Order By sHierarchie5
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie5
            oDataTable.Rows.Add(sTexte)
        Next
        DdlHierarchie5.DataSource = oDataTable
        DdlHierarchie5.DataValueField = "libelle"
        DdlHierarchie5.DataTextField = "libelle"
        DdlHierarchie5.DataBind()

        If Not Page.Request(DdlHierarchie5.ID) Is Nothing AndAlso Page.Request(DdlHierarchie5.ID).ToString <> "" Then
            Try
                DdlHierarchie5.SelectedValue = Page.Request(DdlHierarchie5.ID).ToString
            Catch
            End Try
        End If

    End Sub
    Private Sub ChargerListeHierarchie6(ByVal oHierarchies As HierarchiesFixe)

        Dim DdlHierarchie1 As DropDownList = CType(Page.FindControl("DdlHierarchie1"), DropDownList)
        If DdlHierarchie1 Is Nothing Then Exit Sub
        Dim DdlHierarchie2 As DropDownList = CType(Page.FindControl("DdlHierarchie2"), DropDownList)
        If DdlHierarchie2 Is Nothing Then Exit Sub
        Dim DdlHierarchie3 As DropDownList = CType(Page.FindControl("DdlHierarchie3"), DropDownList)
        If DdlHierarchie3 Is Nothing Then Exit Sub
        Dim DdlHierarchie4 As DropDownList = CType(Page.FindControl("DdlHierarchie4"), DropDownList)
        If DdlHierarchie4 Is Nothing Then Exit Sub
        Dim DdlHierarchie5 As DropDownList = CType(Page.FindControl("DdlHierarchie5"), DropDownList)
        If DdlHierarchie5 Is Nothing Then Exit Sub
        Dim DdlHierarchie6 As DropDownList = CType(Page.FindControl("DdlHierarchie6"), DropDownList)
        If DdlHierarchie6 Is Nothing Then Exit Sub

        Dim sListeHierarchie6 = From oHierarchie In oHierarchies Where oHierarchie.sHierarchie1 = DdlHierarchie1.Text And oHierarchie.sHierarchie2 = DdlHierarchie2.Text And oHierarchie.sHierarchie3 = DdlHierarchie3.Text And oHierarchie.sHierarchie4 = DdlHierarchie4.Text And oHierarchie.sHierarchie5 = DdlHierarchie5.Text Group oHierarchie By oHierarchie.sHierarchie6 Into TupleGroup = Group Select sHierarchie6 Order By sHierarchie6
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("libelle")
        For Each sTexte In sListeHierarchie6
            oDataTable.Rows.Add(sTexte)
        Next
        DdlHierarchie6.DataSource = oDataTable
        DdlHierarchie6.DataValueField = "libelle"
        DdlHierarchie6.DataTextField = "libelle"
        DdlHierarchie6.DataBind()

        DdlHierarchie6_SelectedIndexChanged(Me, EventArgs.Empty)

        If Not Page.Request(DdlHierarchie6.ID) Is Nothing AndAlso Page.Request(DdlHierarchie6.ID).ToString <> "" Then
            Try
                DdlHierarchie6.SelectedValue = Page.Request(DdlHierarchie6.ID).ToString
            Catch
            End Try
        End If


    End Sub

    Private Sub DdlHierarchie1_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim oHierarchies As New HierarchiesFixe
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim oClient As New Client(nIdClient)
        oHierarchies.chargerHierarchiesFixe(oClient)
        ChargerListeHierarchie2(oHierarchies)
        ChargerListeHierarchie3(oHierarchies)
        ChargerListeHierarchie4(oHierarchies)
        ChargerListeHierarchie5(oHierarchies)
        ChargerListeHierarchie6(oHierarchies)
    End Sub
    Private Sub DdlHierarchie2_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim oHierarchies As New HierarchiesFixe
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim oClient As New Client(nIdClient)
        oHierarchies.chargerHierarchiesFixe(oClient)
        ChargerListeHierarchie3(oHierarchies)
        ChargerListeHierarchie4(oHierarchies)
        ChargerListeHierarchie5(oHierarchies)
        ChargerListeHierarchie6(oHierarchies)
    End Sub
    Private Sub DdlHierarchie3_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim oHierarchies As New HierarchiesFixe
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim oClient As New Client(nIdClient)
        oHierarchies.chargerHierarchiesFixe(oClient)
        ChargerListeHierarchie4(oHierarchies)
        ChargerListeHierarchie5(oHierarchies)
        ChargerListeHierarchie6(oHierarchies)
    End Sub
    Private Sub DdlHierarchie4_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim oHierarchies As New HierarchiesFixe
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim oClient As New Client(nIdClient)
        oHierarchies.chargerHierarchiesFixe(oClient)
        ChargerListeHierarchie5(oHierarchies)
        ChargerListeHierarchie6(oHierarchies)
    End Sub
    Private Sub DdlHierarchie5_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim oHierarchies As New HierarchiesFixe
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim oClient As New Client(nIdClient)
        oHierarchies.chargerHierarchiesFixe(oClient)
        ChargerListeHierarchie6(oHierarchies)
    End Sub
    Private Sub DdlCompteFacturant_SelectedIndexChanged(sender As Object, e As EventArgs)
        ChargerCentreFrais()
    End Sub

    Private Sub ChargerCentreFrais()
        Dim oCompteFacturant As New CompteFacturantFixe
        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))

        If Page.FindControl("DdlCompteFacturant") Is Nothing Then Exit Sub

        oCompteFacturant.chargerCompteFacturantFixeParId(CInt(CType(Page.FindControl("DdlCompteFacturant"), DropDownList).SelectedItem.Value))
        Dim oControle As TextBox
        oControle = CType(Page.FindControl("TxtCentreFrais"), TextBox)
        oControle.Text = oCompteFacturant.sLibelleCentreFrais
    End Sub

    'Private Sub DdlCompteFacturantInternet_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    ChargerCentreFraisInternet()
    'End Sub

    'Private Sub ChargerCentreFraisInternet()
    '    Dim oCompteFacturant As New CompteFacturantFixe
    '    Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))

    '    If Page.FindControl("DdlCompteFacturantInternet") Is Nothing Then Exit Sub

    '    oCompteFacturant.chargerCompteFacturantFixeParId(CInt(CType(Page.FindControl("DdlCompteFacturantInternet"), DropDownList).SelectedItem.Value))
    '    Dim oControle As TextBox
    '    oControle = CType(Page.FindControl("TxtCentreFraisInternet"), TextBox)
    '    oControle.Text = oCompteFacturant.sLibelleCentreFrais
    'End Sub

    Private Function TesterChamps(ByVal oParametragesControlesFixe As ParametragesControlesFixe) As String
        Dim sMessage As String = ""
        Dim oListeFiltree = From oParametrageControleFixe In oParametragesControlesFixe Where oParametrageControleFixe.sTypeLigne = Me.DdlTypeLigne.SelectedItem.Text And oParametrageControleFixe.sCas = sTypeModifFixe
        For Each oParametrageControle In oListeFiltree
            If oParametrageControle.bAutorisationNull = False Then
                Select Case oParametrageControle.sTypeControle
                    Case "DropdownList"
                        If CType(Page.FindControl("Ddl" & oParametrageControle.sIdControle), DropDownList).Text = "" Then
                            sMessage += "Le champ " & oParametrageControle.sTextLabel & " de l'onglet " & RecupererNomOnglet(oParametrageControle.sPlaceholder) & " ne doit pas être vide\n"
                        End If
                    Case "Textbox"
                        If CType(Page.FindControl("Txt" & oParametrageControle.sIdControle), TextBox).Text = "" Then
                            sMessage += "Le champ " & oParametrageControle.sTextLabel & " de l'onglet " & RecupererNomOnglet(oParametrageControle.sPlaceholder) & " ne doit pas être vide\n"
                        End If
                End Select
            End If
            If oParametrageControle.sTypeAttendu = "Nombre" Then
                If oParametrageControle.sTypeControle = "Textbox" Then
                    If Not IsNumeric(CType(Page.FindControl("Txt" & oParametrageControle.sIdControle), TextBox).Text) Or CType(Page.FindControl("Txt" & oParametrageControle.sIdControle), TextBox).Text.Contains(".") Or CType(Page.FindControl("Txt" & oParametrageControle.sIdControle), TextBox).Text.Contains(",") Then
                        sMessage += "Le champ " & oParametrageControle.sTextLabel & " de l'onglet " & RecupererNomOnglet(oParametrageControle.sPlaceholder) & " doit être un entier\n"
                    End If
                End If
            End If
            If oParametrageControle.sTypeAttendu = "Date" Then
                If oParametrageControle.sTypeControle = "Textbox" Then
                    If Not IsDate(CType(Page.FindControl("Txt" & oParametrageControle.sIdControle), TextBox).Text) Or CType(Page.FindControl("Txt" & oParametrageControle.sIdControle), TextBox).Text = "" Then
                        sMessage += "Le champ " & oParametrageControle.sTextLabel & " de l'onglet " & RecupererNomOnglet(oParametrageControle.sPlaceholder) & " doit être une date\n"
                    End If
                End If
            End If
        Next
        Return sMessage
    End Function

    Private Function RecupererNomOnglet(ByVal sPlaceholder As String) As String
        Dim sResultat As String = ""
        Select Case CInt(sPlaceholder.Replace("PlaceHolder", String.Empty).Split(CChar("_"))(0))
            Case 1
                sResultat = "Accès au réseau"
            Case 2
                sResultat = "Facturation"
            Case 3
                sResultat = "Informations d'installation"
            Case 4
                sResultat = "Dates"
            Case 5
                sResultat = "Options"
            Case 6
                sResultat = "Commentaires"
        End Select
        Return sResultat
    End Function

    Private Sub BtnInsertValider_Click(sender As Object, e As EventArgs) Handles BtnInsertValider.Click
        Dim oNouvelleLigneFixe As New LigneFixe


        Try
            oNouvelleLigneFixe.oHierarchieFixe = New HierarchieFixe
            oNouvelleLigneFixe.oCompteFacturantFixe = New CompteFacturantFixe
            oNouvelleLigneFixe.oUsageFixe = New UsageFixe
            oNouvelleLigneFixe.oTypeLigneFixe = New TypeLigneFixe
            oNouvelleLigneFixe.oContratFixe = New ContratFixe
            oNouvelleLigneFixe.oTypeSelectionFixe = New TypeSelectionFixe
            oNouvelleLigneFixe.oChoixListeFixe = New ChoixListeFixe




            oNouvelleLigneFixe.sNumeroLigneFixe = Me.TxtNumeroGSM.Text
            oNouvelleLigneFixe.oUsageFixe.chargerUsageFixeParId(CInt(Me.DdlUsage.SelectedValue), CInt(User.Identity.Name.Split(CChar("_"))(1)))
            oNouvelleLigneFixe.oTypeLigneFixe.chargerTypeLigneFixeParId(CInt(Me.DdlTypeLigne.SelectedValue))
            oNouvelleLigneFixe.dDateActivation = CDate(Me.TxtDateActivation.Text)
            oNouvelleLigneFixe.nIdClient = CInt(User.Identity.Name.Split(CChar("_"))(1))



            If sTypeModifFixe = "insert" Then oNouvelleLigneFixe.sNumeroLigneFixe = "-"

            If sTypeModifFixe = "edit" Or sTypeModifFixe = "delete" Then
                oNouvelleLigneFixe.nIdLigneFixe = oLigneFixe.nIdLigneFixe

            End If
            Dim oParametragesControlesFixe As New ParametragesControlesFixe
            oParametragesControlesFixe.chargerParametragesControlesFixe(CInt(User.Identity.Name.Split(CChar("_"))(1)))

            Dim sTexteValidation As String = TesterChamps(oParametragesControlesFixe)


            If sTexteValidation <> "" Then
                Me.TextePopup.Value = sTexteValidation
                Exit Sub
            End If


            RemplirDonneesForulaire(oParametragesControlesFixe, oNouvelleLigneFixe, "Fixe")

            oNouvelleLigneFixe.oHierarchieFixe.chargerHierarchieFixeParId(RecupererIdHierarchie)



            Dim nIdTeteLigne As Integer = 0

            Select Case sTypeModifFixe
                Case "insert"

                    Dim oControleChkInternet As CheckBox = CType(Page.FindControl("ChkLigneInternet"), CheckBox)
                    Dim bCommanderInternet As Boolean = False
                    If Not oControleChkInternet Is Nothing AndAlso oControleChkInternet.Checked = True Then bCommanderInternet = True

                    If oNouvelleLigneFixe.nNombreAcces > 1 Then
                        For i = 1 To oNouvelleLigneFixe.nNombreAcces
                            oNouvelleLigneFixe.insererLigneFixe()
                            CreerCommande(oNouvelleLigneFixe, "creation")




                            If nIdTeteLigne = 0 And (oNouvelleLigneFixe.oTypeLigneFixe.sLibelleTypeLigne = "Ligne Numéris accès de base" Or oNouvelleLigneFixe.oTypeLigneFixe.sLibelleTypeLigne = "Ligne Numéris accès primaire") Then nIdTeteLigne = oNouvelleLigneFixe.nIdLigneFixe
                            If nIdTeteLigne <> 0 Then
                                oNouvelleLigneFixe.nIdTeteLigne = nIdTeteLigne
                                oNouvelleLigneFixe.editerLigneFixe()
                            End If
                        Next
                    Else
                        oNouvelleLigneFixe.insererLigneFixe()
                        CreerCommande(oNouvelleLigneFixe, "creation")



                    End If
                Case "edit"


                    MettreANothingValeursIdentiques(oNouvelleLigneFixe)
                    If oLigneFixe.oTypeLigneFixe.sLibelleTypeLigne = "Ligne Numéris accès de base" Or oLigneFixe.oTypeLigneFixe.sLibelleTypeLigne = "Ligne Numéris accès primaire" Then
                        Dim oLignesSecondaires As New LigneFixes
                        oLignesSecondaires.ChargerLignesSecondairesSelonIdLigne(oNouvelleLigneFixe)

                        For Each oLigneSecondaire In oLignesSecondaires
                            Dim oLigneFixeTemp As New LigneFixe
                            oLigneFixeTemp = oNouvelleLigneFixe
                            oLigneFixeTemp.nIdLigneFixe = oLigneSecondaire.nIdLigneFixe

                            ' PROCEDURE QUI INSERE DANS HISTORIQUECOMMANDE ET QUI MET A JOUR/INSERE DANS COMMANDE
                            CreerHistoriqueCommande(oLigneFixeTemp)
                            ' FIN

                            RemplirDonneesForulaire(oParametragesControlesFixe, oLigneFixeTemp, "Fixe")
                            oLigneFixeTemp.editerLigneFixe()
                            CreerCommande(oLigneFixeTemp, "modification")
                            oNouvelleLigneFixe.nIdLigneFixe = oLigneFixe.nIdTeteLigne
                        Next
                        ' PROCEDURE QUI INSERE DANS HISTORIQUECOMMANDE ET QUI MET A JOUR/INSERE DANS COMMANDE
                        CreerHistoriqueCommande(oLigneFixe)
                        ' FIN
                        oNouvelleLigneFixe.editerLigneFixe()
                        CreerCommande(oNouvelleLigneFixe, "modification")
                    Else
                        ' PROCEDURE QUI INSERE DANS HISTORIQUECOMMANDE ET QUI MET A JOUR/INSERE DANS COMMANDE
                        CreerHistoriqueCommande(oLigneFixe)
                        ' FIN
                        oNouvelleLigneFixe.editerLigneFixe()
                        CreerCommande(oNouvelleLigneFixe, "modification")
                    End If


                Case "delete"

                    If oNouvelleLigneFixe.oTypeLigneFixe.sLibelleTypeLigne = "Ligne Numéris accès de base" Or oNouvelleLigneFixe.oTypeLigneFixe.sLibelleTypeLigne = "Ligne Numéris accès primaire" Then
                        Dim oControleChk As CheckBox = CType(Page.FindControl("ChkResiliation"), CheckBox)
                        Dim oControleLst As ListBox = CType(Page.FindControl("LstLignesSecondaires"), ListBox)
                        If oControleChk.Checked = True Then

                            If oControleLst.GetSelectedIndices.Count = 0 Then
                                Me.TextePopup.Value = "Aucune liste secondaire n'est sélectionnée\n"
                                Exit Sub
                            End If

                            Dim oLigneSecondaire As New LigneFixe
                            oLigneSecondaire.nIdLigneFixe = CInt(oControleLst.SelectedValue)
                            oLigneSecondaire.chargerLigneFixeParId()
                            RemplirDonneesForulaire(oParametragesControlesFixe, oLigneSecondaire, "Fixe")
                            oLigneSecondaire.supprimerLigneFixe()
                            CreerCommande(oLigneSecondaire, "suppression", True)
                            If IsNumeric(oNouvelleLigneFixe.nNombreAcces) Then
                                Dim oLignesSecondaires As New LigneFixes
                                oLignesSecondaires.ChargerLignesSecondairesSelonIdLigne(oNouvelleLigneFixe)
                                For Each oLigneSecondaire In oLignesSecondaires
                                    oLigneSecondaire.nNombreAcces = oLigneSecondaire.nNombreAcces - 1
                                    oLigneSecondaire.editerLigneFixe()
                                Next
                                oNouvelleLigneFixe.nNombreAcces = oNouvelleLigneFixe.nNombreAcces - 1
                                oNouvelleLigneFixe.editerLigneFixe()
                            End If
                        Else
                            Dim oLignesSecondaires As New LigneFixes
                            oLignesSecondaires.ChargerLignesSecondairesSelonIdLigne(oNouvelleLigneFixe)
                            For Each oLigneSecondaire In oLignesSecondaires
                                RemplirDonneesForulaire(oParametragesControlesFixe, oLigneSecondaire, "Fixe")
                                oLigneSecondaire.supprimerLigneFixe()
                                CreerCommande(oLigneSecondaire, "suppression")
                            Next
                            oNouvelleLigneFixe.supprimerLigneFixe()
                            CreerCommande(oNouvelleLigneFixe, "suppression")
                        End If
                    Else
                        oNouvelleLigneFixe.supprimerLigneFixe()
                        CreerCommande(oNouvelleLigneFixe, "suppression")
                    End If

            End Select
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
        Catch ex As Exception
            Throw ex
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
        Finally

        End Try
    End Sub

    Private Sub CreerHistoriqueCommande(ByRef oLigneFixe As LigneFixe)
        Try
            Dim oHistoriqueCommandeFixe As New HistoriqueCommandeFixe()
            Dim oCommandeFixe As New CommandeFixe()

            oHistoriqueCommandeFixe.oCommandeFixe = oCommandeFixe
            oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe = oLigneFixe
            oHistoriqueCommandeFixe.oCommandeFixe.ChargerCommandeParIdLigne()

            ' S'IL N'Y A PAS D'IDCOMMANDE (CAR PAS DE COMMANDE SUR CETTE LIGNE) PRENDRE LES INFORMATIONS DANS LIGNETECHNIQUE
            If oHistoriqueCommandeFixe.oCommandeFixe.nIdCommande = 0 Then
                oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.chargerLigneFixeParId()
            End If

            'SAUVEGARDER L'HISTORIQUE
            oHistoriqueCommandeFixe.InsertionHistoriqueCommandeFixe()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub MettreANothingValeursIdentiques(oLigneAModifier As LigneFixe)
        If oLigneAModifier.bStTransfertAppelNonReponse = oLigneFixe.bStTransfertAppelNonReponse Then oLigneAModifier.bStTransfertAppelNonReponse = Nothing
        If oLigneAModifier.dDateActivation = oLigneFixe.dDateActivation Then oLigneAModifier.dDateActivation = Nothing
        If oLigneAModifier.dDateDesactivation = oLigneFixe.dDateDesactivation Then oLigneAModifier.dDateDesactivation = Nothing
        If oLigneAModifier.dDateDispositionLocaux = oLigneFixe.dDateDispositionLocaux Then oLigneAModifier.dDateDispositionLocaux = Nothing
        If oLigneAModifier.dDateMiseEnService = oLigneFixe.dDateMiseEnService Then oLigneAModifier.dDateMiseEnService = Nothing
        If oLigneAModifier.nNombreAcces = oLigneFixe.nNombreAcces Then oLigneAModifier.nNombreAcces = Nothing
        If oLigneAModifier.nNombreSDA = oLigneFixe.nNombreSDA Then oLigneAModifier.nNombreSDA = Nothing
        If oLigneAModifier.nSpecialisationCanauxArrive = oLigneFixe.nSpecialisationCanauxArrive Then oLigneAModifier.nSpecialisationCanauxArrive = Nothing
        If oLigneAModifier.nSpecialisationCanauxDepart = oLigneFixe.nSpecialisationCanauxDepart Then oLigneAModifier.nSpecialisationCanauxDepart = Nothing
        If oLigneAModifier.nSpecialisationCanauxMixte = oLigneFixe.nSpecialisationCanauxMixte Then oLigneAModifier.nSpecialisationCanauxMixte = Nothing
        If oLigneAModifier.oChoixListeFixe.nIdChoixListe = oLigneFixe.oChoixListeFixe.nIdChoixListe Then oLigneAModifier.oChoixListeFixe = New ChoixListeFixe
        If oLigneAModifier.oCompteFacturantFixe.nIdCompteFacturant = oLigneFixe.oCompteFacturantFixe.nIdCompteFacturant Then oLigneAModifier.oCompteFacturantFixe = New CompteFacturantFixe
        If oLigneAModifier.oContratFixe.nIdContrat = oLigneFixe.oContratFixe.nIdContrat Then oLigneAModifier.oContratFixe = New ContratFixe
        If oLigneAModifier.oHierarchieFixe.nIdHierarchie = oLigneFixe.oHierarchieFixe.nIdHierarchie Then oLigneAModifier.oHierarchieFixe = New HierarchieFixe
        If oLigneAModifier.oTypeSelectionFixe.nIdTypeSelection = oLigneFixe.oTypeSelectionFixe.nIdTypeSelection Then oLigneAModifier.oTypeSelectionFixe = New TypeSelectionFixe
        If oLigneAModifier.oTypeLigneFixe.nIdTypeLigne = oLigneFixe.oTypeLigneFixe.nIdTypeLigne Then oLigneAModifier.oTypeLigneFixe = New TypeLigneFixe
        If oLigneAModifier.oUsageFixe.nIdUsage = oLigneFixe.oUsageFixe.nIdUsage Then oLigneAModifier.oUsageFixe = New UsageFixe
        If oLigneAModifier.sAdresse = oLigneFixe.sAdresse Then oLigneAModifier.sAdresse = Nothing
        If oLigneAModifier.sAdresseEmail = oLigneFixe.sAdresseEmail Then oLigneAModifier.sAdresseEmail = Nothing
        If oLigneAModifier.sChoixModem = oLigneFixe.sChoixModem Then oLigneAModifier.sChoixModem = Nothing
        If oLigneAModifier.sCodePostal = oLigneFixe.sCodePostal Then oLigneAModifier.sCodePostal = Nothing
        If oLigneAModifier.sCommentaire = oLigneFixe.sCommentaire Then oLigneAModifier.sCommentaire = Nothing
        If oLigneAModifier.sComplementAdresse = oLigneFixe.sComplementAdresse Then oLigneAModifier.sComplementAdresse = Nothing
        If oLigneAModifier.sFixeContact = oLigneFixe.sFixeContact Then oLigneAModifier.sFixeContact = Nothing
        If oLigneAModifier.sIaPrecisionsComplementairesParution = oLigneFixe.sIaPrecisionsComplementairesParution Then oLigneAModifier.sIaPrecisionsComplementairesParution = Nothing
        If oLigneAModifier.sLocalite = oLigneFixe.sLocalite Then oLigneAModifier.sLocalite = Nothing
        If oLigneAModifier.sMailContact = oLigneFixe.sMailContact Then oLigneAModifier.sMailContact = Nothing
        If oLigneAModifier.sMobileContact = oLigneFixe.sMobileContact Then oLigneAModifier.sMobileContact = Nothing
        If oLigneAModifier.sNomContact = oLigneFixe.sNomContact Then oLigneAModifier.sNomContact = Nothing
        If oLigneAModifier.sNomEtablissement = oLigneFixe.sNomEtablissement Then oLigneAModifier.sNomEtablissement = Nothing
        If oLigneAModifier.sStNumeroRenvoi = oLigneFixe.sStNumeroRenvoi Then oLigneAModifier.sStNumeroRenvoi = Nothing
        If oLigneAModifier.sTypeAdresseEmail = oLigneFixe.sTypeAdresseEmail Then oLigneAModifier.sTypeAdresseEmail = Nothing
    End Sub

    Private Sub RemplirDonneesForulaire(ByVal oParametragesControlesFixe As ParametragesControlesFixe, ByVal oLigneFixe As LigneFixe, ByVal sTypeAcces As String)
        Dim oListeFiltree = From oParametrageControleFixe In oParametragesControlesFixe Where oParametrageControleFixe.sTypeLigne = Me.DdlTypeLigne.SelectedItem.Text And oParametrageControleFixe.sCas = sTypeModifFixe And oParametrageControleFixe.sNomMembre <> ""
        For Each oParametrageControle In oListeFiltree

            If sTypeAcces = "Fixe" Then
                Select Case oParametrageControle.sNomMembre

                    Case "bStRenvoiTerminal" : oLigneFixe.bStRenvoiTerminal = CType(Page.FindControl("ChkRenvoiTerminal"), CheckBox).Checked
                    Case "bStSecretPermanent" : oLigneFixe.bStSecretPermanent = CType(Page.FindControl("ChkSecretPermanent"), CheckBox).Checked
                    Case "bStTransfertAppelNonReponse" : oLigneFixe.bStTransfertAppelNonReponse = CType(Page.FindControl("ChkTransertAppelNonReponse"), CheckBox).Checked
                    Case "bStTransfertAppel" : oLigneFixe.bStTransfertAppel = CType(Page.FindControl("ChkTransertAppel"), CheckBox).Checked
                    Case "bStMessagerieVocale" : oLigneFixe.bStMessagerieVocale = CType(Page.FindControl("ChkMessagerieVocale"), CheckBox).Checked

                    Case "nSpecialisationCanauxArrive" : oLigneFixe.nSpecialisationCanauxArrive = CInt(CType(Page.FindControl("TxtArrivee"), TextBox).Text)
                    Case "nSpecialisationCanauxDepart" : oLigneFixe.nSpecialisationCanauxDepart = CInt(CType(Page.FindControl("TxtDepart"), TextBox).Text)
                    Case "nSpecialisationCanauxMixte" : oLigneFixe.nSpecialisationCanauxMixte = CInt(CType(Page.FindControl("TxtMixte"), TextBox).Text)
                    Case "nNombreAcces" : oLigneFixe.nNombreAcces = CInt(CType(Page.FindControl("TxtNbAcces"), TextBox).Text)
                    Case "nNombreSDA" : oLigneFixe.nNombreSDA = CInt(CType(Page.FindControl("TxtNbSDA"), TextBox).Text)
                    Case "sStNumeroRenvoi" : oLigneFixe.sStNumeroRenvoi = CType(Page.FindControl("TxtNumeroRenvoi"), TextBox).Text
                    Case "sIaPrecisionsComplementairesParution" : oLigneFixe.sIaPrecisionsComplementairesParution = CType(Page.FindControl("TxtPrecisionsParution"), TextBox).Text

                    Case "oCompteFacturantFixe.nIdCompteFacturant" : oLigneFixe.oCompteFacturantFixe.chargerCompteFacturantFixeParId(CInt(CType(Page.FindControl("DdlCompteFacturant"), DropDownList).SelectedValue))
                    Case "oContratFixe.nIdContrat" : oLigneFixe.oContratFixe.nIdContrat = CInt(CType(Page.FindControl("DdlContrat"), DropDownList).SelectedValue) : oLigneFixe.oContratFixe.chargerContratFixeParId()
                    Case "oTypeSelectionFixe.nIdTypeSelection" : oLigneFixe.oTypeSelectionFixe.nIdTypeSelection = CInt(CType(Page.FindControl("DdlTypeSelectionFixe"), DropDownList).SelectedValue) : oLigneFixe.oTypeSelectionFixe.chargerTypeSelectionFixeParId()
                    Case "oChoixListeFixe.nIdChoixListe" : oLigneFixe.oChoixListeFixe.nIdChoixListe = CInt(CType(Page.FindControl("DdlChoixListeFixe"), DropDownList).SelectedValue) : oLigneFixe.oChoixListeFixe.chargerChoixListeFixeParId()

                    Case "sNomEtablissement" : oLigneFixe.sNomEtablissement = CType(Page.FindControl("TxtNomEtablissement"), TextBox).Text
                    Case "sAdresse" : oLigneFixe.sAdresse = CType(Page.FindControl("TxtAdresse"), TextBox).Text
                    Case "sCodePostal" : oLigneFixe.sCodePostal = CType(Page.FindControl("TxtCodePostal"), TextBox).Text
                    Case "sLocalite" : oLigneFixe.sLocalite = CType(Page.FindControl("TxtLocalite"), TextBox).Text
                    Case "sComplementAdresse" : oLigneFixe.sComplementAdresse = CType(Page.FindControl("TxtComplementAdresse"), TextBox).Text
                    Case "sNomContact" : oLigneFixe.sNomContact = CType(Page.FindControl("DdlNom"), DropDownList).Text
                    Case "sFixeContact" : oLigneFixe.sFixeContact = CType(Page.FindControl("TxtFixe"), TextBox).Text
                    Case "sMobileContact" : oLigneFixe.sMobileContact = CType(Page.FindControl("TxtMobile"), TextBox).Text
                    Case "sMailContact" : oLigneFixe.sMailContact = CType(Page.FindControl("TxtEmail"), TextBox).Text
                    Case "sReferenceClient" : oLigneFixe.sReferenceClient = CType(Page.FindControl("TxtReferenceClient"), TextBox).Text
                    Case "dDateDispositionLocaux" : oLigneFixe.dDateDispositionLocaux = CDate(CType(Page.FindControl("TxtDateDisposition"), TextBox).Text)
                    Case "dDateMiseEnService" : oLigneFixe.dDateMiseEnService = CDate(CType(Page.FindControl("TxtDateService"), TextBox).Text)
                End Select
            End If


        Next
    End Sub


    Private Sub CreerCommande(ByVal oNouvelleLigneFixe As LigneFixe, ByVal sTypeCommande As String, Optional ByVal bResiliationPartielle As Boolean = False)
        Dim oCommandeFixe As New CommandeFixe
        oCommandeFixe = New CommandeFixe
        oCommandeFixe.bEstRealiseFacture = False
        oCommandeFixe.bEstTelecharge = False
        oCommandeFixe.bResiliationPartielle = bResiliationPartielle
        oCommandeFixe.oLigneFixe = oNouvelleLigneFixe
        oCommandeFixe.sTypeCommande = sTypeCommande
        If User.Identity.Name.Split(CChar("_"))(2) <> "" And User.Identity.Name.Split(CChar("_"))(2) <> "0" Then
            oCommandeFixe.nIdUtilisateurPrincipal = 0
            oCommandeFixe.nIdUtilisateurSecondaire = CInt(User.Identity.Name.Split(CChar("_"))(2))
        Else
            oCommandeFixe.nIdUtilisateurPrincipal = CInt(User.Identity.Name.Split(CChar("_"))(0))
            oCommandeFixe.nIdUtilisateurSecondaire = 0
        End If
        oCommandeFixe.ajouterCommandeFixe()

    End Sub


    Private Function RecupererIdHierarchie() As Integer
        Dim oControleHierarchie1 As DropDownList = CType(Page.FindControl("DdlHierarchie1"), DropDownList)
        Dim oControleHierarchie2 As DropDownList = CType(Page.FindControl("DdlHierarchie2"), DropDownList)
        Dim oControleHierarchie3 As DropDownList = CType(Page.FindControl("DdlHierarchie3"), DropDownList)
        Dim oControleHierarchie4 As DropDownList = CType(Page.FindControl("DdlHierarchie4"), DropDownList)
        Dim oControleHierarchie5 As DropDownList = CType(Page.FindControl("DdlHierarchie5"), DropDownList)
        Dim oControleHierarchie6 As DropDownList = CType(Page.FindControl("DdlHierarchie6"), DropDownList)


        Dim oHierarchiesFixe As New HierarchiesFixe
        Dim oClient As New Client
        oClient.chargerClientParId(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        Dim Resultat As Integer = 0
        oHierarchiesFixe.chargerHierarchiesFixe(oClient)
        Dim oListeFiltree = From oHierarchie In oHierarchiesFixe Where oHierarchie.sHierarchie1 = oControleHierarchie1.Text And oHierarchie.sHierarchie2 = oControleHierarchie2.Text And oHierarchie.sHierarchie3 = oControleHierarchie3.Text And oHierarchie.sHierarchie4 = oControleHierarchie4.Text And oHierarchie.sHierarchie5 = oControleHierarchie5.Text And oHierarchie.sHierarchie6 = oControleHierarchie6.Text
        For Each oHierarchie In oListeFiltree
            Resultat = oHierarchie.nIdHierarchie
        Next
        Return Resultat
    End Function

    Private Sub BtnInsertAnnuler_Click(sender As Object, e As EventArgs) Handles BtnInsertAnnuler.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub
End Class