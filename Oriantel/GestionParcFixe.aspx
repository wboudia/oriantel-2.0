﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GestionParcFixe.aspx.vb" Inherits="Oriantel.GestionParcFixe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64 128x128" href="../images/icones/favicon.ico" />

    <title>Oria Extranet</title>

    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css' />

    <link href="css/onglets/jquery-ui-1.10.3.customParc.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/colorbox.css" rel="stylesheet" type="text/css" media="screen" />

    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-fr.js"></script>
    <script type="text/javascript" src="js/jquery.formalize.min.js"></script>
    <script type="text/javascript" src="js/jquery.colorbox-min.js"></script>
    <link href="css/gestionParc.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/alert/alertify.bootstrap.css" />
    <link rel="stylesheet" href="css/alert/alertify.core.css" />
    <script type="text/javascript" src="js/alertify.min.js"></script>
    <script>

        $(function () {
            //
            $.fn.disableTab = function (tabIndex, hide) {

                // Get the array of disabled tabs, if any
                var disabledTabs = this.tabs("option", "disabled");

                if ($.isArray(disabledTabs)) {
                    var pos = $.inArray(tabIndex, disabledTabs);

                    if (pos < 0) {
                        disabledTabs.push(tabIndex);
                    }
                }
                else {
                    disabledTabs = [tabIndex];
                }

                this.tabs("option", "disabled", disabledTabs);

                if (hide === true) {
                    $(this).find('li:eq(' + tabIndex + ')').addClass('ui-state-hidden');
                }

                // Enable chaining
                return this;
            };

            $.fn.enableTab = function (tabIndex) {
                $(this).find('li:eq(' + tabIndex + ')').removeClass('ui-state-hidden');
                this.tabs("enable", tabIndex);
                return this;


            };
            //

            if (document.getElementById("MemoireOnglets").value != "") {
                $("#tabs").tabs({ active: document.getElementById("MemoireOnglets").value });
            }
            else {
                $("#tabs").tabs();
            }
           

            //if (document.getElementById("MasquageOnglets").value !== "") {
            //    var tableau = document.getElementById("MasquageOnglets").value.split(";");
            //    for (var i = 0; i < tableau.length ; i++) {
            //        $('#tabs').disableTab(tableau[i], true);
            //    }
            //}


            if (document.getElementById("TextePopup").value !== "") {
                alertify.set({ labels: { ok: "Ok" } });
                alertify.set({ buttonFocus: "none" });
                alertify.set({ buttonReverse: true });
                var sTexte = document.getElementById("TextePopup").value;
                sTexte = "<b style='color:black;'>VALIDATION IMPOSSIBLE</b><br /><br/><span style='color:red'>" + sTexte + "</span>";
                sTexte = sTexte.replace(/\\n/g, "<br/>");
                alertify.alert(sTexte);
                document.getElementById("TextePopup").value = "";
            }
        });


        function GererMemoireOnglets(idOnglet) {
            document.getElementById("MemoireOnglets").value = idOnglet;
        }

        function OuvrirPopup(Lien, largeur, hauteur, raffraichissement) {

            if (typeof (raffraichissement) == 'undefined') {
                raffraichissement = 0;
            }

            $.colorbox.settings.opacity = 0.7;

            jQuery.extend(jQuery.colorbox.settings, {
                current: "image {current} sur {total}",
                previous: "pr&eacute;c&eacute;dente",
                next: "suivante",
                close: "<span style='color:#206797;'><b style='position:relative;'>x&nbsp;</b>Fermer</span>",
                xhrError: "Impossible de charger ce contenu.",
                imgError: "Impossible de charger cette image.",
                slideshowStart: "d&eacute;marrer la pr&eacute;sentation",
                slideshowStop: "arr&ecirc;ter la pr&eacute;sentation",
                onClosed: function () { if (raffraichissement == 1) { __doPostBack('checkboxpostback', '') } }
            });

            $.colorbox({
                innerWidth: largeur + "px",
                innerHeight: hauteur + "px",
                iframe: true,
                href: Lien
            });
                    }

        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().beginAsyncPostBack();
            function EndRequestHandler(sender, args) {

                                $("#TxtDateActivation").datepicker($.datepicker.regional["fr"]);
                if (document.getElementById("TxtDateDisposition") !== null) {
                    $("#TxtDateDisposition").datepicker($.datepicker.regional["fr"]);
                    //$("#TxtDateDisposition").val('')
                }
                if (document.getElementById("TxtDateService") !== null) {
                    $("#TxtDateService").datepicker($.datepicker.regional["fr"]);
                    //$("#TxtDateService").val('')
                }

                if (document.getElementById("MasquageOnglets").value !== "") {
                    var tableau = document.getElementById("MasquageOnglets").value.split(";");
                    for (var i = 0; i < tableau.length ; i++) {
                        $('#tabs').disableTab(tableau[i], true);
                    }
                }
                            }
        });






    </script>
</head>
<body style="width: 975Px; height: 400px">
    <form id="form1" runat="server">



        <asp:ScriptManager ID="sc1" runat="server" EnablePartialRendering="true" EnableViewState="false"></asp:ScriptManager>

        <asp:HiddenField ID="idArticle" runat="server" />
        <asp:HiddenField ID="MemoireOnglets" runat="server" />
        <asp:HiddenField ID="MasquageOnglets" runat="server" />
        <asp:HiddenField ID="TextePopup" runat="server" />

        <asp:UpdatePanel ID="UpdatePanelPostback" UpdateMode="Always" runat="server">
            <ContentTemplate>
                <asp:CheckBox ID="checkboxpostback" runat="server" Visible="false" AutoPostBack="true" CausesValidation="true" />

            </ContentTemplate>
        </asp:UpdatePanel>

        <div style="text-align: center; width: 975Px; height: 20px; float: left">

            <span style="padding-top: 10px; font-size: 14pt; text-transform: uppercase; position: relative;">
                <asp:Image ID="ImageAction" Visible="false" runat="server" Style="height: 22px; width: 22px; position: absolute; left: -27Px;" />
                <asp:Label ID="LblTitre" runat="server" Text=""></asp:Label>
            </span>



        </div>

        <div id="Principal" style="position: absolute; top: 40px; left: 12px; height: 80px; width: 975Px;">
            <img src="images/GestionParc/cadre_general.png" style="z-index: 99; left: 0px; position: absolute; top: 0px; height: 105px; width: 975Px; right: 8px;" />
            <asp:Label ID="lblBoiteGeneral" runat="server" Height="16px" Style="left: 20px; position: absolute; top: 8px; z-index: 102; right: 969px;" Width="250px">Informations générales : </asp:Label>
            <p>
                <asp:Label ID="Label1" runat="server" Text="Type de ligne :" Width="85px" Height="16px" Style="position: absolute; left: 20px; top: 50px; z-index: 100"></asp:Label>
                <asp:DropDownList ID="DdlTypeLigne" AutoPostBack="true" runat="server" Class="champs" Height="20px" Width="190px" Style="position: absolute; left: 105px; top: 50px; z-index: 100"></asp:DropDownList>
            </p>
            <p>
                <asp:Label ID="Label2" runat="server" Text="Usage :" Width="85px" Height="16px" Style="position: absolute; left: 310Px; top: 50px; z-index: 100"></asp:Label>
                <asp:DropDownList ID="DdlUsage" runat="server" Class="champs" Height="20px" Width="190px" Style="position: absolute; left: 360px; top: 50px; z-index: 100"></asp:DropDownList>
            </p>
            <p>
                <asp:Label ID="Label3" runat="server" Text="Numéro :" Height="16px" Width="115px" Style="position: absolute; left: 570px; top: 50px; z-index: 100"></asp:Label>
                <asp:TextBox ID="TxtNumeroGSM" Enabled="true" Class="champs" runat="server" Width="80px" Style="position: absolute; height: 17px; left: 630px; top: 48px; z-index: 100"></asp:TextBox>
            </p>
            <p>
                <asp:Label ID="LblDateActivation" runat="server" Text="Date activation :" Height="16px" Width="115px" Style="position: absolute; left: 730px; top: 50px; z-index: 100"></asp:Label>
                <asp:TextBox ID="TxtDateActivation" Enabled="true" Class="champs" runat="server" Width="70px" Height="17px" Style="position: absolute; left: 840px; top: 48px; z-index: 100"></asp:TextBox>
            </p>
        </div>


        <div id="tabs" style="position: absolute; left: 12px; top: 153px; width: 975Px">
            <ul class="ulonglets" style="text-align: center; vertical-align: middle;">
                <li><a href="#tabs1" onclick="GererMemoireOnglets(0);">Accès au<br />
                    réseau</a></li>
                <li><a href="#tabs2" onclick="GererMemoireOnglets(1);">Facturation<br />
                    &nbsp;<br />
                </a></li>
                <li><a href="#tabs3" onclick="GererMemoireOnglets(2);">Informations<br />
                    d'installation</a></li>
                <li><a href="#tabs4" onclick="GererMemoireOnglets(3);">Dates<br />
                    &nbsp;<br />
                </a></li>
                <li><a href="#tabs5" onclick="GererMemoireOnglets(4);">Options <br />&nbsp;<br /></a></li>
               
                <li><a href="#tabs6" onclick="GererMemoireOnglets(5);">Commentaires<br />
                    &nbsp;<br />
                </a></li>
            </ul>
            <div id="tabs1" style="border: 1px solid white; height: 290px">
                <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Always" runat="server">
                    <ContentTemplate>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder1_1" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder1_2" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder1_3" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder1_4" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder1_5" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder1_6" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                                <asp:PlaceHolder ID="PlaceHolder1_7" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder1_8" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder1_9" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>

                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img src="images/Chargement.gif" alt="">
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="tabs2" runat="server" style="display: block; border: 1px solid white; height: 290px">
                <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Always" runat="server">
                    <ContentTemplate>

                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder2_1" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder2_2" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder2_3" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder2_4" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder2_5" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder2_6" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder2_7" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder2_8" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>

                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img src="images/Chargement.gif" alt="">
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="tabs3" style="border: 1px solid white; height: 290px">
                <asp:UpdatePanel ID="UpdatePanel3" UpdateMode="Always" runat="server">
                    <ContentTemplate>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder3_1" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder3_2" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder3_3" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder3_4" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder3_5" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder3_6" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder3_7" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder3_8" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder3_9" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div style="float: left; width: 50%;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder3_10" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>


                        <asp:UpdateProgress ID="UpdateProgress3" runat="server" AssociatedUpdatePanelID="UpdatePanel3"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img src="images/Chargement.gif" alt="">
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
           
            <div id="tabs4" runat="server" style="display: block; border: 1px solid white; height: 290px">
                <asp:UpdatePanel ID="UpdatePanel4" UpdateMode="Always" runat="server">
                    <ContentTemplate>

                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder4_1" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder4_2" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>

                        <asp:UpdateProgress ID="UpdateProgress4" runat="server" AssociatedUpdatePanelID="UpdatePanel4"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img src="images/Chargement.gif" alt="">
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="tabs5" runat="server" style="display: block; border: 1px solid white; height: 290px">
                <asp:UpdatePanel ID="UpdatePanel5" UpdateMode="Always" runat="server">
                    <ContentTemplate>

                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder5_1" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder5_2" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder5_3" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder5_4" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder5_5" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                         <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder5_6" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                         <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder5_7" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                            <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder5_8" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                            <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder5_9" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>

                        <asp:UpdateProgress ID="UpdateProgress5" runat="server" AssociatedUpdatePanelID="UpdatePanel5"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img src="images/Chargement.gif" alt="">
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            

       <%--     <div id="tabs7" runat="server" style="display: block; border: 1px solid white; height: 290px">
                <asp:UpdatePanel ID="UpdatePanel7" UpdateMode="Always" runat="server">
                    <ContentTemplate>

                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder7_1" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder7_2" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder7_3" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder7_4" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder7_5" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                         <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder7_6" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                        

                        <asp:UpdateProgress ID="UpdateProgress7" runat="server" AssociatedUpdatePanelID="UpdatePanel7"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img src="images/Chargement.gif" alt="">
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>--%>


            <div id="tabs6" runat="server" style="display: block; border: 1px solid white; height: 290px">
                <asp:UpdatePanel ID="UpdatePanel6" UpdateMode="Always" runat="server">
                    <ContentTemplate>

                        <div style="padding=0px; margin: 0Px; height: 300Px;">
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder6_1" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>
                          <div>
                            <p>
                                <asp:PlaceHolder ID="PlaceHolder6_2" runat="server"></asp:PlaceHolder>
                            </p>
                        </div>

                        <asp:UpdateProgress ID="UpdateProgress6" runat="server" AssociatedUpdatePanelID="UpdatePanel6"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div class="loading-image">
                                    <img src="images/Chargement.gif" alt="">
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </div>

        <div id="DivAction" runat="server" style="display: block; position: absolute; top: 512px; left: 12px; height: 60px; width: 975Px;">
            <img src="images/GestionParc/cadre_action.png" style="z-index: 99; left: 0px; position: absolute; top: 0px; height: 60px; width: 975px; right: -180px;" />
            <asp:Label ID="lblBoiteAction" runat="server" Height="16px" Style="left: 20px; position: absolute; top: 8px; z-index: 102; right: 919px;" Width="250px">Actions : </asp:Label>

            <div id="DivActionInsert" style="position: absolute; z-index: 100; top: 30px; left: 0px" runat="server">
                <asp:Button ID="BtnInsertValider" class="champs" runat="server" Text="Valider" Style="position: absolute; z-index: 101; width: 80px; top: 3px; left: 392px;" />
                <asp:Button ID="BtnInsertAnnuler" class="champs" runat="server" Text="Annuler" Style="position: absolute; z-index: 101; width: 80px; top: 3px; left: 503px;" />
            </div>
        </div>


        <div id="DivInformation" runat="server" style="position: absolute; top: 576px; left: 12px; height: 20px; width: 975Px;">
            <p style="width: 550px">
                <asp:Label ID="LblInformation" Visible="false" runat="server" Text="" Style="position: absolute; left: 1px; top: 0px"></asp:Label>
            </p>
        </div>

    </form>
</body>
</html>
