function getConteneurInfo(){
    if (sessionStorage['BandeauInfo'] != 1) {
		document.getElementById('BandeauInfo').style.display = 'block';
	}else{
		document.getElementById('BandeauInfo').style.display = 'none';
		document.getElementById('ConteneurInfos').style.display = 'none';
	}
}

function getInfos(){
	if(document.getElementById('ConteneurInfos').style.display == "none"){
		document.getElementById('ConteneurInfos').style.display = "block";
	}else{
		document.getElementById('ConteneurInfos').style.display = "none";
	}
}