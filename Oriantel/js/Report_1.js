//--------------Ajout des évènements--------------\\
function addHandlers() {
    Sys.Application.add_init(appInit);
    Sys.Application.add_load(appLoad);
    Sys.Application.add_unload(appUnload);

    with (Sys.WebForms.PageRequestManager.getInstance()) {
        //add_pageLoaded(init)
        add_initializeRequest(asyncInitialiseRequest);
        add_beginRequest(asyncBeginRequest);
        add_endRequest(asyncEndRequest);
    }
}

function pageLoad(sender, args) {
    if (args.get_isPartialLoad()) {
        //Partial Postback
        appLoad();
    }
    else {
        //Full Postback
        addHandlers();
        appLoad();
    }
}

$(document).ready(function () {
});

function appInit() {
}

function appLoad() {
    
}

function appUnload() {
}

function asyncInitialiseRequest(sender, args) {
    if (sender._postBackSettings.sourceElement != undefined) {
        var sourceId = sender._postBackSettings.sourceElement.id;
    }
    else {
        var sourceId = ""
    }
}

function asyncBeginRequest(sender, args) {
    if (sender._postBackSettings.sourceElement != undefined) {
        var sourceId = sender._postBackSettings.sourceElement.id;
    }
    else {
        var sourceId = "";
    }
}

function asyncEndRequest(sender, args) {
    if (sender._postBackSettings.sourceElement != undefined) {
        var sourceId = sender._postBackSettings.sourceElement.id;
    }
    else {
        var sourceId = "";
    }
    try {
        CalculerTailleTableau();
    }
    catch (e) { }

}

function CalculerTailleTableau() {
    var ihauteurFenetre = 0;
    var iHauteurEntete = 0;
    var iHauteurFiltre = 0;
    var iHauteurOutils = 0;
    var iHauteurFooter = 0;
    var iHauteurOnglets = 0;
    var iHauteurTotal = 0;
    var iHauteurPagination = 0;
    var iHauteurEntetes = 0;
    var iHauteurTitreTableau = 0;
    var iResultatHauteurTableau = 0;

    if (document.all) {
        ihauteurFenetre = document.documentElement.clientHeight;
    }
    else {
        ihauteurFenetre = window.innerHeight;
    }

        iHauteurEntete = $('#blocAriane').outerHeight();

    var hauteurboitefiltre = 0;
    $("div[id^='DivBoiteFiltre']").each(function () {
        hauteurboitefiltre = $(this).outerHeight() + 10
    });
    if (hauteurboitefiltre != 0) {
        iHauteurFiltre = hauteurboitefiltre
    }
    else
    {
        iHauteurFiltre = $("div[id^='idDivColsFiltres']").outerHeight() + 20;
    }
    
    //if ($("div[id^='DivBoiteFiltre']") != undefined) {
    //    iHauteurFiltre = $("div[id^='DivBoiteFiltre']").outerHeight() + 85;
    //    alert($("div[id^='DivBoiteFiltre']").id);
    //}
    //else {
    //    iHauteurFiltre = $("div[id^='idDivColsFiltres']").outerHeight() + 66;
    //    alert('Pas Boite');
    //}

    iHauteurOutils = $("div[id^='idDivBoiteOutils']").outerHeight() + 56;
    if (document.getElementById("TitreTableauPresent").value == "oui") {
        iHauteurTitreTableau = 20;
    }
    
    if (document.getElementById("HiddenFooter") != undefined) {
        iHauteurFooter = $('#footer').height() + 25;
    }
    else {
        //Sert a enlever la barre de l'image de fond en bas des popup
        iHauteurFooter=-27;
    }

if (document.getElementById("NombreNiveauxOnglets").value == "1") {
    iHauteurOnglets = 36;
}
else {
    iHauteurOnglets = 77;
}

if (document.getElementById("PossedePagination").value == "oui") {
    iHauteurPagination = 38;
}
 
var tableau = document.getElementById("ListeGridId").value.split("|");
var resultat = "";
for (var i = 1; i <= tableau.length - 1; i++) {
    if (document.getElementById(tableau[i]) != undefined) {
        resultat = $("#" + document.getElementById(tableau[i]).id + " thead").outerHeight();
        if (resultat > parseInt(iHauteurEntetes)) {
            iHauteurEntetes = resultat;
        }

        resultat = $("#" + document.getElementById(tableau[i]).id + " tfoot").outerHeight() + 10;
        if (resultat > parseInt(iHauteurTotal)) {
            iHauteurTotal = resultat;
        }
    }
}

iResultatHauteurTableau = ihauteurFenetre - (iHauteurEntete + iHauteurTitreTableau + iHauteurFiltre + iHauteurOutils + iHauteurFooter + iHauteurOnglets + iHauteurTotal + iHauteurPagination + iHauteurEntetes)

for (var i = 1; i <= tableau.length - 1; i++) {
    if (document.getElementById(tableau[i]) != undefined) {
        //if (iResultatHauteurTableau < 300) {
        $("#" + document.getElementById(tableau[i]).id + " tbody").css({ "max-height": iResultatHauteurTableau + "px" });
        //if ((iHauteurTitreTableau + iHauteurTotal + iHauteurEntetes)<300) {
        //    $("#" + document.getElementById(tableau[i]).id + " tbody").css({ "min-height": iHauteurTitreTableau + iHauteurTotal + iHauteurEntetes + "px" });
        //}

        //Gestion de la barre horizontale pour IE 8-9-10-11
        if (getInternetExplorerVersion() >= 8) {
            var taillethead = $("#" + document.getElementById(tableau[i]).id + " thead").outerHeight();
            var tailletbody = $("#" + document.getElementById(tableau[i]).id + " tbody").outerHeight();
            var tailletfooter = $("#" + document.getElementById(tableau[i]).id + " tfooter").outerHeight();
            var tailletotaletableau = taillethead + tailletbody + tailletfooter + 45
            $("#" + document.getElementById(tableau[i]).id).parent().css({ "height": tailletotaletableau + "px" });
        }
        //}
    }
}
    }

function GestionSurvol() {
    var tableau = document.getElementById("ListeGridId").value.split("|");
    //'---Gestion du survol'
    //for (var i = 1; i <= tableau.length - 1; i++) {
    //    if (document.getElementById(tableau[i]) != undefined) {
    //        $("#" + document.getElementById(tableau[i]).id).tableHover({ clickClass: 'click' });
    //    }
    //}

    for (var i = 1; i <= tableau.length - 1; i++) {
        if (document.getElementById(tableau[i]) != undefined) {
            $("#" + document.getElementById(tableau[i]).id + " tbody tr").click(function () {
                $(this).closest("tr").siblings().removeClass("click");
                $(this).toggleClass("click");
            })

        }
    }
    }

function getInternetExplorerVersion() {
    var rv = -1;
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    else if (navigator.appName == 'Netscape') {
        var ua = navigator.userAgent;
        var re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

$(window).on('resize', function () {
    CalculerTailleTableau();
});

function InIEvent() {
    window.scrollTo(0, 0);
    $(document).ready(function () {
        if (document.getElementById("LienTritre1Selectionne").value != "") {
            $('#accordeon').accordion({ header: '.titre', active: parseInt(document.getElementById("LienTritre1Selectionne").value) - 1 });
        }
        else {
            $('#accordeon').accordion({ header: '.titre' });
        }

        //SCROLL FIXED
        if ($(document).height() > 800) {
            if ($('html').attr('class').toUpperCase() != 'IE6' && $('html').attr('class').toUpperCase() != 'IE7' && $('html').attr('class').toUpperCase() != 'IE8') {
                $('.ZoneFiltre').scrollToFixed({ minWidth: 1024 });
            }
        }

        if ($('#EtatAffichageSommaire').val() == '1') {
            $('#blocCorps').css('margin-left', '0px');
            $('#blocSommaire').hide(0, function () { });
            $('#EtatAffichageSommaire').val() == '1'
        }
        else {
            $('#blocSommaire').show(0, function () {
                $('#blocCorps').css('margin-left', '224px');
                $('body').css('background', 'url(images/bkgd.png) left bottom');
                $('#EtatAffichageSommaire').val('0');
            });
        }
        
        try {

            if (document.getElementById("ValeurOngletNiv1Selectionne").value != "") {
                $("#tabs-Niv1").tabs({ active: document.getElementById("ValeurOngletNiv1Selectionne").value.split('_')[0].split('-')[2] - 1 });
            }
            else {
                $("#tabs-Niv1").tabs();
            }
            
            if (document.getElementById("ValeurOngletNiv2Selectionne").value != "" && document.getElementById("ValeurOngletNiv2Selectionne").value.split('_')[0].split('-')[2] == document.getElementById("ValeurOngletNiv1Selectionne").value.split('_')[0].split('-')[2]) {
                $("#tabs-Niv2-1").tabs({ active: document.getElementById("ValeurOngletNiv2Selectionne").value.split('_')[0].split('-')[3] - 1 });
            }
            else {
                $("#tabs-Niv2-1").tabs();
            }
            
            if (document.getElementById("ValeurOngletNiv2Selectionne").value != "" && document.getElementById("ValeurOngletNiv2Selectionne").value.split('_')[0].split('-')[2] == document.getElementById("ValeurOngletNiv1Selectionne").value.split('_')[0].split('-')[2]) {
                $("#tabs-Niv2-2").tabs({ active: document.getElementById("ValeurOngletNiv2Selectionne").value.split('_')[0].split('-')[3] - 1 });
            }
            else {
                $("#tabs-Niv2-2").tabs();
            }
                    }
        catch (e) {

        }

        $("select.styled").customSelect();
        $("input, textarea").placeholder();

        CalculerTailleTableau();
        GestionSurvol();

                if (document.getElementById("Notification").value != "") {
            var texte = document.getElementById("Notification").value;
            document.getElementById("Notification").value = "";
            alertify.set({ labels: { ok: "Ok" } });
            alertify.set({ buttonFocus: "none" });
            alertify.set({ buttonReverse: true });
            alertify.alert(texte);
        }
        
        if (document.getElementById("FichierTelechargement").value != "") {
            document.location.href = document.getElementById("FichierTelechargement").value;
            document.getElementById("FichierTelechargement").value = "";
        }
      
        // Get a PageRequestManager reference.
        //var prm = Sys.WebForms.PageRequestManager.getInstance();
        // Hook the _initializeRequest event and add our own handler.
        //prm.add_initializeRequest(InitializeRequest);

    });
}

function toggle_visibility_tools(idDivOutils) {
    $('div[id^="DivOutils"]').each(function () {

        $('div[id^="DivOutils"]').slideToggle("medium", function () {
            if ($('div[id^="DivOutils"]').is(':visible')) {
                document.getElementById("BandeauMasque").value = idDivOutils;
                document.getElementById("Outil").value = document.getElementById("Outil").value + "|" + idDivOutils;
            }
            else {
                document.getElementById("BandeauMasque").value = "";
                document.getElementById("Outil").value = document.getElementById("Outil").value.replace(idDivOutils, "");
                document.getElementById("Outil").value = document.getElementById("Outil").value.replace("||", "|");
                if (document.getElementById("Outil").value == "") {
                    document.getElementById("Outil").value = "none";
                }
            }
            CalculerTailleTableau();
        })
        $(this).siblings('.LinkFiltres').toggleClass('highlight');
    })
}

function toggle_visibility_filters(idDivBoiteFiltre) {
    $('div[id^="idDivColsFiltres"]').each(function () {

        $('div[id^="idDivColsFiltres"]').slideToggle("medium", function () {
            if ($('div[id^="idDivColsFiltres"]').is(':visible')) {
                document.getElementById("BandeauMasqueFiltre").value = idDivBoiteFiltre;
                document.getElementById("BoiteFiltre").value = document.getElementById("BoiteFiltre").value + "|" + idDivBoiteFiltre;
            }
            else {
                document.getElementById("BandeauMasqueFiltre").value = "";
                document.getElementById("BoiteFiltre").value = document.getElementById("BoiteFiltre").value.replace(idDivBoiteFiltre, "");
                document.getElementById("BoiteFiltre").value = document.getElementById("BoiteFiltre").value.replace("||", "|");
            }
            CalculerTailleTableau();
        })
        $(this).siblings('.LinkFiltres').toggleClass('highlight');
    })
}

$(document).ready(InIEvent);

function retourArriere() {
    document.getElementById("retourActive").value = 'true'
    document.getElementById("autorisationEnregistrementNavigation").value = "false"
    document.getElementById("ValeurOngletNiv1Selectionne").value = "";
    document.getElementById("ValeurOngletNiv2Selectionne").value = "";
    document.getElementById("ChaineFiltreRedirection").value = "";
    document.getElementById("ReferenceInsert").value = "";
    document.getElementById("BloquerLigneAjout").value = "";
    document.getElementById("BloquerColoneAction").value = "";
    document.getElementById("ReferenceModificationLigne").value = "";
    document.getElementById("chaineFiltreRetour").value = "";
    document.getElementById("ListeTriTableau").value = "";

    var tableau = document.getElementById("chaineArianeRetour").value.split("µ");
    var resultat = "";
    for (var i = 0; i < tableau.length - 1; i++) {
        if (resultat != "") {
            resultat += "µ";
        }
        resultat += tableau[i];
    }
    document.getElementById("chaineArianeRetour").value = resultat;

    if (document.getElementById("listeEtapeNavigation").value == "0") {
        history.back();
    }
}

function ValoriserMemoireRetour(sChainurlActuelle) {
    var etapeNavigation = parseInt(document.getElementById("etapeNavigation").value);
    document.getElementById("etapeNavigation").value = etapeNavigation + 1;
    document.getElementById("listeEtapeNavigation").value += '|' + parseInt(etapeNavigation + 1);
    if (sChainurlActuelle != '') {
        document.getElementById("chaineFiltreRetour").value = sChainurlActuelle;
    }
}

function ReferenceInsert(texte) {
    document.getElementById("ReferenceInsert").value = texte;
}

function BloquerLigneAjout(texte) {
    document.getElementById("BloquerLigneAjout").value = texte;
}

function BloquerColoneAction(texte) {
    document.getElementById("BloquerColoneAction").value = texte;
}

function TelechargerBonCommandeMobile(texte) {
    document.getElementById("TelechargementBonCommandeMobile").value = texte;
    __doPostBack('checkboxpostback', '');
}

function Supprimer_Balises_Input(Controle) {
    var Texte = Controle.value;
    Controle.value = Texte.replace(/<[^>]*>/g, "");
}

function EditerStock(idArticle,typemodif,filtreTypeMateriel) {
    if (typeof (filtreTypeMateriel) == 'undefined') {
        filtreTypeMateriel = 'aucun';
    }
   OuvrirPopup('GestionStock.aspx?idArticleStock=' + idArticle + '&typemodif=' + typemodif + '&filtreTypeMateriel=' + filtreTypeMateriel, '645', '525', 1);
}

function EditerStockCNES(idArticle, typemodif, filtreTypeMateriel) {
    if (typeof (filtreTypeMateriel) == 'undefined') {
        filtreTypeMateriel = 'aucun';
    }
    OuvrirPopup('GestionStockCNES.aspx?idArticleStock=' + idArticle + '&typemodif=' + typemodif + '&filtreTypeMateriel=' + filtreTypeMateriel, '645', '525', 1);
}

function EditerLigneParc(idLigne, typemodif) {
    OuvrirPopup('GestionParc.aspx?idLigne=' + idLigne + '&typemodif=' + typemodif, '805', '667', 1);
}

function EditerLigneParcCNES(idLigne, typemodif) {
    OuvrirPopup('GestionParcCNES.aspx?idLigne=' + idLigne + '&typemodif=' + typemodif, '805', '667', 1);
}

function EditerLigneParcFixe(numeroLigne, typemodif) {
    OuvrirPopup('GestionParcFixe.aspx?numeroLigne=' + numeroLigne + '&typemodif=' + typemodif, '1050', '667', 1);
}

function EditerLigneParcRetourMateriel(idLigne, typemodifparc, idArticle, typemodifMateriel, filtreTypeMateriel) {
    OuvrirPopupRetourMateriel('GestionParc.aspx?idLigne=' + idLigne + '&typemodif=' + typemodifparc, '755', '667', 'GestionStock.aspx?idArticleStock=' + idArticle + '&typemodif=' + typemodifMateriel + '&filtreTypeMateriel=' + filtreTypeMateriel);
 }

function EditerLigneParcRetourMaterielCNES(idLigne, typemodifparc, idArticle, typemodifMateriel, filtreTypeMateriel) {
    OuvrirPopupRetourMateriel('GestionParcCNES.aspx?idLigne=' + idLigne + '&typemodif=' + typemodifparc, '755', '667', 'GestionStockCNES.aspx?idArticleStock=' + idArticle + '&typemodif=' + typemodifMateriel + '&filtreTypeMateriel=' + filtreTypeMateriel);
}

function ActiverLigneParc(idLigne) {
    OuvrirPopup('GestionParcActivation.aspx?idLigne=' + idLigne , '355', '290', 1);
}

function ActiverLigneParcFixe(idLigne) {
    OuvrirPopup('GestionParcFixeActivation.aspx?idLigne=' + idLigne, '355', '290', 1);
}

function OuvrirPopupPostIt(numLigne) {
    OuvrirPopup('PostIt.aspx?numLigne=' + numLigne, '700', '450', 1)
}

function PreparerRedirection(idModule, idRapport, ChaineFiltreRedirection, Ariane) {
    document.getElementById("idModule").value = idModule;
    document.getElementById("idRapport").value = idRapport;

    //affectation des variables de mémorisation
    document.getElementById("ChaineFiltreRedirection").value = ChaineFiltreRedirection;
    document.getElementById("autorisationEnregistrementNavigation").value = "true"
    if ($.contains(Ariane, "µ") == true || document.getElementById("chaineArianeRetour").value == "" || document.getElementById("chaineArianeRetour").value == undefined) {
        document.getElementById("chaineArianeRetour").value = Ariane;
    }
    else {
        document.getElementById("chaineArianeRetour").value += 'µ' + Ariane;
    }

    ValoriserMemoireRetour(ChaineFiltreRedirection);
    document.getElementById("ValeurOngletNiv1Selectionne").value = "";
}

function ValorisationEtat(idModule, idRapport, ChaineFiltreRedirection) {
    document.getElementById("idModule").value = idModule;
    document.getElementById("idRapport").value = idRapport;
    document.getElementById("ValeurOngletNiv1Selectionne").value = "";
    document.getElementById("ValeurOngletNiv2Selectionne").value = "";
    document.getElementById("ReferenceInsert").value = "";
    document.getElementById("BloquerLigneAjout").value = "";
    document.getElementById("BloquerColoneAction").value = "";
    document.getElementById("ReferenceModificationLigne").value = "";
    document.getElementById("chaineArianeRetour").value = "";
    document.getElementById("autorisationEnregistrementNavigation").value = "true"
    document.getElementById("BandeauMasque").value = 'none';
    document.getElementById("ListeTriTableau").value = "";

    if (ChaineFiltreRedirection == undefined) {
        document.getElementById("ChaineFiltreRedirection").value = "";
        document.getElementById("MoisGlissant").value = "";
        document.getElementById("OutilTop").value = "";
        document.getElementById("Decimale").value = "";
        document.getElementById("TTC").value = "";
        document.getElementById("NumeroEntier").value = "";
        document.getElementById("Pagination").value = "";
        document.getElementById("Recherche").value = "";
        document.getElementById("Outil").value = "";
        document.getElementById("Tri").value = "";
        document.getElementById("Filtre").value = "";
        document.getElementById("chaineFiltreRetour").value = "";
        SelectionItemActif();
    }
}

function NotifierTri(sGridId, sDataField) {
    document.getElementById("TriActif").value = sGridId + '|' + sDataField;
    __doPostBack('checkboxpostback', '');
}

function RedirectionAdresse(lien) {
    var nDerniereSlashPathName = document.location.pathname.lastIndexOf("/");
    document.location.href = window.location.origin + window.location.pathname.substring(0, nDerniereSlashPathName + 1) + lien;
}

function CloturerTicket(sInfos, sTicket, dDateCloture) {
    if (dDateCloture == 'KO') {
        alertify.set({ labels: { ok: "Ok" } });
        alertify.set({ buttonFocus: "none" });
        alertify.set({ buttonReverse: true });
        alertify.alert("Le ticket n°" + sTicket + " a déjà été cloturé.");
    }
    else {
        alertify.set({ labels: { ok: "Oui", cancel: "Non" } });
        alertify.set({ buttonFocus: "none" });
        alertify.set({ buttonReverse: true });
        alertify.confirm("Etes vous certain de vouloir cloturer le ticket n° " + sTicket + " ?", function (e) {
            if (e) {
                document.getElementById("ReferenceCloture").value = sInfos;
                __doPostBack('checkboxpostback', '');
            }
        });
    }
}

function SupprimerTicket(sInfos, sTicket) {
    alertify.set({ labels: { ok: "Oui", cancel: "Non" } });
    alertify.set({ buttonFocus: "none" });
    alertify.set({ buttonReverse: true });
    alertify.confirm("Etes vous certain de vouloir supprimer le ticket n° " + sTicket + " ?", function (e) {
        if (e) {
            document.getElementById("ReferenceSuppression").value = sInfos;
            __doPostBack('checkboxpostback', '');
        }
    });
}

function SupprimerTicketDetail(sInfos, sTicketDetail) {
    alertify.set({ labels: { ok: "Oui", cancel: "Non" } });
    alertify.set({ buttonFocus: "none" });
    alertify.set({ buttonReverse: true });
    alertify.confirm("Etes vous certain de vouloir supprimer cette action ?", function (e) {
        if (e) {
            document.getElementById("ReferenceSuppressionDetail").value = sInfos;
            __doPostBack('checkboxpostback', '');
        }
    });
}

function SupprimerLigne(sInfos, sTexte) {
    alertify.set({ labels: { ok: "Oui", cancel: "Non" } });
    alertify.set({ buttonFocus: "none" });
    alertify.set({ buttonReverse: true });
    alertify.confirm("Etes vous certain de vouloir supprimer " + sTexte + " ?", function (e) {
        if (e) {
            document.getElementById("ReferenceSuppressionLigne").value = sInfos;
            __doPostBack('checkboxpostback', '');
        }
    });
}

function ModifierLigne(sInfos, sTexte) {  
    document.getElementById("ReferenceModificationLigne").value = sInfos;
    __doPostBack('checkboxpostback', '');
}

function SelectionItemActif() {
    for (i = 0; i < 100 + 1 ; i++) {
        var LinkButton = document.getElementById("LienTitre2_Rapport_" + i);
        if (LinkButton != null) {
            if (i == document.getElementById("idRapport").value) {
                LinkButton.setAttribute("class", "active");
            }
            else {
                LinkButton.setAttribute("class", " ");
            }
        }
    }
}

function AnnulerEdition() {
    document.getElementById("ReferenceModificationLigne").value = "";
    __doPostBack('checkboxpostback', '');
}

function ValoriserCheckbox(champ, idcheckbox) {
    document.getElementById(champ).value = document.getElementById(idcheckbox).checked
}

function ValoriserDropdownList(champ, iddropdownlist) {
    document.getElementById(champ).value = document.getElementById(iddropdownlist).value
}

function ValoriserRecherche(champ, idinput) {
    var tableau = document.getElementById(champ).value.split("|");
    var resultat = "";
    var esttrouve = false;
    for (var i = 0; i < tableau.length ; i++) {
        if (tableau[i].split("=")[0] == idinput) {
            if (document.getElementById(idinput).value != "") {
                resultat = resultat + idinput + "=" + document.getElementById(idinput).value + "|";
                esttrouve = true;
            }
        }
        else {
            if (tableau[i] != "") {
                resultat = resultat + tableau[i].split("=")[0] + "=" + tableau[i].split("=")[1] + "|";
            }
        }
    }

    if (esttrouve == false) {
        if (document.getElementById(idinput).value != "") {
            resultat = resultat + idinput + "=" + document.getElementById(idinput).value + "|";
        }
    }
    document.getElementById(champ).value = resultat.substring(0, resultat.length - 1);
}

function ValoriserSeuil(champ, idinput) {
    if (document.getElementById(idinput).value == "") {
        document.getElementById(champ).value = "VIDE";
    }
    else {
        document.getElementById(champ).value = "";
    }

    if (isNaN(document.getElementById(idinput).value) == false) {

        //var tableau = document.getElementById(champ).value.split("|");
        //var resultat = "";
        //var esttrouve = false;
        //for (var i = 0; i < tableau.length ; i++) {
        //    if (tableau[i].split("=")[0] == idinput) {
        //        if (document.getElementById(idinput).value != "") {
        //            resultat = resultat + idinput + "=" + document.getElementById(idinput).value + "|";
        //            esttrouve = true;
        //        }
        //    }
        //    else {
        //        if (tableau[i] != "") {
        //            resultat = resultat + tableau[i].split("=")[0] + "=" + tableau[i].split("=")[1] + "|";
        //        }
        //    }
        //}

        //if (esttrouve == false) {
        //    if (document.getElementById(idinput).value != "") {
        //        resultat = resultat + idinput + "=" + document.getElementById(idinput).value + "|";
        //    }
        //}
        //document.getElementById(champ).value = resultat.substring(0, resultat.length - 1);
    }
    else {
        alertify.set({ labels: { ok: "Ok" } });
        alertify.set({ buttonFocus: "none" });
        alertify.set({ buttonReverse: true });
        alertify.alert('Vous devez saisir une valeur numérique');
        document.getElementById(idinput).value = "";
    }
}

function ValoriserPagination(champ, iddropdownlist) {
    var tableau = document.getElementById(champ).value.split("|");
    var resultat = "";
    var esttrouve = false;
    for (var i = 0; i < tableau.length ; i++) {
        if (tableau[i].split("=")[0] == iddropdownlist) {
            resultat = resultat + iddropdownlist + "=" + document.getElementById(iddropdownlist).value + "|";
            esttrouve = true;
        }
        else {
            if (tableau[i] != "") {
                resultat = resultat + tableau[i].split("=")[0] + "=" + tableau[i].split("=")[1] + "|";
            }
        }
    }

    if (esttrouve == false) {
        resultat = resultat + iddropdownlist + "=" + document.getElementById(iddropdownlist).value + "|";
    }
    document.getElementById(champ).value = resultat.substring(0, resultat.length - 1);
}

function ValoriserFiltre(idFiltre, idDropdown) {

    var tableau = document.getElementById("Filtre").value.split("µ");
    var resultat = "";
    var esttrouve = false;
    for (var i = 0; i < tableau.length ; i++) {
        if (tableau[i].split("=")[0] == idFiltre) {
            resultat = resultat + idFiltre + "=" + document.getElementById(idDropdown).value + "µ";
            esttrouve = true;
        }
        else {
            if (tableau[i] != "") {
                resultat = resultat + tableau[i].split("=")[0] + "=" + tableau[i].split("=")[1] + "µ";
            }
        }
    }

    if (esttrouve == false) {
        resultat = resultat + idFiltre + "=" + document.getElementById(idDropdown).value + "µ";
    }
    document.getElementById("Filtre").value = resultat.substring(0, resultat.length - 1);
}

function ajouterLienElementAriane(lien) {
    var UL = document.getElementById('ULAriane');
    var newli1 = document.createElement('li');
    newli1.innerHTML = '<a href="#">' + lien + '</a>';
    UL.appendChild(newli1);
}

function GererMemoireOnglets(idDiv1, idDiv2, idOnglet, autoriserpostback) {
    document.getElementById("autorisationEnregistrementNavigation").value = "true";
    document.getElementById("ValeurOngletNiv1Selectionne").value = idDiv1;
    //si IdDiv2
    if (idDiv2 != '0') {
        document.getElementById("ValeurOngletNiv2Selectionne").value = idDiv2;
        document.getElementById("IdOngletNiv2Selectionne").value = idOnglet;
    }
    __doPostBack('checkboxpostback', '');
}

function OuvrirPopup(Lien, largeur, hauteur,raffraichissement) {

    if (typeof (raffraichissement) == 'undefined') {
        raffraichissement = 0;
    }
    $.colorbox.settings.opacity = 0.7;

    jQuery.extend(jQuery.colorbox.settings, {
        current: "image {current} sur {total}",
        previous: "pr&eacute;c&eacute;dente",
        next: "suivante",
        close: "<span style='color:#206797;'><b style='position:relative;'>x&nbsp;</b>Fermer</span>",
        xhrError: "Impossible de charger ce contenu.",
        imgError: "Impossible de charger cette image.",
        slideshowStart: "d&eacute;marrer la pr&eacute;sentation",
        slideshowStop: "arr&ecirc;ter la pr&eacute;sentation",
        onClosed: function () { if (raffraichissement == 1) { __doPostBack('checkboxpostback', '') } }
    });
    $.colorbox({
        width: largeur + "px",
        height: hauteur + "px",
        iframe: true,
        href: Lien
    });
}

function OuvrirPopupRetourMateriel(Lien, largeur, hauteur, lienRetour) {
   
    if (typeof (raffraichissement) == 'undefined') {
        raffraichissement = 0;
    }
    $.colorbox.settings.opacity = 0.7;

    jQuery.extend(jQuery.colorbox.settings, {
        current: "image {current} sur {total}",
        previous: "pr&eacute;c&eacute;dente",
        next: "suivante",
        close: "<span style='color:#206797;'><b style='position:relative;'>x&nbsp;</b>Fermer</span>",
        xhrError: "Impossible de charger ce contenu.",
        imgError: "Impossible de charger cette image.",
        slideshowStart: "d&eacute;marrer la pr&eacute;sentation",
        slideshowStop: "arr&ecirc;ter la pr&eacute;sentation",
        onClosed: function () { parent.OuvrirPopup(lienRetour, '645', '525', 1) }
    });
    $.colorbox({
        width: largeur + "px",
        height: hauteur + "px",
        iframe: true,
        href: Lien
    });
}

function AffichageSommaire() {
    if ($('#blocSommaire').css('display') != 'none') {
        $('#blocSommaire').hide('slow', function () {
            $('#EtatAffichageSommaire').val('1');
        });
        $('#blocCorps').css('margin-left', '0px');
        $('body').css('background', 'url(images/bkgd2.png) left bottom');
    }
    else {
        $('#blocSommaire').show('slow', function () {
            $('#blocCorps').css('margin-left', '224px');
            $('body').css('background', 'url(images/bkgd.png) left bottom');
            $('#EtatAffichageSommaire').val('0');
        });
    }
}