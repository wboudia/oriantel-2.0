﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Report_1.aspx.vb" Inherits="Oriantel.Report_1" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<%--<%@ Import Namespace="System.Web.Optimization" %>--%>

<!DOCTYPE html>

<%--<html xmlns="http://www.w3.org/1999/xhtml">--%>
<!--[if lt IE 7 ]> <html class="ie6" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="" xmlns="http://www.w3.org/1999/xhtml"> <!--<![endif]-->

<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64 128x128" href="images/icones/favicon.ico" />

    <title>Oria Extranet</title>

    <%--Feuilles CSS--%>
    <link href="css/reset.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/retour.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/positionnement.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/formalize.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/table.css" rel="stylesheet" type="text/css" media="screen" />
     <link href="css/colorbox.css" rel="stylesheet" type="text/css" media="screen" />
    <%--<link href="css/font.css" rel="stylesheet" type="text/css" media="screen" />--%>
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css' />

    <link href="css/sommaire/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/onglets/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" media="screen" />

   <%-- <link rel="stylesheet" href="css/alert/alertify.default.css" />--%>
    <link rel="stylesheet" href="css/alert/alertify.bootstrap.css" />
    <link rel="stylesheet" href="css/alert/alertify.core.css" />
   

    <!--[if lt IE 9]> <link href="css/ie.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
    <!--[if IE 6]> <link href="css/ie6.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
    <!--[if IE 7]> <link href="css/ie7.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
    <!--[if IE 8]> <link href="css/ie8.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->

    <!--<style type="text/css">
            #accordeon li, #accordeon ul {
                display: block;
            }
        </style>!-->

    <%--<script type="text/javascript" src="js/Bundle.js"></script>--%>
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.formalize.min.js"></script>
    <%--<script type="text/javascript" src="js/Underscore.js"></script>--%>
    <script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
    <script type="text/javascript" src="js/jquery.customSelect.min.js"></script>
    <script type="text/javascript" src="js/jquery.fancyform.min.js"></script>
    <script type="text/javascript" src="js/jquery.tablehover.js"></script>
    <script type="text/javascript" src="js/jquery.colorbox-min.js"></script>
     

    <script type="text/javascript" src="js/Report_1.js"></script>
    <script src="js/scroll.js"></script>
    <script src="js/fixedHeaderTable/jquery.fixedheadertable.min.js" type="text/javascript"></script>
    <%-- <script type="text/javascript" src="js/gremlins.min.js"></script>
    <script src="js/jquery.konami.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="js/alertify.min.js"></script>



    <!--[if lt IE 7]>
            <script src="js/IE7FixBehaviour.js"></script>
	    <![endif]-->

    <!--[if lt IE 8]>     
        <script type="text/javascript">
	    $(document).ready(function() {
		    $('#accordeon').accordion({
			    autoHeight: false,
	            animated : false});
	    });
	    </script><![endif]-->

    <!-- BANDEAU TEMP !-->
    <%--<link rel="stylesheet" href="css/bandeau.css" type="text/css" />
    <script type="text/javascript" src="js/bandeau.js"></script>--%>
    <!-- FIN BANDEAU TEMP !-->

</head>
<body id="PageBody" runat="server" >
    <!--[if (!IE)|(gte IE 8)]><!--><img src="images/bkgd2.png"  style="display:none;" /><!--<![endif]-->

    <!-- BANDEAU TEMP !-->
    <%--<div style="display: block;" id="BandeauInfo">
     L'accès à votre extranet sera perturbé le 11/12/15 suite au déménagement de nos locaux.
     <span id="LienInfos"><a onclick="getInfos(); return false;" title="En savoir plus" alt="En savoir plus" target="_blank">En savoir plus</a></span>
     <a onclick="sessionStorage['BandeauInfo'] = 1;getConteneurInfo();return false;" title="OK" style="cursor:pointer;"><span id="ConteneurOk">OK</span></a>
    </div>
 
    <div id="ConteneurInfos" style="display: none;">
     <div id="ContenuInfos" class="texte_catalogue">
     <span style="text-decoration:underline;">Déménagement des locaux ORIA</span><br/>
    Suite au déménagement de nos locaux, l'accès à l'extranet sera interrompu durant la matinée ainsi qu'une partie de l'après-midi.<br/>Nous nous excusons pour la gêne occasionnée.
    <br/><br />
     </div>
    </div>
    <script>getConteneurInfo();</script>--%>
    <!-- FIN BANDEAU TEMP !-->

    <form id="form1" runat="server">

        <asp:ScriptManager ID="sc1" runat="server" EnablePartialRendering="true" EnableViewState="false"></asp:ScriptManager>

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(InIEvent);
        </script>

        <%-- --------------------------------------Gestion Entetes et taille------------------------------------------------------------------------------%>
        <asp:UpdatePanel ID="UpdatePanelEntetes" UpdateMode="Conditional" ChildrenAsTriggers="false" EnableHistory="false" EnableViewState="false" runat="server" OnUnload="UpdatePanel_Unload">
            <ContentTemplate>
                <asp:HiddenField ID="InstructionsEntete" Value="" runat="server" />
                <asp:HiddenField ID="ListeGridId" Value="" runat="server" />
                <asp:HiddenField ID="ListeTriTableau" Value="" runat="server" />
                <asp:HiddenField ID="PremierChargementOnglet" runat="server" />
                <asp:HiddenField ID="PossedePagination" Value="non" runat="server" />
                <asp:HiddenField ID="NombreNiveauxOnglets" Value="1" runat="server" />
                <asp:HiddenField ID="TitreTableauPresent" Value="1" runat="server" />
                <asp:HiddenField ID="BandeauMasque" Value="none" runat="server" />
                 <asp:HiddenField ID="BandeauMasqueFiltre" Value="none" runat="server" />
                <asp:HiddenField ID="TriActif" Value="" runat="server" />

                <asp:HiddenField ID="ChaineFiltreRedirection" runat="server" />
                <asp:HiddenField ID="MoisGlissant" runat="server" />
                <asp:HiddenField ID="OutilTop" runat="server" />
                <asp:HiddenField ID="Decimale" runat="server" />
                <asp:HiddenField ID="TTC" runat="server" />
                <asp:HiddenField ID="NumeroEntier" runat="server" />
                <asp:HiddenField ID="Pagination" runat="server" />
                <asp:HiddenField ID="Recherche" runat="server" />
                <asp:HiddenField ID="Seuil" runat="server" />
                <asp:HiddenField ID="Outil" runat="server" />
                <asp:HiddenField ID="Tri" runat="server" />
                <asp:HiddenField ID="Filtre" runat="server" />
                <asp:HiddenField ID="BoiteFiltre" runat="server" />
                <asp:HiddenField ID="ReferenceInsert" runat="server" />
                <asp:HiddenField ID="ReferenceCloture" runat="server" />
                <asp:HiddenField ID="ReferenceSuppressionLigne" runat="server" />
                <asp:HiddenField ID="ReferenceModificationLigne" runat="server" />
                <asp:HiddenField ID="EstPopup" runat="server" />
                <asp:HiddenField ID="TelechargementBonCommandeMobile" runat="server" />
                

                

                <asp:HiddenField ID="ReferenceSuppression" runat="server" />
                <asp:HiddenField ID="ReferenceSuppressionDetail" runat="server" />
                <asp:HiddenField ID="BloquerLigneAjout" runat="server" />
                <asp:HiddenField ID="BloquerColoneAction" runat="server" />
                <asp:HiddenField ID="Notification" runat="server" />
                 <asp:HiddenField ID="FichierTelechargement" runat="server" />


                <asp:HiddenField ID="LienTritre1Selectionne" Value="" runat="server" />
                 <asp:HiddenField ID="LienTritre2Selectionne" Value="" runat="server" />
                <asp:HiddenField ID="EtatAffichageSommaire" Value="" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <%-- --------------------------------------Fin Gestion Entetes et taille------------------------------------------------------------------------------%>


        <%-- --------------------------------------Ariane------------------------------------------------------------------------------%>
        <asp:UpdatePanel ID="UpdatePanelAriane" UpdateMode="Conditional" ChildrenAsTriggers="false" EnableHistory="false" EnableViewState="false" runat="server" OnUnload="UpdatePanel_Unload">
            <ContentTemplate>
                <div id="blocAriane">
                    <div>
                        <ul class="breadcrumb" id="ULAriane">
                            <li><a href="#" onclick="AffichageSommaire();" class="HideSommaire">&nbsp;</a></li>
                            <li id="idLiHome"><a href="Default.aspx" class="home">Home</a></li>
                            <asp:PlaceHolder ID="PlaceHolderAriane" runat="server"></asp:PlaceHolder>
                        </ul>
                    </div>
                    <asp:LinkButton ID="lnkClose" class="Deconnexion" runat="server"><asp:Image runat="server" ID="imgLnkClose" ImageUrl="Images/Icones/Deconnexion.gif" /></asp:LinkButton>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <%-- --------------------------------------Fin Ariane------------------------------------------------------------------------------%>

        <div id="bloccentre">
            <%-- --------------------------------------Sommaire-----------------------------------------------------------------------------%>
            <asp:UpdatePanel ID="UpdatePanelSommaire" UpdateMode="Conditional" RenderMode="Block" ChildrenAsTriggers="false" runat="server" OnUnload="UpdatePanel_Unload">
                <ContentTemplate>
                    <div id="blocSommaire" class="sommaire">
                        <asp:HiddenField ID="hfaccordion" runat="server" />
                        <div id="sidebar">
                            <div id="logo-partner">
                                <asp:Image ID="logoClient" runat="server" />
                            </div>
                            <asp:PlaceHolder ID="PlaceHolderTitreSommaire" runat="server"></asp:PlaceHolder>
                            <div id="sidebar-menu" class="sidebar-minheight">
                                <ul id="accordeon">
                                    <asp:PlaceHolder ID="PlaceHolderElementSommaire" runat="server"></asp:PlaceHolder>
                                </ul>
                                <%--<asp:LinkButton class="close" ID="lnkClose" runat="server">Fermer la session</asp:LinkButton>--%>
                            </div>
                        </div>
                        <div id="blocFondSommaire">
                            <span>&nbsp;</span>
                        </div>
                    </div>

                    <%--Champs de mémoire--%>



                    <asp:HiddenField ID="ValeurOngletNiv1Selectionne" Value="" runat="server" />
                    <asp:HiddenField ID="ValeurOngletNiv2Selectionne" Value="" runat="server" />
                    <asp:HiddenField ID="IdOngletNiv2Selectionne" runat="server" />
                    <asp:HiddenField ID="etapeNavigation" Value="0" runat="server" />
                    <asp:HiddenField ID="listeEtapeNavigation" Value="0" runat="server" />
                    <asp:HiddenField ID="retourActive" Value="false" runat="server" />
                    <asp:HiddenField ID="chaineFiltreRetour" Value="" runat="server" />
                    <asp:HiddenField ID="chaineArianeRetour" Value="" runat="server" />
                    <asp:HiddenField ID="autorisationEnregistrementNavigation" Value="" runat="server" />
                    <asp:HiddenField ID="idRapportTemporaireSommaire" Value="" runat="server" />
                    <asp:HiddenField ID="idModule" Value="" runat="server" />
                    <asp:HiddenField ID="idRapport" Value="" runat="server" />
                    <asp:HiddenField ID="listeIdRapports" Value="" runat="server" />
                    <asp:HiddenField ID="idcontainerParent" Value="" runat="server" />
                    <asp:HiddenField ID="IdRapportLog" Value="" runat="server" />


                </ContentTemplate>
            </asp:UpdatePanel>
            <%-- --------------------------------------Fin Sommaire------------------------------------------------------------------------------%>
            <%-- --------------------------------------Corps------------------------------------------------------------------------------%>
            <asp:UpdatePanel ID="UpdatePanelCorps" UpdateMode="Conditional" ChildrenAsTriggers="false" runat="server" EnableViewState="false" OnUnload="UpdatePanel_Unload">
                <ContentTemplate>
                    <div id="blocCorps" class="onglets" runat="server">
                        <asp:PlaceHolder ID="PlaceHolderEntetes" runat="server"></asp:PlaceHolder>

                        <div id="tabs-Niv1">
                            <asp:PlaceHolder ID="PlaceHolderBoutonArriere" runat="server"></asp:PlaceHolder>
                            <ul class="ulonglets">
                                <asp:PlaceHolder ID="PlaceHolderHidden" runat="server"></asp:PlaceHolder>
                                <asp:PlaceHolder ID="PlaceHolderTitreOnglets" runat="server"></asp:PlaceHolder>
                            </ul>
                            <asp:PlaceHolder ID="PlaceHolderDivOnglets" runat="server"></asp:PlaceHolder>

                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanelCorps"
                                DisplayAfter="0">
                                <ProgressTemplate>
                                    <div class="loading-image">
                                        <img src="images/Chargement.gif" alt="">
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanelSommaire"
                                DisplayAfter="0">
                                <ProgressTemplate>
                                    <div class="loading-image">
                                        <img src="images/Chargement.gif" alt="">
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <%--<asp:UpdateProgress ID="UpdateProgress3" runat="server" AssociatedUpdatePanelID="UpdatePanelAriane"
                                DisplayAfter="0">
                                <ProgressTemplate>
                                    <div class="loading-image">
                                        <img src="images/Chargement.gif" alt="">
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>

                        </div>



                    </div>



                    <asp:Literal ID="ltlJsFiltres" Text="" runat="server"></asp:Literal>
                    <asp:HiddenField ID="idDownloadButtonExcel" Value="" runat="server" />
                    <asp:HiddenField ID="idDownloadButtonPDF" Value="" runat="server" />
                   
                    
                </ContentTemplate>
            </asp:UpdatePanel>
            <%-- --------------------------------------Fin Corps------------------------------------------------------------------------------%>
        </div>



        <%-- --------------------------------------Footer------------------------------------------------------------------------------%>
        <asp:UpdatePanel ID="UpdatePanelFooter" UpdateMode="Conditional" ChildrenAsTriggers="false" runat="server" OnUnload="UpdatePanel_Unload">
            <ContentTemplate>
                <div id="blocFooter">
                    <div id="footer" class="clearfix">
                        <ul id="footer-menu">
                            <asp:PlaceHolder ID="PlaceHolderFooterModules" runat="server"></asp:PlaceHolder>
                        </ul>
                        <ul id="footer-links">
                            <%--<li><a href="#">Contact</a></li>--%>
                            <asp:PlaceHolder ID="PlaceHolderFooterNotice" runat="server"></asp:PlaceHolder>
                            <li class="logofoot">Oria</li>
                        </ul>
                    </div>
                </div>




            </ContentTemplate>
        </asp:UpdatePanel>
        <%--<script type="text/javascript">
            jQuery(document).ready(function () {
                // Stockage des références des différents éléments dans des variables
                ZoneFiltre = $('#ZoneFiltre_1');

                // Calcul de la marge entre le haut du document et #ZoneFiltre
                fixedLimit = ZoneFiltre.offset().top - parseFloat(ZoneFiltre.css('marginTop').replace(/auto/, 0));

                // On déclenche un événement scroll pour mettre à jour le positionnement au chargement de la page
                $(window).trigger('scroll');

                $(window).scroll(function (event) {
                    // Valeur de défilement lors du chargement de la page
                    windowScroll = $(window).scrollTop();

                    // Mise à jour du positionnement en fonction du scroll
                    if (windowScroll >= fixedLimit) {
                        ZoneFiltre.addClass('SuiviZoneFiltre');
                    } else {
                        ZoneFiltre.removeClass('SuiviZoneFiltre');
                    }
                });
            });
        </script>--%>
        <%--<script type="text/javascript">
            jQuery(document).ready(function () {
                PlacementTableau();
            });
        </script>--%>
        <%-- --------------------------------------Fin Footer------------------------------------------------------------------------------%>

        <div id="cboxOverlay" style="opacity: 0; cursor: pointer; visibility: visible; display: none;"></div>

    </form>
    <%--<script type="text/javascript">
        function InitializeRequest(sender, args) {
            if (sender._postBackSettings.sourceElement.id == "idDownloadFile") {
                var iframe = document.createElement("iframe");
                var checkboxMoisGlissant = document.getElementById("checkbox_MoisGlissant" + "_" + document.getElementById("idcontainerParent").value + "--" + document.getElementById("listeIdRapports").value).checked;
                var checkboxTTC = document.getElementById("checkbox_TTC" + "_" + document.getElementById("idcontainerParent").value + "--" + document.getElementById("listeIdRapports").value).checked;
                iframe.src = "GenerateFile.aspx?listeIdRapports=" + document.getElementById("listeIdRapports").value + "&idModule=" + document.getElementById("idModule").value + "&BloquerColonneAction=" + document.getElementById("BloquerColoneAction").value + "&idcontainerParent=" + document.getElementById("idcontainerParent").value + "&MoisGlissant=" + checkboxMoisGlissant + "&TTC=" + checkboxTTC;
                iframe.style.display = "none";
                document.body.appendChild(iframe);
            }
        }
    </script>--%>
    <%--<script type="text/javascript">
        var horde = gremlins.createHorde();
        horde.unleash();
    </script>--%>
</body>
</html>
