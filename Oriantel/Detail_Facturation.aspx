﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Detail_Facturation.aspx.vb" Inherits="Oriantel.Detail_Facturation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <title>Oria Extranet</title>

    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css' />
    <link href="css/retour.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="css/DetailFacturation.css" rel="stylesheet" type="text/css" media="screen" />

    <style type="text/css">
        #divDatesFactures {
            width: 603px;
        }
        #form1 {
            width: 604px;
            height: 603px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 614px; height: 608px; float: left">
            <div style="width: 606px; height: 32px; float: left">
                <h1 style="text-align: center;width: 100%;">
                    <span style="font-size: 14pt; text-transform: uppercase;">
                        <asp:PlaceHolder ID="PlaceHolderTitre" runat="server"></asp:PlaceHolder>


                    </span>&nbsp;<br />
                    <span style="font-size: 7pt">&nbsp;</span><br>
                    <asp:Image ID="Image1" runat="server" Height="1px" ImageUrl="Images/detailFacturation/hb.png" Width="320px" /></h1>

            </div>

            <a id="boutonretour" runat="server" class="historyback" href="javascript:history.go(-1)">Allez à la page précédente</a>

            <img src="images/detailFacturation/cadre_info_fact-3.png" style="z-index: 99; left: 12px; position: absolute; top: 98px; height: 195px; width: 603px; right: 574px;" />
             <asp:Label ID="lblBoiteDetailFacture" runat="server" Height="16px"  Style="left: 20px; position: absolute; top: 106px; z-index: 102; right: 919px;" Width="250px">Informations relatives à la facture : </asp:Label>
            <%--Operateur--%>
            <p align="left">
                <asp:Label ID="Label8" runat="server" Height="16px"
                    Width="115px" Style="left: 20px; position: absolute; top: 136px; z-index: 102; ">Opérateur : </asp:Label>
                <asp:Label ID="STV_Operateur" runat="server" 
                    Width="264px" Style="left: 135px; position: absolute; top: 136px; z-index: 103;"></asp:Label>
                &nbsp;
            </p>
            <%--Montant HT--%>
            <p align="left">
                <asp:Label ID="Label9" runat="server"
                    Width="115px" Style="left: 20px; position: absolute; top: 166px; z-index: 104; " Height="16px">Montant HT:</asp:Label>
                <asp:Label ID="STV_MontantHT" runat="server" 
                    Style="left: 135px; position: absolute; top: 166px; z-index: 105; height: 15px; width: 274px;"></asp:Label>
            </p>
            <%--Montant TTC--%>
            <p align="left">
                <asp:Label ID="Label2" runat="server"
                    Width="115px" Style="left: 20px; position: absolute; top: 190px; z-index: 104; right: 1054px;" Height="16px">Montant TTC:</asp:Label>
                <asp:Label ID="STV_MontantTTC" runat="server"
                    Style="left: 135px; position: absolute; top: 190px; z-index: 107; height: 10px; width: 107px;"></asp:Label>
            </p>
            <%--Montant Paye--%>
            <p align="left">
                <asp:Label ID="lblMontantPaye" runat="server"
                    Width="115px" Style="left: 20px; position: absolute; top: 214px; z-index: 104; right: 1054px;" Height="16px">Montant  payé HT:</asp:Label>
                <asp:Label ID="STV_MontantPaye" runat="server"
                    Style="position: absolute; top: 214px; z-index: 104; left: 135px; height: 20px; width: 272px;"></asp:Label>
            </p>
              <%--Date Facure--%>
            <p align="left">
                <asp:Label ID="Label3" runat="server"
                    Width="115px" Style="left: 20px; position: absolute; top: 242px; z-index: 104; right: 1054px;" Height="16px">Date facture:</asp:Label>
                <asp:Label ID="STV_DateFacture" runat="server"
                    Style="position: absolute; top: 242px; z-index: 104; left: 135px; height: 20px; width: 272px;"></asp:Label>
            </p>
            <%--Statut--%>
            <p align="left">
                <asp:Label ID="Label1" runat="server"
                    Height="16px" Style="z-index: 115; left: 20px; position: absolute; top: 269px"
                    Width="115px">Statut :</asp:Label>
                 <img id="ImgStatut" runat="server" src="blank" style="z-index: 99; left: 135px; position: absolute; top: 269px; height: 15px; width: 15px; " />
                <asp:Label ID="STV_Statut" runat="server" Height="11px" Style="left: 162px; position: absolute; top: 269px; z-index: 109; width: 445px;"></asp:Label>
            </p>

            <%--Pieces telechargeables--%>

             <asp:Label ID="lblBoitePiecesDisponibles" runat="server" Height="16px"  Style="left: 417px; position: absolute; top: 155px; z-index: 102; right: 609px; width: 163px;">Pièces disponibles : </asp:Label>
            <asp:ImageButton ID="BP_Annexe" runat="server" ImageUrl="~/images/detailFacturation/bouton_annexe.png"
                Style="z-index: 118; left: 425px; position: absolute; top: 186px;  " />
            <asp:Label ID="lblAnnexePapier" runat="server"
                Style="left: 415px; position: absolute; top: 242px; z-index: 104; right: 593px; height: 22px; width: 66px;"
                Font-Underline="True">Annexe comptable</asp:Label>
            
            <a id="LienFacture" runat="server" href ="" target="_blank"><IMG runat="server" ID="Img_Facture" src="images/detailFacturation/icone_pdf.png"  Style="left: 524px; position: absolute; top: 190px; z-index: 114; height: 46px; width: 50px;" /></a>
            <asp:Label ID="lblFacturePDF" runat="server"
                Style="left: 521px; position: absolute; top: 242px; z-index: 104; right: 495px; height: 14px; width: 58px;"
                Font-Underline="True">Format PDF</asp:Label>

            <%-- Dates--%>
            <div id="divDatesFactures" runat="server">
                <img src="images/detailFacturation/cadre_date_fact-3.png" style="left: 12px; position: absolute; top: 303px;  width: 603px; z-index: 103;" />
                <asp:Label ID="lblBoiteDatesFactures" runat="server" Height="16px" Width="115px" Style="left: 20px; position: absolute; top:310px; z-index: 104; right: 1054px;">Dates : </asp:Label>
                <p align="left" style="text-align: left">
                    <div style="left: 18px; position: absolute; top: 338px; z-index: 111;">
                        <asp:PlaceHolder ID="PlaceHolderFacturesDates" runat="server"></asp:PlaceHolder>
                    </div>
                </p>
            </div>

            <%-- Factures Liees--%>
            <div id="divFacturesLiees" runat="server">
                <asp:Label ID="lblBoiteFacturesLiees" runat="server" Height="16px" Width="115px" Style="left: 20px; position: absolute; top:472px; z-index: 102; right: 1054px;">Documents liés : </asp:Label>
                <img src="images/detailFacturation/cadre_fact_liees-3.png" style="left: 12px; position: absolute; top: 465px; z-index: 101;  width: 603px;" />
                <p align="left">
                    <div style="left: 18px; position: absolute; top: 500px; z-index: 108;">
                        <asp:PlaceHolder ID="PlaceHolderFacturesLiees" runat="server"></asp:PlaceHolder>
                    </div>
                </p>
            </div>
        </div>
               
    </form>
</body>
</html>
