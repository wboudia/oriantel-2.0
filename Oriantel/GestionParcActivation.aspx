﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GestionParcActivation.aspx.vb" Inherits="Oriantel.GestionParcActivation" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html>

<%--<html xmlns="http://www.w3.org/1999/xhtml">--%>
<!--[if lt IE 7 ]> <html class="ie6" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->


<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64 128x128" href="../images/icones/favicon.ico" />

    <title>Oria Extranet</title>

    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css' />
    <link href="css/gestionParc.css" rel="stylesheet" type="text/css" media="screen" />
    
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="js/jquery.ui.datepicker-fr.js"></script>
    <link href="js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.css" rel="stylesheet" />

     <script>
     $(function() {
         $("#TxtDateActivation").datepicker($.datepicker.regional["fr"]);
    });
    </script>
</head>
<body style="width: 285px; height: 204px">
    <form id="form1" runat="server">

        <div id="DivArticle" style="position: absolute; top: 30px; left: 5px; height: 80px; width: 280px;">
            <img src="images/GestionParc/cadre_activation.png" style="z-index: 99; left: 5px; position: absolute; top: 0px; height: 80px; width: 280px; right: -5px;" />
            <asp:Label ID="lblBoiteActivation" runat="server" Height="16px" Style="left: 10px; position: absolute; top: 5px; z-index: 102; right: 919px;" Width="250px">Activation : </asp:Label>
            <p>
                <asp:Label ID="Label1" runat="server" Text="Numéro" Height="16px" Width="115px" Style="left: 10px; position: absolute; top: 34px; z-index: 102;"></asp:Label>
                <asp:TextBox ID="TxtNumeroGSM"  Class="champs" runat="server" Style="left: 105px;height:14px; position: absolute; top: 34px; z-index: 103; width: 144px;"></asp:TextBox>
            </p>
            <p >
                <asp:Label ID="Label2" runat="server" Text="Date d'activation" Height="16px" Width="115px" Style="left: 10px; position: absolute; top: 54px; z-index: 102;"></asp:Label>
                <asp:TextBox ID="TxtDateActivation" Class="champs" runat="server" Style="left: 105px;height:14px; position: absolute; top: 54px; z-index: 103; width: 144px;"></asp:TextBox>
            </p>
        </div>



        <div id="DivAction" runat="server" style="display: block; position: absolute; top: 130px; left: 5px; height: 60px; width: 280px;">
            <img src="images/GestionParc/cadre_action.png" style="z-index: 99; left: 5px; position: absolute; top: 0px; height: 60px; width: 280px; right: -180px;" />
            <asp:Label ID="lblBoiteAction" runat="server" Height="16px" Style="left: 10px; position: absolute; top: 5px; z-index: 102; right: 919px;" Width="250px">Actions : </asp:Label>

            <div id="DivActionDelete" style="display: block;" runat="server">
                <asp:Button ID="BtnActiverValider" runat="server" Text="Valider" Style="left: 65px; position: absolute; top: 34px; z-index: 102;" />
                <asp:Button ID="BtnActiverAnnuler" runat="server" Text="Annuler" Style="left: 157px; position: absolute; top: 34px; z-index: 102;" />
            </div>
           
        </div>
 
        <div id="DivInformation" runat="server" style=" position: absolute; top: 200px; left: 12px; height: 20px; width: 280px;">
            <p style="width: 270px">
                <asp:Label ID="LblInformation" Visible="true" runat="server" Text="" Style="position: absolute; left: 1px; top: 0px"></asp:Label>
            </p>
        </div>

       
    </form>
</body>
</html>
