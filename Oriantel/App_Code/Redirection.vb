﻿Public Class Redirection

    Private _nIdRedirection As Integer
    Public Property nIdRedirection() As Integer
        Get
            Return _nIdRedirection
        End Get
        Set(ByVal value As Integer)
            _nIdRedirection = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Private _nidColonne As Integer
    Public Property nidColonne() As Integer
        Get
            Return _nidColonne
        End Get
        Set(ByVal value As Integer)
            _nidColonne = value
        End Set
    End Property

    Private _sChaineFiltre As String
    Public Property sChaineFiltre() As String
        Get
            Return _sChaineFiltre
        End Get
        Set(ByVal value As String)
            _sChaineFiltre = value
        End Set
    End Property

    Private _nIdRapportCible As Integer
    Public Property nIdRapportCible() As Integer
        Get
            Return _nIdRapportCible
        End Get
        Set(ByVal value As Integer)
            _nIdRapportCible = value
        End Set
    End Property

    Private _sPageRapportCible As String
    Public Property sPageRapportCible() As String
        Get
            Return _sPageRapportCible
        End Get
        Set(ByVal value As String)
            _sPageRapportCible = value
        End Set
    End Property

    Private _bEstPopup As Boolean
    Public Property bEstPopup() As Boolean
        Get
            Return _bEstPopup
        End Get
        Set(ByVal value As Boolean)
            _bEstPopup = value
        End Set
    End Property

    Private _sLargueurPopup As String
    Public Property sLargueurPopup() As String
        Get
            Return _sLargueurPopup
        End Get
        Set(ByVal value As String)
            _sLargueurPopup = value
        End Set
    End Property

    Private _sHauteurPopup As String
    Public Property sHauteurPopup() As String
        Get
            Return _sHauteurPopup
        End Get
        Set(ByVal value As String)
            _sHauteurPopup = value
        End Set
    End Property

    Private _bRafraichissementFermeturePopup As Boolean
    Public Property bRafraichissementFermeturePopup() As Boolean
        Get
            Return _bRafraichissementFermeturePopup
        End Get
        Set(ByVal value As Boolean)
            _bRafraichissementFermeturePopup = value
        End Set
    End Property

    Private _bEstListeMois As Boolean
    Public Property bEstListeMois() As Boolean
        Get
            Return _bEstListeMois
        End Get
        Set(ByVal value As Boolean)
            _bEstListeMois = value
        End Set
    End Property
End Class
