﻿Public Class Client

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Private _sNomClient As String
    Public Property sNomClient() As String
        Get
            Return _sNomClient
        End Get
        Set(ByVal value As String)
            _sNomClient = value
        End Set
    End Property

    Private _sEnteteExport As String
    Public Property sEnteteExport() As String
        Get
            Return _sEnteteExport
        End Get
        Set(ByVal value As String)
            _sEnteteExport = value
        End Set
    End Property

    Private _sLogo As String
    Public Property sLogo() As String
        Get
            Return _sLogo
        End Get
        Set(ByVal value As String)
            _sLogo = value
        End Set
    End Property

    Private _sBaseClient As String
    Public Property sBaseClient() As String
        Get
            Return _sBaseClient
        End Get
        Set(ByVal value As String)
            _sBaseClient = value
        End Set
    End Property

    Private _sMonnaieClient As String
    Public Property sMonnaieClient() As String
        Get
            Return _sMonnaieClient
        End Get
        Set(ByVal value As String)
            _sMonnaieClient = value
        End Set
    End Property

    Private _sPrefixeClient As String
    Public Property sPrefixeClient() As String
        Get
            Return _sPrefixeClient
        End Get
        Set(ByVal value As String)
            _sPrefixeClient = value
        End Set
    End Property

    Private _oMailings As Mailings
    Public Property oMailings() As Mailings
        Get
            Return _oMailings
        End Get
        Set(ByVal value As Mailings)
            _oMailings = value
        End Set
    End Property

    Private _sLangueClient As String
    Public Property sLangueClient() As String
        Get
            Return _sLangueClient
        End Get
        Set(ByVal value As String)
            _sLangueClient = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal idClient As Integer)
        nIdClient = idClient
    End Sub

    Public Sub New(ByVal idClient As Integer, ByVal nomClient As String, ByVal enteteExport As String, ByVal logo As String, ByVal baseClient As String, ByVal prefixeClient As String, ByVal monnaieClient As String)
        nIdClient = idClient
        sNomClient = nomClient
        sEnteteExport = enteteExport
        sLogo = logo
        sBaseClient = baseClient
        sPrefixeClient = prefixeClient
        sMonnaieClient = monnaieClient
    End Sub

    Public Sub chargerClientParId(ByVal idClient As Integer)
        cDal.chargerClientParId(Me, idClient)
    End Sub

    Public Sub RecupererLangueClient()
        cDal.RecupererLangueClient(Me)
    End Sub

End Class
