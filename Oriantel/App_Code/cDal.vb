﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.HttpContext
Imports System.Web.UI
Imports System.Web.Security
Imports System.Diagnostics
Imports System.Web
Imports Oriantel

Public Class cDal

    Public Shared Sub CreationSessionUtilisateur(ByRef existe As Boolean, ByRef pLogin As String, ByRef pPass As String)
        Dim sql As New cSQL("Administration")
        Dim requetePrincipale As String = "SELECT idUtilisateur, idClient, login, nom, prenom, civilite, mail, dateCreation, dateSuppression FROM Utilisateur WHERE login = '" & pLogin & "' AND password = '" & pPass & "' "
        Dim readerSQL As SqlDataReader = Nothing
        Dim oClient As Client

        Try
            readerSQL = sql.chargerDataReader(requetePrincipale)

            If readerSQL.HasRows = False Then
                readerSQL.Close()
                Dim requeteUserSecond As String = "SELECT idClient, idUtilisateur, login, nom, prenom, civilite, mail, dateCreation, dateSuppression, idUtilisateurPrimaire FROM UtilisateurSecondaire WHERE login = '" & pLogin & "' AND password = '" & pPass & "' "
                readerSQL = sql.chargerDataReader(requeteUserSecond)

                If readerSQL.HasRows = True Then
                    Dim oUtilisateurSecond As New UtilisateurSecondaire()

                    Do Until Not readerSQL.Read()
                        oClient = New Client
                        oClient.chargerClientParId(CInt(readerSQL("idClient")))
                        ChargerPropriete(oUtilisateurSecond.oClient, oClient)
                        ChargerPropriete(oUtilisateurSecond.nIdUtilisateur, readerSQL("idUtilisateur"))
                        ChargerPropriete(oUtilisateurSecond.sLogin, readerSQL("login"))
                        ChargerPropriete(oUtilisateurSecond.sNom, readerSQL("nom"))
                        ChargerPropriete(oUtilisateurSecond.sPrenom, readerSQL("prenom"))
                        ChargerPropriete(oUtilisateurSecond.sCivilite, readerSQL("civilite"))
                        ChargerPropriete(oUtilisateurSecond.dDateCreation, readerSQL("dateCreation"))
                        ChargerPropriete(oUtilisateurSecond.dDateSuppression, readerSQL("dateSuppression"))
                        ChargerPropriete(oUtilisateurSecond.oUtilisateur.nIdUtilisateur, readerSQL("idUtilisateurPrimaire"))
                        If Not oUtilisateurSecond.oUtilisateur.nIdUtilisateur = Nothing Then
                            oUtilisateurSecond.oUtilisateur.chargerUtilisateurParId(oUtilisateurSecond.oUtilisateur.nIdUtilisateur, oClient.nIdClient)

                            existe = True
                        End If

                        Exit Do
                    Loop

                    chargerDonneesUtilisateurPrincipalSession(oUtilisateurSecond.dDateSuppression, oUtilisateurSecond.sCivilite, oUtilisateurSecond.sPrenom, oUtilisateurSecond.sNom, oUtilisateurSecond.sLogin, oUtilisateurSecond.oUtilisateur.nIdUtilisateur, oUtilisateurSecond.oClient.nIdClient.ToString, existe, oUtilisateurSecond.nIdUtilisateur.ToString)

                End If
            Else
                Dim oUtilisateur As New Utilisateur()

                Do Until Not readerSQL.Read()
                    oClient = New Client
                    oClient.chargerClientParId(CInt(readerSQL("idClient")))
                    ChargerPropriete(oUtilisateur.oClient, oClient)
                    ChargerPropriete(oUtilisateur.nIdUtilisateur, readerSQL("idUtilisateur"))
                    ChargerPropriete(oUtilisateur.sNom, readerSQL("nom"))
                    ChargerPropriete(oUtilisateur.sPrenom, readerSQL("prenom"))
                    ChargerPropriete(oUtilisateur.sCivilite, readerSQL("civilite"))
                    ChargerPropriete(oUtilisateur.dDateCreation, readerSQL("dateCreation"))
                    ChargerPropriete(oUtilisateur.dDateSuppression, readerSQL("dateSuppression"))

                Loop

                existe = True

                chargerDonneesUtilisateurPrincipalSession(oUtilisateur.dDateSuppression, oUtilisateur.sCivilite, oUtilisateur.sPrenom, oUtilisateur.sNom, oUtilisateur.sLogin, oUtilisateur.nIdUtilisateur, oUtilisateur.oClient.nIdClient.ToString, existe)
            End If
            readerSQL.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Private Shared Sub chargerDonneesUtilisateurPrincipalSession(ByVal dDateSuppression As DateTime, ByVal sCivilite As String, ByVal sPrenom As String, ByVal sNom As String, ByVal sLogin As String, ByVal sIdUtilisateur As Integer, ByVal sIdClient As String, ByRef existe As Boolean, Optional ByVal sIdUtilisateurSecondaire As String = "0")
        Dim bCreationSession = False

        ' Vérification que le compte de l'utilisateur n'est pas fermé
        If Not dDateSuppression = Nothing Then
            If DateTime.Compare(DateTime.Today, dDateSuppression) < 0 Then
                bCreationSession = True
            End If
        Else
            bCreationSession = True
        End If

        If bCreationSession = True Then
            Dim nomComplet As String = sCivilite & sPrenom & sNom
            Current.Session.Add("nomUtilisateur", nomComplet)
            Current.Session.Add("idUtilisateur", sIdUtilisateur)
            Current.Session.Add("login", sLogin)
            Current.Session.Add("idUtilisateurSecondaire", sIdUtilisateurSecondaire)
            Current.Session.Add("idClient", sIdClient)

            existe = True
        Else
            existe = False
        End If

    End Sub

    Public Shared Sub createModuleConnexion(ByRef oModuleConnexion As Modul)
        Dim sql As New cSQL("Administration")
        Dim requete As String = "SELECT idModule, titre, detail, lien, base, libelleClasse FROM Module WHERE Titre='Connexion'"
        Dim readerSQL As SqlDataReader = Nothing
        oModuleConnexion.nIdModule = 0

        Try
            readerSQL = sql.chargerDataReader(requete)
            Do Until Not readerSQL.Read()

                If Not IsDBNull(readerSQL.Item("idModule")) Then
                    oModuleConnexion.nIdModule = CShort(readerSQL.Item("idModule"))
                End If
                If Not IsDBNull(readerSQL.Item("titre")) Then
                    oModuleConnexion.sTitre = CStr(readerSQL.Item("titre"))
                End If
                If Not IsDBNull(readerSQL.Item("detail")) Then
                    oModuleConnexion.sDetail = CStr(readerSQL.Item("detail"))
                End If
                If Not IsDBNull(readerSQL.Item("lien")) Then
                    oModuleConnexion.sLien = CStr(readerSQL.Item("lien"))
                End If
                If Not IsDBNull(readerSQL.Item("base")) Then
                    oModuleConnexion.sBase = CStr(readerSQL.Item("base"))
                End If
                If Not IsDBNull(readerSQL.Item("libelleClasse")) Then
                    oModuleConnexion.sLibelleClasse = CStr(readerSQL.Item("libelleClasse"))
                End If

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub insertionInformationsUtilisateurDansLog(ByRef idclient As Integer, ByRef idUtilisateur As Integer, ByRef idModule As Integer, ByRef sBrowserName As String, ByRef sBrowserVersion As UInt16, ByRef sResolution As String, ByRef sIp As String, ByRef nIdUtilisateurSecondaire As Integer)

        Dim transaction As SqlTransaction
        Dim requete As String = "INSERT INTO [Administration].[dbo].[Logs] ([idClient],[idUtilisateur],[idModule],[ip],[dateHeure],[resolution],[navigateur],[navigateurVersion],[idUtilisateurSecondaire]) " _
            & "VALUES (@idClient,@idUtilisateur,@idModule,@ip,@dateTime,@resolution,@browserName,@browserVersion,@nIdUtilisateurSecondaire)"

        Dim sql As New cSQL("Administration"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@idclient", SqlDbType.Int).Value = idclient
            cmd.Parameters.Add("@idUtilisateur", SqlDbType.Int).Value = idUtilisateur
            cmd.Parameters.Add("@idModule", SqlDbType.Int).Value = idModule
            cmd.Parameters.Add("@ip", SqlDbType.VarChar, 50).Value = sIp
            cmd.Parameters.Add("@dateTime", SqlDbType.DateTime).Value = DateTime.Now
            cmd.Parameters.Add("@resolution", SqlDbType.VarChar, 50).Value = sResolution
            cmd.Parameters.Add("@browserName", SqlDbType.VarChar, 30).Value = sBrowserName
            cmd.Parameters.Add("@browserVersion", SqlDbType.SmallInt).Value = sBrowserVersion
            cmd.Parameters.Add("@nIdUtilisateurSecondaire", SqlDbType.Int).Value = nIdUtilisateurSecondaire

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub InsertionHistoriqueCommandeFixe(ByRef oHistoriqueCommandeFixe As HistoriqueCommandeFixe)
        Dim transaction As SqlTransaction
        Dim requete As String = "INSERT INTO dbo.HistoriqueCommande (idCommande,numeroCommande,typeCommande,estRealiseFacture,estTelecharge,idLigne "
        requete += ",accesReseau_contratLigne,accesReseau_numero,accesReseau_resilliationPartielleInstallation,accesReseau_nombreAcces,accesReseau_nombreSda "
        requete += ",accesReseau_specialisationCanauxArrivee,accesReseau_specialisationCanauxMixte,accesReseau_specialisationCanauxDepart,adresseInstallation_nomEtablissement "
        requete += ",adresseInstallation_adresse,adresseInstallation_codePostal,adresseInstallation_localite,adresseInstallation_complementAdresse,contactInstallation_nom "
        requete += ",contactInstallation_mobile,contactInstallation_telephone,contactInstallation_email,"

        requete += Toolbox.AjouterChampRequete("dateDisposition_dateDispositionLocaux", "DATE", oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.dDateDispositionLocaux)
        requete += Toolbox.AjouterChampRequete("dateDisposition_dateMiseService", "DATE", oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.dDateMiseEnService)


        requete += " facturation_compteFacturantLigne, restitutionEquipement, servicesTelephonie_MessagerieVocale, servicesTelephonie_TransfertAppel, servicesTelephonie_SecretPermanent "
        requete += ", servicesTelephonie_TransfertAppelNonReponse, servicesTelephonie_NumeroRenvoi, servicesTelephonie_RenvoiTerminal, servicesTelephonie_TypeSelection, annuaire_ChoixListe "
        requete += ", annuaire_PrecisionsParution, choixModem_typeModem, adresseEmail_type, adresseEmail_adresse, commentaires, idUtilisateurPrincipal, idUtilisateurSecondaire "
        requete += ","

        requete += Toolbox.AjouterChampRequete("dateCommande", "DATE", oHistoriqueCommandeFixe.oCommandeFixe.dDateCommande)

        requete += " referenceClient) "
        requete += "VALUES "
        requete += "(@nIdCommande,@sNumeroCommande,@sTypeCommande,@bEstRealiseFacture,@bEstTelecharge,@nIdLigne,@sAccesReseauContratLigne "
        requete += ",@sAccesReseauNumero,@bAccesReseauResiliationPartielleInstallation,@nAccesReseauNombreAcces,@nAccesReseauNombreSDA,@nAccesReseauSpecialisationCanauxArrivee "
        requete += ",@nAccesReseauSpecialisationCanauxMixte,@nAccesReseauSpecialisationCanauxDepart,@sAdresseInstallationNomEtablissement,@sAdresseInstallationAdresse,@sAdresseInstallationCodePostal "
        requete += ",@sAdresseInstallationLocalite,@sAdresseInstallationComplementAdresse,@sContactInstallationNom,@sContactInstallationMobile,@sContactInstallationTelephone,@sContactInstallationEmail, "

        requete += Toolbox.AjouterChampRequete("@dDateDispositionDateDispositionLocaux", "DATE", oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.dDateDispositionLocaux)
        requete += Toolbox.AjouterChampRequete("@dDateDispositionDateMiseService", "DATE", oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.dDateMiseEnService)

        requete += "@sFacturationCompteFacturantLigne,@bRestitutionEquipement,@bServicesTelephonieMessagerieVocale,@bServicesTelephonieTransfertAppel "
        requete += ",@bServicesTelephonieSecretPermanent,@bServicesTelephonieTransfertAppelNonReponse,@sServicesTelephonieNumeroRenvoi,@bServicesTelephonieRenvoiTerminal,@sServicesTelephonieTypeSelection,@sAnnuaireChoixListe "
        requete += ",@sAnnuairePrecisionsParution,@sChoixModemTypeModem,@sAdresseEmailType,@sAdresseEmailAdresse,@sCommentaires,@nIdUtilisateurPrincipal,@nIdUtilisateurSecondaire,"

        requete += Toolbox.AjouterChampRequete("@dDateCommande", "DATE", oHistoriqueCommandeFixe.oCommandeFixe.dDateCommande)

        requete += "@sReferenceClient"

        Dim sql As New cSQL("GestionParcFixe"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@nIdCommande", SqlDbType.Int).Value = oHistoriqueCommandeFixe.oCommandeFixe.nIdCommande
            cmd.Parameters.Add("@sNumeroCommande", SqlDbType.VarChar, 50).Value = oHistoriqueCommandeFixe.oCommandeFixe.sNumeroCommande
            cmd.Parameters.Add("@sTypeCommande", SqlDbType.VarChar, 50).Value = oHistoriqueCommandeFixe.oCommandeFixe.sTypeCommande
            cmd.Parameters.Add("@bEstRealiseFacture", SqlDbType.Bit).Value = oHistoriqueCommandeFixe.oCommandeFixe.bEstRealiseFacture
            cmd.Parameters.Add("@bEstTelecharge", SqlDbType.Bit).Value = oHistoriqueCommandeFixe.oCommandeFixe.bEstTelecharge
            cmd.Parameters.Add("@nIdLigne", SqlDbType.Int).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.nIdLigneFixe
            cmd.Parameters.Add("@sAccesReseauContratLigne", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.oContratFixe.sLibelleContrat
            cmd.Parameters.Add("@sAccesReseauNumero", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sNumeroLigneFixe
            cmd.Parameters.Add("@bAccesReseauResiliationPartielleInstallation", SqlDbType.Bit).Value = oHistoriqueCommandeFixe.oCommandeFixe.bResiliationPartielle
            cmd.Parameters.Add("@nAccesReseauNombreAcces", SqlDbType.Int).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.nNombreAcces
            cmd.Parameters.Add("@nAccesReseauNombreSDA", SqlDbType.Int).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.nNombreSDA
            cmd.Parameters.Add("@nAccesReseauSpecialisationCanauxArrivee", SqlDbType.Int).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.nSpecialisationCanauxArrive
            cmd.Parameters.Add("@nAccesReseauSpecialisationCanauxMixte", SqlDbType.Int).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.nSpecialisationCanauxMixte
            cmd.Parameters.Add("@nAccesReseauSpecialisationCanauxDepart", SqlDbType.Int).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.nSpecialisationCanauxDepart
            cmd.Parameters.Add("@sAdresseInstallationNomEtablissement", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sNomEtablissement
            cmd.Parameters.Add("@sAdresseInstallationAdresse", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sAdresse
            cmd.Parameters.Add("@sAdresseInstallationCodePostal", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sCodePostal
            cmd.Parameters.Add("@sAdresseInstallationLocalite", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sLocalite
            cmd.Parameters.Add("@sAdresseInstallationComplementAdresse", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sComplementAdresse
            cmd.Parameters.Add("@sContactInstallationNom", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sNomContact
            cmd.Parameters.Add("@sContactInstallationMobile", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sMobileContact
            cmd.Parameters.Add("@sContactInstallationTelephone", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sFixeContact
            cmd.Parameters.Add("@sContactInstallationEmail", SqlDbType.VarChar).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sMailContact

            Toolbox.AjouterParametreRequete(cmd, "@dDateDispositionDateDispositionLocaux", "DATE", oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.dDateDispositionLocaux)
            Toolbox.AjouterParametreRequete(cmd, "@dDateDispositionDateMiseService", "DATE", oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.dDateMiseEnService)


            cmd.Parameters.Add("@sFacturationCompteFacturantLigne", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.oCompteFacturantFixe.sLibelleCompteFacturant
            cmd.Parameters.Add("@bRestitutionEquipement", SqlDbType.Bit).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.bRestitutionEquipementLoues
            cmd.Parameters.Add("@bServicesTelephonieMessagerieVocale", SqlDbType.Bit).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.bStMessagerieVocale
            cmd.Parameters.Add("@bServicesTelephonieTransfertAppel", SqlDbType.Bit).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.bStTransfertAppel
            cmd.Parameters.Add("@bServicesTelephonieSecretPermanent", SqlDbType.Bit).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.bStSecretPermanent
            cmd.Parameters.Add("@bServicesTelephonieTransfertAppelNonReponse", SqlDbType.Bit).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.bStTransfertAppelNonReponse
            cmd.Parameters.Add("@sServicesTelephonieNumeroRenvoi", SqlDbType.VarChar, 50).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sStNumeroRenvoi
            cmd.Parameters.Add("@bServicesTelephonieRenvoiTerminal", SqlDbType.Bit).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.bStRenvoiTerminal
            cmd.Parameters.Add("@sServicesTelephonieTypeSelection", SqlDbType.VarChar, 50).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.oTypeSelectionFixe.sLibelleTypeSelection
            cmd.Parameters.Add("@sAnnuaireChoixListe", SqlDbType.VarChar, 50).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.oChoixListeFixe.sLibelleChoixListe
            cmd.Parameters.Add("@sAnnuairePrecisionsParution", SqlDbType.VarChar, -1).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sIaPrecisionsComplementairesParution
            cmd.Parameters.Add("@sChoixModemTypeModem", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sChoixModem
            cmd.Parameters.Add("@sAdresseEmailType", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sTypeAdresseEmail
            cmd.Parameters.Add("@sAdresseEmailAdresse", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sAdresseEmail
            cmd.Parameters.Add("@sCommentaires", SqlDbType.VarChar, -1).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sCommentaire
            cmd.Parameters.Add("@nIdUtilisateurPrincipal", SqlDbType.Int).Value = oHistoriqueCommandeFixe.oCommandeFixe.nIdUtilisateurPrincipal
            cmd.Parameters.Add("@nIdUtilisateurSecondaire", SqlDbType.Int).Value = oHistoriqueCommandeFixe.oCommandeFixe.nIdUtilisateurSecondaire

            Toolbox.AjouterParametreRequete(cmd, "@dDateCommande", "DATE", oHistoriqueCommandeFixe.oCommandeFixe.dDateCommande)


            cmd.Parameters.Add("@sReferenceClient", SqlDbType.VarChar, 250).Value = oHistoriqueCommandeFixe.oCommandeFixe.oLigneFixe.sReferenceClient

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub insertionInformationsUtilisateurDansLog(ByRef idClient As Integer, ByRef idUtilisateur As Integer, ByRef idModule As Integer, ByRef sBrowserName As String, ByRef sBrowserVersion As UInt16, ByRef sResolution As String, ByRef sIp As String, ByRef nIdRapport As Integer, ByRef nIdUtilisateurSecondaire As Integer)

        Dim sql As New cSQL("Administration")
        Dim requete As String = "INSERT INTO [Administration].[dbo].[Logs] ([idClient],[idUtilisateur],[idModule],[ip],[dateHeure],[resolution],[navigateur],[navigateurVersion],[idRapport], [idUtilisateurSecondaire]) " _
            & "VALUES (" & CStr(idClient) & "," & CStr(idUtilisateur) & "," & CStr(idModule) & ",'" & sIp & "','" & DateTime.Now & "','" & sResolution & "','" & sBrowserName & "'," & sBrowserVersion & "," & CStr(nIdRapport) & "," & CStr(nIdUtilisateurSecondaire) & ")"

        Try
            sql.executerRequete(requete)
        Catch ex As Exception
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub chargerValeurMemoireSeuil(ByRef oMemoireSeuil As MemoireSeuil)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try

            oMemoireSeuil.sValeur = ""

            Requete = "SELECT valeur"
            Requete &= " FROM MemoireSeuil"
            Requete &= " WHERE idClient =" + oMemoireSeuil.nIdClient.ToString
            Requete &= " AND idUtilisateur = " + oMemoireSeuil.nIdUtilisateur.ToString
            Requete &= " AND idUtilisateurSecondaire = " + oMemoireSeuil.nIdUtilisateurSecondaire.ToString
            Requete &= " AND idModule = " + oMemoireSeuil.nIdModule.ToString
            Requete &= " AND idRapport = " + oMemoireSeuil.nIdRapport.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oMemoireSeuil.sValeur, readerSQL.Item("valeur"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub supprimerValeurMemoireSeuil(ByRef oMemoireSeuil As MemoireSeuil)
        Dim transaction As SqlTransaction
        Dim requete As String = "DELETE FROM MemoireSeuil"
        requete &= " WHERE idClient =" + oMemoireSeuil.nIdClient.ToString
        requete &= " AND idUtilisateur = " + oMemoireSeuil.nIdUtilisateur.ToString
        requete &= " AND idUtilisateurSecondaire = " + oMemoireSeuil.nIdUtilisateurSecondaire.ToString
        requete &= " AND idModule = " + oMemoireSeuil.nIdModule.ToString
        requete &= " AND idRapport = " + oMemoireSeuil.nIdRapport.ToString

        Dim sql As New cSQL("Administration"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub ecrireValeurMemoireSeuil(ByRef oMemoireSeuil As MemoireSeuil)
        Dim transaction As SqlTransaction
        Dim requete As String = "INSERT INTO MemoireSeuil (idClient, idUtilisateur, idUtilisateurSecondaire, idModule, idRapport, valeur) VALUES        (@idClient, @idUtilisateur, @idUtilisateurSecondaire, @idModule, @idRapport, @valeur)"

        Dim sql As New cSQL("Administration"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@idClient", SqlDbType.Int).Value = oMemoireSeuil.nIdClient
            cmd.Parameters.Add("@idUtilisateur", SqlDbType.Int).Value = oMemoireSeuil.nIdUtilisateur
            cmd.Parameters.Add("@idUtilisateurSecondaire", SqlDbType.Int).Value = oMemoireSeuil.nIdUtilisateurSecondaire
            cmd.Parameters.Add("@idModule", SqlDbType.Int).Value = oMemoireSeuil.nIdModule
            cmd.Parameters.Add("@idRapport", SqlDbType.Int).Value = oMemoireSeuil.nIdRapport
            cmd.Parameters.Add("@Valeur", SqlDbType.VarChar, 50).Value = oMemoireSeuil.sValeur

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub



    Public Shared Sub Req_Infos_Utilisateur(ByRef oUtilisateur As Utilisateur, ByVal idUtilisateur As Integer, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""

        Requete &= "SELECT dbo.Client.idClient, dbo.Client.nomClient, dbo.Client.enteteExport, dbo.Client.logo, dbo.Client.prefixeClient, dbo.Utilisateur.idUtilisateur, dbo.Utilisateur.login, " _
            & "dbo.Utilisateur.nom, dbo.Utilisateur.prenom, dbo.Utilisateur.civilite, dbo.Utilisateur.mail, dbo.Utilisateur.dateCreation " _
            & "FROM dbo.Client INNER JOIN dbo.Utilisateur ON dbo.Client.idClient = dbo.Utilisateur.idClient " _
            & "GROUP BY dbo.Client.idClient, dbo.Client.nomClient, dbo.Client.enteteExport, dbo.Client.logo, dbo.Client.prefixeClient, dbo.Utilisateur.idUtilisateur, dbo.Utilisateur.login, " _
            & "dbo.Utilisateur.nom, dbo.Utilisateur.prenom, dbo.Utilisateur.civilite, dbo.Utilisateur.mail, dbo.Utilisateur.dateCreation " _
            & "HAVING(dbo.Utilisateur.idUtilisateur = " & idUtilisateur & " and dbo.Client.idClient=" & nIdClient.ToString & ")"

        Dim readerSQL As SqlDataReader = Nothing
        Try
            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oUtilisateur.nIdUtilisateur = CInt(readerSQL.Item("idUtilisateur"))

                Dim oClient As New Client
                oClient.chargerClientParId(CInt(readerSQL.Item("idClient")))
                ChargerPropriete(oUtilisateur.oClient, oClient)
                'oUtilisateur.oClient = New Client(CInt(readerSQL.Item("idClient")), CStr(readerSQL.Item("nomClient")), CStr(readerSQL.Item("enteteExport")), CStr(readerSQL.Item("logo")), CStr(readerSQL.Item("prefixeClient")))
                oUtilisateur.sLogin = CStr(readerSQL.Item("login"))
                oUtilisateur.sNom = CStr(readerSQL.Item("nom"))
                oUtilisateur.sPrenom = CStr(readerSQL.Item("prenom"))
                oUtilisateur.sCivilite = CStr(readerSQL.Item("civilite"))
                oUtilisateur.sMail = CStr(readerSQL.Item("mail"))
                oUtilisateur.dDateCreation = CDate(readerSQL.Item("dateCreation"))

                Exit Do
            Loop
            'Dim exceptionTest As New Exception("Erreur en test")
            'Throw exceptionTest
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub Req_Infos_Utilisateur_Secondaire(ByRef oUtilisateurSecondaire As UtilisateurSecondaire, ByVal idUtilisateurSecondaire As Integer, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""

        Requete &= "SELECT dbo.Client.idClient, dbo.Client.nomClient, dbo.Client.enteteExport, dbo.Client.logo, dbo.Client.prefixeClient, dbo.UtilisateurSecondaire.login,"
        Requete &= " dbo.UtilisateurSecondaire.nom, dbo.UtilisateurSecondaire.prenom, dbo.UtilisateurSecondaire.civilite, dbo.UtilisateurSecondaire.mail,"
        Requete &= " dbo.UtilisateurSecondaire.dateCreation, dbo.UtilisateurSecondaire.idUtilisateur, dbo.UtilisateurSecondaire.idUtilisateurPrimaire"
        Requete &= " FROM dbo.Client INNER JOIN"
        Requete &= " dbo.Utilisateur ON dbo.Client.idClient = dbo.Utilisateur.idClient INNER JOIN"
        Requete &= " dbo.UtilisateurSecondaire ON dbo.Utilisateur.idUtilisateur = dbo.UtilisateurSecondaire.idUtilisateurPrimaire and dbo.Utilisateur.idClient = dbo.UtilisateurSecondaire.idClient INNER JOIN"
        Requete &= " dbo.Logs ON dbo.Utilisateur.idUtilisateur = dbo.Logs.idUtilisateur"
        Requete &= " GROUP BY dbo.Client.idClient, dbo.Client.nomClient, dbo.Client.enteteExport, dbo.Client.logo, dbo.Client.prefixeClient, dbo.UtilisateurSecondaire.login,"
        Requete &= " dbo.UtilisateurSecondaire.nom, dbo.UtilisateurSecondaire.prenom, dbo.UtilisateurSecondaire.civilite, dbo.UtilisateurSecondaire.mail,"
        Requete &= " dbo.UtilisateurSecondaire.dateCreation, dbo.UtilisateurSecondaire.idUtilisateur, dbo.UtilisateurSecondaire.idUtilisateurPrimaire"
        Requete &= " HAVING (dbo.UtilisateurSecondaire.idUtilisateur = " & idUtilisateurSecondaire & " and dbo.Client.idClient= " & nIdClient.ToString & ")"

        Dim readerSQL As SqlDataReader = Nothing
        Try
            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oUtilisateurSecondaire.nIdUtilisateur = CInt(readerSQL.Item("idUtilisateur"))
                'oUtilisateur.oClient = New Client(CInt(readerSQL.Item("idClient")), CStr(readerSQL.Item("nomClient")), CStr(readerSQL.Item("enteteExport")), CStr(readerSQL.Item("logo")), CStr(readerSQL.Item("prefixeClient")))
                oUtilisateurSecondaire.sLogin = CStr(readerSQL.Item("login"))
                oUtilisateurSecondaire.sNom = CStr(readerSQL.Item("nom"))
                oUtilisateurSecondaire.sPrenom = CStr(readerSQL.Item("prenom"))
                oUtilisateurSecondaire.sCivilite = CStr(readerSQL.Item("civilite"))
                oUtilisateurSecondaire.sMail = CStr(readerSQL.Item("mail"))
                oUtilisateurSecondaire.dDateCreation = CDate(readerSQL.Item("dateCreation"))

                Dim oClient As New Client()
                oUtilisateurSecondaire.oClient = oClient
                oUtilisateurSecondaire.oClient.chargerClientParId(CInt(readerSQL.Item("idClient")))

                Dim oUtilisateurPrimaire As New Utilisateur()
                oUtilisateurPrimaire.chargerUtilisateurParId(CInt(readerSQL.Item("idUtilisateurPrimaire")), nIdClient)
                oUtilisateurSecondaire.oUtilisateur = oUtilisateurPrimaire

                Exit Do
            Loop

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub Req_Logs_Utilisateur(ByRef oLogs As Logs)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "SELECT dbo.Logs.dateHeure FROM dbo.Logs WHERE idUtilisateur=" & oLogs.oUtilisateur.nIdUtilisateur & " AND idClient = " & oLogs.oUtilisateur.oClient.nIdClient.ToString & " AND idRapport is not null AND (idUtilisateurSecondaire is null OR idUtilisateurSecondaire=0) ORDER BY dbo.Logs.dateHeure DESC"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oLogs.dDateHeure = CDate(readerSQL.Item("dateHeure"))
                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub Req_Logs_Utilisateur_Secondaire(ByRef oLogs As Logs)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "SELECT dbo.Logs.dateHeure FROM dbo.Logs WHERE idUtilisateurSecondaire=" & oLogs.oUtilisateurSecondaire.nIdUtilisateur & " AND idClient = " & oLogs.oUtilisateur.oClient.nIdClient.ToString & " AND idRapport is not null ORDER BY dbo.Logs.dateHeure DESC"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oLogs.dDateHeure = CDate(readerSQL.Item("dateHeure"))
                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub Req_Documents_Utilisateur(ByRef oDocuments As Documents, ByRef oUtilisateur As Utilisateur)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "  SELECT dbo.[document].idDocument, dbo.[Document].libelle, dbo.[Document].lien, dbo.[Document].ordre, dbo.[Document].libelleClasse"
            Requete &= " FROM dbo.Utilisateur INNER JOIN"
            Requete &= " dbo.[Document] ON dbo.Utilisateur.idClient = dbo.[Document].idClient"
            Requete &= " WHERE(dbo.Utilisateur.idUtilisateur = " & oUtilisateur.nIdUtilisateur.ToString & " AND dbo.Document.idClient=" & oUtilisateur.oClient.nIdClient.ToString & ")"
            Requete &= " ORDER BY dbo.[Document].ordre"
            readerSQL = sql.chargerDataReader(Requete)

            Do Until Not readerSQL.Read()
                oDocuments.Add(New Document(CInt(readerSQL.Item("idDocument")), oUtilisateur.oClient, CStr(readerSQL.Item("libelle")), CStr(readerSQL.Item("lien")), CUShort(readerSQL.Item("ordre")), CStr(readerSQL.Item("libelleClasse"))))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerListeModulesUnUtilisateur(ByRef oModules As Moduls, ByVal idUtilisateur As Integer, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "SELECT dbo.Module.idModule, dbo.Module.titre, dbo.Module.detail, dbo.Module.lien, dbo.Module.base, dbo.Module.libelleClasse, dbo.Module.libelleClasseFooter, dbo.Module.libelleClasseSommaire"
            Requete &= " FROM dbo.Utilisateur INNER JOIN"
            Requete &= " dbo.Acces ON dbo.Utilisateur.idUtilisateur = dbo.Acces.idUtilisateur"
            Requete &= " AND dbo.Utilisateur.idClient = dbo.Acces.idClient INNER JOIN"
            Requete &= " dbo.Module ON dbo.Acces.idModule = dbo.Module.idModule"
            Requete &= " WHERE dbo.Utilisateur.idUtilisateur = " & idUtilisateur & " and dbo.Module.titre <>'Connexion' and dbo.Acces.idClient = " & nIdClient.ToString
            Requete &= " ORDER BY dbo.Acces.ordre"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                'oModules.Add(New Modul(readerSQL.Item("idModule"), readerSQL.Item("titre"), readerSQL.Item("detail"), readerSQL.Item("lien"), readerSQL.Item("base"), readerSQL.Item("libelleClasse")))
                Dim oModule As New Modul()
                If Not IsDBNull(readerSQL.Item("idModule")) Then
                    oModule.nIdModule = CShort(readerSQL.Item("idModule"))
                End If
                If Not IsDBNull(readerSQL.Item("titre")) Then
                    oModule.sTitre = CStr(readerSQL.Item("titre"))
                End If
                If Not IsDBNull(readerSQL.Item("detail")) Then
                    oModule.sDetail = CStr(readerSQL.Item("detail"))
                End If
                If Not IsDBNull(readerSQL.Item("lien")) Then
                    oModule.sLien = CStr(readerSQL.Item("lien"))
                End If
                If Not IsDBNull(readerSQL.Item("base")) Then
                    oModule.sBase = CStr(readerSQL.Item("base"))
                End If
                If Not IsDBNull(readerSQL.Item("libelleClasse")) Then
                    oModule.sLibelleClasse = CStr(readerSQL.Item("libelleClasse"))
                End If
                If Not IsDBNull(readerSQL.Item("libelleClasseFooter")) Then
                    oModule.sLibelleClasseFooter = CStr(readerSQL.Item("libelleClasseFooter"))
                End If
                If Not IsDBNull(readerSQL.Item("libelleClasseSommaire")) Then
                    oModule.sLibelleClasseSommaire = CStr(readerSQL.Item("libelleClasseSommaire"))
                End If
                oModules.Add(oModule)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Sub

    Public Shared Sub chargerModulesParId(ByRef oModule As Modul, ByVal idModule As Integer, ByRef oClient As Client)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "SELECT dbo.Module.idModule, dbo.Module.titre, dbo.Module.detail, dbo.Module.lien, dbo.Module.base, dbo.Module.libelleClasse, dbo.Module.libelleClasseFooter, dbo.Module.libelleClasseSommaire"
            Requete &= " FROM dbo.Module "
            Requete &= " WHERE dbo.Module.idModule = " & idModule & " "
            readerSQL = sql.chargerDataReader(Requete)

            Do Until Not readerSQL.Read()
                If Not IsDBNull(readerSQL.Item("idModule")) Then
                    oModule.nIdModule = CShort(readerSQL.Item("idModule"))
                End If
                If Not IsDBNull(readerSQL.Item("titre")) Then
                    oModule.sTitre = CStr(readerSQL.Item("titre"))
                End If
                If Not IsDBNull(readerSQL.Item("detail")) Then
                    oModule.sDetail = CStr(readerSQL.Item("detail"))
                End If
                If Not IsDBNull(readerSQL.Item("lien")) Then
                    oModule.sLien = CStr(readerSQL.Item("lien"))
                End If
                If Not IsDBNull(readerSQL.Item("base")) Then
                    oModule.sBase = CStr(readerSQL.Item("base"))
                End If
                If Not IsDBNull(readerSQL.Item("libelleClasse")) Then
                    oModule.sLibelleClasse = CStr(readerSQL.Item("libelleClasse"))
                End If
                If Not IsDBNull(readerSQL.Item("libelleClasseFooter")) Then
                    oModule.sLibelleClasseFooter = CStr(readerSQL.Item("libelleClasseFooter"))
                End If
                If Not IsDBNull(readerSQL.Item("libelleClasseSommaire")) Then
                    oModule.sLibelleClasseSommaire = CStr(readerSQL.Item("libelleClasseSommaire"))
                End If
                If oModule.nIdModule = 9 Then oClient.sBaseClient = "GestionParc"
                If oModule.nIdModule = 13 Then oClient.sBaseClient = "GestionParcFixe"
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerRappportsParModuleUtilisateur(ByRef oRapports As Rapports, ByVal idModule As Integer, ByVal idUtilisateur As Integer, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "SELECT * "
            Requete &= " FROM dbo.Rapport"
            Requete &= " WHERE dbo.Rapport.idUtilisateur = " & idUtilisateur & " and dbo.Rapport.idModule =" & idModule & " AND dbo.Rapport.estActif = 1 AND dbo.Rapport.idClient = " & nIdClient.ToString
            Requete &= " Order by dbo.Rapport.sommaireOrdreNiv1, dbo.Rapport.sommaireOrdreNiv2, dbo.Rapport.ongletOrdreNiv1, dbo.Rapport.ongletOrdreNiv2, dbo.Rapport.ongletOrdreRapport "

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim oRapport As New Rapport()
                Dim oClient As New Client()
                Dim oModule As New Modul()
                Dim oUtilisateur As New Utilisateur()

                oClient.chargerClientParId(CInt(readerSQL.Item("idClient")))
                oModule.chargerModulesParId(CInt(readerSQL.Item("idModule")), oClient)
                oUtilisateur.chargerUtilisateurParId(CInt(readerSQL.Item("idUtilisateur")), oClient.nIdClient)

                ChargerPropriete(oRapport.oClient, oClient)
                ChargerPropriete(oRapport.oModule, oModule)
                ChargerPropriete(oRapport.oUtilisateur, oUtilisateur)
                ChargerPropriete(oRapport.nIdRapport, readerSQL.Item("idRapport"))
                ChargerPropriete(oRapport.sPageRapport, readerSQL.Item("pageRapport"))
                ChargerPropriete(oRapport.sTableRapport, readerSQL.Item("tableRapport"))
                ChargerPropriete(oRapport.sSommaireNiv1, readerSQL.Item("sommaireNiv1"))
                ChargerPropriete(oRapport.sSommaireNiv2, readerSQL.Item("sommaireNiv2"))
                ChargerPropriete(oRapport.nSommaireOrdreNiv1, readerSQL.Item("sommaireOrdreNiv1"))
                ChargerPropriete(oRapport.nSommaireOrdreNiv2, readerSQL.Item("sommaireOrdreNiv2"))
                ChargerPropriete(oRapport.sOngletNiv1, readerSQL.Item("ongletNiv1"))
                ChargerPropriete(oRapport.sOngletNiv2, readerSQL.Item("ongletNiv2"))
                ChargerPropriete(oRapport.nOngletOrdreNiv1, readerSQL.Item("ongletOrdreNiv1"))
                ChargerPropriete(oRapport.nOngletOrdreNiv2, readerSQL.Item("ongletOrdreNiv2"))
                ChargerPropriete(oRapport.nOngletOrdreRapport, readerSQL.Item("ongletOrdreRapport"))
                ChargerPropriete(oRapport.ntailleBoite, readerSQL.Item("tailleBoite"))
                ChargerPropriete(oRapport.sTriDonnees, readerSQL.Item("triDonnees"))
                ChargerPropriete(oRapport.bEstActif, readerSQL.Item("estActif"))
                ChargerPropriete(oRapport.bEstVisible, readerSQL.Item("estVisible"))
                ChargerPropriete(oRapport.sTitreTableau, readerSQL.Item("titreTableau"))
                ChargerPropriete(oRapport.bAfficherTotal, readerSQL.Item("afficherTotal"))
                ChargerPropriete(oRapport.snotificationRequete, readerSQL.Item("notificationRequete"))
                ChargerPropriete(oRapport.bNotificationNiveau1, readerSQL.Item("notificationNiveau1"))
                ChargerPropriete(oRapport.bNotificationNiveau2, readerSQL.Item("notificationNiveau2"))
                ChargerPropriete(oRapport.sTypeTotal, readerSQL.Item("typeTotal"))
                ChargerPropriete(oRapport.sTitreTotal, readerSQL.Item("titreTotal"))
                ChargerPropriete(oRapport.bEstHover, readerSQL.Item("estHover"))
                ChargerPropriete(oRapport.bEstFiltreBoite, readerSQL.Item("estFiltreBoite"))
                ChargerPropriete(oRapport.bEstBoiteOutilOuverte, readerSQL.Item("estBoiteOutilOuverte"))
                ChargerPropriete(oRapport.bAfficherPagination, readerSQL.Item("afficherPagination"))
                ChargerPropriete(oRapport.bAfficherNombreEnregistrements, readerSQL.Item("afficherNombreEnregistrements"))
                ChargerPropriete(oRapport.bAfficherRecherche, readerSQL.Item("afficherRecherche"))
                ChargerPropriete(oRapport.bAfficherSeuil, readerSQL.Item("afficherSeuil"))
                ChargerPropriete(oRapport.sOutilTop, readerSQL.Item("outilTop"))
                ChargerPropriete(oRapport.bAutorisationAjout, readerSQL.Item("autorisationAjout"))
                ChargerPropriete(oRapport.bAutorisationModification, readerSQL.Item("autorisationModification"))
                ChargerPropriete(oRapport.bAutorisationSuppression, readerSQL.Item("autorisationSuppression"))
                ChargerPropriete(oRapport.sTestInsertion, readerSQL.Item("testInsertion"))
                ChargerPropriete(oRapport.sTestModification, readerSQL.Item("testModification"))
                ChargerPropriete(oRapport.sTableAction, readerSQL.Item("tableAction"))
                ChargerPropriete(oRapport.bAutoriserFusionColonnes, readerSQL.Item("autoriserFusionColonnes"))
                ChargerPropriete(oRapport.bAfficheDonneesApresRecherche, readerSQL.Item("afficheDonneesApresRecherche"))
                ChargerPropriete(oRapport.bAfficherCaseMoisGlissant, readerSQL.Item("afficherCaseMoisGlissant"))
                ChargerPropriete(oRapport.bAfficherEnMoisGlissant, readerSQL.Item("afficherEnMoisGlissant"))
                ChargerPropriete(oRapport.bAfficherCaseTTC, readerSQL.Item("afficherCaseTTC"))
                ChargerPropriete(oRapport.bAfficherEnTTC, readerSQL.Item("afficherEnTTC"))
                ChargerPropriete(oRapport.bAfficherHtTtcDansLibelleTotal, readerSQL.Item("afficherHtTtcDansLibelleTotal"))
                ChargerPropriete(oRapport.AfficherCaseDecimale, readerSQL.Item("afficherCaseDecimale"))
                ChargerPropriete(oRapport.bAfficherAvecDecimales, readerSQL.Item("afficherAvecDecimales"))
                ChargerPropriete(oRapport.bAfficherCaseNumComplet, readerSQL.Item("afficherCaseNumComplet"))
                ChargerPropriete(oRapport.bAfficherNumeroEntier, readerSQL.Item("afficherNumeroEntier"))
                ChargerPropriete(oRapport.bAfficherExport, readerSQL.Item("afficherExport"))
                ChargerPropriete(oRapport.sNomFichierExport, readerSQL.Item("nomFichierExport"))
                ChargerPropriete(oRapport.sColonneLiaisonMoisTva, readerSQL.Item("colonneLiaisonMoisTva"))
                ChargerPropriete(oRapport.sObjetMailing, readerSQL.Item("objetMailing"))
                ChargerPropriete(oRapport.sSpecReq, readerSQL.Item("specReq"))

                oRapports.Add(oRapport)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerRappportParModuleUtilisateurIdRapport(ByRef oRapport As Rapport, ByVal idModule As Integer, ByVal idUtilisateur As Integer, ByVal idRapport As Integer, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "SELECT * "
            Requete &= " FROM dbo.Rapport"
            Requete &= " WHERE dbo.Rapport.idUtilisateur = " & idUtilisateur & " and dbo.Rapport.idModule =" & idModule & " and dbo.Rapport.idRapport=" & idRapport & " AND dbo.Rapport.estActif = 1 AND dbo.Rapport.idClient = " & nIdClient.ToString
            Requete &= " Order by dbo.Rapport.sommaireOrdreNiv1, dbo.Rapport.sommaireOrdreNiv2, dbo.Rapport.ongletOrdreNiv1, dbo.Rapport.ongletOrdreNiv2, dbo.Rapport.ongletOrdreRapport"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim oClient As New Client()
                Dim oModule As New Modul()
                Dim oUtilisateur As New Utilisateur()

                oClient.chargerClientParId(CInt(readerSQL.Item("idClient")))
                oModule.chargerModulesParId(CInt(readerSQL.Item("idModule")), oClient)
                oUtilisateur.chargerUtilisateurParId(CInt(readerSQL.Item("idUtilisateur")), oClient.nIdClient)

                ChargerPropriete(oRapport.oClient, oClient)
                ChargerPropriete(oRapport.oModule, oModule)
                ChargerPropriete(oRapport.oUtilisateur, oUtilisateur)
                ChargerPropriete(oRapport.nIdRapport, readerSQL.Item("idRapport"))
                ChargerPropriete(oRapport.sPageRapport, readerSQL.Item("pageRapport"))
                ChargerPropriete(oRapport.sTableRapport, readerSQL.Item("tableRapport"))
                ChargerPropriete(oRapport.sSommaireNiv1, readerSQL.Item("sommaireNiv1"))
                ChargerPropriete(oRapport.sSommaireNiv2, readerSQL.Item("sommaireNiv2"))
                ChargerPropriete(oRapport.nSommaireOrdreNiv1, readerSQL.Item("sommaireOrdreNiv1"))
                ChargerPropriete(oRapport.nSommaireOrdreNiv2, readerSQL.Item("sommaireOrdreNiv2"))
                ChargerPropriete(oRapport.sOngletNiv1, readerSQL.Item("ongletNiv1"))
                ChargerPropriete(oRapport.sOngletNiv2, readerSQL.Item("ongletNiv2"))
                ChargerPropriete(oRapport.nOngletOrdreNiv1, readerSQL.Item("ongletOrdreNiv1"))
                ChargerPropriete(oRapport.nOngletOrdreNiv2, readerSQL.Item("ongletOrdreNiv2"))
                ChargerPropriete(oRapport.nOngletOrdreRapport, readerSQL.Item("ongletOrdreRapport"))
                ChargerPropriete(oRapport.ntailleBoite, readerSQL.Item("tailleBoite"))
                ChargerPropriete(oRapport.sTriDonnees, readerSQL.Item("triDonnees"))
                ChargerPropriete(oRapport.bEstActif, readerSQL.Item("estActif"))
                ChargerPropriete(oRapport.bEstVisible, readerSQL.Item("estVisible"))
                ChargerPropriete(oRapport.bEstFiltreBoite, readerSQL.Item("estFiltreBoite"))
                ChargerPropriete(oRapport.sTitreTableau, readerSQL.Item("titreTableau"))
                ChargerPropriete(oRapport.bAfficherTotal, readerSQL.Item("afficherTotal"))
                ChargerPropriete(oRapport.snotificationRequete, readerSQL.Item("notificationRequete"))
                ChargerPropriete(oRapport.bNotificationNiveau1, readerSQL.Item("notificationNiveau1"))
                ChargerPropriete(oRapport.bNotificationNiveau2, readerSQL.Item("notificationNiveau2"))
                ChargerPropriete(oRapport.sTypeTotal, readerSQL.Item("typeTotal"))
                ChargerPropriete(oRapport.sTitreTotal, readerSQL.Item("titreTotal"))
                ChargerPropriete(oRapport.bEstBoiteOutilOuverte, readerSQL.Item("estBoiteOutilOuverte"))
                ChargerPropriete(oRapport.bAfficherPagination, readerSQL.Item("afficherPagination"))
                ChargerPropriete(oRapport.bAfficherNombreEnregistrements, readerSQL.Item("afficherNombreEnregistrements"))
                ChargerPropriete(oRapport.bAfficherRecherche, readerSQL.Item("afficherRecherche"))
                ChargerPropriete(oRapport.bAfficherSeuil, readerSQL.Item("afficherSeuil"))
                ChargerPropriete(oRapport.sOutilTop, readerSQL.Item("outilTop"))
                ChargerPropriete(oRapport.bAutorisationAjout, readerSQL.Item("autorisationAjout"))
                ChargerPropriete(oRapport.bAutorisationModification, readerSQL.Item("autorisationModification"))
                ChargerPropriete(oRapport.bAutorisationSuppression, readerSQL.Item("autorisationSuppression"))
                ChargerPropriete(oRapport.sTestInsertion, readerSQL.Item("testInsertion"))
                ChargerPropriete(oRapport.sTestModification, readerSQL.Item("testModification"))
                ChargerPropriete(oRapport.sTableAction, readerSQL.Item("tableAction"))
                ChargerPropriete(oRapport.bAutoriserFusionColonnes, readerSQL.Item("autoriserFusionColonnes"))
                ChargerPropriete(oRapport.bAfficheDonneesApresRecherche, readerSQL.Item("afficheDonneesApresRecherche"))
                ChargerPropriete(oRapport.bAfficherCaseMoisGlissant, readerSQL.Item("afficherCaseMoisGlissant"))
                ChargerPropriete(oRapport.bAfficherEnMoisGlissant, readerSQL.Item("afficherEnMoisGlissant"))
                ChargerPropriete(oRapport.bAfficherCaseTTC, readerSQL.Item("afficherCaseTTC"))
                ChargerPropriete(oRapport.bAfficherEnTTC, readerSQL.Item("afficherEnTTC"))
                ChargerPropriete(oRapport.bAfficherHtTtcDansLibelleTotal, readerSQL.Item("afficherHtTtcDansLibelleTotal"))
                ChargerPropriete(oRapport.AfficherCaseDecimale, readerSQL.Item("afficherCaseDecimale"))
                ChargerPropriete(oRapport.bAfficherAvecDecimales, readerSQL.Item("afficherAvecDecimales"))
                ChargerPropriete(oRapport.bAfficherCaseNumComplet, readerSQL.Item("afficherCaseNumComplet"))
                ChargerPropriete(oRapport.bAfficherNumeroEntier, readerSQL.Item("afficherNumeroEntier"))
                ChargerPropriete(oRapport.bAfficherExport, readerSQL.Item("afficherExport"))
                ChargerPropriete(oRapport.sNomFichierExport, readerSQL.Item("nomFichierExport"))
                ChargerPropriete(oRapport.sColonneLiaisonMoisTva, readerSQL.Item("colonneLiaisonMoisTva"))
                ChargerPropriete(oRapport.sObjetMailing, readerSQL.Item("objetMailing"))
                ChargerPropriete(oRapport.sSpecReq, readerSQL.Item("specReq"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerRappportsParModuleUtilisateurIdRapport(ByRef oRapports As Rapports, ByVal idModule As Integer, ByVal idUtilisateur As Integer, ByVal idRapport As Integer, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oRapportActuel As New Rapport

        oRapportActuel.chargerRappportParModuleUtilisateurIdRapport(idModule, idUtilisateur, idRapport, nIdClient)

        Try
            Requete = "SELECT * "
            Requete &= " FROM dbo.Rapport"
            Requete &= " WHERE dbo.Rapport.idUtilisateur = " & idUtilisateur & " AND dbo.Rapport.idClient = " & nIdClient.ToString & " and dbo.Rapport.idModule =" & idModule & " and sommaireOrdreNiv1=" & oRapportActuel.nSommaireOrdreNiv1
            Requete &= " and  sommaireOrdreNiv2=" & oRapportActuel.nSommaireOrdreNiv2
            Requete &= " AND dbo.Rapport.estActif = 1"
            Requete &= " Order by dbo.Rapport.sommaireOrdreNiv1, dbo.Rapport.sommaireOrdreNiv2, dbo.Rapport.ongletOrdreNiv1, dbo.Rapport.ongletOrdreNiv2, dbo.Rapport.ongletOrdreRapport"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim oRapport As New Rapport()
                Dim oClient As New Client()
                Dim oModule As New Modul()
                Dim oUtilisateur As New Utilisateur()

                oClient.chargerClientParId(CInt(readerSQL.Item("idClient")))
                oModule.chargerModulesParId(CInt(readerSQL.Item("idModule")), oClient)
                oUtilisateur.chargerUtilisateurParId(CInt(readerSQL.Item("idUtilisateur")), oClient.nIdClient)

                ChargerPropriete(oRapport.oClient, oClient)
                ChargerPropriete(oRapport.oModule, oModule)
                ChargerPropriete(oRapport.oUtilisateur, oUtilisateur)

                ChargerPropriete(oRapport.nIdRapport, readerSQL.Item("idRapport"))
                ChargerPropriete(oRapport.sPageRapport, readerSQL.Item("pageRapport"))
                ChargerPropriete(oRapport.sTableRapport, readerSQL.Item("tableRapport"))
                ChargerPropriete(oRapport.sSommaireNiv1, readerSQL.Item("sommaireNiv1"))
                ChargerPropriete(oRapport.sSommaireNiv2, readerSQL.Item("sommaireNiv2"))
                ChargerPropriete(oRapport.nSommaireOrdreNiv1, readerSQL.Item("sommaireOrdreNiv1"))
                ChargerPropriete(oRapport.nSommaireOrdreNiv2, readerSQL.Item("sommaireOrdreNiv2"))
                ChargerPropriete(oRapport.sOngletNiv1, readerSQL.Item("ongletNiv1"))
                ChargerPropriete(oRapport.sOngletNiv2, readerSQL.Item("ongletNiv2"))
                ChargerPropriete(oRapport.nOngletOrdreNiv1, readerSQL.Item("ongletOrdreNiv1"))
                ChargerPropriete(oRapport.nOngletOrdreNiv2, readerSQL.Item("ongletOrdreNiv2"))
                ChargerPropriete(oRapport.nOngletOrdreRapport, readerSQL.Item("ongletOrdreRapport"))
                ChargerPropriete(oRapport.ntailleBoite, readerSQL.Item("tailleBoite"))
                ChargerPropriete(oRapport.sTriDonnees, readerSQL.Item("triDonnees"))
                ChargerPropriete(oRapport.bEstActif, readerSQL.Item("estActif"))
                ChargerPropriete(oRapport.bEstVisible, readerSQL.Item("estVisible"))
                ChargerPropriete(oRapport.sTitreTableau, readerSQL.Item("titretableau"))
                ChargerPropriete(oRapport.bAfficherTotal, readerSQL.Item("afficherTotal"))
                ChargerPropriete(oRapport.snotificationRequete, readerSQL.Item("notificationRequete"))
                ChargerPropriete(oRapport.bNotificationNiveau1, readerSQL.Item("notificationNiveau1"))
                ChargerPropriete(oRapport.bNotificationNiveau2, readerSQL.Item("notificationNiveau2"))
                ChargerPropriete(oRapport.sTypeTotal, readerSQL.Item("typeTotal"))
                ChargerPropriete(oRapport.sTitreTotal, readerSQL.Item("titreTotal"))
                ChargerPropriete(oRapport.bEstHover, readerSQL.Item("estHover"))
                ChargerPropriete(oRapport.bEstFiltreBoite, readerSQL.Item("estFiltreBoite"))
                ChargerPropriete(oRapport.bEstBoiteOutilOuverte, readerSQL.Item("estBoiteOutilOuverte"))
                ChargerPropriete(oRapport.bAfficherPagination, readerSQL.Item("afficherPagination"))
                ChargerPropriete(oRapport.bAfficherNombreEnregistrements, readerSQL.Item("afficherNombreEnregistrements"))
                ChargerPropriete(oRapport.bAfficherRecherche, readerSQL.Item("afficherRecherche"))
                ChargerPropriete(oRapport.bAfficherSeuil, readerSQL.Item("afficherSeuil"))
                ChargerPropriete(oRapport.sOutilTop, readerSQL.Item("outilTop"))
                ChargerPropriete(oRapport.bAutorisationAjout, readerSQL.Item("autorisationAjout"))
                ChargerPropriete(oRapport.bAutorisationModification, readerSQL.Item("autorisationModification"))
                ChargerPropriete(oRapport.bAutorisationSuppression, readerSQL.Item("autorisationSuppression"))
                ChargerPropriete(oRapport.sTestInsertion, readerSQL.Item("testInsertion"))
                ChargerPropriete(oRapport.sTestModification, readerSQL.Item("testModification"))
                ChargerPropriete(oRapport.sTableAction, readerSQL.Item("tableAction"))
                ChargerPropriete(oRapport.bAutoriserFusionColonnes, readerSQL.Item("autoriserFusionColonnes"))
                ChargerPropriete(oRapport.bAfficheDonneesApresRecherche, readerSQL.Item("afficheDonneesApresRecherche"))
                ChargerPropriete(oRapport.bAfficherCaseMoisGlissant, readerSQL.Item("afficherCaseMoisGlissant"))
                ChargerPropriete(oRapport.bAfficherEnMoisGlissant, readerSQL.Item("afficherEnMoisGlissant"))
                ChargerPropriete(oRapport.bAfficherCaseTTC, readerSQL.Item("afficherCaseTTC"))
                ChargerPropriete(oRapport.bAfficherEnTTC, readerSQL.Item("afficherEnTTC"))
                ChargerPropriete(oRapport.bAfficherHtTtcDansLibelleTotal, readerSQL.Item("afficherHtTtcDansLibelleTotal"))
                ChargerPropriete(oRapport.AfficherCaseDecimale, readerSQL.Item("afficherCaseDecimale"))
                ChargerPropriete(oRapport.bAfficherAvecDecimales, readerSQL.Item("afficherAvecDecimales"))
                ChargerPropriete(oRapport.bAfficherCaseNumComplet, readerSQL.Item("afficherCaseNumComplet"))
                ChargerPropriete(oRapport.bAfficherNumeroEntier, readerSQL.Item("afficherNumeroEntier"))
                ChargerPropriete(oRapport.bAfficherExport, readerSQL.Item("afficherExport"))
                ChargerPropriete(oRapport.sNomFichierExport, readerSQL.Item("nomFichierExport"))
                ChargerPropriete(oRapport.sColonneLiaisonMoisTva, readerSQL.Item("colonneLiaisonMoisTva"))
                ChargerPropriete(oRapport.sObjetMailing, readerSQL.Item("objetMailing"))
                ChargerPropriete(oRapport.sSpecReq, readerSQL.Item("specReq"))
                oRapports.Add(oRapport)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerRappportsParModuleUtilisateurIdRapport_ListeIdRapports(ByRef oRapports As Rapports, ByVal idModule As Integer, ByVal idUtilisateur As Integer, ByVal oRapportActuel As Rapport)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "SELECT dbo.Rapport.idRapport ,dbo.Rapport.ongletOrdreNiv1, dbo.Rapport.ongletOrdreNiv2 "
            Requete &= " FROM dbo.Rapport"
            Requete &= " WHERE dbo.Rapport.idUtilisateur = " & idUtilisateur & " AND dbo.Rapport.idClient = " & oRapportActuel.oClient.nIdClient.ToString & " and dbo.Rapport.idModule =" & idModule & " and sommaireOrdreNiv1=" & oRapportActuel.nSommaireOrdreNiv1
            Requete &= " and  sommaireOrdreNiv2=" & oRapportActuel.nSommaireOrdreNiv2
            Requete &= " AND dbo.Rapport.estActif = 1"
            Requete &= " Order by dbo.Rapport.sommaireOrdreNiv1, dbo.Rapport.sommaireOrdreNiv2, dbo.Rapport.ongletOrdreNiv1, dbo.Rapport.ongletOrdreNiv2, dbo.Rapport.ongletOrdreRapport"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim oRapport As New Rapport()
                Dim oClient As New Client()
                Dim oModule As New Modul()
                Dim oUtilisateur As New Utilisateur()

                ChargerPropriete(oRapport.nIdRapport, readerSQL.Item("idRapport"))
                ChargerPropriete(oRapport.nOngletOrdreNiv1, readerSQL.Item("ongletOrdreNiv1"))
                ChargerPropriete(oRapport.nOngletOrdreNiv2, readerSQL.Item("ongletOrdreNiv2"))

                oRapports.Add(oRapport)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub


    Public Shared Sub chargerClientParId(ByRef oClient As Client, ByVal idClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oMailings As New Mailings
        Try
            Requete = "SELECT *"
            Requete &= " FROM dbo.Client "
            Requete &= " WHERE dbo.Client.idClient = " & idClient & " "

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oClient.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oClient.sNomClient, readerSQL.Item("nomClient"))
                ChargerPropriete(oClient.sEnteteExport, readerSQL.Item("enteteExport"))
                ChargerPropriete(oClient.sLogo, readerSQL.Item("logo"))
                ChargerPropriete(oClient.sBaseClient, readerSQL.Item("baseClient"))
                ChargerPropriete(oClient.sPrefixeClient, readerSQL.Item("prefixeClient"))
                ChargerPropriete(oClient.sMonnaieClient, readerSQL.Item("monnaieClient"))
                ChargerPropriete(oClient.sLangueClient, readerSQL.Item("langueClient"))
                oMailings = New Mailings
                oMailings.chargerListeMailingParClient(oClient.nIdClient)
                ChargerPropriete(oClient.oMailings, oMailings)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Function RecupererNombreNotification(ByVal sBase As String, ByVal Requete As String) As Integer
        Dim sql As New cSQL(sBase)
        Dim readerSQL As SqlDataReader = Nothing
        Dim nResultat As Integer = 0
        Try

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                nResultat = CInt(readerSQL.Item(0))
            Loop
            Return nResultat
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Function

    Public Shared Sub chargerListeMailingParClient(ByRef oMailings As Mailings, ByVal idClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oMailing As Mailing
        Try
            Requete = "SELECT idMailing, Mail"
            Requete &= " FROM dbo.Mailing "
            Requete &= " WHERE dbo.Mailing.idClient = " & idClient & " "

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oMailing = New Mailing
                ChargerPropriete(oMailing.nIdMailing, readerSQL.Item("idMailing"))
                ChargerPropriete(oMailing.mail, readerSQL.Item("Mail"))
                oMailings.Add(oMailing)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerPostItLigne(ByRef oPostItLigne As PostItLigne, ByRef oClient As Client)
        Dim sql As New cSQL(oClient.sBaseClient)
        Dim Requete As String = "SELECT idPostIt, infoPostIt FROM PostIt WHERE numLigne=@numLigne"
        Dim oCommandeSQL As New SqlCommand(Requete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing

        Try
            oCommandeSQL.Parameters.Add("@numLigne", SqlDbType.VarChar, 30).Value = oPostItLigne.sNumLigne
            readerSQL = oCommandeSQL.ExecuteReader()
            Do Until Not readerSQL.Read()
                ChargerPropriete(oPostItLigne.nIdPostIt, readerSQL.Item("idPostIt"))
                ChargerPropriete(oPostItLigne.sInfoPostIt, readerSQL.Item("infoPostIt"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            oCommandeSQL = Nothing
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub InsertionPostItLigne(ByRef oPostItLigne As PostItLigne, ByRef oClient As Client)
        Dim transaction As SqlTransaction
        Dim requete As String = "INSERT INTO PostIt (numLigne, infoPostIt) VALUES (@numLigne, @infoPostIt)"

        Dim sql As New cSQL(oClient.sBaseClient)
        Dim cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@numLigne", SqlDbType.VarChar, 30).Value = oPostItLigne.sNumLigne
            cmd.Parameters.Add("@infoPostIt", SqlDbType.VarChar, 1000).Value = Trim(oPostItLigne.sInfoPostIt)

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub updatePostItLigne(ByRef oPostItLigne As PostItLigne, ByRef oClient As Client)
        Dim transaction As SqlTransaction
        Dim requete As String = "UPDATE PostIt Set infoPostIt=@infoPostIt WHERE idPostIt=@idPostIt AND numLigne=@numLigne"

        Dim sql As New cSQL(oClient.sBaseClient)
        Dim cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@idPostIt", SqlDbType.Int).Value = oPostItLigne.nIdPostIt
            cmd.Parameters.Add("@numLigne", SqlDbType.VarChar, 30).Value = oPostItLigne.sNumLigne
            cmd.Parameters.Add("@infoPostIt", SqlDbType.VarChar, 1000).Value = Trim(oPostItLigne.sInfoPostIt)

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub ChargerPostItLiaisonSelonNomTableAvecPostIt(ByRef oPostItLiaison As PostItLiaison)
        Dim Requete = "SELECT idPostItLiaison, nomColonneNumTel "
        Requete += "From dbo.PostItLiaison "
        Requete += "WHERE idRapport=@nIdRapport AND idClient=@nIdClient AND idModule=@nIdModule"

        Dim sql As New cSQL("Administration")
        Dim cmd As New SqlCommand(Requete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing

        Try
            cmd.Parameters.Add("@nIdRapport", SqlDbType.Int).Value = oPostItLiaison.oRapport.nIdRapport
            cmd.Parameters.Add("@nIdClient", SqlDbType.Int).Value = oPostItLiaison.oRapport.oClient.nIdClient
            cmd.Parameters.Add("@nIdModule", SqlDbType.Int).Value = oPostItLiaison.oRapport.oModule.nIdModule
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                ChargerPropriete(oPostItLiaison.nIdPostItLiaison, readerSQL.Item("idPostItLiaison"))
                ChargerPropriete(oPostItLiaison.sNomColonneNumTel, readerSQL.Item("nomColonneNumTel"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub ChargerPostItLiaisonsSelonSelonClient(ByRef oPostItLiaisons As PostItLiaisons, ByRef oClient As Client)
        oPostItLiaisons.Clear()
        Dim Requete = "SELECT idPostItLiaison, nomColonneNumTel "
        Requete += "From dbo.PostItLiaison WHERE idClient=@nIdClient"

        Dim sql As New cSQL("Administration")
        Dim cmd As New SqlCommand(Requete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing

        Try
            cmd.Parameters.Add("@nIdClient", SqlDbType.Int).Value = oClient.nIdClient
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                Dim oPostItLiaison As New PostItLiaison()
                ChargerPropriete(oPostItLiaison.nIdPostItLiaison, readerSQL.Item("idPostItLiaison"))
                ChargerPropriete(oPostItLiaison.sNomTableAvecPostIt, readerSQL.Item("nomTableAvecPostIt"))
                ChargerPropriete(oPostItLiaison.sNomColonneNumTel, readerSQL.Item("nomColonneNumTel"))

                oPostItLiaisons.Add(oPostItLiaison)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub recupererListeTablePostItSelonClient(ByRef oListeTableRapport As List(Of String), ByRef oClient As Client)
        Dim Requete As String = "Select dbo.Rapport.tableRapport From dbo.Colonne INNER Join "
        Requete += "dbo.ParamRapportColonne On dbo.Colonne.idParamColonnes = dbo.ParamRapportColonne.idParamColonnes And "
        Requete += "dbo.Colonne.idClient = dbo.ParamRapportColonne.idClient INNER JOIN "
        Requete += "dbo.Rapport On dbo.ParamRapportColonne.idRapport = dbo.Rapport.idRapport And dbo.ParamRapportColonne.idClient = dbo.Rapport.idClient And "
        Requete += "dbo.ParamRapportColonne.idUtilisateur = dbo.Rapport.idUtilisateur And dbo.ParamRapportColonne.idModule = dbo.Rapport.idModule "
        Requete += "WHERE(dbo.Colonne.nomColonne = 'postit') AND (dbo.Colonne.idClient = @nIdClient)"

        Dim sql As New cSQL("Administration")
        Dim cmd As New SqlCommand(Requete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            cmd.Parameters.Add("@nIdClient", SqlDbType.Int).Value = oClient.nIdClient
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                Dim sTableRapport As String = ""
                ChargerPropriete(sTableRapport, readerSQL.Item("tableRapport"))
                If sTableRapport <> "" Then
                    oListeTableRapport.Add(sTableRapport)
                End If
            Loop

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargeUtilisateurParId(ByRef oUtilisateur As Utilisateur, ByVal idUtilisateur As Integer, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oClient As New Client

        Try
            Requete = "Select *"
            Requete &= " FROM dbo.Utilisateur "
            Requete &= " WHERE dbo.Utilisateur.idUtilisateur = " & idUtilisateur & " And dbo.Utilisateur.idClient = " & nIdClient

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oUtilisateur.nIdUtilisateur, readerSQL.Item("idUtilisateur"))

                oClient.chargerClientParId(CInt(readerSQL.Item("idClient")))
                ChargerPropriete(oUtilisateur.oClient, oClient)
                ChargerPropriete(oUtilisateur.sLogin, readerSQL.Item("login"))
                ChargerPropriete(oUtilisateur.sNom, readerSQL.Item("nom"))
                ChargerPropriete(oUtilisateur.sPrenom, readerSQL.Item("prenom"))
                ChargerPropriete(oUtilisateur.sCivilite, readerSQL.Item("civilite"))
                ChargerPropriete(oUtilisateur.sMail, readerSQL.Item("mail"))
                ChargerPropriete(oUtilisateur.dDateCreation, readerSQL.Item("dateCreation"))
                ChargerPropriete(oUtilisateur.dDateSuppression, readerSQL.Item("dateSuppression"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Sub

    Public Shared Sub chargerUtilisateurSecondaireFiltres(ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres, ByRef oUtilisateurSecondaire As UtilisateurSecondaire)
        oUtilisateurSecondaireFiltres.Clear()

        Dim sql As New cSQL("Administration")
        Dim Requete = "SELECT idFiltre, valeurFiltre, estRestriction, estTous FROM dbo.UtilisateurSecondaireFiltre WHERE (idUtilisateurPrimaire = @idUtilisateurPrimaire) "
        Requete += "And (idUtilisateurSecondaire = @idUtilisateurSecondaire) AND (idClient = @idClient)"
        Dim cmd As New SqlCommand(Requete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing

        Try
            cmd.Parameters.Add("@idUtilisateurPrimaire", SqlDbType.Int).Value = oUtilisateurSecondaire.oUtilisateur.nIdUtilisateur
            cmd.Parameters.Add("@idUtilisateurSecondaire", SqlDbType.Int).Value = oUtilisateurSecondaire.nIdUtilisateur
            cmd.Parameters.Add("@idClient", SqlDbType.Int).Value = oUtilisateurSecondaire.oClient.nIdClient

            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                Dim oUtilisateurSecondaireFiltre As New UtilisateurSecondaireFiltre()
                Dim oFiltre As New Filtre()

                oUtilisateurSecondaireFiltre.oUtilisateurPrimaire = oUtilisateurSecondaire.oUtilisateur
                oUtilisateurSecondaireFiltre.oUtilisateurSecondaire = oUtilisateurSecondaire

                ChargerPropriete(oFiltre.nIdFiltre, readerSQL.Item("idFiltre"))
                oUtilisateurSecondaireFiltre.oFiltre = oFiltre

                ChargerPropriete(oUtilisateurSecondaireFiltre.sValeurFiltre, readerSQL.Item("valeurFiltre"))
                ChargerPropriete(oUtilisateurSecondaireFiltre.bEstRestriction, readerSQL.Item("estRestriction"))
                ChargerPropriete(oUtilisateurSecondaireFiltre.bEstTous, readerSQL.Item("estTous"))

                oUtilisateurSecondaireFiltres.Add(oUtilisateurSecondaireFiltre)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Sub

    Public Shared Sub chargerFiltresParRapports(ByRef oFiltres As Filtres, ByVal orapports As Rapports, ByVal oRapportReference As Rapport, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim Requete_Where As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim orapport As New Rapport

        Try

            Requete &= " Select dbo.Filtre.idFiltre, dbo.Filtre.nomColonne, dbo.Filtre.libelleFiltre, dbo.Filtre.longueurFiltre, dbo.Filtre.estVisible, dbo.Filtre.estLieFiltre, dbo.Filtre.valeurFiltre, "
            Requete &= " dbo.Filtre.estTous, dbo.Filtre.estRestriction, dbo.Filtre.estMultiselection, dbo.Filtre.idFiltreRedirection,"
            Requete &= " dbo.ParamRapportFiltre.idClient, dbo.ParamRapportFiltre.idUtilisateur, dbo.ParamRapportFiltre.idModule, dbo.ParamRapportFiltre.ordreFiltre"
            Requete &= " FROM dbo.Filtre INNER JOIN"
            Requete &= " dbo.ParamRapportFiltre On dbo.Filtre.idFiltre = dbo.ParamRapportFiltre.idFiltre And dbo.Filtre.idClient = dbo.ParamRapportFiltre.idClient"

            Requete &= " Where "
            For Each orapport In orapports
                If orapport.nOngletOrdreNiv1 = oRapportReference.nOngletOrdreNiv1 And orapport.nOngletOrdreNiv2 = oRapportReference.nOngletOrdreNiv2 Then
                    If Requete_Where <> "" Then Requete_Where &= " Or "
                    Requete_Where &= "dbo.ParamRapportFiltre.idRapport = " & orapport.nIdRapport
                End If
            Next
            Requete &= Requete_Where

            Requete &= " GROUP BY dbo.Filtre.idFiltre, dbo.Filtre.nomColonne, dbo.Filtre.libelleFiltre, dbo.Filtre.longueurFiltre, dbo.Filtre.valeurFiltre, dbo.Filtre.idFiltreRedirection, dbo.Filtre.estVisible, "
            Requete &= " dbo.Filtre.estLieFiltre, dbo.Filtre.estTous, dbo.Filtre.estRestriction, dbo.Filtre.estMultiselection,"
            Requete &= " dbo.ParamRapportFiltre.idClient, dbo.ParamRapportFiltre.idUtilisateur, dbo.ParamRapportFiltre.idModule, dbo.ParamRapportFiltre.ordreFiltre"

            Requete &= " Having dbo.ParamRapportFiltre.idClient = " & orapports.Item(0).oClient.nIdClient & " And dbo.ParamRapportFiltre.idUtilisateur = " & orapports.Item(0).oUtilisateur.nIdUtilisateur
            Requete &= " And dbo.ParamRapportFiltre.idModule = " & orapports.Item(0).oModule.nIdModule
            Requete &= " ORDER BY dbo.ParamRapportFiltre.ordreFiltre"

            '' *** - MODIFICATION DES VALEURS SI UTILISATEUR SECONDAIRE ET UN FILTRE AVEC LE MEME ID EST PRESENT - *** '
            'Dim oUtilisateurSecondaireFiltres As New UtilisateurSecondaireFiltres()
            'If sidUtilisateurSecondaire <> "0" Then
            '    Dim oUtilisateurSecondaire As New UtilisateurSecondaire()
            '    oUtilisateurSecondaire.Req_Infos_Utilisateur_Secondaire(CInt(sidUtilisateurSecondaire), CInt(orapport.oClient.nIdClient))
            '    oUtilisateurSecondaireFiltres.chargerUtilisateurSecondaireFiltres(oUtilisateurSecondaire)
            'End If
            ' *** FIN MODIFICATION DES VALEURS SI UTILISATEUR SECONDAIRE ET UN FILTRE AVEC LE MEME ID EST PRESENT *** '

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim oFiltre As New Filtre()
                ChargerPropriete(oFiltre.oClient, orapport.oClient)
                ChargerPropriete(oFiltre.nIdFiltre, readerSQL.Item("idFiltre"))
                ChargerPropriete(oFiltre.sNomColonne, readerSQL.Item("nomColonne"))
                ChargerPropriete(oFiltre.sLibelleFiltre, readerSQL.Item("libelleFiltre"))
                ChargerPropriete(oFiltre.nLongueurFiltre, readerSQL.Item("longueurFiltre"))
                ChargerPropriete(oFiltre.bEstVisible, readerSQL.Item("estVisible"))
                ChargerPropriete(oFiltre.bEstLieFiltre, readerSQL.Item("estLieFiltre"))
                ChargerPropriete(oFiltre.sValeurFiltre, readerSQL.Item("valeurFiltre"))
                ChargerPropriete(oFiltre.bEstTous, readerSQL.Item("estTous"))
                ChargerPropriete(oFiltre.bEstRestriction, readerSQL.Item("estRestriction"))
                ChargerPropriete(oFiltre.bEstMultiselection, readerSQL.Item("estMultiselection"))
                ChargerPropriete(oFiltre.nIdFiltreRedirection, readerSQL.Item("idFiltreRedirection"))
                ChargerPropriete(oFiltre.sTypeColonne, ChargerTypeColonneFiltre(oFiltre, orapports))

                ' *** - MODIFICATION DES VALEURS SI UTILISATEUR SECONDAIRE ET UN FILTRE AVEC LE MEME ID EST PRESENT - *** '
                For Each oUtilisateurSecondaireFiltre As UtilisateurSecondaireFiltre In oUtilisateurSecondaireFiltres
                    If oFiltre.nIdFiltre = oUtilisateurSecondaireFiltre.oFiltre.nIdFiltre Then
                        oFiltre.sValeurFiltre = oUtilisateurSecondaireFiltre.sValeurFiltre
                        oFiltre.bEstTous = oUtilisateurSecondaireFiltre.bEstTous
                        oFiltre.bEstRestriction = oUtilisateurSecondaireFiltre.bEstRestriction
                    End If
                Next
                ' *** FIN MODIFICATION DES VALEURS SI UTILISATEUR SECONDAIRE ET UN FILTRE AVEC LE MEME ID EST PRESENT *** '

                oFiltres.Add(oFiltre)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Function ChargerTypeColonneFiltre(ByVal oFiltre As Filtre, ByVal Orapports As Rapports) As String
        Dim Resultat As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim Requete As String = ""
        Dim sql As cSQL = Nothing

        For Each oRapport In Orapports
            If Resultat = "" Then
                sql = New cSQL((oRapport.oClient.sBaseClient))

                Requete = "Select DATA_TYPE"
                Requete &= " FROM INFORMATION_SCHEMA.COLUMNS"
                Requete &= " WHERE TABLE_NAME = '" & oRapport.sTableRapport & "' AND COLUMN_NAME = N'" & oFiltre.sNomColonne & "'"

                Try
                    readerSQL = sql.chargerDataReader(Requete)
                    Do Until Not readerSQL.Read()
                        Resultat = CStr(readerSQL.Item("DATA_TYPE"))
                    Loop

                    Select Case Resultat
                        Case "bigint", "numeric", "smallint", "decimal", "smallmoney", "int", "tinyint", "money", "float", "real"
                            Resultat = "numerique"
                        Case "date", "datetimeoffset", "datetime2", "smalldatetime", "datetime", "time"
                            Resultat = "date"
                        Case "char", "varchar", "text", "nchar", "nvarchar", "ntext"
                            Resultat = "texte"
                        Case "bit"
                            Resultat = "booleen"
                    End Select

                Catch ex As Exception
                    Throw ex
                Finally
                    If Not readerSQL Is Nothing Then
                        sql.libererDataReader(readerSQL)
                    End If
                    sql.fermerConnexion()
                    sql = Nothing
                End Try
            End If
        Next

        Return Resultat
    End Function

    Public Shared Function ChargerTypeColonne(ByVal sBase As String, ByVal sTable As String, ByVal sChamp As String) As String
        Dim Resultat As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim Requete As String = ""
        Dim sql As cSQL = Nothing


        sql = New cSQL((sBase))

        Requete = "SELECT DATA_TYPE"
        Requete &= " FROM INFORMATION_SCHEMA.COLUMNS"
        Requete &= " WHERE TABLE_NAME = '" & sTable & "' AND COLUMN_NAME = N'" & sChamp & "'"

        Try
            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Resultat = CStr(readerSQL.Item("DATA_TYPE"))
            Loop

            Select Case Resultat
                Case "bigint", "numeric", "smallint", "decimal", "smallmoney", "int", "tinyint", "money", "float", "real"
                    Resultat = "numerique"
                Case "date", "datetimeoffset", "datetime2", "smalldatetime", "datetime", "time"
                    Resultat = "date"
                Case "char", "varchar", "text", "nchar", "nvarchar", "ntext"
                    Resultat = "texte"
                Case "bit"
                    Resultat = "booleen"
            End Select

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try


        Return Resultat
    End Function

    Public Shared Sub ChargerValeursFiltre(ByVal sListeIdRapports As String, ByVal oFiltres As Filtres, ByVal oRapport As Rapport, ByVal oRapports As Rapports, ByVal IdDiv As String, ByVal ListeControles As List(Of Object), ByRef ListeValeurs As List(Of ArrayList), ByVal bMoisGlissant As Boolean, ByVal ChaineRedirection As String)
        Dim sql As New cSQL(oRapport.oClient.sBaseClient)
        Dim Requete As String = ""
        Dim Requete_Where As String = ""
        Dim Requete_Having As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim PremierPassageWhere As Boolean = True
        Dim ControleTrouve As Object
        Dim TableValeurs As New ArrayList
        Dim PremierPassageWhereMulti As Boolean = True
        Dim PremierPassageHavingMulti As Boolean = True
        Dim Valeur As String = ""
        Dim PremierPassageHaving As Boolean = True
        Dim oRapportTrouveGeneral As New Rapport
        Dim oRapportTrouve As New Rapport
        Dim sItemSelectionne As String = ""
        Dim Filtreactu As String = "Tous"
        'Try

        Dim sTous As String = "Tous"
        If oRapport.oClient.sLangueClient = "US" Then sTous = "All"

        For Each oFiltreActuel In oFiltres
            Dim oFiltreTest As New Filtre
            Dim nIdRapportTrouveGeneral As Integer = oFiltreTest.TesterFiltrePresentRapport(oFiltreActuel.nIdFiltre, oRapport, oRapports, False)
            If nIdRapportTrouveGeneral > 0 Then
                oRapportTrouveGeneral = oRapports.Find(Function(p) p.nIdRapport = nIdRapportTrouveGeneral)

                Requete = ""
                Requete_Having = ""
                Requete_Where = ""

                PremierPassageWhere = True
                PremierPassageHaving = True

                'If oFiltreActuel.sTypeColonne = "booleen" Then
                '    Requete &= "SELECT CASE WHEN dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "] = 0 THEN 'Non' ELSE 'Oui' END AS [" & oFiltreActuel.sNomColonne & "]"
                'Else


                Requete &= "SELECT dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]"
                'End If
                Requete &= " FROM dbo.[" & oRapportTrouveGeneral.sTableRapport & "]"


                If oFiltreActuel.bEstLieFiltre Then
                    For Each oFiltre In oFiltres
                        Dim nIdRapportTrouve As Integer = oFiltreTest.TesterFiltrePresentRapport(oFiltre.nIdFiltre, oRapport, oRapports, False)
                        If nIdRapportTrouve > 0 Then
                            If oFiltreActuel.nIdFiltre <> oFiltre.nIdFiltre And nIdRapportTrouve = oRapportTrouveGeneral.nIdRapport Then
                                oRapportTrouve = oRapports.Find(Function(p) p.nIdRapport = nIdRapportTrouve)
                                ControleTrouve = Nothing

                                sItemSelectionne = ""
                                For i = 0 To ListeControles.Count - 1
                                    If TypeOf ListeControles(i) Is System.Web.UI.WebControls.DropDownList Then
                                        Dim ddlControle As DropDownList = CType(ListeControles(i), DropDownList)
                                        If ddlControle.ID = "Filtre_" & IdDiv & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports Then
                                            ControleTrouve = ListeControles(i)
                                            If Not HttpContext.Current.Request("Filtre_" & IdDiv & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports) Is Nothing Then
                                                sItemSelectionne = HttpContext.Current.Request("Filtre_" & IdDiv & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports).ToString
                                            Else
                                                If ChaineRedirection <> "" Then
                                                    For Each sTexte In ChaineRedirection.Split(CChar("µ"))
                                                        If CInt(sTexte.Split(CChar("="))(0)) = oFiltre.nIdFiltre Then
                                                            sItemSelectionne = sTexte.Split(CChar("="))(1)
                                                        End If
                                                    Next
                                                End If
                                            End If

                                        End If
                                    ElseIf TypeOf ListeControles(i) Is System.Web.UI.WebControls.ListBox Then
                                        Dim ddlControle As ListBox = CType(ListeControles(i), ListBox)
                                        If ddlControle.ID = "Filtre_" & IdDiv & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports Then
                                            ControleTrouve = ListeControles(i)
                                            If Not HttpContext.Current.Request("Filtre_" & IdDiv & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports) Is Nothing Then
                                                sItemSelectionne = ""
                                                For Each item As String In HttpContext.Current.Request("Filtre_" & IdDiv & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports).ToString.Split(CChar(","))
                                                    sItemSelectionne += item + ";"
                                                Next
                                            Else
                                                If ChaineRedirection <> "" Then
                                                    For Each sTexte In ChaineRedirection.Split(CChar("µ"))
                                                        If CInt(sTexte.Split(CChar("="))(0)) = oFiltre.nIdFiltre Then
                                                            sItemSelectionne += sTexte.Split(CChar("="))(1) + ";"
                                                        End If
                                                    Next
                                                End If
                                                If sItemSelectionne <> "" Then sItemSelectionne = sItemSelectionne.Remove(sItemSelectionne.Length - 1, 1)
                                            End If

                                        End If
                                    End If
                                Next

                                If Not IsNothing(ControleTrouve) Then
                                    Select Case oFiltre.bEstMultiselection
                                        Case True
                                            Dim oControle As ListBox = CType(ControleTrouve, ListBox)

                                            PremierPassageWhereMulti = True
                                            '---Multiselection / Valeur selectionee
                                            For Each ItemSelectionne In sItemSelectionne.Split(CChar(";"))
                                                If ItemSelectionne <> sTous And ItemSelectionne <> "" And oFiltre.sTypeColonne <> "" Then
                                                    If PremierPassageWhere = True Then
                                                        PremierPassageWhere = False
                                                        PremierPassageWhereMulti = False
                                                        Requete_Where &= " WHERE ("
                                                    Else
                                                        If PremierPassageWhereMulti = True Then
                                                            PremierPassageWhereMulti = False
                                                            Requete_Where &= " And ("
                                                        Else
                                                            Requete_Where &= " OR "
                                                        End If

                                                    End If
                                                    Valeur = Toolbox.ConvertirValeurFiltre(ItemSelectionne, oFiltre.sNomColonne, oRapport.oClient)
                                                    Select Case oFiltre.sTypeColonne
                                                        Case "numerique"
                                                            If oFiltre.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                                                                Requete_Where &= " (dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                                Requete_Where &= " OR "
                                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & (Toolbox.ReplaceQuote(CStr(CInt(Valeur) - 1))) & ")"
                                                            Else
                                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                            End If
                                                        Case "texte", "date"
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "] Like '%" & Toolbox.ReplaceQuote(Valeur) & "%'"
                                                        Case "booleen"
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                                    End Select
                                                End If
                                            Next
                                            If PremierPassageWhereMulti = False Then Requete_Where &= ")"

                                            '---Pas encore de données mais valeur par defaut présente
                                            If sItemSelectionne = "" And oFiltre.sValeurFiltre <> sTous And oFiltre.sTypeColonne <> "" Then
                                                PremierPassageWhereMulti = True
                                                For Each Valeur In oFiltre.sValeurFiltre.Split(CChar(";"))
                                                    If PremierPassageWhere = True Then
                                                        Requete_Where &= " WHERE ("
                                                        PremierPassageWhere = False
                                                        PremierPassageWhereMulti = False
                                                    Else
                                                        If PremierPassageWhereMulti = True Then
                                                            PremierPassageWhereMulti = False
                                                            Requete_Where &= " And ("
                                                        Else
                                                            Requete_Where &= " OR "
                                                        End If
                                                    End If
                                                    Select Case oFiltre.sTypeColonne
                                                        Case "numerique"
                                                            If oFiltre.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                                                                Requete_Where &= " (dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                                Requete_Where &= " OR "
                                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & (Toolbox.ReplaceQuote(CStr(CInt(Valeur) - 1))) & ")"
                                                            Else
                                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                            End If
                                                        Case "texte", "date"
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "] Like '%" & Toolbox.ReplaceQuote(Valeur) & "%'"
                                                        Case "booleen"
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                                    End Select

                                                Next
                                                If PremierPassageWhereMulti = False Then Requete_Where &= ")"
                                            End If

                                            '--- si Est restriction et Tous est selectionne
                                            If oFiltre.bEstRestriction = True And oFiltre.bEstTous = True And sItemSelectionne = sTous And oFiltre.sTypeColonne <> "" Then
                                                PremierPassageWhereMulti = True
                                                For Each Valeur In oFiltre.sValeurFiltre.Split(CChar(";"))
                                                    If PremierPassageWhere = True Then
                                                        Requete_Where &= " WHERE ("
                                                        PremierPassageWhere = False
                                                        PremierPassageWhereMulti = False
                                                    Else
                                                        If PremierPassageWhereMulti = True Then
                                                            PremierPassageWhereMulti = False
                                                            Requete_Where &= " And ("
                                                        Else
                                                            Requete_Where &= " OR "
                                                        End If
                                                    End If
                                                    Select Case oFiltre.sTypeColonne
                                                        Case "numerique"
                                                            If oFiltre.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                                                                Requete_Where &= " (dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                                Requete_Where &= " OR "
                                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & (Toolbox.ReplaceQuote(CStr(CInt(Valeur) - 1))) & ")"
                                                            Else
                                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                            End If
                                                        Case "texte", "date"
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "] Like '%" & Toolbox.ReplaceQuote(Valeur) & "%'"
                                                        Case "booleen"
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                                    End Select

                                                Next
                                                If PremierPassageWhereMulti = False Then Requete_Where &= ")"
                                            End If

                                        Case False
                                            Dim oControle As DropDownList = CType(ControleTrouve, DropDownList)
                                            'DropdownList / Valeur Séléctionnee
                                            Filtreactu = oFiltre.sValeurFiltre
                                            If sItemSelectionne <> sTous And sItemSelectionne <> "" And oFiltre.sTypeColonne <> "" Then
                                                If PremierPassageWhere = True Then
                                                    PremierPassageWhere = False
                                                    Requete_Where &= " WHERE "
                                                Else
                                                    Requete_Where &= " And "
                                                End If
                                                Valeur = Toolbox.ConvertirValeurFiltre(sItemSelectionne, oFiltre.sNomColonne, oRapport.oClient)
                                                Select Case oFiltre.sTypeColonne
                                                    Case "numerique"
                                                        If oFiltre.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                                                            Requete_Where &= " (dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                            Requete_Where &= " OR "
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & (Toolbox.ReplaceQuote(CStr(CInt(Valeur) - 1))) & ")"
                                                        Else
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                        End If
                                                    Case "texte", "date"
                                                        '  Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "] like '%" & Toolbox.ReplaceQuote(Valeur) & "%'"
                                                        ' Dim ressource As String = "Fabrice"
                                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "] Like '%Fabrice Brindejonc%'"
                                                    Case "booleen"
                                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                                End Select
                                                '---Pas encore de données mais valeur par defaut présente

                                            ElseIf sItemSelectionne = "" And oFiltre.sValeurFiltre <> sTous And oFiltre.sValeurFiltre <> "#Derniere valeur" And oFiltre.sTypeColonne <> "" Then
                                                '---Si Est tous = false (on ne filtre que sur la première valeur)
                                                If oFiltre.bEstTous = False Then
                                                    If PremierPassageWhere = True Then
                                                        PremierPassageWhere = False
                                                        Requete_Where &= " WHERE "
                                                    Else
                                                        Requete_Where &= " And "
                                                    End If
                                                    Select Case oFiltre.sTypeColonne
                                                        Case "numerique"
                                                            If oFiltre.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                                                                Requete_Where &= " (dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(oFiltre.sValeurFiltre.Split(CChar(";"))(0))
                                                                Requete_Where &= " OR "
                                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & (Toolbox.ReplaceQuote(CStr(CInt(oFiltre.sValeurFiltre.Split(CChar(";"))(0)) - 1))) & ")"
                                                            Else
                                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(oFiltre.sValeurFiltre.Split(CChar(";"))(0))
                                                            End If

                                                        Case "texte", "date"
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "] like '%" & Toolbox.ReplaceQuote(oFiltre.sValeurFiltre.Split(CChar(";"))(0)) & "%'"
                                                        Case "booleen"
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(oFiltre.sValeurFiltre.Split(CChar(";"))(0))
                                                    End Select
                                                Else
                                                    '--- Si est Tous = true  (on filtre sur toutes les valeurs)
                                                    PremierPassageWhereMulti = True
                                                    For Each Valeur In oFiltre.sValeurFiltre.Split(CChar(";"))
                                                        If oFiltre.sTypeColonne <> "" Then
                                                            If PremierPassageWhere = True Then
                                                                Requete_Where &= " WHERE ("
                                                                PremierPassageWhere = False
                                                                PremierPassageWhereMulti = False
                                                            Else
                                                                If PremierPassageWhereMulti = True Then
                                                                    PremierPassageWhereMulti = False
                                                                    Requete_Where &= " And ("
                                                                Else
                                                                    Requete_Where &= " OR "
                                                                End If
                                                            End If
                                                            Select Case oFiltre.sTypeColonne
                                                                Case "numerique"
                                                                    If oFiltre.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                                                                        Requete_Where &= " (dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                                        Requete_Where &= " OR "
                                                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & (Toolbox.ReplaceQuote(CStr(CInt(Valeur) - 1))) & ")"
                                                                    Else
                                                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                                    End If
                                                                Case "texte", "date"
                                                                    Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "] Like '%" & Toolbox.ReplaceQuote(Valeur) & "%'"
                                                                Case "booleen"
                                                                    Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                                            End Select
                                                        End If
                                                    Next
                                                    If PremierPassageWhereMulti = False Then Requete_Where &= ")"
                                                End If
                                                '---Pas encore de données mais valeur par defaut à calculer 
                                            ElseIf sItemSelectionne = "" And oFiltre.sValeurFiltre = "#Derniere valeur" And oFiltre.sTypeColonne <> "" Then
                                                Dim DerniereValeur As String = ""
                                                If Not IsNothing(oFiltre.sDernierValeur) Then
                                                    DerniereValeur = oFiltre.sDernierValeur
                                                Else
                                                    DerniereValeur = ChargerDerniereValeurFiltre(oFiltre, oFiltres, oRapportTrouve, False)
                                                    oFiltre.sDernierValeur = DerniereValeur
                                                End If

                                                If PremierPassageWhere = True Then
                                                    PremierPassageWhere = False
                                                    Requete_Where &= " WHERE "
                                                Else
                                                    Requete_Where &= " And "
                                                End If
                                                Select Case oFiltre.sTypeColonne
                                                    Case "numerique"
                                                        If oFiltre.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                                                            Requete_Where &= " (dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(DerniereValeur)
                                                            Requete_Where &= " OR "
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & (Toolbox.ReplaceQuote(CStr(CInt(DerniereValeur) - 1))) & ")"
                                                        Else
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(DerniereValeur)
                                                        End If
                                                    Case "texte", "date"
                                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "] Like '%" & Toolbox.ReplaceQuote(Valeur) & "%'"
                                                    Case "booleen"
                                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(DerniereValeur)
                                                End Select
                                                '--- si Est restriction et Tous est selectionne
                                            ElseIf oFiltre.bEstRestriction = True And oFiltre.bEstTous = True And sItemSelectionne = sTous And oFiltre.sTypeColonne <> "" Then
                                                PremierPassageWhereMulti = True
                                                For Each Valeur In oFiltre.sValeurFiltre.Split(CChar(";"))
                                                    If PremierPassageWhere = True Then
                                                        Requete_Where &= " WHERE ("
                                                        PremierPassageWhere = False
                                                        PremierPassageWhereMulti = False
                                                    Else
                                                        If PremierPassageWhereMulti = True Then
                                                            PremierPassageWhereMulti = False
                                                            Requete_Where &= " And ("
                                                        Else
                                                            Requete_Where &= " OR "
                                                        End If
                                                    End If
                                                    Select Case oFiltre.sTypeColonne
                                                        Case "numerique"
                                                            If oFiltre.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                                                                Requete_Where &= " (dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                                Requete_Where &= " OR "
                                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & (Toolbox.ReplaceQuote(CStr(CInt(Valeur) - 1))) & ")"
                                                            Else
                                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                            End If
                                                        Case "texte", "date"
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "] Like '%" & Toolbox.ReplaceQuote(Valeur) & "%'"
                                                        Case "booleen"
                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                                    End Select

                                                Next
                                                If PremierPassageWhereMulti = False Then Requete_Where &= ")"
                                            End If

                                            '---Gestion du where des redirections
                                            If oFiltre.bEstVisible = False Then
                                                If Not ChaineRedirection Is Nothing AndAlso ChaineRedirection <> "" Then
                                                    Dim sTableFiltreRedirection = ChaineRedirection.Split(CChar("{"))(0).Split(CChar("µ"))
                                                    For Each TableFiltreRedirection In sTableFiltreRedirection
                                                        If CInt(TableFiltreRedirection.Split(CChar("="))(0)) = oFiltre.nIdFiltre Then
                                                            Valeur = TableFiltreRedirection.Split(CChar("="))(1)
                                                            If Valeur <> "" And Valeur.ToUpper <> sTous.ToUpper And oFiltre.sTypeColonne <> "" Then
                                                                If PremierPassageWhere = True Then
                                                                    PremierPassageWhere = False
                                                                    Requete_Where &= " WHERE "
                                                                Else
                                                                    Requete_Where &= " And "
                                                                End If

                                                                Select Case oFiltre.sTypeColonne
                                                                    Case "numerique"
                                                                        If oFiltre.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                                                                            Requete_Where &= " (dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                                            Requete_Where &= " OR "
                                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & (Toolbox.ReplaceQuote(CStr(CInt(Valeur) - 1))) & ")"
                                                                        Else
                                                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Toolbox.ConvertirValeurFiltre(Valeur, oFiltre.sNomColonne, oRapport.oClient))
                                                                        End If
                                                                    Case "texte", "date"
                                                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "] like '%" & Toolbox.ReplaceQuote(Toolbox.ConvertirValeurFiltre(Valeur, oFiltre.sNomColonne, oRapport.oClient)) & "%'"
                                                                    Case "booleen"
                                                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Toolbox.ConvertirValeurFiltre(Valeur, oFiltre.sNomColonne, oRapport.oClient))
                                                                End Select
                                                            End If
                                                        End If
                                                    Next
                                                End If
                                            End If

                                    End Select
                                End If
                            End If
                        End If
                    Next

                    If oFiltreActuel.bEstRestriction = True And oFiltreActuel.sTypeColonne <> "" Then
                        PremierPassageHavingMulti = True
                        For Each Valeur In oFiltreActuel.sValeurFiltre.Split(CChar(";"))
                            If PremierPassageHaving = True Then
                                Requete_Having &= " HAVING ("
                                PremierPassageHaving = False
                                PremierPassageHavingMulti = False
                            Else
                                If PremierPassageHavingMulti = True Then
                                    PremierPassageHavingMulti = False
                                    Requete_Having &= " And ("
                                Else
                                    Requete_Having &= " OR "
                                End If
                            End If
                            'If (sListeIdRapports = "98") And (oFiltreActuel.nIdFiltre = 180) Then
                            '    Requete_Having = Requete_Having + "dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "] Like '%" & Toolbox.ReplaceQuote(Valeur) & "%'"
                            'Else
                            Select Case oFiltreActuel.sTypeColonne
                                Case "numerique"
                                    If oFiltreActuel.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                                        Requete_Where &= " (dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                        Requete_Where &= " OR "
                                        Requete_Where &= " dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]=" & (Toolbox.ReplaceQuote(CStr(CInt(Valeur) - 1))) & ")"
                                    Else
                                        Requete_Where &= " dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]=" & Toolbox.ReplaceQuote(Toolbox.ConvertirValeurFiltre(Valeur, oFiltreActuel.sNomColonne, oRapport.oClient))
                                    End If
                                Case "texte", "date"
                                    Requete_Having &= " dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "] Like '%" & Toolbox.ReplaceQuote(Valeur) & "%'"
                                Case "booleen"
                                    Requete_Having &= " dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                            End Select
                            'End If

                        Next
                        If PremierPassageHavingMulti = False Then Requete_Having &= ")"
                    End If

                End If

                ''---Restriction du filtre annee à la derniere annee quand on est en mode Mois Glissant
                'If oFiltreActuel.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                '    If PremierPassageWhere = True Then
                '        Requete_Where &= " WHERE "
                '        PremierPassageWhere = False
                '        PremierPassageWhereMulti = False
                '    Else
                '        If PremierPassageWhereMulti = True Then
                '            PremierPassageWhereMulti = False
                '            Requete_Where &= " And ("
                '        Else
                '            Requete_Where &= " OR "
                '        End If
                '    End If
                '    Valeur = CStr(Recuperer_Dernier_Annee_Table(oRapportTrouve))
                '    Select Case oFiltreActuel.sTypeColonne
                '        Case "numerique"
                '            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                '    End Select
                'End If


                '---Gestion du where des redirections
                If oFiltreActuel.bEstVisible = False Then
                    If Not ChaineRedirection Is Nothing AndAlso ChaineRedirection <> "" Then
                        Dim sTableFiltreRedirection = ChaineRedirection.Split(CChar("µ"))
                        For Each TableFiltreRedirection In sTableFiltreRedirection
                            If CInt(TableFiltreRedirection.Split(CChar("="))(0)) = oFiltreActuel.nIdFiltre Then
                                Valeur = TableFiltreRedirection.Split(CChar("="))(1)
                                If Valeur <> "" And oFiltreActuel.sTypeColonne <> "" Then
                                    If PremierPassageWhere = True Then
                                        PremierPassageWhere = False
                                        Requete_Where &= " WHERE "
                                    Else
                                        Requete_Where &= " And "
                                    End If

                                    Select Case oFiltreActuel.sTypeColonne
                                        Case "numerique"
                                            If oFiltreActuel.sNomColonne.ToUpper = "ANNEE" And bMoisGlissant = True Then
                                                Requete_Where &= " (dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                                Requete_Where &= " OR "
                                                Requete_Where &= " dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]=" & (Toolbox.ReplaceQuote(CStr(CInt(Valeur) - 1))) & ")"
                                            Else
                                                Requete_Where &= " dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]=" & Toolbox.ReplaceQuote(Toolbox.ConvertirValeurFiltre(Valeur, oFiltreActuel.sNomColonne, oRapport.oClient))
                                            End If
                                        Case "texte", "date"
                                            Requete_Where &= " dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "] like '%" & Toolbox.ReplaceQuote(Toolbox.ConvertirValeurFiltre(Valeur, oFiltreActuel.sNomColonne, oRapport.oClient)) & "%'"
                                        Case "booleen"
                                            Requete_Where &= " dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Toolbox.ConvertirValeurFiltre(Valeur, oFiltreActuel.sNomColonne, oRapport.oClient))
                                    End Select
                                End If
                            End If
                        Next
                    End If
                End If


                If Requete_Where <> " WHERE " Then
                    Requete &= Requete_Where
                End If

                'If oFiltreActuel.sTypeColonne = "booleen" Then
                '    Requete &= " GROUP BY CASE WHEN dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "] = 0 THEN 'Non' ELSE 'Oui' END"
                'Else

                Requete &= " GROUP BY dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]"


                Requete &= Requete_Having
                Requete &= " ORDER BY dbo.[" & oRapportTrouveGeneral.sTableRapport & "].[" & oFiltreActuel.sNomColonne & "]"
                Filtreactu = Filtreactu

                If (sListeIdRapports = "99") And (oFiltreActuel.nIdFiltre = 180) Then


                    TableValeurs = New ArrayList
                    'TableValeurs.Add("Hasina")
                    'TableValeurs.Add("Fabrice Brindejonc")
                    'TableValeurs.Add("CD 31")

                    ListeValeurs.Add(TableValeurs)
                    readerSQL = sql.chargerDataReader(Requete)

                    Do Until Not readerSQL.Read()

                        If readerSQL.Item(oFiltreActuel.sNomColonne).ToString <> "" Then
                            If oFiltreActuel.sTypeColonne = "booleen" Then
                                TableValeurs.Add(Toolbox.ReplaceBooleenTexte(readerSQL.Item(oFiltreActuel.sNomColonne).ToString))
                            Else
                                TableValeurs.Add(readerSQL.Item(oFiltreActuel.sNomColonne))
                            End If
                        End If
                    Loop


                    sql.libererDataReader(readerSQL)
                    ' ListeValeurs.Add(TableValeurs)
                    '    'Else
                    '    '    TableValeurs = New ArrayList
                    '    '    If Not oFiltreActuel.sValeurFiltre Is Nothing AndAlso oFiltreActuel.sValeurFiltre <> "" Then
                    '    '        For Each sValeur As String In oFiltreActuel.sValeurFiltre.Split(CChar(";"))
                    '    '            If sValeur <> "" Then
                    '    '                If oFiltreActuel.sTypeColonne = "booleen" Then
                    '    '                    TableValeurs.Add(Toolbox.ReplaceBooleenTexte(sValeur))
                    '    '                Else
                    '    '                    TableValeurs.Add(sValeur)
                    '    '                End If
                    '    '            End If
                    '    '        Next
                    '    '    End If
                    '    '    ListeValeurs.Add(TableValeurs)
                    '    'End If

                Else

                    If oFiltreActuel.bEstVisible = True And (oFiltreActuel.nIdFiltre <> 180) Then

                        readerSQL = sql.chargerDataReader(Requete)
                        TableValeurs = New ArrayList
                        Do Until Not readerSQL.Read()
                            If readerSQL.Item(oFiltreActuel.sNomColonne).ToString <> "" Then
                                If oFiltreActuel.sTypeColonne = "booleen" Then
                                    TableValeurs.Add(Toolbox.ReplaceBooleenTexte(readerSQL.Item(oFiltreActuel.sNomColonne).ToString))
                                Else
                                    TableValeurs.Add(readerSQL.Item(oFiltreActuel.sNomColonne))
                                End If
                            End If
                        Loop
                        sql.libererDataReader(readerSQL)
                        ListeValeurs.Add(TableValeurs)
                    Else
                        TableValeurs = New ArrayList
                        If Not oFiltreActuel.sValeurFiltre Is Nothing AndAlso oFiltreActuel.sValeurFiltre <> "" Then
                            For Each sValeur As String In oFiltreActuel.sValeurFiltre.Split(CChar(";"))
                                If sValeur <> "" Then
                                    If oFiltreActuel.sTypeColonne = "booleen" Then
                                        TableValeurs.Add(Toolbox.ReplaceBooleenTexte(sValeur))
                                    Else
                                        TableValeurs.Add(sValeur)
                                    End If
                                End If
                            Next
                        End If
                        ListeValeurs.Add(TableValeurs)
                    End If
                End If
            End If
        Next
        ' Catch ex As Exception
        'Throw ex
        'Finally
        If Not readerSQL Is Nothing Then
            sql.libererDataReader(readerSQL)
        End If
        sql.fermerConnexion()
        sql = Nothing
        'End Try
    End Sub

    Private Shared Function ChargerDerniereValeurFiltre(ByRef oFiltre As Filtre, ByRef oFiltres As Filtres, ByVal oRapport As Rapport, ByVal ChercherautresDerniereValeur As Boolean) As String
        Dim sql As New cSQL(oRapport.oClient.sBaseClient)
        Dim Requete As String = ""
        Dim Requete_Where As String = ""
        Dim Requete_Having As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim PremierPassageWhere As Boolean = True
        Dim PremierPassageWhereMulti As Boolean = True
        Dim PremierPassageHavingMulti As Boolean = True
        Dim Resultat As String = ""
        Dim PremierPassageHaving As Boolean = True
        'Try

        Dim sTous As String = "Tous"
        If oRapport.oClient.sLangueClient = "US" Then sTous = "All"

        Requete &= "Select Max(dbo.[" & oRapport.sTableRapport & "].[" & oFiltre.sNomColonne & "]) As " & oFiltre.sNomColonne
        Requete &= " FROM dbo.[" & oRapport.sTableRapport & "]"

        For Each oFiltreBoucle In oFiltres
            If oFiltre.nIdFiltre <> oFiltreBoucle.nIdFiltre Then
                If oFiltreBoucle.sValeurFiltre <> sTous And oFiltreBoucle.sValeurFiltre <> "" And oFiltreBoucle.sValeurFiltre <> "#Derniere valeur" Then
                    PremierPassageWhereMulti = True
                    For Each Valeur In oFiltreBoucle.sValeurFiltre.Split(CChar(";"))
                        If PremierPassageWhere = True Then
                            Requete_Where &= " WHERE ("
                            PremierPassageWhere = False
                            PremierPassageWhereMulti = False
                        Else
                            If PremierPassageWhereMulti = True Then
                                PremierPassageWhereMulti = False
                                Requete_Where &= " And ("
                            Else
                                Requete_Where &= " Or "
                            End If
                        End If
                        Select Case oFiltreBoucle.sTypeColonne
                            Case "numerique"
                                Requete_Where &= " dbo.[" & oRapport.sTableRapport & "].[" & oFiltreBoucle.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                            Case "texte", "Date"
                                Requete_Where &= " dbo.[" & oRapport.sTableRapport & "].[" & oFiltreBoucle.sNomColonne & "]='" & Toolbox.ReplaceQuote(Valeur) & "'"
                        End Select
                    Next
                    If PremierPassageWhereMulti = False Then Requete_Where &= ")"
                End If
                If (oFiltreBoucle.sValeurFiltre = "#Derniere valeur" And ChercherautresDerniereValeur = True) Or (oFiltreBoucle.sValeurFiltre = "#Derniere valeur" And Not IsNothing(oFiltreBoucle.sDernierValeur)) Then
                    Dim DerniereValeur As String = ""
                    If Not IsNothing(oFiltreBoucle.sDernierValeur) Then
                        DerniereValeur = oFiltreBoucle.sDernierValeur
                    Else
                        DerniereValeur = ChargerDerniereValeurFiltre(oFiltreBoucle, oFiltres, oRapport, True)
                        oFiltreBoucle.sDernierValeur = DerniereValeur
                    End If
                    If PremierPassageWhere = True Then
                        PremierPassageWhere = False
                        Requete_Where &= " WHERE "
                    Else
                        Requete_Where &= " And "
                    End If

                    Select Case oFiltreBoucle.sTypeColonne
                        Case "numerique"
                            Requete_Where &= " dbo.[" & oRapport.sTableRapport & "].[" & oFiltreBoucle.sNomColonne & "]=" & Toolbox.ReplaceQuote(DerniereValeur)
                        Case "texte", "date"
                            Requete_Where &= " dbo.[" & oRapport.sTableRapport & "].[" & oFiltreBoucle.sNomColonne & "]='" & Toolbox.ReplaceQuote(DerniereValeur) & "'"
                    End Select

                End If
            Else
                If oFiltre.sValeurFiltre <> sTous And oFiltre.sValeurFiltre <> "" And oFiltreBoucle.sValeurFiltre <> "#Derniere valeur" Then
                    PremierPassageHavingMulti = True

                    For Each Valeur In oFiltre.sValeurFiltre.Split(CChar(";"))

                        If PremierPassageHaving = True Then
                            Requete_Having &= " HAVING ("
                            PremierPassageHaving = False
                            PremierPassageHavingMulti = False
                        Else
                            If PremierPassageHavingMulti = True Then
                                PremierPassageHavingMulti = False
                                Requete_Having &= " And ("
                            Else
                                Requete_Having &= " OR "
                            End If
                        End If

                        Select Case oFiltreBoucle.sTypeColonne
                            Case "numerique"
                                Requete_Having &= " dbo.[" & oRapport.sTableRapport & "].[" & oFiltreBoucle.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                            Case "texte", "date"
                                Requete_Having &= " dbo.[" & oRapport.sTableRapport & "].[" & oFiltreBoucle.sNomColonne & "]='" & Toolbox.ReplaceQuote(Valeur) & "'"
                        End Select

                    Next
                    If PremierPassageHavingMulti = False Then Requete_Having &= ")"
                End If
            End If
        Next

        Requete &= Requete_Where
        'Requete &= " GROUP BY dbo.[" & oRapport.sTableRapport & "].[" & oFiltre.sNomColonne & "]"

        Requete &= Requete_Having

        readerSQL = sql.chargerDataReader(Requete)

        Do Until Not readerSQL.Read()
            If IsDBNull(readerSQL.Item(oFiltre.sNomColonne)) Then
                Resultat = "0"
            Else
                Resultat = CStr(readerSQL.Item(oFiltre.sNomColonne))
            End If

        Loop
        sql.libererDataReader(readerSQL)

        'Catch ex As Exception
        '    Throw ex
        'Finally
        If Not readerSQL Is Nothing Then
            sql.libererDataReader(readerSQL)
        End If
        sql.fermerConnexion()
        sql = Nothing
        ' End Try

        Return Resultat

    End Function

    Private Shared Sub ChargerPropriete(ByRef Propriete As Object, ByVal oItem As Object)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Acces, ByVal oItem As Acces)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Client, ByVal oItem As Client)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Colonne, ByVal oItem As Colonne)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Modul, ByVal oItem As Modul)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Utilisateur, ByVal oItem As Utilisateur)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Mailings, ByVal oItem As Mailings)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Article, ByVal oItem As Article)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Fonction, ByVal oItem As Fonction)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As CompteFacturant, ByVal oItem As CompteFacturant)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Discrimination, ByVal oItem As Discrimination)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Forfait, ByVal oItem As Forfait)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Hierarchie, ByVal oItem As Hierarchie)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As OptionsUtilisateur, ByVal oItem As OptionsUtilisateur)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub
    Private Shared Sub ChargerPropriete(ByRef Propriete As OptionProfil, ByVal oItem As OptionProfil)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As CategorieOptionUtilisateur, ByVal oItem As CategorieOptionUtilisateur)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub


    Private Shared Sub ChargerPropriete(ByRef Propriete As Stocks, ByVal oItem As Stocks)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub


    Private Shared Sub ChargerPropriete(ByRef Propriete As Operateur, ByVal oItem As Operateur)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub


    Private Shared Sub ChargerPropriete(ByRef Propriete As TypeForfait, ByVal oItem As TypeForfait)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As TypeMateriel, ByVal oItem As TypeMateriel)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Marque, ByVal oItem As Marque)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As HierarchieFixe, ByVal oItem As HierarchieFixe)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As CompteFacturantFixe, ByVal oItem As CompteFacturantFixe)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As UsageFixe, ByVal oItem As UsageFixe)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As TypeLigneFixe, ByVal oItem As TypeLigneFixe)
        If Not IsDBNull(oItem) Then
            Propriete = oItem
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Integer, ByVal oItem As Object)
        If Not IsDBNull(oItem) Then
            Propriete = Convert.ToInt32(oItem)
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Short, ByVal oItem As Object)
        If Not IsDBNull(oItem) Then
            Propriete = Convert.ToInt16(oItem)
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As UInt16, ByVal oItem As Object)
        If Not IsDBNull(oItem) Then
            Propriete = Convert.ToUInt16(oItem)
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Date, ByVal oItem As Object)
        If Not IsDBNull(oItem) Then
            Propriete = Convert.ToDateTime(oItem)
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Boolean, ByVal oItem As Object)
        If Not IsDBNull(oItem) Then
            Propriete = Convert.ToBoolean(oItem)
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As String, ByVal oItem As Object)
        If Not IsDBNull(oItem) Then
            Propriete = Convert.ToString(oItem)
        End If
    End Sub

    Private Shared Sub ChargerPropriete(ByRef Propriete As Double, ByVal oItem As Object)
        If Not IsDBNull(oItem) Then
            Propriete = Convert.ToDouble(oItem)
        End If
    End Sub

    Public Shared Sub ChargerListeColonnes(ByRef oColonnes As Colonnes, ByRef oRapport As Rapport, ByVal oCheckboxMoisGlissant As CheckBox, ByVal DivId As String, ByVal oBloquerColoneAction As HiddenField)
        Dim sRequete As String = ""
        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing
        Dim oColonne As Colonne


        Dim nNombreAnnees As Integer
        Dim nDebutMoisAnnee As Integer
        Dim nFinMoisAnnee As Integer
        Dim nReferenceAnnee As Integer
        Dim nColonneReference As Integer

        Dim bMoisGlissant As Boolean = False
        If Not oCheckboxMoisGlissant Is Nothing And oRapport.bAfficherCaseMoisGlissant = True Then
            If oCheckboxMoisGlissant.Checked = True Then
                bMoisGlissant = True
            End If
        Else
            If oRapport.bAfficherEnMoisGlissant = True Then bMoisGlissant = True
        End If

        Dim iDernierMois As Integer = 0
        If bMoisGlissant = True Then
            Dim iAnnee As Integer = cDal.Recuperer_Dernier_Annee_Table(oRapport)
            iDernierMois = cDal.Recuperer_Dernier_Mois_Table(oRapport, iAnnee)
        End If



        Try

            sRequete = "SELECT dbo.Colonne.idParamColonnes, dbo.Colonne.idClient, dbo.Colonne.nomColonne, dbo.Colonne.enteteNiveau1, dbo.Colonne.infobulleNiveau1, "
            sRequete &= " dbo.Colonne.enteteNiveau2, dbo.Colonne.infobulleNiveau2, dbo.Colonne.enteteNiveau3, dbo.Colonne.infobulleNiveau3, dbo.Colonne.enteteNiveau4,"
            sRequete &= " dbo.Colonne.infobulleNiveau4, dbo.Colonne.typeDonnees, dbo.Colonne.typeRegroupement, dbo.Colonne.mefDonnees, dbo.Colonne.estVisible,"
            sRequete &= " dbo.Colonne.estMasquable, dbo.Colonne.estTriAutorise, dbo.Colonne.classeCellule, dbo.Colonne.siNull, dbo.Colonne.siZero, dbo.Colonne.estConvertibleTTC,"
            sRequete &= " dbo.Colonne.estFusionnable, dbo.Colonne.estDigitsMasquables, dbo.Colonne.nombreDigitsMasques,dbo.Colonne.estSeuil, dbo.Colonne.largeurColonne, dbo.Colonne.styleDonnees,"
            sRequete &= " dbo.Colonne.styleSeuil, dbo.Colonne.valeurSeuil, dbo.Colonne.estModifiable, dbo.Colonne.controleModification, dbo.Colonne.tableModification,"
            sRequete &= " dbo.Colonne.champModification, dbo.Colonne.filtreModification,"
            sRequete &= " dbo.Colonne.nomValeurMois, dbo.Colonne.detailListeMois, dbo.Colonne.estExtractExcel, dbo.Colonne.estComposeeBalises"
            sRequete &= " FROM            dbo.Colonne INNER JOIN"
            sRequete &= " dbo.ParamRapportColonne ON dbo.Colonne.idParamColonnes = dbo.ParamRapportColonne.idParamColonnes AND "
            sRequete &= " dbo.Colonne.idClient = dbo.ParamRapportColonne.idClient INNER JOIN"
            sRequete &= " dbo.Rapport ON dbo.ParamRapportColonne.idRapport = dbo.Rapport.idRapport AND dbo.ParamRapportColonne.idClient = dbo.Rapport.idClient AND "
            sRequete &= " dbo.ParamRapportColonne.idUtilisateur = dbo.Rapport.idUtilisateur And dbo.ParamRapportColonne.idModule = dbo.Rapport.idModule"
            sRequete &= " WHERE        (dbo.Rapport.idUtilisateur = " & CStr(oRapport.oUtilisateur.nIdUtilisateur) & ") AND"
            sRequete &= " (dbo.Rapport.idRapport = " & CStr(oRapport.nIdRapport) & ") AND"
            sRequete &= "  (dbo.Rapport.idModule = " & CStr(oRapport.oModule.nIdModule) & ") AND"
            sRequete &= "  (dbo.Rapport.idClient = " & CStr(oRapport.oClient.nIdClient) & ")"
            sRequete &= " ORDER BY dbo.ParamRapportColonne.ordrecolonne"

            readerSQL = sql.chargerDataReader(sRequete)

            Do Until Not readerSQL.Read()
                Dim sEnteteNiveau1 As String = CStr(readerSQL.Item("enteteNiveau1"))
                If CStr(readerSQL.Item("nomColonne")).ToUpper = "LISTEMOIS" Then
                    nNombreAnnees = CStr(readerSQL.Item("detailListeMois")).Split(CChar(";")).Length
                    For nIndiceAnnee = 0 To CStr(readerSQL.Item("detailListeMois")).Split(CChar(";")).Length - 1
                        nDebutMoisAnnee = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(0))
                        nFinMoisAnnee = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(1))
                        nReferenceAnnee = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(2))
                        nColonneReference = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(3))
                        nDebutMoisAnnee = nDebutMoisAnnee + (12 * nReferenceAnnee)
                        nFinMoisAnnee = nFinMoisAnnee + (12 * nReferenceAnnee)


                        For i = nDebutMoisAnnee + iDernierMois To nFinMoisAnnee

                            oColonne = New Colonne(100000 + CInt(readerSQL.Item("idParamColonnes")) + i, oRapport.oClient, "A" & CStr(nReferenceAnnee) & "M" & CStr(Toolbox.retraiterMoisMultiAnnee(i)), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau1")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau1")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau3")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau3")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau4")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau4")), i, oRapport.oClient), CStr(readerSQL.Item("typeDonnees")), CStr(readerSQL.Item("typeRegroupement")), CStr(readerSQL.Item("mefDonnees")),
                                          CBool(readerSQL.Item("estVisible")), CBool(readerSQL.Item("estMasquable")), CBool(readerSQL.Item("estTriAutorise")), CStr(readerSQL.Item("classeCellule")), CStr(readerSQL.Item("siNull")), CStr(readerSQL.Item("siZero")),
                                          CBool(readerSQL.Item("estConvertibleTTC")), CBool(readerSQL.Item("estFusionnable")), CBool(readerSQL.Item("estDigitsMasquables")), CShort(readerSQL.Item("nombreDigitsMasques")), CBool(readerSQL.Item("estSeuil")), CInt(readerSQL.Item("largeurColonne")),
                                          CStr(readerSQL.Item("styleDonnees")), CStr(readerSQL.Item("styleSeuil")), CStr(readerSQL.Item("valeurSeuil")), CBool(readerSQL.Item("estModifiable")), CStr(readerSQL.Item("controleModification")),
                                          CStr(readerSQL.Item("tableModification")), CStr(readerSQL.Item("champModification")), CStr(readerSQL.Item("filtreModification")), CStr(readerSQL.Item("nomValeurMois")), CStr(nReferenceAnnee) + ":" + CStr(nColonneReference), CBool(readerSQL.Item("estExtractExcel")), CBool(readerSQL.Item("estComposeeBalises")))
                            oColonnes.Add(oColonne)
                        Next
                        If nDebutMoisAnnee > 0 Then
                            For i = nDebutMoisAnnee To iDernierMois
                                oColonne = New Colonne(100000 + CInt(readerSQL.Item("idParamColonnes")) + i, oRapport.oClient, "A" & CStr(nReferenceAnnee) & "M" & CStr(Toolbox.retraiterMoisMultiAnnee(i)), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau1")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau1")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau3")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau3")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau4")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau4")), i, oRapport.oClient), CStr(readerSQL.Item("typeDonnees")), CStr(readerSQL.Item("typeRegroupement")), CStr(readerSQL.Item("mefDonnees")),
                                          CBool(readerSQL.Item("estVisible")), CBool(readerSQL.Item("estMasquable")), CBool(readerSQL.Item("estTriAutorise")), CStr(readerSQL.Item("classeCellule")), CStr(readerSQL.Item("siNull")), CStr(readerSQL.Item("siZero")),
                                          CBool(readerSQL.Item("estConvertibleTTC")), CBool(readerSQL.Item("estFusionnable")), CBool(readerSQL.Item("estDigitsMasquables")), CShort(readerSQL.Item("nombreDigitsMasques")), CBool(readerSQL.Item("estSeuil")), CInt(readerSQL.Item("largeurColonne")),
                                          CStr(readerSQL.Item("styleDonnees")), CStr(readerSQL.Item("styleSeuil")), CStr(readerSQL.Item("valeurSeuil")), CBool(readerSQL.Item("estModifiable")), CStr(readerSQL.Item("controleModification")),
                                          CStr(readerSQL.Item("tableModification")), CStr(readerSQL.Item("champModification")), CStr(readerSQL.Item("filtreModification")), CStr(readerSQL.Item("nomValeurMois")), CStr(nReferenceAnnee) + ":" + CStr(nColonneReference), CBool(readerSQL.Item("estExtractExcel")), CBool(readerSQL.Item("estComposeeBalises")))
                                oColonnes.Add(oColonne)
                            Next
                        End If
                    Next
                Else
                    If sEnteteNiveau1 <> "Action" Or oBloquerColoneAction.Value = "" Then
                        oColonne = New Colonne(CInt(readerSQL.Item("idParamColonnes")), oRapport.oClient, CStr(readerSQL.Item("nomColonne")), CStr(readerSQL.Item("enteteNiveau1")), CStr(readerSQL.Item("infobulleNiveau1")),
                                               CStr(readerSQL.Item("enteteNiveau2")), CStr(readerSQL.Item("infobulleNiveau2")), CStr(readerSQL.Item("enteteNiveau3")), CStr(readerSQL.Item("infobulleNiveau3")),
                                               CStr(readerSQL.Item("enteteNiveau4")), CStr(readerSQL.Item("infobulleNiveau4")), CStr(readerSQL.Item("typeDonnees")), CStr(readerSQL.Item("typeRegroupement")), CStr(readerSQL.Item("mefDonnees")),
                                               CBool(readerSQL.Item("estVisible")), CBool(readerSQL.Item("estMasquable")), CBool(readerSQL.Item("estTriAutorise")), CStr(readerSQL.Item("classeCellule")), CStr(readerSQL.Item("siNull")), CStr(readerSQL.Item("siZero")),
                                               CBool(readerSQL.Item("estConvertibleTTC")), CBool(readerSQL.Item("estFusionnable")), CBool(readerSQL.Item("estDigitsMasquables")), CShort(readerSQL.Item("nombreDigitsMasques")), CBool(readerSQL.Item("estSeuil")), CInt(readerSQL.Item("largeurColonne")),
                                               CStr(readerSQL.Item("styleDonnees")), CStr(readerSQL.Item("styleSeuil")), CStr(readerSQL.Item("valeurSeuil")), CBool(readerSQL.Item("estModifiable")), CStr(readerSQL.Item("controleModification")),
                                               CStr(readerSQL.Item("tableModification")), CStr(readerSQL.Item("champModification")), CStr(readerSQL.Item("filtreModification")), CStr(readerSQL.Item("nomValeurMois")), CStr(readerSQL.Item("detailListeMois")), CBool(readerSQL.Item("estExtractExcel")), CBool(readerSQL.Item("estComposeeBalises")))
                        oColonnes.Add(oColonne)
                    End If
                End If
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub ChargerColonne(ByRef oColonne As Colonne, ByVal nReferenceColonne As Integer, ByVal nIdClient As Integer)
        Dim sRequete As String = ""
        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing
        Dim oClient As New Client
        oClient.chargerClientParId(nIdClient)

        Try
            sRequete = "SELECT dbo.colonne.idParamColonnes,dbo.colonne.nomColonne,dbo.colonne.enteteNiveau1,dbo.colonne.infobulleNiveau1,dbo.colonne.enteteNiveau2,dbo.colonne.infobulleNiveau2,"
            sRequete &= " dbo.colonne.enteteNiveau3,dbo.colonne.infobulleNiveau3,dbo.colonne.enteteNiveau4,dbo.colonne.infobulleNiveau4,dbo.colonne.typeDonnees,dbo.colonne.typeRegroupement,dbo.colonne.mefDonnees,dbo.colonne.estVisible,"
            sRequete &= " dbo.colonne.estMasquable, dbo.colonne.estTriAutorise,dbo.colonne.classeCellule,  dbo.colonne.siNull,dbo.colonne.siZero,dbo.colonne.estConvertibleTTC,dbo.colonne.estFusionnable,dbo.colonne.estDigitsMasquables,dbo.colonne.nombreDigitsMasques, dbo.colonne.estSeuil,dbo.colonne.largeurColonne,"
            sRequete &= " dbo.colonne.styleDonnees,dbo.colonne.styleSeuil,dbo.colonne.valeurSeuil,dbo.colonne.estModifiable,dbo.colonne.controleModification,dbo.colonne.tableModification,dbo.Colonne.champModification, dbo.Colonne.filtreModification, dbo.colonne.nomValeurMois, dbo.colonne.detailListeMois, dbo.colonne.estExtractExcel, dbo.colonne.estComposeeBalises"
            sRequete &= " FROM dbo.Colonne "
            sRequete &= " WHERE dbo.colonne.idParamColonnes = " & CStr(nReferenceColonne) & " AND dbo.colonne.idClient=" & nIdClient.ToString
            readerSQL = sql.chargerDataReader(sRequete)

            Do Until Not readerSQL.Read()
                Dim sEnteteNiveau1 As String = CStr(readerSQL.Item("enteteNiveau1"))


                oColonne = New Colonne(CInt(readerSQL.Item("idParamColonnes")), oClient, CStr(readerSQL.Item("nomColonne")), CStr(readerSQL.Item("enteteNiveau1")), CStr(readerSQL.Item("infobulleNiveau1")),
                                       CStr(readerSQL.Item("enteteNiveau2")), CStr(readerSQL.Item("infobulleNiveau2")), CStr(readerSQL.Item("enteteNiveau3")), CStr(readerSQL.Item("infobulleNiveau3")),
                                       CStr(readerSQL.Item("enteteNiveau4")), CStr(readerSQL.Item("infobulleNiveau4")), CStr(readerSQL.Item("typeDonnees")), CStr(readerSQL.Item("typeRegroupement")), CStr(readerSQL.Item("mefDonnees")),
                                       CBool(readerSQL.Item("estVisible")), CBool(readerSQL.Item("estMasquable")), CBool(readerSQL.Item("estTriAutorise")), CStr(readerSQL.Item("classeCellule")), CStr(readerSQL.Item("siNull")), CStr(readerSQL.Item("siZero")),
                                       CBool(readerSQL.Item("estConvertibleTTC")), CBool(readerSQL.Item("estFusionnable")), CBool(readerSQL.Item("estDigitsMasquables")), CShort(readerSQL.Item("nombreDigitsMasques")), CBool(readerSQL.Item("estSeuil")), CInt(readerSQL.Item("largeurColonne")),
                                       CStr(readerSQL.Item("styleDonnees")), CStr(readerSQL.Item("styleSeuil")), CStr(readerSQL.Item("valeurSeuil")), CBool(readerSQL.Item("estModifiable")), CStr(readerSQL.Item("controleModification")),
                                       CStr(readerSQL.Item("tableModification")), CStr(readerSQL.Item("champModification")), CStr(readerSQL.Item("filtreModification")), CStr(readerSQL.Item("nomValeurMois")), CStr(readerSQL.Item("detailListeMois")), CBool(readerSQL.Item("estExtractExcel")), CBool(readerSQL.Item("estComposeeBalises")))

            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub ChargerListeColonnesParRapports(ByRef oColonnes As Colonnes, ByRef oRapports As Rapports, ByVal oCheckboxMoisGlissant As CheckBox, ByVal DivId As String, ByVal oBloquerColoneAction As HiddenField)
        Dim sRequete As String = ""
        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing
        Dim oColonne As Colonne
        Dim sEnteteNiveau1 As String
        ' Try

        Dim nNombreAnnees As Integer
        Dim nDebutMoisAnnee As Integer
        Dim nFinMoisAnnee As Integer
        Dim nReferenceAnnee As Integer
        Dim nColonneReference As Integer

        For Each oRapport In oRapports

            Dim bMoisGlissant As Boolean = False
            If Not oCheckboxMoisGlissant Is Nothing And oRapport.bAfficherCaseMoisGlissant = True Then
                If oCheckboxMoisGlissant.Checked = True Then
                    bMoisGlissant = True
                End If
            Else
                If oRapport.bAfficherEnMoisGlissant = True Then bMoisGlissant = True
            End If

            Dim iDernierMois As Integer = 0
            If bMoisGlissant = True Then
                Dim iAnnee As Integer = cDal.Recuperer_Dernier_Annee_Table(oRapport)
                iDernierMois = cDal.Recuperer_Dernier_Mois_Table(oRapport, iAnnee)
            End If

            sRequete = "SELECT dbo.Colonne.idParamColonnes, dbo.Colonne.nomColonne, dbo.Colonne.enteteNiveau1, dbo.Colonne.infobulleNiveau1, "
            sRequete &= " dbo.Colonne.enteteNiveau2, dbo.Colonne.infobulleNiveau2, dbo.Colonne.enteteNiveau3, dbo.Colonne.infobulleNiveau3, dbo.Colonne.enteteNiveau4,"
            sRequete &= " dbo.Colonne.infobulleNiveau4, dbo.Colonne.typeDonnees, dbo.Colonne.typeRegroupement, dbo.Colonne.mefDonnees, dbo.Colonne.estVisible,"
            sRequete &= " dbo.Colonne.estMasquable, dbo.Colonne.estTriAutorise, dbo.Colonne.classeCellule, dbo.Colonne.siNull, dbo.Colonne.siZero, dbo.Colonne.estConvertibleTTC,"
            sRequete &= " dbo.Colonne.estFusionnable, dbo.Colonne.estDigitsMasquables, dbo.Colonne.nombreDigitsMasques, dbo.Colonne.estSeuil, dbo.Colonne.largeurColonne, dbo.Colonne.styleDonnees,"
            sRequete &= " dbo.Colonne.styleSeuil, dbo.Colonne.valeurSeuil, dbo.Colonne.estModifiable, dbo.Colonne.controleModification, dbo.Colonne.tableModification,"
            sRequete &= " dbo.Colonne.champModification, dbo.Colonne.filtreModification,"
            sRequete &= " dbo.Rapport.idRapport, dbo.Colonne.nomValeurMois, dbo.Colonne.detailListeMois, dbo.Colonne.estExtractExcel, dbo.Colonne.estComposeeBalises,"
            sRequete &= " dbo.Rapport.idClient"
            sRequete &= " FROM            dbo.Colonne INNER JOIN"
            sRequete &= " dbo.ParamRapportColonne ON dbo.Colonne.idParamColonnes = dbo.ParamRapportColonne.idParamColonnes AND "
            sRequete &= " dbo.Colonne.idClient = dbo.ParamRapportColonne.idClient INNER JOIN"
            sRequete &= " dbo.Rapport ON dbo.ParamRapportColonne.idRapport = dbo.Rapport.idRapport AND dbo.ParamRapportColonne.idClient = dbo.Rapport.idClient AND "
            sRequete &= " dbo.ParamRapportColonne.idUtilisateur = dbo.Rapport.idUtilisateur And dbo.ParamRapportColonne.idModule = dbo.Rapport.idModule"
            sRequete &= " WHERE        (dbo.Rapport.idUtilisateur = " & CStr(oRapport.oUtilisateur.nIdUtilisateur) & ") AND"
            sRequete &= "  (dbo.Rapport.idRapport = " & CStr(oRapport.nIdRapport) & ") AND"
            sRequete &= "  (dbo.Rapport.idModule = " & CStr(oRapport.oModule.nIdModule) & ") AND"
            sRequete &= "  (dbo.Rapport.idClient =  " & CStr(oRapport.oClient.nIdClient) & ")"
            sRequete &= " ORDER BY dbo.ParamRapportColonne.ordrecolonne"

            readerSQL = sql.chargerDataReader(sRequete)

            Dim i As Integer
            Do Until Not readerSQL.Read()
                sEnteteNiveau1 = CStr(readerSQL.Item("enteteNiveau1"))
                If CStr(readerSQL.Item("nomColonne")).ToUpper = "LISTEMOIS" Then

                    nNombreAnnees = CStr(readerSQL.Item("detailListeMois")).Split(CChar(";")).Length
                    For nIndiceAnnee = 0 To CStr(readerSQL.Item("detailListeMois")).Split(CChar(";")).Length - 1
                        nDebutMoisAnnee = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(0))
                        nFinMoisAnnee = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(1))
                        nReferenceAnnee = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(2))
                        nColonneReference = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(3))
                        nDebutMoisAnnee = nDebutMoisAnnee + (12 * nReferenceAnnee)
                        nFinMoisAnnee = nFinMoisAnnee + (12 * nReferenceAnnee)

                        For i = nDebutMoisAnnee + iDernierMois To nFinMoisAnnee
                            Dim oRapportsTemp As New Rapports
                            Dim oRapportTemp As New Rapport()
                            oRapportTemp.chargerRappportParModuleUtilisateurIdRapport(oRapport.oModule.nIdModule, oRapport.oUtilisateur.nIdUtilisateur, CInt(readerSQL.Item("idRapport")), oRapport.oClient.nIdClient)
                            oRapportsTemp.Add(oRapportTemp)
                            oColonne = New Colonne(100000 + CInt(readerSQL.Item("idParamColonnes")) + i, oRapport.oClient, "A" & CStr(nReferenceAnnee) & "M" & CStr(Toolbox.retraiterMoisMultiAnnee(i)), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau1")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau1")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau3")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau3")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau4")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau4")), i, oRapport.oClient), CStr(readerSQL.Item("typeDonnees")), CStr(readerSQL.Item("typeRegroupement")), CStr(readerSQL.Item("mefDonnees")),
                                          CBool(readerSQL.Item("estVisible")), CBool(readerSQL.Item("estMasquable")), CBool(readerSQL.Item("estTriAutorise")), CStr(readerSQL.Item("classeCellule")), CStr(readerSQL.Item("siNull")), CStr(readerSQL.Item("siZero")),
                                          CBool(readerSQL.Item("estConvertibleTTC")), CBool(readerSQL.Item("estFusionnable")), CBool(readerSQL.Item("estDigitsMasquables")), CShort(readerSQL.Item("nombreDigitsMasques")), CBool(readerSQL.Item("estSeuil")), CInt(readerSQL.Item("largeurColonne")),
                                          CStr(readerSQL.Item("styleDonnees")), CStr(readerSQL.Item("styleSeuil")), CStr(readerSQL.Item("valeurSeuil")), CBool(readerSQL.Item("estModifiable")), CStr(readerSQL.Item("controleModification")),
                                          CStr(readerSQL.Item("tableModification")), CStr(readerSQL.Item("champModification")), CStr(readerSQL.Item("filtreModification")), oRapportsTemp, CStr(readerSQL.Item("nomValeurMois")), CStr(nReferenceAnnee) + ":" + CStr(nColonneReference), CBool(readerSQL.Item("estExtractExcel")), CBool(readerSQL.Item("estComposeeBalises")))
                            Dim oColTmp As Colonne = oColonnes.Find(Function(p) p.sNomColonne = CStr(i))
                            If oColTmp Is Nothing Then
                                oColonnes.Add(oColonne)
                            Else
                                oColTmp.oRapports.Add(oRapportTemp)
                            End If
                        Next
                        If nDebutMoisAnnee > 0 Then
                            For i = nDebutMoisAnnee To nFinMoisAnnee
                                Dim oRapportsTemp As New Rapports
                                Dim oRapportTemp As New Rapport()
                                oRapportTemp.chargerRappportParModuleUtilisateurIdRapport(oRapport.oModule.nIdModule, oRapport.oUtilisateur.nIdUtilisateur, CInt(readerSQL.Item("idRapport")), oRapport.oClient.nIdClient)
                                oRapportsTemp.Add(oRapportTemp)
                                oColonne = New Colonne(100000 + CInt(readerSQL.Item("idParamColonnes")) + i, oRapport.oClient, "A" & CStr(nReferenceAnnee) & "M" & CStr(Toolbox.retraiterMoisMultiAnnee(i)), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau1")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau1")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau3")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau3")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau4")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau4")), i, oRapport.oClient), CStr(readerSQL.Item("typeDonnees")), CStr(readerSQL.Item("typeRegroupement")), CStr(readerSQL.Item("mefDonnees")),
                                          CBool(readerSQL.Item("estVisible")), CBool(readerSQL.Item("estMasquable")), CBool(readerSQL.Item("estTriAutorise")), CStr(readerSQL.Item("classeCellule")), CStr(readerSQL.Item("siNull")), CStr(readerSQL.Item("siZero")),
                                          CBool(readerSQL.Item("estConvertibleTTC")), CBool(readerSQL.Item("estFusionnable")), CBool(readerSQL.Item("estDigitsMasquables")), CShort(readerSQL.Item("nombreDigitsMasques")), CBool(readerSQL.Item("estSeuil")), CInt(readerSQL.Item("largeurColonne")),
                                          CStr(readerSQL.Item("styleDonnees")), CStr(readerSQL.Item("styleSeuil")), CStr(readerSQL.Item("valeurSeuil")), CBool(readerSQL.Item("estModifiable")), CStr(readerSQL.Item("controleModification")),
                                          CStr(readerSQL.Item("tableModification")), CStr(readerSQL.Item("champModification")), CStr(readerSQL.Item("filtreModification")), oRapportsTemp, CStr(readerSQL.Item("nomValeurMois")), CStr(nReferenceAnnee) + ":" + CStr(nColonneReference), CBool(readerSQL.Item("estExtractExcel")), CBool(readerSQL.Item("estComposeeBalises")))
                                Dim oColTmp As Colonne = oColonnes.Find(Function(p) p.sNomColonne = CStr(i))
                                If oColTmp Is Nothing Then
                                    oColonnes.Add(oColonne)
                                Else
                                    oColTmp.oRapports.Add(oRapportTemp)
                                End If
                            Next
                        End If
                    Next
                Else
                    Dim oRapportsTemp As New Rapports
                    Dim oRapportTemp As New Rapport()

                    oRapportTemp.chargerRappportParModuleUtilisateurIdRapport(oRapport.oModule.nIdModule, oRapport.oUtilisateur.nIdUtilisateur, CInt(readerSQL.Item("idRapport")), oRapport.oClient.nIdClient)
                    oRapportsTemp.Add(oRapportTemp)
                    oColonne = New Colonne(CInt(readerSQL.Item("idParamColonnes")), oRapport.oClient, CStr(readerSQL.Item("nomColonne")), CStr(readerSQL.Item("enteteNiveau1")), CStr(readerSQL.Item("infobulleNiveau1")),
                                           CStr(readerSQL.Item("enteteNiveau2")), CStr(readerSQL.Item("infobulleNiveau2")), CStr(readerSQL.Item("enteteNiveau3")), CStr(readerSQL.Item("infobulleNiveau3")),
                                           CStr(readerSQL.Item("enteteNiveau4")), CStr(readerSQL.Item("infobulleNiveau4")), CStr(readerSQL.Item("typeDonnees")), CStr(readerSQL.Item("typeRegroupement")), CStr(readerSQL.Item("mefDonnees")),
                                           CBool(readerSQL.Item("estVisible")), CBool(readerSQL.Item("estMasquable")), CBool(readerSQL.Item("estTriAutorise")), CStr(readerSQL.Item("classeCellule")), CStr(readerSQL.Item("siNull")), CStr(readerSQL.Item("siZero")),
                                           CBool(readerSQL.Item("estConvertibleTTC")), CBool(readerSQL.Item("estFusionnable")), CBool(readerSQL.Item("estDigitsMasquables")), CShort(readerSQL.Item("nombreDigitsMasques")), CBool(readerSQL.Item("estSeuil")), CInt(readerSQL.Item("largeurColonne")),
                                           CStr(readerSQL.Item("styleDonnees")), CStr(readerSQL.Item("styleSeuil")), CStr(readerSQL.Item("valeurSeuil")), CBool(readerSQL.Item("estModifiable")), CStr(readerSQL.Item("controleModification")),
                                           CStr(readerSQL.Item("tableModification")), CStr(readerSQL.Item("champModification")), CStr(readerSQL.Item("filtreModification")), oRapportsTemp, CStr(readerSQL.Item("nomValeurMois")), CStr(nReferenceAnnee) + ":" + CStr(nColonneReference), CBool(readerSQL.Item("estExtractExcel")), CBool(readerSQL.Item("estComposeeBalises")))
                    Dim oColTmp As Colonne = oColonnes.Find(Function(p) p.sNomColonne = CStr(readerSQL.Item("nomColonne")))
                    If oColTmp Is Nothing Then
                        If sEnteteNiveau1 <> "Action" Or oBloquerColoneAction.Value = "" Then
                            oColonnes.Add(oColonne)
                        End If
                    Else
                        oColTmp.oRapports.Add(oRapportTemp)
                    End If
                End If
            Loop

            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If

        Next
        'Catch ex As Exception
        '    Throw ex
        'Finally
        If Not readerSQL Is Nothing Then
            sql.libererDataReader(readerSQL)
        End If
        sql.fermerConnexion()
        sql = Nothing
        'End Try
    End Sub

    Public Shared Function RecupererFormatListeMois(ByRef oSerie As Serie) As Colonne
        Dim sRequete As String = ""
        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing
        Dim oColonne As Colonne = Nothing

        Try


            sRequete = "SELECT        dbo.Colonne.idParamColonnes, dbo.Colonne.nomColonne, dbo.Colonne.enteteNiveau1, dbo.Colonne.infobulleNiveau1, dbo.Colonne.enteteNiveau2, "
            sRequete += " dbo.Colonne.infobulleNiveau2, dbo.Colonne.enteteNiveau3, dbo.Colonne.infobulleNiveau3, dbo.Colonne.enteteNiveau4, dbo.Colonne.infobulleNiveau4,"
            sRequete += " dbo.Colonne.typeDonnees, dbo.Colonne.typeRegroupement, dbo.Colonne.mefDonnees, dbo.Colonne.estVisible, dbo.Colonne.estMasquable,"
            sRequete += " dbo.Colonne.estTriAutorise, dbo.Colonne.classeCellule, dbo.Colonne.siNull, dbo.Colonne.siZero, dbo.Colonne.estConvertibleTTC, dbo.Colonne.estFusionnable,"
            sRequete += " dbo.Colonne.estDigitsMasquables, dbo.Colonne.nombreDigitsMasques, dbo.Colonne.estSeuil, dbo.Colonne.largeurColonne, dbo.Colonne.styleDonnees, dbo.Colonne.styleSeuil,"
            sRequete += " dbo.Colonne.valeurSeuil, dbo.Colonne.estModifiable, dbo.Colonne.controleModification, dbo.Colonne.tableModification,dbo.Colonne.champModification, dbo.Colonne.filtreModification, dbo.Colonne.nomValeurMois,"
            sRequete += " dbo.Colonne.detailListeMois, dbo.Colonne.estExtractExcel, dbo.Colonne.estComposeeBalises, dbo.ParamSerieColonne.idClient"
            sRequete += " FROM            dbo.Colonne INNER JOIN"
            sRequete += " dbo.ParamSerieColonne ON dbo.Colonne.idParamColonnes = dbo.ParamSerieColonne.idParamColonne AND "
            sRequete += " dbo.Colonne.idClient = dbo.ParamSerieColonne.idClient"
            sRequete += " WHERE        (dbo.ParamSerieColonne.idSerie = " & oSerie.nIdSerie & ") AND (dbo.Colonne.nomColonne LIKE '%ListeMois%') AND (dbo.ParamSerieColonne.idClient = " & CStr(oSerie.oClient.nIdClient) & ")"




            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                oColonne = New Colonne(CInt(readerSQL.Item("idParamColonnes")), oSerie.oClient, CStr(readerSQL.Item("nomColonne")), CStr(readerSQL.Item("enteteNiveau1")), CStr(readerSQL.Item("infobulleNiveau1")),
                    CStr(readerSQL.Item("enteteNiveau2")), CStr(readerSQL.Item("infobulleNiveau2")), CStr(readerSQL.Item("enteteNiveau3")), CStr(readerSQL.Item("infobulleNiveau3")),
                    CStr(readerSQL.Item("enteteNiveau4")), CStr(readerSQL.Item("infobulleNiveau4")), CStr(readerSQL.Item("typeDonnees")), CStr(readerSQL.Item("typeRegroupement")), CStr(readerSQL.Item("mefDonnees")),
                    CBool(readerSQL.Item("estVisible")), CBool(readerSQL.Item("estMasquable")), CBool(readerSQL.Item("estTriAutorise")), CStr(readerSQL.Item("classeCellule")), CStr(readerSQL.Item("siNull")), CStr(readerSQL.Item("siZero")),
                    CBool(readerSQL.Item("estConvertibleTTC")), CBool(readerSQL.Item("estFusionnable")), CBool(readerSQL.Item("estDigitsMasquables")), CShort(readerSQL.Item("nombreDigitsMasques")), CBool(readerSQL.Item("estSeuil")), CInt(readerSQL.Item("largeurColonne")),
                    CStr(readerSQL.Item("styleDonnees")), CStr(readerSQL.Item("styleSeuil")), CStr(readerSQL.Item("valeurSeuil")), CBool(readerSQL.Item("estModifiable")), CStr(readerSQL.Item("controleModification")),
                    CStr(readerSQL.Item("tableModification")), CStr(readerSQL.Item("champModification")), CStr(readerSQL.Item("filtreModification")), CStr(readerSQL.Item("nomValeurMois")), CStr(readerSQL.Item("detailListeMois")), CBool(readerSQL.Item("estExtractExcel")), CBool(readerSQL.Item("estComposeeBalises")))
                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

        Return oColonne

    End Function

    Public Shared Sub ChargerListeColonnesGraphique(ByRef oColonnes As Colonnes, ByRef oRapport As Rapport, ByVal oCheckboxMoisGlissant As CheckBox, ByVal DivId As String, ByRef oSerie As Serie)
        Dim sRequete As String = ""
        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing
        Dim oColonne As Colonne

        Dim nNombreAnnees As Integer
        Dim nDebutMoisAnnee As Integer
        Dim nFinMoisAnnee As Integer
        Dim nReferenceAnnee As Integer
        Dim nColonneReference As Integer

        Dim bMoisGlissant As Boolean = False
        If Not oCheckboxMoisGlissant Is Nothing And oRapport.bAfficherCaseMoisGlissant = True Then
            If oCheckboxMoisGlissant.Checked = True Then
                bMoisGlissant = True
            End If
        Else
            If oRapport.bAfficherEnMoisGlissant = True Then bMoisGlissant = True
        End If

        Dim iDernierMois As Integer = 0
        If bMoisGlissant = True Then
            Dim iAnnee As Integer = cDal.Recuperer_Dernier_Annee_Table(oRapport)
            iDernierMois = cDal.Recuperer_Dernier_Mois_Table(oRapport, iAnnee)
        End If

        Try

            sRequete = "SELECT        dbo.Colonne.idParamColonnes, dbo.Colonne.nomColonne, dbo.Colonne.enteteNiveau1, dbo.Colonne.infobulleNiveau1, dbo.Colonne.enteteNiveau2, "
            sRequete += " dbo.Colonne.infobulleNiveau2, dbo.Colonne.enteteNiveau3, dbo.Colonne.infobulleNiveau3, dbo.Colonne.enteteNiveau4, dbo.Colonne.infobulleNiveau4,"
            sRequete += " dbo.Colonne.typeDonnees, dbo.Colonne.typeRegroupement, dbo.Colonne.mefDonnees, dbo.Colonne.estVisible, dbo.Colonne.estMasquable,"
            sRequete += " dbo.Colonne.estTriAutorise, dbo.Colonne.classeCellule, dbo.Colonne.siNull, dbo.Colonne.siZero, dbo.Colonne.estConvertibleTTC, dbo.Colonne.estFusionnable,"
            sRequete += " dbo.Colonne.estDigitsMasquables, dbo.Colonne.nombreDigitsMasques, dbo.Colonne.estSeuil, dbo.Colonne.largeurColonne, dbo.Colonne.styleDonnees, dbo.Colonne.styleSeuil,"
            sRequete += " dbo.Colonne.valeurSeuil, dbo.Colonne.estModifiable, dbo.Colonne.controleModification, dbo.Colonne.tableModification,dbo.Colonne.champModification, dbo.Colonne.filtreModification, dbo.Colonne.nomValeurMois,"
            sRequete += " dbo.Colonne.detailListeMois, dbo.Colonne.estExtractExcel, dbo.Colonne.estComposeeBalises, dbo.ParamSerieColonne.idClient"
            sRequete += " FROM            dbo.Colonne INNER JOIN"
            sRequete += " dbo.ParamSerieColonne ON dbo.Colonne.idParamColonnes = dbo.ParamSerieColonne.idParamColonne AND "
            sRequete += " dbo.Colonne.idClient = dbo.ParamSerieColonne.idClient"
            sRequete += " WHERE        (dbo.ParamSerieColonne.idSerie = " & oSerie.nIdSerie & ") AND (dbo.ParamSerieColonne.idClient = " & oSerie.oClient.nIdClient.ToString & ")"

            readerSQL = sql.chargerDataReader(sRequete)

            Do Until Not readerSQL.Read()
                If CStr(readerSQL.Item("nomColonne")).ToUpper = "LISTEMOIS" Then

                    nNombreAnnees = CStr(readerSQL.Item("detailListeMois")).Split(CChar(";")).Length
                    For nIndiceAnnee = 0 To CStr(readerSQL.Item("detailListeMois")).Split(CChar(";")).Length - 1
                        nDebutMoisAnnee = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(0))
                        nFinMoisAnnee = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(1))
                        nReferenceAnnee = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(2))
                        nColonneReference = CInt(CStr(readerSQL.Item("detailListeMois")).Split(CChar(";"))(nIndiceAnnee).Split(CChar(":"))(3))
                        nDebutMoisAnnee = nDebutMoisAnnee + (12 * nReferenceAnnee)
                        nFinMoisAnnee = nFinMoisAnnee + (12 * nReferenceAnnee)

                        For i = nDebutMoisAnnee + iDernierMois To nFinMoisAnnee
                            oColonne = New Colonne(100000 + CInt(readerSQL.Item("idParamColonnes")) + i, oSerie.oClient, "A" & CStr(nReferenceAnnee) & "M" & CStr(Toolbox.retraiterMoisMultiAnnee(i)), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau1")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau1")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau3")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau3")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau4")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau4")), i, oRapport.oClient), CStr(readerSQL.Item("typeDonnees")), CStr(readerSQL.Item("typeRegroupement")), CStr(readerSQL.Item("mefDonnees")),
                                          CBool(readerSQL.Item("estVisible")), CBool(readerSQL.Item("estMasquable")), CBool(readerSQL.Item("estTriAutorise")), CStr(readerSQL.Item("classeCellule")), CStr(readerSQL.Item("siNull")), CStr(readerSQL.Item("siZero")),
                                          CBool(readerSQL.Item("estConvertibleTTC")), CBool(readerSQL.Item("estFusionnable")), CBool(readerSQL.Item("estDigitsMasquables")), CShort(readerSQL.Item("nombreDigitsMasques")), CBool(readerSQL.Item("estSeuil")), CInt(readerSQL.Item("largeurColonne")),
                                          CStr(readerSQL.Item("styleDonnees")), CStr(readerSQL.Item("styleSeuil")), CStr(readerSQL.Item("valeurSeuil")), CBool(readerSQL.Item("estModifiable")), CStr(readerSQL.Item("controleModification")),
                                          CStr(readerSQL.Item("tableModification")), CStr(readerSQL.Item("champModification")), CStr(readerSQL.Item("filtreModification")), CStr(readerSQL.Item("nomValeurMois")), CStr(nReferenceAnnee) + ":" + CStr(nColonneReference), CBool(readerSQL.Item("estExtractExcel")), CBool(readerSQL.Item("estComposeeBalises")))
                            oColonnes.Add(oColonne)
                        Next
                        If nDebutMoisAnnee > 0 Then
                            For i = nDebutMoisAnnee To iDernierMois
                                oColonne = New Colonne(100000 + CInt(readerSQL.Item("idParamColonnes")) + i, oSerie.oClient, "A" & CStr(nReferenceAnnee) & "M" & CStr(Toolbox.retraiterMoisMultiAnnee(i)), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau1")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau1")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau2")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau3")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau3")), i, oRapport.oClient),
                                          Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("enteteNiveau4")), i, oRapport.oClient), Toolbox.VerifierEnteteLibelleMois(CStr(readerSQL.Item("infobulleNiveau4")), i, oRapport.oClient), CStr(readerSQL.Item("typeDonnees")), CStr(readerSQL.Item("typeRegroupement")), CStr(readerSQL.Item("mefDonnees")),
                                          CBool(readerSQL.Item("estVisible")), CBool(readerSQL.Item("estMasquable")), CBool(readerSQL.Item("estTriAutorise")), CStr(readerSQL.Item("classeCellule")), CStr(readerSQL.Item("siNull")), CStr(readerSQL.Item("siZero")),
                                          CBool(readerSQL.Item("estConvertibleTTC")), CBool(readerSQL.Item("estFusionnable")), CBool(readerSQL.Item("estDigitsMasquables")), CShort(readerSQL.Item("nombreDigitsMasques")), CBool(readerSQL.Item("estSeuil")), CInt(readerSQL.Item("largeurColonne")),
                                          CStr(readerSQL.Item("styleDonnees")), CStr(readerSQL.Item("styleSeuil")), CStr(readerSQL.Item("valeurSeuil")), CBool(readerSQL.Item("estModifiable")), CStr(readerSQL.Item("controleModification")),
                                          CStr(readerSQL.Item("tableModification")), CStr(readerSQL.Item("champModification")), CStr(readerSQL.Item("filtreModification")), CStr(readerSQL.Item("nomValeurMois")), CStr(nReferenceAnnee) + ":" + CStr(nColonneReference), CBool(readerSQL.Item("estExtractExcel")), CBool(readerSQL.Item("estComposeeBalises")))
                                oColonnes.Add(oColonne)
                            Next
                        End If
                    Next
                Else
                    oColonne = New Colonne(CInt(readerSQL.Item("idParamColonnes")), oSerie.oClient, CStr(readerSQL.Item("nomColonne")), CStr(readerSQL.Item("enteteNiveau1")), CStr(readerSQL.Item("infobulleNiveau1")),
                                           CStr(readerSQL.Item("enteteNiveau2")), CStr(readerSQL.Item("infobulleNiveau2")), CStr(readerSQL.Item("enteteNiveau3")), CStr(readerSQL.Item("infobulleNiveau3")),
                                           CStr(readerSQL.Item("enteteNiveau4")), CStr(readerSQL.Item("infobulleNiveau4")), CStr(readerSQL.Item("typeDonnees")), CStr(readerSQL.Item("typeRegroupement")), CStr(readerSQL.Item("mefDonnees")),
                                           CBool(readerSQL.Item("estVisible")), CBool(readerSQL.Item("estMasquable")), CBool(readerSQL.Item("estTriAutorise")), CStr(readerSQL.Item("classeCellule")), CStr(readerSQL.Item("siNull")), CStr(readerSQL.Item("siZero")),
                                           CBool(readerSQL.Item("estConvertibleTTC")), CBool(readerSQL.Item("estFusionnable")), CBool(readerSQL.Item("estDigitsMasquables")), CShort(readerSQL.Item("nombreDigitsMasques")), CBool(readerSQL.Item("estSeuil")), CInt(readerSQL.Item("largeurColonne")),
                                           CStr(readerSQL.Item("styleDonnees")), CStr(readerSQL.Item("styleSeuil")), CStr(readerSQL.Item("valeurSeuil")), CBool(readerSQL.Item("estModifiable")), CStr(readerSQL.Item("controleModification")),
                                           CStr(readerSQL.Item("tableModification")), CStr(readerSQL.Item("champModification")), CStr(readerSQL.Item("filtreModification")), CStr(readerSQL.Item("nomValeurMois")), CStr(nReferenceAnnee) + ":" + CStr(nColonneReference), CBool(readerSQL.Item("estExtractExcel")), CBool(readerSQL.Item("estComposeeBalises")))
                    oColonnes.Add(oColonne)
                End If
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub affichageParamRapportColonne(ByRef oParamRapportColonne As ParamRapportColonne)

        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing
        Dim sRequete As String = "SELECT afficher FROM ParamRapportColonne WHERE idRapport=" & CStr(oParamRapportColonne.oRapport.nIdRapport) & " AND idClient=" & CStr(oParamRapportColonne.oRapport.oClient.nIdClient) &
            " AND idUtilisateur=" & CStr(oParamRapportColonne.oRapport.oUtilisateur.nIdUtilisateur) & " AND idModule=" & CStr(oParamRapportColonne.oRapport.oModule.nIdModule) & " AND idParamColonnes=" & CStr(oParamRapportColonne.oColonne.nIdColonne)

        Try
            oParamRapportColonne.bAfficher = True
            readerSQL = sql.chargerDataReader(sRequete)

            Do Until Not readerSQL.Read()
                ChargerPropriete(oParamRapportColonne.bAfficher, readerSQL.Item("afficher"))
                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Sub

    Public Shared Function CompterEnregistrements(ByVal oRapport As Rapport, ByVal sRequete As String) As Boolean
        Dim sql As New cSQL(oRapport.oClient.sBaseClient)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            Return readerSQL.HasRows

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Function

    Public Shared Function Creer_Requete_Tableau(ByVal oRapport As Rapport, ByVal oColonnes As Colonnes, ByRef ListeControles As List(Of Object), ByVal Div As HtmlGenericControl, ByVal oCheckboxTTC As CheckBox, ByVal oCheckboxMoisGlissant As CheckBox, ByVal sValeurOutilTop As String, ByVal sListeTriTableau As String, ByVal sTriActif As String, ByVal oBloquerColoneAction As HiddenField, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres) As String()
        Dim sRequeteSelect As String = ""
        Dim sRequeteSelectVide As String = ""
        Dim sRequeteFrom As String = ""
        Dim sRequeteWhere As String = ""
        Dim sRequeteGroupBy As String = ""
        Dim sRequeteHavingNormal As String = ""
        Dim sRequeteOrderBy As String = ""
        Dim sRequeteFinale As String = ""
        Dim sRequeteOperation As String = ""
        Dim bMoisContientBalises As Boolean = False

        Dim oColonneReferenceAnnee As New Colonne

        Dim nReferenceAnnee As Integer
        Dim nReferenceColonne As Integer

        Dim sTableTop() As String

        Dim sTous As String = "Tous"
        If oRapport.oClient.sLangueClient = "US" Then sTous = "All"

        Dim sql As New cSQL(oRapport.oClient.sBaseClient)
        Dim readerSQL As SqlDataReader = Nothing
        Dim oColonne As Colonne

        Dim Resultat(2) As String


        Dim oRedirections As New Redirections
        oRedirections.chargerRedirectionsParRapport(oRapport, oCheckboxMoisGlissant, Div.ID, oBloquerColoneAction)

        Try

            '---TTC
            Dim bAfficherTTC As Boolean = False
            Dim stauxTva As String = ""

            If Not oCheckboxTTC Is Nothing And oRapport.bAfficherCaseTTC = True Then
                bAfficherTTC = oCheckboxTTC.Checked
            End If


            Dim PresenceMois As Boolean = False
            Dim bMoisGlissant As Boolean = False

            '---Recherche
            Dim sTexteRecherche As String = ""
            Dim inputCol2Ligne2 As TextBox = Nothing

            Dim sListeIdRapports As String = Toolbox.RecupererListeIdRapports(oRapport)

            If Not CType(Div.FindControl("recherche" & "_" & Div.ID & "_" & sListeIdRapports), TextBox) Is Nothing And oRapport.bAfficherRecherche = True Then
                inputCol2Ligne2 = CType(Div.FindControl("recherche" & "_" & Div.ID & "_" & sListeIdRapports), TextBox)
                If inputCol2Ligne2.Text <> "Rechercher dans le tableau" Then
                    sTexteRecherche = inputCol2Ligne2.Text
                End If
            End If

            sRequeteSelect &= "SELECT "
            If sValeurOutilTop.ToUpper <> sTous.ToUpper And sValeurOutilTop <> "" And oRapport.sOutilTop <> "" Then
                sRequeteSelect &= " TOP " & Toolbox.FormaterFiltreTop(sValeurOutilTop, "Enlever", oRapport.oClient)
            End If

            sRequeteSelectVide &= "SELECT "
            sRequeteGroupBy &= "GROUP BY "
            sRequeteOperation &= sRequeteSelect
            sRequeteHavingNormal = "HAVING ("
            For Each oColonne In oColonnes



                If oColonne.nIdColonne > 100000 Then
                    PresenceMois = True
                End If
                If Not oCheckboxMoisGlissant Is Nothing AndAlso oCheckboxMoisGlissant.Checked = True AndAlso oRapport.bAfficherCaseMoisGlissant = True Then
                    bMoisGlissant = True
                End If
                If bAfficherTTC = True And oColonne.bEstConvertibleTTC = True And oRapport.bAfficherCaseTTC = True Then
                    stauxTva = "tauxTVA"
                Else
                    stauxTva = "1"
                End If

                If oColonne.nIdColonne > 100000 Then

                    nReferenceAnnee = CInt(oColonne.sDetailListeMois.Split(CChar(":"))(0))
                    nReferenceColonne = CInt(oColonne.sDetailListeMois.Split(CChar(":"))(1))

                    '---On traite les mois
                    Select Case oColonne.sTypeDonnees
                        Case "nombre"
                            Dim sNomColonneValeurMois As String = ""
                            If oColonne.sNomValeurMois = "" Then
                                sNomColonneValeurMois = "ValeurMois"
                            Else
                                sNomColonneValeurMois = oColonne.sNomValeurMois
                            End If

                            If nReferenceColonne = 0 Then
                                sRequeteSelect &= " " & oColonne.sTypeRegroupement & "(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " THEN [" & sNomColonneValeurMois & "] * " & stauxTva & " END)  AS [" & "A" & CStr(nReferenceAnnee) & "M" & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & "],"
                                sRequeteHavingNormal &= " " & oColonne.sTypeRegroupement & "(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " THEN [" & sNomColonneValeurMois & "] * " & stauxTva & " END)  LIKE'%" & sTexteRecherche & "%' OR"
                            Else
                                cDal.ChargerColonne(oColonneReferenceAnnee, nReferenceColonne, oRapport.oClient.nIdClient)
                                sRequeteSelect &= " " & oColonne.sTypeRegroupement & "(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " AND [" & oRapport.sTableRapport & "].[Annee] = [" & oColonneReferenceAnnee.sNomColonne & "] " & Toolbox.AjouterSigneOperation(nReferenceAnnee) & " THEN [" & sNomColonneValeurMois & "] * " & stauxTva & " END)  AS [" & "A" & CStr(nReferenceAnnee) & "M" & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & "],"
                                sRequeteHavingNormal &= " " & oColonne.sTypeRegroupement & "(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " AND [" & oRapport.sTableRapport & "].[Annee] = [" & oColonneReferenceAnnee.sNomColonne & "] " & Toolbox.AjouterSigneOperation(nReferenceAnnee) & " THEN [" & sNomColonneValeurMois & "] * " & stauxTva & " END)  LIKE'%" & sTexteRecherche & "%' OR"
                            End If
                            sRequeteOperation &= " " & oColonne.sTypeRegroupement & "([" & "A" & CStr(nReferenceAnnee) & "M" & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & "]) AS [" & "A" & CStr(nReferenceAnnee) & "M" & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & "],"
                            sRequeteSelectVide &= " " & " '' AS [" & "A" & CStr(nReferenceAnnee) & "M" & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & "],"
                        Case "nombrebalise"
                            bMoisContientBalises = True
                            Dim sNomColonneValeurMois As String = ""
                            If oColonne.sNomValeurMois = "" Then
                                sNomColonneValeurMois = "ValeurMois"
                            Else
                                sNomColonneValeurMois = oColonne.sNomValeurMois
                            End If

                            If nReferenceColonne = 0 Then
                                sRequeteSelect &= " " & "MAX(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " THEN Administration.dbo.BalisesGauche([" & sNomColonneValeurMois & "]) END)  "
                                sRequeteSelect &= " + convert(varchar," & oColonne.sTypeRegroupement & "(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " THEN Administration.dbo.BalisesMilieu([" & sNomColonneValeurMois & "]) END) * MAX(" + stauxTva + "))  "
                                sRequeteSelect &= " +" & "MAX(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " THEN Administration.dbo.BalisesDroite([" & sNomColonneValeurMois & "]) END)  AS [" & "A" & CStr(nReferenceAnnee) & "M" & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & "],"

                                sRequeteHavingNormal &= " " & "MAX(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " THEN Administration.dbo.BalisesGauche([" & sNomColonneValeurMois & "]) END)  "
                                sRequeteHavingNormal &= " + convert(varchar," & oColonne.sTypeRegroupement & "(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " THEN Administration.dbo.BalisesMilieu([" & sNomColonneValeurMois & "]) END) * MAX(" + stauxTva + "))  "
                                sRequeteHavingNormal &= " +" & "MAX(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " THEN Administration.dbo.BalisesDroite([" & sNomColonneValeurMois & "]) END)   LIKE'%" & sTexteRecherche & "%' OR"
                            Else
                                sRequeteSelect &= " " & "MAX(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " AND [" & oRapport.sTableRapport & "].[Annee] = [" & oColonneReferenceAnnee.sNomColonne & "] " & Toolbox.AjouterSigneOperation(nReferenceAnnee) & "  THEN Administration.dbo.BalisesGauche([" & sNomColonneValeurMois & "]) END)  "
                                sRequeteSelect &= " + convert(varchar," & oColonne.sTypeRegroupement & "(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " AND [" & oRapport.sTableRapport & "].[Annee] = [" & oColonneReferenceAnnee.sNomColonne & "] " & Toolbox.AjouterSigneOperation(nReferenceAnnee) & " THEN Administration.dbo.BalisesMilieu([" & sNomColonneValeurMois & "]) END) * MAX(" + stauxTva + "))  "
                                sRequeteSelect &= " +" & "MAX(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " AND [" & oRapport.sTableRapport & "].[Annee] = [" & oColonneReferenceAnnee.sNomColonne & "] " & Toolbox.AjouterSigneOperation(nReferenceAnnee) & " THEN Administration.dbo.BalisesDroite([" & sNomColonneValeurMois & "]) END)  AS [" & "A" & CStr(nReferenceAnnee) & "M" & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & "],"

                                sRequeteHavingNormal &= " " & "MAX(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " AND [" & oRapport.sTableRapport & "].[Annee] = [" & oColonneReferenceAnnee.sNomColonne & "] " & Toolbox.AjouterSigneOperation(nReferenceAnnee) & " THEN Administration.dbo.BalisesGauche([" & sNomColonneValeurMois & "]) END)  "
                                sRequeteHavingNormal &= " + convert(varchar," & oColonne.sTypeRegroupement & "(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " AND [" & oRapport.sTableRapport & "].[Annee] = [" & oColonneReferenceAnnee.sNomColonne & "] " & Toolbox.AjouterSigneOperation(nReferenceAnnee) & " THEN Administration.dbo.BalisesMilieu([" & sNomColonneValeurMois & "]) END) * MAX(" + stauxTva + "))  "
                                sRequeteHavingNormal &= " +" & "MAX(CASE WHEN [ListeMois] = " & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & " AND [" & oRapport.sTableRapport & "].[Annee] = [" & oColonneReferenceAnnee.sNomColonne & "] " & Toolbox.AjouterSigneOperation(nReferenceAnnee) & " THEN Administration.dbo.BalisesDroite([" & sNomColonneValeurMois & "]) END)   LIKE'%" & sTexteRecherche & "%' OR"
                            End If


                            sRequeteSelectVide &= " " & " '' AS [" & "A" & CStr(nReferenceAnnee) & "M" & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & "],"
                            sRequeteOperation &= " " & oColonne.sTypeRegroupement & "(Administration.dbo.BalisesMilieu([" & "A" & CStr(nReferenceAnnee) & "M" & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & "])) AS [" & "A" & CStr(nReferenceAnnee) & "M" & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient) & "],"


                    End Select
                ElseIf oColonne.sNomColonne.Contains("|") Then
                    If bMoisContientBalises = False Then
                        Select Case oColonne.sTypeDonnees
                            Case "nombre"
                                sRequeteSelect &= " " & oColonne.sTypeRegroupement & "([" & oColonne.sNomColonne.Split(CChar("|"))(1) & "] * " & stauxTva & " ) AS [" & oColonne.sNomColonne.Split(CChar("|"))(0) & "],"
                                sRequeteSelectVide &= " " & " '' AS [" & oColonne.sNomColonne.Split(CChar("|"))(0) & "],"
                                sRequeteOperation &= "OPERATION([" & oColonne.sNomColonne.Split(CChar("|"))(0) & "]) AS [" & oColonne.sNomColonne.Split(CChar("|"))(0) & "],"
                                sRequeteHavingNormal &= " " & oColonne.sTypeRegroupement & "([" & oColonne.sNomColonne.Split(CChar("|"))(1) & "] * " & stauxTva & " )  LIKE'%" & sTexteRecherche & "%' OR"
                        End Select
                    Else
                        Select Case oColonne.sTypeDonnees
                            Case "nombre"
                                sRequeteSelect &= " " & oColonne.sTypeRegroupement & "(Administration.dbo.BalisesMilieu([" & oColonne.sNomColonne.Split(CChar("|"))(1) & "]) * " & stauxTva & " ) AS [" & oColonne.sNomColonne.Split(CChar("|"))(0) & "],"
                                sRequeteSelectVide &= " " & " '' AS [" & oColonne.sNomColonne.Split(CChar("|"))(0) & "],"
                                sRequeteOperation &= "OPERATION(Administration.dbo.BalisesMilieu([" & oColonne.sNomColonne.Split(CChar("|"))(0) & "])) AS [" & oColonne.sNomColonne.Split(CChar("|"))(0) & "],"
                                sRequeteHavingNormal &= " " & oColonne.sTypeRegroupement & "(Administration.dbo.BalisesMilieu([" & oColonne.sNomColonne.Split(CChar("|"))(1) & "]) * " & stauxTva & " )  LIKE'%" & sTexteRecherche & "%' OR"
                        End Select
                    End If
                Else
                    Select Case oColonne.sTypeDonnees
                        Case "texte", "date", "booleen", "action"
                            If oColonne.sNomColonne.ToUpper = "AUCUN" Then
                                sRequeteSelect &= " '' AS [" & oColonne.sNomColonne & "],"
                                sRequeteSelectVide &= " '' AS [" & oColonne.sNomColonne & "],"
                            Else
                                sRequeteSelect &= " [" & oColonne.sNomColonne & "] AS [" & oColonne.sNomColonne & "],"
                                sRequeteSelectVide &= " '' AS [" & oColonne.sNomColonne & "],"
                                sRequeteGroupBy &= " [" & oColonne.sNomColonne & "]  ,"
                                sRequeteHavingNormal &= " [" & oColonne.sNomColonne & "] LIKE'%" & sTexteRecherche & "%' OR"
                            End If

                        Case "nombre"
                            If oColonne.sMefDonnees.Contains("time") Then
                                sRequeteSelect &= " Administration.dbo.ConvertTimeToHHMMSS(" & oColonne.sTypeRegroupement & "([" & oColonne.sNomColonne & "] * " & stauxTva & " )) AS [" & oColonne.sNomColonne & "],"
                                sRequeteSelectVide &= " " & " '' AS [" & oColonne.sNomColonne & "],"


                                sRequeteOperation &= " Administration.dbo.ConvertTimeToHHMMSS(OPERATION(Administration.dbo.ConvertTimeToSecond([" & oColonne.sNomColonne & "])))  AS [" & oColonne.sNomColonne & "],"
                                sRequeteHavingNormal &= " Administration.dbo.ConvertTimeToHHMMSS(" & oColonne.sTypeRegroupement & "([" & oColonne.sNomColonne & "] * " & stauxTva & "))  LIKE'%" & sTexteRecherche & "%' OR"
                                If oColonne.sTypeRegroupement = "" Then
                                    sRequeteGroupBy &= " [" & oColonne.sNomColonne & "] * " & stauxTva & "  ,"
                                End If
                            Else
                                sRequeteSelect &= " " & oColonne.sTypeRegroupement & "([" & oColonne.sNomColonne & "] * " & stauxTva & ")  AS [" & oColonne.sNomColonne & "],"
                                sRequeteSelectVide &= " " & " '' AS [" & oColonne.sNomColonne & "],"
                                sRequeteOperation &= " OPERATION([" & oColonne.sNomColonne & "]) AS [" & oColonne.sNomColonne & "],"
                                sRequeteHavingNormal &= " " & oColonne.sTypeRegroupement & "([" & oColonne.sNomColonne & "] * " & stauxTva & ")  LIKE'%" & sTexteRecherche & "%' OR"
                                If oColonne.sTypeRegroupement = "" Then
                                    sRequeteGroupBy &= " [" & oColonne.sNomColonne & "] * " & stauxTva & "  ,"
                                End If
                            End If








                            'Case "action"
                            '    sRequeteSelect &= " '' AS [" & oColonne.sNomColonne & "],"
                            'sRequeteGroupBy &= " [" & oColonne.sNomColonne & "]  ,"
                            'sRequeteHavingNormal &= " [" & oColonne.sNomColonne & "] LIKE'%" & sTexteRecherche & "%' OR"
                    End Select
                End If

            Next

            If sRequeteSelect <> "SELECT " Then sRequeteSelect = sRequeteSelect.Remove(sRequeteSelect.Length - 1, 1)
            If sRequeteSelectVide <> "SELECT " Then sRequeteSelectVide = sRequeteSelectVide.Remove(sRequeteSelectVide.Length - 1, 1)
            If sRequeteGroupBy <> "GROUP BY " Then sRequeteGroupBy = sRequeteGroupBy.Remove(sRequeteGroupBy.Length - 1, 1)
            If sRequeteOperation <> "SELECT " Then sRequeteOperation = sRequeteOperation.Remove(sRequeteOperation.Length - 1, 1)
            If sRequeteHavingNormal <> "HAVING " Then sRequeteHavingNormal = sRequeteHavingNormal.Remove(sRequeteHavingNormal.Length - 3, 3)

            '---Gestion du Top
            If Not oRapport.sSpecReq Is Nothing Then
                sTableTop = oRapport.sSpecReq.Split(CChar(";"))
                If sTableTop.Length = 3 Then
                    If sTableTop(0).ToUpper.StartsWith("TOP") Then
                        sRequeteSelect = sRequeteSelect.Replace("SELECT ", "SELECT " + sTableTop(0).ToUpper + " ")
                    End If
                End If
            End If

            If sRequeteGroupBy = "GROUP BY " Then
                sRequeteGroupBy = ""
            End If

            sRequeteHavingNormal += ") "

            sRequeteFrom &= " FROM [" & oRapport.sTableRapport & "] "
            If bAfficherTTC Then
                sRequeteFrom &= " INNER Join Administration.dbo.ReturnTvaByIdClient(" & oRapport.oClient.nIdClient & ") ON"
                sRequeteFrom &= " [" & oRapport.sTableRapport & "].[" & oRapport.sColonneLiaisonMoisTva & "] = Administration.dbo.ReturnTvaByIdClient.Mois AND"
                sRequeteFrom &= "  [" & oRapport.sTableRapport & "].Annee = Administration.dbo.ReturnTvaByIdClient.Annee "
            End If

            Dim iDernierMois As Integer = 12
            Dim oRapports As New Rapports
            oRapports.chargerRappportsParModuleUtilisateurIdRapport(oRapport.oModule.nIdModule, oRapport.oUtilisateur.nIdUtilisateur, oRapport.nIdRapport, oRapport.oClient.nIdClient)
            Dim oFiltres As New Filtres
            oFiltres.chargerFiltresParRapports(oRapports, oRapport, oUtilisateurSecondaireFiltres)

            Dim iAnnee As Integer = 0
            sRequeteWhere += cDal.CreerClauseWhere(oFiltres, oRapport, oRapports, ListeControles, Div.ID, bMoisGlissant) + " "

            If bMoisGlissant = True Then
                Dim sRequeteWhereTemporaire As String = ""

                iAnnee = cDal.Recuperer_Dernier_Annee_Table(oRapport)
                iDernierMois = cDal.Recuperer_Dernier_Mois_Table(oRapport, iAnnee)

                sRequeteWhereTemporaire = " WHERE (" & sRequeteWhere.Replace(" WHERE ", "") & " AND ListeMois<=" & iDernierMois & ") OR ("
                Dim sReplaceAnnee As String = sRequeteWhere.Replace(" WHERE ", "").Replace("[annee]=" & CStr(iAnnee), "[annee]=" & CStr(iAnnee - 1))
                sReplaceAnnee = sReplaceAnnee.Replace(" WHERE ", "").Replace("[Annee]=" & CStr(iAnnee), "[Annee]=" & CStr(iAnnee - 1))
                sRequeteWhereTemporaire += sReplaceAnnee & " AND ListeMois>" & iDernierMois & ")"

                sRequeteWhere = sRequeteWhereTemporaire
            End If

            If Not oRapport.sSpecReq Is Nothing Then
                sTableTop = oRapport.sSpecReq.Split(CChar(";"))
                If sTableTop.Length = 3 Then
                    If sTableTop(0).ToUpper.StartsWith("TOP") Then
                        sRequeteOrderBy = " ORDER BY [" + oColonnes.Find(Function(p) p.nIdColonne = CInt(sTableTop(1))).sNomColonne + "]" + sTableTop(2)
                    End If
                End If
            End If

            If (sValeurOutilTop.ToUpper <> sTous.ToUpper And sValeurOutilTop <> "") And oRapport.sOutilTop <> "" Then
                Dim oTris As New Tris
                Dim idGrid As String = Replace("Grid_" & oRapport.sSommaireNiv1 & "_" & oRapport.sSommaireNiv2 & "_Rapport" & CStr(oRapport.nIdRapport), " ", "__").Replace("-", "ù").Replace("'", "")
                Dim sTable() As String = sListeTriTableau.Split(CChar("\"))
                If sListeTriTableau <> "" Then
                    For i = 0 To sTable.Length - 1 Step 3
                        If i + 2 <= sTable.Length - 1 Then
                            If idGrid = sTable(i) Then
                                oTris.Add(New Tri(sTable(i), sTable(i + 1), sTable(i + 2)))
                            End If
                        End If
                    Next
                End If

                If sTriActif.Split(CChar("|"))(0) = idGrid Then
                    Dim oTriActif As Tri = oTris.Find(Function(p) p.sdatafield = sTriActif.Split(CChar("|"))(1))
                    If oTriActif Is Nothing Then
                        oTris.Add(New Tri(sTriActif.Split(CChar("|"))(0), sTriActif.Split(CChar("|"))(1), "desc"))
                    Else
                        Select Case oTriActif.sTri.ToUpper
                            Case "ASC"
                                oTriActif.sTri = "desc"
                            Case "DESC"
                                oTriActif.sTri = "asc"
                        End Select
                    End If
                End If

                Dim oTrisFinal As New Tris
                For Each oTri In oTris
                    If sTriActif <> "" Then
                        If sTriActif.Split(CChar("|"))(0) = idGrid Then
                            If sTriActif.Split(CChar("|"))(1) = oTri.sdatafield Then
                                oTrisFinal.Add(oTri)
                            End If
                        Else
                            oTrisFinal.Add(oTri)
                        End If
                    Else
                        oTrisFinal.Add(oTri)
                    End If
                Next

                If oTrisFinal.Count > 0 Then
                    sRequeteOrderBy = " ORDER BY "
                    For Each oTri In oTrisFinal
                        sRequeteOrderBy &= " [" & Toolbox.EnleverPartieTotalNomColonne(oColonnes.Find(Function(p) Toolbox.EnleverPartieTotalNomColonne(p.sNomColonne) = oTri.sdatafield).sNomColonne) & "] " + oTri.sTri & ","
                    Next
                    If sRequeteOrderBy <> " ORDER BY " Then sRequeteOrderBy = sRequeteOrderBy.Remove(sRequeteOrderBy.Length - 1, 1)
                End If
            End If



            sRequeteFinale = sRequeteSelect & sRequeteFrom & sRequeteWhere & sRequeteGroupBy & sRequeteHavingNormal & sRequeteOrderBy

            Resultat(0) = sRequeteFinale
            Resultat(1) = sRequeteOperation & " FROM (" & Resultat(0) & ") AS derivedtbl_1"
            Resultat(2) = sRequeteSelectVide

            Return Resultat
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Function

    Public Shared Sub ChargerDetailFactureParId(ByRef oDetailFacturation As DetailFacturation, ByVal nIdFacture As Integer, oUtilisateur As Utilisateur)
        Dim sRequete As String = ""
        Dim sql As New cSQL("Facturation")
        Dim readerSQL As SqlDataReader = Nothing

        Try
            sRequete = "SELECT dbo.Facture.idFacture, dbo.Facture.numeroFacture, dbo.Operateur.libelleOperateur, dbo.Facture.montant AS montantHT, "
            sRequete += " dbo.Facture.montant * ReturnTvaByIdClient_1.tauxTva AS montantTTC, dbo.Facture.montantPaye AS montantPayeHT, dbo.Statut.libelleStatut, dbo.Statut.lienImage , dbo.Facture.dateFacture, "
            sRequete += " dbo.TypeFacture.libelleTypeFacture, dbo.CompteFacturant.libelleCompteFacturant , dbo.mandat.sLibelleMandat, dbo.Periode.Annee, dbo.Periode.Mois "
            sRequete += " FROM dbo.Marche INNER JOIN"
            sRequete += " dbo.Operateur ON dbo.Marche.idMarche = dbo.Operateur.idMarche INNER JOIN"
            sRequete += " dbo.Client ON dbo.Marche.idClient = dbo.Client.idClient INNER JOIN"
            sRequete += " dbo.Statut ON dbo.Client.idClient = dbo.Statut.idClient INNER JOIN"
            sRequete += " dbo.Lot INNER JOIN"
            sRequete += " dbo.Facture INNER JOIN"
            sRequete += " dbo.CompteFacturant ON dbo.Facture.idCompteFacturant = dbo.CompteFacturant.idCompteFacturant ON dbo.Lot.idLot = dbo.CompteFacturant.idLot ON "
            sRequete += " dbo.Statut.idStatut = dbo.Facture.idStatut LEFT OUTER JOIN"
            sRequete += " dbo.Mandat ON dbo.Facture.idFacture = dbo.Mandat.idFacture INNER JOIN"
            sRequete += " dbo.TypeFacture ON dbo.Client.idClient = dbo.TypeFacture.idClient AND dbo.Facture.idTypeFacture = dbo.TypeFacture.idTypeFacture INNER JOIN"
            sRequete += " dbo.TypePrestation ON dbo.Operateur.idOperateur = dbo.TypePrestation.idOperateur AND "
            sRequete += " dbo.Lot.idTypePrestation = dbo.TypePrestation.idTypePrestation INNER JOIN"
            sRequete += " dbo.Periode ON dbo.Facture.idPeriode = dbo.Periode.Id_Periode INNER JOIN"
            sRequete += " Administration.dbo.ReturnTvaByIdClient(" + CStr(oUtilisateur.oClient.nIdClient) + ") AS ReturnTvaByIdClient_1 ON dbo.Periode.Annee = ReturnTvaByIdClient_1.Annee AND "
            sRequete += " dbo.Periode.Mois = ReturnTvaByIdClient_1.Mois LEFT OUTER JOIN"
            sRequete += " dbo.DateType RIGHT OUTER JOIN"
            sRequete += " dbo.Dates ON dbo.DateType.idDateType = dbo.Dates.idDateType ON dbo.Facture.idFacture = dbo.Dates.idFacture FULL OUTER JOIN"
            sRequete += " dbo.Liaison ON dbo.Facture.idFacture = dbo.Liaison.idFacturePrincipale"
            sRequete += " WHERE dbo.Facture.idFacture = " + CStr(nIdFacture)

            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oDetailFacturation.nIdFacture, readerSQL.Item("idFacture"))
                ChargerPropriete(oDetailFacturation.sNumeroFacture, readerSQL.Item("numeroFacture"))
                ChargerPropriete(oDetailFacturation.sLibelleOperateur, readerSQL.Item("libelleOperateur"))
                ChargerPropriete(oDetailFacturation.nMontantHT, readerSQL.Item("montantHT"))
                ChargerPropriete(oDetailFacturation.nMontantTTC, readerSQL.Item("MontantTTC"))
                ChargerPropriete(oDetailFacturation.nMontantPayeHT, readerSQL.Item("montantPayeHT"))
                ChargerPropriete(oDetailFacturation.sLibelleStatut, readerSQL.Item("libelleStatut"))
                ChargerPropriete(oDetailFacturation.sLibelleTypeFacture, readerSQL.Item("libelleTypeFacture"))
                ChargerPropriete(oDetailFacturation.sLienImageStatut, readerSQL.Item("lienImage"))
                ChargerPropriete(oDetailFacturation.sLibelleCompteFacturant, readerSQL.Item("libelleCompteFacturant"))
                ChargerPropriete(oDetailFacturation.sLibelleMandat, readerSQL.Item("sLibelleMandat"))
                ChargerPropriete(oDetailFacturation.dDateFacture, readerSQL.Item("dateFacture"))
                ChargerPropriete(oDetailFacturation.nAnnee, readerSQL.Item("Annee"))
                ChargerPropriete(oDetailFacturation.nMois, readerSQL.Item("Mois"))
                Dim oDetailFacturations As New DetailFacturations
                oDetailFacturations.ChargerListeFacturesLiees(oDetailFacturation.nIdFacture, oUtilisateur)
                oDetailFacturation.oListeFacturesLiees = oDetailFacturations
                Dim oDetailFacturationDates As New DetailFacturationDates
                oDetailFacturationDates.ChargerListeDatesDetailFactures(oDetailFacturation.nIdFacture)
                oDetailFacturation.oListeDatesFactures = oDetailFacturationDates
                Dim oDetailFacturationDocuments As New DetailFacturationDocuments
                oDetailFacturationDocuments.ChargerListeDocumentsFactures(oDetailFacturation.nIdFacture)
                oDetailFacturation.oListeDocuments = oDetailFacturationDocuments
            Loop

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Sub

    Public Shared Sub ChargerListeDocumentsFactures(ByRef oDetailFacturationDocuments As DetailFacturationDocuments, ByVal nIdFacture As Integer)
        Dim sRequete As String = ""
        Dim Sql As New cSQL("Facturation")
        Dim readerSQL As SqlDataReader = Nothing
        Dim oDetailFacturationDocument As DetailFacturationDocument
        Try
            sRequete = "SELECT idDocument, titreDocument, commentaireDocument, dateDocument, nomFichier, idFacture FROM Document WHERE idFacture = " & CStr(nIdFacture)

            readerSQL = Sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                oDetailFacturationDocument = New DetailFacturationDocument
                ChargerPropriete(oDetailFacturationDocument.nIdDocument, readerSQL.Item("idDocument"))
                ChargerPropriete(oDetailFacturationDocument.sTitreDocument, readerSQL.Item("titreDocument"))
                ChargerPropriete(oDetailFacturationDocument.sCommentaireDocument, readerSQL.Item("commentaireDocument"))
                ChargerPropriete(oDetailFacturationDocument.dDateDocument, readerSQL.Item("dateDocument"))
                ChargerPropriete(oDetailFacturationDocument.sNomFichier, readerSQL.Item("nomFichier"))
                ChargerPropriete(oDetailFacturationDocument.nIdFacture, readerSQL.Item("idFacture"))
                oDetailFacturationDocuments.Add(oDetailFacturationDocument)
            Loop

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                Sql.libererDataReader(readerSQL)
            End If
            Sql.fermerConnexion()
            Sql = Nothing
        End Try
    End Sub


    Public Shared Sub ChargerListeFacturesLiees(ByRef oDetailFacturations As DetailFacturations, ByVal nIdFacture As Integer, oUtilisateur As Utilisateur)
        Dim sRequete As String = ""
        Dim sql As New cSQL("Facturation")
        Dim readerSQL As SqlDataReader = Nothing
        Dim oDetailFacturation As DetailFacturation
        Try

            sRequete = "SELECT dbo.Facture.idFacture, dbo.Facture.numeroFacture, dbo.Operateur.libelleOperateur, dbo.Facture.montant AS montantHT, "
            sRequete += " dbo.Facture.montant * ReturnTvaByIdClient_1.tauxTva AS montantTTC, dbo.Facture.montantPaye AS montantPayeHT, dbo.Statut.libelleStatut, dbo.Statut.lienImage,  "
            sRequete += " dbo.TypeFacture.libelleTypeFacture,dbo.Liaison.commentaireLiaison, dbo.mandat.sLibelleMandat"
            sRequete += " FROM dbo.Marche INNER JOIN"
            sRequete += " dbo.Operateur ON dbo.Marche.idMarche = dbo.Operateur.idMarche INNER JOIN"
            sRequete += " dbo.Client ON dbo.Marche.idClient = dbo.Client.idClient INNER JOIN"
            sRequete += " dbo.Statut ON dbo.Client.idClient = dbo.Statut.idClient INNER JOIN"
            sRequete += " dbo.Lot INNER JOIN"
            sRequete += " dbo.Facture INNER JOIN"
            sRequete += " dbo.CompteFacturant ON dbo.Facture.idCompteFacturant = dbo.CompteFacturant.idCompteFacturant ON dbo.Lot.idLot = dbo.CompteFacturant.idLot ON "
            sRequete += " dbo.Statut.idStatut = dbo.Facture.idStatut LEFT OUTER JOIN"
            sRequete += " dbo.Mandat ON dbo.Facture.idFacture = dbo.Mandat.idFacture INNER JOIN"
            sRequete += " dbo.TypeFacture ON dbo.Client.idClient = dbo.TypeFacture.idClient AND dbo.Facture.idTypeFacture = dbo.TypeFacture.idTypeFacture INNER JOIN"
            sRequete += " dbo.TypePrestation ON dbo.Operateur.idOperateur = dbo.TypePrestation.idOperateur AND "
            sRequete += " dbo.Lot.idTypePrestation = dbo.TypePrestation.idTypePrestation INNER JOIN"
            sRequete += " dbo.Periode ON dbo.Facture.idPeriode = dbo.Periode.Id_Periode INNER JOIN"
            sRequete += " Administration.dbo.ReturnTvaByIdClient(" + CStr(oUtilisateur.oClient.nIdClient) + ") AS ReturnTvaByIdClient_1 ON dbo.Periode.Annee = ReturnTvaByIdClient_1.Annee AND "
            sRequete += " dbo.Periode.Mois = ReturnTvaByIdClient_1.Mois INNER JOIN"
            sRequete += " dbo.Liaison ON dbo.Facture.idFacture = dbo.Liaison.idFactureLiee FULL OUTER JOIN"
            sRequete += " dbo.Liaison AS Liaison_1 ON dbo.Facture.idFacture = Liaison_1.idFacturePrincipale"
            sRequete += " WHERE dbo.Liaison.idFacturePrincipale = " + CStr(nIdFacture)

            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                oDetailFacturation = New DetailFacturation
                ChargerPropriete(oDetailFacturation.nIdFacture, readerSQL.Item("idFacture"))
                ChargerPropriete(oDetailFacturation.sNumeroFacture, readerSQL.Item("numeroFacture"))
                ChargerPropriete(oDetailFacturation.sLibelleOperateur, readerSQL.Item("libelleOperateur"))
                ChargerPropriete(oDetailFacturation.nMontantHT, readerSQL.Item("montantHT"))
                ChargerPropriete(oDetailFacturation.nMontantTTC, readerSQL.Item("MontantTTC"))
                ChargerPropriete(oDetailFacturation.nMontantPayeHT, readerSQL.Item("montantPayeHT"))
                ChargerPropriete(oDetailFacturation.sLibelleStatut, readerSQL.Item("libelleStatut"))
                ChargerPropriete(oDetailFacturation.sLibelleTypeFacture, readerSQL.Item("libelleTypeFacture"))
                ChargerPropriete(oDetailFacturation.sCommentaireLiaison, readerSQL.Item("commentaireLiaison"))
                ChargerPropriete(oDetailFacturation.sLienImageStatut, readerSQL.Item("lienImage"))
                ChargerPropriete(oDetailFacturation.sLibelleMandat, readerSQL.Item("sLibelleMandat"))
                oDetailFacturations.Add(oDetailFacturation)
            Loop

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub ChargerListeDatesDetailFactures(ByRef oDetailFacturationDates As DetailFacturationDates, ByVal nIdFacture As Integer)
        Dim sRequete As String = ""
        Dim sql As New cSQL("Facturation")
        Dim readerSQL As SqlDataReader = Nothing
        Dim oDetailFacturationDate As DetailFacturationDate
        Try

            sRequete = "SELECT dbo.DateType.libelleDateType, dbo.Dates.dateFacture"
            sRequete += " FROM dbo.Dates INNER JOIN"
            sRequete += " dbo.DateType ON dbo.Dates.idDateType = dbo.DateType.idDateType"
            sRequete += " WHERE dbo.Dates.idFacture = " + CStr(nIdFacture)
            sRequete += " ORDER BY dbo.DateType.ordreDateType"

            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                oDetailFacturationDate = New DetailFacturationDate
                ChargerPropriete(oDetailFacturationDate.sLibelleDateType, readerSQL.Item("libelleDateType"))
                ChargerPropriete(oDetailFacturationDate.dDateFacture, readerSQL.Item("dateFacture"))
                oDetailFacturationDates.Add(oDetailFacturationDate)
            Loop

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Function Recuperer_Dernier_Annee_Table(ByVal oRapport As Rapport) As Integer
        Dim sRequete As String = ""
        Dim sql As New cSQL(oRapport.oClient.sBaseClient)
        Dim readerSQL As SqlDataReader = Nothing
        Dim oColonnes As New Colonnes()
        Dim Resultat As String = "12"

        Try
            sRequete = "SELECT MAX(Annee) AS AnneesMax FROM dbo.[" & oRapport.sTableRapport & "]"
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                Resultat = readerSQL.Item("AnneesMax").ToString
                Exit Do
            Loop
            If Resultat = "" Then Resultat = "0"
            Return CInt(Resultat)
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Function

    Public Shared Function Recuperer_Dernier_Mois_Table(ByVal oRapport As Rapport, ByVal iAnnee As Integer) As Integer
        Dim sRequete As String = ""
        Dim sql As New cSQL(oRapport.oClient.sBaseClient)
        Dim readerSQL As SqlDataReader = Nothing
        Dim oColonnes As New Colonnes()
        Dim Resultat As String = "12"

        Try
            sRequete = "SELECT MAX(ListeMois) AS ListeMoisMax FROM dbo.[" & oRapport.sTableRapport & "] WHERE (Annee = " & iAnnee & ")"
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                Resultat = readerSQL.Item("ListeMoisMax").ToString
                Exit Do
            Loop
            If Resultat = "" Then Resultat = "0"
            Return CInt(Resultat)
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Function

    Public Shared Sub ChargerLigneTotal(ByVal oColonnes As Colonnes, ByVal oRapport As Rapport, ByRef e As GridViewRowEventArgs, ByRef ListeControles As List(Of Object), ByVal Div As HtmlGenericControl, ByVal oCheckboxMoisGlissant As CheckBox, ByVal oCheckBoxTTC As CheckBox, ByVal oCheckboxDecimales As CheckBox, ByVal sValeurOutilTop As String, ByVal sListeTriTableau As String, ByVal sTriActif As String, ByVal oGrid As GridView, ByVal iDerniereColonneVisible As Integer, ByVal oBloquerColoneAction As HiddenField, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres)
        Dim sRequete As String = ""
        Dim sql As New cSQL(oRapport.oClient.sBaseClient)
        Dim readerSQL As SqlDataReader = Nothing
        Dim i As Integer
        Dim nColSpan As Integer = 0
        Dim nTailleColspan = 0
        Dim TableBordures As New List(Of String)
        Dim bPremiereCellule As Boolean = Nothing
        Dim nNbColonnesASupprimer As Integer = 0

        Dim iTotal As Double = 0
        Dim IdColonneTotal As Integer = 0

        Try
            sRequete = cDal.Creer_Requete_Tableau(oRapport, oColonnes, ListeControles, Div, oCheckBoxTTC, oCheckboxMoisGlissant, sValeurOutilTop, sListeTriTableau, sTriActif, oBloquerColoneAction, oUtilisateurSecondaireFiltres)(1).Replace("OPERATION", oRapport.sTypeTotal.ToUpper)

            e.Row.Cells(0).Text = oRapport.sTitreTotal
            e.Row.TableSection = TableRowSection.TableFooter

            If oRapport.bAfficherHtTtcDansLibelleTotal = True And oRapport.bAfficherCaseTTC = True Then
                Dim bAfficherTTC As Boolean = oCheckBoxTTC.Checked

                Select Case oRapport.oClient.sMonnaieClient
                    Case "euro"
                        Select Case bAfficherTTC
                            Case True
                                e.Row.Cells(0).Text += " (€ TTC)"
                            Case False
                                e.Row.Cells(0).Text += " (€ HT)"
                        End Select
                    Case "dollar"
                        Select Case bAfficherTTC
                            Case True
                                e.Row.Cells(0).Text += " ($ TTC)"
                            Case False
                                e.Row.Cells(0).Text += " ($)"
                        End Select
                End Select




            End If

            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                i = 0
                For Each oColonne In oColonnes


                    If oColonne.sTypeDonnees.ToUpper <> "NOMBRE" And oColonne.sTypeDonnees.ToUpper <> "NOMBREBALISE" And oGrid.Columns(i).Visible = False Then
                        nNbColonnesASupprimer += 1
                    End If

                    If oColonne.sTypeDonnees.ToUpper <> "NOMBRE" And oColonne.sTypeDonnees.ToUpper <> "NOMBREBALISE" And oGrid.Columns(i).Visible = True Then
                        nColSpan += 1
                        nTailleColspan += oColonne.nLargeurColonne + 1
                    End If

                    If oColonne.sTypeDonnees.ToUpper = "NOMBRE" Or oColonne.sTypeDonnees.ToUpper = "NOMBREBALISE" Then
                        Dim sNomColonneRetraire As String = ""
                        If oColonne.sNomColonne.Contains("|") Then
                            sNomColonneRetraire = oColonne.sNomColonne.Split(CChar("|"))(0)
                        Else
                            sNomColonneRetraire = oColonne.sNomColonne
                        End If

                        Dim sAffichage As String = readerSQL.Item(sNomColonneRetraire).ToString


                        If oColonnes(i).sNomColonne.Contains("|") Then
                            iTotal = CDbl(sAffichage)
                            IdColonneTotal = i
                        End If

                        e.Row.Cells(i).Text = Toolbox.Gerer_Format(sAffichage, oColonne, oCheckboxDecimales)


                        e.Row.Cells(i).CssClass = "C_total_Fusion"
                        e.Row.Cells(i).Width = oColonne.nLargeurColonne

                        If IsNumeric(e.Row.Cells(i).Text) Then
                            If CInt(e.Row.Cells(i).Text) = 0 Then e.Row.Cells(i).Text = oColonne.sSiZero
                            If e.Row.Cells(i).Text = "" Then e.Row.Cells(i).Text = oColonne.sSiNull
                        Else
                            If Not oColonne.sMefDonnees.ToUpper.Contains("TIME") Then
                                e.Row.Cells(i).Text = ""
                            Else
                                If e.Row.Cells(i).Text = "00:00:00" Then e.Row.Cells(i).Text = oColonne.sSiZero
                                If e.Row.Cells(i).Text = "" Then e.Row.Cells(i).Text = oColonne.sSiNull
                            End If
                        End If

                        If iDerniereColonneVisible = i Then
                            e.Row.Cells(i).Attributes.Add("style", "min-width:" & CStr(oColonne.nLargeurColonne + 20) & "Px;")
                        Else
                            e.Row.Cells(i).Attributes.Add("style", "min-width:" & (oColonne.nLargeurColonne).ToString & "Px;")
                        End If
                        Toolbox.AppliquerFormatCellule(oColonne.sStyleDonnees, e.Row.Cells(i))
                    End If

                    i += 1
                Next
            Loop

            e.Row.Cells(0).ColumnSpan = nColSpan
            e.Row.Cells(0).Width = New Unit(nTailleColspan, UnitType.Pixel)
            e.Row.Cells(0).CssClass = "C_total"

            For i = nColSpan + nNbColonnesASupprimer - 1 To 1 Step -1
                e.Row.Cells.Remove(e.Row.Cells(i))
            Next

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub SauvegardeEtatCheckboxChoixVisible(ByRef oParamRapportColonne As ParamRapportColonne)
        Dim sql As New cSQL("Administration")
        Dim requete As String = "UPDATE ParamRapportColonne Set afficher='" & oParamRapportColonne.bAfficher & "' WHERE idRapport=" & oParamRapportColonne.oRapport.nIdRapport &
            " AND idClient=" & oParamRapportColonne.oRapport.oClient.nIdClient & " AND idUtilisateur=" & oParamRapportColonne.oRapport.oUtilisateur.nIdUtilisateur &
            " AND idModule=" & oParamRapportColonne.oRapport.oModule.nIdModule & " AND idParamColonnes=" & oParamRapportColonne.oColonne.nIdColonne
        Try
            sql.executerRequete(requete)
        Catch ex As Exception
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try

    End Sub

    Public Shared Function CreerClauseWhere(ByVal oFiltres As Filtres, ByVal oRapport As Rapport, ByVal oRapports As Rapports, ByRef ListeControles As List(Of Object), ByVal IdDiv As String, ByVal bMoisGlissant As Boolean) As String

        Dim sql As New cSQL(oRapport.oClient.sBaseClient)
        Dim Requete As String = ""
        Dim Requete_Where As String = ""
        Dim Requete_Having As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim PremierPassageWhere As Boolean = True
        Dim ControleTrouve As Object
        Dim TableValeurs As New ArrayList
        Dim PremierPassageWhereMulti As Boolean = True
        Dim PremierPassageHavingMulti As Boolean = True
        Dim Valeur As String = ""
        Dim PremierPassageHaving As Boolean = True
        Dim oRapportTrouve As New Rapport
        'Dim oRapports As New Rapports


        'oRapports.chargerRappportsParModuleUtilisateur(oRapport.oModule.nIdModule, oRapport.oUtilisateur.nIdUtilisateur)

        Dim sTous As String = "Tous"
        If oRapport.oClient.sLangueClient = "US" Then sTous = "All"


        For Each oFiltre In oFiltres
            ControleTrouve = Nothing

            Dim nIdRapportTrouve As Integer = oFiltre.TesterFiltrePresentRapport(oFiltre.nIdFiltre, oRapport, oRapports, True)
            If nIdRapportTrouve > 0 Then
                oRapportTrouve = oRapports.Find(Function(p) p.nIdRapport = nIdRapportTrouve)
                For i = 0 To ListeControles.Count - 1
                    If TypeOf ListeControles(i) Is System.Web.UI.WebControls.DropDownList Then
                        Dim ddlControle As DropDownList = CType(ListeControles(i), DropDownList)
                        If CDbl(ddlControle.ID.Split(CChar("_"))(ddlControle.ID.Split(CChar("_")).Length - 2)) = oFiltre.nIdFiltre Then
                            ControleTrouve = ListeControles(i)
                        End If
                    ElseIf TypeOf ListeControles(i) Is System.Web.UI.WebControls.ListBox Then
                        Dim ddlControle As ListBox = CType(ListeControles(i), ListBox)
                        If CDbl(ddlControle.ID.Split(CChar("_"))(ddlControle.ID.Split(CChar("_")).Length - 2)) = oFiltre.nIdFiltre Then
                            ControleTrouve = ListeControles(i)
                        End If
                    End If
                Next

                If Not IsNothing(ControleTrouve) And oRapportTrouve.nIdRapport = oRapport.nIdRapport Then
                    Select Case oFiltre.bEstMultiselection
                        Case True
                            Dim oControle As ListBox = CType(ControleTrouve, ListBox)

                            PremierPassageWhereMulti = True
                            '---Multiselection / Valeur selectionee
                            For Each ItemSelectionne In oControle.GetSelectedIndices
                                If oControle.Items(ItemSelectionne).Value <> sTous And oControle.Items(ItemSelectionne).Value <> "" Then
                                    If PremierPassageWhere = True Then
                                        PremierPassageWhere = False
                                        PremierPassageWhereMulti = False
                                        Requete_Where &= " WHERE ("
                                    Else
                                        If PremierPassageWhereMulti = True Then
                                            PremierPassageWhereMulti = False
                                            Requete_Where &= " And ("
                                        Else
                                            Requete_Where &= " OR "
                                        End If

                                    End If
                                    Valeur = Toolbox.ConvertirValeurFiltre(oControle.Items(ItemSelectionne).Value, oFiltre.sNomColonne, oRapport.oClient)
                                    Select Case oFiltre.sTypeColonne
                                        Case "numerique"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                        Case "texte", "date"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]='" & Toolbox.ReplaceQuote(Valeur) & "'"
                                        Case "booleen"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                    End Select
                                End If
                            Next
                            If PremierPassageWhereMulti = False Then Requete_Where &= ")"

                            '---Pas encore de données mais valeur par defaut présente
                            If oControle.SelectedValue = "" And oFiltre.sValeurFiltre <> sTous Then
                                PremierPassageWhereMulti = True
                                For Each Valeur In oFiltre.sValeurFiltre.Split(CChar(";"))
                                    If PremierPassageWhere = True Then
                                        Requete_Where &= " WHERE ("
                                        PremierPassageWhere = False
                                        PremierPassageWhereMulti = False
                                    Else
                                        If PremierPassageWhereMulti = True Then
                                            PremierPassageWhereMulti = False
                                            Requete_Where &= " And ("
                                        Else
                                            Requete_Where &= " OR "
                                        End If
                                    End If
                                    Select Case oFiltre.sTypeColonne
                                        Case "numerique"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                        Case "texte", "date"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]='" & Toolbox.ReplaceQuote(Valeur) & "'"
                                        Case "booleen"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                    End Select

                                Next
                                If PremierPassageWhereMulti = False Then Requete_Where &= ")"
                            End If

                            '--- si Est restriction et Tous est selectionne
                            If oFiltre.bEstRestriction = True And oFiltre.bEstTous = True And oControle.SelectedValue = sTous Then
                                PremierPassageWhereMulti = True
                                For Each Valeur In oFiltre.sValeurFiltre.Split(CChar(";"))
                                    If PremierPassageWhere = True Then
                                        Requete_Where &= " WHERE ("
                                        PremierPassageWhere = False
                                        PremierPassageWhereMulti = False
                                    Else
                                        If PremierPassageWhereMulti = True Then
                                            PremierPassageWhereMulti = False
                                            Requete_Where &= " And ("
                                        Else
                                            Requete_Where &= " OR "
                                        End If
                                    End If
                                    Select Case oFiltre.sTypeColonne
                                        Case "numerique"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                        Case "texte", "date"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]='" & Toolbox.ReplaceQuote(Valeur) & "'"
                                        Case "booleen"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                    End Select

                                Next
                                If PremierPassageWhereMulti = False Then Requete_Where &= ")"
                            End If

                        Case False
                            Dim oControle As DropDownList = CType(ControleTrouve, DropDownList)
                            'DropdownList / Valeur Séléctionnee
                            If oControle.SelectedValue <> sTous And oControle.SelectedValue <> "" Then
                                If PremierPassageWhere = True Then
                                    PremierPassageWhere = False
                                    Requete_Where &= " WHERE "
                                Else
                                    Requete_Where &= " And "
                                End If
                                Valeur = Toolbox.ConvertirValeurFiltre(oControle.SelectedValue, oFiltre.sNomColonne, oRapport.oClient)

                                Select Case oFiltre.sTypeColonne
                                    Case "numerique"
                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                    Case "texte", "date"
                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]='" & Toolbox.ReplaceQuote(Valeur) & "'"
                                    Case "booleen"
                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                End Select
                                '---Pas encore de données mais valeur par defaut présente
                            ElseIf oControle.SelectedValue = "" And oFiltre.sValeurFiltre <> sTous And oFiltre.sValeurFiltre <> "#Derniere valeur" Then
                                '---Si Est tous = false (on ne filtre que sur la première valeur)
                                If oFiltre.bEstTous = False Then
                                    If PremierPassageWhere = True Then
                                        PremierPassageWhere = False
                                        Requete_Where &= " WHERE "
                                    Else
                                        Requete_Where &= " And "
                                    End If
                                    Select Case oFiltre.sTypeColonne
                                        Case "numerique"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                        Case "texte", "date"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]='" & Toolbox.ReplaceQuote(oFiltre.sValeurFiltre.Split(CChar(";"))(0)) & "'"
                                        Case "booleen"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(oFiltre.sValeurFiltre.Split(CChar(";"))(0))
                                    End Select
                                Else
                                    '--- Si est Tous = true  (on filtre sur toutes les valeurs)
                                    PremierPassageWhereMulti = True
                                    For Each Valeur In oFiltre.sValeurFiltre.Split(CChar(";"))
                                        If PremierPassageWhere = True Then
                                            Requete_Where &= " WHERE ("
                                            PremierPassageWhere = False
                                            PremierPassageWhereMulti = False
                                        Else
                                            If PremierPassageWhereMulti = True Then
                                                PremierPassageWhereMulti = False
                                                Requete_Where &= " And ("
                                            Else
                                                Requete_Where &= " OR "
                                            End If
                                        End If
                                        Select Case oFiltre.sTypeColonne
                                            Case "numerique"
                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                            Case "texte", "date"
                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]='" & Toolbox.ReplaceQuote(Valeur) & "'"
                                            Case "booleen"
                                                Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                        End Select

                                    Next
                                    If PremierPassageWhereMulti = False Then Requete_Where &= ")"
                                End If
                                '---Pas encore de données mais valeur par defaut à calculer 
                            ElseIf oControle.SelectedValue = "" And oFiltre.sValeurFiltre = "#Derniere valeur" Then
                                Dim DerniereValeur As String = ""
                                If Not IsNothing(oFiltre.sDernierValeur) Then
                                    DerniereValeur = oFiltre.sDernierValeur
                                Else
                                    DerniereValeur = ChargerDerniereValeurFiltre(oFiltre, oFiltres, oRapport, False)
                                    oFiltre.sDernierValeur = DerniereValeur
                                End If

                                If PremierPassageWhere = True Then
                                    PremierPassageWhere = False
                                    Requete_Where &= " WHERE "
                                Else
                                    Requete_Where &= " And "
                                End If
                                Select Case oFiltre.sTypeColonne
                                    Case "numerique"
                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(DerniereValeur)
                                    Case "texte", "date"
                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]='" & Toolbox.ReplaceQuote(DerniereValeur) & "'"
                                    Case "booleen"
                                        Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(DerniereValeur)
                                End Select
                                '--- si Est restriction et Tous est selectionne
                            ElseIf oFiltre.bEstRestriction = True And oFiltre.bEstTous = True And oControle.SelectedValue = sTous Then
                                PremierPassageWhereMulti = True
                                For Each Valeur In oFiltre.sValeurFiltre.Split(CChar(";"))
                                    If PremierPassageWhere = True Then
                                        Requete_Where &= " WHERE ("
                                        PremierPassageWhere = False
                                        PremierPassageWhereMulti = False
                                    Else
                                        If PremierPassageWhereMulti = True Then
                                            PremierPassageWhereMulti = False
                                            Requete_Where &= " And ("
                                        Else
                                            Requete_Where &= " OR "
                                        End If
                                    End If
                                    Select Case oFiltre.sTypeColonne
                                        Case "numerique"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceQuote(Valeur)
                                        Case "texte", "date"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]='" & Toolbox.ReplaceQuote(Valeur) & "'"
                                        Case "booleen"
                                            Requete_Where &= " dbo.[" & oRapportTrouve.sTableRapport & "].[" & oFiltre.sNomColonne & "]=" & Toolbox.ReplaceBooleenSQL(Valeur)
                                    End Select

                                Next
                                If PremierPassageWhereMulti = False Then Requete_Where &= ")"
                            End If
                    End Select
                End If
            End If
        Next

        Return Requete_Where
    End Function

    Public Shared Function testerSiColonneRedirection(ByVal oColonne As Colonne, ByVal nIdClient As Integer) As Boolean
        Dim bResultat As Boolean = False

        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing

        Dim sRequete As String = "SELECT idRedirection, idColonne FROM  dbo.Redirection WHERE idColonne = " & CStr(oColonne.nIdColonne) & " AND idClient=" & nIdClient.ToString
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            bResultat = readerSQL.HasRows
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

        Return testerSiColonneRedirection

    End Function

    Public Shared Sub chargerRedirectionsParRapport(ByRef oRedirections As Redirections, ByVal oRapport As Rapport, ByVal oCheckboxMoisGlissant As CheckBox, ByVal DivId As String, ByVal oBloquerColoneAction As HiddenField)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try

            Dim bMoisGlissant As Boolean = False
            If Not oCheckboxMoisGlissant Is Nothing And oRapport.bAfficherCaseMoisGlissant = True Then
                If oCheckboxMoisGlissant.Checked = True Then
                    bMoisGlissant = True
                End If
            Else
                If oRapport.bAfficherEnMoisGlissant = True Then bMoisGlissant = True
            End If

            Dim iDernierMois As Integer = 0
            If bMoisGlissant = True Then
                Dim iAnnee As Integer = cDal.Recuperer_Dernier_Annee_Table(oRapport)
                iDernierMois = cDal.Recuperer_Dernier_Mois_Table(oRapport, iAnnee)
            End If

            Dim oColonnes As New Colonnes
            oColonnes.ChargerListeColonnes(oRapport, oCheckboxMoisGlissant, DivId, oBloquerColoneAction)


            Requete = "SELECT *"
            Requete += " FROM dbo.Redirection INNER JOIN"
            Requete += " dbo.ParamRapportRedirection ON dbo.Redirection.idRedirection = dbo.ParamRapportRedirection.idRedirections"
            Requete += " AND dbo.Redirection.idClient = dbo.ParamRapportRedirection.idClient"
            Requete += " WHERE dbo.ParamRapportRedirection.idRapport = " & CStr(oRapport.nIdRapport)
            Requete += " And dbo.ParamRapportRedirection.idClient = " & CStr(oRapport.oClient.nIdClient)
            Requete += " AND dbo.ParamRapportRedirection.idUtilisateur =  " & CStr(oRapport.oUtilisateur.nIdUtilisateur)
            Requete += " AND dbo.ParamRapportRedirection.idModule = " & CStr(oRapport.oModule.nIdModule)

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim bEstListeMois As Boolean = False
                If CBool(readerSQL.Item("estListeMois")) = True Then
                    bEstListeMois = True
                End If

                If bEstListeMois = True Then
                    For i = 1 + iDernierMois To 12
                        Dim oRedirection As New Redirection
                        ChargerPropriete(oRedirection.oClient, oRapport.oClient)
                        ChargerPropriete(oRedirection.nIdRedirection, readerSQL.Item("idRedirection"))
                        ChargerPropriete(oRedirection.nidColonne, 100000 + i + CInt(readerSQL.Item("idColonne")))
                        ChargerPropriete(oRedirection.sChaineFiltre, readerSQL.Item("chaineFiltre"))
                        ChargerPropriete(oRedirection.nIdRapportCible, readerSQL.Item("idRapportCible"))
                        ChargerPropriete(oRedirection.sPageRapportCible, readerSQL.Item("pageRapportCible"))
                        ChargerPropriete(oRedirection.bEstPopup, readerSQL.Item("estPopup"))
                        ChargerPropriete(oRedirection.sLargueurPopup, readerSQL.Item("largeurPopup"))
                        ChargerPropriete(oRedirection.sHauteurPopup, readerSQL.Item("hauteurPopup"))
                        ChargerPropriete(oRedirection.bRafraichissementFermeturePopup, readerSQL.Item("rafraichissementFermeturePopup"))
                        ChargerPropriete(oRedirection.bEstListeMois, readerSQL.Item("estListeMois"))
                        oRedirections.Add(oRedirection)
                    Next
                    For i = 1 To iDernierMois
                        Dim oRedirection As New Redirection
                        ChargerPropriete(oRedirection.oClient, oRapport.oClient)
                        ChargerPropriete(oRedirection.nIdRedirection, readerSQL.Item("idRedirection"))
                        ChargerPropriete(oRedirection.nidColonne, 100000 + i + CInt(readerSQL.Item("idColonne")))
                        ChargerPropriete(oRedirection.sChaineFiltre, readerSQL.Item("chaineFiltre"))
                        ChargerPropriete(oRedirection.nIdRapportCible, readerSQL.Item("idRapportCible"))
                        ChargerPropriete(oRedirection.sPageRapportCible, readerSQL.Item("pageRapportCible"))
                        ChargerPropriete(oRedirection.bEstPopup, readerSQL.Item("estPopup"))
                        ChargerPropriete(oRedirection.sLargueurPopup, readerSQL.Item("largeurPopup"))
                        ChargerPropriete(oRedirection.sHauteurPopup, readerSQL.Item("hauteurPopup"))
                        ChargerPropriete(oRedirection.bRafraichissementFermeturePopup, readerSQL.Item("rafraichissementFermeturePopup"))
                        ChargerPropriete(oRedirection.bEstListeMois, readerSQL.Item("estListeMois"))
                        oRedirections.Add(oRedirection)
                    Next
                Else
                    Dim oRedirection As New Redirection
                    ChargerPropriete(oRedirection.oClient, oRapport.oClient)
                    ChargerPropriete(oRedirection.nIdRedirection, readerSQL.Item("idRedirection"))
                    ChargerPropriete(oRedirection.nidColonne, readerSQL.Item("idColonne"))
                    ChargerPropriete(oRedirection.sChaineFiltre, readerSQL.Item("chaineFiltre"))
                    ChargerPropriete(oRedirection.nIdRapportCible, readerSQL.Item("idRapportCible"))
                    ChargerPropriete(oRedirection.sPageRapportCible, readerSQL.Item("pageRapportCible"))
                    ChargerPropriete(oRedirection.bEstPopup, readerSQL.Item("estPopup"))
                    ChargerPropriete(oRedirection.sLargueurPopup, readerSQL.Item("largeurPopup"))
                    ChargerPropriete(oRedirection.sHauteurPopup, readerSQL.Item("hauteurPopup"))
                    ChargerPropriete(oRedirection.bRafraichissementFermeturePopup, readerSQL.Item("rafraichissementFermeturePopup"))
                    ChargerPropriete(oRedirection.bEstListeMois, readerSQL.Item("estListeMois"))
                    oRedirections.Add(oRedirection)
                End If

            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub recuperationTypeGraphique(ByRef oTypeGraphique As TypeGraphique, ByRef oRapport As Rapport)

        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing

        Dim sRequete As String = "SELECT dbo.TypeGraphique.idTypeGraphique, dbo.TypeGraphique.typeGraphique, dbo.ParamRapportAffichageGraphique.idRapport, "
        sRequete &= "dbo.ParamRapportAffichageGraphique.idClient, dbo.ParamRapportAffichageGraphique.idUtilisateur, dbo.ParamRapportAffichageGraphique.idModule "
        sRequete &= "FROM dbo.ParamRapportAffichageGraphique INNER JOIN "
        sRequete &= "dbo.AffichageGraphique ON dbo.ParamRapportAffichageGraphique.idAffichageGraphique = dbo.AffichageGraphique.idAffichageGraphique INNER JOIN "
        sRequete &= "dbo.Serie ON dbo.AffichageGraphique.idSerie = dbo.Serie.idSerie INNER JOIN "
        sRequete &= "dbo.TypeGraphique ON dbo.Serie.idTypeGraphique = dbo.TypeGraphique.idTypeGraphique "
        sRequete &= "WHERE (dbo.ParamRapportAffichageGraphique.idRapport = " & oRapport.nIdRapport.ToString & ") AND (dbo.ParamRapportAffichageGraphique.idClient = " & oRapport.oClient.nIdClient.ToString & ") AND "
        sRequete &= "(dbo.ParamRapportAffichageGraphique.idUtilisateur = " & oRapport.oUtilisateur.nIdUtilisateur.ToString & ") AND (dbo.ParamRapportAffichageGraphique.idModule = " & oRapport.oModule.nIdModule.ToString & ")"
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oTypeGraphique.nIdTypeGraphique, readerSQL("idTypeGraphique"))
                ChargerPropriete(oTypeGraphique.stypeGraphique, readerSQL("typeGraphique"))
                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Sub

    Public Shared Sub RecupererEntete(ByRef oColonne As Colonne, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing

        Dim sRequete As String = "SELECT enteteNiveau1 FROM dbo.Colonne WHERE idParamColonnes = " & oColonne.nIdColonne.ToString & " and idClient = " & nIdClient.ToString
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oColonne.sEnteteNiveau1, readerSQL("enteteNiveau1"))
                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Function estGraphique(ByRef Orapport As Rapport) As Boolean
        Dim bEstGraphique As Boolean = False
        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing

        Dim sRequete As String = "Select dbo.ParamRapportAffichageGraphique.idAffichageGraphique "
        sRequete &= "FROM dbo.ParamRapportAffichageGraphique INNER JOIN "
        sRequete &= "dbo.Rapport ON dbo.ParamRapportAffichageGraphique.idRapport = dbo.Rapport.idRapport AND "
        sRequete &= "dbo.ParamRapportAffichageGraphique.idClient = dbo.Rapport.idClient And dbo.ParamRapportAffichageGraphique.idUtilisateur = dbo.Rapport.idUtilisateur And "
        sRequete &= "dbo.ParamRapportAffichageGraphique.idModule = dbo.Rapport.idModule "
        sRequete &= "WHERE (dbo.Rapport.idRapport = " & Orapport.nIdRapport.ToString & ") AND (dbo.Rapport.idClient = " & Orapport.oClient.nIdClient.ToString & ") "
        sRequete &= "AND (dbo.Rapport.idUtilisateur = " & Orapport.oUtilisateur.nIdUtilisateur.ToString & ") AND (dbo.Rapport.idModule = " & Orapport.oModule.nIdModule.ToString & ")"
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(bEstGraphique, readerSQL("idAffichageGraphique"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

        Return bEstGraphique
    End Function

    Public Shared Sub recupererGraphiqueParId(ByRef oGraphique As Graphique)
        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing

        Dim sRequete As String = "SELECT affichage3d, largeurParPoint, tailleLegende, inclination3D, rotation3D FROM dbo.Graphique WHERE idGraphique =" & oGraphique.nIdGraphique.ToString
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oGraphique.bAffichage3d, readerSQL("affichage3d"))
                ChargerPropriete(oGraphique.nLargeurParPoint, readerSQL("largeurParPoint"))
                ChargerPropriete(oGraphique.nTailleLegende, readerSQL("tailleLegende"))
                ChargerPropriete(oGraphique.nInclination3D, readerSQL("inclination3D"))
                ChargerPropriete(oGraphique.nRotation3D, readerSQL("rotation3D"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Sub

    Public Shared Sub recupererLegendeParId(ByRef oLegende As Legende)
        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing

        Dim sRequete As String = "SELECT nomLegende, ancrageInterneGraphique, position, entrelacementLegende FROM dbo.Legende WHERE idLegende=" & oLegende.nIdLegende.ToString
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oLegende.sNomLegende, readerSQL("nomLegende"))
                ChargerPropriete(oLegende.bAncrageInterneGraphique, readerSQL("ancrageInterneGraphique"))
                ChargerPropriete(oLegende.sPosition, readerSQL("position"))
                ChargerPropriete(oLegende.bEntrelacementLegende, readerSQL("entrelacementLegende"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub recupererAffichagePersonnaliseGraphique(ByRef oAffichagePersonnaliseGraphique As AffichagePersonnaliseGraphique)
        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing

        Dim sRequete As String = "SELECT valeurLegende, valeurLabel, valeurDetachee, valeurExterieure, changementCasValeurFaible FROM dbo.AffichagePersonnaliseGraphique WHERE idAffichagePersonnaliseGraphique=" & oAffichagePersonnaliseGraphique.nIdAffichagePersonnaliseGraphique.ToString
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oAffichagePersonnaliseGraphique.sValeurLegende, readerSQL("valeurLegende"))
                ChargerPropriete(oAffichagePersonnaliseGraphique.sValeurLabel, readerSQL("valeurLabel"))
                ChargerPropriete(oAffichagePersonnaliseGraphique.sValeurDetachee, readerSQL("valeurDetachee"))
                ChargerPropriete(oAffichagePersonnaliseGraphique.sValeurExterieure, readerSQL("valeurExterieure"))
                ChargerPropriete(oAffichagePersonnaliseGraphique.bChangementCasValeurFaible, readerSQL("changementCasValeurFaible"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub recupererSerieParId(ByRef oSerie As Serie, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing

        Dim oClient As New Client
        oClient.chargerClientParId(nIdClient)

        Dim sRequete As String = "SELECT idTypeGraphique, tooltip, legendeTexte, labelSurGraphique, idAffichagePersonnaliseGraphique FROM dbo.Serie WHERE idSerie = " & oSerie.nIdSerie.ToString & " AND idClient = " & nIdClient.ToString
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()

                Dim nIdTypeGraphique As Integer = 0
                ChargerPropriete(nIdTypeGraphique, readerSQL("idTypeGraphique"))

                Dim oTypeGraphique As New TypeGraphique
                oTypeGraphique.nIdTypeGraphique = nIdTypeGraphique
                oTypeGraphique.recupererTypeGraphiqueParId()
                oSerie.oTypeGraphique = oTypeGraphique

                Dim nIdAffichagePersonnaliseGraphique As UShort = 0
                ChargerPropriete(nIdAffichagePersonnaliseGraphique, readerSQL("idAffichagePersonnaliseGraphique"))

                Dim oAffichagePersonnaliseGraphique As New AffichagePersonnaliseGraphique
                oAffichagePersonnaliseGraphique.nIdAffichagePersonnaliseGraphique = nIdAffichagePersonnaliseGraphique
                oAffichagePersonnaliseGraphique.recupererAffichagePersonnaliseGraphique()
                oSerie.oAffichagePersonnaliseGraphique = oAffichagePersonnaliseGraphique

                ChargerPropriete(oSerie.sTooltip, readerSQL("tooltip"))
                ChargerPropriete(oSerie.sLegendeTexte, readerSQL("legendeTexte"))
                ChargerPropriete(oSerie.bLabelSurGraphique, readerSQL("labelSurGraphique"))

                ChargerPropriete(oSerie.oClient, oClient)

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub recupererTypeGraphiqueParId(ByRef oTypeGraphique As TypeGraphique)

        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing

        Dim sRequete As String = "SELECT typeGraphique FROM dbo.TypeGraphique WHERE idTypeGraphique = " & oTypeGraphique.nIdTypeGraphique.ToString
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oTypeGraphique.stypeGraphique, readerSQL("typeGraphique"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Sub

    Public Shared Sub recupererAffichageGraphiqueParId(ByRef oAffichageGraphique As AffichageGraphique, ByVal nIdClient As Integer)

        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing
        Dim oClient As New Client
        oClient.chargerClientParId(nIdClient)


        'RECUPERATION DE TOUS LES AFFICHAGEGRAPHIQUE ET TOUS LES OBJETS EN DECOULANT
        Dim sRequete As String = "SELECT idSerie, idGraphique, idLegende, Abscisse, Ordonnée, FiltrerPointsVides, TitreAbscisse, TitreOrdonnee FROM dbo.AffichageGraphique WHERE idAffichageGraphique = " & oAffichageGraphique.nIdAffichageGraphique.ToString & " AND idClient = " & nIdClient.ToString
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()

                Dim nIdSerie As Integer

                ChargerPropriete(nIdSerie, readerSQL("idSerie"))
                oAffichageGraphique.oSerie.nIdSerie = nIdSerie
                oAffichageGraphique.oSerie.recupererSerieParId(nIdClient)

                Dim nIdGraphique As Integer
                ChargerPropriete(nIdGraphique, readerSQL("idGraphique"))
                oAffichageGraphique.oGraphique.nIdGraphique = nIdGraphique
                oAffichageGraphique.oGraphique.recupererGraphiqueParId()

                Dim nIdLegende As Integer
                ChargerPropriete(nIdLegende, readerSQL("idLegende"))
                oAffichageGraphique.oLegende.nIdLegende = nIdLegende
                oAffichageGraphique.oLegende.recupererLegendeParId()

                ChargerPropriete(oAffichageGraphique.sAbscisse, readerSQL("Abscisse"))
                ChargerPropriete(oAffichageGraphique.sOrdonnee, readerSQL("Ordonnée"))
                ChargerPropriete(oAffichageGraphique.bFiltrePointsVides, readerSQL("FiltrerPointsVides"))
                ChargerPropriete(oAffichageGraphique.sTitreAbscisse, readerSQL("TitreAbscisse"))
                ChargerPropriete(oAffichageGraphique.sTitreOrdonnee, readerSQL("TitreOrdonnee"))
                ChargerPropriete(oAffichageGraphique.oClient, oClient)
                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Sub

    Public Shared Sub recupererListeParamRapportAffichageGraphique(ByRef oParamRapportAffichageGraphiques As ParamRapportAffichageGraphiques, ByRef oRapport As Rapport)

        Dim sql As New cSQL("Administration")
        Dim readerSQL As SqlDataReader = Nothing

        'LISTE DE TOUS LES IDAFFICHAGEGRAPHIQUE
        Dim sRequete As String = "SELECT idAffichageGraphique FROM dbo.ParamRapportAffichageGraphique WHERE (idRapport = " & oRapport.nIdRapport.ToString & ") " &
            "AND (idClient = " & oRapport.oClient.nIdClient.ToString & ") AND (idUtilisateur = " & oRapport.oUtilisateur.nIdUtilisateur.ToString & ") AND (idModule = " & oRapport.oModule.nIdModule.ToString & ")"
        Try
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()

                Dim oParamRapportAffichageGraphique As New ParamRapportAffichageGraphique(oRapport)
                Dim oAffichageGraphique As New AffichageGraphique(CInt(readerSQL("idAffichageGraphique")))
                oAffichageGraphique.recupererAffichageGraphiqueParId(oRapport.oClient.nIdClient)

                oParamRapportAffichageGraphique.oAffichageGraphique = oAffichageGraphique

                oParamRapportAffichageGraphiques.Add(oParamRapportAffichageGraphique)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try


    End Sub

    Public Shared Sub chargerNavigationParOrdre(ByRef oNavigation As Navigation, ByVal sSessionId As String, ByVal iOrdreLien As Integer, ByVal nIdUtilisateur As Integer, ByVal nIdModule As Integer, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "SELECT sessionId,"
            Requete += " idUtilisateur,"
            Requete += " idClient,"
            Requete += " idModule,"
            Requete += " ordreLien,"
            Requete += " idRapport,"
            Requete += " chaineFiltre,"
            Requete += " chaineArianne,"
            Requete += " datelien,"
            Requete += " datelien,"
            Requete += " valeurOngletNiv1Selectionne,"
            Requete += " valeurOngletNiv2Selectionne"
            Requete += " FROM dbo.Navigation"
            Requete += " WHERE sessionId = '" & Toolbox.ReplaceQuote(sSessionId) & "'"
            Requete += " AND idUtilisateur = " & CStr(nIdUtilisateur)
            Requete += " AND idClient = " & CStr(nIdClient)
            Requete += " AND idModule = " & CStr(nIdModule)
            Requete += " AND ordreLien = " & CStr(iOrdreLien)

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oNavigation.sSessionId, readerSQL.Item("sessionId"))
                ChargerPropriete(oNavigation.nIdUtilisateur, readerSQL.Item("idUtilisateur"))
                ChargerPropriete(oNavigation.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oNavigation.nIdModule, readerSQL.Item("idModule"))
                ChargerPropriete(oNavigation.nIdRapport, readerSQL.Item("idRapport"))
                ChargerPropriete(oNavigation.sChaineFiltre, readerSQL.Item("chaineFiltre"))
                ChargerPropriete(oNavigation.sChaineAriane, readerSQL.Item("chaineArianne"))
                ChargerPropriete(oNavigation.sValeurOngletNiv1Selectionne, readerSQL.Item("valeurOngletNiv1Selectionne"))
                ChargerPropriete(oNavigation.sValeurOngletNiv2Selectionne, readerSQL.Item("valeurOngletNiv2Selectionne"))
                ChargerPropriete(oNavigation.nOrdreLien, readerSQL.Item("ordreLien"))
                ChargerPropriete(oNavigation.dDateLien, readerSQL.Item("datelien"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerNavigationParChaineAriane(ByRef oNavigation As Navigation, ByVal sSessionId As String, ByVal sChaineAriane As String, ByVal nIdUtilisateur As Integer, ByVal nIdModule As Integer, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "SELECT sessionId,"
            Requete += " idUtilisateur,"
            Requete += " idClient,"
            Requete += " idModule,"
            Requete += " ordreLien,"
            Requete += " idRapport,"
            Requete += " chaineFiltre,"
            Requete += " chaineArianne,"
            Requete += " valeurOngletNiv1Selectionne,"
            Requete += " valeurOngletNiv2Selectionne,"
            Requete += " datelien"
            Requete += " FROM dbo.Navigation"
            Requete += " WHERE sessionId = '" & Toolbox.ReplaceQuote(sSessionId) & "'"
            Requete += " AND idUtilisateur = " & CStr(nIdUtilisateur)
            Requete += " AND idClient = " & CStr(nIdClient)
            Requete += " AND idModule = " & CStr(nIdModule)
            Requete += " AND chaineArianne ='" & Toolbox.ReplaceQuote(sChaineAriane) & " '"
            Requete += " ORDER BY ordreLien DESC"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oNavigation.sSessionId, readerSQL.Item("sessionId"))
                ChargerPropriete(oNavigation.nIdUtilisateur, readerSQL.Item("idUtilisateur"))
                ChargerPropriete(oNavigation.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oNavigation.nIdModule, readerSQL.Item("idModule"))
                ChargerPropriete(oNavigation.nIdRapport, readerSQL.Item("idRapport"))
                ChargerPropriete(oNavigation.sChaineFiltre, readerSQL.Item("chaineFiltre"))
                ChargerPropriete(oNavigation.sChaineAriane, readerSQL.Item("chaineArianne"))
                ChargerPropriete(oNavigation.sValeurOngletNiv1Selectionne, readerSQL.Item("valeurOngletNiv1Selectionne"))
                ChargerPropriete(oNavigation.sValeurOngletNiv2Selectionne, readerSQL.Item("valeurOngletNiv2Selectionne"))
                ChargerPropriete(oNavigation.nOrdreLien, readerSQL.Item("ordreLien"))
                ChargerPropriete(oNavigation.dDateLien, readerSQL.Item("datelien"))
                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub ecrireNavigationParOrdre(ByVal sSessionId As String, ByVal iOrdreLien As Integer, ByVal nIdUtilisateur As Integer, ByVal nIdModule As Integer, ByVal nIdClient As Integer, ByVal nIdRapport As Integer, ByVal sChaineFiltre As String, ByVal sChaineAriane As String, ByVal sValeurOngletNiv1Selectionne As String, ByVal sValeurOngletNiv2Selectionne As String)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""


        Try
            Requete = "DELETE "
            Requete += " FROM dbo.Navigation"
            Requete += " WHERE sessionId = '" & Toolbox.ReplaceQuote(sSessionId) & "'"
            Requete += " AND idUtilisateur = " & CStr(nIdUtilisateur)
            Requete += " AND idClient = " & CStr(nIdClient)
            Requete += " AND idModule = " & CStr(nIdModule)
            Requete += " AND ordreLien >= " & CStr(iOrdreLien)
            sql.executerRequete(Requete)


            Requete = "INSERT "
            Requete += " INTO Navigation ("
            Requete += " sessionId,"
            Requete += "  idUtilisateur,"
            Requete += "  idClient,"
            Requete += "  idModule,"
            Requete += "  ordreLien,"
            Requete += "  idRapport,"
            Requete += "  chaineFiltre,"
            Requete += "  chaineArianne,"
            Requete += "  valeurOngletNiv1Selectionne,"
            Requete += "  valeurOngletNiv2Selectionne,"
            Requete += "  datelien)"
            Requete += " VALUES ("
            Requete += " '" & Toolbox.ReplaceQuote(sSessionId) & "',"
            Requete += nIdUtilisateur & ","
            Requete += nIdClient & ","
            Requete += nIdModule & ","
            Requete += iOrdreLien & ","
            Requete += nIdRapport & ","
            Requete += " '" & Toolbox.ReplaceQuote(sChaineFiltre) & "',"
            Requete += " '" & Toolbox.ReplaceQuote(sChaineAriane) & "',"
            Requete += " '" & Toolbox.ReplaceQuote(sValeurOngletNiv1Selectionne) & "',"
            Requete += " '" & Toolbox.ReplaceQuote(sValeurOngletNiv2Selectionne) & "',"
            Requete += " CURRENT_TIMESTAMP"
            Requete += ")"
            sql.executerRequete(Requete)


        Catch ex As Exception
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub SupprimerDerniereNavigation(ByVal sSessionId As String, ByVal nIdUtilisateur As Integer, ByVal nIdModule As Integer, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim Max_iOrdreLien As String = ""

        Try

            Requete = "SELECT MAX(ordreLien) AS OrdreLienMax"
            Requete += " FROM dbo.Navigation"
            Requete += " WHERE idUtilisateur = " + CStr(nIdUtilisateur)
            Requete += " AND idClient =  " + CStr(nIdClient)
            Requete += " AND idModule = " + CStr(nIdModule)
            Requete += " AND sessionId = '" + Toolbox.ReplaceQuote(sSessionId) + "'"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(Max_iOrdreLien, readerSQL.Item("OrdreLienMax"))
            Loop
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If

            If Max_iOrdreLien <> "" Then
                Requete = "Delete "
                Requete += " FROM dbo.Navigation"
                Requete += " WHERE sessionId = '" & Toolbox.ReplaceQuote(sSessionId) & "'"
                Requete += " AND idUtilisateur = " & CStr(nIdUtilisateur)
                Requete += " AND idClient = " & CStr(nIdClient)
                Requete += " AND idModule = " & CStr(nIdModule)
                Requete += " AND ordreLien = " & CStr(Max_iOrdreLien)
                sql.executerRequete(Requete)
            End If
        Catch ex As Exception
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub chargerPremierRapportRedirection(ByRef oRapport As Rapport, ByVal idModule As Integer, ByVal idUtilisateur As Integer, ByVal idRapport As Integer, ByVal nIdClient As Integer)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try

            Requete = "SELECT TOP (1)  dbo.Rapport.*"
            Requete &= " FROM dbo.Rapport INNER JOIN"
            Requete &= " dbo.Rapport AS Rapport_1 ON dbo.Rapport.idClient = Rapport_1.idClient AND dbo.Rapport.idUtilisateur = Rapport_1.idUtilisateur AND "
            Requete &= " dbo.Rapport.idModule = Rapport_1.idModule And"
            Requete &= " dbo.Rapport.tableRapport = Rapport_1.tableRapport"
            Requete &= " WHERE dbo.Rapport.idUtilisateur = " & idUtilisateur & " AND dbo.Rapport.idClient = " & nIdClient.ToString & " AND dbo.Rapport.idModule = " & idModule & " AND Rapport_1.idRapport = " & idRapport & " AND dbo.Rapport.estActif = 1 AND "
            Requete &= " dbo.Rapport.estVisible = 1"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim oClient As New Client()
                Dim oModule As New Modul()
                Dim oUtilisateur As New Utilisateur()

                oClient.chargerClientParId(CInt(readerSQL.Item("idClient")))
                oModule.chargerModulesParId(CInt(readerSQL.Item("idModule")), oClient)
                oUtilisateur.chargerUtilisateurParId(CInt(readerSQL.Item("idUtilisateur")), oClient.nIdClient)

                ChargerPropriete(oRapport.oClient, oClient)
                ChargerPropriete(oRapport.oModule, oModule)
                ChargerPropriete(oRapport.oUtilisateur, oUtilisateur)
                ChargerPropriete(oRapport.nIdRapport, readerSQL.Item("idRapport"))
                ChargerPropriete(oRapport.sPageRapport, readerSQL.Item("pageRapport"))
                ChargerPropriete(oRapport.sTableRapport, readerSQL.Item("tableRapport"))
                ChargerPropriete(oRapport.sSommaireNiv1, readerSQL.Item("sommaireNiv1"))
                ChargerPropriete(oRapport.sSommaireNiv2, readerSQL.Item("sommaireNiv2"))
                ChargerPropriete(oRapport.nSommaireOrdreNiv1, readerSQL.Item("sommaireOrdreNiv1"))
                ChargerPropriete(oRapport.nSommaireOrdreNiv2, readerSQL.Item("sommaireOrdreNiv2"))
                ChargerPropriete(oRapport.sOngletNiv1, readerSQL.Item("ongletNiv1"))
                ChargerPropriete(oRapport.sOngletNiv2, readerSQL.Item("ongletNiv2"))
                ChargerPropriete(oRapport.nOngletOrdreNiv1, readerSQL.Item("ongletOrdreNiv1"))
                ChargerPropriete(oRapport.nOngletOrdreNiv2, readerSQL.Item("ongletOrdreNiv2"))
                ChargerPropriete(oRapport.nOngletOrdreRapport, readerSQL.Item("ongletOrdreRapport"))
                ChargerPropriete(oRapport.ntailleBoite, readerSQL.Item("tailleBoite"))
                ChargerPropriete(oRapport.sTriDonnees, readerSQL.Item("triDonnees"))
                ChargerPropriete(oRapport.bEstActif, readerSQL.Item("estActif"))
                ChargerPropriete(oRapport.bEstVisible, readerSQL.Item("estVisible"))
                ChargerPropriete(oRapport.bEstFiltreBoite, readerSQL.Item("estFiltreBoite"))
                ChargerPropriete(oRapport.bAfficherTotal, readerSQL.Item("afficherTotal"))
                ChargerPropriete(oRapport.snotificationRequete, readerSQL.Item("notificationRequete"))
                ChargerPropriete(oRapport.bNotificationNiveau1, readerSQL.Item("notificationNiveau1"))
                ChargerPropriete(oRapport.bNotificationNiveau2, readerSQL.Item("notificationNiveau2"))
                ChargerPropriete(oRapport.sTypeTotal, readerSQL.Item("typeTotal"))
                ChargerPropriete(oRapport.sTitreTotal, readerSQL.Item("titreTotal"))
                ChargerPropriete(oRapport.bEstBoiteOutilOuverte, readerSQL.Item("estBoiteOutilOuverte"))
                ChargerPropriete(oRapport.bAfficherPagination, readerSQL.Item("afficherPagination"))
                ChargerPropriete(oRapport.bAfficherNombreEnregistrements, readerSQL.Item("afficherNombreEnregistrements"))
                ChargerPropriete(oRapport.bAfficherRecherche, readerSQL.Item("afficherRecherche"))
                ChargerPropriete(oRapport.bAfficherSeuil, readerSQL.Item("afficherSeuil"))
                ChargerPropriete(oRapport.sOutilTop, readerSQL.Item("outilTop"))
                ChargerPropriete(oRapport.bAutorisationAjout, readerSQL.Item("autorisationAjout"))
                ChargerPropriete(oRapport.bAutorisationModification, readerSQL.Item("autorisationModification"))
                ChargerPropriete(oRapport.sTestInsertion, readerSQL.Item("testInsertion"))
                ChargerPropriete(oRapport.sTestModification, readerSQL.Item("testModification"))
                ChargerPropriete(oRapport.sTableAction, readerSQL.Item("tableAction"))
                ChargerPropriete(oRapport.bAutorisationSuppression, readerSQL.Item("autorisationSuppression"))
                ChargerPropriete(oRapport.bAutoriserFusionColonnes, readerSQL.Item("autoriserFusionColonnes"))
                ChargerPropriete(oRapport.bAfficheDonneesApresRecherche, readerSQL.Item("afficheDonneesApresRecherche"))
                ChargerPropriete(oRapport.bAfficherCaseMoisGlissant, readerSQL.Item("afficherCaseMoisGlissant"))
                ChargerPropriete(oRapport.bAfficherEnMoisGlissant, readerSQL.Item("afficherEnMoisGlissant"))
                ChargerPropriete(oRapport.bAfficherCaseTTC, readerSQL.Item("afficherCaseTTC"))
                ChargerPropriete(oRapport.bAfficherEnTTC, readerSQL.Item("afficherEnTTC"))
                ChargerPropriete(oRapport.bAfficherHtTtcDansLibelleTotal, readerSQL.Item("afficherHtTtcDansLibelleTotal"))
                ChargerPropriete(oRapport.AfficherCaseDecimale, readerSQL.Item("afficherCaseDecimale"))
                ChargerPropriete(oRapport.bAfficherAvecDecimales, readerSQL.Item("afficherAvecDecimales"))
                ChargerPropriete(oRapport.bAfficherCaseNumComplet, readerSQL.Item("afficherCaseNumComplet"))
                ChargerPropriete(oRapport.bAfficherNumeroEntier, readerSQL.Item("afficherNumeroEntier"))
                ChargerPropriete(oRapport.bAfficherExport, readerSQL.Item("afficherExport"))
                ChargerPropriete(oRapport.sNomFichierExport, readerSQL.Item("nomFichierExport"))
                ChargerPropriete(oRapport.sColonneLiaisonMoisTva, readerSQL.Item("colonneLiaisonMoisTva"))
                ChargerPropriete(oRapport.sObjetMailing, readerSQL.Item("objetMailing"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub effacerNavigation()
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""


        Try
            Requete = "DELETE "
            Requete += " FROM dbo.Navigation"
            Requete += " WHERE DATEPART(hour, CURRENT_TIMESTAMP - datelien) * 60 * 60 + DATEPART(minute, CURRENT_TIMESTAMP - datelien) * 60 + DATEPART(second, CURRENT_TIMESTAMP - datelien) > 20 * 60"
            sql.executerRequete(Requete)

        Catch ex As Exception
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub


    Public Shared Function TesterFiltrePresentRapport(ByVal nIdFiltre As Integer, ByVal oRapport As Rapport, ByVal oRapportsTraitement As Rapports, ByVal RestreidreIdRapport As Boolean) As Integer
        Dim sql As New cSQL("Administration")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim Resultat As Integer = 0
        Dim RequeteWhereidRapport As String = ""

        For Each OrapportTraitement In oRapportsTraitement
            If oRapport.nOngletOrdreNiv1 = OrapportTraitement.nOngletOrdreNiv1 And oRapport.nOngletOrdreNiv2 = OrapportTraitement.nOngletOrdreNiv2 And oRapport.nSommaireOrdreNiv1 = OrapportTraitement.nSommaireOrdreNiv1 And oRapport.nSommaireOrdreNiv2 = OrapportTraitement.nSommaireOrdreNiv2 Then
                RequeteWhereidRapport &= " idRapport = " & CStr(OrapportTraitement.nIdRapport) & " OR "
            End If
        Next
        If RequeteWhereidRapport <> "" Then RequeteWhereidRapport = RequeteWhereidRapport.Remove(RequeteWhereidRapport.Length - 4, 4)

        Try

            Requete &= " SELECT idFiltre, idModule, idRapport, idClient, idUtilisateur"
            Requete &= " FROM dbo.ParamRapportFiltre"
            Requete &= " WHERE idFiltre = " & CStr(nIdFiltre) & " AND idModule = " & CStr(oRapport.oModule.nIdModule) & " AND"
            Requete &= " idClient = " & CStr(oRapport.oClient.nIdClient) & " AND idUtilisateur = " & CStr(oRapport.oUtilisateur.nIdUtilisateur)

            If RestreidreIdRapport = False Then
                Requete &= " AND (" & RequeteWhereidRapport & ")"
            Else
                Requete &= " AND idRapport = " & CStr(oRapport.nIdRapport)
            End If


            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Resultat = CInt(readerSQL.Item("idRapport").ToString)
            Loop

            Return Resultat
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Function

    Public Shared Sub RecupererListeValeursTable(ByRef oControle As DropDownList, ByVal sNomBase As String, ByVal sNomTable As String, ByVal sNomChamp As String, ByVal sFiltre As String)
        Dim sql As New cSQL(sNomBase)
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try

            Dim ListeChamps() As String = sNomChamp.Split(CChar(";"))


            Requete &= " SELECT "
            For Each sTexte In ListeChamps
                Requete &= " [" & sTexte & "],"
            Next
            Requete = Requete.Remove(Requete.Length - 1, 1)
            Requete &= " FROM dbo.[" & sNomTable & "]"
            Requete &= sFiltre

            Dim oDataTable As New DataTable
            oDataTable.Columns.Add("id")
            oDataTable.Columns.Add("valeur")

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Select Case ListeChamps.Count
                    Case 1
                        oDataTable.Rows.Add(readerSQL.Item(0).ToString, readerSQL.Item(0).ToString)
                    Case 2
                        oDataTable.Rows.Add(readerSQL.Item(0).ToString, readerSQL.Item(1).ToString)
                End Select

                'sListeValeurs.Add(readerSQL.Item(0).ToString)
            Loop

            oControle.DataSource = oDataTable
            oControle.DataValueField = "id"
            oControle.DataTextField = "valeur"
            oControle.DataBind()


        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub insertionValeurs(ByVal sBase As String, ByVal sTable As String, ByVal sListeColonnesInsert As String, ByVal sListeValeursInsert As String)
        Dim sql As New cSQL(sBase)

        Dim sTypeColonnes As New List(Of String)

        Dim sColonnes As String = ""
        For Each sTexte As String In sListeColonnesInsert.Split(CChar("|"))
            sColonnes += "[" + sTexte + "],"
            sTypeColonnes.Add(cDal.ChargerTypeColonne(sBase, sTable, sTexte))
        Next
        If sColonnes <> "" Then sColonnes = sColonnes.Remove(sColonnes.Length - 1, 1)

        Dim sValeurs As String = ""
        Dim i As Integer = 0
        For Each sTexte As String In sListeValeursInsert.Split(CChar("|"))
            Select Case sTypeColonnes(i)
                Case "numerique"
                    sValeurs += sTexte.Replace(",", ".") + ","
                Case "texte", "date"
                    sValeurs += "'" + Toolbox.ReplaceQuote(sTexte) + "',"
                Case "booleen"
                    sValeurs += "'" + Toolbox.ReplaceBooleenSQL(sTexte) + "',"
            End Select

            i += 1
        Next
        If sValeurs <> "" Then sValeurs = sValeurs.Remove(sValeurs.Length - 1, 1)

        Dim requete As String = "INSERT INTO [" & sBase & "].[dbo].[" & sTable & "] (" + sColonnes + ") VALUES (" + sValeurs + ")"
        Try
            sql.executerRequete(requete)
        Catch ex As Exception
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub editionValeurs(ByVal sBase As String, ByVal sTable As String, ByVal sListeColonnesInsert As String, ByVal sListeValeursInsert As String, ByVal sCritere As String)
        Dim sql As New cSQL(sBase)

        Dim sTypeColonnes As New List(Of String)
        Dim i As Integer = 0

        Dim sColonnes() As String = sListeColonnesInsert.Split(CChar("|"))
        Dim sValeurs() As String = sListeValeursInsert.Split(CChar("|"))
        For Each sTexte As String In sColonnes
            sTypeColonnes.Add(cDal.ChargerTypeColonne(sBase, sTable, sTexte))
        Next

        Dim requete As String = "UPDATE [" & sBase & "].[dbo].[" & sTable & "] SET "
        For i = 0 To sColonnes.Length - 1
            requete += " [" + sColonnes(i) + "]="
            Select Case sTypeColonnes(i)
                Case "numerique"
                    requete += sValeurs(i).Replace(",", ".") + ","
                Case "texte", "date"
                    requete += "'" + Toolbox.ReplaceQuote(sValeurs(i)) + "',"
                Case "booleen"
                    requete += "'" + Toolbox.ReplaceBooleenSQL(sValeurs(i)) + "',"
            End Select
        Next
        If i > 0 Then requete = requete.Remove(requete.Length - 1, 1)
        If sCritere <> "" Then
            requete += " WHERE " & sCritere
            sql.executerRequete(requete)
        End If

        Try

        Catch ex As Exception
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Function testerModification(ByVal sBase As String, ByVal sRequeteTest As String, ByVal sListeColonnesInsert As String, ByVal sListeValeursInsert As String, ByVal scritere As String) As Boolean
        Dim sql As New cSQL(sBase)

        Dim i As Integer = 0
        Dim Requete As String = sRequeteTest.Split(CChar("|"))(0)
        Dim sListeCriteres() As String = sRequeteTest.Split(CChar("|"))(1).Split(CChar(";"))
        Dim sColonnes() As String = sListeColonnesInsert.Split(CChar("|"))
        Dim sValeurs() As String = sListeValeursInsert.Split(CChar("|"))
        For Each sTexte In sListeCriteres
            i = 0
            For Each sColonne In sColonnes
                If sTexte.Replace("@", "") = sColonne Then
                    Requete = Requete.Replace(sTexte, Toolbox.ReplaceQuote(sValeurs(i)))
                End If
                i += 1
            Next
        Next
        If scritere <> "" Then
            If Requete.ToUpper.Contains("WHERE") Then
                Requete += " AND ( NOT " + scritere + ")"
            Else
                Requete += " WHERE Not " + scritere
            End If
        End If
        Dim readerSQL As SqlDataReader = Nothing
        Try
            readerSQL = sql.chargerDataReader(Requete)
            Return readerSQL.HasRows
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try


    End Function




    Public Shared Sub CloturerTicket(ByVal sBase As String, ByVal sTable As String, ByVal sColonne As String, ByVal sCritere As String)
        Dim sql As New cSQL(sBase)
        Dim requete As String = "UPDATE [" & sBase & "].[dbo].[" & sTable & "] SET [" + sColonne + "] =sysdatetime() WHERE " + sCritere
        Try
            sql.executerRequete(requete)
        Catch ex As Exception
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub SupprimerTicket(ByVal sBase As String, ByVal sTable As String, ByVal sTableDetail As String, ByVal sCritere As String)
        Dim sql As New cSQL(sBase)

        Try
            Dim requete As String = "UPDATE [" & sBase & "].[dbo].[" & sTable & "] SET [Supprime] ='True' WHERE " + sCritere
            sql.executerRequete(requete)
            requete = "UPDATE [" & sBase & "].[dbo].[" & sTableDetail & "] SET [Supprime] ='True' WHERE " + sCritere
            sql.executerRequete(requete)
        Catch ex As Exception
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub SupprimerTicketDetail(ByVal sBase As String, ByVal sTableDetail As String, ByVal sCritere As String)
        Dim sql As New cSQL(sBase)
        Try
            Dim requete As String = "UPDATE [" & sBase & "].[dbo].[" & sTableDetail & "] SET [Supprime] ='True' WHERE " + sCritere
            sql.executerRequete(requete)
        Catch ex As Exception
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Function SupprimerParc(ByVal sBase As String, ByVal sTable As String, ByVal sCritere As String, Optional TableTest As String = "") As Boolean

        Dim sql As New cSQL(sBase)
        Dim Requete As String = ""
        Dim Autorisation_Suppression As Boolean = True
        Dim readerSQL As SqlDataReader = Nothing
        Try
            If TableTest <> "" Then



                Requete = "SELECT * from [" & TableTest & "]" & " Where " & sCritere
                readerSQL = sql.chargerDataReader(Requete)
                If readerSQL.HasRows = True Then Autorisation_Suppression = False
                If Not readerSQL Is Nothing Then
                    sql.libererDataReader(readerSQL)
                End If

                If Autorisation_Suppression = True Then
                    Requete = "UPDATE [" & sBase & "].[dbo].[" & sTable & "] SET [estActif] = 0, dateDesactivation=getdate()  WHERE " + sCritere
                    sql.executerRequete(Requete)
                    Return True
                Else
                    Return False
                End If
            Else
                Requete = "UPDATE [" & sBase & "].[dbo].[" & sTable & "] SET [estActif] = 0, dateDesactivation=getdate()  WHERE " + sCritere
                sql.executerRequete(Requete)
                Return True
            End If



        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
        End Try

    End Function

    Public Shared Sub chargerArticleParId(ByRef oArticle As Article, ByVal idArticle As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oTypeMateriel As TypeMateriel
        Dim oMarque As Marque
        Try
            Requete = "SELECT idTypeMateriel, idMarque, modele, prix1, prix2, reference, estEnrolable"
            Requete &= " FROM dbo.Catalogue"
            Requete &= " WHERE idArticle = " & idArticle.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oTypeMateriel = New TypeMateriel
                oMarque = New Marque
                ChargerPropriete(oArticle.nIdArticle, idArticle)
                oTypeMateriel.chargerTypeMaterielParId(CInt(readerSQL.Item("idTypeMateriel")))
                ChargerPropriete(oArticle.oTypeMateriel, oTypeMateriel)
                oMarque.chargerMarqueParId(CInt(readerSQL.Item("idMarque")))
                ChargerPropriete(oArticle.oMarque, oMarque)
                ChargerPropriete(oArticle.sModele, readerSQL.Item("modele"))
                ChargerPropriete(oArticle.nPrix1, readerSQL.Item("prix1"))
                ChargerPropriete(oArticle.nPrix2, readerSQL.Item("prix2"))
                ChargerPropriete(oArticle.sReference, readerSQL.Item("reference"))
                ChargerPropriete(oArticle.bEstEnrolable, readerSQL.Item("estEnrolable"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerStockParId(ByRef oStock As Stock, ByVal idArticleStock As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oMailings As New Mailings
        Dim oArticle As New Article
        Try
            Requete = "SELECT idArticleStock, codeEmei, numSerie, codeReference, codeDesimlockage1, etiquetteInventaire, estDesimlocke, codePin1, codePuk1, codePuk2, codeVerrouillage, estEnrole, etat, "
            Requete &= " dateRentree, dateSortie, prix, dernierDetenteur, idArticle, idClient"
            Requete &= " FROM dbo.Stock"
            Requete &= " WHERE idArticleStock = " & idArticleStock.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oArticle.chargerArticleParId(CInt(readerSQL.Item("idArticle").ToString))
                ChargerPropriete(oStock.oArticle, oArticle)
                ChargerPropriete(oStock.nIdArticleStock, readerSQL.Item("idArticleStock"))
                ChargerPropriete(oStock.sCodeReference, readerSQL.Item("codeReference"))
                ChargerPropriete(oStock.sCodeEmei, readerSQL.Item("codeEmei"))
                ChargerPropriete(oStock.sNumSerie, readerSQL.Item("numSerie"))
                ChargerPropriete(oStock.sCodeDesimlockage1, readerSQL.Item("codeDesimlockage1"))
                ChargerPropriete(oStock.sEtiquetteInventaire, readerSQL.Item("etiquetteInventaire"))
                ChargerPropriete(oStock.bEstDesimlocke, readerSQL.Item("estDesimlocke"))
                ChargerPropriete(oStock.sCodePin1, readerSQL.Item("codePin1"))
                ChargerPropriete(oStock.sCodePuk1, readerSQL.Item("codePuk1"))
                ChargerPropriete(oStock.sCodePuk2, readerSQL.Item("codePuk2"))
                ChargerPropriete(oStock.sCodeVerrouillage, readerSQL.Item("codeVerrouillage"))
                ChargerPropriete(oStock.bEstEnrole, readerSQL.Item("estEnrole"))
                ChargerPropriete(oStock.nPrix, readerSQL.Item("prix"))
                ChargerPropriete(oStock.sEtat, readerSQL.Item("etat"))
                ChargerPropriete(oStock.dDateRentree, readerSQL.Item("dateRentree"))
                ChargerPropriete(oStock.dDateSortie, readerSQL.Item("dateSortie"))
                ChargerPropriete(oStock.sDernierDetenteur, readerSQL.Item("dernierDetenteur"))
                ChargerPropriete(oStock.nIdClient, readerSQL.Item("idClient"))

                ChargerPropriete(oStock.sTypeLiaison, cDal.RecupererTypeLiaisonStock(CInt(readerSQL.Item("idArticleStock").ToString)))
                ChargerPropriete(oStock.dDateAttribution, cDal.RecupererDateAttributionStock(CInt(readerSQL.Item("idArticleStock").ToString)))

            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Function RecupererTypeLiaisonStock(ByVal nIdArticleStock As Integer) As String
        Dim sResultat As String = ""

        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "SELECT typeLiaison FROM LiaisonStock WHERE idArticleStock = " + nIdArticleStock.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                sResultat = readerSQL.Item("typeLiaison").ToString
            Loop
            Return sResultat
        Catch ex As Exception
            Throw ex
            Return ""
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing

        End Try
    End Function

    Public Shared Function RecupererDateAttributionStock(ByVal nIdArticleStock As Integer) As Date
        Dim sResultat As Date

        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "SELECT dateAttribution FROM LiaisonStock WHERE idArticleStock = " + nIdArticleStock.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                sResultat = CDate(readerSQL.Item("dateAttribution"))
            Loop
            Return sResultat
        Catch ex As Exception
            Throw ex
            Return Nothing
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing

        End Try
    End Function

    Public Shared Sub RecupererLangueClient(ByRef oClient As Client)
        Dim sql As New cSQL("Administration")
        Dim Requete As String = "SELECT langueClient FROM Client WHERE idClient=@nIdClient"
        Dim readerSQL As SqlDataReader = Nothing
        Dim cmd As New SqlCommand(Requete, sql.connexion)
        Try
            cmd.Parameters.Add("@nIdClient", SqlDbType.Int).Value = oClient.nIdClient
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                ChargerPropriete(oClient.sLangueClient, readerSQL.Item("langueClient"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub


    Public Shared Sub chargerArticles(ByRef oArticles As Articles, ByVal idClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oArticle As Article
        Dim oTypeMateriel As TypeMateriel
        Dim oMarque As Marque
        Try
            Requete = "SELECT idArticle, idTypeMateriel, idMarque, modele, prix1, prix2, reference, estEnrolable"
            Requete &= " FROM dbo.Catalogue"
            Requete &= " WHERE  idClient = " & idClient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oArticle = New Article
                oTypeMateriel = New TypeMateriel
                oMarque = New Marque
                ChargerPropriete(oArticle.nIdArticle, readerSQL.Item("idArticle"))
                oTypeMateriel.chargerTypeMaterielParId(CInt(readerSQL.Item("idTypeMateriel")))
                ChargerPropriete(oArticle.oTypeMateriel, oTypeMateriel)
                oMarque.chargerMarqueParId(CInt(readerSQL.Item("idMarque")))
                ChargerPropriete(oArticle.oMarque, oMarque)
                ChargerPropriete(oArticle.sModele, readerSQL.Item("modele"))
                ChargerPropriete(oArticle.nPrix1, readerSQL.Item("prix1"))
                ChargerPropriete(oArticle.nPrix2, readerSQL.Item("prix2"))
                ChargerPropriete(oArticle.sReference, readerSQL.Item("reference"))
                ChargerPropriete(oArticle.bEstEnrolable, readerSQL.Item("estEnrolable"))
                oArticles.Add(oArticle)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerFonctions(ByRef oFonctionss As Fonctionss, ByVal idClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oFonction As Fonction

        Try
            Requete = "SELECT idFonction, libelle, idClient"
            Requete &= " FROM dbo.Fonction"
            Requete &= " WHERE  idClient = " & idClient.ToString & " AND estActif = 1"
            Requete &= " ORDER BY libelle"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oFonction = New Fonction
                ChargerPropriete(oFonction.nIdFonction, readerSQL.Item("idFonction"))
                ChargerPropriete(oFonction.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oFonction.sLibelle, readerSQL.Item("libelle"))

                oFonctionss.Add(oFonction)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerCompteFacturants(ByRef oCompteFacturants As CompteFacturants, ByVal idClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oCompteFacturant As CompteFacturant

        Try
            Requete = "SELECT idCompteFacturant, libelle, idClient"
            Requete &= " FROM dbo.CompteFacturant"
            Requete &= " WHERE  idClient = " & idClient.ToString & " AND estActif = 1"
            Requete &= " ORDER BY libelle"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oCompteFacturant = New CompteFacturant
                ChargerPropriete(oCompteFacturant.nIdCompteFacturant, readerSQL.Item("idCompteFacturant"))
                ChargerPropriete(oCompteFacturant.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oCompteFacturant.sLibelle, readerSQL.Item("libelle"))

                oCompteFacturants.Add(oCompteFacturant)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerUsagesFixe(ByRef oUsagesFixe As UsagesFixe, ByRef oClient As Client)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oUsageFixe As UsageFixe

        Try
            Requete = "SELECT idUsage, libelleUsage,typeUsage, idClient"
            Requete &= " FROM dbo.Usage"
            Requete &= " WHERE  idClient = " & oClient.nIdClient.ToString & " AND typeUsage='Téléphonie fixe' and libelleUsage<>'En attente affectation'"
            Requete &= " ORDER BY libelleUsage ,typeUsage"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oUsageFixe = New UsageFixe
                oUsageFixe.oClient = New Client()

                ChargerPropriete(oUsageFixe.nIdUsage, readerSQL.Item("idUsage"))
                ChargerPropriete(oUsageFixe.oClient.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oUsageFixe.sLibelleUsage, readerSQL.Item("libelleUsage"))
                ChargerPropriete(oUsageFixe.sTypeUsage, readerSQL.Item("typeUsage"))

                oUsagesFixe.Add(oUsageFixe)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerTypesLignesFixe(ByRef oTypesLignesFixe As TypesLignesFixe, ByRef oClient As Client)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oTypeLigneFixe As TypeLigneFixe

        Try
            Requete = "SELECT idTypeLigne, libelleTypeLigne, idClient"
            Requete &= " FROM dbo.TypeLigne"
            Requete &= " WHERE  idClient = " & oClient.nIdClient.ToString
            Requete &= " ORDER BY libelleTypeLigne "

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oTypeLigneFixe = New TypeLigneFixe
                oTypeLigneFixe.oClient = oClient

                ChargerPropriete(oTypeLigneFixe.nIdTypeLigne, readerSQL.Item("idTypeLigne"))
                ChargerPropriete(oTypeLigneFixe.oClient.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oTypeLigneFixe.sLibelleTypeLigne, readerSQL.Item("libelleTypeLigne"))

                oTypesLignesFixe.Add(oTypeLigneFixe)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerCompteFacturantsFixe(ByRef oCompteFacturantsFixe As CompteFacturantsFixe, ByRef oClient As Client)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oCompteFacturantFixe As CompteFacturantFixe

        Try
            Requete = "SELECT idCompteFacturant, libelleCompteFacturant, libelleCentreFrais, idClient"
            Requete &= " FROM dbo.CompteFacturant"
            Requete &= " WHERE  idClient = " & oClient.nIdClient.ToString & " AND estActif = 1"
            Requete &= " ORDER BY libelleCompteFacturant"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oCompteFacturantFixe = New CompteFacturantFixe
                ChargerPropriete(oCompteFacturantFixe.nIdCompteFacturant, readerSQL.Item("idCompteFacturant"))
                ChargerPropriete(oCompteFacturantFixe.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oCompteFacturantFixe.sLibelleCompteFacturant, readerSQL.Item("libelleCompteFacturant"))
                ChargerPropriete(oCompteFacturantFixe.sLibelleCentreFrais, readerSQL.Item("libelleCentreFrais"))

                oCompteFacturantsFixe.Add(oCompteFacturantFixe)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerContratFixesParTypeLigne(ByRef oContratFixes As ContratFixes, ByRef oTypeLigneFixe As TypeLigneFixe)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        oContratFixes.Clear()
        Try
            sRequete = "SELECT idContrat, libelleContrat FROM dbo.Contrat WHERE (idTypeLigne = " & oTypeLigneFixe.nIdTypeLigne.ToString & ") AND (idClient = " & oTypeLigneFixe.oClient.nIdClient.ToString & ") Order By libelleContrat"
            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                Dim oContratFixe As New ContratFixe()
                ChargerPropriete(oContratFixe.nIdContrat, readerSQL.Item("idContrat"))
                ChargerPropriete(oContratFixe.sLibelleContrat, readerSQL.Item("libelleContrat"))
                oContratFixe.oTypeLigneFixe = oTypeLigneFixe
                oContratFixe.oClient = oTypeLigneFixe.oClient

                oContratFixes.Add(oContratFixe)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerChoixListeFixeParId(ByRef oChoixListeFixe As ChoixListeFixe)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT libelleChoixListe, idClient FROM dbo.ChoixListe WHERE idChoixListe=@idChoixListeFixe"
        Dim cmd As New SqlCommand(sRequete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            cmd.Parameters.Add("@idChoixListeFixe", SqlDbType.Int).Value = oChoixListeFixe.nIdChoixListe
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                oChoixListeFixe.oClient = New Client()

                ChargerPropriete(oChoixListeFixe.sLibelleChoixListe, readerSQL.Item("libelleChoixListe"))
                ChargerPropriete(oChoixListeFixe.oClient.nIdClient, readerSQL.Item("idClient"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerContratFixeParId(ByRef oContratFixe As ContratFixe)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT idTypeLigne, libelleContrat, idClient FROM dbo.Contrat WHERE idContrat=@idContratFixe"
        Dim cmd As New SqlCommand(sRequete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            cmd.Parameters.Add("@idContratFixe", SqlDbType.Int).Value = oContratFixe.nIdContrat
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                oContratFixe.oClient = New Client()
                oContratFixe.oTypeLigneFixe = New TypeLigneFixe()

                ChargerPropriete(oContratFixe.oTypeLigneFixe.nIdTypeLigne, readerSQL.Item("idTypeLigne"))
                ChargerPropriete(oContratFixe.sLibelleContrat, readerSQL.Item("libelleContrat"))
                ChargerPropriete(oContratFixe.oClient.nIdClient, readerSQL.Item("idClient"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerContratFixeParId(ByRef oContratFixe As ContratFixe, ByVal nIdClient As Integer)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT idContrat, idTypeLigne, libelleContrat, idClient FROM dbo.Contrat WHERE libelleContrat=@libelleContratFixe and idClient=@idClient"
        Dim cmd As New SqlCommand(sRequete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            cmd.Parameters.Add("@idClient", SqlDbType.Int).Value = nIdClient
            cmd.Parameters.Add("@libelleContratFixe", SqlDbType.VarChar, 100).Value = "Forfait Internet Pro"
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                oContratFixe.oClient = New Client()
                oContratFixe.oTypeLigneFixe = New TypeLigneFixe()

                ChargerPropriete(oContratFixe.oTypeLigneFixe.nIdTypeLigne, readerSQL.Item("idTypeLigne"))
                ChargerPropriete(oContratFixe.nIdContrat, readerSQL.Item("idContrat"))
                ChargerPropriete(oContratFixe.sLibelleContrat, readerSQL.Item("libelleContrat"))
                ChargerPropriete(oContratFixe.oClient.nIdClient, readerSQL.Item("idClient"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub



    Public Shared Sub chargerTypeSelectionFixeParId(ByRef oTypeSelectionFixe As TypeSelectionFixe)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT libelleTypeSelection, idClient FROM dbo.TypeSelection WHERE idTypeSelection=@idTypeSelectionFixe"
        Dim cmd As New SqlCommand(sRequete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            cmd.Parameters.Add("@idTypeSelectionFixe", SqlDbType.Int).Value = oTypeSelectionFixe.nIdTypeSelection
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                oTypeSelectionFixe.oClient = New Client()

                ChargerPropriete(oTypeSelectionFixe.sLibelleTypeSelection, readerSQL.Item("libelleTypeSelection"))
                ChargerPropriete(oTypeSelectionFixe.oClient.nIdClient, readerSQL.Item("idClient"))
                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerTypeMailParId(ByRef oTypeMail As TypeMail)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT libelleTypeMail, idClient FROM dbo.TypeMail WHERE idTypeMail=@idTypeMail"
        Dim cmd As New SqlCommand(sRequete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            cmd.Parameters.Add("@idTypeMail", SqlDbType.Int).Value = oTypeMail.nIdTypeMail
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                oTypeMail.oClient = New Client()

                ChargerPropriete(oTypeMail.sLibelleTypeMail, readerSQL.Item("libelleTypeMail"))
                ChargerPropriete(oTypeMail.oClient.nIdClient, readerSQL.Item("idClient"))
                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub




    Public Shared Sub chargerChoixListeFixes(ByRef oChoixListeFixes As ChoixListeFixes, ByRef oClient As Client)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT idChoixListe, libelleChoixListe FROM dbo.ChoixListe WHERE (idClient = " & oClient.nIdClient.ToString & ")"
        Dim readerSQL As SqlDataReader = Nothing
        oChoixListeFixes.Clear()
        Try

            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                Dim oChoixListeFixe As New ChoixListeFixe()
                ChargerPropriete(oChoixListeFixe.nIdChoixListe, readerSQL.Item("idChoixListe"))
                ChargerPropriete(oChoixListeFixe.sLibelleChoixListe, readerSQL.Item("libelleChoixListe"))
                oChoixListeFixe.oClient = oClient

                oChoixListeFixes.Add(oChoixListeFixe)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerTypeSelectionFixes(ByRef oTypeSelectionFixes As TypeSelectionFixes, ByRef oClient As Client)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT idTypeSelection, libelleTypeSelection FROM dbo.TypeSelection WHERE (idClient = " & oClient.nIdClient.ToString & ")"
        Dim readerSQL As SqlDataReader = Nothing
        oTypeSelectionFixes.Clear()
        Try

            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                Dim oTypeSelectionFixe As New TypeSelectionFixe()
                ChargerPropriete(oTypeSelectionFixe.nIdTypeSelection, readerSQL.Item("idTypeSelection"))
                ChargerPropriete(oTypeSelectionFixe.sLibelleTypeSelection, readerSQL.Item("libelleTypeSelection"))
                oTypeSelectionFixe.oClient = oClient

                oTypeSelectionFixes.Add(oTypeSelectionFixe)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerTypeMails(ByRef oTypeMails As TypeMails, ByRef oClient As Client)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT idTypeMail, libelleTypeMail FROM dbo.TypeMail WHERE (idClient = " & oClient.nIdClient.ToString & ")"
        Dim readerSQL As SqlDataReader = Nothing
        oTypeMails.Clear()
        Try

            readerSQL = sql.chargerDataReader(sRequete)
            Do Until Not readerSQL.Read()
                Dim oTypeMail As New TypeMail()
                ChargerPropriete(oTypeMail.nIdTypeMail, readerSQL.Item("idTypeMail"))
                ChargerPropriete(oTypeMail.sLibelleTypeMail, readerSQL.Item("libelleTypeMail"))
                oTypeMail.oClient = oClient

                oTypeMails.Add(oTypeMail)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub



    Public Shared Sub chargerContactInstallationFixes(ByRef oContactInstallationFixes As ContactInstallationFixes, ByRef oClient As Client)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT idContact, nomContact, mobileContact, fixeContact, eMailContact FROM dbo.ContactInstallation WHERE idClient=@idClient ORDER BY nomContact ASC"
        Dim cmd As New SqlCommand(sRequete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            cmd.Parameters.Add("@idClient", SqlDbType.Int).Value = oClient.nIdClient
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                Dim oContactInstallationFixe As New ContactInstallationFixe()
                ChargerPropriete(oContactInstallationFixe.nIdContact, readerSQL.Item("idContact"))
                ChargerPropriete(oContactInstallationFixe.sNomContact, readerSQL.Item("nomContact"))
                ChargerPropriete(oContactInstallationFixe.sMobileContact, readerSQL.Item("mobileContact"))
                ChargerPropriete(oContactInstallationFixe.sFixeContact, readerSQL.Item("fixeContact"))
                ChargerPropriete(oContactInstallationFixe.sEmailContact, readerSQL.Item("eMailContact"))
                oContactInstallationFixe.oClient = oClient

                oContactInstallationFixes.Add(oContactInstallationFixe)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerContactInstallationFixe(ByRef oContactInstallationFixe As ContactInstallationFixe)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT idContact, nomContact, mobileContact, fixeContact, eMailContact, idClient FROM dbo.ContactInstallation WHERE idContact=@idContact"
        Dim cmd As New SqlCommand(sRequete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            cmd.Parameters.Add("@idContact", SqlDbType.Int).Value = oContactInstallationFixe.nIdContact
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                oContactInstallationFixe.oClient = New Client()
                ChargerPropriete(oContactInstallationFixe.nIdContact, readerSQL.Item("idContact"))
                ChargerPropriete(oContactInstallationFixe.sNomContact, readerSQL.Item("nomContact"))
                ChargerPropriete(oContactInstallationFixe.sMobileContact, readerSQL.Item("mobileContact"))
                ChargerPropriete(oContactInstallationFixe.sFixeContact, readerSQL.Item("fixeContact"))
                ChargerPropriete(oContactInstallationFixe.sEmailContact, readerSQL.Item("eMailContact"))
                ChargerPropriete(oContactInstallationFixe.oClient.nIdClient, readerSQL.Item("idClient"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerAdresseInstallationFixes(ByRef oAdresseInstallationFixes As AdresseInstallationFixes, ByRef oClient As Client)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT niveau_1_Centre_Frais, niveau_2_Centre_Frais, niveau_3_Centre_Frais, niveau_4_Centre_Frais, Caracteristique_1_Centre_Frais FROM dbo.Vue_Centre_Frais WHERE idClient=@idClient ORDER BY niveau_1_Centre_Frais ASC"
        Dim cmd As New SqlCommand(sRequete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            cmd.Parameters.Add("@idClient", SqlDbType.Int).Value = oClient.nIdClient
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                Dim oAdresseInstallationFixe As New AdresseInstallationFixe()
                ChargerPropriete(oAdresseInstallationFixe.sCodeSite, readerSQL.Item("niveau_1_Centre_Frais"))
                ChargerPropriete(oAdresseInstallationFixe.sNomSite, readerSQL.Item("niveau_2_Centre_Frais"))
                ChargerPropriete(oAdresseInstallationFixe.sAdresseSite, readerSQL.Item("niveau_3_Centre_Frais"))
                ChargerPropriete(oAdresseInstallationFixe.sCodePostal, readerSQL.Item("niveau_4_Centre_Frais"))
                ChargerPropriete(oAdresseInstallationFixe.sLocalite, readerSQL.Item("Caracteristique_1_Centre_Frais"))
                oAdresseInstallationFixe.oClient = oClient

                oAdresseInstallationFixes.Add(oAdresseInstallationFixe)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub ChargerLignesSecondairesSelonIdLigne(ByRef oLigneFixes As LigneFixes, ByRef oLigneFixe As LigneFixe)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT idLigne, numeroLigne, idHierarchie, idCompteFacturant, idUsage, idTypeLigne, dateActivation, dateDesactivation, idClient, idContrat, nombreAcces, nombreSDA, "
        sRequete += "specialisationCanauxArrivee, specialisationCanauxMixte, specialisationCanauxDepart, choixModem, typeAdresseEmail, adresseEmail, stMessagerieVocale, "
        sRequete += "stTransfertAppel, stSecretPermanent, stTransfertAppelNonReponse, stNumeroRenvoi, stRenvoiTerminal, idTypeSelection, idChoixListe, iaInscriptionGroupee, "
        sRequete += "iaListeProfessionelle, iaActiviteProfessionelle, iaPrecisionsComplementairesParution, commentaire, restitutionEquipementLoues, idTeteLigne "
        sRequete += "FROM dbo.LigneTechnique "
        sRequete += "WHERE idTeteLigne = @idTeteLigne AND idTeteLigne<>idLigne and dateDESACTIVATION IS NULL"
        Dim cmd As New SqlCommand(sRequete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            cmd.Parameters.Add("@idTeteLigne", SqlDbType.Int).Value = oLigneFixe.nIdLigneFixe
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                Dim oLigne As New LigneFixe()
                Dim oHierarchie As New HierarchieFixe()
                Dim oCompteFacturant As New CompteFacturantFixe()
                Dim oUsage As New UsageFixe()
                Dim oTypeLigne As New TypeLigneFixe()
                Dim oContrat As New ContratFixe()
                Dim oTypeSelection As New TypeSelectionFixe()
                Dim oChoixListe As New ChoixListeFixe()

                oLigne.oHierarchieFixe = oHierarchie
                oLigne.oCompteFacturantFixe = oCompteFacturant
                oLigne.oUsageFixe = oUsage
                oLigne.oTypeLigneFixe = oTypeLigne
                oLigne.oContratFixe = oContrat
                oLigne.oTypeSelectionFixe = oTypeSelection
                oLigne.oChoixListeFixe = oChoixListe

                ChargerPropriete(oLigne.nIdLigneFixe, readerSQL.Item("idLigne"))
                ChargerPropriete(oLigne.sNumeroLigneFixe, readerSQL.Item("numeroLigne"))
                ChargerPropriete(oLigne.oHierarchieFixe.nIdHierarchie, readerSQL.Item("idHierarchie"))
                ChargerPropriete(oLigne.oCompteFacturantFixe.nIdCompteFacturant, readerSQL.Item("idCompteFacturant"))
                ChargerPropriete(oLigne.oUsageFixe.nIdUsage, readerSQL.Item("idUsage"))
                ChargerPropriete(oLigne.oTypeLigneFixe.nIdTypeLigne, readerSQL.Item("idTypeLigne"))
                ChargerPropriete(oLigne.dDateActivation, readerSQL.Item("dateActivation"))
                ChargerPropriete(oLigne.dDateDesactivation, readerSQL.Item("dateDesactivation"))
                ChargerPropriete(oLigne.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oLigne.oContratFixe.nIdContrat, readerSQL.Item("idContrat"))
                ChargerPropriete(oLigne.nNombreAcces, readerSQL.Item("nombreAcces"))
                ChargerPropriete(oLigne.nNombreSDA, readerSQL.Item("nombreSDA"))
                ChargerPropriete(oLigne.nSpecialisationCanauxArrive, readerSQL.Item("specialisationCanauxArrivee"))
                ChargerPropriete(oLigne.nSpecialisationCanauxMixte, readerSQL.Item("specialisationCanauxMixte"))
                ChargerPropriete(oLigne.nSpecialisationCanauxDepart, readerSQL.Item("specialisationCanauxDepart"))
                ChargerPropriete(oLigne.sChoixModem, readerSQL.Item("choixModem"))
                ChargerPropriete(oLigne.sTypeAdresseEmail, readerSQL.Item("typeAdresseEmail"))
                ChargerPropriete(oLigne.sAdresseEmail, readerSQL.Item("adresseEmail"))
                ChargerPropriete(oLigne.bStMessagerieVocale, readerSQL.Item("stMessagerieVocale"))
                ChargerPropriete(oLigne.bStTransfertAppel, readerSQL.Item("stTransfertAppel"))
                ChargerPropriete(oLigne.bStSecretPermanent, readerSQL.Item("stSecretPermanent"))
                ChargerPropriete(oLigneFixe.bStTransfertAppelNonReponse, readerSQL.Item("stTransfertAppelNonReponse"))
                ChargerPropriete(oLigne.sStNumeroRenvoi, readerSQL.Item("stNumeroRenvoi"))
                ChargerPropriete(oLigne.bStRenvoiTerminal, readerSQL.Item("stRenvoiTerminal"))
                ChargerPropriete(oLigne.oTypeSelectionFixe.nIdTypeSelection, readerSQL.Item("idTypeSelection"))
                ChargerPropriete(oLigne.oChoixListeFixe.nIdChoixListe, readerSQL.Item("idChoixListe"))
                ChargerPropriete(oLigne.sIaPrecisionsComplementairesParution, readerSQL.Item("iaPrecisionsComplementairesParution"))
                ChargerPropriete(oLigne.sCommentaire, readerSQL.Item("commentaire"))
                ChargerPropriete(oLigne.bRestitutionEquipementLoues, readerSQL.Item("restitutionEquipementLoues"))
                ChargerPropriete(oLigne.nIdTeteLigne, readerSQL.Item("idTeteLigne"))

                oLigneFixes.Add(oLigne)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerAdresseInstallationFixeParCodeSite(ByRef oAdresseInstallationFixe As AdresseInstallationFixe)
        Dim sql As New cSQL("GestionParcFixe")
        Dim sRequete As String = "SELECT niveau_2_Centre_Frais, niveau_3_Centre_Frais, niveau_4_Centre_Frais, Caracteristique_1_Centre_Frais, idClient FROM dbo.Vue_Centre_Frais WHERE niveau_1_Centre_Frais=@sCodeSite"
        Dim cmd As New SqlCommand(sRequete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing
        Try
            cmd.Parameters.Add("@sCodeSite", SqlDbType.VarChar, 100).Value = oAdresseInstallationFixe.sCodeSite
            readerSQL = cmd.ExecuteReader()
            Do Until Not readerSQL.Read()
                oAdresseInstallationFixe.oClient = New Client()
                ChargerPropriete(oAdresseInstallationFixe.sNomSite, readerSQL.Item("niveau_2_Centre_Frais"))
                ChargerPropriete(oAdresseInstallationFixe.sAdresseSite, readerSQL.Item("niveau_3_Centre_Frais"))
                ChargerPropriete(oAdresseInstallationFixe.sCodePostal, readerSQL.Item("niveau_4_Centre_Frais"))
                ChargerPropriete(oAdresseInstallationFixe.sLocalite, readerSQL.Item("Caracteristique_1_Centre_Frais"))
                ChargerPropriete(oAdresseInstallationFixe.oClient.nIdClient, readerSQL.Item("idClient"))

                Exit Do
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerParametragesControlesFixe(ByRef oParametragesControlesFixe As ParametragesControlesFixe, ByVal idClient As Integer)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oParametrageControleFixe As ParametrageControleFixe

        Try

            Requete = "SELECT idClient, typeControle, idControle, placeholder, textLabel, topControle, leftLabel, leftControle, widthLabel, widthControle,heightLabel, heightControle, cas, typeLigne, verrouille, autoPostback, nomMembre, autorisationNull, typeAttendu"
            Requete &= " FROM dbo.ParametrageControle"
            Requete &= " WHERE  idClient = " & idClient.ToString
            Requete &= " ORDER BY cas, typeLigne, placeholder"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oParametrageControleFixe = New ParametrageControleFixe
                ChargerPropriete(oParametrageControleFixe.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oParametrageControleFixe.sTypeControle, readerSQL.Item("typeControle"))
                ChargerPropriete(oParametrageControleFixe.sIdControle, readerSQL.Item("idControle"))
                ChargerPropriete(oParametrageControleFixe.sPlaceholder, readerSQL.Item("placeholder"))
                ChargerPropriete(oParametrageControleFixe.sTextLabel, readerSQL.Item("textLabel"))
                ChargerPropriete(oParametrageControleFixe.sTopControle, readerSQL.Item("topControle"))
                ChargerPropriete(oParametrageControleFixe.sLeftLabel, readerSQL.Item("leftLabel"))
                ChargerPropriete(oParametrageControleFixe.sLeftControle, readerSQL.Item("leftControle"))
                ChargerPropriete(oParametrageControleFixe.sWidthLabel, readerSQL.Item("widthLabel"))
                ChargerPropriete(oParametrageControleFixe.sWidthControle, readerSQL.Item("widthControle"))
                ChargerPropriete(oParametrageControleFixe.sHeightLabel, readerSQL.Item("heightLabel"))
                ChargerPropriete(oParametrageControleFixe.sHeightControle, readerSQL.Item("heightControle"))
                ChargerPropriete(oParametrageControleFixe.sCas, readerSQL.Item("cas"))
                ChargerPropriete(oParametrageControleFixe.sTypeLigne, readerSQL.Item("typeLigne"))
                ChargerPropriete(oParametrageControleFixe.bVerrouille, readerSQL.Item("verrouille"))
                ChargerPropriete(oParametrageControleFixe.bAutoPostBack, readerSQL.Item("autoPostback"))
                ChargerPropriete(oParametrageControleFixe.sNomMembre, readerSQL.Item("nomMembre"))
                ChargerPropriete(oParametrageControleFixe.bAutorisationNull, readerSQL.Item("autorisationNull"))
                ChargerPropriete(oParametrageControleFixe.sTypeAttendu, readerSQL.Item("typeAttendu"))
                oParametragesControlesFixe.Add(oParametrageControleFixe)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerHierarchies(ByRef oHierarchies As Hierarchies, ByVal idClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oHierarchie As Hierarchie

        Try
            Requete = "SELECT idHierarchie, hierarchie1, hierarchie2, hierarchie3, hierarchie4, hierarchie5, hierarchie6, hierarchie7, hierarchie8, hierarchie9,idClient"
            Requete &= " FROM Hierarchie"
            Requete &= " WHERE  idClient = " & idClient.ToString & " AND estActif = 1"
            Requete &= " ORDER BY  hierarchie1, hierarchie2, hierarchie3, hierarchie4, hierarchie5, hierarchie6, hierarchie7, hierarchie8, hierarchie9"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oHierarchie = New Hierarchie
                ChargerPropriete(oHierarchie.nIdHierarchie, readerSQL.Item("idHierarchie"))
                ChargerPropriete(oHierarchie.sHierarchie1, readerSQL.Item("hierarchie1"))
                ChargerPropriete(oHierarchie.sHierarchie2, readerSQL.Item("hierarchie2"))
                ChargerPropriete(oHierarchie.sHierarchie3, readerSQL.Item("hierarchie3"))
                ChargerPropriete(oHierarchie.sHierarchie4, readerSQL.Item("hierarchie4"))
                ChargerPropriete(oHierarchie.sHierarchie5, readerSQL.Item("hierarchie5"))
                ChargerPropriete(oHierarchie.sHierarchie6, readerSQL.Item("hierarchie6"))
                ChargerPropriete(oHierarchie.sHierarchie7, readerSQL.Item("hierarchie7"))
                ChargerPropriete(oHierarchie.sHierarchie8, readerSQL.Item("hierarchie8"))
                ChargerPropriete(oHierarchie.sHierarchie9, readerSQL.Item("hierarchie9"))
                ChargerPropriete(oHierarchie.nIdClient, readerSQL.Item("idClient"))

                oHierarchies.Add(oHierarchie)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerHierarchiesFixe(ByRef oHierarchiesFixe As HierarchiesFixe, ByRef oClient As Client)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oHierarchieFixe As HierarchieFixe

        Try
            Requete = "SELECT idHierarchie, hierarchie1, hierarchie2, hierarchie3, hierarchie4, hierarchie5, hierarchie6, hierarchie7, hierarchie8, hierarchie9,idClient"
            Requete &= " FROM Hierarchie"
            Requete &= " WHERE  idClient = " & oClient.nIdClient.ToString & " AND estActif = 1"
            Requete &= " ORDER BY  hierarchie1, hierarchie2, hierarchie3, hierarchie4, hierarchie5, hierarchie6, hierarchie7, hierarchie8, hierarchie9"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oHierarchieFixe = New HierarchieFixe
                ChargerPropriete(oHierarchieFixe.nIdHierarchie, readerSQL.Item("idHierarchie"))
                ChargerPropriete(oHierarchieFixe.sHierarchie1, readerSQL.Item("hierarchie1"))
                ChargerPropriete(oHierarchieFixe.sHierarchie2, readerSQL.Item("hierarchie2"))
                ChargerPropriete(oHierarchieFixe.sHierarchie3, readerSQL.Item("hierarchie3"))
                ChargerPropriete(oHierarchieFixe.sHierarchie4, readerSQL.Item("hierarchie4"))
                ChargerPropriete(oHierarchieFixe.sHierarchie5, readerSQL.Item("hierarchie5"))
                ChargerPropriete(oHierarchieFixe.sHierarchie6, readerSQL.Item("hierarchie6"))
                ChargerPropriete(oHierarchieFixe.sHierarchie7, readerSQL.Item("hierarchie7"))
                ChargerPropriete(oHierarchieFixe.sHierarchie8, readerSQL.Item("hierarchie8"))
                ChargerPropriete(oHierarchieFixe.sHierarchie9, readerSQL.Item("hierarchie9"))
                ChargerPropriete(oHierarchieFixe.nIdClient, readerSQL.Item("idClient"))

                oHierarchiesFixe.Add(oHierarchieFixe)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerForfaits(ByRef oForfaits As Forfaits, ByVal idClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oForfait As Forfait
        Dim oOperateur As Operateur
        Dim oTypeForfait As TypeForfait

        Try
            Requete = "SELECT idForfait, libelle, prixAbonnement, idOperateur, idTypeForfait, idClient"
            Requete &= " FROM Forfait"
            Requete &= " WHERE  idClient = " & idClient.ToString & " AND estActif = 1"
            Requete &= " ORDER BY  libelle"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oForfait = New Forfait
                oOperateur = New Operateur
                oTypeForfait = New TypeForfait
                ChargerPropriete(oForfait.nIdForfait, readerSQL.Item("idForfait"))
                ChargerPropriete(oForfait.sLibelle, readerSQL.Item("libelle"))
                ChargerPropriete(oForfait.nPrixAbonnement, readerSQL.Item("prixAbonnement"))
                oOperateur.chargerOperateurParId(CInt(readerSQL.Item("idOperateur")))
                ChargerPropriete(oForfait.oOperateur, oOperateur)
                oTypeForfait.chargerTypeForfaitParId(CInt(readerSQL.Item("idTypeForfait")))
                ChargerPropriete(oForfait.oTypeForfait, oTypeForfait)
                ChargerPropriete(oForfait.nIdClient, readerSQL.Item("idClient"))

                oForfaits.Add(oForfait)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerDiscriminations(ByRef oDiscriminations As Discriminations, ByVal idClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oDiscrimination As Discrimination

        Try
            Requete = "SELECT idDiscrimination, libelle, idClient"
            Requete &= " FROM Discrimination"
            Requete &= " WHERE  idClient = " & idClient.ToString & " AND estActif = 1"
            Requete &= " ORDER BY  libelle"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oDiscrimination = New Discrimination
                ChargerPropriete(oDiscrimination.nIdDiscrimination, readerSQL.Item("idDiscrimination"))
                ChargerPropriete(oDiscrimination.sLibelle, readerSQL.Item("libelle"))
                ChargerPropriete(oDiscrimination.nIdClient, readerSQL.Item("idClient"))
                oDiscriminations.Add(oDiscrimination)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerOptionsUtilisateurParIdLigne(ByRef oOptionsUtilisateur As OptionsUtilisateur, ByVal nIdLigne As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oOptionUtilisateur As OptionUtilisateur
        Dim oCategorieOptionUtilisateur As CategorieOptionUtilisateur

        Try
            Requete = "SELECT dbo.[Option].idOption, dbo.[Option].libelle, dbo.[Option].idCategorieOption, dbo.[Option].idClient "
            Requete += " FROM dbo.LiaisonOption INNER JOIN dbo.[Option] ON dbo.LiaisonOption.idOption = dbo.[Option].idOption INNER JOIN"
            Requete += " dbo.CategorieOption ON dbo.[Option].idCategorieOption = dbo.CategorieOption.idCategorieOption"
            Requete += " WHERE dbo.LiaisonOption.idLigne = " + nIdLigne.ToString
            Requete += " ORDER BY dbo.CategorieOption.libelle, dbo.[Option].libelle"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oCategorieOptionUtilisateur = New CategorieOptionUtilisateur
                oOptionUtilisateur = New OptionUtilisateur
                ChargerPropriete(oOptionUtilisateur.nIdOption, readerSQL.Item("idOption"))
                ChargerPropriete(oOptionUtilisateur.sLibelle, readerSQL.Item("libelle"))
                oCategorieOptionUtilisateur.chargerCategorieOptionUtilisateurParId(CInt(readerSQL.Item("idCategorieOption")))
                ChargerPropriete(oOptionUtilisateur.oCategorieOptionUtilisateur, oCategorieOptionUtilisateur)
                ChargerPropriete(oOptionUtilisateur.nidClient, readerSQL.Item("idClient"))
                oOptionsUtilisateur.Add(oOptionUtilisateur)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerOptionsProfil(oOptionsProfils As OptionsProfils, sNomProfil As String, nIdClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oOptionProfil As New OptionProfil
        Dim oCategorieOptionUtilisateur As CategorieOptionUtilisateur

        Try
            Requete = "Select DISTINCT dbo.[Option].idOption , IdCategorieOption, dbo.[Option].libelle "
            Requete += " From dbo.Profils INNER Join"
            Requete += " dbo.LiaisonOption ON dbo.Profils.IdOption = dbo.LiaisonOption.idOption INNER Join"
            Requete += " dbo.Ligne On dbo.LiaisonOption.idLigne = dbo.Ligne.idLigne INNER Join"
            Requete += " dbo.[Option] ON dbo.LiaisonOption.idOption = dbo.[Option].idOption"
            Requete += " Where (dbo.[Option].idClient = '" + nIdClient.ToString + "') And (Profils.NomProfil = '" + sNomProfil + "')"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oCategorieOptionUtilisateur = New CategorieOptionUtilisateur
                oOptionProfil = New OptionProfil
                ChargerPropriete(oOptionProfil.nIdOptionProfil, readerSQL.Item("idOption"))
                ChargerPropriete(oOptionProfil.sLibelle, readerSQL.Item("libelle"))
                oCategorieOptionUtilisateur.chargerCategorieOptionUtilisateurParId(CInt(readerSQL.Item("idCategorieOption")))
                ChargerPropriete(oOptionProfil.oCategorieOptionUtilisateur, oCategorieOptionUtilisateur)
                oOptionsProfils.Add(oOptionProfil)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerLigneFixeParNumero(ByRef oLigneFixe As LigneFixe, ByVal sNumero As String, ByRef oClient As Client)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oHierarchieFixe As HierarchieFixe
        Dim oCompteFacturantFixe As CompteFacturantFixe
        Dim oUsageFixe As UsageFixe
        Dim oTypeLigneFixe As TypeLigneFixe

        Try

            Requete = "Select idLigne, numeroLigne, idHierarchie, idCompteFacturant, idUsage, idTypeLigne, dateActivation, dateDesactivation, idContrat, nombreAcces, nombreSDA, "
            Requete += " specialisationCanauxArrivee, specialisationCanauxMixte, specialisationCanauxDepart, choixModem, typeAdresseEmail, adresseEmail,stMessagerieVocale, stSecretPermanent,"
            Requete += " stTransfertAppel, stNumeroRenvoi, stRenvoiTerminal, idTypeSelection, idChoixListe, iaPrecisionsComplementairesParution, commentaire, restitutionEquipementLoues, idClient, idTeteLigne,"
            Requete += " nomEtablissement, adresse, codePostal, localite, complementAdresse, nomContact, fixeContact, mobileContact, mailContact"
            Requete += " FROM dbo.LigneTechnique"
            Requete += " WHERE numeroLigne = '" + sNumero + "' AND idClient=" & oClient.nIdClient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oHierarchieFixe = New HierarchieFixe
                oCompteFacturantFixe = New CompteFacturantFixe
                oUsageFixe = New UsageFixe
                oTypeLigneFixe = New TypeLigneFixe

                oLigneFixe.oChoixListeFixe = New ChoixListeFixe()
                oLigneFixe.oTypeSelectionFixe = New TypeSelectionFixe()
                oLigneFixe.oContratFixe = New ContratFixe()

                ChargerPropriete(oLigneFixe.nIdLigneFixe, readerSQL.Item("idLigne"))
                ChargerPropriete(oLigneFixe.sNumeroLigneFixe, readerSQL.Item("numeroLigne"))
                oHierarchieFixe.chargerHierarchieFixeParId(CInt(readerSQL.Item("idHierarchie")))
                ChargerPropriete(oLigneFixe.oHierarchieFixe, oHierarchieFixe)
                oCompteFacturantFixe.chargerCompteFacturantFixeParId(CInt(readerSQL.Item("idCompteFacturant")))
                ChargerPropriete(oLigneFixe.oCompteFacturantFixe, oCompteFacturantFixe)
                oUsageFixe.chargerUsageFixeParId(CInt(readerSQL.Item("idUsage")), CInt(readerSQL.Item("idClient")))
                ChargerPropriete(oLigneFixe.oUsageFixe, oUsageFixe)
                oTypeLigneFixe.chargerTypeLigneFixeParId(CInt(readerSQL.Item("idTypeLigne")))
                ChargerPropriete(oLigneFixe.oTypeLigneFixe, oTypeLigneFixe)
                ChargerPropriete(oLigneFixe.dDateActivation, readerSQL.Item("dateActivation"))
                ChargerPropriete(oLigneFixe.dDateDesactivation, readerSQL.Item("dateDesactivation"))
                ChargerPropriete(oLigneFixe.oContratFixe.nIdContrat, readerSQL.Item("idContrat"))
                ChargerPropriete(oLigneFixe.nNombreAcces, readerSQL.Item("nombreAcces"))
                ChargerPropriete(oLigneFixe.nNombreSDA, readerSQL.Item("nombreSDA"))
                ChargerPropriete(oLigneFixe.nSpecialisationCanauxArrive, readerSQL.Item("specialisationCanauxArrivee"))
                ChargerPropriete(oLigneFixe.nSpecialisationCanauxMixte, readerSQL.Item("specialisationCanauxMixte"))
                ChargerPropriete(oLigneFixe.nSpecialisationCanauxDepart, readerSQL.Item("specialisationCanauxDepart"))
                ChargerPropriete(oLigneFixe.sChoixModem, readerSQL.Item("choixModem"))
                ChargerPropriete(oLigneFixe.sTypeAdresseEmail, readerSQL.Item("typeAdresseEmail"))
                ChargerPropriete(oLigneFixe.sAdresseEmail, readerSQL.Item("adresseEmail"))
                ChargerPropriete(oLigneFixe.bStMessagerieVocale, readerSQL.Item("stMessagerieVocale"))
                ChargerPropriete(oLigneFixe.bStSecretPermanent, readerSQL.Item("stSecretPermanent"))
                ChargerPropriete(oLigneFixe.bStTransfertAppel, readerSQL.Item("stTransfertAppel"))
                ChargerPropriete(oLigneFixe.sStNumeroRenvoi, readerSQL.Item("stNumeroRenvoi"))
                ChargerPropriete(oLigneFixe.bStRenvoiTerminal, readerSQL.Item("stRenvoiTerminal"))
                ChargerPropriete(oLigneFixe.oTypeSelectionFixe.nIdTypeSelection, readerSQL.Item("idTypeSelection"))
                ChargerPropriete(oLigneFixe.oChoixListeFixe.nIdChoixListe, readerSQL.Item("idChoixListe"))
                ChargerPropriete(oLigneFixe.sIaPrecisionsComplementairesParution, readerSQL.Item("iaPrecisionsComplementairesParution"))
                ChargerPropriete(oLigneFixe.bRestitutionEquipementLoues, readerSQL.Item("restitutionEquipementLoues"))
                ChargerPropriete(oLigneFixe.nIdTeteLigne, readerSQL.Item("idTeteLigne"))
                ChargerPropriete(oLigneFixe.sNomEtablissement, readerSQL.Item("nomEtablissement"))
                ChargerPropriete(oLigneFixe.sAdresse, readerSQL.Item("adresse"))
                ChargerPropriete(oLigneFixe.sCodePostal, readerSQL.Item("codePostal"))
                ChargerPropriete(oLigneFixe.sLocalite, readerSQL.Item("localite"))
                ChargerPropriete(oLigneFixe.sComplementAdresse, readerSQL.Item("complementAdresse"))
                ChargerPropriete(oLigneFixe.sNomContact, readerSQL.Item("nomContact"))
                ChargerPropriete(oLigneFixe.sFixeContact, readerSQL.Item("fixeContact"))
                ChargerPropriete(oLigneFixe.sMobileContact, readerSQL.Item("mobileContact"))
                ChargerPropriete(oLigneFixe.sMailContact, readerSQL.Item("mailContact"))


                oLigneFixe.oChoixListeFixe.chargerChoixListeFixeParId()
                oLigneFixe.oTypeSelectionFixe.chargerTypeSelectionFixeParId()
                oLigneFixe.oContratFixe.chargerContratFixeParId()

            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerLigneFixeParId(ByRef oLigneFixe As LigneFixe)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oHierarchieFixe As HierarchieFixe
        Dim oCompteFacturantFixe As CompteFacturantFixe
        Dim oUsageFixe As UsageFixe
        Dim oTypeLigneFixe As TypeLigneFixe

        Try

            Requete = "SELECT idLigne, numeroLigne, idHierarchie, idCompteFacturant, idUsage, idTypeLigne, dateActivation, dateDesactivation, idContrat, nombreAcces, nombreSDA, "
            Requete += " specialisationCanauxArrivee, specialisationCanauxMixte, specialisationCanauxDepart, choixModem, typeAdresseEmail, adresseEmail,stMessagerieVocale, stSecretPermanent,"
            Requete += " stTransfertAppel, stNumeroRenvoi, stRenvoiTerminal, idTypeSelection, idChoixListe, iaPrecisionsComplementairesParution, commentaire, restitutionEquipementLoues, idClient, idTeteLigne,"
            Requete += " nomEtablissement, adresse, codePostal, localite, complementAdresse, nomContact, fixeContact, mobileContact, mailContact"
            Requete += " FROM dbo.LigneTechnique"
            Requete += " WHERE idLigne = " & oLigneFixe.nIdLigneFixe

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oHierarchieFixe = New HierarchieFixe
                oCompteFacturantFixe = New CompteFacturantFixe
                oUsageFixe = New UsageFixe
                oTypeLigneFixe = New TypeLigneFixe

                oLigneFixe.oChoixListeFixe = New ChoixListeFixe()
                oLigneFixe.oTypeSelectionFixe = New TypeSelectionFixe()
                oLigneFixe.oContratFixe = New ContratFixe()

                ChargerPropriete(oLigneFixe.nIdLigneFixe, readerSQL.Item("idLigne"))
                ChargerPropriete(oLigneFixe.sNumeroLigneFixe, readerSQL.Item("numeroLigne"))
                oHierarchieFixe.chargerHierarchieFixeParId(CInt(readerSQL.Item("idHierarchie")))
                ChargerPropriete(oLigneFixe.oHierarchieFixe, oHierarchieFixe)
                oCompteFacturantFixe.chargerCompteFacturantFixeParId(CInt(readerSQL.Item("idCompteFacturant")))
                ChargerPropriete(oLigneFixe.oCompteFacturantFixe, oCompteFacturantFixe)
                oUsageFixe.chargerUsageFixeParId(CInt(readerSQL.Item("idUsage")), CInt(readerSQL.Item("idClient")))
                ChargerPropriete(oLigneFixe.oUsageFixe, oUsageFixe)
                oTypeLigneFixe.chargerTypeLigneFixeParId(CInt(readerSQL.Item("idTypeLigne")))
                ChargerPropriete(oLigneFixe.oTypeLigneFixe, oTypeLigneFixe)
                ChargerPropriete(oLigneFixe.dDateActivation, readerSQL.Item("dateActivation"))
                ChargerPropriete(oLigneFixe.dDateDesactivation, readerSQL.Item("dateDesactivation"))
                ChargerPropriete(oLigneFixe.oContratFixe.nIdContrat, readerSQL.Item("idContrat"))
                ChargerPropriete(oLigneFixe.nNombreAcces, readerSQL.Item("nombreAcces"))
                ChargerPropriete(oLigneFixe.nNombreSDA, readerSQL.Item("nombreSDA"))
                ChargerPropriete(oLigneFixe.nSpecialisationCanauxArrive, readerSQL.Item("specialisationCanauxArrivee"))
                ChargerPropriete(oLigneFixe.nSpecialisationCanauxMixte, readerSQL.Item("specialisationCanauxMixte"))
                ChargerPropriete(oLigneFixe.nSpecialisationCanauxDepart, readerSQL.Item("specialisationCanauxDepart"))
                ChargerPropriete(oLigneFixe.sChoixModem, readerSQL.Item("choixModem"))
                ChargerPropriete(oLigneFixe.sTypeAdresseEmail, readerSQL.Item("typeAdresseEmail"))
                ChargerPropriete(oLigneFixe.sAdresseEmail, readerSQL.Item("adresseEmail"))
                ChargerPropriete(oLigneFixe.bStMessagerieVocale, readerSQL.Item("stMessagerieVocale"))
                ChargerPropriete(oLigneFixe.bStSecretPermanent, readerSQL.Item("stSecretPermanent"))
                ChargerPropriete(oLigneFixe.bStTransfertAppel, readerSQL.Item("stTransfertAppel"))
                ChargerPropriete(oLigneFixe.sStNumeroRenvoi, readerSQL.Item("stNumeroRenvoi"))
                ChargerPropriete(oLigneFixe.bStRenvoiTerminal, readerSQL.Item("stRenvoiTerminal"))
                ChargerPropriete(oLigneFixe.oTypeSelectionFixe.nIdTypeSelection, readerSQL.Item("idTypeSelection"))
                ChargerPropriete(oLigneFixe.oChoixListeFixe.nIdChoixListe, readerSQL.Item("idChoixListe"))
                ChargerPropriete(oLigneFixe.sIaPrecisionsComplementairesParution, readerSQL.Item("iaPrecisionsComplementairesParution"))
                ChargerPropriete(oLigneFixe.bRestitutionEquipementLoues, readerSQL.Item("restitutionEquipementLoues"))
                ChargerPropriete(oLigneFixe.nIdTeteLigne, readerSQL.Item("idTeteLigne"))
                ChargerPropriete(oLigneFixe.sNomEtablissement, readerSQL.Item("nomEtablissement"))
                ChargerPropriete(oLigneFixe.sAdresse, readerSQL.Item("adresse"))
                ChargerPropriete(oLigneFixe.sCodePostal, readerSQL.Item("codePostal"))
                ChargerPropriete(oLigneFixe.sLocalite, readerSQL.Item("localite"))
                ChargerPropriete(oLigneFixe.sComplementAdresse, readerSQL.Item("complementAdresse"))
                ChargerPropriete(oLigneFixe.sNomContact, readerSQL.Item("nomContact"))
                ChargerPropriete(oLigneFixe.sFixeContact, readerSQL.Item("fixeContact"))
                ChargerPropriete(oLigneFixe.sMobileContact, readerSQL.Item("mobileContact"))
                ChargerPropriete(oLigneFixe.sMailContact, readerSQL.Item("mailContact"))

                oLigneFixe.oChoixListeFixe.chargerChoixListeFixeParId()
                oLigneFixe.oTypeSelectionFixe.chargerTypeSelectionFixeParId()
                oLigneFixe.oContratFixe.chargerContratFixeParId()

            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerStocksUtilisateurParIdLigne(ByRef oStocks As Stocks, ByVal nIdLigne As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oStock As Stock
        Dim oArticle As Article
        Try
            Requete = "SELECT        dbo.Stock.idArticleStock, dbo.Stock.codeEmei, dbo.Stock.numSerie, dbo.Stock.codeReference, dbo.Stock.codeDesimlockage1, dbo.Stock.etiquetteInventaire, "
            Requete += " dbo.Stock.estDesimlocke, dbo.Stock.codePin1, dbo.Stock.codePuk1, dbo.Stock.codePuk2, dbo.Stock.codeVerrouillage, dbo.Stock.estEnrole, dbo.Stock.etat, dbo.Stock.dateRentree,"
            Requete += " dbo.Stock.dateSortie, dbo.Stock.prix, dbo.Stock.dernierDetenteur, dbo.Stock.idArticle, dbo.Stock.idClient, dbo.LiaisonStock.typeLiaison, dbo.LiaisonStock.dateAttribution"
            Requete += " FROM            dbo.Stock INNER JOIN"
            Requete += " dbo.LiaisonStock ON dbo.Stock.idArticleStock = dbo.LiaisonStock.idArticleStock"
            Requete += " WHERE dbo.LiaisonStock.idLigne = " + nIdLigne.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oStock = New Stock
                oArticle = New Article
                oArticle.chargerArticleParId(CInt(readerSQL.Item("idArticle").ToString))
                ChargerPropriete(oStock.oArticle, oArticle)
                ChargerPropriete(oStock.nIdArticleStock, readerSQL.Item("idArticleStock"))
                ChargerPropriete(oStock.sCodeReference, readerSQL.Item("codeReference"))
                ChargerPropriete(oStock.sCodeEmei, readerSQL.Item("codeEmei"))
                ChargerPropriete(oStock.sNumSerie, readerSQL.Item("numSerie"))
                ChargerPropriete(oStock.sCodeDesimlockage1, readerSQL.Item("codeDesimlockage1"))
                ChargerPropriete(oStock.sEtiquetteInventaire, readerSQL.Item("etiquetteInventaire"))
                ChargerPropriete(oStock.bEstDesimlocke, readerSQL.Item("estDesimlocke"))
                ChargerPropriete(oStock.sCodePin1, readerSQL.Item("codePin1"))
                ChargerPropriete(oStock.sCodePuk1, readerSQL.Item("codePuk1"))
                ChargerPropriete(oStock.sCodePuk2, readerSQL.Item("codePuk2"))
                ChargerPropriete(oStock.sCodeVerrouillage, readerSQL.Item("codeVerrouillage"))
                ChargerPropriete(oStock.bEstEnrole, readerSQL.Item("estEnrole"))
                ChargerPropriete(oStock.nPrix, readerSQL.Item("prix"))
                ChargerPropriete(oStock.sEtat, readerSQL.Item("etat"))
                ChargerPropriete(oStock.dDateRentree, readerSQL.Item("dateRentree"))
                ChargerPropriete(oStock.dDateSortie, readerSQL.Item("dateSortie"))
                ChargerPropriete(oStock.sDernierDetenteur, readerSQL.Item("dernierDetenteur"))
                ChargerPropriete(oStock.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oStock.sTypeLiaison, readerSQL.Item("typeLiaison"))
                ChargerPropriete(oStock.dDateAttribution, readerSQL.Item("dateAttribution"))

                oStocks.Add(oStock)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerStocks(ByRef oStocks As Stocks, ByVal nIdClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oStock As Stock
        Dim oArticle As Article
        Try
            Requete = "SELECT        dbo.Stock.idArticleStock, dbo.Stock.codeEmei, dbo.Stock.numSerie, dbo.Stock.codeReference, dbo.Stock.codeDesimlockage1, dbo.Stock.etiquetteInventaire, "
            Requete += " dbo.Stock.estDesimlocke, dbo.Stock.codePin1, dbo.Stock.codePuk1, dbo.Stock.codePuk2, dbo.Stock.codeVerrouillage, dbo.Stock.estEnrole, dbo.Stock.etat, dbo.Stock.dateRentree,"
            Requete += " dbo.Stock.dateSortie, dbo.Stock.prix, dbo.Stock.dernierDetenteur, dbo.Stock.idArticle, dbo.Stock.idClient"
            Requete += " FROM dbo.Stock"
            Requete += " WHERE dbo.Stock.idClient = " + nIdClient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oStock = New Stock
                oArticle = New Article
                oArticle.chargerArticleParId(CInt(readerSQL.Item("idArticle").ToString))
                ChargerPropriete(oStock.oArticle, oArticle)
                ChargerPropriete(oStock.nIdArticleStock, readerSQL.Item("idArticleStock"))
                ChargerPropriete(oStock.sCodeReference, readerSQL.Item("codeReference"))
                ChargerPropriete(oStock.sCodeEmei, readerSQL.Item("codeEmei"))
                ChargerPropriete(oStock.sNumSerie, readerSQL.Item("numSerie"))
                ChargerPropriete(oStock.sCodeDesimlockage1, readerSQL.Item("codeDesimlockage1"))
                ChargerPropriete(oStock.sEtiquetteInventaire, readerSQL.Item("etiquetteInventaire"))
                ChargerPropriete(oStock.bEstDesimlocke, readerSQL.Item("estDesimlocke"))
                ChargerPropriete(oStock.sCodePin1, readerSQL.Item("codePin1"))
                ChargerPropriete(oStock.sCodePuk1, readerSQL.Item("codePuk1"))
                ChargerPropriete(oStock.sCodePuk2, readerSQL.Item("codePuk2"))
                ChargerPropriete(oStock.sCodeVerrouillage, readerSQL.Item("codeVerrouillage"))
                ChargerPropriete(oStock.bEstEnrole, readerSQL.Item("estEnrole"))
                ChargerPropriete(oStock.nPrix, readerSQL.Item("prix"))
                ChargerPropriete(oStock.sEtat, readerSQL.Item("etat"))
                ChargerPropriete(oStock.dDateRentree, readerSQL.Item("dateRentree"))
                ChargerPropriete(oStock.dDateSortie, readerSQL.Item("dateSortie"))
                ChargerPropriete(oStock.sDernierDetenteur, readerSQL.Item("dernierDetenteur"))
                ChargerPropriete(oStock.nIdClient, readerSQL.Item("idClient"))

                oStocks.Add(oStock)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerStocksLibres(ByRef oStocks As Stocks, ByVal nIdClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oStock As Stock
        Dim oArticle As Article
        Try
            Requete = "SELECT        dbo.Stock.idArticleStock, dbo.Stock.codeEmei, dbo.Stock.numSerie, dbo.Stock.codeReference, dbo.Stock.codeDesimlockage1, dbo.Stock.etiquetteInventaire, "
            Requete += " dbo.Stock.estDesimlocke, dbo.Stock.codePin1, dbo.Stock.codePuk1, dbo.Stock.codePuk2, dbo.Stock.codeVerrouillage, dbo.Stock.estEnrole, dbo.Stock.etat, dbo.Stock.dateRentree,"
            Requete += " dbo.Stock.dateSortie, dbo.Stock.prix, dbo.Stock.dernierDetenteur, dbo.Stock.idArticle, dbo.Stock.idClient"
            Requete += " FROM dbo.Stock"
            Requete += " WHERE dbo.Stock.idClient = " + nIdClient.ToString & " AND dbo.stock.etat='Non attribué'"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oStock = New Stock
                oArticle = New Article
                oArticle.chargerArticleParId(CInt(readerSQL.Item("idArticle").ToString))
                ChargerPropriete(oStock.oArticle, oArticle)
                ChargerPropriete(oStock.nIdArticleStock, readerSQL.Item("idArticleStock"))
                ChargerPropriete(oStock.sCodeReference, readerSQL.Item("codeReference"))
                ChargerPropriete(oStock.sCodeEmei, readerSQL.Item("codeEmei"))
                ChargerPropriete(oStock.sNumSerie, readerSQL.Item("numSerie"))
                ChargerPropriete(oStock.sCodeDesimlockage1, readerSQL.Item("codeDesimlockage1"))
                ChargerPropriete(oStock.sEtiquetteInventaire, readerSQL.Item("etiquetteInventaire"))
                ChargerPropriete(oStock.bEstDesimlocke, readerSQL.Item("estDesimlocke"))
                ChargerPropriete(oStock.sCodePin1, readerSQL.Item("codePin1"))
                ChargerPropriete(oStock.sCodePuk1, readerSQL.Item("codePuk1"))
                ChargerPropriete(oStock.sCodePuk2, readerSQL.Item("codePuk2"))
                ChargerPropriete(oStock.sCodeVerrouillage, readerSQL.Item("codeVerrouillage"))
                ChargerPropriete(oStock.bEstEnrole, readerSQL.Item("estEnrole"))
                ChargerPropriete(oStock.nPrix, readerSQL.Item("prix"))
                ChargerPropriete(oStock.sEtat, readerSQL.Item("etat"))
                ChargerPropriete(oStock.dDateRentree, readerSQL.Item("dateRentree"))
                ChargerPropriete(oStock.dDateSortie, readerSQL.Item("dateSortie"))
                ChargerPropriete(oStock.sDernierDetenteur, readerSQL.Item("dernierDetenteur"))
                ChargerPropriete(oStock.nIdClient, readerSQL.Item("idClient"))

                oStocks.Add(oStock)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerOptionsClient(ByRef oOptionsUtilisateur As OptionsUtilisateur, ByVal nIdClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oOptionUtilisateur As OptionUtilisateur
        Dim oCategorieOptionUtilisateur As CategorieOptionUtilisateur

        Try
            Requete = "SELECT dbo.[Option].idOption, dbo.[Option].libelle, dbo.[Option].idCategorieOption, dbo.[Option].idClient "
            Requete += " FROM dbo.[Option] INNER JOIN dbo.CategorieOption ON dbo.[Option].idCategorieOption = dbo.CategorieOption.idCategorieOption "
            Requete += " WHERE dbo.[Option].estActif = 1 and dbo.[Option].IdClient = " + nIdClient.ToString
            Requete += " ORDER BY dbo.CategorieOption.libelle, dbo.[Option].libelle"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oOptionUtilisateur = New OptionUtilisateur
                oCategorieOptionUtilisateur = New CategorieOptionUtilisateur
                ChargerPropriete(oOptionUtilisateur.nIdOption, readerSQL.Item("idOption"))
                ChargerPropriete(oOptionUtilisateur.sLibelle, readerSQL.Item("libelle"))
                oCategorieOptionUtilisateur.chargerCategorieOptionUtilisateurParId(CInt(readerSQL.Item("idCategorieOption")))
                ChargerPropriete(oOptionUtilisateur.oCategorieOptionUtilisateur, oCategorieOptionUtilisateur)
                ChargerPropriete(oOptionUtilisateur.nidClient, readerSQL.Item("idClient"))
                oOptionsUtilisateur.Add(oOptionUtilisateur)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerOptionParId(ByRef oOptionUtilisateur As OptionUtilisateur, ByVal nIdOptionUtilisateur As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oCategorieOptionUtilisateur As CategorieOptionUtilisateur

        Try
            Requete = "SELECT dbo.[Option].idOption, dbo.[Option].libelle, dbo.[Option].idCategorieOption, dbo.[Option].idClient "
            Requete += " FROM dbo.[Option] "
            Requete += " WHERE dbo.[Option].idOption = " + nIdOptionUtilisateur.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oCategorieOptionUtilisateur = New CategorieOptionUtilisateur
                ChargerPropriete(oOptionUtilisateur.nIdOption, readerSQL.Item("idOption"))
                ChargerPropriete(oOptionUtilisateur.sLibelle, readerSQL.Item("libelle"))
                oCategorieOptionUtilisateur.chargerCategorieOptionUtilisateurParId(CInt(readerSQL.Item("idCategorieOption")))
                ChargerPropriete(oOptionUtilisateur.oCategorieOptionUtilisateur, oCategorieOptionUtilisateur)
                ChargerPropriete(oOptionUtilisateur.nidClient, readerSQL.Item("idClient"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerOptionParIdProfil(ByRef oOptionProfil As OptionProfil, ByVal nIdOptionProfil As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oCategorieOptionUtilisateur As CategorieOptionUtilisateur

        Try
            Requete = "SELECT dbo.[Option].idOption, dbo.[Option].libelle, dbo.[Option].idCategorieOption, dbo.[Option].idClient "
            Requete += " FROM dbo.[Option] "
            Requete += " WHERE dbo.[Option].idOption = " + nIdOptionProfil.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oCategorieOptionUtilisateur = New CategorieOptionUtilisateur
                ChargerPropriete(oOptionProfil.nIdOptionProfil, readerSQL.Item("idOption"))
                ChargerPropriete(oOptionProfil.sLibelle, readerSQL.Item("libelle"))
                oCategorieOptionUtilisateur.chargerCategorieOptionUtilisateurParId(CInt(readerSQL.Item("idCategorieOption")))
                ChargerPropriete(oOptionProfil.oCategorieOptionUtilisateur, oCategorieOptionUtilisateur)
                'ChargerPropriete(oOptionProfil.nidProfil, readerSQL.Item("idProfil"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub enleverOption(ByVal oLigne As Ligne, ByVal oOptionUtilisateur As OptionUtilisateur)
        Dim transaction As SqlTransaction
        Dim requete As String = "DELETE FROM LiaisonOption WHERE idLigne=@idLigne AND idOption=@idOption"

        Dim sql As New cSQL("GestionParc"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@idLigne", SqlDbType.Int).Value = oLigne.nIdLigne
            cmd.Parameters.Add("@idOption", SqlDbType.Int).Value = oOptionUtilisateur.nIdOption

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub ajouterCommande(ByVal oCommande As Commande)
        Dim sql As New cSQL("GestionParc")
        Dim transaction As SqlTransaction = Nothing

        Dim requete1 As String = "INSERT INTO Commande"
        requete1 += " (typeCommande, estAffichable, estRealiseFacture, estTelecharge, idLigne, idUtilisateurPrincipal, idUtilisateurSecondaire"
        If Not oCommande.oAncienneLigne Is Nothing Then
            requete1 += ", ancienNom, ancienPrenom, ancienNumeroData, ancienIdFonction, ancienIdCompteFacturant, ancienIdDiscrimination, ancienIdForfait, ancienIdHierarchie"
        End If
        If Not oCommande.oNouvelleLigne Is Nothing Then
            requete1 += ", nouveauNom, nouveauPrenom, nouveauNumeroData, nouveauiIdFonction, nouveauIdCompteFacturant , nouveauIdDiscrimination , nouveauIdForfait, nouveauIdHierarchie"
        End If
        requete1 += ")"
        requete1 += " OUTPUT INSERTED.idCommande"
        requete1 += " VALUES  (@typeCommande, @estAffichable, @estRealiseFacture, @estTelecharge, @idLigne, @idUtilisateurPrincipal, @idUtilisateurSecondaire"
        If Not oCommande.oAncienneLigne Is Nothing Then
            requete1 += ",@ancienNom, @ancienPrenom, @ancienNumeroData, @ancienIdFonction, @ancienIdCompteFacturant, @ancienIdDiscrimination, @ancienIdForfait, @ancienIdHierarchie"
        End If
        If Not oCommande.oNouvelleLigne Is Nothing Then
            requete1 += ",@nouveauNom, @nouveauPrenom, @nouveauNumeroData ,@nouveauiIdFonction, @nouveauIdCompteFacturant, @nouveauIdDiscrimination, @nouveauIdForfait, @nouveauIdHierarchie"
        End If
        requete1 += ")"

        Try
            Dim cmd1 As New SqlCommand(requete1, sql.connexion)
            transaction = sql.connexion.BeginTransaction()
            cmd1.Transaction = transaction
            cmd1.Parameters.Add("@typeCommande", SqlDbType.VarChar, 50).Value = oCommande.sTypeCommande
            cmd1.Parameters.Add("@estAffichable", SqlDbType.Bit).Value = oCommande.bEstAffichable
            cmd1.Parameters.Add("@estRealiseFacture", SqlDbType.Bit).Value = oCommande.bEstRealiseFacture
            cmd1.Parameters.Add("@estTelecharge", SqlDbType.Bit).Value = oCommande.bEstTelecharge
            cmd1.Parameters.Add("@idUtilisateurPrincipal", SqlDbType.Int).Value = oCommande.nIdUtilisateurPrincipal
            cmd1.Parameters.Add("@idUtilisateurSecondaire", SqlDbType.Int).Value = oCommande.nIdUtilisateurSecondaire
            If Not oCommande.oAncienneLigne Is Nothing Then
                cmd1.Parameters.Add("@idLigne", SqlDbType.Int).Value = oCommande.oAncienneLigne.nIdLigne
                cmd1.Parameters.Add("@ancienNom", SqlDbType.VarChar, 150).Value = oCommande.oAncienneLigne.sNom
                cmd1.Parameters.Add("@ancienPrenom", SqlDbType.VarChar, 150).Value = oCommande.oAncienneLigne.sPrenom
                cmd1.Parameters.Add("@ancienNumeroData", SqlDbType.VarChar, 150).Value = oCommande.oAncienneLigne.sNumeroData
                cmd1.Parameters.Add("@ancienIdFonction", SqlDbType.Int).Value = oCommande.oAncienneLigne.oFonction.nIdFonction
                cmd1.Parameters.Add("@ancienIdCompteFacturant", SqlDbType.Int).Value = oCommande.oAncienneLigne.oCompteFacturant.nIdCompteFacturant
                cmd1.Parameters.Add("@ancienIdDiscrimination", SqlDbType.Int).Value = oCommande.oAncienneLigne.oDiscrimination.nIdDiscrimination
                cmd1.Parameters.Add("@ancienIdHierarchie", SqlDbType.Int).Value = oCommande.oAncienneLigne.oHierarchie.nIdHierarchie
                cmd1.Parameters.Add("@ancienIdForfait", SqlDbType.Int).Value = 140
                'cmd1.Parameters.Add("@ancienIdForfait", SqlDbType.Int).Value = oCommande.oAncienneLigne.oForfait.nIdForfait
            End If
            If Not oCommande.oNouvelleLigne Is Nothing Then
                If oCommande.oAncienneLigne Is Nothing Then
                    cmd1.Parameters.Add("@idLigne", SqlDbType.Int).Value = oCommande.oNouvelleLigne.nIdLigne
                End If
                cmd1.Parameters.Add("@nouveauNom", SqlDbType.VarChar, 150).Value = oCommande.oNouvelleLigne.sNom
                cmd1.Parameters.Add("@nouveauPrenom", SqlDbType.VarChar, 150).Value = oCommande.oNouvelleLigne.sPrenom
                cmd1.Parameters.Add("@nouveauNumeroData", SqlDbType.VarChar, 150).Value = oCommande.oNouvelleLigne.sNumeroData
                cmd1.Parameters.Add("@nouveauiIdFonction", SqlDbType.Int).Value = oCommande.oNouvelleLigne.oFonction.nIdFonction
                cmd1.Parameters.Add("@nouveauIdCompteFacturant", SqlDbType.Int).Value = oCommande.oNouvelleLigne.oCompteFacturant.nIdCompteFacturant
                cmd1.Parameters.Add("@nouveauIdHierarchie", SqlDbType.Int).Value = oCommande.oNouvelleLigne.oHierarchie.nIdHierarchie
                cmd1.Parameters.Add("@nouveauIdDiscrimination", SqlDbType.Int).Value = oCommande.oNouvelleLigne.oDiscrimination.nIdDiscrimination
                cmd1.Parameters.Add("@nouveauIdForfait", SqlDbType.Int).Value = 140
                'cmd1.Parameters.Add("@nouveauIdForfait", SqlDbType.Int).Value = oCommande.oNouvelleLigne.oForfait.nIdForfait
            End If
            oCommande.nIdCommande = CInt(cmd1.ExecuteScalar())


            If Not oCommande.oOptionsAjoutees Is Nothing Then
                For Each oOption In oCommande.oOptionsAjoutees
                    Dim requete2 As String = "INSERT INTO CommandeOption (idCommande, idOption, typeAction) VALUES (@idCommande, @idOption, @typeAction)"
                    Dim cmd2 As New SqlCommand(requete2, sql.connexion)
                    cmd2.Transaction = transaction
                    cmd2.Parameters.Add("@idCommande", SqlDbType.Int).Value = oCommande.nIdCommande
                    cmd2.Parameters.Add("@idOption", SqlDbType.Int).Value = oOption.nIdOption
                    cmd2.Parameters.Add("@typeAction", SqlDbType.VarChar, 50).Value = "Ajout"
                    cmd2.ExecuteNonQuery()
                Next
            End If
            If Not oCommande.oOptionsRetirees Is Nothing Then
                For Each oOption In oCommande.oOptionsRetirees
                    Dim requete2 As String = "INSERT INTO CommandeOption (idCommande, idOption, typeAction) VALUES (@idCommande, @idOption, @typeAction)"
                    Dim cmd2 As New SqlCommand(requete2, sql.connexion)
                    cmd2.Transaction = transaction
                    cmd2.Parameters.Add("@idCommande", SqlDbType.Int).Value = oCommande.nIdCommande
                    cmd2.Parameters.Add("@idOption", SqlDbType.Int).Value = oOption.nIdOption
                    cmd2.Parameters.Add("@typeAction", SqlDbType.VarChar, 50).Value = "Retrait"
                    cmd2.ExecuteNonQuery()
                Next
            End If
            If Not oCommande.oStockAjoutes Is Nothing Then
                For Each oStock In oCommande.oStockAjoutes
                    Dim requete2 As String = "INSERT INTO CommandeStock (idCommande, idStock, typeAction) VALUES (@idCommande, @idStock, @typeAction)"
                    Dim cmd2 As New SqlCommand(requete2, sql.connexion)
                    cmd2.Transaction = transaction
                    cmd2.Parameters.Add("@idCommande", SqlDbType.Int).Value = oCommande.nIdCommande
                    cmd2.Parameters.Add("@idStock", SqlDbType.Int).Value = oStock.nIdArticleStock
                    cmd2.Parameters.Add("@typeAction", SqlDbType.VarChar, 50).Value = "Ajout"
                    cmd2.ExecuteNonQuery()
                Next
            End If
            If Not oCommande.oStockRetires Is Nothing Then
                For Each oStock In oCommande.oStockRetires
                    Dim requete2 As String = "INSERT INTO CommandeStock (idCommande, idStock, typeAction) VALUES (@idCommande, @idStock, @typeAction)"
                    Dim cmd2 As New SqlCommand(requete2, sql.connexion)
                    cmd2.Transaction = transaction
                    cmd2.Parameters.Add("@idCommande", SqlDbType.Int).Value = oCommande.nIdCommande
                    cmd2.Parameters.Add("@idStock", SqlDbType.Int).Value = oStock.nIdArticleStock
                    cmd2.Parameters.Add("@typeAction", SqlDbType.VarChar, 50).Value = "Retrait"
                    cmd2.ExecuteNonQuery()
                Next
            End If

            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub ajouterCommandeFixe(ByVal oCommande As CommandeFixe)
        Dim sql As New cSQL("GestionParcFixe")
        Dim transaction As SqlTransaction = Nothing

        Dim requete1 As String = "INSERT INTO Commande"
        requete1 += " ("
        requete1 += Toolbox.AjouterChampRequete("idUtilisateurPrincipal", "numerique", oCommande.nIdUtilisateurPrincipal)
        requete1 += Toolbox.AjouterChampRequete("idUtilisateurSecondaire", "numerique", oCommande.nIdUtilisateurSecondaire)
        requete1 += Toolbox.AjouterChampRequete("typeCommande", "texte", oCommande.sTypeCommande)
        requete1 += Toolbox.AjouterChampRequete("estRealiseFacture", "booleen", oCommande.bEstRealiseFacture)
        requete1 += Toolbox.AjouterChampRequete("estTelecharge", "booleen", oCommande.bEstTelecharge)
        requete1 += Toolbox.AjouterChampRequete("idLigne", "numerique", oCommande.oLigneFixe.nIdLigneFixe)
        requete1 += Toolbox.AjouterChampRequete("accesReseau_contratLigne", "texte", oCommande.oLigneFixe.oContratFixe.sLibelleContrat)
        requete1 += Toolbox.AjouterChampRequete("accesReseau_numero", "texte", oCommande.oLigneFixe.sNumeroLigneFixe)
        requete1 += Toolbox.AjouterChampRequete("accesReseau_resilliationPartielleInstallation", "booleen", oCommande.bResiliationPartielle)
        requete1 += Toolbox.AjouterChampRequete("accesReseau_nombreAcces", "numerique", oCommande.oLigneFixe.nNombreAcces)
        requete1 += Toolbox.AjouterChampRequete("accesReseau_nombreSda", "numerique", oCommande.oLigneFixe.nNombreSDA)
        requete1 += Toolbox.AjouterChampRequete("accesReseau_specialisationCanauxArrivee", "numerique", oCommande.oLigneFixe.nSpecialisationCanauxArrive)
        requete1 += Toolbox.AjouterChampRequete("accesReseau_specialisationCanauxMixte", "numerique", oCommande.oLigneFixe.nSpecialisationCanauxMixte)
        requete1 += Toolbox.AjouterChampRequete("accesReseau_specialisationCanauxDepart", "numerique", oCommande.oLigneFixe.nSpecialisationCanauxDepart)
        requete1 += Toolbox.AjouterChampRequete("adresseInstallation_nomEtablissement", "texte", oCommande.oLigneFixe.sNomEtablissement)
        requete1 += Toolbox.AjouterChampRequete("adresseInstallation_adresse", "texte", oCommande.oLigneFixe.sAdresse)
        requete1 += Toolbox.AjouterChampRequete("adresseInstallation_codePostal", "texte", oCommande.oLigneFixe.sCodePostal)
        requete1 += Toolbox.AjouterChampRequete("adresseInstallation_localite", "texte", oCommande.oLigneFixe.sLocalite)
        requete1 += Toolbox.AjouterChampRequete("adresseInstallation_complementAdresse", "texte", oCommande.oLigneFixe.sComplementAdresse)
        requete1 += Toolbox.AjouterChampRequete("contactInstallation_nom", "texte", oCommande.oLigneFixe.sNomContact)
        requete1 += Toolbox.AjouterChampRequete("contactInstallation_mobile", "texte", oCommande.oLigneFixe.sMobileContact)
        requete1 += Toolbox.AjouterChampRequete("contactInstallation_telephone", "texte", oCommande.oLigneFixe.sFixeContact)
        requete1 += Toolbox.AjouterChampRequete("contactInstallation_email", "texte", oCommande.oLigneFixe.sMailContact)
        requete1 += Toolbox.AjouterChampRequete("dateDisposition_dateDispositionLocaux", "date", oCommande.oLigneFixe.dDateDispositionLocaux)
        requete1 += Toolbox.AjouterChampRequete("dateDisposition_dateMiseService", "date", oCommande.oLigneFixe.dDateMiseEnService)
        requete1 += Toolbox.AjouterChampRequete("facturation_compteFacturantLigne", "texte", oCommande.oLigneFixe.oCompteFacturantFixe.sLibelleCompteFacturant)
        requete1 += Toolbox.AjouterChampRequete("restitutionEquipement", "booleen", oCommande.oLigneFixe.bRestitutionEquipementLoues)
        requete1 += Toolbox.AjouterChampRequete("servicesTelephonie_MessagerieVocale", "booleen", oCommande.oLigneFixe.bStMessagerieVocale)
        requete1 += Toolbox.AjouterChampRequete("servicesTelephonie_TransfertAppel", "booleen", oCommande.oLigneFixe.bStTransfertAppel)
        requete1 += Toolbox.AjouterChampRequete("servicesTelephonie_SecretPermanent", "booleen", oCommande.oLigneFixe.bStSecretPermanent)
        requete1 += Toolbox.AjouterChampRequete("servicesTelephonie_TransfertAppelNonReponse", "booleen", oCommande.oLigneFixe.bStTransfertAppelNonReponse)
        requete1 += Toolbox.AjouterChampRequete("servicesTelephonie_NumeroRenvoi", "texte", oCommande.oLigneFixe.sStNumeroRenvoi)
        requete1 += Toolbox.AjouterChampRequete("servicesTelephonie_RenvoiTerminal", "booleen", oCommande.oLigneFixe.bStRenvoiTerminal)
        requete1 += Toolbox.AjouterChampRequete("servicesTelephonie_TypeSelection", "texte", oCommande.oLigneFixe.oTypeSelectionFixe.sLibelleTypeSelection)
        requete1 += Toolbox.AjouterChampRequete("annuaire_ChoixListe", "texte", oCommande.oLigneFixe.oChoixListeFixe.sLibelleChoixListe)
        requete1 += Toolbox.AjouterChampRequete("annuaire_PrecisionsParution", "texte", oCommande.oLigneFixe.sIaPrecisionsComplementairesParution)
        requete1 += Toolbox.AjouterChampRequete("choixModem_typeModem", "texte", oCommande.oLigneFixe.sChoixModem)
        requete1 += Toolbox.AjouterChampRequete("adresseEmail_type", "texte", oCommande.oLigneFixe.sTypeAdresseEmail)
        requete1 += Toolbox.AjouterChampRequete("adresseEmail_adresse", "texte", oCommande.oLigneFixe.sAdresseEmail)
        requete1 += Toolbox.AjouterChampRequete("commentaires", "texte", oCommande.oLigneFixe.sCommentaire)
        requete1 += Toolbox.AjouterChampRequete("referenceClient", "texte", oCommande.oLigneFixe.sReferenceClient)
        If requete1 <> "" Then requete1 = requete1.Remove(requete1.Length - 2, 2)

        requete1 += ")"
        requete1 += " OUTPUT INSERTED.idCommande"
        requete1 += " VALUES  ("
        requete1 += Toolbox.AjouterChampRequete("@idUtilisateurPrincipal", "numerique", oCommande.nIdUtilisateurPrincipal)
        requete1 += Toolbox.AjouterChampRequete("@idUtilisateurSecondaire", "numerique", oCommande.nIdUtilisateurSecondaire)
        requete1 += Toolbox.AjouterChampRequete("@typeCommande", "texte", oCommande.sTypeCommande)
        requete1 += Toolbox.AjouterChampRequete("@estRealiseFacture", "booleen", oCommande.bEstRealiseFacture)
        requete1 += Toolbox.AjouterChampRequete("@estTelecharge", "booleen", oCommande.bEstTelecharge)
        requete1 += Toolbox.AjouterChampRequete("@idLigne", "numerique", oCommande.oLigneFixe.nIdLigneFixe)
        requete1 += Toolbox.AjouterChampRequete("@accesReseau_contratLigne", "texte", oCommande.oLigneFixe.oContratFixe.sLibelleContrat)
        requete1 += Toolbox.AjouterChampRequete("@accesReseau_numero", "texte", oCommande.oLigneFixe.sNumeroLigneFixe)
        requete1 += Toolbox.AjouterChampRequete("@accesReseau_resilliationPartielleInstallation", "booleen", oCommande.bResiliationPartielle)
        requete1 += Toolbox.AjouterChampRequete("@accesReseau_nombreAcces", "numerique", oCommande.oLigneFixe.nNombreAcces)
        requete1 += Toolbox.AjouterChampRequete("@accesReseau_nombreSda", "numerique", oCommande.oLigneFixe.nNombreSDA)
        requete1 += Toolbox.AjouterChampRequete("@accesReseau_specialisationCanauxArrivee", "numerique", oCommande.oLigneFixe.nSpecialisationCanauxArrive)
        requete1 += Toolbox.AjouterChampRequete("@accesReseau_specialisationCanauxMixte", "numerique", oCommande.oLigneFixe.nSpecialisationCanauxMixte)
        requete1 += Toolbox.AjouterChampRequete("@accesReseau_specialisationCanauxDepart", "numerique", oCommande.oLigneFixe.nSpecialisationCanauxDepart)
        requete1 += Toolbox.AjouterChampRequete("@adresseInstallation_nomEtablissement", "texte", oCommande.oLigneFixe.sNomEtablissement)
        requete1 += Toolbox.AjouterChampRequete("@adresseInstallation_adresse", "texte", oCommande.oLigneFixe.sAdresse)
        requete1 += Toolbox.AjouterChampRequete("@adresseInstallation_codePostal", "texte", oCommande.oLigneFixe.sCodePostal)
        requete1 += Toolbox.AjouterChampRequete("@adresseInstallation_localite", "texte", oCommande.oLigneFixe.sLocalite)
        requete1 += Toolbox.AjouterChampRequete("@adresseInstallation_complementAdresse", "texte", oCommande.oLigneFixe.sComplementAdresse)
        requete1 += Toolbox.AjouterChampRequete("@contactInstallation_nom", "texte", oCommande.oLigneFixe.sNomContact)
        requete1 += Toolbox.AjouterChampRequete("@contactInstallation_mobile", "texte", oCommande.oLigneFixe.sMobileContact)
        requete1 += Toolbox.AjouterChampRequete("@contactInstallation_telephone", "texte", oCommande.oLigneFixe.sFixeContact)
        requete1 += Toolbox.AjouterChampRequete("@contactInstallation_email", "texte", oCommande.oLigneFixe.sMailContact)
        requete1 += Toolbox.AjouterChampRequete("@dateDisposition_dateDispositionLocaux", "date", oCommande.oLigneFixe.dDateDispositionLocaux)
        requete1 += Toolbox.AjouterChampRequete("@dateDisposition_dateMiseService", "date", oCommande.oLigneFixe.dDateMiseEnService)
        requete1 += Toolbox.AjouterChampRequete("@facturation_compteFacturantLigne", "texte", oCommande.oLigneFixe.oCompteFacturantFixe.sLibelleCompteFacturant)
        requete1 += Toolbox.AjouterChampRequete("@restitutionEquipement", "booleen", oCommande.oLigneFixe.bRestitutionEquipementLoues)
        requete1 += Toolbox.AjouterChampRequete("@servicesTelephonie_MessagerieVocale", "booleen", oCommande.oLigneFixe.bStMessagerieVocale)
        requete1 += Toolbox.AjouterChampRequete("@servicesTelephonie_TransfertAppel", "booleen", oCommande.oLigneFixe.bStTransfertAppel)
        requete1 += Toolbox.AjouterChampRequete("@servicesTelephonie_SecretPermanent", "booleen", oCommande.oLigneFixe.bStSecretPermanent)
        requete1 += Toolbox.AjouterChampRequete("@servicesTelephonie_TransfertAppelNonReponse", "booleen", oCommande.oLigneFixe.bStTransfertAppelNonReponse)
        requete1 += Toolbox.AjouterChampRequete("@servicesTelephonie_NumeroRenvoi", "texte", oCommande.oLigneFixe.sStNumeroRenvoi)
        requete1 += Toolbox.AjouterChampRequete("@servicesTelephonie_RenvoiTerminal", "booleen", oCommande.oLigneFixe.bStRenvoiTerminal)
        requete1 += Toolbox.AjouterChampRequete("@servicesTelephonie_TypeSelection", "texte", oCommande.oLigneFixe.oTypeSelectionFixe.sLibelleTypeSelection)
        requete1 += Toolbox.AjouterChampRequete("@annuaire_ChoixListe", "texte", oCommande.oLigneFixe.oChoixListeFixe.sLibelleChoixListe)
        requete1 += Toolbox.AjouterChampRequete("@annuaire_PrecisionsParution", "texte", oCommande.oLigneFixe.sIaPrecisionsComplementairesParution)
        requete1 += Toolbox.AjouterChampRequete("@choixModem_typeModem", "texte", oCommande.oLigneFixe.sChoixModem)
        requete1 += Toolbox.AjouterChampRequete("@adresseEmail_type", "texte", oCommande.oLigneFixe.sTypeAdresseEmail)
        requete1 += Toolbox.AjouterChampRequete("@adresseEmail_adresse", "texte", oCommande.oLigneFixe.sAdresseEmail)
        requete1 += Toolbox.AjouterChampRequete("@commentaires", "texte", oCommande.oLigneFixe.sCommentaire)
        requete1 += Toolbox.AjouterChampRequete("@referenceClient", "texte", oCommande.oLigneFixe.sReferenceClient)
        If requete1 <> "" Then requete1 = requete1.Remove(requete1.Length - 2, 2)

        requete1 += ")"

        Try
            Dim cmd1 As New SqlCommand(requete1, sql.connexion)
            transaction = sql.connexion.BeginTransaction()
            cmd1.Transaction = transaction


            Toolbox.AjouterParametreRequete(cmd1, "@typeCommande", "VarChar", oCommande.sTypeCommande, 50)
            Toolbox.AjouterParametreRequete(cmd1, "@estRealiseFacture", "Bit", oCommande.bEstRealiseFacture)
            Toolbox.AjouterParametreRequete(cmd1, "@estTelecharge", "Bit", oCommande.bEstTelecharge)
            Toolbox.AjouterParametreRequete(cmd1, "@idLigne", "Int", oCommande.oLigneFixe.nIdLigneFixe)
            Toolbox.AjouterParametreRequete(cmd1, "@accesReseau_contratLigne", "VarChar", oCommande.oLigneFixe.oContratFixe.sLibelleContrat, 250)
            Toolbox.AjouterParametreRequete(cmd1, "@accesReseau_numero", "VarChar", oCommande.oLigneFixe.sNumeroLigneFixe, 250)
            Toolbox.AjouterParametreRequete(cmd1, "@accesReseau_resilliationPartielleInstallation", "Bit", oCommande.bResiliationPartielle)
            Toolbox.AjouterParametreRequete(cmd1, "@accesReseau_nombreAcces", "Int", oCommande.oLigneFixe.nNombreAcces)
            Toolbox.AjouterParametreRequete(cmd1, "@accesReseau_nombreSda", "Int", oCommande.oLigneFixe.nNombreSDA)
            Toolbox.AjouterParametreRequete(cmd1, "@accesReseau_specialisationCanauxArrivee", "Int", oCommande.oLigneFixe.nSpecialisationCanauxArrive)
            Toolbox.AjouterParametreRequete(cmd1, "@accesReseau_specialisationCanauxMixte", "Int", oCommande.oLigneFixe.nSpecialisationCanauxMixte)
            Toolbox.AjouterParametreRequete(cmd1, "@accesReseau_specialisationCanauxDepart", "Int", oCommande.oLigneFixe.nSpecialisationCanauxDepart)
            Toolbox.AjouterParametreRequete(cmd1, "@adresseInstallation_nomEtablissement", "VarChar", oCommande.oLigneFixe.sNomEtablissement, 250)
            Toolbox.AjouterParametreRequete(cmd1, "@adresseInstallation_adresse", "VarChar", oCommande.oLigneFixe.sAdresse, 250)
            Toolbox.AjouterParametreRequete(cmd1, "@adresseInstallation_codePostal", "VarChar", oCommande.oLigneFixe.sCodePostal, 25)
            Toolbox.AjouterParametreRequete(cmd1, "@adresseInstallation_localite", "VarChar", oCommande.oLigneFixe.sLocalite, 250)
            Toolbox.AjouterParametreRequete(cmd1, "@adresseInstallation_complementAdresse", "VarChar", oCommande.oLigneFixe.sComplementAdresse, 1000)
            Toolbox.AjouterParametreRequete(cmd1, "@contactInstallation_nom", "VarChar", oCommande.oLigneFixe.sNomContact, 250)
            Toolbox.AjouterParametreRequete(cmd1, "@contactInstallation_mobile", "VarChar", oCommande.oLigneFixe.sMobileContact, 15)
            Toolbox.AjouterParametreRequete(cmd1, "@contactInstallation_telephone", "VarChar", oCommande.oLigneFixe.sFixeContact, 15)
            Toolbox.AjouterParametreRequete(cmd1, "@contactInstallation_email", "VarChar", oCommande.oLigneFixe.sMailContact, 250)
            Toolbox.AjouterParametreRequete(cmd1, "@dateDisposition_dateDispositionLocaux", "Datetime", oCommande.oLigneFixe.dDateDispositionLocaux)
            Toolbox.AjouterParametreRequete(cmd1, "@dateDisposition_dateMiseService", "Datetime", oCommande.oLigneFixe.dDateMiseEnService)
            Toolbox.AjouterParametreRequete(cmd1, "@facturation_compteFacturantLigne", "VarChar", oCommande.oLigneFixe.oCompteFacturantFixe.sLibelleCompteFacturant, 150)
            Toolbox.AjouterParametreRequete(cmd1, "@restitutionEquipement", "Bit", oCommande.oLigneFixe.bRestitutionEquipementLoues)
            Toolbox.AjouterParametreRequete(cmd1, "@servicesTelephonie_MessagerieVocale", "Bit", oCommande.oLigneFixe.bStMessagerieVocale)
            Toolbox.AjouterParametreRequete(cmd1, "@servicesTelephonie_TransfertAppel", "Bit", oCommande.oLigneFixe.bStTransfertAppel)
            Toolbox.AjouterParametreRequete(cmd1, "@servicesTelephonie_SecretPermanent", "Bit", oCommande.oLigneFixe.bStSecretPermanent)
            Toolbox.AjouterParametreRequete(cmd1, "@servicesTelephonie_TransfertAppelNonReponse", "Bit", oCommande.oLigneFixe.bStTransfertAppelNonReponse)
            Toolbox.AjouterParametreRequete(cmd1, "@servicesTelephonie_NumeroRenvoi", "VarChar", oCommande.oLigneFixe.sStNumeroRenvoi, 50)
            Toolbox.AjouterParametreRequete(cmd1, "@servicesTelephonie_RenvoiTerminal", "Bit", oCommande.oLigneFixe.bStRenvoiTerminal)
            Toolbox.AjouterParametreRequete(cmd1, "@servicesTelephonie_TypeSelection", "VarChar", oCommande.oLigneFixe.oTypeSelectionFixe.sLibelleTypeSelection, 50)
            Toolbox.AjouterParametreRequete(cmd1, "@annuaire_ChoixListe", "VarChar", oCommande.oLigneFixe.oChoixListeFixe.sLibelleChoixListe, 50)
            Toolbox.AjouterParametreRequete(cmd1, "@annuaire_PrecisionsParution", "VarChar", oCommande.oLigneFixe.sIaPrecisionsComplementairesParution, 1000)
            Toolbox.AjouterParametreRequete(cmd1, "@choixModem_typeModem", "VarChar", oCommande.oLigneFixe.sChoixModem, 250)
            Toolbox.AjouterParametreRequete(cmd1, "@adresseEmail_type", "VarChar", oCommande.oLigneFixe.sTypeAdresseEmail, 250)
            Toolbox.AjouterParametreRequete(cmd1, "@adresseEmail_adresse", "VarChar", oCommande.oLigneFixe.sAdresseEmail, 250)
            Toolbox.AjouterParametreRequete(cmd1, "@commentaires", "VarChar", oCommande.oLigneFixe.sCommentaire, 1000)
            Toolbox.AjouterParametreRequete(cmd1, "@idUtilisateurPrincipal", "Int", oCommande.nIdUtilisateurPrincipal)
            Toolbox.AjouterParametreRequete(cmd1, "@idUtilisateurSecondaire", "Int", oCommande.nIdUtilisateurSecondaire)
            Toolbox.AjouterParametreRequete(cmd1, "@referenceClient", "VarChar", oCommande.oLigneFixe.sReferenceClient, 250)
            oCommande.nIdCommande = CInt(cmd1.ExecuteScalar())




            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub PasserEnTelechargeCommande(ByVal oCommande As Commande, ByVal sNumeroCommande As String)
        Dim transaction As SqlTransaction
        Dim requete As String = "UPDATE Commande Set numeroCommande = @sNumeroCommande, estTelecharge = 1 WHERE idCommande = @idCommande"

        Dim sql As New cSQL("GestionParc"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@sNumeroCommande", SqlDbType.VarChar, 50).Value = sNumeroCommande
            cmd.Parameters.Add("@idCommande", SqlDbType.Int).Value = oCommande.nIdCommande

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub PasserEnTelechargeCommandeFixe(ByVal oCommandeFixe As CommandeFixe)
        Dim transaction As SqlTransaction
        Dim requete As String = "UPDATE Commande Set  estTelecharge = 1 WHERE idCommande = @idCommande"

        Dim sql As New cSQL("GestionParcFixe"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try

            cmd.Parameters.Add("@idCommande", SqlDbType.Int).Value = oCommandeFixe.nIdCommande

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub


    Public Shared Sub ajouterOption(ByVal oLigne As Ligne, ByVal oOptionUtilisateur As OptionUtilisateur)
        Dim transaction As SqlTransaction
        Dim requete As String = "insert into LiaisonOption (idLigne, idOption, dateAttribution) values(@idLigne,@idOption,getdate())"

        Dim sql As New cSQL("GestionParc"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@idLigne", SqlDbType.Int).Value = oLigne.nIdLigne
            cmd.Parameters.Add("@idOption", SqlDbType.Int).Value = oOptionUtilisateur.nIdOption

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub ajouterStock(ByVal oLigne As Ligne, ByVal oStock As Stock)
        Dim transaction As SqlTransaction = Nothing
        Dim requete1 As String = "INSERT INTO LiaisonStock (idLigne, idArticleStock, typeLiaison, dateAttribution) VALUES (@idLigne, @idArticleStock, @typeLiaison, GETDATE())"
        Dim requete2 As String = "UPDATE Stock Set etat = @etat, dernierDetenteur = @dernierDetenteur WHERE (idArticleStock = @idArticleStock)"

        Dim sql As New cSQL("GestionParc")

        Try
            Dim cmd1 As New SqlCommand(requete1, sql.connexion)

            transaction = sql.connexion.BeginTransaction()

            cmd1.Transaction = transaction
            cmd1.Parameters.Add("@idLigne", SqlDbType.Int).Value = oLigne.nIdLigne
            cmd1.Parameters.Add("@idArticleStock", SqlDbType.Int).Value = oStock.nIdArticleStock
            cmd1.Parameters.Add("@typeLiaison", SqlDbType.VarChar, 50).Value = oStock.sTypeLiaison
            cmd1.ExecuteNonQuery()

            Dim cmd2 As New SqlCommand(requete2, sql.connexion)
            cmd2.Transaction = transaction
            cmd2.Parameters.Add("@idArticleStock", SqlDbType.Int).Value = oStock.nIdArticleStock
            cmd2.Parameters.Add("@etat", SqlDbType.VarChar, 50).Value = "Attribué"
            cmd2.Parameters.Add("@dernierDetenteur", SqlDbType.VarChar, 50).Value = oLigne.sNom & " " & oLigne.sPrenom
            cmd2.ExecuteNonQuery()

            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub enleverStock(ByVal oLigne As Ligne, ByVal oStock As Stock)
        Dim transaction As SqlTransaction = Nothing
        Dim requete1 As String = "DELETE FROM LiaisonStock WHERE idLigne = @idLigne And idArticleStock = @idArticleStock"
        Dim requete2 As String = "UPDATE Stock Set etat = @etat WHERE (idArticleStock = @idArticleStock)"

        Dim sql As New cSQL("GestionParc")

        Try
            Dim cmd1 As New SqlCommand(requete1, sql.connexion)

            transaction = sql.connexion.BeginTransaction()

            cmd1.Transaction = transaction
            cmd1.Parameters.Add("@idLigne", SqlDbType.Int).Value = oLigne.nIdLigne
            cmd1.Parameters.Add("@idArticleStock", SqlDbType.Int).Value = oStock.nIdArticleStock
            cmd1.ExecuteNonQuery()

            Dim cmd2 As New SqlCommand(requete2, sql.connexion)
            cmd2.Transaction = transaction
            cmd2.Parameters.Add("@idArticleStock", SqlDbType.Int).Value = oStock.nIdArticleStock
            cmd2.Parameters.Add("@etat", SqlDbType.VarChar, 50).Value = "Non Attribué"
            cmd2.ExecuteNonQuery()

            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub chargerLigneParId(ByRef oLigne As Ligne, ByVal nIdLigne As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim readerSQL As SqlDataReader = Nothing
        Dim requete As String
        Dim oFonction As New Fonction
        Dim oCompteFacturant As New CompteFacturant
        Dim oDiscrimination As New Discrimination
        Dim oForfait As New Forfait
        Dim oHierarchie As New Hierarchie
        Dim oOptionsUtilisateur As New OptionsUtilisateur
        Dim oStocks As New Stocks
        Try
            requete = "Select"
            requete += " idLigne,"
            requete += " numeroGsm,"
            requete += " numeroData,"
            requete += " nom,"
            requete += " prenom,"
            requete += " estActif,"
            requete += " dateActivation,"
            requete += " dateDesactivation,"
            requete += " idFonction,"
            requete += " idCompteFacturant,"
            requete += " idDiscrimination,"
            requete += " idForfait,"
            requete += " idHierarchie,"
            requete += " idClient"
            requete += " FROM Ligne"
            requete += " WHERE idLigne=" + nIdLigne.ToString


            readerSQL = sql.chargerDataReader(requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oLigne.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oLigne.nIdLigne, readerSQL.Item("idLigne"))
                ChargerPropriete(oLigne.sNumeroGSM, readerSQL.Item("numeroGsm"))
                ChargerPropriete(oLigne.sNumeroData, readerSQL.Item("numeroData"))
                ChargerPropriete(oLigne.sNom, readerSQL.Item("nom"))
                ChargerPropriete(oLigne.sPrenom, readerSQL.Item("prenom"))
                ChargerPropriete(oLigne.bEstActif, readerSQL.Item("estActif"))
                ChargerPropriete(oLigne.dDateActivation, readerSQL.Item("dateActivation"))
                ChargerPropriete(oLigne.dDateDesactivation, readerSQL.Item("dateDesactivation"))
                oFonction.chargerFonctionParId(CInt(readerSQL.Item("idFonction")))
                ChargerPropriete(oLigne.oFonction, oFonction)
                oCompteFacturant.chargerCompteFacturantParId(CInt(readerSQL.Item("idCompteFacturant")))
                ChargerPropriete(oLigne.oCompteFacturant, oCompteFacturant)
                oDiscrimination.chargerDiscriminationParId(CInt(readerSQL.Item("idDiscrimination")))
                ChargerPropriete(oLigne.oDiscrimination, oDiscrimination)
                oForfait.chargerForfaitParId(CInt(readerSQL.Item("idForfait")))
                ChargerPropriete(oLigne.oForfait, oForfait)
                oHierarchie.chargerHierarchieParId(CInt(readerSQL.Item("idHierarchie")))
                ChargerPropriete(oLigne.oHierarchie, oHierarchie)

                oOptionsUtilisateur.chargerOptionsUtilisateurParIdLigne(CInt(readerSQL.Item("idLigne")))
                ChargerPropriete(oLigne.oOptionsUtilisateur, oOptionsUtilisateur)
                oStocks.chargerStocksUtilisateurParIdLigne(CInt(readerSQL.Item("idLigne")))
                ChargerPropriete(oLigne.oStocks, oStocks)
            Loop

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerLigneStructureParId(ByRef oLigne As Ligne, ByVal nIdLigne As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim readerSQL As SqlDataReader = Nothing
        Dim requete As String
        Dim oFonction As New Fonction
        Dim oCompteFacturant As New CompteFacturant
        Dim oDiscrimination As New Discrimination
        Dim oForfait As New Forfait
        Dim oHierarchie As New Hierarchie
        Dim oOptionsUtilisateur As New OptionsUtilisateur
        Dim oStocks As New Stocks
        Try
            requete = "Select"
            requete += " idLigne,"
            requete += " numeroGsm,"
            requete += " numeroData,"
            requete += " nom,"
            requete += " prenom,"
            requete += " estActif,"
            requete += " dateActivation,"
            requete += " dateDesactivation,"
            requete += " idFonction,"
            requete += " idCompteFacturant,"
            requete += " idDiscrimination,"
            requete += " idForfait,"
            requete += " idHierarchie,"
            requete += " idClient"
            requete += " FROM LigneCNES"
            requete += " WHERE idHierarchie=" + nIdLigne.ToString
            requete += " AND VIP = '1'"


            readerSQL = sql.chargerDataReader(requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oLigne.nIdClient, readerSQL.Item("idClient"))
                ChargerPropriete(oLigne.nIdLigne, readerSQL.Item("idLigne"))
                ChargerPropriete(oLigne.sNumeroGSM, readerSQL.Item("numeroGsm"))
                ChargerPropriete(oLigne.sNumeroData, readerSQL.Item("numeroData"))
                ChargerPropriete(oLigne.sNom, readerSQL.Item("nom"))
                ChargerPropriete(oLigne.sPrenom, readerSQL.Item("prenom"))
                ChargerPropriete(oLigne.bEstActif, readerSQL.Item("estActif"))
                ChargerPropriete(oLigne.dDateActivation, readerSQL.Item("dateActivation"))
                ChargerPropriete(oLigne.dDateDesactivation, readerSQL.Item("dateDesactivation"))
                oFonction.chargerFonctionParId(CInt(readerSQL.Item("idFonction")))
                ChargerPropriete(oLigne.oFonction, oFonction)
                oCompteFacturant.chargerCompteFacturantParId(CInt(readerSQL.Item("idCompteFacturant")))
                ChargerPropriete(oLigne.oCompteFacturant, oCompteFacturant)
                oDiscrimination.chargerDiscriminationParId(CInt(readerSQL.Item("idDiscrimination")))
                ChargerPropriete(oLigne.oDiscrimination, oDiscrimination)
                oForfait.chargerForfaitParId(CInt(readerSQL.Item("idForfait")))
                ChargerPropriete(oLigne.oForfait, oForfait)
                oHierarchie.chargerHierarchieParId(CInt(readerSQL.Item("idHierarchie")))
                ChargerPropriete(oLigne.oHierarchie, oHierarchie)

                oOptionsUtilisateur.chargerOptionsUtilisateurParIdLigne(CInt(readerSQL.Item("idLigne")))
                ChargerPropriete(oLigne.oOptionsUtilisateur, oOptionsUtilisateur)
                oStocks.chargerStocksUtilisateurParIdLigne(CInt(readerSQL.Item("idLigne")))
                ChargerPropriete(oLigne.oStocks, oStocks)
            Loop

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerHierarchieParId(ByRef ohierarchie As Hierarchie, ByVal nIdHierarchie As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "Select        idHierarchie, hierarchie1, hierarchie2, hierarchie3, hierarchie4, hierarchie5, hierarchie6, hierarchie7, hierarchie8, hierarchie9"
            Requete &= " FROM Hierarchie"
            Requete &= " WHERE idHierarchie =" + nIdHierarchie.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(ohierarchie.nIdHierarchie, readerSQL.Item("idHierarchie"))
                ChargerPropriete(ohierarchie.sHierarchie1, readerSQL.Item("hierarchie1"))
                ChargerPropriete(ohierarchie.sHierarchie2, readerSQL.Item("hierarchie2"))
                ChargerPropriete(ohierarchie.sHierarchie3, readerSQL.Item("hierarchie3"))
                ChargerPropriete(ohierarchie.sHierarchie4, readerSQL.Item("hierarchie4"))
                ChargerPropriete(ohierarchie.sHierarchie5, readerSQL.Item("hierarchie5"))
                ChargerPropriete(ohierarchie.sHierarchie6, readerSQL.Item("hierarchie6"))
                ChargerPropriete(ohierarchie.sHierarchie7, readerSQL.Item("hierarchie7"))
                ChargerPropriete(ohierarchie.sHierarchie8, readerSQL.Item("hierarchie8"))
                ChargerPropriete(ohierarchie.sHierarchie9, readerSQL.Item("hierarchie9"))

            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerHierarchieFixeParId(ByRef ohierarchieFixe As HierarchieFixe, ByVal nIdHierarchieFixe As Integer)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "Select        idHierarchie, hierarchie1, hierarchie2, hierarchie3, hierarchie4, hierarchie5, hierarchie6, hierarchie7, hierarchie8, hierarchie9"
            Requete &= " FROM Hierarchie"
            Requete &= " WHERE idHierarchie =" + nIdHierarchieFixe.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(ohierarchieFixe.nIdHierarchie, readerSQL.Item("idHierarchie"))
                ChargerPropriete(ohierarchieFixe.sHierarchie1, readerSQL.Item("hierarchie1"))
                ChargerPropriete(ohierarchieFixe.sHierarchie2, readerSQL.Item("hierarchie2"))
                ChargerPropriete(ohierarchieFixe.sHierarchie3, readerSQL.Item("hierarchie3"))
                ChargerPropriete(ohierarchieFixe.sHierarchie4, readerSQL.Item("hierarchie4"))
                ChargerPropriete(ohierarchieFixe.sHierarchie5, readerSQL.Item("hierarchie5"))
                ChargerPropriete(ohierarchieFixe.sHierarchie6, readerSQL.Item("hierarchie6"))
                ChargerPropriete(ohierarchieFixe.sHierarchie7, readerSQL.Item("hierarchie7"))
                ChargerPropriete(ohierarchieFixe.sHierarchie8, readerSQL.Item("hierarchie8"))
                ChargerPropriete(ohierarchieFixe.sHierarchie9, readerSQL.Item("hierarchie9"))

            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerForfaitParId(ByRef oForfait As Forfait, ByVal nIdForfait As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oOperateur As Operateur
        Dim oTypeForfait As TypeForfait
        Try
            Requete = "Select idForfait, libelle, prixAbonnement, idOperateur, idTypeForfait"
            Requete &= " FROM Forfait"
            Requete &= " WHERE idForfait =" + nIdForfait.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oOperateur = New Operateur
                oTypeForfait = New TypeForfait
                ChargerPropriete(oForfait.nIdForfait, readerSQL.Item("idForfait"))
                ChargerPropriete(oForfait.sLibelle, readerSQL.Item("libelle"))
                ChargerPropriete(oForfait.nPrixAbonnement, readerSQL.Item("prixAbonnement"))
                oOperateur.chargerOperateurParId(CInt(readerSQL.Item("idOperateur")))
                ChargerPropriete(oForfait.oOperateur, oOperateur)
                oTypeForfait.chargerTypeForfaitParId(CInt(readerSQL.Item("idTypeForfait")))
                ChargerPropriete(oForfait.oTypeForfait, oTypeForfait)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerOperateurParId(ByRef oOperateur As Operateur, ByVal nIdOperateur As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "Select idOperateur, libelle"
            Requete &= " FROM Operateur"
            Requete &= " WHERE idOperateur =" + nIdOperateur.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oOperateur.nIdOperateur, readerSQL.Item("idOperateur"))
                ChargerPropriete(oOperateur.sLibelle, readerSQL.Item("libelle"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Function TesterFiltrePresentRapportSiNumeroExiste(ByVal sNumero As String, ByVal nIdClient As Integer) As Boolean
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "Select numeroGsm, estActif, idClient FROM dbo.Ligne WHERE numeroGsm = '" & Toolbox.ReplaceQuote(sNumero) & "' AND estActif = 1 AND idClient = " & nIdClient.ToString
            readerSQL = sql.chargerDataReader(Requete)
            Return readerSQL.HasRows
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Function

    Public Shared Function TesterSiNumeroExisteFixe(ByVal oLigneFixe As LigneFixe) As Boolean
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "SELECT numeroLigne FROM  dbo.Vue_Ligne WHERE (estActif = 1) AND (numeroLigne = '" & oLigneFixe.sNumeroLigneFixe & "' AND idClient = " & oLigneFixe.nIdClient & ")"
            readerSQL = sql.chargerDataReader(Requete)
            Return readerSQL.HasRows
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Function

    Public Shared Sub chargerCategorieOptionUtilisateurParId(ByRef oCategorieOptionUtilisateur As CategorieOptionUtilisateur, ByVal nIdCategorieOptionUtilisateur As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "SELECT idCategorieOption, libelle"
            Requete &= " FROM CategorieOption"
            Requete &= " WHERE idCategorieOption =" + nIdCategorieOptionUtilisateur.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oCategorieOptionUtilisateur.nIdCategorie, readerSQL.Item("idCategorieOption"))
                ChargerPropriete(oCategorieOptionUtilisateur.sLibelle, readerSQL.Item("libelle"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerTypeForfaitParId(ByRef oTypeForfait As TypeForfait, ByVal nIdTypeForfait As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "SELECT idTypeForfait, libelle"
            Requete &= " FROM TypeForfait"
            Requete &= " WHERE idTypeForfait =" + nIdTypeForfait.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oTypeForfait.nIdTypeForfait, readerSQL.Item("idTypeForfait"))
                ChargerPropriete(oTypeForfait.sLibelle, readerSQL.Item("libelle"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerEmployesGestionParc(ByVal oLignes As Lignes, ByVal nIdClient As Integer, ByVal sTypeAffectation As String)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "SELECT Employes.matricule, Employes.prenom + ' ' + Employes.nom AS Nom"
            Requete &= " FROM Employes"
            Requete &= " WHERE idClient =" + nIdClient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim oLigne As New Ligne
                ChargerPropriete(oLigne.nIdLigne, readerSQL.Item("Matricule"))
                ChargerPropriete(oLigne.sNom, readerSQL.Item("Nom"))

                oLignes.Add(oLigne)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerLignesGestionParc(ByVal oLignes As Lignes, ByVal nIdClient As Integer, ByVal sTypeAffectation As String)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "SELECT Ligne.idLigne, Ligne.prenom, Ligne.nom"
            Requete &= " FROM Ligne, LiaisonStock"
            Requete &= " WHERE Liaisonstock.idligne = Ligne.idligne "
            Requete &= " AND idClient =" + nIdClient.ToString
            Requete &= " AND TypeLiaison= '" + sTypeAffectation + "'"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim oLigne As New Ligne
                ChargerPropriete(oLigne.nIdLigne, readerSQL.Item("idLigne"))
                ChargerPropriete(oLigne.sNom, readerSQL.Item("Nom"))
                ChargerPropriete(oLigne.sPrenom, readerSQL.Item("Prenom"))

                oLignes.Add(oLigne)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerStructuresGestionParc(ByVal olignes As Lignes, ByVal nIdclient As Integer, ByVal sTypeAffectation As String)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "SELECT DISTINCT hierarchie.idHierarchie, hierarchie.hierarchie2"
            Requete &= " FROM hierarchie"
            Requete &= " WHERE idClient =" + nIdclient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim oLigne As New Ligne
                ChargerPropriete(oLigne.nIdLigne, readerSQL.Item("idHierarchie"))
                ChargerPropriete(oLigne.sNom, readerSQL.Item("hierarchie2"))

                olignes.Add(oLigne)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerDiscriminationParId(ByRef oDiscrimination As Discrimination, ByVal nIdDiscrimination As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "SELECT idDiscrimination, libelle"
            Requete &= " FROM Discrimination"
            Requete &= " WHERE idDiscrimination =" + nIdDiscrimination.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oDiscrimination.nIdDiscrimination, readerSQL.Item("idDiscrimination"))
                ChargerPropriete(oDiscrimination.sLibelle, readerSQL.Item("libelle"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerCompteFacturantParId(ByRef oCompteFacturant As CompteFacturant, ByVal nIdCompteFacturant As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "SELECT idCompteFacturant, libelle"
            Requete &= " FROM CompteFacturant"
            Requete &= " WHERE idCompteFacturant =" + nIdCompteFacturant.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oCompteFacturant.nIdCompteFacturant, readerSQL.Item("idCompteFacturant"))
                ChargerPropriete(oCompteFacturant.sLibelle, readerSQL.Item("libelle"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerAffectation(ByVal oAffectations As Affectations, ByVal nIdClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "SELECT DISTINCT TypeLiaison "
            Requete &= " FROM LiaisonStock, Ligne"
            Requete &= " WHERE Liaisonstock.idligne = Ligne.idligne and Ligne.idClient =" + nIdClient.ToString
            Requete &= " ORDER BY TypeLiaison DESC"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim oAffectation As New Affectation
                ChargerPropriete(oAffectation.sAffectation, readerSQL.Item("TypeLiaison"))

                oAffectations.Add(oAffectation)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub RecupererAffectation(ByVal oLigne As Ligne)
        Dim sql As New cSQL("GestionParc")
        Dim requete As String = "SELECT TypeLiaison FROM  LiaisonStock"
        requete += "WHERE iDLigne = " + oLigne.nIdLigne.ToString
        Dim readerSQL As SqlDataReader = Nothing

        Try
            readerSQL = sql.chargerDataReader(requete)
            Do Until Not readerSQL.Read()
                oLigne.sAffectation = readerSQL.Item("TypeLiaison").ToString
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If

            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Sub
    Public Shared Sub RecupererAffectationParIdLigne(ByVal oLigne As Ligne, ByVal Affectation As String)
        Dim sql As New cSQL("GestionParc")
        Dim requete As String = "SELECT TypeLiaison FROM  LiaisonStock"
        requete += "WHERE iDLigne = " + oLigne.nIdLigne.ToString
        Dim readerSQL As SqlDataReader = Nothing

        Try
            readerSQL = sql.chargerDataReader(requete)
            Do Until Not readerSQL.Read()
                Affectation = readerSQL.Item("TypeLiaison").ToString
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If

            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Sub
    Public Shared Sub chargerProfil(ByVal oProfils As Profils, ByVal nIdClient As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "Select DISTINCT NomProfil "
            Requete &= " FROM Profils"
            Requete &= " WHERE Profils.idClient =" + nIdClient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                Dim oProfil As New Profil
                ChargerPropriete(oProfil.sNomProfil, readerSQL.Item("NomProfil"))

                oProfils.Add(oProfil)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerCompteFacturantFixeParId(ByRef oCompteFacturantFixe As CompteFacturantFixe, ByVal nIdCompteFacturantFixe As Integer)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "Select idCompteFacturant, libelleCompteFacturant, libelleCentreFrais"
            Requete &= " FROM CompteFacturant"
            Requete &= " WHERE idCompteFacturant =" + nIdCompteFacturantFixe.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oCompteFacturantFixe.nIdCompteFacturant, readerSQL.Item("idCompteFacturant"))
                ChargerPropriete(oCompteFacturantFixe.sLibelleCompteFacturant, readerSQL.Item("libelleCompteFacturant"))
                ChargerPropriete(oCompteFacturantFixe.sLibelleCentreFrais, readerSQL.Item("libelleCentreFrais"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerUsageFixeParId(ByRef oUsageFixe As UsageFixe, ByVal nIdUsageFixe As Integer, ByVal idClient As Integer)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "Select idUsage, libelleUsage, typeUsage"
            Requete &= " FROM Usage"
            Requete &= " WHERE idUsage =" + nIdUsageFixe.ToString & " And idClient=" & idClient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oUsageFixe.nIdUsage, readerSQL.Item("idUsage"))
                ChargerPropriete(oUsageFixe.sLibelleUsage, readerSQL.Item("libelleUsage"))
                ChargerPropriete(oUsageFixe.sTypeUsage, readerSQL.Item("typeUsage"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub chargerUsageFixeInternet(ByRef oUsageFixe As UsageFixe, ByVal idClient As Integer)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "Select idUsage, libelleUsage, typeUsage"
            Requete &= " FROM Usage"
            Requete &= " WHERE libelleUsage ='Adsl' and idclient=" + idClient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oUsageFixe.nIdUsage, readerSQL.Item("idUsage"))
                ChargerPropriete(oUsageFixe.sLibelleUsage, readerSQL.Item("libelleUsage"))
                ChargerPropriete(oUsageFixe.sTypeUsage, readerSQL.Item("typeUsage"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub



    Public Shared Sub chargerTypeLigneFixeParId(ByRef oTypeLigneFixe As TypeLigneFixe, ByVal nIdTypeLigneFixe As Integer)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "SELECT idTypeLigne, libelleTypeLigne"
            Requete &= " FROM TypeLigne"
            Requete &= " WHERE idTypeLigne =" + nIdTypeLigneFixe.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oTypeLigneFixe.nIdTypeLigne, readerSQL.Item("idTypeLigne"))
                ChargerPropriete(oTypeLigneFixe.sLibelleTypeLigne, readerSQL.Item("libelleTypeLigne"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerTypeLigneFixeInternet(ByRef oTypeLigneFixe As TypeLigneFixe, ByVal nIdClient As Integer)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "SELECT idTypeLigne, libelleTypeLigne"
            Requete &= " FROM TypeLigne"
            Requete &= " WHERE libelleTypeLigne ='Internet' AND idClient=" & nIdClient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oTypeLigneFixe.nIdTypeLigne, readerSQL.Item("idTypeLigne"))
                ChargerPropriete(oTypeLigneFixe.sLibelleTypeLigne, readerSQL.Item("libelleTypeLigne"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerFonctionParId(ByRef oFonction As Fonction, ByVal nIdFonction As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "Select idFonction, libelle"
            Requete &= " FROM Fonction"
            Requete &= " WHERE idFonction =" + nIdFonction.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oFonction.nIdFonction, readerSQL.Item("idFonction"))
                ChargerPropriete(oFonction.sLibelle, readerSQL.Item("libelle"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerTypeMaterielParId(ByRef oTypeMateriel As TypeMateriel, ByVal nIdTypeMateriel As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "Select idTypeMateriel, libelle"
            Requete &= " FROM TypeMateriel"
            Requete &= " WHERE idTypeMateriel =" + nIdTypeMateriel.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oTypeMateriel.nIdTypeMateriel, readerSQL.Item("idTypeMateriel"))
                ChargerPropriete(oTypeMateriel.sLibelle, readerSQL.Item("libelle"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub
    Public Shared Sub chargerMarqueParId(ByRef oMarque As Marque, ByVal nIdMarque As Integer)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Try
            Requete = "Select idMarque, libelle"
            Requete &= " FROM Marque"
            Requete &= " WHERE idMarque =" + nIdMarque.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                ChargerPropriete(oMarque.nIdMarque, readerSQL.Item("idMarque"))
                ChargerPropriete(oMarque.sLibelle, readerSQL.Item("libelle"))
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub InsererLigne(ByRef oLigne As Ligne)
        Dim transaction As SqlTransaction
        Dim requete As String = "INSERT INTO Ligne"
        requete += " (numeroGsm, numeroData, nom, prenom, estActif, dateActivation, idFonction, idCompteFacturant, idDiscrimination, idForfait, idHierarchie, idClient)"
        requete += " OUTPUT INSERTED.idLigne"
        requete += " VALUES (@numeroGsm, @numeroData, @nom, @prenom, @estActif, @dateActivation, @idFonction, @idCompteFacturant, @idDiscrimination, @idForfait, @idHierarchie, @idClient)"

        Dim sql As New cSQL("GestionParc"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@numeroGsm", SqlDbType.VarChar, 50).Value = oLigne.sNumeroGSM
            cmd.Parameters.Add("@numeroData", SqlDbType.VarChar, 50).Value = oLigne.sNumeroData
            cmd.Parameters.Add("@nom", SqlDbType.VarChar, 50).Value = oLigne.sNom
            cmd.Parameters.Add("@prenom", SqlDbType.VarChar, 50).Value = oLigne.sPrenom
            cmd.Parameters.Add("@estActif", SqlDbType.Bit).Value = oLigne.bEstActif
            cmd.Parameters.Add("@dateActivation", SqlDbType.DateTime).Value = oLigne.dDateActivation
            cmd.Parameters.Add("@idFonction", SqlDbType.Int).Value = oLigne.oFonction.nIdFonction
            cmd.Parameters.Add("@idCompteFacturant", SqlDbType.Int).Value = oLigne.oCompteFacturant.nIdCompteFacturant
            cmd.Parameters.Add("@idDiscrimination", SqlDbType.Int).Value = oLigne.oDiscrimination.nIdDiscrimination
            cmd.Parameters.Add("@idForfait", SqlDbType.Int).Value = oLigne.oForfait.nIdForfait
            cmd.Parameters.Add("@idHierarchie", SqlDbType.Int).Value = oLigne.oHierarchie.nIdHierarchie
            cmd.Parameters.Add("@idClient", SqlDbType.Int).Value = oLigne.nIdClient

            oLigne.nIdLigne = CInt(cmd.ExecuteScalar())
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub insererLigneFixe(ByRef oLigneFixe As LigneFixe)
        Dim oTransaction As SqlTransaction

        Dim sRequete As String = "INSERT INTO LigneTechnique"
        sRequete += " ("
        sRequete += Toolbox.AjouterChampRequete("numeroLigne", "texte", oLigneFixe.sNumeroLigneFixe)
        sRequete += Toolbox.AjouterChampRequete("idHierarchie", "numerique", oLigneFixe.oHierarchieFixe.nIdHierarchie)
        sRequete += Toolbox.AjouterChampRequete("idCompteFacturant", "numerique", oLigneFixe.oCompteFacturantFixe.nIdCompteFacturant)
        sRequete += Toolbox.AjouterChampRequete("idUsage", "numerique", oLigneFixe.oUsageFixe.nIdUsage)
        sRequete += Toolbox.AjouterChampRequete("idTypeLigne", "numerique", oLigneFixe.oTypeLigneFixe.nIdTypeLigne)
        sRequete += Toolbox.AjouterChampRequete("dateActivation", "Date", oLigneFixe.dDateActivation)
        sRequete += Toolbox.AjouterChampRequete("idClient", "numerique", oLigneFixe.nIdClient)
        sRequete += Toolbox.AjouterChampRequete("idContrat", "numerique", oLigneFixe.oContratFixe.nIdContrat)
        sRequete += Toolbox.AjouterChampRequete("nombreAcces", "numerique", oLigneFixe.nNombreAcces)
        sRequete += Toolbox.AjouterChampRequete("nombreSDA", "numerique", oLigneFixe.nNombreSDA)
        sRequete += Toolbox.AjouterChampRequete("specialisationCanauxArrivee", "numerique", oLigneFixe.nSpecialisationCanauxArrive)
        sRequete += Toolbox.AjouterChampRequete("specialisationCanauxMixte", "numerique", oLigneFixe.nSpecialisationCanauxMixte)
        sRequete += Toolbox.AjouterChampRequete("specialisationCanauxDepart", "numerique", oLigneFixe.nSpecialisationCanauxDepart)
        sRequete += Toolbox.AjouterChampRequete("choixModem", "texte", oLigneFixe.sChoixModem)
        sRequete += Toolbox.AjouterChampRequete("typeAdresseEmail", "texte", oLigneFixe.sTypeAdresseEmail)
        sRequete += Toolbox.AjouterChampRequete("adresseEmail", "texte", oLigneFixe.sAdresseEmail)
        sRequete += Toolbox.AjouterChampRequete("idTeteLigne", "numerique", oLigneFixe.nIdTeteLigne)
        sRequete += Toolbox.AjouterChampRequete("stMessagerieVocale", "booleen", oLigneFixe.bStMessagerieVocale)
        sRequete += Toolbox.AjouterChampRequete("stNumeroRenvoi", "texte", oLigneFixe.sStNumeroRenvoi)
        sRequete += Toolbox.AjouterChampRequete("stSecretPermanent", "booleen", oLigneFixe.bStSecretPermanent)
        sRequete += Toolbox.AjouterChampRequete("stTransfertAppel", "booleen", oLigneFixe.bStTransfertAppel)
        sRequete += Toolbox.AjouterChampRequete("stRenvoiTerminal", "booleen", oLigneFixe.bStRenvoiTerminal)
        sRequete += Toolbox.AjouterChampRequete("idTypeSelection", "numerique", oLigneFixe.oTypeSelectionFixe.nIdTypeSelection)
        sRequete += Toolbox.AjouterChampRequete("idChoixListe", "numerique", oLigneFixe.oChoixListeFixe.nIdChoixListe)
        sRequete += Toolbox.AjouterChampRequete("iaPrecisionsComplementairesParution", "texte", oLigneFixe.sIaPrecisionsComplementairesParution)
        sRequete += Toolbox.AjouterChampRequete("nomEtablissement", "texte", oLigneFixe.sNomEtablissement)
        sRequete += Toolbox.AjouterChampRequete("adresse", "texte", oLigneFixe.sAdresse)
        sRequete += Toolbox.AjouterChampRequete("codePostal", "texte", oLigneFixe.sCodePostal)
        sRequete += Toolbox.AjouterChampRequete("localite", "texte", oLigneFixe.sLocalite)
        sRequete += Toolbox.AjouterChampRequete("complementAdresse", "texte", oLigneFixe.sComplementAdresse)
        sRequete += Toolbox.AjouterChampRequete("nomContact", "texte", oLigneFixe.sNomContact)
        sRequete += Toolbox.AjouterChampRequete("fixeContact", "texte", oLigneFixe.sFixeContact)
        sRequete += Toolbox.AjouterChampRequete("mobileContact", "texte", oLigneFixe.sMobileContact)
        sRequete += Toolbox.AjouterChampRequete("mailContact", "texte", oLigneFixe.sMailContact)
        If sRequete <> "" Then sRequete = sRequete.Remove(sRequete.Length - 2, 2)
        sRequete += ") "
        sRequete += " OUTPUT INSERTED.idLigne"
        sRequete += " VALUES ("
        sRequete += Toolbox.AjouterChampRequete("@numeroLigne", "texte", oLigneFixe.sNumeroLigneFixe)
        sRequete += Toolbox.AjouterChampRequete("@idHierarchie", "numerique", oLigneFixe.oHierarchieFixe.nIdHierarchie)
        sRequete += Toolbox.AjouterChampRequete("@idCompteFacturant", "numerique", oLigneFixe.oCompteFacturantFixe.nIdCompteFacturant)
        sRequete += Toolbox.AjouterChampRequete("@idUsage", "numerique", oLigneFixe.oUsageFixe.nIdUsage)
        sRequete += Toolbox.AjouterChampRequete("@idTypeLigne", "numerique", oLigneFixe.oTypeLigneFixe.nIdTypeLigne)
        sRequete += Toolbox.AjouterChampRequete("@dateActivation", "Date", oLigneFixe.dDateActivation)
        sRequete += Toolbox.AjouterChampRequete("@idClient", "numerique", oLigneFixe.nIdClient)
        sRequete += Toolbox.AjouterChampRequete("@idContrat", "numerique", oLigneFixe.oContratFixe.nIdContrat)
        sRequete += Toolbox.AjouterChampRequete("@nombreAcces", "numerique", oLigneFixe.nNombreAcces)
        sRequete += Toolbox.AjouterChampRequete("@nombreSDA", "numerique", oLigneFixe.nNombreSDA)
        sRequete += Toolbox.AjouterChampRequete("@specialisationCanauxArrivee", "numerique", oLigneFixe.nSpecialisationCanauxArrive)
        sRequete += Toolbox.AjouterChampRequete("@specialisationCanauxMixte", "numerique", oLigneFixe.nSpecialisationCanauxMixte)
        sRequete += Toolbox.AjouterChampRequete("@specialisationCanauxDepart", "numerique", oLigneFixe.nSpecialisationCanauxDepart)
        sRequete += Toolbox.AjouterChampRequete("@choixModem", "texte", oLigneFixe.sChoixModem)
        sRequete += Toolbox.AjouterChampRequete("@typeAdresseEmail", "texte", oLigneFixe.sTypeAdresseEmail)
        sRequete += Toolbox.AjouterChampRequete("@adresseEmail", "texte", oLigneFixe.sAdresseEmail)
        sRequete += Toolbox.AjouterChampRequete("@idTeteLigne", "numerique", oLigneFixe.nIdTeteLigne)
        sRequete += Toolbox.AjouterChampRequete("@stMessagerieVocale", "booleen", oLigneFixe.bStMessagerieVocale)
        sRequete += Toolbox.AjouterChampRequete("@stNumeroRenvoi", "texte", oLigneFixe.sStNumeroRenvoi)
        sRequete += Toolbox.AjouterChampRequete("@stSecretPermanent", "booleen", oLigneFixe.bStSecretPermanent)
        sRequete += Toolbox.AjouterChampRequete("@stTransfertAppel", "booleen", oLigneFixe.bStTransfertAppel)
        sRequete += Toolbox.AjouterChampRequete("@stRenvoiTerminal", "booleen", oLigneFixe.bStRenvoiTerminal)
        sRequete += Toolbox.AjouterChampRequete("@idTypeSelection", "numerique", oLigneFixe.oTypeSelectionFixe.nIdTypeSelection)
        sRequete += Toolbox.AjouterChampRequete("@idChoixListe", "numerique", oLigneFixe.oChoixListeFixe.nIdChoixListe)
        sRequete += Toolbox.AjouterChampRequete("@iaPrecisionsComplementairesParution", "texte", oLigneFixe.sIaPrecisionsComplementairesParution)
        sRequete += Toolbox.AjouterChampRequete("@nomEtablissement", "texte", oLigneFixe.sNomEtablissement)
        sRequete += Toolbox.AjouterChampRequete("@adresse", "texte", oLigneFixe.sAdresse)
        sRequete += Toolbox.AjouterChampRequete("@codePostal", "texte", oLigneFixe.sCodePostal)
        sRequete += Toolbox.AjouterChampRequete("@localite", "texte", oLigneFixe.sLocalite)
        sRequete += Toolbox.AjouterChampRequete("@complementAdresse", "texte", oLigneFixe.sComplementAdresse)
        sRequete += Toolbox.AjouterChampRequete("@nomContact", "texte", oLigneFixe.sNomContact)
        sRequete += Toolbox.AjouterChampRequete("@fixeContact", "texte", oLigneFixe.sFixeContact)
        sRequete += Toolbox.AjouterChampRequete("@mobileContact", "texte", oLigneFixe.sMobileContact)
        sRequete += Toolbox.AjouterChampRequete("@mailContact", "texte", oLigneFixe.sMailContact)
        If sRequete <> "" Then sRequete = sRequete.Remove(sRequete.Length - 2, 2)
        sRequete += ") "

        Dim sql As New cSQL("GestionParcFixe"),
       cmd As New SqlCommand(sRequete, sql.connexion)
        oTransaction = sql.connexion.BeginTransaction()
        cmd.Transaction = oTransaction

        Try

            Toolbox.AjouterParametreRequete(cmd, "@numeroLigne", "VarChar", oLigneFixe.sNumeroLigneFixe, 50)
            Toolbox.AjouterParametreRequete(cmd, "@idHierarchie", "Int", oLigneFixe.oHierarchieFixe.nIdHierarchie)
            Toolbox.AjouterParametreRequete(cmd, "@idCompteFacturant", "Int", oLigneFixe.oCompteFacturantFixe.nIdCompteFacturant)
            Toolbox.AjouterParametreRequete(cmd, "@idUsage", "Int", oLigneFixe.oUsageFixe.nIdUsage)
            Toolbox.AjouterParametreRequete(cmd, "@idTypeLigne", "Int", oLigneFixe.oTypeLigneFixe.nIdTypeLigne)
            Toolbox.AjouterParametreRequete(cmd, "@dateActivation", "DateTime", oLigneFixe.dDateActivation)
            Toolbox.AjouterParametreRequete(cmd, "@idClient", "Int", oLigneFixe.nIdClient)
            Toolbox.AjouterParametreRequete(cmd, "@idContrat", "Int", oLigneFixe.oContratFixe.nIdContrat)
            Toolbox.AjouterParametreRequete(cmd, "@nombreAcces", "Int", oLigneFixe.nNombreAcces)
            Toolbox.AjouterParametreRequete(cmd, "@nombreSDA", "Int", oLigneFixe.nNombreSDA)
            Toolbox.AjouterParametreRequete(cmd, "@specialisationCanauxArrivee", "Int", oLigneFixe.nSpecialisationCanauxArrive)
            Toolbox.AjouterParametreRequete(cmd, "@specialisationCanauxMixte", "Int", oLigneFixe.nSpecialisationCanauxMixte)
            Toolbox.AjouterParametreRequete(cmd, "@specialisationCanauxDepart", "Int", oLigneFixe.nSpecialisationCanauxDepart)
            Toolbox.AjouterParametreRequete(cmd, "@choixModem", "VarChar", oLigneFixe.sChoixModem, 100)
            Toolbox.AjouterParametreRequete(cmd, "@typeAdresseEmail", "VarChar", oLigneFixe.sTypeAdresseEmail, 100)
            Toolbox.AjouterParametreRequete(cmd, "@adresseEmail", "VarChar", oLigneFixe.sAdresseEmail, 100)
            Toolbox.AjouterParametreRequete(cmd, "@idTeteLigne", "Int", oLigneFixe.nIdTeteLigne)
            Toolbox.AjouterParametreRequete(cmd, "@stMessagerieVocale", "Bit", oLigneFixe.bStMessagerieVocale)
            Toolbox.AjouterParametreRequete(cmd, "@stNumeroRenvoi", "VarChar", oLigneFixe.sStNumeroRenvoi, 100)
            Toolbox.AjouterParametreRequete(cmd, "@stSecretPermanent", "Bit", oLigneFixe.bStSecretPermanent)
            Toolbox.AjouterParametreRequete(cmd, "@stTransfertAppel", "Bit", oLigneFixe.bStTransfertAppel)
            Toolbox.AjouterParametreRequete(cmd, "@stRenvoiTerminal", "Bit", oLigneFixe.bStRenvoiTerminal)
            Toolbox.AjouterParametreRequete(cmd, "@idTypeSelection", "Int", oLigneFixe.oTypeSelectionFixe.nIdTypeSelection)
            Toolbox.AjouterParametreRequete(cmd, "@idChoixListe", "Int", oLigneFixe.oChoixListeFixe.nIdChoixListe)
            Toolbox.AjouterParametreRequete(cmd, "@iaPrecisionsComplementairesParution", "Varchar", oLigneFixe.sIaPrecisionsComplementairesParution, 1000)
            Toolbox.AjouterParametreRequete(cmd, "@nomEtablissement", "VarChar", oLigneFixe.sNomEtablissement, 250)
            Toolbox.AjouterParametreRequete(cmd, "@adresse", "VarChar", oLigneFixe.sAdresse, 250)
            Toolbox.AjouterParametreRequete(cmd, "@codePostal", "VarChar", oLigneFixe.sCodePostal, 25)
            Toolbox.AjouterParametreRequete(cmd, "@localite", "VarChar", oLigneFixe.sLocalite, 250)
            Toolbox.AjouterParametreRequete(cmd, "@complementAdresse", "VarChar", oLigneFixe.sLocalite, 1000)
            Toolbox.AjouterParametreRequete(cmd, "@nomContact", "VarChar", oLigneFixe.sNomContact, 250)
            Toolbox.AjouterParametreRequete(cmd, "@fixeContact", "VarChar", oLigneFixe.sFixeContact, 15)
            Toolbox.AjouterParametreRequete(cmd, "@mobileContact", "VarChar", oLigneFixe.sMobileContact, 15)
            Toolbox.AjouterParametreRequete(cmd, "@mailContact", "VarChar", oLigneFixe.sMailContact, 250)

            oLigneFixe.nIdLigneFixe = CInt(cmd.ExecuteScalar())
            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub editerCommandeFixe(ByVal oCommande As CommandeFixe)
        Dim oTransaction As SqlTransaction


        Dim sRequete As String = "Update Commande set "

        sRequete += Toolbox.AjouterChampRequete("numeroCommande=@numeroCommande", "texte", oCommande.sNumeroCommande)
        sRequete += Toolbox.AjouterChampRequete("typeCommande=@typeCommande", "texte", oCommande.sTypeCommande)
        sRequete += Toolbox.AjouterChampRequete("estRealiseFacture=@estRealiseFacture", "texte", oCommande.bEstRealiseFacture)
        sRequete += Toolbox.AjouterChampRequete("estTelecharge=@estTelecharge", "booleen", oCommande.bEstTelecharge)
        sRequete += Toolbox.AjouterChampRequete("accesReseau_contratLigne=@accesReseau_contratLigne", "texte", oCommande.bEstTelecharge)

        If sRequete <> "" Then sRequete = sRequete.Remove(sRequete.Length - 2, 2)

        sRequete += " Where idLigne=@idLigne"


        Dim sql As New cSQL("GestionParcFixe"),
       cmd As New SqlCommand(sRequete, sql.connexion)
        oTransaction = sql.connexion.BeginTransaction()
        cmd.Transaction = oTransaction

        Try
            cmd.Parameters.Add("@idLigne", SqlDbType.Int).Value = oCommande.oLigneFixe.nIdLigneFixe
            Toolbox.AjouterParametreRequete(cmd, "@idHierarchie", "Int", oCommande.oLigneFixe.oHierarchieFixe.nIdHierarchie)
            Toolbox.AjouterParametreRequete(cmd, "@idCompteFacturant", "Int", oCommande.oLigneFixe.oCompteFacturantFixe.nIdCompteFacturant)
            Toolbox.AjouterParametreRequete(cmd, "@idUsage", "Int", oCommande.oLigneFixe.oUsageFixe.nIdUsage)
            Toolbox.AjouterParametreRequete(cmd, "@idTypeLigne", "Int", oCommande.oLigneFixe.oTypeLigneFixe.nIdTypeLigne)
            Toolbox.AjouterParametreRequete(cmd, "@idContrat", "Int", oCommande.oLigneFixe.oContratFixe.nIdContrat)
            Toolbox.AjouterParametreRequete(cmd, "@nombreAcces", "Int", oCommande.oLigneFixe.nNombreAcces)
            Toolbox.AjouterParametreRequete(cmd, "@nombreSDA", "Int", oCommande.oLigneFixe.nNombreSDA)
            Toolbox.AjouterParametreRequete(cmd, "@specialisationCanauxArrivee", "Int", oCommande.oLigneFixe.nSpecialisationCanauxArrive)
            Toolbox.AjouterParametreRequete(cmd, "@specialisationCanauxMixte", "Int", oCommande.oLigneFixe.nSpecialisationCanauxMixte)
            Toolbox.AjouterParametreRequete(cmd, "@specialisationCanauxDepart", "Int", oCommande.oLigneFixe.nSpecialisationCanauxDepart)
            Toolbox.AjouterParametreRequete(cmd, "@choixModem", "VarChar", oCommande.oLigneFixe.sChoixModem, 100)
            Toolbox.AjouterParametreRequete(cmd, "@typeAdresseEmail", "VarChar", oCommande.oLigneFixe.sTypeAdresseEmail, 100)
            Toolbox.AjouterParametreRequete(cmd, "@adresseEmail", "VarChar", oCommande.oLigneFixe.sAdresseEmail, 100)
            Toolbox.AjouterParametreRequete(cmd, "@idTeteLigne", "Int", oCommande.oLigneFixe.nIdTeteLigne)
            Toolbox.AjouterParametreRequete(cmd, "@stMessagerieVocale", "Bit", oCommande.oLigneFixe.bStMessagerieVocale)
            Toolbox.AjouterParametreRequete(cmd, "@stNumeroRenvoi", "VarChar", oCommande.oLigneFixe.sStNumeroRenvoi, 100)
            Toolbox.AjouterParametreRequete(cmd, "@stSecretPermanent", "Bit", oCommande.oLigneFixe.bStSecretPermanent)
            Toolbox.AjouterParametreRequete(cmd, "@stTransfertAppel", "Bit", oCommande.oLigneFixe.bStTransfertAppel)
            Toolbox.AjouterParametreRequete(cmd, "@stTransfertAppelNonReponse", "Bit", oCommande.oLigneFixe.bStTransfertAppelNonReponse)
            Toolbox.AjouterParametreRequete(cmd, "@stRenvoiTerminal", "Bit", oCommande.oLigneFixe.bStRenvoiTerminal)
            Toolbox.AjouterParametreRequete(cmd, "@idTypeSelection", "Int", oCommande.oLigneFixe.oTypeSelectionFixe.nIdTypeSelection)
            Toolbox.AjouterParametreRequete(cmd, "@idChoixListe", "Int", oCommande.oLigneFixe.oChoixListeFixe.nIdChoixListe)
            Toolbox.AjouterParametreRequete(cmd, "@iaPrecisionsComplementairesParution", "Varchar", oCommande.oLigneFixe.sIaPrecisionsComplementairesParution, 1000)
            Toolbox.AjouterParametreRequete(cmd, "@nomEtablissement", "VarChar", oCommande.oLigneFixe.sNomEtablissement, 250)
            Toolbox.AjouterParametreRequete(cmd, "@adresse", "VarChar", oCommande.oLigneFixe.sAdresse, 250)
            Toolbox.AjouterParametreRequete(cmd, "@codePostal", "VarChar", oCommande.oLigneFixe.sCodePostal, 25)
            Toolbox.AjouterParametreRequete(cmd, "@localite", "VarChar", oCommande.oLigneFixe.sLocalite, 250)
            Toolbox.AjouterParametreRequete(cmd, "@complementAdresse", "VarChar", oCommande.oLigneFixe.sLocalite, 1000)
            Toolbox.AjouterParametreRequete(cmd, "@nomContact", "VarChar", oCommande.oLigneFixe.sNomContact, 250)
            Toolbox.AjouterParametreRequete(cmd, "@fixeContact", "VarChar", oCommande.oLigneFixe.sFixeContact, 15)
            Toolbox.AjouterParametreRequete(cmd, "@mobileContact", "VarChar", oCommande.oLigneFixe.sMobileContact, 15)
            Toolbox.AjouterParametreRequete(cmd, "@mailContact", "VarChar", oCommande.oLigneFixe.sMailContact, 250)

            cmd.ExecuteNonQuery()
            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub editerLigneFixe(ByVal oLigneFixe As LigneFixe)
        Dim oTransaction As SqlTransaction


        Dim sRequete As String = "Update LigneTechnique Set "


        sRequete += Toolbox.AjouterChampRequete("idHierarchie=@idHierarchie", "numerique", oLigneFixe.oHierarchieFixe.nIdHierarchie)
        sRequete += Toolbox.AjouterChampRequete("idCompteFacturant=@idCompteFacturant", "numerique", oLigneFixe.oCompteFacturantFixe.nIdCompteFacturant)
        sRequete += Toolbox.AjouterChampRequete("idUsage=@idUsage", "numerique", oLigneFixe.oUsageFixe.nIdUsage)
        sRequete += Toolbox.AjouterChampRequete("idTypeLigne=@idTypeLigne", "numerique", oLigneFixe.oTypeLigneFixe.nIdTypeLigne)
        sRequete += Toolbox.AjouterChampRequete("idContrat=@idContrat", "numerique", oLigneFixe.oContratFixe.nIdContrat)
        sRequete += Toolbox.AjouterChampRequete("nombreAcces=@nombreAcces", "numerique", oLigneFixe.nNombreAcces)
        sRequete += Toolbox.AjouterChampRequete("nombreSDA=@nombreSDA", "numerique", oLigneFixe.nNombreSDA)
        sRequete += Toolbox.AjouterChampRequete("specialisationCanauxArrivee=@specialisationCanauxArrivee", "numerique", oLigneFixe.nSpecialisationCanauxArrive)
        sRequete += Toolbox.AjouterChampRequete("specialisationCanauxMixte=@specialisationCanauxMixte", "numerique", oLigneFixe.nSpecialisationCanauxMixte)
        sRequete += Toolbox.AjouterChampRequete("specialisationCanauxDepart=@specialisationCanauxDepart", "numerique", oLigneFixe.nSpecialisationCanauxDepart)
        sRequete += Toolbox.AjouterChampRequete("choixModem=@choixModem", "texte", oLigneFixe.sChoixModem)
        sRequete += Toolbox.AjouterChampRequete("typeAdresseEmail=@typeAdresseEmail", "texte", oLigneFixe.sTypeAdresseEmail)
        sRequete += Toolbox.AjouterChampRequete("adresseEmail=@adresseEmail", "texte", oLigneFixe.sAdresseEmail)
        sRequete += Toolbox.AjouterChampRequete("idTeteLigne=@idTeteLigne", "numerique", oLigneFixe.nIdTeteLigne)
        sRequete += Toolbox.AjouterChampRequete("stMessagerieVocale=@stMessagerieVocale", "booleen", oLigneFixe.bStMessagerieVocale)
        sRequete += Toolbox.AjouterChampRequete("stNumeroRenvoi=@stNumeroRenvoi", "texte", oLigneFixe.sStNumeroRenvoi)
        sRequete += Toolbox.AjouterChampRequete("stSecretPermanent=@stSecretPermanent", "booleen", oLigneFixe.bStSecretPermanent)
        sRequete += Toolbox.AjouterChampRequete("stTransfertAppel=@stTransfertAppel", "booleen", oLigneFixe.bStTransfertAppel)
        sRequete += Toolbox.AjouterChampRequete("stTransfertAppelNonReponse=@stTransfertAppelNonReponse", "booleen", oLigneFixe.bStTransfertAppelNonReponse)
        sRequete += Toolbox.AjouterChampRequete("stRenvoiTerminal=@stRenvoiTerminal", "booleen", oLigneFixe.bStRenvoiTerminal)
        sRequete += Toolbox.AjouterChampRequete("idTypeSelection=@idTypeSelection", "numerique", oLigneFixe.oTypeSelectionFixe.nIdTypeSelection)
        sRequete += Toolbox.AjouterChampRequete("idChoixListe=@idChoixListe", "numerique", oLigneFixe.oChoixListeFixe.nIdChoixListe)
        sRequete += Toolbox.AjouterChampRequete("iaPrecisionsComplementairesParution=@iaPrecisionsComplementairesParution", "texte", oLigneFixe.sIaPrecisionsComplementairesParution)
        sRequete += Toolbox.AjouterChampRequete("nomEtablissement=@nomEtablissement", "texte", oLigneFixe.sNomEtablissement)
        sRequete += Toolbox.AjouterChampRequete("adresse=@adresse", "texte", oLigneFixe.sAdresse)
        sRequete += Toolbox.AjouterChampRequete("codePostal=@codePostal", "texte", oLigneFixe.sCodePostal)
        sRequete += Toolbox.AjouterChampRequete("localite=@localite", "texte", oLigneFixe.sLocalite)
        sRequete += Toolbox.AjouterChampRequete("complementAdresse=@complementAdresse", "texte", oLigneFixe.sComplementAdresse)
        sRequete += Toolbox.AjouterChampRequete("nomContact=@nomContact", "texte", oLigneFixe.sNomContact)
        sRequete += Toolbox.AjouterChampRequete("fixeContact=@fixeContact", "texte", oLigneFixe.sFixeContact)
        sRequete += Toolbox.AjouterChampRequete("mobileContact=@mobileContact", "texte", oLigneFixe.sMobileContact)
        sRequete += Toolbox.AjouterChampRequete("mailContact=@mailContact", "texte", oLigneFixe.sMailContact)
        If sRequete <> "" Then sRequete = sRequete.Remove(sRequete.Length - 2, 2)

        sRequete += " Where idLigne=@idLigne"


        Dim sql As New cSQL("GestionParcFixe"),
       cmd As New SqlCommand(sRequete, sql.connexion)
        oTransaction = sql.connexion.BeginTransaction()
        cmd.Transaction = oTransaction

        Try
            cmd.Parameters.Add("@idLigne", SqlDbType.Int).Value = oLigneFixe.nIdLigneFixe
            Toolbox.AjouterParametreRequete(cmd, "@idHierarchie", "Int", oLigneFixe.oHierarchieFixe.nIdHierarchie)
            Toolbox.AjouterParametreRequete(cmd, "@idCompteFacturant", "Int", oLigneFixe.oCompteFacturantFixe.nIdCompteFacturant)
            Toolbox.AjouterParametreRequete(cmd, "@idUsage", "Int", oLigneFixe.oUsageFixe.nIdUsage)
            Toolbox.AjouterParametreRequete(cmd, "@idTypeLigne", "Int", oLigneFixe.oTypeLigneFixe.nIdTypeLigne)
            Toolbox.AjouterParametreRequete(cmd, "@idContrat", "Int", oLigneFixe.oContratFixe.nIdContrat)
            Toolbox.AjouterParametreRequete(cmd, "@nombreAcces", "Int", oLigneFixe.nNombreAcces)
            Toolbox.AjouterParametreRequete(cmd, "@nombreSDA", "Int", oLigneFixe.nNombreSDA)
            Toolbox.AjouterParametreRequete(cmd, "@specialisationCanauxArrivee", "Int", oLigneFixe.nSpecialisationCanauxArrive)
            Toolbox.AjouterParametreRequete(cmd, "@specialisationCanauxMixte", "Int", oLigneFixe.nSpecialisationCanauxMixte)
            Toolbox.AjouterParametreRequete(cmd, "@specialisationCanauxDepart", "Int", oLigneFixe.nSpecialisationCanauxDepart)
            Toolbox.AjouterParametreRequete(cmd, "@choixModem", "VarChar", oLigneFixe.sChoixModem, 100)
            Toolbox.AjouterParametreRequete(cmd, "@typeAdresseEmail", "VarChar", oLigneFixe.sTypeAdresseEmail, 100)
            Toolbox.AjouterParametreRequete(cmd, "@adresseEmail", "VarChar", oLigneFixe.sAdresseEmail, 100)
            Toolbox.AjouterParametreRequete(cmd, "@idTeteLigne", "Int", oLigneFixe.nIdTeteLigne)
            Toolbox.AjouterParametreRequete(cmd, "@stMessagerieVocale", "Bit", oLigneFixe.bStMessagerieVocale)
            Toolbox.AjouterParametreRequete(cmd, "@stNumeroRenvoi", "VarChar", oLigneFixe.sStNumeroRenvoi, 100)
            Toolbox.AjouterParametreRequete(cmd, "@stSecretPermanent", "Bit", oLigneFixe.bStSecretPermanent)
            Toolbox.AjouterParametreRequete(cmd, "@stTransfertAppel", "Bit", oLigneFixe.bStTransfertAppel)
            Toolbox.AjouterParametreRequete(cmd, "@stTransfertAppelNonReponse", "Bit", oLigneFixe.bStTransfertAppelNonReponse)
            Toolbox.AjouterParametreRequete(cmd, "@stRenvoiTerminal", "Bit", oLigneFixe.bStRenvoiTerminal)
            Toolbox.AjouterParametreRequete(cmd, "@idTypeSelection", "Int", oLigneFixe.oTypeSelectionFixe.nIdTypeSelection)
            Toolbox.AjouterParametreRequete(cmd, "@idChoixListe", "Int", oLigneFixe.oChoixListeFixe.nIdChoixListe)
            Toolbox.AjouterParametreRequete(cmd, "@iaPrecisionsComplementairesParution", "Varchar", oLigneFixe.sIaPrecisionsComplementairesParution, 1000)
            Toolbox.AjouterParametreRequete(cmd, "@nomEtablissement", "VarChar", oLigneFixe.sNomEtablissement, 250)
            Toolbox.AjouterParametreRequete(cmd, "@adresse", "VarChar", oLigneFixe.sAdresse, 250)
            Toolbox.AjouterParametreRequete(cmd, "@codePostal", "VarChar", oLigneFixe.sCodePostal, 25)
            Toolbox.AjouterParametreRequete(cmd, "@localite", "VarChar", oLigneFixe.sLocalite, 250)
            Toolbox.AjouterParametreRequete(cmd, "@complementAdresse", "VarChar", oLigneFixe.sLocalite, 1000)
            Toolbox.AjouterParametreRequete(cmd, "@nomContact", "VarChar", oLigneFixe.sNomContact, 250)
            Toolbox.AjouterParametreRequete(cmd, "@fixeContact", "VarChar", oLigneFixe.sFixeContact, 15)
            Toolbox.AjouterParametreRequete(cmd, "@mobileContact", "VarChar", oLigneFixe.sMobileContact, 15)
            Toolbox.AjouterParametreRequete(cmd, "@mailContact", "VarChar", oLigneFixe.sMailContact, 250)

            cmd.ExecuteNonQuery()
            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub supprimerLigneFixe(ByVal oLigneFixe As LigneFixe)
        Dim oTransaction As SqlTransaction


        Dim sRequete As String = "Update LigneTechnique Set"
        sRequete += " dateDesactivation=@dateDesactivation"
        sRequete += " Where idLigne=@idLigne"


        Dim sql As New cSQL("GestionParcFixe"),
       cmd As New SqlCommand(sRequete, sql.connexion)
        oTransaction = sql.connexion.BeginTransaction()
        cmd.Transaction = oTransaction

        Try
            cmd.Parameters.Add("@idLigne", SqlDbType.Int).Value = oLigneFixe.nIdLigneFixe
            cmd.Parameters.Add("@dateDesactivation", SqlDbType.DateTime).Value = Now

            cmd.ExecuteNonQuery()
            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub



    Public Shared Sub EditerLigne(ByVal oLigne As Ligne)
        Dim transaction As SqlTransaction
        Dim requete As String = "UPDATE Ligne Set"
        requete += " numeroGsm = @numeroGsm,"
        requete += "  numeroData = @numeroData,"
        requete += "  nom = @nom,"
        requete += "  prenom = @prenom,"
        requete += "  idFonction = @idFonction, "
        requete += " idCompteFacturant = @idCompteFacturant,"
        requete += "  idDiscrimination = @idDiscrimination,"
        requete += "  idForfait = @idForfait,"
        requete += "  idHierarchie = @idHierarchie"
        requete += "  WHERE idLigne=@idLigne"
        Dim sql As New cSQL("GestionParc"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction
        Try
            cmd.Parameters.Add("@numeroGsm", SqlDbType.VarChar, 50).Value = oLigne.sNumeroGSM
            cmd.Parameters.Add("@numeroData", SqlDbType.VarChar, 50).Value = oLigne.sNumeroData
            cmd.Parameters.Add("@nom", SqlDbType.VarChar, 50).Value = oLigne.sNom
            cmd.Parameters.Add("@prenom", SqlDbType.VarChar, 50).Value = oLigne.sPrenom
            cmd.Parameters.Add("@idFonction", SqlDbType.Int).Value = oLigne.oFonction.nIdFonction
            cmd.Parameters.Add("@idCompteFacturant", SqlDbType.Int).Value = oLigne.oCompteFacturant.nIdCompteFacturant
            cmd.Parameters.Add("@idDiscrimination", SqlDbType.Int).Value = oLigne.oDiscrimination.nIdDiscrimination
            cmd.Parameters.Add("@idForfait", SqlDbType.Int).Value = oLigne.oForfait.nIdForfait
            cmd.Parameters.Add("@idHierarchie", SqlDbType.Int).Value = oLigne.oHierarchie.nIdHierarchie
            cmd.Parameters.Add("@idLigne", SqlDbType.Int).Value = oLigne.nIdLigne

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub activerLigne(ByVal oLigne As Ligne)
        Dim transaction As SqlTransaction
        Dim requete As String = "UPDATE Ligne Set"
        requete += " numeroGsm = @numeroGsm,"
        requete += "  dateActivation = @dateActivation"
        requete += "  WHERE idLigne=@idLigne"
        Dim sql As New cSQL("GestionParc"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction
        Try
            cmd.Parameters.Add("@numeroGsm", SqlDbType.VarChar, 50).Value = oLigne.sNumeroGSM
            cmd.Parameters.Add("@dateActivation", SqlDbType.DateTime).Value = oLigne.dDateActivation
            cmd.Parameters.Add("@idLigne", SqlDbType.Int).Value = oLigne.nIdLigne

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub activerLigneFixe(ByVal oLigneFixe As LigneFixe)
        Dim transaction As SqlTransaction
        Dim requete As String = "UPDATE LigneTechnique Set"
        requete += " numeroLigne = @numeroLigne,"
        requete += "  dateActivation = @dateActivation"
        requete += "  WHERE idLigne=@idLigne"
        Dim sql As New cSQL("GestionParcFixe"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction
        Try
            cmd.Parameters.Add("@numeroLigne", SqlDbType.VarChar, 50).Value = oLigneFixe.sNumeroLigneFixe
            cmd.Parameters.Add("@dateActivation", SqlDbType.DateTime).Value = oLigneFixe.dDateActivation
            cmd.Parameters.Add("@idLigne", SqlDbType.Int).Value = oLigneFixe.nIdLigneFixe

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub


    Public Shared Sub supprimerLigne(ByVal oLigne As Ligne)
        Dim transaction As SqlTransaction
        Dim requete As String = "UPDATE Ligne Set"
        requete += " estActif = @estActif,"
        requete += "  dateDesactivation = @dateDesactivation"
        requete += "  WHERE idLigne=@idLigne"
        Dim sql As New cSQL("GestionParc"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction
        Try
            cmd.Parameters.Add("@estActif", SqlDbType.Bit).Value = False
            cmd.Parameters.Add("@dateDesactivation", SqlDbType.DateTime).Value = Now
            cmd.Parameters.Add("@idLigne", SqlDbType.Int).Value = oLigne.nIdLigne

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub mettreAJourStock(ByVal oStock As Stock)

        Dim transaction As SqlTransaction
        Dim requete As String = "UPDATE  [GestionParc].[dbo].[Stock] Set "
        requete += " codeEmei = @codeEmei,"
        requete += " numSerie = @numSerie,"
        requete += " codeReference = @codeReference,"
        requete += " codeDesimlockage1 = @codeDesimlockage1,"
        requete += " etiquetteInventaire = @etiquetteInventaire,"
        requete += " estDesimlocke = @estDesimlocke,"
        requete += " codePin1 = @codePin1,"
        requete += " codePuk1 = @codePuk1,"
        requete += " codePuk2 = @codePuk2,"
        requete += " codeVerrouillage = @codeVerrouillage,"
        requete += " estEnrole = @estEnrole,"
        requete += " etat = @etat,"
        requete += " dateSortie = @dateSortie,"
        requete += " prix = @prix,"
        requete += " idArticle = @idArticle"
        requete += " WHERE idArticleStock=@idArticleStock"

        Dim sql As New cSQL("GestionParc"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@codeEmei", SqlDbType.VarChar, 50).Value = oStock.sCodeEmei
            cmd.Parameters.Add("@numSerie", SqlDbType.VarChar, 50).Value = oStock.sNumSerie
            cmd.Parameters.Add("@codeReference", SqlDbType.VarChar, 50).Value = oStock.sCodeReference
            cmd.Parameters.Add("@codeDesimlockage1", SqlDbType.VarChar, 50).Value = oStock.sCodeDesimlockage1
            cmd.Parameters.Add("@etiquetteInventaire", SqlDbType.VarChar, 50).Value = oStock.sEtiquetteInventaire
            cmd.Parameters.Add("@estDesimlocke", SqlDbType.Bit).Value = oStock.bEstDesimlocke
            cmd.Parameters.Add("@codePin1", SqlDbType.VarChar, 50).Value = oStock.sCodePin1
            cmd.Parameters.Add("@codePuk1", SqlDbType.VarChar, 50).Value = oStock.sCodePuk1
            cmd.Parameters.Add("@codePuk2", SqlDbType.VarChar, 50).Value = oStock.sCodePuk2
            cmd.Parameters.Add("@codeVerrouillage", SqlDbType.VarChar, 50).Value = oStock.sCodeVerrouillage
            cmd.Parameters.Add("@estEnrole", SqlDbType.Bit).Value = oStock.bEstEnrole
            cmd.Parameters.Add("@etat", SqlDbType.VarChar, 50).Value = oStock.sEtat
            cmd.Parameters.Add("@dateSortie", SqlDbType.DateTime).Value = IIf(oStock.dDateSortie = Nothing, DBNull.Value, oStock.dDateSortie)
            cmd.Parameters.Add("@prix", SqlDbType.Float).Value = oStock.nPrix
            cmd.Parameters.Add("@idArticle", SqlDbType.Int).Value = oStock.oArticle.nIdArticle
            cmd.Parameters.Add("@idArticleStock", SqlDbType.Int).Value = oStock.nIdArticleStock


            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub


    Public Shared Function verifierPresenceTableLiaison(ByVal oStock As Stock) As Boolean

        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oMailings As New Mailings
        Dim oArticle As New Article
        Try
            Requete = "   Select        idArticleStock As Expr1"
            Requete += " FROM dbo.LiaisonStock"
            Requete += " WHERE idArticleStock = " + oStock.nIdArticleStock.ToString


            readerSQL = sql.chargerDataReader(Requete)
            If readerSQL.HasRows Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Function

    Public Shared Function RecupererLigneAffecteStock(ByVal oStock As Stock) As Ligne

        Dim sql As New cSQL("GestionParc")
        Dim oLigne As New Ligne
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = "   Select idLigne"
            Requete += " FROM dbo.LiaisonStock"
            Requete += " WHERE idArticleStock = " + oStock.nIdArticleStock.ToString


            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oLigne.chargerLigneParId(CInt(readerSQL("idLigne").ToString))
            Loop
            Return oLigne
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Function

    Public Shared Sub insererStock(ByVal oStock As Stock)

        Dim transaction As SqlTransaction
        Dim requete As String = "INSERT INTO Stock"
        requete += " (codeEmei, numSerie, codeReference, codeDesimlockage1, etiquetteInventaire, estDesimlocke, codePin1, codePuk1, codePuk2, codeVerrouillage, estEnrole, etat, dateRentree, prix, idArticle, idClient)"
        requete += " VALUES  (@codeEmei, @numSerie, @codeReference, @codeDesimlockage1,@etiquetteInventaire, @estDesimlocke, @codePin1, @codePuk1, @codePuk2, @codeVerrouillage, @estEnrole, @etat, GETDATE(), @prix, @idArticle, @idClient)"

        Dim sql As New cSQL("GestionParc"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@codeEmei", SqlDbType.VarChar, 50).Value = oStock.sCodeEmei
            cmd.Parameters.Add("@numSerie", SqlDbType.VarChar, 50).Value = oStock.sNumSerie
            cmd.Parameters.Add("@codeReference", SqlDbType.VarChar, 50).Value = oStock.sCodeReference
            cmd.Parameters.Add("@codeDesimlockage1", SqlDbType.VarChar, 50).Value = oStock.sCodeDesimlockage1
            cmd.Parameters.Add("@etiquetteInventaire", SqlDbType.VarChar, 50).Value = oStock.sEtiquetteInventaire
            cmd.Parameters.Add("@estDesimlocke", SqlDbType.Bit).Value = oStock.bEstDesimlocke
            cmd.Parameters.Add("@codePin1", SqlDbType.VarChar, 50).Value = oStock.sCodePin1
            cmd.Parameters.Add("@codePuk1", SqlDbType.VarChar, 50).Value = oStock.sCodePuk1
            cmd.Parameters.Add("@codePuk2", SqlDbType.VarChar, 50).Value = oStock.sCodePuk2
            cmd.Parameters.Add("@codeVerrouillage", SqlDbType.VarChar, 50).Value = oStock.sCodeVerrouillage
            cmd.Parameters.Add("@estEnrole", SqlDbType.Bit).Value = oStock.bEstEnrole
            cmd.Parameters.Add("@etat", SqlDbType.VarChar, 50).Value = oStock.sEtat
            cmd.Parameters.Add("@prix", SqlDbType.Float).Value = oStock.nPrix
            cmd.Parameters.Add("@idArticle", SqlDbType.Int).Value = oStock.oArticle.nIdArticle
            cmd.Parameters.Add("@idClient", SqlDbType.Int).Value = oStock.nIdClient


            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Sub supprimerStock(ByVal oStock As Stock)

        Dim transaction As SqlTransaction
        Dim requete As String = "DELETE FROM Stock WHERE idArticleStock=@idArticleStock"

        Dim sql As New cSQL("GestionParc"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@idArticleStock", SqlDbType.Int).Value = oStock.nIdArticleStock

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Function RecupererDernierCodeReference(ByVal sDebutCodeReference As String, ByVal nIdClient As Integer) As Long
        Dim nResultat As Long = 0
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing


        Try
            Requete = " Select  ISNULL(MAX(CONVERT(int, RIGHT(codeReference, 4))),1) As Resultat"
            Requete += " FROM dbo.Stock"
            Requete += " WHERE codeReference Like '" & sDebutCodeReference & "%' AND idClient =" & nIdClient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                nResultat = CInt(readerSQL.Item("Resultat"))
            Loop
            nResultat += 1
            Return nResultat
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try



    End Function

    Public Shared Sub ChargerCommandesATelecharger(ByRef oCommandes As Commandes, ByVal sTypeCommande As String, ByVal oClient As Client)
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oNouvelleLigne As Ligne
        Dim oAncienneLigne As Ligne
        Dim oCommande As Commande
        Dim oFonction As Fonction
        Dim oCompteFacturant As CompteFacturant
        Dim oDiscrimination As Discrimination
        Dim oForfait As Forfait
        Dim oHierarchie As Hierarchie
        Try
            Requete = " SELECT idCommande, typeCommande, estAffichable, estRealiseFacture, estTelecharge, dbo.Commande.idLigne, ancienNom, nouveauNom, ancienPrenom, nouveauPrenom, "
            Requete += " ancienNumeroData, nouveauNumeroData, ancienIdFonction, nouveauiIdFonction, ancienIdCompteFacturant, nouveauIdCompteFacturant, ancienIdDiscrimination,"
            Requete += " nouveauIdDiscrimination, ancienIdForfait, nouveauIdForfait, ancienIdHierarchie, nouveauIdHierarchie, dateCommande"
            Requete += " FROM dbo.Commande INNER JOIN dbo.Ligne ON dbo.Commande.idLigne = dbo.Ligne.idLigne"
            Requete += " WHERE typeCommande = '" + Toolbox.ReplaceQuote(sTypeCommande) + "' AND dbo.Ligne.idClient =" + oClient.nIdClient.ToString + " AND estAffichable = 1 AND estTelecharge = 0"


            readerSQL = sql.chargerDataReader(Requete)

            Do Until Not readerSQL.Read()
                oCommande = New Commande
                oAncienneLigne = New Ligne
                oNouvelleLigne = New Ligne


                ChargerPropriete(oCommande.nIdCommande, readerSQL.Item("idCommande"))
                ChargerPropriete(oCommande.sTypeCommande, readerSQL.Item("typeCommande"))
                ChargerPropriete(oCommande.bEstAffichable, readerSQL.Item("estAffichable"))
                ChargerPropriete(oCommande.bEstRealiseFacture, readerSQL.Item("estRealiseFacture"))
                ChargerPropriete(oCommande.bEstTelecharge, readerSQL.Item("estTelecharge"))

                ChargerPropriete(oAncienneLigne.nIdLigne, readerSQL.Item("idLigne"))
                ChargerPropriete(oAncienneLigne.sNom, readerSQL.Item("ancienNom"))
                ChargerPropriete(oAncienneLigne.sPrenom, readerSQL.Item("ancienPrenom"))
                ChargerPropriete(oAncienneLigne.sNumeroData, readerSQL.Item("ancienNumeroData"))
                If Not IsDBNull(readerSQL.Item("ancienIdFonction")) Then
                    oFonction = New Fonction
                    oFonction.chargerFonctionParId(CInt(readerSQL.Item("ancienIdFonction")))
                    ChargerPropriete(oAncienneLigne.oFonction, oFonction)
                End If
                If Not IsDBNull(readerSQL.Item("ancienIdCompteFacturant")) Then
                    oCompteFacturant = New CompteFacturant
                    oCompteFacturant.chargerCompteFacturantParId(CInt(readerSQL.Item("ancienIdCompteFacturant")))
                    ChargerPropriete(oAncienneLigne.oCompteFacturant, oCompteFacturant)
                End If
                If Not IsDBNull(readerSQL.Item("ancienIdDiscrimination")) Then
                    oDiscrimination = New Discrimination
                    oDiscrimination.chargerDiscriminationParId(CInt(readerSQL.Item("ancienIdDiscrimination")))
                    ChargerPropriete(oAncienneLigne.oDiscrimination, oDiscrimination)
                End If
                If Not IsDBNull(readerSQL.Item("ancienIdForfait")) Then
                    oForfait = New Forfait
                    oForfait.chargerForfaitParId(CInt(readerSQL.Item("ancienIdForfait")))
                    ChargerPropriete(oAncienneLigne.oForfait, oForfait)
                End If
                If Not IsDBNull(readerSQL.Item("ancienIdHierarchie")) Then
                    oHierarchie = New Hierarchie
                    oHierarchie.chargerHierarchieParId(CInt(readerSQL.Item("ancienIdHierarchie")))
                    ChargerPropriete(oAncienneLigne.oHierarchie, oHierarchie)
                End If
                oCommande.oAncienneLigne = oAncienneLigne

                ChargerPropriete(oNouvelleLigne.nIdLigne, readerSQL.Item("idLigne"))
                ChargerPropriete(oNouvelleLigne.sNom, readerSQL.Item("nouveauNom"))
                ChargerPropriete(oNouvelleLigne.sPrenom, readerSQL.Item("nouveauPrenom"))
                ChargerPropriete(oNouvelleLigne.sNumeroData, readerSQL.Item("nouveauNumeroData"))
                If Not IsDBNull(readerSQL.Item("nouveauiIdFonction")) Then
                    oFonction = New Fonction
                    oFonction.chargerFonctionParId(CInt(readerSQL.Item("nouveauiIdFonction")))
                    ChargerPropriete(oAncienneLigne.oFonction, oFonction)
                End If
                If Not IsDBNull(readerSQL.Item("nouveauIdCompteFacturant")) Then
                    oCompteFacturant = New CompteFacturant
                    oCompteFacturant.chargerCompteFacturantParId(CInt(readerSQL.Item("nouveauIdCompteFacturant")))
                    ChargerPropriete(oNouvelleLigne.oCompteFacturant, oCompteFacturant)
                End If
                If Not IsDBNull(readerSQL.Item("nouveauIdDiscrimination")) Then
                    oDiscrimination = New Discrimination
                    oDiscrimination.chargerDiscriminationParId(CInt(readerSQL.Item("nouveauIdDiscrimination")))
                    ChargerPropriete(oNouvelleLigne.oDiscrimination, oDiscrimination)
                End If
                If Not IsDBNull(readerSQL.Item("nouveauIdForfait")) Then
                    oForfait = New Forfait
                    oForfait.chargerForfaitParId(CInt(readerSQL.Item("nouveauIdForfait")))
                    ChargerPropriete(oNouvelleLigne.oForfait, oForfait)
                End If
                If Not IsDBNull(readerSQL.Item("nouveauIdHierarchie")) Then
                    oHierarchie = New Hierarchie
                    oHierarchie.chargerHierarchieParId(CInt(readerSQL.Item("nouveauIdHierarchie")))
                    ChargerPropriete(oNouvelleLigne.oHierarchie, oHierarchie)
                End If
                oCommande.oNouvelleLigne = oNouvelleLigne

                oCommande.oOptionsAjoutees = cDal.chargerOptionCommandeMobile("Ajout", oCommande.nIdCommande)
                oCommande.oOptionsRetirees = cDal.chargerOptionCommandeMobile("Retrait", oCommande.nIdCommande)

                oCommande.oStockAjoutes = cDal.chargerStockCommandeMobile("Ajout", oCommande.nIdCommande)
                oCommande.oStockRetires = cDal.chargerStockCommandeMobile("Retrait", oCommande.nIdCommande)

                oCommandes.Add(oCommande)
            Loop



        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub ChargerCommandeParIdLigne(ByRef oCommande As CommandeFixe)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Requete = " SELECT dbo.Commande.idCommande,"
        Requete += " dbo.Commande.numeroCommande, "
        Requete += " dbo.Commande.typeCommande, "
        Requete += " dbo.Commande.estRealiseFacture, "
        Requete += " dbo.Commande.estTelecharge, "
        Requete += " dbo.Commande.idLigne, "
        Requete += " dbo.Commande.accesReseau_contratLigne, "
        Requete += " dbo.Commande.accesReseau_numero, "
        Requete += " dbo.Commande.accesReseau_resilliationPartielleInstallation, "
        Requete += " dbo.Commande.accesReseau_nombreAcces, "
        Requete += " dbo.Commande.accesReseau_nombreSda, "
        Requete += " dbo.Commande.accesReseau_specialisationCanauxArrivee,"
        Requete += " dbo.Commande.accesReseau_specialisationCanauxMixte, "
        Requete += " dbo.Commande.accesReseau_specialisationCanauxDepart,"
        Requete += " dbo.Commande.adresseInstallation_nomEtablissement,"
        Requete += " dbo.Commande.adresseInstallation_adresse, "
        Requete += " dbo.Commande.adresseInstallation_codePostal,"
        Requete += " dbo.Commande.adresseInstallation_localite,"
        Requete += " dbo.Commande.adresseInstallation_complementAdresse, "
        Requete += " dbo.Commande.contactInstallation_nom,"
        Requete += " dbo.Commande.contactInstallation_mobile,"
        Requete += " dbo.Commande.contactInstallation_telephone, "
        Requete += " dbo.Commande.contactInstallation_email, "
        Requete += " dbo.Commande.dateDisposition_dateDispositionLocaux,"
        Requete += " dbo.Commande.dateDisposition_dateMiseService, "
        Requete += " dbo.Commande.facturation_compteFacturantLigne,"
        Requete += " dbo.Commande.restitutionEquipement,"
        Requete += " dbo.Commande.servicesTelephonie_MessagerieVocale, "
        Requete += " dbo.Commande.servicesTelephonie_TransfertAppel,"
        Requete += " dbo.Commande.servicesTelephonie_SecretPermanent, "
        Requete += " dbo.Commande.servicesTelephonie_TransfertAppelNonReponse,"
        Requete += " dbo.Commande.servicesTelephonie_NumeroRenvoi, "
        Requete += " dbo.Commande.servicesTelephonie_RenvoiTerminal,"
        Requete += " dbo.Commande.servicesTelephonie_TypeSelection,"
        Requete += " dbo.Commande.annuaire_ChoixListe, "
        Requete += " dbo.Commande.annuaire_PrecisionsParution,"
        Requete += " dbo.Commande.choixModem_typeModem,"
        Requete += " dbo.Commande.adresseEmail_type, "
        Requete += " dbo.Commande.adresseEmail_adresse, "
        Requete += " dbo.Commande.commentaires,"
        Requete += " dbo.Commande.idUtilisateurPrincipal,"
        Requete += " dbo.Commande.idUtilisateurSecondaire, "
        Requete += " dbo.Commande.dateCommande,"
        Requete += " dbo.Commande.referenceClient "
        Requete += " FROM dbo.Commande "
        Requete += " WHERE idLigne=@nIdLigne"
        Dim cmd As New SqlCommand(Requete, sql.connexion)
        Dim readerSQL As SqlDataReader = Nothing

        Try
            cmd.Parameters.Add("@nIdLigne", SqlDbType.Int).Value = oCommande.oLigneFixe.nIdLigneFixe
            readerSQL = cmd.ExecuteReader()

            Do Until Not readerSQL.Read()
                Dim oLigne = New LigneFixe()
                oLigne.oContratFixe = New ContratFixe()
                oCommande.oLigneFixe = oLigne

                ChargerPropriete(oCommande.nIdCommande, readerSQL.Item("idCommande"))
                ChargerPropriete(oCommande.sTypeCommande, readerSQL.Item("typeCommande"))
                ChargerPropriete(oCommande.bEstRealiseFacture, readerSQL.Item("estRealiseFacture"))
                ChargerPropriete(oCommande.bEstTelecharge, readerSQL.Item("estTelecharge"))
                ChargerPropriete(oCommande.oLigneFixe.nIdLigneFixe, readerSQL.Item("idLigne"))
                ChargerPropriete(oCommande.oLigneFixe.oContratFixe.sLibelleContrat, readerSQL.Item("accesReseau_contratLigne"))
                ChargerPropriete(oCommande.oLigneFixe.sNumeroLigneFixe, readerSQL.Item("accesReseau_numero"))
                ChargerPropriete(oCommande.bResiliationPartielle, readerSQL.Item("accesReseau_resilliationPartielleInstallation"))
                ChargerPropriete(oCommande.oLigneFixe.nNombreAcces, readerSQL.Item("accesReseau_nombreAcces"))
                ChargerPropriete(oCommande.oLigneFixe.nNombreSDA, readerSQL.Item("accesReseau_nombreSda"))
                ChargerPropriete(oCommande.oLigneFixe.nSpecialisationCanauxArrive, readerSQL.Item("accesReseau_specialisationCanauxArrivee"))
                ChargerPropriete(oCommande.oLigneFixe.nSpecialisationCanauxMixte, readerSQL.Item("accesReseau_specialisationCanauxMixte"))
                ChargerPropriete(oCommande.oLigneFixe.nSpecialisationCanauxDepart, readerSQL.Item("accesReseau_specialisationCanauxDepart"))
                ChargerPropriete(oCommande.oLigneFixe.sNomEtablissement, readerSQL.Item("adresseInstallation_nomEtablissement"))
                ChargerPropriete(oCommande.oLigneFixe.sAdresse, readerSQL.Item("adresseInstallation_adresse"))
                ChargerPropriete(oCommande.oLigneFixe.sCodePostal, readerSQL.Item("adresseInstallation_codePostal"))
                ChargerPropriete(oCommande.oLigneFixe.sLocalite, readerSQL.Item("adresseInstallation_localite"))
                ChargerPropriete(oCommande.oLigneFixe.sComplementAdresse, readerSQL.Item("adresseInstallation_complementAdresse"))
                ChargerPropriete(oCommande.oLigneFixe.sNomContact, readerSQL.Item("contactInstallation_nom"))
                ChargerPropriete(oCommande.oLigneFixe.sFixeContact, readerSQL.Item("contactInstallation_telephone"))
                ChargerPropriete(oCommande.oLigneFixe.sMobileContact, readerSQL.Item("contactInstallation_mobile"))
                ChargerPropriete(oCommande.oLigneFixe.sMailContact, readerSQL.Item("contactInstallation_email"))
                ChargerPropriete(oCommande.oLigneFixe.dDateDispositionLocaux, readerSQL.Item("dateDisposition_dateDispositionLocaux"))
                ChargerPropriete(oCommande.oLigneFixe.dDateMiseEnService, readerSQL.Item("dateDisposition_dateMiseService"))
                oCommande.oLigneFixe.oCompteFacturantFixe = New CompteFacturantFixe()
                ChargerPropriete(oCommande.oLigneFixe.oCompteFacturantFixe.sLibelleCompteFacturant, readerSQL.Item("facturation_compteFacturantLigne"))
                ChargerPropriete(oCommande.oLigneFixe.bRestitutionEquipementLoues, readerSQL.Item("restitutionEquipement"))
                ChargerPropriete(oCommande.oLigneFixe.bStMessagerieVocale, readerSQL.Item("servicesTelephonie_MessagerieVocale"))
                ChargerPropriete(oCommande.oLigneFixe.bStTransfertAppel, readerSQL.Item("servicesTelephonie_TransfertAppel"))
                ChargerPropriete(oCommande.oLigneFixe.bStSecretPermanent, readerSQL.Item("servicesTelephonie_SecretPermanent"))
                ChargerPropriete(oCommande.oLigneFixe.bStTransfertAppelNonReponse, readerSQL.Item("servicesTelephonie_TransfertAppelNonReponse"))
                ChargerPropriete(oCommande.oLigneFixe.sStNumeroRenvoi, readerSQL.Item("servicesTelephonie_NumeroRenvoi"))
                ChargerPropriete(oCommande.oLigneFixe.bStRenvoiTerminal, readerSQL.Item("servicesTelephonie_RenvoiTerminal"))
                oCommande.oLigneFixe.oTypeSelectionFixe = New TypeSelectionFixe
                ChargerPropriete(oCommande.oLigneFixe.oTypeSelectionFixe.sLibelleTypeSelection, readerSQL.Item("servicesTelephonie_TypeSelection"))
                oCommande.oLigneFixe.oChoixListeFixe = New ChoixListeFixe
                ChargerPropriete(oCommande.oLigneFixe.oChoixListeFixe.sLibelleChoixListe, readerSQL.Item("annuaire_ChoixListe"))
                ChargerPropriete(oCommande.oLigneFixe.sIaPrecisionsComplementairesParution, readerSQL.Item("annuaire_PrecisionsParution"))
                ChargerPropriete(oCommande.oLigneFixe.sChoixModem, readerSQL.Item("choixModem_typeModem"))
                ChargerPropriete(oCommande.oLigneFixe.sTypeAdresseEmail, readerSQL.Item("adresseEmail_type"))
                ChargerPropriete(oCommande.oLigneFixe.sAdresseEmail, readerSQL.Item("adresseEmail_adresse"))
                ChargerPropriete(oCommande.oLigneFixe.sCommentaire, readerSQL.Item("commentaires"))
                ChargerPropriete(oCommande.nIdUtilisateurPrincipal, readerSQL.Item("idUtilisateurPrincipal"))
                ChargerPropriete(oCommande.nIdUtilisateurSecondaire, readerSQL.Item("idUtilisateurSecondaire"))
                ChargerPropriete(oCommande.dDateCommande, readerSQL.Item("dateCommande"))
                ChargerPropriete(oCommande.oLigneFixe.sReferenceClient, readerSQL.Item("referenceClient"))

                Exit Do
            Loop

        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Sub ChargerCommandesFixeATelecharger(ByRef oCommandes As CommandeFixes, ByVal sTypeCommande As String, ByVal oClient As Client)
        Dim sql As New cSQL("GestionParcFixe")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oCommande As CommandeFixe

        Try
            Requete = " SELECT        dbo.Commande.idCommande,"
            Requete += " dbo.Commande.numeroCommande, "
            Requete += " dbo.Commande.typeCommande, "
            Requete += " dbo.Commande.estRealiseFacture, "
            Requete += " dbo.Commande.estTelecharge, "
            Requete += " dbo.Commande.idLigne, "
            Requete += " dbo.Commande.accesReseau_contratLigne, "
            Requete += " dbo.Commande.accesReseau_numero, "
            Requete += " dbo.Commande.accesReseau_resilliationPartielleInstallation, "
            Requete += " dbo.Commande.accesReseau_nombreAcces, "
            Requete += " dbo.Commande.accesReseau_nombreSda, "
            Requete += " dbo.Commande.accesReseau_specialisationCanauxArrivee,"
            Requete += " dbo.Commande.accesReseau_specialisationCanauxMixte, "
            Requete += " dbo.Commande.accesReseau_specialisationCanauxDepart,"
            Requete += " dbo.Commande.adresseInstallation_nomEtablissement,"
            Requete += " dbo.Commande.adresseInstallation_adresse, "
            Requete += " dbo.Commande.adresseInstallation_codePostal,"
            Requete += " dbo.Commande.adresseInstallation_localite,"
            Requete += " dbo.Commande.adresseInstallation_complementAdresse, "
            Requete += " dbo.Commande.contactInstallation_nom,"
            Requete += " dbo.Commande.contactInstallation_mobile,"
            Requete += " dbo.Commande.contactInstallation_telephone, "
            Requete += " dbo.Commande.contactInstallation_email, "
            Requete += " dbo.Commande.dateDisposition_dateDispositionLocaux,"
            Requete += " dbo.Commande.dateDisposition_dateMiseService, "
            Requete += " dbo.Commande.facturation_compteFacturantLigne,"
            Requete += " dbo.Commande.restitutionEquipement,"
            Requete += " dbo.Commande.servicesTelephonie_MessagerieVocale, "
            Requete += " dbo.Commande.servicesTelephonie_TransfertAppel,"
            Requete += " dbo.Commande.servicesTelephonie_SecretPermanent, "
            Requete += " dbo.Commande.servicesTelephonie_TransfertAppelNonReponse,"
            Requete += " dbo.Commande.servicesTelephonie_NumeroRenvoi, "
            Requete += " dbo.Commande.servicesTelephonie_RenvoiTerminal,"
            Requete += " dbo.Commande.servicesTelephonie_TypeSelection,"
            Requete += " dbo.Commande.annuaire_ChoixListe, "
            Requete += " dbo.Commande.annuaire_PrecisionsParution,"
            Requete += " dbo.Commande.choixModem_typeModem,"
            Requete += " dbo.Commande.adresseEmail_type, "
            Requete += " dbo.Commande.adresseEmail_adresse, "
            Requete += " dbo.Commande.commentaires,"
            Requete += " dbo.Commande.idUtilisateurPrincipal,"
            Requete += " dbo.Commande.idUtilisateurSecondaire, "
            Requete += " dbo.Commande.dateCommande,"
            Requete += " dbo.Commande.referenceClient "
            Requete += " FROM            dbo.Commande INNER JOIN"
            Requete += " dbo.LigneTechnique ON dbo.Commande.idLigne = dbo.LigneTechnique.idLigne"
            Requete += " WHERE typeCommande = '" + Toolbox.ReplaceQuote(sTypeCommande) + "' AND dbo.LigneTechnique.idClient =" + oClient.nIdClient.ToString + " AND estTelecharge = 0"


            readerSQL = sql.chargerDataReader(Requete)

            Do Until Not readerSQL.Read()
                oCommande = New CommandeFixe
                Dim oLigne = New LigneFixe
                ChargerPropriete(oCommande.nIdCommande, readerSQL.Item("idCommande"))
                ChargerPropriete(oCommande.sTypeCommande, readerSQL.Item("typeCommande"))
                ChargerPropriete(oCommande.bEstRealiseFacture, readerSQL.Item("estRealiseFacture"))
                ChargerPropriete(oCommande.bEstTelecharge, readerSQL.Item("estTelecharge"))
                ChargerPropriete(oLigne.nIdLigneFixe, readerSQL.Item("idLigne"))
                oLigne.oContratFixe = New ContratFixe
                ChargerPropriete(oLigne.oContratFixe.sLibelleContrat, readerSQL.Item("accesReseau_contratLigne"))
                ChargerPropriete(oLigne.sNumeroLigneFixe, readerSQL.Item("accesReseau_numero"))
                ChargerPropriete(oCommande.bResiliationPartielle, readerSQL.Item("accesReseau_resilliationPartielleInstallation"))
                ChargerPropriete(oLigne.nNombreAcces, readerSQL.Item("accesReseau_nombreAcces"))
                ChargerPropriete(oLigne.nNombreSDA, readerSQL.Item("accesReseau_nombreSda"))
                ChargerPropriete(oLigne.nSpecialisationCanauxArrive, readerSQL.Item("accesReseau_specialisationCanauxArrivee"))
                ChargerPropriete(oLigne.nSpecialisationCanauxMixte, readerSQL.Item("accesReseau_specialisationCanauxMixte"))
                ChargerPropriete(oLigne.nSpecialisationCanauxDepart, readerSQL.Item("accesReseau_specialisationCanauxDepart"))
                ChargerPropriete(oLigne.sNomEtablissement, readerSQL.Item("adresseInstallation_nomEtablissement"))
                ChargerPropriete(oLigne.sAdresse, readerSQL.Item("adresseInstallation_adresse"))
                ChargerPropriete(oLigne.sCodePostal, readerSQL.Item("adresseInstallation_codePostal"))
                ChargerPropriete(oLigne.sLocalite, readerSQL.Item("adresseInstallation_localite"))
                ChargerPropriete(oLigne.sComplementAdresse, readerSQL.Item("adresseInstallation_complementAdresse"))
                ChargerPropriete(oLigne.sNomContact, readerSQL.Item("contactInstallation_nom"))
                ChargerPropriete(oLigne.sFixeContact, readerSQL.Item("contactInstallation_telephone"))
                ChargerPropriete(oLigne.sMobileContact, readerSQL.Item("contactInstallation_mobile"))
                ChargerPropriete(oLigne.sMailContact, readerSQL.Item("contactInstallation_email"))
                ChargerPropriete(oLigne.dDateDispositionLocaux, readerSQL.Item("dateDisposition_dateDispositionLocaux"))
                ChargerPropriete(oLigne.dDateMiseEnService, readerSQL.Item("dateDisposition_dateMiseService"))
                oLigne.oCompteFacturantFixe = New CompteFacturantFixe
                ChargerPropriete(oLigne.oCompteFacturantFixe.sLibelleCompteFacturant, readerSQL.Item("facturation_compteFacturantLigne"))
                ChargerPropriete(oLigne.bRestitutionEquipementLoues, readerSQL.Item("restitutionEquipement"))
                ChargerPropriete(oLigne.bStMessagerieVocale, readerSQL.Item("servicesTelephonie_MessagerieVocale"))
                ChargerPropriete(oLigne.bStTransfertAppel, readerSQL.Item("servicesTelephonie_TransfertAppel"))
                ChargerPropriete(oLigne.bStSecretPermanent, readerSQL.Item("servicesTelephonie_SecretPermanent"))
                ChargerPropriete(oLigne.bStTransfertAppelNonReponse, readerSQL.Item("servicesTelephonie_TransfertAppelNonReponse"))
                ChargerPropriete(oLigne.sStNumeroRenvoi, readerSQL.Item("servicesTelephonie_NumeroRenvoi"))
                ChargerPropriete(oLigne.bStRenvoiTerminal, readerSQL.Item("servicesTelephonie_RenvoiTerminal"))
                oLigne.oTypeSelectionFixe = New TypeSelectionFixe
                ChargerPropriete(oLigne.oTypeSelectionFixe.sLibelleTypeSelection, readerSQL.Item("servicesTelephonie_TypeSelection"))
                oLigne.oChoixListeFixe = New ChoixListeFixe
                ChargerPropriete(oLigne.oChoixListeFixe.sLibelleChoixListe, readerSQL.Item("annuaire_ChoixListe"))
                ChargerPropriete(oLigne.sIaPrecisionsComplementairesParution, readerSQL.Item("annuaire_PrecisionsParution"))
                ChargerPropriete(oLigne.sChoixModem, readerSQL.Item("choixModem_typeModem"))
                ChargerPropriete(oLigne.sTypeAdresseEmail, readerSQL.Item("adresseEmail_type"))
                ChargerPropriete(oLigne.sAdresseEmail, readerSQL.Item("adresseEmail_adresse"))
                ChargerPropriete(oLigne.sCommentaire, readerSQL.Item("commentaires"))
                ChargerPropriete(oCommande.nIdUtilisateurPrincipal, readerSQL.Item("idUtilisateurPrincipal"))
                ChargerPropriete(oCommande.nIdUtilisateurSecondaire, readerSQL.Item("idUtilisateurSecondaire"))
                ChargerPropriete(oCommande.dDateCommande, readerSQL.Item("dateCommande"))
                ChargerPropriete(oLigne.sReferenceClient, readerSQL.Item("referenceClient"))


                oCommande.oLigneFixe = oLigne
                oCommandes.Add(oCommande)
            Loop



        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Sub

    Public Shared Function RecupererNumeroCommandeEnCoursClient(ByVal nIdClient As Integer) As Long
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim nResultat As Long = 0
        Try
            Requete = " SELECT numeroCommandeEnCours FROM Client where idClient = " + nIdClient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                nResultat = CLng(readerSQL.Item("numeroCommandeEnCours"))
            Loop

            Return nResultat
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Function

    Public Shared Function RecupererChampFromClientGestionParc(ByVal nIdClient As Integer, ByVal sChamp As String) As String
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim sResultat As String = ""
        Try
            Requete = " SELECT " + sChamp + " FROM Client where idClient = " + nIdClient.ToString

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                If Not IsDBNull(readerSQL.Item(sChamp)) Then
                    sResultat = readerSQL.Item(sChamp).ToString
                End If
            Loop

            Return sResultat
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try
    End Function

    Public Shared Sub MettreAJourNumeroCommande(ByVal nIdClient As Integer, ByVal nNumeroCommande As Long)
        Dim transaction As SqlTransaction
        Dim requete As String = "UPDATE Client SET  numeroCommandeEnCours = @numeroCommandeEnCours WHERE idClient = @idClient"

        Dim sql As New cSQL("GestionParc"),
        cmd As New SqlCommand(requete, sql.connexion)
        transaction = sql.connexion.BeginTransaction()
        cmd.Transaction = transaction

        Try
            cmd.Parameters.Add("@numeroCommandeEnCours", SqlDbType.BigInt).Value = nNumeroCommande
            cmd.Parameters.Add("@idClient", SqlDbType.Int).Value = nIdClient

            cmd.ExecuteNonQuery()
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            sql.fermerConnexion()
        End Try
    End Sub

    Public Shared Function chargerOptionCommandeMobile(ByVal sTypeAction As String, ByVal nIdCommande As Integer) As OptionsUtilisateur
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oOptionsUtilisateur As New OptionsUtilisateur
        Dim oOptionUtilisateur As OptionUtilisateur
        Try
            Requete = " SELECT idOption FROM CommandeOption where idCommande = " + nIdCommande.ToString & " AND typeAction='" + Toolbox.ReplaceQuote(sTypeAction) + "'"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oOptionUtilisateur = New OptionUtilisateur
                oOptionUtilisateur.chargerOptionParId(CInt(readerSQL.Item("idOption")))
                oOptionsUtilisateur.Add(oOptionUtilisateur)
            Loop

            Return oOptionsUtilisateur
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Function

    Public Shared Function chargerStockCommandeMobile(ByVal sTypeAction As String, ByVal nIdCommande As Integer) As Stocks
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing
        Dim oStocks As New Stocks
        Dim oStock As Stock
        Try
            Requete = " SELECT idStock FROM CommandeStock where idCommande = " + nIdCommande.ToString & " AND typeAction='" + Toolbox.ReplaceQuote(sTypeAction) + "'"

            readerSQL = sql.chargerDataReader(Requete)
            Do Until Not readerSQL.Read()
                oStock = New Stock
                oStock.chargerStockParId(CInt(readerSQL.Item("idStock")))
                oStocks.Add(oStock)
            Loop

            Return oStocks
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Function

    Public Shared Function RecupererInfosLigne(ByVal nIdClient As Integer, ByVal oLigne As Ligne) As Ligne
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = " SELECT numeroGSM, numeroData, DateActivation, DateDesactivation, Matricule, Site, Direction  FROM ligne where idClient = '" & nIdClient & "' AND idLigne='" + CStr(oLigne.nIdLigne) + "'"

            readerSQL = sql.chargerDataReader(Requete)

            Do Until Not readerSQL.Read()
                If Not DBNull.Value.Equals(readerSQL.Item("NumeroGSM")) Then
                    oLigne.sNumeroGSM = CStr(readerSQL.Item("numeroGSM"))
                Else oLigne.sNumeroGSM = "-"
                End If
                If readerSQL.Item("numeroData") <> "" Then
                    oLigne.sNumeroData = CStr(readerSQL.Item("numeroData"))
                Else oLigne.sNumeroData = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("DateActivation")) Then
                    oLigne.dDateActivation = CDate(readerSQL.Item("DateActivation"))
                    'Else oLigne.dDateActivation = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("DateDesactivation")) Then
                    oLigne.dDateDesactivation = CDate(readerSQL.Item("DateDesactivation"))
                    'Else oLigne.dDateDesactivation = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Matricule")) Then
                    oLigne.sMatricule = CStr(readerSQL.Item("Matricule"))
                Else oLigne.sMatricule = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Site")) Then
                    oLigne.sSite = CStr(readerSQL.Item("Site"))
                Else oLigne.sSite = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Direction")) Then
                    oLigne.sDirection = CStr(readerSQL.Item("Direction"))
                Else oLigne.sDirection = "-"
                End If
            Loop

            Return oLigne
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Function
    Public Shared Function RecupererInfosLigneCNES(ByVal nIdClient As Integer, ByVal oLigne As Ligne) As Ligne
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = " SELECT  dbo.LigneCNES.nom, dbo.LigneCNES.Prenom, dbo.LigneCNES.numeroGsm, dbo.LigneCNES.numeroData, dbo.LigneCNES.dateActivation, dbo.LigneCNES.dateDesactivation, dbo.LigneCNES.Matricule,"
            Requete += " dbo.LigneCNES.Site, dbo.LigneCNES.Direction, dbo.LigneCNES.Mail, dbo.LigneCNES.idFonction as Fonction, dbo.LigneCNES.idCompteFacturant as CompteFacturant, dbo.LigneCNES.idDiscrimination as Discrimination, dbo.LigneCNES.idForfait as Forfait, dbo.LigneCNES.idHierarchie as Hierarchie"
            Requete += " FROM dbo.LigneCNES INNER JOIN"
            Requete += " dbo.Hierarchie ON dbo.LigneCNES.idHierarchie = dbo.Hierarchie.idHierarchie INNER JOIN"
            Requete += " dbo.Fonction On dbo.LigneCNES.idFonction = dbo.Fonction.idFonction INNER JOIN"
            Requete += " dbo.CompteFacturant ON dbo.LigneCNES.idCompteFacturant = dbo.CompteFacturant.idCompteFacturant INNER JOIN"
            Requete += " dbo.Discrimination On dbo.LigneCNES.idDiscrimination = dbo.Discrimination.idDiscrimination INNER JOIN"
            Requete += " dbo.Forfait ON dbo.LigneCNES.idForfait = dbo.Forfait.idForfait"
            Requete += " WHERE ligneCNES.idClient = '" & nIdClient & "' AND idLigne='" + CStr(oLigne.nIdLigne) + "'"

            readerSQL = sql.chargerDataReader(Requete)

            Do Until Not readerSQL.Read()
                If Not DBNull.Value.Equals(readerSQL.Item("nom")) Then
                    oLigne.sNom = CStr(readerSQL.Item("nom"))
                Else oLigne.sNoM = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("prenom")) Then
                    oLigne.sPrenom = CStr(readerSQL.Item("prenom"))
                Else oLigne.sPrenom = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("NumeroGSM")) Then
                    oLigne.sNumeroGSM = CStr(readerSQL.Item("numeroGSM"))
                Else oLigne.sNumeroGSM = "-"
                End If
                If readerSQL.Item("numeroData") <> "" Then
                    oLigne.sNumeroData = CStr(readerSQL.Item("numeroData"))
                Else oLigne.sNumeroData = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("DateActivation")) Then
                    oLigne.dDateActivation = CDate(readerSQL.Item("DateActivation"))
                    'Else oLigne.dDateActivation = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("DateDesactivation")) Then
                    oLigne.dDateDesactivation = CDate(readerSQL.Item("DateDesactivation"))
                    'Else oLigne.lDateDesactivation = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Matricule")) Then
                    oLigne.sMatricule = CStr(readerSQL.Item("Matricule"))
                Else oLigne.sMatricule = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Site")) Then
                    oLigne.sSite = CStr(readerSQL.Item("Site"))
                Else oLigne.sSite = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Direction")) Then
                    oLigne.sDirection = CStr(readerSQL.Item("Direction"))
                Else oLigne.sDirection = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Mail")) Then
                    oLigne.sMail = CStr(readerSQL.Item("Mail"))
                Else oLigne.sMail = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Fonction")) Then
                    oLigne.oFonction.sLibelle = CStr(readerSQL.Item("Fonction"))
                Else oLigne.oFonction.sLibelle = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("CompteFacturant")) Then
                    oLigne.oCompteFacturant.sLibelle = CStr(readerSQL.Item("CompteFacturant"))
                Else oLigne.oCompteFacturant.sLibelle = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Discrimination")) Then
                    oLigne.oDiscrimination.sLibelle = CStr(readerSQL.Item("Discrimination"))
                Else oLigne.oDiscrimination.sLibelle = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Forfait")) Then
                    oLigne.oForfait.sLibelle = CStr(readerSQL.Item("Forfait"))
                Else oLigne.oForfait.sLibelle = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Hierarchie")) Then
                    oLigne.oHierarchie.sHierarchie1 = CStr(readerSQL.Item("Hierarchie"))
                Else oLigne.oHierarchie.sHierarchie1 = "-"
                End If
            Loop

            Return oLigne
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Function
    Public Shared Function RecupererInfosStructureCNES(ByVal nIdClient As Integer, ByVal oLigne As Ligne) As Ligne
        Dim sql As New cSQL("GestionParc")
        Dim Requete As String = ""
        Dim readerSQL As SqlDataReader = Nothing

        Try
            Requete = " SELECT  dbo.LigneCNES.nom, dbo.LigneCNES.Prenom, dbo.LigneCNES.numeroGsm, dbo.LigneCNES.numeroData, dbo.LigneCNES.dateActivation, dbo.LigneCNES.dateDesactivation, dbo.LigneCNES.Matricule,"
            Requete += " dbo.LigneCNES.Site, dbo.LigneCNES.Direction, dbo.LigneCNES.Mail, dbo.LigneCNES.idFonction as Fonction, dbo.LigneCNES.idCompteFacturant as CompteFacturant, dbo.LigneCNES.idDiscrimination as Discrimination, dbo.LigneCNES.idForfait as Forfait, dbo.LigneCNES.idHierarchie as Hierarchie"
            Requete += " FROM dbo.LigneCNES INNER JOIN"
            Requete += " dbo.Hierarchie ON dbo.LigneCNES.idHierarchie = dbo.Hierarchie.idHierarchie INNER JOIN"
            Requete += " dbo.Fonction On dbo.LigneCNES.idFonction = dbo.Fonction.idFonction INNER JOIN"
            Requete += " dbo.CompteFacturant ON dbo.LigneCNES.idCompteFacturant = dbo.CompteFacturant.idCompteFacturant INNER JOIN"
            Requete += " dbo.Discrimination On dbo.LigneCNES.idDiscrimination = dbo.Discrimination.idDiscrimination INNER JOIN"
            Requete += " dbo.Forfait ON dbo.LigneCNES.idForfait = dbo.Forfait.idForfait"
            Requete += " WHERE ligneCNES.idClient = '" & nIdClient & "' AND Hierarchie.Hierarchie2='" + oLigne.sNom + "' AND VIP = 'True'"

            readerSQL = sql.chargerDataReader(Requete)

            Do Until Not readerSQL.Read()
                If Not DBNull.Value.Equals(readerSQL.Item("nom")) Then
                    oLigne.sNom = CStr(readerSQL.Item("nom"))
                Else oLigne.sNom = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("prenom")) Then
                    oLigne.sPrenom = CStr(readerSQL.Item("prenom"))
                Else oLigne.sPrenom = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("NumeroGSM")) Then
                    oLigne.sNumeroGSM = CStr(readerSQL.Item("numeroGSM"))
                Else oLigne.sNumeroGSM = "-"
                End If
                If readerSQL.Item("numeroData") <> "" Then
                    oLigne.sNumeroData = CStr(readerSQL.Item("numeroData"))
                Else oLigne.sNumeroData = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("DateActivation")) Then
                    oLigne.dDateActivation = CDate(readerSQL.Item("DateActivation"))
                    'Else oLigne.dDateActivation = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("DateDesactivation")) Then
                    oLigne.dDateDesactivation = CDate(readerSQL.Item("DateDesactivation"))
                    'Else oLigne.lDateDesactivation = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Matricule")) Then
                    oLigne.sMatricule = CStr(readerSQL.Item("Matricule"))
                Else oLigne.sMatricule = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Site")) Then
                    oLigne.sSite = CStr(readerSQL.Item("Site"))
                Else oLigne.sSite = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Direction")) Then
                    oLigne.sDirection = CStr(readerSQL.Item("Direction"))
                Else oLigne.sDirection = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Mail")) Then
                    oLigne.sMail = CStr(readerSQL.Item("Mail"))
                Else oLigne.sMail = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Fonction")) Then
                    oLigne.oFonction.nIdFonction = CInt(readerSQL.Item("Fonction"))
                    'Else oLigne.oFonction.nIdFonction = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("CompteFacturant")) Then
                    oLigne.oCompteFacturant.nIdCompteFacturant = CInt(readerSQL.Item("CompteFacturant"))
                    'Else oLigne.oCompteFacturant.nIdCompteFacturant = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Discrimination")) Then
                    oLigne.oDiscrimination.nIdDiscrimination = CInt(readerSQL.Item("Discrimination"))
                    'Else oLigne.oDiscrimination.nIdDiscrimination = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Forfait")) Then
                    oLigne.oForfait.nIdForfait = CInt(readerSQL.Item("Forfait"))
                    'Else oLigne.oForfait.nIdForfait = "-"
                End If
                If Not DBNull.Value.Equals(readerSQL.Item("Hierarchie")) Then
                    oLigne.oHierarchie.nIdHierarchie = CInt(readerSQL.Item("Hierarchie"))
                    'Else oLigne.oHierarchie.nIdHierarchie = "-"
                End If
            Loop

            Return oLigne
        Catch ex As Exception
            Throw ex
        Finally
            If Not readerSQL Is Nothing Then
                sql.libererDataReader(readerSQL)
            End If
            sql.fermerConnexion()
            sql = Nothing
        End Try

    End Function
End Class

