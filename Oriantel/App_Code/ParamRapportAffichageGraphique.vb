﻿Public Class ParamRapportAffichageGraphique

    Private _oRapport As Rapport
    Public Property oRapport() As Rapport
        Get
            Return _oRapport
        End Get
        Set(ByVal value As Rapport)
            _oRapport = value
        End Set
    End Property

    Private _oAffichageGraphique As AffichageGraphique
    Public Property oAffichageGraphique() As AffichageGraphique
        Get
            Return _oAffichageGraphique
        End Get
        Set(ByVal value As AffichageGraphique)
            _oAffichageGraphique = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal oRapport As Rapport)
        Me.oRapport = oRapport
    End Sub

End Class
