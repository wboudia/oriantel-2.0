﻿Public Class Tva

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Private _nIdPeriodeDepart As Integer
    Public Property nIdPeriodeDepart() As Integer
        Get
            Return _nIdPeriodeDepart
        End Get
        Set(ByVal value As Integer)
            _nIdPeriodeDepart = value
        End Set
    End Property

    Private _nIdPeriodeFin As Integer
    Public Property nIdPeriodeFin() As Integer
        Get
            Return _nIdPeriodeFin
        End Get
        Set(ByVal value As Integer)
            _nIdPeriodeFin = value
        End Set
    End Property

    Private _nTauxTva As Single
    Public Property nTauxTva() As Single
        Get
            Return _nTauxTva
        End Get
        Set(ByVal value As Single)
            _nTauxTva = value
        End Set
    End Property

End Class
