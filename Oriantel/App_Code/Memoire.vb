﻿Public Class Memoire

    Private _bMoisGlissant As Boolean
    Public Property bMoisGlissant() As Boolean
        Get
            Return _bMoisGlissant
        End Get
        Set(ByVal value As Boolean)
            _bMoisGlissant = value
        End Set
    End Property

    Private _bDecimales As Boolean
    Public Property bDecimales() As Boolean
        Get
            Return _bDecimales
        End Get
        Set(ByVal value As Boolean)
            _bDecimales = value
        End Set
    End Property

    Private _bTTC As Boolean
    Public Property bTTC() As Boolean
        Get
            Return _bTTC
        End Get
        Set(ByVal value As Boolean)
            _bTTC = value
        End Set
    End Property

    Private _bNumeroEntier As Boolean
    Public Property bNumeroEntier() As Boolean
        Get
            Return _bNumeroEntier
        End Get
        Set(ByVal value As Boolean)
            _bNumeroEntier = value
        End Set
    End Property

    Private _sPagination As String
    Public Property sPagination() As String
        Get
            Return _sPagination
        End Get
        Set(ByVal value As String)
            _sPagination = value
        End Set
    End Property

    Private _sEnregistrement As String
    Public Property sEnregistrement() As String
        Get
            Return _sEnregistrement
        End Get
        Set(ByVal value As String)
            _sEnregistrement = value
        End Set
    End Property

    Private _sRecherche As String
    Public Property srecherche() As String
        Get
            Return _srecherche
        End Get
        Set(ByVal value As String)
            _srecherche = value
        End Set
    End Property

    Private _sOutil As String
    Public Property sOutil() As String
        Get
            Return _sOutil
        End Get
        Set(ByVal value As String)
            _sOutil = value
        End Set
    End Property

    Private _sTri As String
    Public Property sTri() As String
        Get
            Return _sTri
        End Get
        Set(ByVal value As String)
            _sTri = value
        End Set
    End Property

End Class
