Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class cSQL

    Public connexion As SqlConnection
    Public serveur As String = "192.168.144.95\extranet"
    'Public serveur As String = "192.168.31.31"
    'Public serveur As String = "localhost"
    Public base As String
    Public utilisateur As String = "user_extranet"
    Public password As String = "Oria31"

    Public Sub New()
    End Sub

    Public Sub New(ByVal pBase As String)
        base = pBase
        ouvrirConnexion()
    End Sub

    Public Function connexionString() As String
        Return serveur
    End Function

    Public Sub ouvrirConnexion()
        connexion = New SqlConnection()
        connexion.ConnectionString = "Data Source=" & serveur & ";Initial Catalog=" & base & ";User ID=" & utilisateur & ";Password=" & password
        connexion.Open()
    End Sub

    Public Sub executerRequete(ByVal pReq As String)
        Dim myCommand As SqlCommand = New SqlCommand(pReq, connexion)

        myCommand.ExecuteNonQuery()
        myCommand.Dispose()
    End Sub

    Public Function chargerDataReader(ByVal pReq As String) As SqlDataReader
        Dim commandSQL As SqlCommand = connexion.CreateCommand()
        Dim readerSQL As SqlDataReader

        commandSQL.CommandText = pReq
        readerSQL = commandSQL.ExecuteReader()

        commandSQL.Dispose()
        commandSQL = Nothing
        Return readerSQL
    End Function

    Public Sub libererDataReader(ByRef pReader As SqlDataReader)
        pReader.Close()
    End Sub

    Public Sub fermerConnexion()
        connexion.Close()
        connexion = Nothing
    End Sub

End Class