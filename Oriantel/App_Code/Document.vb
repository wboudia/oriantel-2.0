﻿Public Class Document

    Private _nIdDocument As Integer
    Public Property nIdDocument() As Integer
        Get
            Return _nIdDocument
        End Get
        Set(ByVal value As Integer)
            _nIdDocument = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _sLien As String
    Public Property sLien() As String
        Get
            Return _sLien
        End Get
        Set(ByVal value As String)
            _sLien = value
        End Set
    End Property

    Private _nOrdre As UInt16
    Public Property nOrdre() As UInt16
        Get
            Return _nOrdre
        End Get
        Set(ByVal value As UInt16)
            _nOrdre = value
        End Set
    End Property

    Private _slibelleClasse As String
    Public Property slibelleClasse() As String
        Get
            Return _slibelleClasse
        End Get
        Set(ByVal value As String)
            _slibelleClasse = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal idDocument As Integer, ByVal oClient As Client, ByVal libelle As String, ByVal lien As String, ByVal ordre As UInt16, ByVal libelleClasse As String)
        Me.nIdDocument = idDocument
        Me.oClient = oClient
        Me.sLibelle = libelle
        Me.sLien = lien
        Me.nOrdre = ordre
        Me.slibelleClasse = libelleClasse
    End Sub

End Class
