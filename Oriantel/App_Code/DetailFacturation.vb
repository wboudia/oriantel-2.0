﻿Public Class DetailFacturation

    Private _nIdFacture As Integer
    Public Property nIdFacture() As Integer
        Get
            Return _nIdFacture
        End Get
        Set(ByVal value As Integer)
            _nIdFacture = value
        End Set
    End Property

    Private _sNumeroFacture As String
    Public Property sNumeroFacture() As String
        Get
            Return _sNumeroFacture
        End Get
        Set(ByVal value As String)
            _sNumeroFacture = value
        End Set
    End Property

    Private _sLibelleOperateur As String
    Public Property sLibelleOperateur() As String
        Get
            Return _sLibelleOperateur
        End Get
        Set(ByVal value As String)
            _sLibelleOperateur = value
        End Set
    End Property

    Private _nMontantHT As Double
    Public Property nMontantHT() As Double
        Get
            Return _nMontantHT
        End Get
        Set(ByVal value As Double)
            _nMontantHT = value
        End Set
    End Property

    Private _nMontantTTC As Double
    Public Property nMontantTTC() As Double
        Get
            Return _nMontantTTC
        End Get
        Set(ByVal value As Double)
            _nMontantTTC = value
        End Set
    End Property

    Private _nMontantPayeHT As Double
    Public Property nMontantPayeHT() As Double
        Get
            Return _nMontantPayeHT
        End Get
        Set(ByVal value As Double)
            _nMontantPayeHT = value
        End Set
    End Property

    Private _sLibelleStatut As String
    Public Property sLibelleStatut() As String
        Get
            Return _sLibelleStatut
        End Get
        Set(ByVal value As String)
            _sLibelleStatut = value
        End Set
    End Property

    Private _sLibelleTypeFacture As String
    Public Property sLibelleTypeFacture() As String
        Get
            Return _sLibelleTypeFacture
        End Get
        Set(ByVal value As String)
            _sLibelleTypeFacture = value
        End Set
    End Property

    Private _oListeFacturesLiees As DetailFacturations
    Public Property oListeFacturesLiees() As DetailFacturations
        Get
            Return _oListeFacturesLiees
        End Get
        Set(ByVal value As DetailFacturations)
            _oListeFacturesLiees = value
        End Set
    End Property

    Private _oListeDatesFactures As DetailFacturationDates
    Public Property oListeDatesFactures() As DetailFacturationDates
        Get
            Return _oListeDatesFactures
        End Get
        Set(ByVal value As DetailFacturationDates)
            _oListeDatesFactures = value
        End Set
    End Property

    Private _oListeDocuments As DetailFacturationDocuments
    Public Property oListeDocuments() As DetailFacturationDocuments
        Get
            Return _oListeDocuments
        End Get
        Set(ByVal value As DetailFacturationDocuments)
            _oListeDocuments = value
        End Set
    End Property

    Private _sCommentaireLiaison As String
    Public Property sCommentaireLiaison() As String
        Get
            Return _sCommentaireLiaison
        End Get
        Set(ByVal value As String)
            _sCommentaireLiaison = value
        End Set
    End Property

    Private _sLienImageStatut As String
    Public Property sLienImageStatut() As String
        Get
            Return _sLienImageStatut
        End Get
        Set(ByVal value As String)
            _sLienImageStatut = value
        End Set
    End Property

    Private _sLibelleCompteFacturant As String
    Public Property sLibelleCompteFacturant() As String
        Get
            Return _sLibelleCompteFacturant
        End Get
        Set(ByVal value As String)
            _sLibelleCompteFacturant = value
        End Set
    End Property

    Private _sLibelleMandat As String
    Public Property sLibelleMandat() As String
        Get
            Return _sLibelleMandat
        End Get
        Set(ByVal value As String)
            _sLibelleMandat = value
        End Set
    End Property

    Private _nAnnee As Integer
    Public Property nAnnee() As Integer
        Get
            Return _nAnnee
        End Get
        Set(ByVal value As Integer)
            _nAnnee = value
        End Set
    End Property


    Private _nMois As Integer
    Public Property nMois() As Integer
        Get
            Return _nMois
        End Get
        Set(ByVal value As Integer)
            _nMois = value
        End Set
    End Property

    Private _dDateFacture As Date
    Public Property dDateFacture() As Date
        Get
            Return _dDateFacture
        End Get
        Set(ByVal value As Date)
            _dDateFacture = value
        End Set
    End Property




    Public Sub ChargerDetailFactureParId(ByVal idFacture As Integer, ByVal oUtilisateur As Utilisateur)
        cDal.ChargerDetailFactureParId(Me, idFacture, oUtilisateur)
    End Sub


End Class
