﻿Public Class Colonnes : Inherits List(Of Colonne)

    Public Sub ChargerListeColonnes(ByRef oRapport As Rapport, ByVal oCheckboxMoisGlissant As CheckBox, ByVal DivId As String, ByVal oBloquerColoneAction As HiddenField)
        cDal.ChargerListeColonnes(Me, oRapport, oCheckboxMoisGlissant, DivId, oBloquerColoneAction)
    End Sub

    Public Sub ChargerListeColonnesParRapports(ByRef oRapports As Rapports, ByVal oCheckboxMoisGlissant As CheckBox, ByVal DivId As String, ByVal oBloquerColoneAction As HiddenField)
        cDal.ChargerListeColonnesParRapports(Me, oRapports, oCheckboxMoisGlissant, DivId, oBloquerColoneAction)
    End Sub

    Public Sub ChargerListeColonnesGraphique(ByRef oRapport As Rapport, ByVal oCheckboxMoisGlissant As CheckBox, ByVal DivId As String, ByRef oSerie As Serie)
        cDal.ChargerListeColonnesGraphique(Me, oRapport, oCheckboxMoisGlissant, DivId, oSerie)
    End Sub

End Class
