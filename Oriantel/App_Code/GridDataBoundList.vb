﻿Public Class GridDataBoundList


    Private _sIdGridview As String
    Public Property sIdGridview() As String
        Get
            Return _sIdGridview
        End Get
        Set(ByVal value As String)
            _sIdGridview = value
        End Set
    End Property

    Private _oListeRapports As Rapports
    Public Property oListeRapports() As Rapports
        Get
            Return _oListeRapports
        End Get
        Set(ByVal value As Rapports)
            _oListeRapports = value
        End Set
    End Property

    Private _sListeIdRapports As String
    Public Property sListeIdRapports() As String
        Get
            Return _sListeIdRapports
        End Get
        Set(ByVal value As String)
            _sListeIdRapports = value
        End Set
    End Property

    Private _oRapportsSource As Rapports
    Public Property oRapportsSource() As Rapports
        Get
            Return _oRapportsSource
        End Get
        Set(ByVal value As Rapports)
            _oRapportsSource = value
        End Set
    End Property

    Private _oColonnes As Colonnes
    Public Property oColonnes() As colonnes
        Get
            Return _oColonnes
        End Get
        Set(ByVal value As colonnes)
            _oColonnes = value
        End Set
    End Property

    Private _oFiltresRapportSource As Filtres
    Public Property oFiltresRapportSource() As Filtres
        Get
            Return _oFiltresRapportSource
        End Get
        Set(ByVal value As Filtres)
            _oFiltresRapportSource = value
        End Set
    End Property

    Private _oRedirections As Redirections
    Public Property oRedirections() As Redirections
        Get
            Return _oRedirections
        End Get
        Set(ByVal value As Redirections)
            _oRedirections = value
        End Set
    End Property


    Private _oMemoire As Memoire
    Public Property oMemoire() As Memoire
        Get
            Return _oMemoire
        End Get
        Set(ByVal value As Memoire)
            _oMemoire = value
        End Set
    End Property


End Class
