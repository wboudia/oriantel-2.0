﻿Public Class Fonction

    Private _nIdFonction As Integer
    Public Property nIdFonction() As Integer
        Get
            Return _nIdFonction
        End Get
        Set(ByVal value As Integer)
            _nIdFonction = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Public Sub chargerFonctionParId(ByVal nIdFonction As Integer)
        cDal.chargerFonctionParId(Me, nIdFonction)
    End Sub



End Class
