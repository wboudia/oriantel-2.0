﻿Public Class Forfait

    Private _nIdForfait As Integer
    Public Property nIdForfait() As Integer
        Get
            Return _nIdForfait
        End Get
        Set(ByVal value As Integer)
            _nIdForfait = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _nPrixAbonnement As Double
    Public Property nPrixAbonnement() As Double
        Get
            Return _nPrixAbonnement
        End Get
        Set(ByVal value As Double)
            _nPrixAbonnement = value
        End Set
    End Property


    Private _oOperateur As Operateur
    Public Property oOperateur() As Operateur
        Get
            Return _oOperateur
        End Get
        Set(ByVal value As Operateur)
            _oOperateur = value
        End Set
    End Property

    Private _oTypeForfait As TypeForfait
    Public Property oTypeForfait() As TypeForfait
        Get
            Return _oTypeForfait
        End Get
        Set(ByVal value As TypeForfait)
            _oTypeForfait = value
        End Set
    End Property



    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Public Sub chargerForfaitParId(ByVal nIdForfait As Integer)
        cDal.chargerForfaitParId(Me, nIdForfait)
    End Sub

End Class
