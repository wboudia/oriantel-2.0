﻿Public Class TypeMateriel

    Private _nIdTypeMateriel As Integer
    Public Property nIdTypeMateriel() As Integer
        Get
            Return _nIdTypeMateriel
        End Get
        Set(ByVal value As Integer)
            _nIdTypeMateriel = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Public Sub chargerTypeMaterielParId(ByVal nIdTypeMateriel As Integer)
        cDal.chargerTypeMaterielParId(Me, nIdTypeMateriel)
    End Sub

End Class
