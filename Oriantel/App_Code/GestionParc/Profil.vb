﻿Public Class Profil

    Private _nIdProfil As Integer
    Public Property nIdProfil() As Integer
        Get
            Return _nIdProfil
        End Get
        Set(ByVal value As Integer)
            _nIdProfil = value
        End Set
    End Property

    Private _sNomProfil As String
    Public Property sNomProfil() As String
        Get
            Return _sNomProfil
        End Get
        Set(ByVal value As String)
            _sNomProfil = value
        End Set
    End Property

    Private _oOptionsProfils As OptionsProfils
    Public Property oOptionsProfils() As OptionsProfils
        Get
            Return _oOptionsProfils
        End Get
        Set(ByVal value As OptionsProfils)
            _oOptionsProfils = value
        End Set
    End Property

End Class
