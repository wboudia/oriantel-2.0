﻿Public Class OptionUtilisateur
    Private _nIdOption As Integer
    Public Property nIdOption() As Integer
        Get
            Return _nIdOption
        End Get
        Set(ByVal value As Integer)
            _nIdOption = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _oCategorieOptionUtilisateur As CategorieOptionUtilisateur
    Public Property oCategorieOptionUtilisateur() As CategorieOptionUtilisateur
        Get
            Return _oCategorieOptionUtilisateur
        End Get
        Set(ByVal value As CategorieOptionUtilisateur)
            _oCategorieOptionUtilisateur = value
        End Set
    End Property

    Private _nidClient As String
    Public Property nidClient() As String
        Get
            Return _nidClient
        End Get
        Set(ByVal value As String)
            _nidClient = value
        End Set
    End Property

    Public Sub chargerOptionParId(ByVal nIdOptionUtilisateur As Integer)
        cDal.chargerOptionParId(Me, nIdOptionUtilisateur)
    End Sub

End Class
