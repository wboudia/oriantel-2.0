﻿Public Class Discrimination

    Private _nIdDiscrimination As Integer
    Public Property nIdDiscrimination() As Integer
        Get
            Return _nIdDiscrimination
        End Get
        Set(ByVal value As Integer)
            _nIdDiscrimination = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Public Sub chargerDiscriminationParId(ByVal nIdDiscrimination As Integer)
        cDal.chargerDiscriminationParId(Me, nIdDiscrimination)
    End Sub

End Class
