﻿Public Class Article

    Private _nIdArticle As Integer
    Public Property nIdArticle() As Integer
        Get
            Return _nIdArticle
        End Get
        Set(ByVal value As Integer)
            _nIdArticle = value
        End Set
    End Property

    Private _oTypeMateriel As TypeMateriel
    Public Property oTypeMateriel() As TypeMateriel
        Get
            Return _oTypeMateriel
        End Get
        Set(ByVal value As TypeMateriel)
            _oTypeMateriel = value
        End Set
    End Property

    Private _oMarque As Marque
    Public Property oMarque() As Marque
        Get
            Return _oMarque
        End Get
        Set(ByVal value As Marque)
            _oMarque = value
        End Set
    End Property

    Private _sModele As String
    Public Property sModele() As String
        Get
            Return _sModele
        End Get
        Set(ByVal value As String)
            _sModele = value
        End Set
    End Property

    Private _nPrix1 As Double
    Public Property nPrix1() As Double
        Get
            Return _nPrix1
        End Get
        Set(ByVal value As Double)
            _nPrix1 = value
        End Set
    End Property

    Private _nPrix2 As Double
    Public Property nPrix2() As Double
        Get
            Return _nPrix2
        End Get
        Set(ByVal value As Double)
            _nPrix2 = value
        End Set
    End Property

    Private _sReference As String
    Public Property sReference() As String
        Get
            Return _sReference
        End Get
        Set(ByVal value As String)
            _sReference = value
        End Set
    End Property

    Private _bEstEnrolable As Boolean
    Public Property bEstEnrolable() As Boolean
        Get
            Return _bEstEnrolable
        End Get
        Set(ByVal value As Boolean)
            _bEstEnrolable = value
        End Set
    End Property

    Public Sub chargerArticleParId(ByVal nIdArticle As Integer)
        cDal.chargerArticleParId(Me, nIdArticle)
    End Sub
End Class
