﻿Public Class Operateur

    Private _nIdOperateur As Integer
    Public Property nIdOperateur() As Integer
        Get
            Return _nIdOperateur
        End Get
        Set(ByVal value As Integer)
            _nIdOperateur = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property


    Public Sub chargerOperateurParId(ByVal nIdOperateur As Integer)
        cDal.chargerOperateurParId(Me, nIdOperateur)
    End Sub

End Class
