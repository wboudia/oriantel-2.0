﻿Public Class CompteFacturant

    Private _nIdCompteFacturant As Integer
    Public Property nIdCompteFacturant() As Integer
        Get
            Return _nIdCompteFacturant
        End Get
        Set(ByVal value As Integer)
            _nIdCompteFacturant = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Public Sub chargerCompteFacturantParId(ByVal nIdCompteFacturant As Integer)
        cDal.chargerCompteFacturantParId(Me, nIdCompteFacturant)
    End Sub

End Class
