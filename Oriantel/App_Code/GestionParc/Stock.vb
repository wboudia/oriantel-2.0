﻿Public Class Stock

    Private _nIdArticleStock As Integer
    Public Property nIdArticleStock() As Integer
        Get
            Return _nIdArticleStock
        End Get
        Set(ByVal value As Integer)
            _nIdArticleStock = value
        End Set
    End Property

    Private _oArticle As Article
    Public Property oArticle() As Article
        Get
            Return _oArticle
        End Get
        Set(ByVal value As Article)
            _oArticle = value
        End Set
    End Property

    Private _sCodeReference As String
    Public Property sCodeReference() As String
        Get
            Return _sCodeReference
        End Get
        Set(ByVal value As String)
            _sCodeReference = value
        End Set
    End Property

    Private _sCodeEmei As String
    Public Property sCodeEmei() As String
        Get
            Return _sCodeEmei
        End Get
        Set(ByVal value As String)
            _sCodeEmei = value
        End Set
    End Property

    Private _sCodeDesimlockage1 As String
    Public Property sCodeDesimlockage1() As String
        Get
            Return _sCodeDesimlockage1
        End Get
        Set(ByVal value As String)
            _sCodeDesimlockage1 = value
        End Set
    End Property

    Private _sEtiquetteInventaire As String
    Public Property sEtiquetteInventaire() As String
        Get
            Return _sEtiquetteInventaire
        End Get
        Set(ByVal value As String)
            _sEtiquetteInventaire = value
        End Set
    End Property

    Private _bEstDesimlocke As Boolean
    Public Property bEstDesimlocke() As Boolean
        Get
            Return _bEstDesimlocke
        End Get
        Set(ByVal value As Boolean)
            _bEstDesimlocke = value
        End Set
    End Property

    Private _sCodeVerrouillage As String
    Public Property sCodeVerrouillage() As String
        Get
            Return _sCodeVerrouillage
        End Get
        Set(ByVal value As String)
            _sCodeVerrouillage = value
        End Set
    End Property

    Private _bEstEnrole As Boolean
    Public Property bEstEnrole() As Boolean
        Get
            Return _bEstEnrole
        End Get
        Set(ByVal value As Boolean)
            _bEstEnrole = value
        End Set
    End Property

    Private _sNumSerie As String
    Public Property sNumSerie() As String
        Get
            Return _sNumSerie
        End Get
        Set(ByVal value As String)
            _sNumSerie = value
        End Set
    End Property

    Private _sCodePin1 As String
    Public Property sCodePin1() As String
        Get
            Return _sCodePin1
        End Get
        Set(ByVal value As String)
            _sCodePin1 = value
        End Set
    End Property

    Private _sCodePuk1 As String
    Public Property sCodePuk1() As String
        Get
            Return _sCodePuk1
        End Get
        Set(ByVal value As String)
            _sCodePuk1 = value
        End Set
    End Property

    Private _sCodePuk2 As String
    Public Property sCodePuk2() As String
        Get
            Return _sCodePuk2
        End Get
        Set(ByVal value As String)
            _sCodePuk2 = value
        End Set
    End Property

    Private _sEtat As String
    Public Property sEtat() As String
        Get
            Return _sEtat
        End Get
        Set(ByVal value As String)
            _sEtat = value
        End Set
    End Property

    Private _dDateRentree As Date
    Public Property dDateRentree() As Date
        Get
            Return _dDateRentree
        End Get
        Set(ByVal value As Date)
            _dDateRentree = value
        End Set
    End Property

    Private _dDateAttribution As Date
    Public Property dDateAttribution() As Date
        Get
            Return _dDateAttribution
        End Get
        Set(ByVal value As Date)
            _dDateAttribution = value
        End Set
    End Property

    Private _nPrix As Double
    Public Property nPrix() As Double
        Get
            Return _nPrix
        End Get
        Set(ByVal value As Double)
            _nPrix = value
        End Set
    End Property

    Private _dDateSortie As Date
    Public Property dDateSortie() As Date
        Get
            Return _dDateSortie
        End Get
        Set(ByVal value As Date)
            _dDateSortie = value
        End Set
    End Property

    Private _sDernierDetenteur As String
    Public Property sDernierDetenteur() As String
        Get
            Return _sDernierDetenteur
        End Get
        Set(ByVal value As String)
            _sDernierDetenteur = value
        End Set
    End Property

    Private _sTypeLiaison As String
    Public Property sTypeLiaison() As String
        Get
            Return _sTypeLiaison
        End Get
        Set(ByVal value As String)
            _sTypeLiaison = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property




    Public Sub chargerStockParId(ByVal nIdArticleStock As Integer)
        cDal.chargerStockParId(Me, nIdArticleStock)
    End Sub

    Public Sub mettreAJourStock()
        cDal.mettreAJourStock(Me)
    End Sub

    Public Function verifierPresenceTableLiaison() As Boolean
        Return cDal.verifierPresenceTableLiaison(Me)
    End Function

    Public Sub insererStock()
        cDal.insererStock(Me)
    End Sub

    Public Sub supprimerStock()
        cDal.supprimerStock(Me)
    End Sub

    Public Function RecupererLigneAffecteStock() As Ligne
        Return cDal.RecupererLigneAffecteStock(Me)
    End Function




End Class
