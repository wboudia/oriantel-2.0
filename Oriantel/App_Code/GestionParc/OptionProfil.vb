﻿Public Class OptionProfil
    Private _nIdOptionProfil As Integer
    Public Property nIdOptionProfil() As Integer
        Get
            Return _nIdOptionProfil
        End Get
        Set(ByVal value As Integer)
            _nIdOptionProfil = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _oCategorieOptionUtilisateur As CategorieOptionUtilisateur
    Public Property oCategorieOptionUtilisateur() As CategorieOptionUtilisateur
        Get
            Return _oCategorieOptionUtilisateur
        End Get
        Set(ByVal value As CategorieOptionUtilisateur)
            _oCategorieOptionUtilisateur = value
        End Set
    End Property

    Private _nidProfil As String
    Public Property nidProfil() As String
        Get
            Return _nidProfil
        End Get
        Set(ByVal value As String)
            _nidProfil = value
        End Set
    End Property

    Public Sub chargerOptionParIdProfil(ByVal nIdOptionProfil As Integer)
        cDal.chargerOptionParIdProfil(Me, nIdOptionProfil)
    End Sub

End Class
