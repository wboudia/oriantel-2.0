﻿Public Class Stocks : Inherits List(Of Stock)


    Public Sub chargerStocksUtilisateurParIdLigne(ByVal nIdLigne As Integer)
        cDal.chargerStocksUtilisateurParIdLigne(Me, nIdLigne)
    End Sub

    Public Sub chargerStocks(ByVal nIdClient As Integer)
        cDal.chargerStocks(Me, nIdClient)
    End Sub

    Public Sub chargerStocksLibres(ByVal nIdClient As Integer)
        cDal.chargerStocksLibres(Me, nIdClient)
    End Sub

End Class
