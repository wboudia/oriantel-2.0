﻿Public Class CategorieOptionUtilisateur

    Private _nIdCategorie As Integer
    Public Property nIdCategorie() As Integer
        Get
            Return _nIdCategorie
        End Get
        Set(ByVal value As Integer)
            _nIdCategorie = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _nidClient As String
    Public Property nidClient() As String
        Get
            Return _nidClient
        End Get
        Set(ByVal value As String)
            _nidClient = value
        End Set
    End Property

    Public Sub chargerCategorieOptionUtilisateurParId(ByVal nIdCategorie As Integer)
        cDal.chargerCategorieOptionUtilisateurParId(Me, nIdCategorie)
    End Sub
End Class
