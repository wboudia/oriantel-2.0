﻿Public Class Commande

    Private _nIdCommande As Integer
    Public Property nIdCommande() As Integer
        Get
            Return _nIdCommande
        End Get
        Set(ByVal value As Integer)
            _nIdCommande = value
        End Set
    End Property

    Private _sTypeCommande As String
    Public Property sTypeCommande() As String
        Get
            Return _sTypeCommande
        End Get
        Set(ByVal value As String)
            _sTypeCommande = value
        End Set
    End Property

    Private _bEstAffichable As Boolean
    Public Property bEstAffichable() As Boolean
        Get
            Return _bEstAffichable
        End Get
        Set(ByVal value As Boolean)
            _bEstAffichable = value
        End Set
    End Property

    Private _bEstTelecharge As Boolean
    Public Property bEstTelecharge() As Boolean
        Get
            Return _bEstTelecharge
        End Get
        Set(ByVal value As Boolean)
            _bEstTelecharge = value
        End Set
    End Property

    Private _bEstRealiseFacture As Boolean
    Public Property bEstRealiseFacture() As Boolean
        Get
            Return _bEstRealiseFacture
        End Get
        Set(ByVal value As Boolean)
            _bEstRealiseFacture = value
        End Set
    End Property

    Private _oNouvelleLigne As ligne
    Public Property oNouvelleLigne() As ligne
        Get
            Return _oNouvelleLigne
        End Get
        Set(ByVal value As ligne)
            _oNouvelleLigne = value
        End Set
    End Property

    Private _oAncienneLigne As ligne
    Public Property oAncienneLigne() As ligne
        Get
            Return _oAncienneLigne
        End Get
        Set(ByVal value As ligne)
            _oAncienneLigne = value
        End Set
    End Property

    Private _oOptionsAjoutees As OptionsUtilisateur
    Public Property oOptionsAjoutees() As OptionsUtilisateur
        Get
            Return _oOptionsAjoutees
        End Get
        Set(ByVal value As OptionsUtilisateur)
            _oOptionsAjoutees = value
        End Set
    End Property

    Private _oOptionsRetirees As OptionsUtilisateur
    Public Property oOptionsRetirees() As OptionsUtilisateur
        Get
            Return _oOptionsRetirees
        End Get
        Set(ByVal value As OptionsUtilisateur)
            _oOptionsRetirees = value
        End Set
    End Property

    Private _oStockAjoutes As Stocks
    Public Property oStockAjoutes() As Stocks
        Get
            Return _oStockAjoutes
        End Get
        Set(ByVal value As Stocks)
            _oStockAjoutes = value
        End Set
    End Property

    Private _oStockRetires As Stocks
    Public Property oStockRetires() As Stocks
        Get
            Return _oStockRetires
        End Get
        Set(ByVal value As Stocks)
            _oStockRetires = value
        End Set
    End Property

    Private _nIdUtilisateurPrincipal As Integer
    Public Property nIdUtilisateurPrincipal() As Integer
        Get
            Return _nIdUtilisateurPrincipal
        End Get
        Set(ByVal value As Integer)
            _nIdUtilisateurPrincipal = value
        End Set
    End Property

    Private _nIdUtilisateurSecondaire As Integer
    Public Property nIdUtilisateurSecondaire() As Integer
        Get
            Return _nIdUtilisateurSecondaire
        End Get
        Set(ByVal value As Integer)
            _nIdUtilisateurSecondaire = value
        End Set
    End Property


    Public Sub ajouterCommande()
        cDal.ajouterCommande(Me)
    End Sub

    Public Sub PasserEnTelechargeCommande(ByVal sNumeroCommande As String)
        cDal.PasserEnTelechargeCommande(Me, sNumeroCommande)
    End Sub

End Class
