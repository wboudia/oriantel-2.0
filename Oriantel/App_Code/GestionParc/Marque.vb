﻿Public Class Marque
    Private _nIdMarque As Integer
    Public Property nIdMarque() As Integer
        Get
            Return _nIdMarque
        End Get
        Set(ByVal value As Integer)
            _nIdMarque = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Public Sub chargerMarqueParId(ByVal nIdMarque As Integer)
        cDal.chargerMarqueParId(Me, nIdMarque)
    End Sub
End Class
