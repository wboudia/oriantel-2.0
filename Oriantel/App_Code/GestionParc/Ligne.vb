﻿Public Class Ligne

    Private _nIdLigne As Integer
    Public Property nIdLigne() As Integer
        Get
            Return _nIdLigne
        End Get
        Set(ByVal value As Integer)
            _nIdLigne = value
        End Set
    End Property

    Private _sAffectation As String
    Public Property sAffectation() As String
        Get
            Return _sAffectation
        End Get
        Set(ByVal value As String)
            _sAffectation = value
        End Set
    End Property

    Private _sNumeroGSM As String
    Public Property sNumeroGSM() As String
        Get
            Return _sNumeroGSM
        End Get
        Set(ByVal value As String)
            _sNumeroGSM = value
        End Set
    End Property

    Private _sNumeroData As String
    Public Property sNumeroData() As String
        Get
            Return _sNumeroData
        End Get
        Set(ByVal value As String)
            _sNumeroData = value
        End Set
    End Property

    Private _sNom As String
    Public Property sNom() As String
        Get
            Return _sNom
        End Get
        Set(ByVal value As String)
            _sNom = value
        End Set
    End Property

    Private _sPrenom As String
    Public Property sPrenom() As String
        Get
            Return _sPrenom
        End Get
        Set(ByVal value As String)
            _sPrenom = value
        End Set
    End Property

    Private _sMail As String
    Public Property sMail() As String
        Get
            Return _sMail
        End Get
        Set(ByVal value As String)
            _sMail = value
        End Set
    End Property

    Private _sMatricule As String
    Public Property sMatricule() As String
        Get
            Return _sMatricule
        End Get
        Set(ByVal value As String)
            _sMatricule = value
        End Set
    End Property

    Private _sStructure As String
    Public Property sStructure() As String
        Get
            Return _sStructure
        End Get
        Set(ByVal value As String)
            _sStructure = value
        End Set
    End Property

    Private _sSite As String
    Public Property sSite() As String
        Get
            Return _sSite
        End Get
        Set(ByVal value As String)
            _sSite = value
        End Set
    End Property

    Private _sDirection As String
    Public Property sDirection() As String
        Get
            Return _sDirection
        End Get
        Set(ByVal value As String)
            _sDirection = value
        End Set
    End Property

    Private _sFixe As String
    Public Property sFixe() As String
        Get
            Return _sFixe
        End Get
        Set(ByVal value As String)
            _sFixe = value
        End Set
    End Property

    Private _sBureau As String
    Public Property sBureau() As String
        Get
            Return _sBureau
        End Get
        Set(ByVal value As String)
            _sBureau = value
        End Set
    End Property

    Private _sSociete As String
    Public Property sSociete() As String
        Get
            Return _sSociete
        End Get
        Set(ByVal value As String)
            _sSociete = value
        End Set
    End Property

    Private _bEstActif As Boolean
    Public Property bEstActif() As Boolean
        Get
            Return _bEstActif
        End Get
        Set(ByVal value As Boolean)
            _bEstActif = value
        End Set
    End Property

    Private _dDateActivation As Date
    Public Property dDateActivation() As Date
        Get
            Return _dDateActivation
        End Get
        Set(ByVal value As Date)
            _dDateActivation = value
        End Set
    End Property

    Private _dDateDesactivation As Date
    Public Property dDateDesactivation() As Date
        Get
            Return _dDateDesactivation
        End Get
        Set(ByVal value As Date)
            _dDateDesactivation = value
        End Set
    End Property

    Private _oFonction As Fonction
    Public Property oFonction() As Fonction
        Get
            Return _oFonction
        End Get
        Set(ByVal value As Fonction)
            _oFonction = value
        End Set
    End Property

    Private _oCompteFacturant As CompteFacturant
    Public Property oCompteFacturant() As CompteFacturant
        Get
            Return _oCompteFacturant
        End Get
        Set(ByVal value As CompteFacturant)
            _oCompteFacturant = value
        End Set
    End Property

    Private _oDiscrimination As Discrimination
    Public Property oDiscrimination() As Discrimination
        Get
            Return _oDiscrimination
        End Get
        Set(ByVal value As Discrimination)
            _oDiscrimination = value
        End Set
    End Property

    Private _oForfait As Forfait
    Public Property oForfait() As Forfait
        Get
            Return _oForfait
        End Get
        Set(ByVal value As Forfait)
            _oForfait = value
        End Set
    End Property

    Private _oHierarchie As Hierarchie
    Public Property oHierarchie() As Hierarchie
        Get
            Return _oHierarchie
        End Get
        Set(ByVal value As Hierarchie)
            _oHierarchie = value
        End Set
    End Property

    Private _oOptionsUtilisateur As OptionsUtilisateur
    Public Property oOptionsUtilisateur() As OptionsUtilisateur
        Get
            Return _oOptionsUtilisateur
        End Get
        Set(ByVal value As OptionsUtilisateur)
            _oOptionsUtilisateur = value
        End Set
    End Property

    Private _oStocks As Stocks
    Public Property oStocks() As Stocks
        Get
            Return _oStocks
        End Get
        Set(ByVal value As Stocks)
            _oStocks = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Public Sub chargerLigneParId(ByVal nIdLigne As Integer)
        cDal.chargerLigneParId(Me, nIdLigne)
        If Me.oOptionsUtilisateur Is Nothing Then
            Me.oOptionsUtilisateur = New OptionsUtilisateur
        End If
        If Me.oStocks Is Nothing Then
            Me.oStocks = New Stocks
        End If
    End Sub

    Public Sub insererLigne()
        cDal.InsererLigne(Me)
    End Sub

    Public Sub editerLigne()
        cDal.EditerLigne(Me)
    End Sub

    Public Sub supprimerLigne()
        cDal.supprimerLigne(Me)
    End Sub

    Public Sub activerLigne()
        cDal.activerLigne(Me)
    End Sub

    Public Sub enleverOption(ByVal oOptionUtilisateur As OptionUtilisateur)
        cDal.enleverOption(Me, oOptionUtilisateur)
    End Sub

    Public Sub ajouterOption(ByVal oOptionUtilisateur As OptionUtilisateur)
        cDal.ajouterOption(Me, oOptionUtilisateur)
    End Sub

    Public Sub ajouterStock(ByVal oStock As Stock)
        cDal.ajouterStock(Me, oStock)
    End Sub

    Public Sub enleverStock(ByVal oStock As Stock)
        cDal.enleverStock(Me, oStock)
    End Sub

    Public Sub RecupererAffectation()
        cDal.RecupererAffectation(Me)
    End Sub

End Class
