﻿Public Class TypeForfait

    Private _nIdTypeForfait As Integer
    Public Property nIdTypeForfait() As Integer
        Get
            Return _nIdTypeForfait
        End Get
        Set(ByVal value As Integer)
            _nIdTypeForfait = value
        End Set
    End Property

    Private _sLibelle As String
    Public Property sLibelle() As String
        Get
            Return _sLibelle
        End Get
        Set(ByVal value As String)
            _sLibelle = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Public Sub chargerTypeForfaitParId(ByVal nIdTypeForfait As Integer)
        cDal.chargerTypeForfaitParId(Me, nIdTypeForfait)
    End Sub

End Class
