﻿Public Class AffichageGraphique

    Private _nIdAffichageGraphique As Integer
    Public Property nIdAffichageGraphique() As Integer
        Get
            Return _nIdAffichageGraphique
        End Get
        Set(ByVal value As Integer)
            _nIdAffichageGraphique = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Private _oSerie As new Serie
    Public Property oSerie() As Serie
        Get
            Return _oSerie
        End Get
        Set(ByVal value As Serie)
            _oSerie = value
        End Set
    End Property

    Private _oGraphique As New Graphique
    Public Property oGraphique() As Graphique
        Get
            Return _oGraphique
        End Get
        Set(ByVal value As Graphique)
            _oGraphique = value
        End Set
    End Property

    Private _oLegende As New Legende
    Public Property oLegende() As Legende
        Get
            Return _oLegende
        End Get
        Set(ByVal value As Legende)
            _oLegende = value
        End Set
    End Property

    Private _sAbscisse As String
    Public Property sAbscisse() As String
        Get
            Return _sAbscisse
        End Get
        Set(ByVal value As String)
            _sAbscisse = value
        End Set
    End Property

    Private _sOrdonnee As String
    Public Property sOrdonnee() As String
        Get
            Return _sOrdonnee
        End Get
        Set(ByVal value As String)
            _sOrdonnee = value
        End Set
    End Property

    Private _bFiltrerPointsVides As Boolean
    Public Property bFiltrePointsVides() As Boolean
        Get
            Return _bFiltrerPointsVides
        End Get
        Set(ByVal value As Boolean)
            _bFiltrerPointsVides = value
        End Set
    End Property

    Private _sTitreAbscisse As String
    Public Property sTitreAbscisse() As String
        Get
            Return _sTitreAbscisse
        End Get
        Set(ByVal value As String)
            _sTitreAbscisse = value
        End Set
    End Property

    Private _sTitreOrdonnee As String
    Public Property sTitreOrdonnee() As String
        Get
            Return _sTitreOrdonnee
        End Get
        Set(ByVal value As String)
            _sTitreOrdonnee = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal idAffichageGraphique As Integer)
        Me.nIdAffichageGraphique = idAffichageGraphique
    End Sub

    Public Sub recupererAffichageGraphiqueParId(ByVal nIdClient As Integer)
        cDal.recupererAffichageGraphiqueParId(Me, nIdClient)
    End Sub

End Class
