﻿Public Class DetailFacturationDocument
    Private _nIdDocument As Integer
    Public Property nIdDocument() As Integer
        Get
            Return _nIdDocument
        End Get
        Set(ByVal value As Integer)
            _nIdDocument = value
        End Set
    End Property

    Private _sTitreDocument As String
    Public Property sTitreDocument() As String
        Get
            Return _sTitreDocument
        End Get
        Set(ByVal value As String)
            _sTitreDocument = value
        End Set
    End Property

    Private _sCommentaireDocument As String
    Public Property sCommentaireDocument() As String
        Get
            Return _sCommentaireDocument
        End Get
        Set(ByVal value As String)
            _sCommentaireDocument = value
        End Set
    End Property

    Private _dDateDocument As Date
    Public Property dDateDocument() As Date
        Get
            Return _dDateDocument
        End Get
        Set(ByVal value As Date)
            _dDateDocument = value
        End Set
    End Property

    Private _sNomFichier As String
    Public Property sNomFichier() As String
        Get
            Return _sNomFichier
        End Get
        Set(ByVal value As String)
            _sNomFichier = value
        End Set
    End Property

    Private _nIdFacture As Integer
    Public Property nIdFacture() As Integer
        Get
            Return _nIdFacture
        End Get
        Set(ByVal value As Integer)
            _nIdFacture = value
        End Set
    End Property
End Class
