﻿Public Class Filtre

    Private _nIdFiltre As Integer
    Public Property nIdFiltre() As Integer
        Get
            Return _nIdFiltre
        End Get
        Set(ByVal value As Integer)
            _nIdFiltre = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Private _sNomColonne As String
    Public Property sNomColonne() As String
        Get
            Return _sNomColonne
        End Get
        Set(ByVal value As String)
            _sNomColonne = value
        End Set
    End Property

    Private _sLibelleFiltre As String
    Public Property sLibelleFiltre() As String
        Get
            Return _sLibelleFiltre
        End Get
        Set(ByVal value As String)
            _sLibelleFiltre = value
        End Set
    End Property

    Private _nLongueurFiltre As UInt16
    Public Property nLongueurFiltre() As UInt16
        Get
            Return _nLongueurFiltre
        End Get
        Set(ByVal value As UInt16)
            _nLongueurFiltre = value
        End Set
    End Property

    Private _bEstVisible As Boolean
    Public Property bEstVisible() As Boolean
        Get
            Return _bEstVisible
        End Get
        Set(ByVal value As Boolean)
            _bEstVisible = value
        End Set
    End Property

    Private _bEstLieFiltre As Boolean
    Public Property bEstLieFiltre() As Boolean
        Get
            Return _bEstLieFiltre
        End Get
        Set(ByVal value As Boolean)
            _bEstLieFiltre = value
        End Set
    End Property

    Private _sValeurFiltre As String
    Public Property sValeurFiltre() As String
        Get
            Return _sValeurFiltre
        End Get
        Set(ByVal value As String)
            _sValeurFiltre = value
        End Set
    End Property

    Private _bEstTous As Boolean
    Public Property bEstTous() As Boolean
        Get
            Return _bEstTous
        End Get
        Set(ByVal value As Boolean)
            _bEstTous = value
        End Set
    End Property

    Private _bEstRestriction As Boolean
    Public Property bEstRestriction() As Boolean
        Get
            Return _bEstRestriction
        End Get
        Set(ByVal value As Boolean)
            _bEstRestriction = value
        End Set
    End Property

    Private _bEstMultiselection As Boolean
    Public Property bEstMultiselection() As Boolean
        Get
            Return _bEstMultiselection
        End Get
        Set(ByVal value As Boolean)
            _bEstMultiselection = value
        End Set
    End Property

    Private _nIdFiltreRedirection As Integer
    Public Property nIdFiltreRedirection() As Integer
        Get
            Return _nIdFiltreRedirection
        End Get
        Set(ByVal value As Integer)
            _nIdFiltreRedirection = value
        End Set
    End Property

	Private _sTypeColonne As String
    Public Property sTypeColonne() As String
        Get
            Return _sTypeColonne
        End Get
        Set(ByVal value As String)
            _sTypeColonne = value
        End Set
    End Property

	Private _sDernierValeur As String
    Public Property sDernierValeur() As String
        Get
            Return _sDernierValeur
        End Get
        Set(ByVal value As String)
            _sDernierValeur = value
        End Set
    End Property


    Public Function TesterFiltrePresentRapport(ByVal nIdFiltre As Integer, ByVal oRapport As Rapport, ByVal oRapports As Rapports, ByVal RestreidreIdRapport As Boolean) As Integer
        Return cDal.TesterFiltrePresentRapport(nIdFiltre, oRapport, oRapports, RestreidreIdRapport)
    End Function
End Class
