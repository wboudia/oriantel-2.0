﻿Public Class Modul

    Private _nIdModule As Int16
    Public Property nIdModule() As Int16
        Get
            Return _nIdModule
        End Get
        Set(ByVal value As Int16)
            _nIdModule = value
        End Set
    End Property

    Private _sTitre As String
    Public Property sTitre() As String
        Get
            Return _sTitre
        End Get
        Set(ByVal value As String)
            _sTitre = value
        End Set
    End Property

    Private _sDetail As String
    Public Property sDetail() As String
        Get
            Return _sDetail
        End Get
        Set(ByVal value As String)
            _sDetail = value
        End Set
    End Property

    Private _sLien As String
    Public Property sLien() As String
        Get
            Return _sLien
        End Get
        Set(ByVal value As String)
            _sLien = value
        End Set
    End Property

    Private _sBase As String
    Public Property sBase() As String
        Get
            Return _sBase
        End Get
        Set(ByVal value As String)
            _sBase = value
        End Set
    End Property

    Private _libelleClasse As String
    Public Property sLibelleClasse() As String
        Get
            Return _libelleClasse
        End Get
        Set(ByVal value As String)
            _libelleClasse = value
        End Set
    End Property

    Private _libelleClasseFooter As String
    Public Property sLibelleClasseFooter() As String
        Get
            Return _libelleClasseFooter
        End Get
        Set(ByVal value As String)
            _libelleClasseFooter = value
        End Set
    End Property

    Private _libelleClasseSommaire As String
    Public Property sLibelleClasseSommaire() As String
        Get
            Return _libelleClasseSommaire
        End Get
        Set(ByVal value As String)
            _libelleClasseSommaire = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal idModule As Int16)
        nIdModule = idModule
    End Sub

    Public Sub New(ByVal idModule As Int16, ByVal titre As String, ByVal detail As String, ByVal lien As String, ByVal base As String, ByVal libelleClasse As String, ByVal libelleClasseFooter As String, ByVal libelleClasseSommaire As String)
        Me.nIdModule = idModule
        Me.sTitre = titre
        Me.sDetail = detail
        Me.sLien = lien
        Me.sBase = base
        Me.sLibelleClasse = libelleClasse
        Me.sLibelleClasseFooter = libelleClasseFooter
        Me.sLibelleClasseSommaire = libelleClasseSommaire
    End Sub

    Public Sub createModuleConnexion()
        cDal.createModuleConnexion(Me)
    End Sub


    Public Sub chargerModulesParId(ByVal idModule As Integer, ByRef oClient As Client)
        cDal.chargerModulesParId(Me, idModule, oClient)
    End Sub

End Class