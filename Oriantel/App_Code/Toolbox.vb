﻿Imports Microsoft.VisualBasic
Imports System.Security.Cryptography
Imports System.Web
Imports System.Drawing
Imports System.Data
Imports System.Data.SqlClient

Public Class Toolbox

    Public Shared Function ReplaceQuote(ByVal Texte As String) As String
        ReplaceQuote = Texte.Replace("'", "''")
    End Function

    Public Shared Function cryptageMD5(ByVal pPass As String) As String

        Dim Bytes() As Byte
        Dim sb As New StringBuilder()

        If String.IsNullOrEmpty(pPass) Then
            Throw New ArgumentNullException
        End If
        Bytes = Encoding.Default.GetBytes(pPass)
        Bytes = MD5.Create().ComputeHash(Bytes)
        For x As Integer = 0 To Bytes.Length - 1
            sb.Append(Bytes(x).ToString("x2"))
        Next

        Return sb.ToString()

    End Function

    Public Shared Function ConvertirValeurFiltre(ByVal Texte As String, ByVal NomColonne As String, ByRef oClient As Client) As String
        Dim Resultat As String = ""
        Select Case NomColonne.ToUpper
            Case "MOIS", "LISTEMOIS"
                Dim myRegex As System.Text.RegularExpressions.Regex
                myRegex = New Regex("^A.+M\d{1,2}$")
                If myRegex.IsMatch(Texte) = True Then
                    Texte = Texte.Split(CChar("M"))(1)
                End If

                oClient.RecupererLangueClient()
                Select Case oClient.sLangueClient
                    Case "US"
                        Select Case Texte
                            Case "1"
                                Resultat = "January"
                            Case "2"
                                Resultat = "February"
                            Case "3"
                                Resultat = "March"
                            Case "4"
                                Resultat = "April"
                            Case "5"
                                Resultat = "May"
                            Case "6"
                                Resultat = "June"
                            Case "7"
                                Resultat = "July"
                            Case "8"
                                Resultat = "August"
                            Case "9"
                                Resultat = "September"
                            Case "10"
                                Resultat = "October"
                            Case "11"
                                Resultat = "November"
                            Case "12"
                                Resultat = "December"
                            Case "January"
                                Resultat = "1"
                            Case "Jan"
                                Resultat = "1"
                            Case "February"
                                Resultat = "2"
                            Case "Feb"
                                Resultat = "2"
                            Case "March"
                                Resultat = "3"
                            Case "Mar"
                                Resultat = "3"
                            Case "April"
                                Resultat = "4"
                            Case "Apr"
                                Resultat = "4"
                            Case "May"
                                Resultat = "5"
                            Case "June"
                                Resultat = "6"
                            Case "July"
                                Resultat = "7"
                            Case Is = "August"
                                Resultat = "8"
                            Case Is = "Aug"
                                Resultat = "8"
                            Case "September"
                                Resultat = "9"
                            Case "Sep"
                                Resultat = "9"
                            Case "October"
                                Resultat = "10"
                            Case "Oct"
                                Resultat = "10"
                            Case "November"
                                Resultat = "11"
                            Case "Nov"
                                Resultat = "11"
                            Case "December"
                                Resultat = "12"
                            Case "Dec"
                                Resultat = "12"
                        End Select
                    Case Else
                        Select Case Texte
                            Case "1"
                                Resultat = "Janvier"
                            Case "2"
                                Resultat = "Février"
                            Case "3"
                                Resultat = "Mars"
                            Case "4"
                                Resultat = "Avril"
                            Case "5"
                                Resultat = "Mai"
                            Case "6"
                                Resultat = "Juin"
                            Case "7"
                                Resultat = "Juillet"
                            Case "8"
                                Resultat = "Août"
                            Case "9"
                                Resultat = "Septembre"
                            Case "10"
                                Resultat = "Octobre"
                            Case "11"
                                Resultat = "Novembre"
                            Case "12"
                                Resultat = "Décembre"
                            Case "Janvier"
                                Resultat = "1"
                            Case "Jan"
                                Resultat = "1"
                            Case "Février"
                                Resultat = "2"
                            Case "Fév"
                                Resultat = "2"
                            Case "Mars"
                                Resultat = "3"
                            Case "Avril"
                                Resultat = "4"
                            Case "Avr"
                                Resultat = "4"
                            Case "Mai"
                                Resultat = "5"
                            Case "Juin"
                                Resultat = "6"
                            Case "Juillet"
                                Resultat = "7"
                            Case "Juil"
                                Resultat = "7"
                            Case Is = "Août"
                                Resultat = "8"
                            Case "Septembre"
                                Resultat = "9"
                            Case "Sep"
                                Resultat = "9"
                            Case "Octobre"
                                Resultat = "10"
                            Case "Oct"
                                Resultat = "10"
                            Case "Novembre"
                                Resultat = "11"
                            Case "Nov"
                                Resultat = "11"
                            Case "Décembre"
                                Resultat = "12"
                            Case "Déc"
                                Resultat = "12"
                        End Select
                End Select
            'Case "ACTIF", "FACTURE"
            '    Select Case Texte
            '        Case "True"
            '            Resultat = "Oui"
            '        Case "False"
            '            Resultat = "Non"
            '        Case Else
            '            Resultat = Texte
            '    End Select
            Case Else
                If Texte <> Nothing Then
                    Resultat = Texte
                End If
        End Select
        Return Resultat
    End Function

    Public Shared Function AjouterSigneOperation(ByVal nNombre As Integer) As String
        If nNombre = 0 Then
            Return ""
        Else
            Return CStr(nNombre)
        End If
    End Function

    Public Shared Function retraiterMoisMultiAnnee(ByVal nMois As Integer) As Integer
        Do Until nMois > 0 And nMois < 13
            If nMois <= 0 Then
                nMois += 12
            Else
                nMois -= 12
            End If
        Loop
        Return nMois
    End Function

    Public Shared Function ConstruireNumCommande(ByVal nNombreDigits As Integer, ByVal nNumCommande As Long) As String
        Dim sResultat As String = ""
        If nNumCommande.ToString.Length <= nNombreDigits Then
            sResultat = nNumCommande.ToString
            Do Until sResultat.Length <> nNombreDigits
                sResultat = "0" & sResultat
            Loop
        Else
            sResultat = "Erreur"
        End If
        Return sResultat
    End Function

    Public Shared Function VerifierEnteteLibelleMois(ByVal Texte As String, ByVal NombreMois As Integer, ByRef oClient As Client) As String
        Dim Resultat As String = ""
        If Texte.ToUpper = "LISTEMOIS" Then

            NombreMois = retraiterMoisMultiAnnee(NombreMois)

            oClient.RecupererLangueClient()
            Select Case oClient.sLangueClient
                Case "US"
                    Select Case NombreMois
                        Case 1
                            Resultat = "Jan"
                        Case 2
                            Resultat = "Feb"
                        Case 3
                            Resultat = "Mar"
                        Case 4
                            Resultat = "Apr"
                        Case 5
                            Resultat = "May"
                        Case 6
                            Resultat = "June"
                        Case 7
                            Resultat = "July"
                        Case 8
                            Resultat = "Aug"
                        Case 9
                            Resultat = "Sep"
                        Case 10
                            Resultat = "Oct"
                        Case 11
                            Resultat = "Nov"
                        Case 12
                            Resultat = "Dec"
                    End Select
                Case Else
                    Select Case NombreMois
                        Case 1
                            Resultat = "Jan"
                        Case 2
                            Resultat = "Fév"
                        Case 3
                            Resultat = "Mars"
                        Case 4
                            Resultat = "Avr"
                        Case 5
                            Resultat = "Mai"
                        Case 6
                            Resultat = "Juin"
                        Case 7
                            Resultat = "Juil"
                        Case 8
                            Resultat = "Août"
                        Case 9
                            Resultat = "Sep"
                        Case 10
                            Resultat = "Oct"
                        Case 11
                            Resultat = "Nov"
                        Case 12
                            Resultat = "Déc"
                    End Select
            End Select
        Else
            Resultat = Texte
        End If
        Return Resultat
    End Function

    Public Shared Function FormaterTableau(ByVal Texte As String) As String
        Dim sValeur As String = ""
        Dim sMef As String = ""
        Dim sCritereMEF As String = ""
        Dim sValeurMEF As String = ""
        Dim sValeurAffichage As String = ""

        sValeur = Texte.Split(CChar("§"))(0)
        sMef = Texte.Split(CChar("§"))(1)

        If sMef <> "" Then
            Dim TableMef() As String = sMef.Split(CChar("|"))
            For i = 0 To TableMef.Length - 1
                sCritereMEF = TableMef(i).Split(CChar(";"))(0)
                sValeurMEF = TableMef(i).Split(CChar(";"))(1).Replace("'", "")
                Select Case sCritereMEF.ToUpper
                    Case "VIDE"
                        If sValeur = "" Then sValeur = sValeurMEF
                    Case "ARRONDI"
                        If IsNumeric(sValeur.Replace(".", ",")) Then
                            sValeur = CStr(Math.Round(CDbl(sValeur.Replace(".", ",")), CInt(sValeurMEF)))
                        End If
                    Case "ZERO"
                        If IsNumeric(sValeur.Replace(".", ",")) Then
                            If CDbl(sValeur.Replace(".", ",")) = 0 Then sValeur = sValeurMEF
                        End If
                    Case "FORMAT"
                        If IsNumeric(sValeur.Replace(".", ",")) Then
                            sValeur = Format(CDbl(sValeur.Replace(".", ",")), sValeurMEF)
                        Else
                            sValeur = Format(sValeur, sValeurMEF)
                        End If
                End Select
            Next
            sValeur = sValeur.Trim(CChar(" "))
        End If

        Return sValeur
    End Function

    Public Shared Sub AppliquerFormatCellule(ByVal sStyle As String, ByRef oCellule As TableCell)
        Dim sTable() As String = sStyle.Split(CChar("|"))
        Dim sCritere As String = ""
        Dim sValeur As String = ""
        For i = 0 To sTable.Length - 1
            sCritere = sTable(i).ToString.Split(CChar(";"))(0)
            sValeur = sTable(i).ToString.Split(CChar(";"))(1)
            Select Case sCritere.ToUpper
                Case "TAILLEPOLICE"
                    oCellule.Font.Size = CInt(sValeur)
                Case "GRAS"
                    If sValeur.ToUpper = "OUI" Then oCellule.Font.Bold = True
                Case "ITALIQUE"
                    If sValeur.ToUpper = "OUI" Then oCellule.Font.Italic = True
                Case "SOULIGNE"
                    If sValeur.ToUpper = "OUI" Then oCellule.Font.Underline = True
                Case "COULEURTEXTE"
                    oCellule.ForeColor = System.Drawing.ColorTranslator.FromHtml(sValeur)
                Case "COULEURFOND"
                    oCellule.BackColor = System.Drawing.ColorTranslator.FromHtml(sValeur)
                Case "ALIGNEMENT"
                    oCellule.Style.Add("text-align", sValeur)
            End Select
        Next
    End Sub

    Public Shared Sub AppliquerFormatPourcentage(ByRef oCellule As TableCell)
        If CDbl(Toolbox.ConvertirNombreFormate(oCellule.Text)) > 0 Then
            oCellule.Text = "+ " & oCellule.Text & " %"
        ElseIf CDbl(Toolbox.ConvertirNombreFormate(oCellule.Text)) = 0 Then
            oCellule.Text = oCellule.Text & " %"
        Else
            oCellule.Text = "- " & oCellule.Text.Replace("-", "") & " %"
        End If
    End Sub

    Public Shared Sub AppliquerFormatProgression(ByRef oCellule As TableCell)
        If CDbl(Toolbox.ConvertirNombreFormate(oCellule.Text)) > 0 Then
            oCellule.Text = "+ " & oCellule.Text & " %" & "&nbsp;&nbsp&nbsp;&nbsp;&nbsp&nbsp;<b>" & "&uarr;" & "</b>"
        ElseIf CDbl(Toolbox.ConvertirNombreFormate(oCellule.Text)) = 0 Then
            oCellule.Text = oCellule.Text & " %"
        Else
            oCellule.Text = "- " & oCellule.Text.Replace("-", "") & " %" & "&nbsp;&nbsp&nbsp;&nbsp;&nbsp&nbsp;<b>" & "&darr;" & "</b>"
        End If
    End Sub

    Public Shared Function ReplaceBooleenSQL(ByVal sValeur As String) As String
        Dim sResultat As String = "0"
        Select Case sValeur.ToUpper
            Case "OUI"
                sResultat = "1"
            Case "NON"
                sResultat = "0"
            Case "TRUE"
                sResultat = "1"
            Case "FALSE"
                sResultat = "0"
        End Select
        Return sResultat
    End Function

    Public Shared Function ReplaceBooleenTexte(ByVal sValeur As String) As String
        Dim sResultat As String = sValeur
        Select Case sValeur.ToUpper
            Case "TRUE"
                sResultat = "Oui"
            Case "FALSE"
                sResultat = "Non"
        End Select
        Return sResultat
    End Function


    Public Shared Function FormaterJavascript(ByVal Texte As String) As String
        Dim sresultat As String = ""
        If Not Texte Is Nothing Then
            If Texte.Contains("communicationsµ24=Financ") Then
                Dim a As String = ""
            End If
            sresultat = Texte
            sresultat = sresultat.Replace("\", "\\")
            sresultat = sresultat.Replace("'", "\'")
            sresultat = sresultat.Replace("&#39;", "\'")

            sresultat = sresultat.Replace("\'\'", "\'")
        End If

        Return sresultat
    End Function

    Public Shared Function ScinderBalisesHtml(ByVal sTexte As String) As List(Of String)

        Dim iStart As Integer = InStr(sTexte, "<")
        Dim iEnd As Integer = InStr(sTexte, ">")
        Dim iLenght As Integer = iEnd - iStart

        Dim sGauche As String = ""
        Dim sDroite As String = ""
        Dim sMilieu As String = ""


        While iStart > 0 And iEnd > 0 And iLenght > 0
            If sTexte.Substring(0, 1) = "<" Then
                sGauche += sTexte.Substring(iStart - 1, iEnd)
            Else
                sDroite += sTexte.Substring(iStart - 1, iEnd - iStart + 1)
            End If

            If iStart = 1 Then
                sTexte = sTexte.Substring(0, iStart - 1) + sTexte.Substring(iLenght + 1, sTexte.Length - iLenght - 1)
            Else
                sTexte = sTexte.Substring(0, iStart - 1)
            End If
            iStart = InStr(sTexte, "<")
            iEnd = InStr(sTexte, ">")
            iLenght = iEnd - iStart


        End While
        sMilieu = LTrim(RTrim(sTexte))

        Dim sResultat As New List(Of String)
        sResultat.Add(sGauche)
        sResultat.Add(sMilieu)
        sResultat.Add(sDroite)

        Return sResultat

    End Function

    Public Shared Function RecupererListeIdRapports(ByVal oRapport As Rapport) As String
        Dim sListeIdRapports As String = ""
        Dim oRapports As New Rapports
        oRapports.chargerRappportsParModuleUtilisateurIdRapport_ListeIdRapports(oRapport.oModule.nIdModule, oRapport.oUtilisateur.nIdUtilisateur, oRapport)
        For Each oRapportTemp In oRapports
            If oRapport.nOngletOrdreNiv1 = oRapportTemp.nOngletOrdreNiv1 And oRapport.nOngletOrdreNiv2 = oRapportTemp.nOngletOrdreNiv2 Then
                sListeIdRapports &= CStr(oRapportTemp.nIdRapport) & "|"
            End If
        Next
        sListeIdRapports = sListeIdRapports.Remove(sListeIdRapports.Length - 1, 1)
        Return sListeIdRapports
    End Function

    Public Shared Function RecupererListeIdRapportsParOrdreOnglet(ByVal oRapports As Rapports, ByVal nOrdreNiv1 As Integer) As String
        Dim sListeIdRapports As String = ""
        For Each oRapportTemp In oRapports
            If nOrdreNiv1 = oRapportTemp.nOngletOrdreNiv1 Then
                sListeIdRapports &= CStr(oRapportTemp.nIdRapport) & "|"
            End If
        Next
        sListeIdRapports = sListeIdRapports.Remove(sListeIdRapports.Length - 1, 1)
        Return sListeIdRapports
    End Function

    Public Shared Function Gerer_Format(ByVal sAffichage As String, ByVal oColonne As Colonne, ByVal oCheckboxDecimales As CheckBox) As String
        If oColonne.sMefDonnees <> "" Then
            Dim sTable() As String = oColonne.sMefDonnees.Split(CChar("|"))
            Dim sCritere As String = ""
            Dim sValeur As String = ""
            Dim stmpNombreDecimale As String = ""

            For iIndiceTable = 0 To sTable.Length - 1
                sCritere = sTable(iIndiceTable).ToString.Split(CChar(";"))(0)
                sValeur = sTable(iIndiceTable).ToString.Split(CChar(";"))(1)
                Select Case sCritere.ToUpper
                    Case "NOMBRE"
                        Dim sFormat As String = "# ### ##0"
                        If sAffichage = "" Then sAffichage = "0"
                        ' Je récupère la partie après le N, dans le cas où le N ne serait pas renseigné, je renseigne 0 à la place
                        If Split(sValeur, "N").Count = 0 Then
                            stmpNombreDecimale = "0}"
                        Else
                            stmpNombreDecimale = Split(sValeur, "N")(1)
                        End If
                        If oCheckboxDecimales Is Nothing OrElse oCheckboxDecimales.Checked = False Then
                            stmpNombreDecimale = "0}"
                        End If

                        Dim iNombreDecimale As UShort = CUShort(Split(stmpNombreDecimale, "}")(0))

                        If iNombreDecimale > 0 Then
                            sFormat += "."
                            For j = 0 To (iNombreDecimale - 1)
                                sFormat += "0"
                            Next
                        End If
                        sAffichage = Microsoft.VisualBasic.Strings.Format(CDbl(sAffichage), sFormat).Replace("-  ", "-")
                End Select
            Next
        End If
        Return sAffichage
    End Function

    Public Shared Function EnleverPartieTotalNomColonne(ByVal sNomChamp As String) As String
        Dim sResultat As String = sNomChamp
        If sNomChamp.Contains("|") Then
            sResultat = sNomChamp.Split(CChar("|"))(0)
        Else
            sResultat = sNomChamp
        End If
        Return sResultat
    End Function

    Public Shared Function FormaterFiltreTop(ByVal sTexte As String, ByVal sType As String, ByVal oClient As Client) As String
        Dim sResultat As String = ""

        Dim sTous As String = "Tous"
        If oClient.sLangueClient = "US" Then sTous = "All"

        Select Case sType
            Case "Ajouter"
                If sTexte.ToUpper <> sTous.ToUpper Then
                    sResultat = "TOP " & sTexte
                Else
                    sResultat = sTexte
                End If
            Case "Enlever"
                sResultat = sTexte.Replace("TOP ", "")
        End Select
        Return sResultat
    End Function

    Public Shared Sub Fusionner_Gridview(ByRef oGrid As GridView, ByVal oColonnes As Colonnes, ByVal bAutorisationFusionnerColonnes As Boolean)
        '---Fusion
        For iIndexLigne As Integer = oGrid.Rows.Count - 2 To 0 Step -1
            Dim Ligne As GridViewRow = oGrid.Rows(iIndexLigne)
            Dim LigneSuivante As GridViewRow = oGrid.Rows(iIndexLigne + 1)
            If Ligne.RowType = DataControlRowType.DataRow And LigneSuivante.RowType = DataControlRowType.DataRow Then
                Dim idColonne As Integer = 0
                For Each oColonne In oColonnes
                    If oColonne.bEstFusionnable = True Then
                        If Ligne.Cells(idColonne).Text = LigneSuivante.Cells(idColonne).Text And Ligne.RowType = DataControlRowType.DataRow Then
                            Ligne.Cells(idColonne).RowSpan = If(LigneSuivante.Cells(idColonne).RowSpan < 2, 2, LigneSuivante.Cells(idColonne).RowSpan + 1)
                            LigneSuivante.Cells(idColonne).Visible = False
                        End If
                    End If
                    idColonne += 1
                Next
            End If
        Next


        If bAutorisationFusionnerColonnes = True Then
            Dim iNombreColSpan As Integer
            For iIndexLigne As Integer = 0 To oGrid.Rows.Count - 1
                For iColonneActuelle = 0 To oGrid.Columns.Count - 1
                    Dim oCellule As TableCell = oGrid.Rows(iIndexLigne).Cells(iColonneActuelle)
                    If oCellule.Text.Contains("columnspan") Then
                        iNombreColSpan = CInt(oCellule.Text.Split(CChar(">"))(0).Split(CChar("="))(1).Split(CChar("µ"))(0))
                        If iColonneActuelle + iNombreColSpan > oGrid.Columns.Count Then iNombreColSpan = oGrid.Columns.Count - iColonneActuelle
                        oCellule.ColumnSpan = iNombreColSpan
                        If iNombreColSpan > 1 Then
                            For i = 2 To iNombreColSpan
                                If iColonneActuelle + i - 1 < oGrid.Columns.Count Then
                                    oGrid.Rows(iIndexLigne).Cells(iColonneActuelle + i - 1).Visible = False
                                End If
                            Next
                            iColonneActuelle += iNombreColSpan - 1
                        End If
                    End If
                Next
            Next
        End If



    End Sub

    Public Shared Function ConvertirNombreFormate(ByVal Texte As String) As String
        Texte = Texte.Replace("&#160;", "")
        Texte = Texte.Replace("&nbsp;", "")
        Return Texte
    End Function

    Public Shared Function ConvertirHtml(ByVal Texte As String) As String
        Return HttpUtility.HtmlDecode(Texte)
    End Function

    Public Shared Function RemoveTagHTML(ByVal sTexte As String) As String
        If Not sTexte Is Nothing Then
            Return Regex.Replace(sTexte, "<.*?>", "")
        Else
            sTexte = ""
        End If

        Return sTexte
    End Function


    Public Shared Function GetPropertyValue(ByVal obj As Object, ByVal PropName As String) As Object
        Dim objType As Type
        Dim pInfo As System.Reflection.PropertyInfo
        Dim PropValue As Object
        Dim sTexte As String = ""
        Try
            '---Permet d'atteindre les valeurs de classes imbriquées
            If PropName.Contains(".") Then
                Do Until Not PropName.Contains(".")
                    objType = obj.GetType()
                    pInfo = objType.GetProperty(PropName.Split(CChar("."))(0))
                    PropValue = pInfo.GetValue(obj, Reflection.BindingFlags.GetProperty, Nothing, Nothing, Nothing)
                    obj = PropValue
                    For i = 1 To PropName.Split(CChar(".")).Count - 1
                        sTexte = PropName.Split(CChar("."))(i) & "."
                    Next
                    If sTexte <> "" Then sTexte = sTexte.Remove(sTexte.Length - 1, 1)
                    PropName = sTexte
                Loop
            End If

            objType = obj.GetType()
            pInfo = objType.GetProperty(PropName)
            PropValue = pInfo.GetValue(obj, Reflection.BindingFlags.GetProperty, Nothing, Nothing, Nothing)
        Catch ex As Exception
            PropValue = Nothing
        End Try
        Return PropValue
    End Function

    Public Shared Function AjouterChampRequete(ByVal sChamp As String, ByVal sType As String, ByVal oObjet As Object) As String
        Dim sResultat As String = ""
        Select Case sType.ToUpper
            Case "NUMERIQUE"
                If Not oObjet Is Nothing Then
                    If Not CType(oObjet, Integer) = 0 Then sResultat = sChamp & ", "
                End If
            Case "TEXTE", "BOOLEEN"
                If Not oObjet Is Nothing Then sResultat = sChamp & ", "
            Case "DATE"
                If Not oObjet Is Nothing Then
                    If IsDate(oObjet) Then
                        If oObjet > New Date(2000, 1, 1) Then
                            If Not oObjet Is Nothing Then sResultat = sChamp & ", "
                        End If
                    End If
                End If
        End Select
        Return sResultat
    End Function

    Public Shared Sub AjouterParametreRequete(ByRef cmd As SqlCommand, ByVal sChamp As String, ByVal sType As String, ByVal oObjet As Object, Optional nTailleVarchar As Integer = 0)
        Select Case sType.ToUpper
            Case "INT"
                If Not oObjet Is Nothing Then
                    If Not CType(oObjet, Integer) = 0 Then
                        cmd.Parameters.Add(sChamp, SqlDbType.Int).Value = oObjet
                    End If

                End If
            Case "VARCHAR", "BIT"
                If Not oObjet Is Nothing Then
                    If sType.ToUpper = "VARCHAR" Then
                        cmd.Parameters.Add(sChamp, SqlDbType.VarChar, nTailleVarchar).Value = oObjet
                    End If
                    If sType.ToUpper = "DATETIME" Then
                        cmd.Parameters.Add(sChamp, SqlDbType.DateTime).Value = oObjet
                    End If
                    If sType.ToUpper = "BIT" Then
                        cmd.Parameters.Add(sChamp, SqlDbType.Bit).Value = oObjet
                    End If
                End If
            Case "DATETIME"
                If Not oObjet Is Nothing Then
                    If IsDate(oObjet) Then
                        If oObjet > New Date(2000, 1, 1) Then
                            cmd.Parameters.Add(sChamp, SqlDbType.DateTime).Value = oObjet
                        End If
                    End If
                End If
        End Select
    End Sub


End Class