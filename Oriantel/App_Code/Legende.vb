﻿Public Class Legende

    Private _nIdLegende As Integer
    Public Property nIdLegende() As Integer
        Get
            Return _nIdLegende
        End Get
        Set(ByVal value As Integer)
            _nIdLegende = value
        End Set
    End Property

    Private _sNomLegende As String
    Public Property sNomLegende() As String
        Get
            Return _sNomLegende
        End Get
        Set(ByVal value As String)
            _sNomLegende = value
        End Set
    End Property

    Private _bAncrageInterneGraphique As Boolean
    Public Property bAncrageInterneGraphique() As Boolean
        Get
            Return _bAncrageInterneGraphique
        End Get
        Set(ByVal value As Boolean)
            _bAncrageInterneGraphique = value
        End Set
    End Property

    Private _sPosition As String
    Public Property sPosition() As String
        Get
            Return _sPosition
        End Get
        Set(ByVal value As String)
            _sPosition = value
        End Set
    End Property

    Private _bEntrelacementLegende As Boolean
    Public Property bEntrelacementLegende() As Boolean
        Get
            Return _bEntrelacementLegende
        End Get
        Set(ByVal value As Boolean)
            _bEntrelacementLegende = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal idLegende As Integer)
        Me.nIdLegende = idLegende
    End Sub

    Public Sub New(ByVal idLegende As Integer, ByVal nomLegende As String, ByVal ancrageInterneGraphique As Boolean, ByVal position As String, ByVal entrelacementLegende As Boolean)
        Me.nIdLegende = idLegende
        Me.sNomLegende = nomLegende
        Me.bAncrageInterneGraphique = ancrageInterneGraphique
        Me.sPosition = position
        Me.bEntrelacementLegende = entrelacementLegende
    End Sub

    Public Sub recupererLegendeParId()
        cDal.recupererLegendeParId(Me)
    End Sub

End Class