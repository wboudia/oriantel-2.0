﻿Public Class ParamRapportColonne

    Private _oRapport As Rapport
    Public Property oRapport() As Rapport
        Get
            Return _oRapport
        End Get
        Set(ByVal value As Rapport)
            _oRapport = value
        End Set
    End Property

    Private _oColonne As Colonne
    Public Property oColonne() As Colonne
        Get
            Return _oColonne
        End Get
        Set(ByVal value As Colonne)
            _oColonne = value
        End Set
    End Property

    Private _bAfficher As Boolean
    Public Property bAfficher() As Boolean
        Get
            Return _bAfficher
        End Get
        Set(ByVal value As Boolean)
            _bAfficher = value
        End Set
    End Property

    Private _nOrdreColonne As Integer
    Public Property nOrdreColonne() As Integer
        Get
            Return _nOrdreColonne
        End Get
        Set(ByVal value As Integer)
            _nOrdreColonne = value
        End Set
    End Property


    Public Sub New()
    End Sub

    Public Sub New(ByVal coRapport As Rapport, ByVal coColonne As Colonne)
        oRapport = coRapport
        oColonne = coColonne
    End Sub

    Public Sub New(ByVal coRapport As Rapport, ByVal coColonne As Colonne, ByVal cbAfficher As Boolean)
        oRapport = coRapport
        oColonne = coColonne
        bAfficher = cbAfficher
    End Sub

    Public Sub SauvegardeEtatCheckboxChoixVisible()
        cDal.SauvegardeEtatCheckboxChoixVisible(Me)
    End Sub

    Public Sub affichageParamRapportColonne()
        cDal.affichageParamRapportColonne(Me)
    End Sub

End Class
