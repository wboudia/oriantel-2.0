﻿Public Class Serie

    Private _nIdSerie As Integer
    Public Property nIdSerie() As Integer
        Get
            Return _nIdSerie
        End Get
        Set(ByVal value As Integer)
            _nIdSerie = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Private _oTypeGraphique As New TypeGraphique
    Public Property oTypeGraphique() As TypeGraphique
        Get
            Return _oTypeGraphique
        End Get
        Set(ByVal value As TypeGraphique)
            _oTypeGraphique = value
        End Set
    End Property

    Private _sTooltip As String
    Public Property sTooltip() As String
        Get
            Return _sTooltip
        End Get
        Set(ByVal value As String)
            _sTooltip = value
        End Set
    End Property

    Private _sLegendeTexte As String
    Public Property sLegendeTexte() As String
        Get
            Return _sLegendeTexte
        End Get
        Set(ByVal value As String)
            _sLegendeTexte = value
        End Set
    End Property

    Private _bLabelSurGraphique As Boolean
    Public Property bLabelSurGraphique() As Boolean
        Get
            Return _bLabelSurGraphique
        End Get
        Set(ByVal value As Boolean)
            _bLabelSurGraphique = value
        End Set
    End Property

    Private _oAffichagePersonnaliseGraphique As New AffichagePersonnaliseGraphique
    Public Property oAffichagePersonnaliseGraphique() As AffichagePersonnaliseGraphique
        Get
            Return _oAffichagePersonnaliseGraphique
        End Get
        Set(ByVal value As AffichagePersonnaliseGraphique)
            _oAffichagePersonnaliseGraphique = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal idSerie As Integer)
        Me.nIdSerie = idSerie
    End Sub

    Public Sub recupererSerieParId(ByVal nIdClient As Integer)
        cDal.recupererSerieParId(Me, nIdClient)
    End Sub

End Class
