﻿Public Class LigneFixe

    Private _nIdLigneFixe As Integer
    Public Property nIdLigneFixe() As Integer
        Get
            Return _nIdLigneFixe
        End Get
        Set(ByVal value As Integer)
            _nIdLigneFixe = value
        End Set
    End Property

    Private _sNumeroLigneFixe As String
    Public Property sNumeroLigneFixe() As String
        Get
            Return _sNumeroLigneFixe
        End Get
        Set(ByVal value As String)
            _sNumeroLigneFixe = value
        End Set
    End Property

    Private _oHierarchieFixe As HierarchieFixe
    Public Property oHierarchieFixe() As HierarchieFixe
        Get
            Return _oHierarchieFixe
        End Get
        Set(ByVal value As HierarchieFixe)
            _oHierarchieFixe = value
        End Set
    End Property

    Private _oCompteFacturantFixe As CompteFacturantFixe
    Public Property oCompteFacturantFixe() As CompteFacturantFixe
        Get
            Return _oCompteFacturantFixe
        End Get
        Set(ByVal value As CompteFacturantFixe)
            _oCompteFacturantFixe = value
        End Set
    End Property

    Private _oUsageFixe As UsageFixe
    Public Property oUsageFixe() As UsageFixe
        Get
            Return _oUsageFixe
        End Get
        Set(ByVal value As UsageFixe)
            _oUsageFixe = value
        End Set
    End Property

    Private _oTypeLigneFixe As TypeLigneFixe
    Public Property oTypeLigneFixe() As TypeLigneFixe
        Get
            Return _oTypeLigneFixe
        End Get
        Set(ByVal value As TypeLigneFixe)
            _oTypeLigneFixe = value
        End Set
    End Property

    Private _dDateActivation As Date
    Public Property dDateActivation() As Date
        Get
            Return _dDateActivation
        End Get
        Set(ByVal value As Date)
            _dDateActivation = value
        End Set
    End Property

    Private _dDateDesactivation As Date
    Public Property dDateDesactivation() As Date
        Get
            Return _dDateDesactivation
        End Get
        Set(ByVal value As Date)
            _dDateDesactivation = value
        End Set
    End Property

    Private _oContratFixe As ContratFixe
    Public Property oContratFixe() As ContratFixe
        Get
            Return _oContratFixe
        End Get
        Set(ByVal value As ContratFixe)
            _oContratFixe = value
        End Set
    End Property

    Private _nNombreAcces As Integer
    Public Property nNombreAcces() As Integer
        Get
            Return _nNombreAcces
        End Get
        Set(ByVal value As Integer)
            _nNombreAcces = value
        End Set
    End Property

    Private _nNombreSDA As Integer
    Public Property nNombreSDA() As Integer
        Get
            Return _nNombreSDA
        End Get
        Set(ByVal value As Integer)
            _nNombreSDA = value
        End Set
    End Property

    Private _nSpecialisationCanauxArrive As Integer
    Public Property nSpecialisationCanauxArrive() As Integer
        Get
            Return _nSpecialisationCanauxArrive
        End Get
        Set(ByVal value As Integer)
            _nSpecialisationCanauxArrive = value
        End Set
    End Property

    Private _nSpecialisationCanauxMixte As Integer
    Public Property nSpecialisationCanauxMixte() As Integer
        Get
            Return _nSpecialisationCanauxMixte
        End Get
        Set(ByVal value As Integer)
            _nSpecialisationCanauxMixte = value
        End Set
    End Property

    Private _nSpecialisationCanauxDepart As Integer
    Public Property nSpecialisationCanauxDepart() As Integer
        Get
            Return _nSpecialisationCanauxDepart
        End Get
        Set(ByVal value As Integer)
            _nSpecialisationCanauxDepart = value
        End Set
    End Property

    Private _bStSecretPermanent As Boolean
    Public Property bStSecretPermanent() As Boolean
        Get
            Return _bStSecretPermanent
        End Get
        Set(ByVal value As Boolean)
            _bStSecretPermanent = value
        End Set
    End Property

    Private _bStTransfertAppelNonReponse As Boolean
    Public Property bStTransfertAppelNonReponse() As Boolean
        Get
            Return _bStTransfertAppelNonReponse
        End Get
        Set(ByVal value As Boolean)
            _bStTransfertAppelNonReponse = value
        End Set
    End Property

    Private _bStTransfertAppel As Boolean
    Public Property bStTransfertAppel() As Boolean
        Get
            Return _bStTransfertAppel
        End Get
        Set(ByVal value As Boolean)
            _bStTransfertAppel = value
        End Set
    End Property

    Private _bStMessagerieVocale As Boolean
    Public Property bStMessagerieVocale() As Boolean
        Get
            Return _bStMessagerieVocale
        End Get
        Set(ByVal value As Boolean)
            _bStMessagerieVocale = value
        End Set
    End Property

    Private _sStNumeroRenvoi As String
    Public Property sStNumeroRenvoi() As String
        Get
            Return _sStNumeroRenvoi
        End Get
        Set(ByVal value As String)
            _sStNumeroRenvoi = value
        End Set
    End Property

    Private _bStRenvoiTerminal As Boolean
    Public Property bStRenvoiTerminal() As Boolean
        Get
            Return _bStRenvoiTerminal
        End Get
        Set(ByVal value As Boolean)
            _bStRenvoiTerminal = value
        End Set
    End Property

    Private _oTypeSelectionFixe As TypeSelectionFixe
    Public Property oTypeSelectionFixe() As TypeSelectionFixe
        Get
            Return _oTypeSelectionFixe
        End Get
        Set(ByVal value As TypeSelectionFixe)
            _oTypeSelectionFixe = value
        End Set
    End Property

    Private _oTypeMail As TypeMail
    Public Property oTypeMail() As TypeMail
        Get
            Return _oTypeMail
        End Get
        Set(ByVal value As TypeMail)
            _oTypeMail = value
        End Set
    End Property

    Private _oChoixListeFixe As ChoixListeFixe
    Public Property oChoixListeFixe() As ChoixListeFixe
        Get
            Return _oChoixListeFixe
        End Get
        Set(ByVal value As ChoixListeFixe)
            _oChoixListeFixe = value
        End Set
    End Property

    Private _sIaPrecisionsComplementairesParution As String
    Public Property sIaPrecisionsComplementairesParution() As String
        Get
            Return _sIaPrecisionsComplementairesParution
        End Get
        Set(ByVal value As String)
            _sIaPrecisionsComplementairesParution = value
        End Set
    End Property

    Private _sChoixModem As String
    Public Property sChoixModem() As String
        Get
            Return _sChoixModem
        End Get
        Set(ByVal value As String)
            _sChoixModem = value
        End Set
    End Property

    Private _sTypeAdresseEmail As String
    Public Property sTypeAdresseEmail() As String
        Get
            Return _sTypeAdresseEmail
        End Get
        Set(ByVal value As String)
            _sTypeAdresseEmail = value
        End Set
    End Property

    Private _sAdresseEmail As String
    Public Property sAdresseEmail() As String
        Get
            Return _sAdresseEmail
        End Get
        Set(ByVal value As String)
            _sAdresseEmail = value
        End Set
    End Property

    Private _sCommentaire As String
    Public Property sCommentaire() As String
        Get
            Return _sCommentaire
        End Get
        Set(ByVal value As String)
            _sCommentaire = value
        End Set
    End Property

    Private _sNomEtablissement As String
    Public Property sNomEtablissement() As String
        Get
            Return _sNomEtablissement
        End Get
        Set(ByVal value As String)
            _sNomEtablissement = value
        End Set
    End Property

    Private _sAdresse As String
    Public Property sAdresse() As String
        Get
            Return _sAdresse
        End Get
        Set(ByVal value As String)
            _sAdresse = value
        End Set
    End Property

    Private _dDateDispositionLocaux As Date
    Public Property dDateDispositionLocaux() As Date
        Get
            Return _dDateDispositionLocaux
        End Get
        Set(ByVal value As Date)
            _dDateDispositionLocaux = value
        End Set
    End Property

    Private _dDateMiseEnService As Date
    Public Property dDateMiseEnService() As Date
        Get
            Return _dDateMiseEnService
        End Get
        Set(ByVal value As Date)
            _dDateMiseEnService = value
        End Set
    End Property

    Private _sCodePostal As String
    Public Property sCodePostal() As String
        Get
            Return _sCodePostal
        End Get
        Set(ByVal value As String)
            _sCodePostal = value
        End Set
    End Property

    Private _sLocalite As String
    Public Property sLocalite() As String
        Get
            Return _sLocalite
        End Get
        Set(ByVal value As String)
            _sLocalite = value
        End Set
    End Property

    Private _sComplementAdresse As String
    Public Property sComplementAdresse() As String
        Get
            Return _sComplementAdresse
        End Get
        Set(ByVal value As String)
            _sComplementAdresse = value
        End Set
    End Property

    Private _sNomContact As String
    Public Property sNomContact() As String
        Get
            Return _sNomContact
        End Get
        Set(ByVal value As String)
            _sNomContact = value
        End Set
    End Property

    Private _sFixeContact As String
    Public Property sFixeContact() As String
        Get
            Return _sFixeContact
        End Get
        Set(ByVal value As String)
            _sFixeContact = value
        End Set
    End Property

    Private _sMobileContact As String
    Public Property sMobileContact() As String
        Get
            Return _sMobileContact
        End Get
        Set(ByVal value As String)
            _sMobileContact = value
        End Set
    End Property

    Private _sMailContact As String
    Public Property sMailContact() As String
        Get
            Return _sMailContact
        End Get
        Set(ByVal value As String)
            _sMailContact = value
        End Set
    End Property

    Private _bRestitutionEquipementLoues As Boolean
    Public Property bRestitutionEquipementLoues() As Boolean
        Get
            Return _bRestitutionEquipementLoues
        End Get
        Set(ByVal value As Boolean)
            _bRestitutionEquipementLoues = value
        End Set
    End Property

        Private _nIdClient As Integer
        Public Property nIdClient() As Integer
            Get
                Return _nIdClient
            End Get
            Set(ByVal value As Integer)
                _nIdClient = value
            End Set
    End Property

    Private _nIdTeteLigne As Integer
    Public Property nIdTeteLigne() As Integer
        Get
            Return _nIdTeteLigne
        End Get
        Set(ByVal value As Integer)
            _nIdTeteLigne = value
        End Set
    End Property

    Private _sReferenceClient As String
    Public Property sReferenceClient() As String
        Get
            Return _sReferenceClient
        End Get
        Set(ByVal value As String)
            _sReferenceClient = value
        End Set
    End Property

    Public Sub chargerLigneFixeParNumero(ByVal sNumero As String, ByRef oClient As Client)
        cDal.chargerLigneFixeParNumero(Me, sNumero, oClient)
    End Sub

    Public Sub activerLigneFixe()
        cDal.activerLigneFixe(Me)
    End Sub

    Public Sub chargerLigneFixeParId()
        cDal.chargerLigneFixeParId(Me)
    End Sub

    Public Sub insererLigneFixe()
        cDal.insererLigneFixe(Me)
    End Sub

    Public Sub editerLigneFixe()
        cDal.editerLigneFixe(Me)
    End Sub

    Public Sub supprimerLigneFixe()
        cDal.supprimerLigneFixe(Me)
    End Sub


End Class

