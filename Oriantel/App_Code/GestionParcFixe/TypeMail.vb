﻿Public Class TypeMail

    Private _nIdTypeMail As Integer
    Public Property nIdTypeMail() As Integer
        Get
            Return _nIdTypeMail
        End Get
        Set(ByVal value As Integer)
            _nIdTypeMail = value
        End Set
    End Property

    Private _sLibelleTypeMail As String
    Public Property sLibelleTypeMail() As String
        Get
            Return _sLibelleTypeMail
        End Get
        Set(ByVal value As String)
            _sLibelleTypeMail = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Public Sub chargerTypeMailParId()
        cDal.chargerTypeMailParId(Me)
    End Sub

End Class
