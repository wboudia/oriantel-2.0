﻿Public Class ContratFixe
    Private _nIdContrat As Integer
    Public Property nIdContrat() As Integer
        Get
            Return _nIdContrat
        End Get
        Set(ByVal value As Integer)
            _nIdContrat = value
        End Set
    End Property

    Private _oTypeLigneFixe As TypeLigneFixe
    Public Property oTypeLigneFixe() As TypeLigneFixe
        Get
            Return _oTypeLigneFixe
        End Get
        Set(ByVal value As TypeLigneFixe)
            _oTypeLigneFixe = value
        End Set
    End Property

    Private _sLibelleContrat As String
    Public Property sLibelleContrat() As String
        Get
            Return _sLibelleContrat
        End Get
        Set(ByVal value As String)
            _sLibelleContrat = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Public Sub chargerContratFixeParId()
        cDal.chargerContratFixeParId(Me)
    End Sub

    Public Sub chargerContratFixeInternet(ByVal nIdClient As Integer)
        cDal.chargerContratFixeParId(Me, nIdClient)
    End Sub

End Class
