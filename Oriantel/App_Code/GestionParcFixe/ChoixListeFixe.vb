﻿Public Class ChoixListeFixe
    Private _nIdChoixListe As Integer
    Public Property nIdChoixListe() As Integer
        Get
            Return _nIdChoixListe
        End Get
        Set(ByVal value As Integer)
            _nIdChoixListe = value
        End Set
    End Property

    Private _sLibelleChoixListe As String
    Public Property sLibelleChoixListe() As String
        Get
            Return _sLibelleChoixListe
        End Get
        Set(ByVal value As String)
            _sLibelleChoixListe = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Public Sub chargerChoixListeFixeParId()
        cDal.chargerChoixListeFixeParId(Me)
    End Sub

End Class
