﻿Public Class ParametrageControleFixe

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Private _sTypeControle As String
    Public Property sTypeControle() As String
        Get
            Return _sTypeControle
        End Get
        Set(ByVal value As String)
            _sTypeControle = value
        End Set
    End Property

    Private _sIdControle As String
    Public Property sIdControle() As String
        Get
            Return _sIdControle
        End Get
        Set(ByVal value As String)
            _sIdControle = value
        End Set
    End Property

    Private _sPlaceholder As String
    Public Property sPlaceholder() As String
        Get
            Return _sPlaceholder
        End Get
        Set(ByVal value As String)
            _sPlaceholder = value
        End Set
    End Property

    Private _sTextLabel As String
    Public Property sTextLabel() As String
        Get
            Return _sTextLabel
        End Get
        Set(ByVal value As String)
            _sTextLabel = value
        End Set
    End Property

    Private _sTopControle As String
    Public Property sTopControle() As String
        Get
            Return _sTopControle
        End Get
        Set(ByVal value As String)
            _sTopControle = value
        End Set
    End Property

    Private _sLeftLabel As String
    Public Property sLeftLabel() As String
        Get
            Return _sLeftLabel
        End Get
        Set(ByVal value As String)
            _sLeftLabel = value
        End Set
    End Property

    Private _sLeftControle As String
    Public Property sLeftControle() As String
        Get
            Return _sLeftControle
        End Get
        Set(ByVal value As String)
            _sLeftControle = value
        End Set
    End Property

    Private _sWidthLabel As String
    Public Property sWidthLabel() As String
        Get
            Return _sWidthLabel
        End Get
        Set(ByVal value As String)
            _sWidthLabel = value
        End Set
    End Property

    Private _sWidthControle As String
    Public Property sWidthControle() As String
        Get
            Return _sWidthControle
        End Get
        Set(ByVal value As String)
            _sWidthControle = value
        End Set
    End Property

    Private _sHeightLabel As String
    Public Property sHeightLabel() As String
        Get
            Return _sHeightLabel
        End Get
        Set(ByVal value As String)
            _sHeightLabel = value
        End Set
    End Property

    Private _sHeightControle As String
    Public Property sHeightControle() As String
        Get
            Return _sHeightControle
        End Get
        Set(ByVal value As String)
            _sHeightControle = value
        End Set
    End Property

    Private _sCas As String
    Public Property sCas() As String
        Get
            Return _sCas
        End Get
        Set(ByVal value As String)
            _sCas = value
        End Set
    End Property

    Private _sTypeLigne As String
    Public Property sTypeLigne() As String
        Get
            Return _sTypeLigne
        End Get
        Set(ByVal value As String)
            _sTypeLigne = value
        End Set
    End Property

    Private _bVerrouille As Boolean
    Public Property bVerrouille() As Boolean
        Get
            Return _bVerrouille
        End Get
        Set(ByVal value As Boolean)
            _bVerrouille = value
        End Set
    End Property

    Private _bAutoPostBack As Boolean
    Public Property bAutoPostBack() As Boolean
        Get
            Return _bAutoPostBack
        End Get
        Set(ByVal value As Boolean)
            _bAutoPostBack = value
        End Set
    End Property

    Private _sNomMembre As String
    Public Property sNomMembre() As String
        Get
            Return _sNomMembre
        End Get
        Set(ByVal value As String)
            _sNomMembre = value
        End Set
    End Property

    Private _bAutorisationNull As Boolean
    Public Property bAutorisationNull() As Boolean
        Get
            Return _bAutorisationNull
        End Get
        Set(ByVal value As Boolean)
            _bAutorisationNull = value
        End Set
    End Property

    Private _sTypeAttendu As String
    Public Property sTypeAttendu() As String
        Get
            Return _sTypeAttendu
        End Get
        Set(ByVal value As String)
            _sTypeAttendu = value
        End Set
    End Property

End Class
