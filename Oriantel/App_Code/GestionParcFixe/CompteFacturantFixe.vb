﻿Public Class CompteFacturantFixe

    Private _nIdCompteFacturant As Integer
    Public Property nIdCompteFacturant() As Integer
        Get
            Return _nIdCompteFacturant
        End Get
        Set(ByVal value As Integer)
            _nIdCompteFacturant = value
        End Set
    End Property

    Private _sLibelleCompteFacturant As String
    Public Property sLibelleCompteFacturant() As String
        Get
            Return _sLibelleCompteFacturant
        End Get
        Set(ByVal value As String)
            _sLibelleCompteFacturant = value
        End Set
    End Property

    Private _sLibelleCentreFrais As String
    Public Property sLibelleCentreFrais() As String
        Get
            Return _sLibelleCentreFrais
        End Get
        Set(ByVal value As String)
            _sLibelleCentreFrais = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Public Sub chargerCompteFacturantFixeParId(ByVal nIdCompteFacturantFixe As Integer)
        cDal.chargerCompteFacturantFixeParId(Me, nIdCompteFacturantFixe)
    End Sub

End Class
