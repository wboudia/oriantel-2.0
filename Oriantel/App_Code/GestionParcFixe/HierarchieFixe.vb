﻿Public Class HierarchieFixe

    Private _nIdHierarchie As Integer
    Public Property nIdHierarchie() As Integer
        Get
            Return _nIdHierarchie
        End Get
        Set(ByVal value As Integer)
            _nIdHierarchie = value
        End Set
    End Property

    Private _sHierarchie1 As String
    Public Property sHierarchie1() As String
        Get
            Return _sHierarchie1
        End Get
        Set(ByVal value As String)
            _sHierarchie1 = value
        End Set
    End Property

    Private _sHierarchie2 As String
    Public Property sHierarchie2() As String
        Get
            Return _sHierarchie2
        End Get
        Set(ByVal value As String)
            _sHierarchie2 = value
        End Set
    End Property

    Private _sHierarchie3 As String
    Public Property sHierarchie3() As String
        Get
            Return _sHierarchie3
        End Get
        Set(ByVal value As String)
            _sHierarchie3 = value
        End Set
    End Property

    Private _sHierarchie4 As String
    Public Property sHierarchie4() As String
        Get
            Return _sHierarchie4
        End Get
        Set(ByVal value As String)
            _sHierarchie4 = value
        End Set
    End Property

    Private _sHierarchie5 As String
    Public Property sHierarchie5() As String
        Get
            Return _sHierarchie5
        End Get
        Set(ByVal value As String)
            _sHierarchie5 = value
        End Set
    End Property

    Private _sHierarchie6 As String
    Public Property sHierarchie6() As String
        Get
            Return _sHierarchie6
        End Get
        Set(ByVal value As String)
            _sHierarchie6 = value
        End Set
    End Property

    Private _sHierarchie7 As String
    Public Property sHierarchie7() As String
        Get
            Return _sHierarchie7
        End Get
        Set(ByVal value As String)
            _sHierarchie7 = value
        End Set
    End Property

    Private _sHierarchie8 As String
    Public Property sHierarchie8() As String
        Get
            Return _sHierarchie8
        End Get
        Set(ByVal value As String)
            _sHierarchie8 = value
        End Set
    End Property

    Private _sHierarchie9 As String
    Public Property sHierarchie9() As String
        Get
            Return _sHierarchie9
        End Get
        Set(ByVal value As String)
            _sHierarchie9 = value
        End Set
    End Property


    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Public Sub chargerHierarchieFixeParId(ByVal nIdHierarchieFixe As Integer)
        cDal.chargerHierarchieFixeParId(Me, nIdHierarchieFixe)
    End Sub

End Class
