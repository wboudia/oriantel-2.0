﻿Public Class UsageFixe
    Private _nIdUsage As Integer
    Public Property nIdUsage() As Integer
        Get
            Return _nIdUsage
        End Get
        Set(ByVal value As Integer)
            _nIdUsage = value
        End Set
    End Property

    Private _sLibelleUsage As String
    Public Property sLibelleUsage() As String
        Get
            Return _sLibelleUsage
        End Get
        Set(ByVal value As String)
            _sLibelleUsage = value
        End Set
    End Property

    Private _sTypeUsage As String
    Public Property sTypeUsage() As String
        Get
            Return _sTypeUsage
        End Get
        Set(ByVal value As String)
            _sTypeUsage = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Public Sub chargerUsageFixeParId(ByVal nIdUsageFixe As Integer, ByVal idClient As Integer)
        cDal.chargerUsageFixeParId(Me, nIdUsageFixe, idClient)
    End Sub

    Public Sub chargerUsageFixeInternet(ByVal idClient As Integer)
        cDal.chargerUsageFixeInternet(Me, idClient)
    End Sub

End Class
