﻿Public Class TypeSelectionFixe

    Private _nIdTypeSelection As Integer
    Public Property nIdTypeSelection() As Integer
        Get
            Return _nIdTypeSelection
        End Get
        Set(ByVal value As Integer)
            _nIdTypeSelection = value
        End Set
    End Property

    Private _sLibelleTypeSelection As String
    Public Property sLibelleTypeSelection() As String
        Get
            Return _sLibelleTypeSelection
        End Get
        Set(ByVal value As String)
            _sLibelleTypeSelection = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Public Sub chargerTypeSelectionFixeParId()
        cDal.chargerTypeSelectionFixeParId(Me)
    End Sub

End Class
