﻿Public Class ContactInstallationFixe

    Private _nIdContact As Integer
    Public Property nIdContact() As Integer
        Get
            Return _nIdContact
        End Get
        Set(ByVal value As Integer)
            _nIdContact = value
        End Set
    End Property

    Private _sNomContact As String
    Public Property sNomContact() As String
        Get
            Return _sNomContact
        End Get
        Set(ByVal value As String)
            _sNomContact = value
        End Set
    End Property

    Private _sMobileContact As String
    Public Property sMobileContact() As String
        Get
            Return _sMobileContact
        End Get
        Set(ByVal value As String)
            _sMobileContact = value
        End Set
    End Property

    Private _sFixeContact As String
    Public Property sFixeContact() As String
        Get
            Return _sFixeContact
        End Get
        Set(ByVal value As String)
            _sFixeContact = value
        End Set
    End Property

    Private _sEmailContact As String
    Public Property sEmailContact() As String
        Get
            Return _sEmailContact
        End Get
        Set(ByVal value As String)
            _sEmailContact = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Public Sub chargerContactInstallationFixe()
        cDal.chargerContactInstallationFixe(Me)
    End Sub

End Class
