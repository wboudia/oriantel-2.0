﻿Public Class AdresseInstallationFixe

    Private _sCodeSite As String
    Public Property sCodeSite() As String
        Get
            Return _sCodeSite
        End Get
        Set(ByVal value As String)
            _sCodeSite = value
        End Set
    End Property

    Private _sNomSite As String
    Public Property sNomSite() As String
        Get
            Return _sNomSite
        End Get
        Set(ByVal value As String)
            _sNomSite = value
        End Set
    End Property

    Private _sAdresseSite As String
    Public Property sAdresseSite() As String
        Get
            Return _sAdresseSite
        End Get
        Set(ByVal value As String)
            _sAdresseSite = value
        End Set
    End Property

    Private _sCodePostal As String
    Public Property sCodePostal() As String
        Get
            Return _sCodePostal
        End Get
        Set(ByVal value As String)
            _sCodePostal = value
        End Set
    End Property

    Private _sLocalite As String
    Public Property sLocalite() As String
        Get
            Return _sLocalite
        End Get
        Set(ByVal value As String)
            _sLocalite = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Public Sub chargerAdresseInstallationFixeParCodeSite()
        cDal.chargerAdresseInstallationFixeParCodeSite(Me)
    End Sub
End Class
