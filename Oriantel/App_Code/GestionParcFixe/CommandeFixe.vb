﻿Public Class CommandeFixe

    Private _nIdCommande As Integer
    Public Property nIdCommande() As Integer
        Get
            Return _nIdCommande
        End Get
        Set(ByVal value As Integer)
            _nIdCommande = value
        End Set
    End Property

    Private _sNumeroCommande As String
    Public Property sNumeroCommande() As String
        Get
            Return _sNumeroCommande
        End Get
        Set(ByVal value As String)
            _sNumeroCommande = value
        End Set
    End Property

    Private _sTypeCommande As String
    Public Property sTypeCommande() As String
        Get
            Return _sTypeCommande
        End Get
        Set(ByVal value As String)
            _sTypeCommande = value
        End Set
    End Property

    Private _bEstTelecharge As Boolean
    Public Property bEstTelecharge() As Boolean
        Get
            Return _bEstTelecharge
        End Get
        Set(ByVal value As Boolean)
            _bEstTelecharge = value
        End Set
    End Property

    Private _bEstRealiseFacture As Boolean
    Public Property bEstRealiseFacture() As Boolean
        Get
            Return _bEstRealiseFacture
        End Get
        Set(ByVal value As Boolean)
            _bEstRealiseFacture = value
        End Set
    End Property

    Private _oLigneFixe As LigneFixe
    Public Property oLigneFixe() As LigneFixe
        Get
            Return _oLigneFixe
        End Get
        Set(ByVal value As LigneFixe)
            _oLigneFixe = value
        End Set
    End Property

    Private _bResiliationPartielle As Boolean
    Public Property bResiliationPartielle() As Boolean
        Get
            Return _bResiliationPartielle
        End Get
        Set(ByVal value As Boolean)
            _bResiliationPartielle = value
        End Set
    End Property


    Private _nIdUtilisateurPrincipal As Integer
    Public Property nIdUtilisateurPrincipal() As Integer
        Get
            Return _nIdUtilisateurPrincipal
        End Get
        Set(ByVal value As Integer)
            _nIdUtilisateurPrincipal = value
        End Set
    End Property

    Private _nIdUtilisateurSecondaire As Integer
    Public Property nIdUtilisateurSecondaire() As Integer
        Get
            Return _nIdUtilisateurSecondaire
        End Get
        Set(ByVal value As Integer)
            _nIdUtilisateurSecondaire = value
        End Set
    End Property

    Private _dDateCommande As Date
    Public Property dDateCommande() As Date
        Get
            Return _dDateCommande
        End Get
        Set(ByVal value As Date)
            _dDateCommande = value
        End Set
    End Property

    Public Sub ChargerCommandeParIdLigne()
        cDal.ChargerCommandeParIdLigne(Me)
    End Sub

    Public Sub ajouterCommandeFixe()
        cDal.ajouterCommandeFixe(Me)
    End Sub

    Public Sub PasserEnTelechargeCommandeFixe()
        cDal.PasserEnTelechargeCommandeFixe(Me)
    End Sub

End Class
