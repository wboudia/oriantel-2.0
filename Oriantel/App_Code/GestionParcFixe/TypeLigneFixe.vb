﻿Public Class TypeLigneFixe
    Private _nIdTypeLigne As Integer
    Public Property nIdTypeLigne() As Integer
        Get
            Return _nIdTypeLigne
        End Get
        Set(ByVal value As Integer)
            _nIdTypeLigne = value
        End Set
    End Property

    Private _sLibelleTypeLigne As String
    Public Property sLibelleTypeLigne() As String
        Get
            Return _sLibelleTypeLigne
        End Get
        Set(ByVal value As String)
            _sLibelleTypeLigne = value
        End Set
    End Property


    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Public Sub chargerTypeLigneFixeParId(ByVal nIdTypeLigneFixe As Integer)
        cDal.chargerTypeLigneFixeParId(Me, nIdTypeLigneFixe)
    End Sub
    Public Sub chargerTypeLigneFixeInternet(ByVal idClient As Integer)
        cDal.chargerTypeLigneFixeInternet(Me, idClient)
    End Sub

End Class
