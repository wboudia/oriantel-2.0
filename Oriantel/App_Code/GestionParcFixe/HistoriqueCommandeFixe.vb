﻿Public Class HistoriqueCommandeFixe

    Private _nIdHistoriqueCommandeFixe As Integer
    Public Property nIdHistoriqueCommandeFixe() As Integer
        Get
            Return _nIdHistoriqueCommandeFixe
        End Get
        Set(ByVal value As Integer)
            _nIdHistoriqueCommandeFixe = value
        End Set
    End Property

    Private _oCommandeFixe As CommandeFixe
    Public Property oCommandeFixe() As CommandeFixe
        Get
            Return _oCommandeFixe
        End Get
        Set(ByVal value As CommandeFixe)
            _oCommandeFixe = value
        End Set
    End Property

    Public Sub InsertionHistoriqueCommandeFixe()
        cDal.InsertionHistoriqueCommandeFixe(Me)
    End Sub

End Class
