﻿Public Class GridViewCustom
        Inherits System.Web.UI.WebControls.GridView

#Region "Properties"

    <ComponentModel.DefaultValue(False)> _
        Public Property EnableRowClick() As Boolean
        Get
            Dim ret As Boolean = False
            Dim obj As Object = ViewState("EnableRowClick")
            If obj IsNot Nothing Then
                ret = CBool(obj)
            End If
            Return ret
        End Get
        Set(ByVal value As Boolean)
            ViewState("EnableRowClick") = value
        End Set
    End Property
#End Region

#Region "Event"


        Private Shared ReadOnly RowClickedEventKey As Object = New Object

    Public Custom Event RowClicked As EventHandler(Of GridViewRowClickedEventArgs)
        AddHandler(ByVal value As EventHandler(Of GridViewRowClickedEventArgs))
            Events.AddHandler(RowClickedEventKey, value)
        End AddHandler

        RemoveHandler(ByVal value As EventHandler(Of GridViewRowClickedEventArgs))
            Events.RemoveHandler(RowClickedEventKey, value)
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As GridViewRowClickedEventArgs)
            Dim ev As EventHandler(Of GridViewRowClickedEventArgs) = TryCast(Events(RowClickedEventKey), EventHandler(Of GridViewRowClickedEventArgs))
            If ev IsNot Nothing Then
                ev(sender, e)
            End If
        End RaiseEvent
    End Event

        Protected Overridable Sub OnRowClicked(ByVal e As GridViewRowClickedEventArgs)
            RaiseEvent RowClicked(Me, e)
        End Sub

#End Region

#Region "Postback handling"
        Protected Overrides Sub RaisePostBackEvent(ByVal eventArgument As String)
            If eventArgument.StartsWith("rc") Then
                Dim index As Integer = Int32.Parse(eventArgument.Substring(2))
                Dim args As New GridViewRowClickedEventArgs(Me.Rows(index))
                OnRowClicked(args)
            Else
                MyBase.RaisePostBackEvent(eventArgument)
            End If

        End Sub
#End Region

#Region "Adding the wiring from client-side to server-side, causing the posback when row is clicked"


        Protected Overrides Sub PrepareControlHierarchy()
            MyBase.PrepareControlHierarchy()

            If EnableRowClick Then
                Dim i As Integer
                For i = 0 To Rows.Count - 1
                    Dim argsData As String = "rc" & Rows(i).RowIndex.ToString()
                    Me.Rows(i).Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(Me, argsData))
                Next
            End If

        End Sub

#End Region

    End Class

#Region "Custom event argument type"
    Public Class GridViewRowClickedEventArgs
        Inherits EventArgs

        Private _row As GridViewRow
        Public Sub New(ByVal row As GridViewRow)
            _row = row
        End Sub
        Public ReadOnly Property Row() As GridViewRow
            Get
                Return _row
            End Get
        End Property
    End Class
#End Region
