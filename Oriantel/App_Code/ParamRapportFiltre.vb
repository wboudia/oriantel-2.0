﻿Public Class ParamRapportFiltre

    Private _nIdFiltre As Integer
    Public Property nIdFiltre() As Integer
        Get
            Return _nIdFiltre
        End Get
        Set(ByVal value As Integer)
            _nIdFiltre = value
        End Set
    End Property

    Private _nIdModule As UInt16
    Public Property nIdModule() As UInt16
        Get
            Return _nIdModule
        End Get
        Set(ByVal value As UInt16)
            _nIdModule = value
        End Set
    End Property

    Private _nIdRapport As Integer
    Public Property nIdRapport() As Integer
        Get
            Return _nIdRapport
        End Get
        Set(ByVal value As Integer)
            _nIdRapport = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Private _nIdUtilisateur As Integer
    Public Property nIdUtilisateur() As Integer
        Get
            Return _nIdUtilisateur
        End Get
        Set(ByVal value As Integer)
            _nIdUtilisateur = value
        End Set
    End Property

    Private _nOrdreFiltre As Integer
    Public Property nOrdreFiltre() As Integer
        Get
            Return _nOrdreFiltre
        End Get
        Set(ByVal value As Integer)
            _nOrdreFiltre = value
        End Set
    End Property


End Class
