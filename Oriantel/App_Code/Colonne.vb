﻿Public Class Colonne

    Private _nIdColonne As Integer
    Public Property nIdColonne() As Integer
        Get
            Return _nIdColonne
        End Get
        Set(ByVal value As Integer)
            _nIdColonne = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Private _sNomColonne As String
    Public Property sNomColonne() As String
        Get
            Return _sNomColonne
        End Get
        Set(ByVal value As String)
            _sNomColonne = value
        End Set
    End Property

    Private _sEnteteNiveau1 As String
    Public Property sEnteteNiveau1() As String
        Get
            Return _sEnteteNiveau1
        End Get
        Set(ByVal value As String)
            _sEnteteNiveau1 = value
        End Set
    End Property

    Private _sInfobulleNiveau1 As String
    Public Property sInfobulleNiveau1() As String
        Get
            Return _sInfobulleNiveau1
        End Get
        Set(ByVal value As String)
            _sInfobulleNiveau1 = value
        End Set
    End Property


    Private _sEnteteNiveau2 As String
    Public Property sEnteteNiveau2() As String
        Get
            Return _sEnteteNiveau2
        End Get
        Set(ByVal value As String)
            _sEnteteNiveau2 = value
        End Set
    End Property

    Private _sInfobulleNiveau2 As String
    Public Property sInfobulleNiveau2() As String
        Get
            Return _sInfobulleNiveau2
        End Get
        Set(ByVal value As String)
            _sInfobulleNiveau2 = value
        End Set
    End Property

    Private _sEnteteNiveau3 As String
    Public Property sEnteteNiveau3() As String
        Get
            Return _sEnteteNiveau3
        End Get
        Set(ByVal value As String)
            _sEnteteNiveau3 = value
        End Set
    End Property

    Private _sInfobulleNiveau3 As String
    Public Property sInfobulleNiveau3() As String
        Get
            Return _sInfobulleNiveau3
        End Get
        Set(ByVal value As String)
            _sInfobulleNiveau3 = value
        End Set
    End Property

    Private _sEnteteNiveau4 As String
    Public Property sEnteteNiveau4() As String
        Get
            Return _sEnteteNiveau4
        End Get
        Set(ByVal value As String)
            _sEnteteNiveau4 = value
        End Set
    End Property

    Private _sInfobulleNiveau4 As String
    Public Property sInfobulleNiveau4() As String
        Get
            Return _sInfobulleNiveau4
        End Get
        Set(ByVal value As String)
            _sInfobulleNiveau4 = value
        End Set
    End Property

    Private _sTypeDonnees As String
    Public Property sTypeDonnees() As String
        Get
            Return _sTypeDonnees
        End Get
        Set(ByVal value As String)
            _sTypeDonnees = value
        End Set
    End Property

    Private _sTypeRegroupement As String
    Public Property sTypeRegroupement() As String
        Get
            Return _sTypeRegroupement
        End Get
        Set(ByVal value As String)
            _sTypeRegroupement = value
        End Set
    End Property

    Private _sMefDonnees As String
    Public Property sMefDonnees() As String
        Get
            Return _sMefDonnees
        End Get
        Set(ByVal value As String)
            _sMefDonnees = value
        End Set
    End Property

    Private _bEstVisible As Boolean
    Public Property bEstVisible() As Boolean
        Get
            Return _bEstVisible
        End Get
        Set(ByVal value As Boolean)
            _bEstVisible = value
        End Set
    End Property

    Private _bEstMasquable As Boolean
    Public Property bEstMasquable() As Boolean
        Get
            Return _bEstMasquable
        End Get
        Set(ByVal value As Boolean)
            _bEstMasquable = value
        End Set
    End Property

    Private _bEstTriAutorise As Boolean
    Public Property bEstTriAutorise() As Boolean
        Get
            Return _bEstTriAutorise
        End Get
        Set(ByVal value As Boolean)
            _bEstTriAutorise = value
        End Set
    End Property

    Private _bEstExtractExcel As Boolean
    Public Property bEstExtractExcel() As Boolean
        Get
            Return _bEstExtractExcel
        End Get
        Set(ByVal value As Boolean)
            _bEstExtractExcel = value
        End Set
    End Property

    Private _sclasseCellule As String
    Public Property sclasseCellule() As String
        Get
            Return _sclasseCellule
        End Get
        Set(ByVal value As String)
            _sclasseCellule = value
        End Set
    End Property

    Private _sSiNull As String
    Public Property sSiNull() As String
        Get
            Return _sSiNull
        End Get
        Set(ByVal value As String)
            _sSiNull = value
        End Set
    End Property

    Private _sSiZero As String
    Public Property sSiZero() As String
        Get
            Return _sSiZero
        End Get
        Set(ByVal value As String)
            _sSiZero = value
        End Set
    End Property

    Private _bEstConvertibleTTC As Boolean
    Public Property bEstConvertibleTTC() As Boolean
        Get
            Return _bEstConvertibleTTC
        End Get
        Set(ByVal value As Boolean)
            _bEstConvertibleTTC = value
        End Set
    End Property

    Private _bEstFusionnable As Boolean
    Public Property bEstFusionnable() As Boolean
        Get
            Return _bEstFusionnable
        End Get
        Set(ByVal value As Boolean)
            _bEstFusionnable = value
        End Set
    End Property

    Private _bEstDigitsMasquables As Boolean
    Public Property bEstDigitsMasquables() As Boolean
        Get
            Return _bEstDigitsMasquables
        End Get
        Set(ByVal value As Boolean)
            _bEstDigitsMasquables = value
        End Set
    End Property

    Private _bEstSeuil As Boolean
    Public Property bEstSeuil() As Boolean
        Get
            Return _bEstSeuil
        End Get
        Set(ByVal value As Boolean)
            _bEstSeuil = value
        End Set
    End Property

    Private _nNombreDigitsMasques As Short
    Public Property nNombreDigitsMasques() As Short
        Get
            Return _nNombreDigitsMasques
        End Get
        Set(ByVal value As Short)
            _nNombreDigitsMasques = value
        End Set
    End Property

    Private _nLargeurColonne As Integer
    Public Property nLargeurColonne() As Integer
        Get
            Return _nLargeurColonne
        End Get
        Set(ByVal value As Integer)
            _nLargeurColonne = value
        End Set
    End Property

    Private _sStyleDonnees As String
    Public Property sStyleDonnees() As String
        Get
            Return _sStyleDonnees
        End Get
        Set(ByVal value As String)
            _sStyleDonnees = value
        End Set
    End Property

    Private _sStyleSeuil As String
    Public Property sStyleSeuil() As String
        Get
            Return _sStyleSeuil
        End Get
        Set(ByVal value As String)
            _sStyleSeuil = value
        End Set
    End Property

    Private _sValeurSeuil As String
    Public Property sValeurSeuil() As String
        Get
            Return _sValeurSeuil
        End Get
        Set(ByVal value As String)
            _sValeurSeuil = value
        End Set
    End Property

    Private _bEstModifiable As Boolean
    Public Property bEstModifiable() As Boolean
        Get
            Return _bEstModifiable
        End Get
        Set(ByVal value As Boolean)
            _bEstModifiable = value
        End Set
    End Property

    Private _sControleModification As String
    Public Property sControleModification() As String
        Get
            Return _sControleModification
        End Get
        Set(ByVal value As String)
            _sControleModification = value
        End Set
    End Property

    Private _sTableModification As String
    Public Property sTableModification() As String
        Get
            Return _sTableModification
        End Get
        Set(ByVal value As String)
            _sTableModification = value
        End Set
    End Property

    Private _sChampModification As String
    Public Property sChampModification() As String
        Get
            Return _sChampModification
        End Get
        Set(ByVal value As String)
            _sChampModification = value
        End Set
    End Property

    Private _sFiltreModification As String
    Public Property sFiltreModification() As String
        Get
            Return _sFiltreModification
        End Get
        Set(ByVal value As String)
            _sFiltreModification = value
        End Set
    End Property

    Private _sNomValeurMois As String
    Public Property sNomValeurMois() As String
        Get
            Return _sNomValeurMois
        End Get
        Set(ByVal value As String)
            _sNomValeurMois = value
        End Set
    End Property

    Private _bEstComposeeBalises As Boolean
    Public Property bEstComposeeBalises() As Boolean
        Get
            Return _bEstComposeeBalises
        End Get
        Set(ByVal value As Boolean)
            _bEstComposeeBalises = value
        End Set
    End Property


    Private _oRapports As Rapports
    Public Property oRapports() As Rapports
        Get
            Return _oRapports
        End Get
        Set(ByVal value As Rapports)
            _oRapports = value
        End Set
    End Property

    Private _sDetailListeMois As String
    Public Property sDetailListeMois() As String
        Get
            Return _sDetailListeMois
        End Get
        Set(ByVal value As String)
            _sDetailListeMois = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal idColonne As Integer)
        nIdColonne = idColonne
    End Sub

    Public Sub New(ByVal idColonne As Integer, ByVal o_Client As Client, ByVal nomColonne As String, ByVal enteteNiveau1 As String, ByVal infobulleNiveau1 As String, ByVal enteteNiveau2 As String, ByVal infobulleNiveau2 As String, ByVal enteteNiveau3 As String, ByVal infobulleNiveau3 As String, ByVal enteteNiveau4 As String, ByVal infobulleNiveau4 As String, ByVal typeDonnees As String, ByVal typeRegroupement As String, ByVal mefDonnees As String, ByVal estVisible As Boolean, ByVal estMasquable As Boolean, ByVal estTriAutorise As Boolean, ByVal classeCellule As String, ByVal siNull As String, ByVal siZero As String, ByVal estConvertibleTTC As Boolean, ByVal estFusionnable As Boolean, ByVal estDigitsMasquables As Boolean, ByVal nombreDigitsMasques As Short, ByVal estSeuil As Boolean, ByVal largeurColonne As Integer, ByVal styleDonnees As String, ByVal styleSeuil As String, ByVal valeurSeuil As String, ByVal estModifiable As Boolean, ByVal controleModification As String, ByVal tableModification As String, ByVal champModification As String, ByVal filtreModification As String, ByVal nomValeurMois As String, ByVal detailListeMois As String, ByVal estExtractExcel As Boolean, ByVal estComposeeBalises As Boolean)
        nIdColonne = idColonne
        oClient = o_Client
        sNomColonne = nomColonne
        sEnteteNiveau1 = enteteNiveau1
        sInfobulleNiveau1 = infobulleNiveau1
        sEnteteNiveau2 = enteteNiveau2
        sInfobulleNiveau2 = infobulleNiveau2
        sEnteteNiveau3 = enteteNiveau3
        sInfobulleNiveau3 = infobulleNiveau3
        sEnteteNiveau4 = enteteNiveau4
        sInfobulleNiveau4 = infobulleNiveau4
        sTypeDonnees = typeDonnees
        sTypeRegroupement = typeRegroupement
        sMefDonnees = mefDonnees
        bEstVisible = estVisible
        bEstMasquable = estMasquable
        bEstTriAutorise = estTriAutorise
        sclasseCellule = classeCellule
        sSiNull = siNull
        sSiZero = siZero
        bEstConvertibleTTC = estConvertibleTTC
        bEstFusionnable = estFusionnable
        bEstDigitsMasquables = estDigitsMasquables
        nNombreDigitsMasques = nombreDigitsMasques
        bEstSeuil = estSeuil
        nLargeurColonne = largeurColonne
        sStyleDonnees = styleDonnees
        sStyleSeuil = styleSeuil
        sValeurSeuil = valeurSeuil
        bEstModifiable = estModifiable
        sControleModification = controleModification
        sTableModification = tableModification
        sChampModification = champModification
        sFiltreModification = filtreModification
        sNomValeurMois = nomValeurMois
        sDetailListeMois = detailListeMois
        bEstExtractExcel = estExtractExcel
        bEstComposeeBalises = estComposeeBalises
    End Sub


    Public Sub New(ByVal idColonne As Integer, ByVal o_Client As Client, ByVal nomColonne As String, ByVal enteteNiveau1 As String, ByVal infobulleNiveau1 As String, ByVal enteteNiveau2 As String, ByVal infobulleNiveau2 As String, ByVal enteteNiveau3 As String, ByVal infobulleNiveau3 As String, ByVal enteteNiveau4 As String, ByVal infobulleNiveau4 As String, ByVal typeDonnees As String, ByVal typeRegroupement As String, ByVal mefDonnees As String, ByVal estVisible As Boolean, ByVal estMasquable As Boolean, ByVal estTriAutorise As Boolean, ByVal classeCellule As String, ByVal siNull As String, ByVal siZero As String, ByVal estConvertibleTTC As Boolean, ByVal estFusionnable As Boolean, ByVal estDigitsMasquables As Boolean, ByVal nombreDigitsMasques As Short, ByVal estSeuil As Boolean, ByVal largeurColonne As Integer, ByVal styleDonnees As String, ByVal styleSeuil As String, ByVal valeurSeuil As String, ByVal estModifiable As Boolean, ByVal controleModification As String, ByVal tableModification As String, ByVal champModification As String, ByVal FiltreModification As String, ByVal listerapports As Rapports, ByVal nomValeurMois As String, ByVal detailListeMois As String, ByVal estExtractExcel As Boolean, ByVal estComposeeBalises As Boolean)
        nIdColonne = idColonne
        oClient = o_Client
        sNomColonne = nomColonne
        sEnteteNiveau1 = enteteNiveau1
        sInfobulleNiveau1 = infobulleNiveau1
        sEnteteNiveau2 = enteteNiveau2
        sInfobulleNiveau2 = infobulleNiveau2
        sEnteteNiveau3 = enteteNiveau3
        sInfobulleNiveau3 = infobulleNiveau3
        sEnteteNiveau4 = enteteNiveau4
        sInfobulleNiveau4 = infobulleNiveau4
        sTypeRegroupement = typeRegroupement
        sTypeDonnees = typeDonnees
        sMefDonnees = mefDonnees
        bEstVisible = estVisible
        bEstMasquable = estMasquable
        bEstTriAutorise = estTriAutorise
        sclasseCellule = classeCellule
        sSiNull = siNull
        sSiZero = siZero
        bEstConvertibleTTC = estConvertibleTTC
        bEstFusionnable = estFusionnable
        bEstDigitsMasquables = estDigitsMasquables
        nNombreDigitsMasques = nombreDigitsMasques
        bEstSeuil = estSeuil
        nLargeurColonne = largeurColonne
        sStyleDonnees = styleDonnees
        sStyleSeuil = styleSeuil
        sValeurSeuil = valeurSeuil
        bEstModifiable = estModifiable
        sControleModification = controleModification
        sTableModification = tableModification
        sChampModification = champModification
        sFiltreModification = FiltreModification
        oRapports = listerapports
        sNomValeurMois = nomValeurMois
        sDetailListeMois = detailListeMois
        bEstExtractExcel = estExtractExcel
        bEstComposeeBalises = estComposeeBalises
    End Sub

    Public Function RecupererFormatListeMois(ByRef oSerie As Serie) As Colonne
        Return cDal.RecupererFormatListeMois(oSerie)
    End Function

    Public Sub RecupererEnteteNiv1(ByVal nIdClient As Integer)
        cDal.RecupererEntete(Me, nIdClient)
    End Sub

End Class