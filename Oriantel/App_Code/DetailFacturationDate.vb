﻿Public Class DetailFacturationDate : Inherits List(Of DetailFacturationDate)

    Private _sLibelleDateType As String
    Public Property sLibelleDateType() As String
        Get
            Return _sLibelleDateType
        End Get
        Set(ByVal value As String)
            _sLibelleDateType = value
        End Set
    End Property

    Private _dDateFacture As Date
    Public Property dDateFacture() As Date
        Get
            Return _dDateFacture
        End Get
        Set(ByVal value As Date)
            _dDateFacture = value
        End Set
    End Property

End Class
