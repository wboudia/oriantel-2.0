﻿Public Class Navigation

    Private _sSessionId As String
    Public Property sSessionId() As String
        Get
            Return _sSessionId
        End Get
        Set(ByVal value As String)
            _sSessionId = value
        End Set
    End Property

    Private _nIdUtilisateur As Integer
    Public Property nIdUtilisateur() As Integer
        Get
            Return _nIdUtilisateur
        End Get
        Set(ByVal value As Integer)
            _nIdUtilisateur = value
        End Set
    End Property

    Private _nIdClient As Integer
    Public Property nIdClient() As Integer
        Get
            Return _nIdClient
        End Get
        Set(ByVal value As Integer)
            _nIdClient = value
        End Set
    End Property

    Private _nIdModule As UInt16
    Public Property nIdModule() As UInt16
        Get
            Return _nIdModule
        End Get
        Set(ByVal value As UInt16)
            _nIdModule = value
        End Set
    End Property

    Private _nOrdreLien As Integer
    Public Property nOrdreLien() As Integer
        Get
            Return _nOrdreLien
        End Get
        Set(ByVal value As Integer)
            _nOrdreLien = value
        End Set
    End Property

    Private _nIdRapport As Integer
    Public Property nIdRapport() As Integer
        Get
            Return _nIdRapport
        End Get
        Set(ByVal value As Integer)
            _nIdRapport = value
        End Set
    End Property

    Private _sChaineFiltre As String
    Public Property sChaineFiltre() As String
        Get
            Return _sChaineFiltre
        End Get
        Set(ByVal value As String)
            _sChaineFiltre = value
        End Set
    End Property

    Private _sChaineAriane As String
    Public Property sChaineAriane() As String
        Get
            Return _sChaineAriane
        End Get
        Set(ByVal value As String)
            _sChaineAriane = value
        End Set
    End Property

    Private _sValeurOngletNiv1Selectionne As String
    Public Property sValeurOngletNiv1Selectionne() As String
        Get
            Return _sValeurOngletNiv1Selectionne
        End Get
        Set(ByVal value As String)
            _sValeurOngletNiv1Selectionne = value
        End Set
    End Property

    Private _sValeurOngletNiv2Selectionne As String
    Public Property sValeurOngletNiv2Selectionne() As String
        Get
            Return _sValeurOngletNiv2Selectionne
        End Get
        Set(ByVal value As String)
            _sValeurOngletNiv2Selectionne = value
        End Set
    End Property



    Private _dDateLien As Date
    Public Property dDateLien() As Date
        Get
            Return _dDateLien
        End Get
        Set(ByVal value As Date)
            _dDateLien = value
        End Set
    End Property

   

    Public Sub New()
    End Sub

    Public Sub chargerNavigationParOrdre(ByVal sSessionId As String, ByVal nOrdreLien As Integer, ByVal nIdUtilisateur As Integer, ByVal nIdModule As Integer, ByVal nIdClient As Integer)
        cDal.chargerNavigationParOrdre(Me, sSessionId, nOrdreLien, nIdUtilisateur, nIdModule, nIdClient)
    End Sub

    Public Sub ecrireNavigationParOrdre(ByVal sSessionId As String, ByVal nOrdreLien As Integer, ByVal nIdUtilisateur As Integer, ByVal nIdModule As Integer, ByVal nIdClient As Integer, ByVal nIdRapport As Integer, ByVal sChaineFiltre As String, ByVal sChaineAriane As String, ByVal sValeurOngletNiv1Selectionne As String, ByVal sValeurOngletNiv2Selectionne As String)
        cDal.ecrireNavigationParOrdre(sSessionId, nOrdreLien, nIdUtilisateur, nIdModule, nIdClient, nIdRapport, sChaineFiltre, sChaineAriane, sValeurOngletNiv1Selectionne, sValeurOngletNiv2Selectionne)
    End Sub

    Public Sub chargerNavigationParChaineAriane(ByVal sSessionId As String, ByVal sChaineAriane As String, ByVal nIdUtilisateur As Integer, ByVal nIdModule As Integer, ByVal nIdClient As Integer)
        cDal.chargerNavigationParChaineAriane(Me, sSessionId, sChaineAriane, nIdUtilisateur, nIdModule, nIdClient)
    End Sub

    Public Sub SupprimerDerniereNavigation(ByVal sSessionId As String, ByVal nIdUtilisateur As Integer, ByVal nIdModule As Integer, ByVal nIdClient As Integer)
        cDal.SupprimerDerniereNavigation(sSessionId, nIdUtilisateur, nIdModule, nIdClient)
    End Sub

    Public Sub effacerNavigation()
        cDal.effacerNavigation()
    End Sub
End Class
