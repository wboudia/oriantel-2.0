﻿Public Class AffichagePersonnaliseGraphique

    Private _nIdAffichagePersonnaliseGraphique As UShort
    Public Property nIdAffichagePersonnaliseGraphique() As UShort
        Get
            Return _nIdAffichagePersonnaliseGraphique
        End Get
        Set(ByVal value As UShort)
            _nIdAffichagePersonnaliseGraphique = value
        End Set
    End Property

    Private _sValeurLegende As String
    Public Property sValeurLegende() As String
        Get
            Return _sValeurLegende
        End Get
        Set(ByVal value As String)
            _sValeurLegende = value
        End Set
    End Property

    Private _sValeurLabel As String
    Public Property sValeurLabel() As String
        Get
            Return _sValeurLabel
        End Get
        Set(ByVal value As String)
            _sValeurLabel = value
        End Set
    End Property

    Private _sValeurDetachee As String
    Public Property sValeurDetachee() As String
        Get
            Return _sValeurDetachee
        End Get
        Set(ByVal value As String)
            _sValeurDetachee = value
        End Set
    End Property

    Private _sValeurExterieure As Boolean
    Public Property sValeurExterieure() As Boolean
        Get
            Return _sValeurExterieure
        End Get
        Set(ByVal value As Boolean)
            _sValeurExterieure = value
        End Set
    End Property

    Private _bChangementCasValeurFaible As Boolean
    Public Property bChangementCasValeurFaible() As Boolean
        Get
            Return _bChangementCasValeurFaible
        End Get
        Set(ByVal value As Boolean)
            _bChangementCasValeurFaible = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub recupererAffichagePersonnaliseGraphique()
        cDal.recupererAffichagePersonnaliseGraphique(Me)
    End Sub

End Class
