﻿Public Class ParamRapportRedirection

    Private _oRapport As Rapport
    Public Property oRapport() As Rapport
        Get
            Return _oRapport
        End Get
        Set(ByVal value As Rapport)
            _oRapport = value
        End Set
    End Property

    Private _oRedirection As Redirection
    Public Property oRedirection() As Redirection
        Get
            Return _oRedirection
        End Get
        Set(ByVal value As Redirection)
            _oRedirection = value
        End Set
    End Property


End Class
