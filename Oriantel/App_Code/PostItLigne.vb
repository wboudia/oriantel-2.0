﻿Public Class PostItLigne

    Private _nIdPostIt As Integer
    Public Property nIdPostIt() As Integer
        Get
            Return _nIdPostIt
        End Get
        Set(ByVal value As Integer)
            _nIdPostIt = value
        End Set
    End Property

    Private _sNumLigne As String
    Public Property sNumLigne() As String
        Get
            Return _sNumLigne
        End Get
        Set(ByVal value As String)
            _sNumLigne = value
        End Set
    End Property

    Private _sInfoPostIt As String
    Public Property sInfoPostIt() As String
        Get
            Return _sInfoPostIt
        End Get
        Set(ByVal value As String)
            _sInfoPostIt = value
        End Set
    End Property

    Public Sub chargerPostItLigne(ByRef oClient As Client)
        cDal.chargerPostItLigne(Me, oClient)
    End Sub

    Public Sub updatePostItLigne(ByRef oClient As Client)
        cDal.updatePostItLigne(Me, oClient)
    End Sub

    Public Sub InsertionPostItLigne(ByRef oClient As Client)
        cDal.InsertionPostItLigne(Me, oClient)
    End Sub

    Public Shared Sub recupererListeTablePostItSelonClient(ByRef oListeTableRapport As List(Of String), ByRef oClient As Client)
        cDal.recupererListeTablePostItSelonClient(oListeTableRapport, oClient)
    End Sub

    'Public Sub updatePostItLigneSurTableRapportPourRepercussionDirecte(ByRef oListeTableRapport As List(Of String), ByRef oClient As Client)
    '    cDal.updatePostItLigneSurTableRapportPourRepercussionDirecte(Me, oListeTableRapport, oClient)
    'End Sub

End Class
