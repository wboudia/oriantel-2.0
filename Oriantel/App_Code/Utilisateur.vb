﻿Public Class Utilisateur

    Private _nIdUtilisateur As Integer
    Public Property nIdUtilisateur() As Integer
        Get
            Return _nIdUtilisateur
        End Get
        Set(ByVal value As Integer)
            _nIdUtilisateur = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property


    Private _sLogin As String
    Public Property sLogin() As String
        Get
            Return _sLogin
        End Get
        Set(ByVal value As String)
            _sLogin = value
        End Set
    End Property

    Private _sPassword As String
    Public Property sPassword() As String
        Get
            Return _sPassword
        End Get
        Set(ByVal value As String)
            _sPassword = value
        End Set
    End Property

    Private _sPasswordClair As String
    Public Property sPasswordClair() As String
        Get
            Return _sPasswordClair
        End Get
        Set(ByVal value As String)
            _sPasswordClair = value
        End Set
    End Property

    Private _sNom As String
    Public Property sNom() As String
        Get
            Return _sNom
        End Get
        Set(ByVal value As String)
            _sNom = value
        End Set
    End Property

    Private _sPrenom As String
    Public Property sPrenom() As String
        Get
            Return _sPrenom
        End Get
        Set(ByVal value As String)
            _sPrenom = value
        End Set
    End Property

    Private _sCivilite As String
    Public Property sCivilite() As String
        Get
            Return _sCivilite
        End Get
        Set(ByVal value As String)
            _sCivilite = value
        End Set
    End Property

    Private _sMail As String
    Public Property sMail() As String
        Get
            Return _sMail
        End Get
        Set(ByVal value As String)
            _sMail = value
        End Set
    End Property

    Private _dDateCreation As Date
    Public Property dDateCreation() As Date
        Get
            Return _dDateCreation
        End Get
        Set(ByVal value As Date)
            _dDateCreation = value
        End Set
    End Property

    Private _dDateSuppression As Date
    Public Property dDateSuppression() As Date
        Get
            Return _dDateSuppression
        End Get
        Set(ByVal value As Date)
            _dDateSuppression = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal idUtilisateur As Integer)
        nIdUtilisateur = idUtilisateur
    End Sub

    Public Sub Req_Infos_Utilisateur(ByVal idUtilisateur As Integer, ByVal nIdClient As Integer)
        cDal.Req_Infos_Utilisateur(Me, idUtilisateur, nIdClient)
    End Sub

    Public Sub chargerUtilisateurParId(ByVal idUtilisateur As Integer, ByVal nIdClient As Integer)
        cDal.chargeUtilisateurParId(Me, idUtilisateur, nIdClient)
    End Sub
End Class