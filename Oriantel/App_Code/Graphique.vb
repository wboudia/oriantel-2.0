﻿Public Class Graphique

    Private _nIdGraphique As Integer
    Public Property nIdGraphique() As Integer
        Get
            Return _nIdGraphique
        End Get
        Set(ByVal value As Integer)
            _nIdGraphique = value
        End Set
    End Property

    Private _bAffichage3d As Boolean
    Public Property bAffichage3d() As Boolean
        Get
            Return _bAffichage3d
        End Get
        Set(ByVal value As Boolean)
            _bAffichage3d = value
        End Set
    End Property

    Private _nLargeurParPoint As Integer
    Public Property nLargeurParPoint() As Integer
        Get
            Return _nLargeurParPoint
        End Get
        Set(ByVal value As Integer)
            _nLargeurParPoint = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Private _nTailleLegende As Integer
    Public Property nTailleLegende() As Integer
        Get
            Return _nTailleLegende
        End Get
        Set(ByVal value As Integer)
            _nTailleLegende = value
        End Set
    End Property

    Private _nInclination3D As Short
    Public Property nInclination3D() As Short
        Get
            Return _nInclination3D
        End Get
        Set(ByVal value As Short)
            _nInclination3D = value
        End Set
    End Property

    Private _nRotation3D As Short
    Public Property nRotation3D() As Short
        Get
            Return _nRotation3D
        End Get
        Set(ByVal value As Short)
            _nRotation3D = value
        End Set
    End Property

    Public Sub New(ByVal idGraphique As Integer)
        Me.nIdGraphique = idGraphique
    End Sub

    Public Sub recupererGraphiqueParId()
        cDal.recupererGraphiqueParId(Me)
    End Sub

End Class
