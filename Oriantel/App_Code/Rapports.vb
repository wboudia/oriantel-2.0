﻿Public Class Rapports : Inherits List(Of Rapport)

    Public Sub chargerRappportsParModuleUtilisateur(ByVal idModule As Integer, ByVal idUtilisateur As Integer, ByVal nIdClient As Integer)
        cDal.chargerRappportsParModuleUtilisateur(Me, idModule, idUtilisateur, nIdClient)
    End Sub

    Public Sub chargerRappportsParModuleUtilisateurIdRapport(ByVal idModule As Integer, ByVal idUtilisateur As Integer, ByVal idRapport As Integer, ByVal nIdClient As Integer)
        cDal.chargerRappportsParModuleUtilisateurIdRapport(Me, idModule, idUtilisateur, idRapport, nIdClient)
    End Sub

    Public Sub chargerRappportsParModuleUtilisateurIdRapport_ListeIdRapports(ByVal idModule As Integer, ByVal idUtilisateur As Integer, ByVal oRapport As Rapport)
        cDal.chargerRappportsParModuleUtilisateurIdRapport_ListeIdRapports(Me, idModule, idUtilisateur, oRapport)
    End Sub

End Class
