﻿Public Class Rapport

    Private _nIdRapport As Integer
    Public Property nIdRapport() As Integer
        Get
            Return _nIdRapport
        End Get
        Set(ByVal value As Integer)
            _nIdRapport = value
        End Set
    End Property

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Private _oUtilisateur As Utilisateur
    Public Property oUtilisateur() As Utilisateur
        Get
            Return _oUtilisateur
        End Get
        Set(ByVal value As Utilisateur)
            _oUtilisateur = value
        End Set
    End Property

    Private _oModule As Modul
    Public Property oModule() As Modul
        Get
            Return _oModule
        End Get
        Set(ByVal value As Modul)
            _oModule = value
        End Set
    End Property

    Private _sPageRapport As String
    Public Property sPageRapport() As String
        Get
            Return _sPageRapport
        End Get
        Set(ByVal value As String)
            _sPageRapport = value
        End Set
    End Property

    Private _sTableRapport As String
    Public Property sTableRapport() As String
        Get
            Return _sTableRapport
        End Get
        Set(ByVal value As String)
            _sTableRapport = value
        End Set
    End Property

    Private _sSommaireNiv1 As String
    Public Property sSommaireNiv1() As String
        Get
            Return _sSommaireNiv1
        End Get
        Set(ByVal value As String)
            _sSommaireNiv1 = value
        End Set
    End Property

    Private _sSommaireNiv2 As String
    Public Property sSommaireNiv2() As String
        Get
            Return _sSommaireNiv2
        End Get
        Set(ByVal value As String)
            _sSommaireNiv2 = value
        End Set
    End Property

    Private _nSommaireOrdreNiv1 As UInt16
    Public Property nSommaireOrdreNiv1() As UInt16
        Get
            Return _nSommaireOrdreNiv1
        End Get
        Set(ByVal value As UInt16)
            _nSommaireOrdreNiv1 = value
        End Set
    End Property

    Private _nSommaireOrdreNiv2 As UInt16
    Public Property nSommaireOrdreNiv2() As UInt16
        Get
            Return _nSommaireOrdreNiv2
        End Get
        Set(ByVal value As UInt16)
            _nSommaireOrdreNiv2 = value
        End Set
    End Property

    Private _sOngletNiv1 As String
    Public Property sOngletNiv1() As String
        Get
            Return _sOngletNiv1
        End Get
        Set(ByVal value As String)
            _sOngletNiv1 = value
        End Set
    End Property

    Private _sOngletNiv2 As String
    Public Property sOngletNiv2() As String
        Get
            Return _sOngletNiv2
        End Get
        Set(ByVal value As String)
            _sOngletNiv2 = value
        End Set
    End Property

    Private _nOngletOrdreNiv1 As UInt16
    Public Property nOngletOrdreNiv1() As UInt16
        Get
            Return _nOngletOrdreNiv1
        End Get
        Set(ByVal value As UInt16)
            _nOngletOrdreNiv1 = value
        End Set
    End Property

    Private _nOngletOrdreNiv2 As UInt16
    Public Property nOngletOrdreNiv2() As UInt16
        Get
            Return _nOngletOrdreNiv2
        End Get
        Set(ByVal value As UInt16)
            _nOngletOrdreNiv2 = value
        End Set
    End Property


    Private _nOngletOrdreRapport As UInt16
    Public Property nOngletOrdreRapport() As UInt16
        Get
            Return _nOngletOrdreRapport
        End Get
        Set(ByVal value As UInt16)
            _nOngletOrdreRapport = value
        End Set
    End Property

    Private _ntailleBoite As UInt16
    Public Property ntailleBoite() As UInt16
        Get
            Return _ntailleBoite
        End Get
        Set(ByVal value As UInt16)
            _ntailleBoite = value
        End Set
    End Property

    Private _sTitreTableau As String
    Public Property sTitreTableau() As String
        Get
            Return _sTitreTableau
        End Get
        Set(ByVal value As String)
            _sTitreTableau = value
        End Set
    End Property

    Private _sTriDonnees As String
    Public Property sTriDonnees() As String
        Get
            Return _sTriDonnees
        End Get
        Set(ByVal value As String)
            _sTriDonnees = value
        End Set
    End Property

    Private _bEstActif As Boolean
    Public Property bEstActif() As Boolean
        Get
            Return _bEstActif
        End Get
        Set(ByVal value As Boolean)
            _bEstActif = value
        End Set
    End Property

    Private _bEstVisible As Boolean
    Public Property bEstVisible() As Boolean
        Get
            Return _bEstVisible
        End Get
        Set(ByVal value As Boolean)
            _bEstVisible = value
        End Set
    End Property

    Private _bAficherTotal As Boolean
    Public Property bAfficherTotal() As Boolean
        Get
            Return _bAficherTotal
        End Get
        Set(ByVal value As Boolean)
            _bAficherTotal = value
        End Set
    End Property

    Private _snotificationRequete As String
    Public Property snotificationRequete() As String
        Get
            Return _snotificationRequete
        End Get
        Set(ByVal value As String)
            _snotificationRequete = value
        End Set
    End Property

    Private _bNotificationNiveau1 As Boolean
    Public Property bNotificationNiveau1() As Boolean
        Get
            Return _bNotificationNiveau1
        End Get
        Set(ByVal value As Boolean)
            _bNotificationNiveau1 = value
        End Set
    End Property

    Private _bNotificationNiveau2 As Boolean
    Public Property bNotificationNiveau2() As Boolean
        Get
            Return _bNotificationNiveau2
        End Get
        Set(ByVal value As Boolean)
            _bNotificationNiveau2 = value
        End Set
    End Property

    Private _sTypeTotal As String
    Public Property sTypeTotal() As String
        Get
            Return _sTypeTotal
        End Get
        Set(ByVal value As String)
            _sTypeTotal = value
        End Set
    End Property

    Private _sTitreTotal As String
    Public Property sTitreTotal() As String
        Get
            Return _sTitreTotal
        End Get
        Set(ByVal value As String)
            _sTitreTotal = value
        End Set
    End Property

    Private _bEstBoiteOutilOuverte As Boolean
    Public Property bEstBoiteOutilOuverte() As Boolean
        Get
            Return _bEstBoiteOutilOuverte
        End Get
        Set(ByVal value As Boolean)
            _bEstBoiteOutilOuverte = value
        End Set
    End Property

    Private _bEstHover As Boolean
    Public Property bEstHover() As Boolean
        Get
            Return _bEstHover
        End Get
        Set(ByVal value As Boolean)
            _bEstHover = value
        End Set
    End Property

    Private _bEstFiltreBoite As Boolean
    Public Property bEstFiltreBoite() As Boolean
        Get
            Return _bEstFiltreBoite
        End Get
        Set(ByVal value As Boolean)
            _bEstFiltreBoite = value
        End Set
    End Property

    Private _bAfficherPagination As Boolean
    Public Property bAfficherPagination() As Boolean
        Get
            Return _bAfficherPagination
        End Get
        Set(ByVal value As Boolean)
            _bAfficherPagination = value
        End Set
    End Property

    Private _bAfficherNombreEnregistrements As Boolean
    Public Property bAfficherNombreEnregistrements() As Boolean
        Get
            Return _bAfficherNombreEnregistrements
        End Get
        Set(ByVal value As Boolean)
            _bAfficherNombreEnregistrements = value
        End Set
    End Property

    Private _bAfficherRecherche As Boolean
    Public Property bAfficherRecherche() As Boolean
        Get
            Return _bAfficherRecherche
        End Get
        Set(ByVal value As Boolean)
            _bAfficherRecherche = value
        End Set
    End Property

    Private _bAfficherSeuil As Boolean
    Public Property bAfficherSeuil() As Boolean
        Get
            Return _bAfficherSeuil
        End Get
        Set(ByVal value As Boolean)
            _bAfficherSeuil = value
        End Set
    End Property

    Private _sOutilTop As String
    Public Property sOutilTop() As String
        Get
            Return _sOutilTop
        End Get
        Set(ByVal value As String)
            _sOutilTop = value
        End Set
    End Property

    Private _bAutorisationAjout As Boolean
    Public Property bAutorisationAjout() As Boolean
        Get
            Return _bAutorisationAjout
        End Get
        Set(ByVal value As Boolean)
            _bAutorisationAjout = value
        End Set
    End Property

    Private _bAutorisationModification As Boolean
    Public Property bAutorisationModification() As Boolean
        Get
            Return _bAutorisationModification
        End Get
        Set(ByVal value As Boolean)
            _bAutorisationModification = value
        End Set
    End Property

    Private _bAutorisationSuppression As Boolean
    Public Property bAutorisationSuppression() As Boolean
        Get
            Return _bAutorisationSuppression
        End Get
        Set(ByVal value As Boolean)
            _bAutorisationSuppression = value
        End Set
    End Property


    Private _sTestInsertion As String
    Public Property sTestInsertion() As String
        Get
            Return _sTestInsertion
        End Get
        Set(ByVal value As String)
            _sTestInsertion = value
        End Set
    End Property

    Private _sTestModification As String
    Public Property sTestModification() As String
        Get
            Return _sTestModification
        End Get
        Set(ByVal value As String)
            _sTestModification = value
        End Set
    End Property

    Private _sTableAction As String
    Public Property sTableAction() As String
        Get
            Return _sTableAction
        End Get
        Set(ByVal value As String)
            _sTableAction = value
        End Set
    End Property

    Private _bAutoriserFusionColonnes As Boolean
    Public Property bAutoriserFusionColonnes() As Boolean
        Get
            Return _bAutoriserFusionColonnes
        End Get
        Set(ByVal value As Boolean)
            _bAutoriserFusionColonnes = value
        End Set
    End Property

    Private _bAfficheDonneesApresRecherche As Boolean
    Public Property bAfficheDonneesApresRecherche() As Boolean
        Get
            Return _bAfficheDonneesApresRecherche
        End Get
        Set(ByVal value As Boolean)
            _bAfficheDonneesApresRecherche = value
        End Set
    End Property


    Private _bAfficherCaseMoisGlissant As Boolean
    Public Property bAfficherCaseMoisGlissant() As Boolean
        Get
            Return _bAfficherCaseMoisGlissant
        End Get
        Set(ByVal value As Boolean)
            _bAfficherCaseMoisGlissant = value
        End Set
    End Property

    Private _bAfficherEnMoisGlissant As Boolean
    Public Property bAfficherEnMoisGlissant() As Boolean
        Get
            Return _bAfficherEnMoisGlissant
        End Get
        Set(ByVal value As Boolean)
            _bAfficherEnMoisGlissant = value
        End Set
    End Property

    Private _bAfficherCaseTTC As Boolean
    Public Property bAfficherCaseTTC() As Boolean
        Get
            Return _bAfficherCaseTTC
        End Get
        Set(ByVal value As Boolean)
            _bAfficherCaseTTC = value
        End Set
    End Property

    Private _bAfficherEnTTC As Boolean
    Public Property bAfficherEnTTC() As Boolean
        Get
            Return _bAfficherEnTTC
        End Get
        Set(ByVal value As Boolean)
            _bAfficherEnTTC = value
        End Set
    End Property

    Private _bAfficherHtTtcDansLibelleTotal As Boolean
    Public Property bAfficherHtTtcDansLibelleTotal() As Boolean
        Get
            Return _bAfficherHtTtcDansLibelleTotal
        End Get
        Set(ByVal value As Boolean)
            _bAfficherHtTtcDansLibelleTotal = value
        End Set
    End Property

    Private _AfficherCaseDecimale As Boolean
    Public Property AfficherCaseDecimale() As Boolean
        Get
            Return _AfficherCaseDecimale
        End Get
        Set(ByVal value As Boolean)
            _AfficherCaseDecimale = value
        End Set
    End Property

    Private _bAfficherAvecDecimales As Boolean
    Public Property bAfficherAvecDecimales() As Boolean
        Get
            Return _bAfficherAvecDecimales
        End Get
        Set(ByVal value As Boolean)
            _bAfficherAvecDecimales = value
        End Set
    End Property

    Private _bAfficherCaseNumComplet As Boolean
    Public Property bAfficherCaseNumComplet() As Boolean
        Get
            Return _bAfficherCaseNumComplet
        End Get
        Set(ByVal value As Boolean)
            _bAfficherCaseNumComplet = value
        End Set
    End Property

    Private _bAfficherNumeroEntier As Boolean
    Public Property bAfficherNumeroEntier() As Boolean
        Get
            Return _bAfficherNumeroEntier
        End Get
        Set(ByVal value As Boolean)
            _bAfficherNumeroEntier = value
        End Set
    End Property

    Private _bAfficherExport As Boolean
    Public Property bAfficherExport() As Boolean
        Get
            Return _bAfficherExport
        End Get
        Set(ByVal value As Boolean)
            _bAfficherExport = value
        End Set
    End Property

    Private _sNomFichierExport As String
    Public Property sNomFichierExport() As String
        Get
            Return _sNomFichierExport
        End Get
        Set(ByVal value As String)
            _sNomFichierExport = value
        End Set
    End Property

    Private _sColonneLiaisonMoisTva As String
    Public Property sColonneLiaisonMoisTva() As String
        Get
            Return _sColonneLiaisonMoisTva
        End Get
        Set(ByVal value As String)
            _sColonneLiaisonMoisTva = value
        End Set
    End Property

    Private _sObjetMailing As String
    Public Property sObjetMailing() As String
        Get
            Return _sObjetMailing
        End Get
        Set(ByVal value As String)
            _sObjetMailing = value
        End Set
    End Property

    Private _sSpecReq As String
    Public Property sSpecReq() As String
        Get
            Return _sSpecReq
        End Get
        Set(ByVal value As String)
            _sSpecReq = value
        End Set
    End Property


    Public Sub New()
    End Sub

    Public Sub New(ByVal idRapport As Integer, ByVal _client As Client, ByVal _utilisateur As Utilisateur, ByVal _module As Modul)
        nIdRapport = idRapport
        oClient = _client
        oUtilisateur = _utilisateur
        oModule = _module
    End Sub

    Public Sub chargerRappportParModuleUtilisateurIdRapport(ByVal idModule As Integer, ByVal idUtilisateur As Integer, ByVal idRapport As Integer, ByVal nIdClient As Integer)
        cDal.chargerRappportParModuleUtilisateurIdRapport(Me, idModule, idUtilisateur, idRapport, nIdClient)
    End Sub

    Public Sub chargerPremierRapportRedirection(ByVal idModule As Integer, ByVal idUtilisateur As Integer, ByVal idRapport As Integer, ByVal nIdClient As Integer)
        cDal.chargerPremierRapportRedirection(Me, idModule, idUtilisateur, idRapport, nIdClient)
    End Sub
End Class
