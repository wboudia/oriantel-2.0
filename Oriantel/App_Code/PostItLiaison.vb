﻿Public Class PostItLiaison
    Private _nIdPostItLiaison As Integer
    Public Property nIdPostItLiaison() As Integer
        Get
            Return _nIdPostItLiaison
        End Get
        Set(ByVal value As Integer)
            _nIdPostItLiaison = value
        End Set
    End Property
    Private _oRapport As Rapport
    Public Property oRapport() As Rapport
        Get
            Return _oRapport
        End Get
        Set(ByVal value As Rapport)
            _oRapport = value
        End Set
    End Property
    Private _sNomTableAvecPostIt As String
    Public Property sNomTableAvecPostIt() As String
        Get
            Return _sNomTableAvecPostIt
        End Get
        Set(ByVal value As String)
            _sNomTableAvecPostIt = value
        End Set
    End Property
    Private _sNomColonneNumTel As String
    Public Property sNomColonneNumTel() As String
        Get
            Return _sNomColonneNumTel
        End Get
        Set(ByVal value As String)
            _sNomColonneNumTel = value
        End Set
    End Property

    Public Sub ChargerPostItLiaisonSelonNomTableAvecPostIt()
        cDal.ChargerPostItLiaisonSelonNomTableAvecPostIt(Me)
    End Sub
End Class
