﻿Public Class UtilisateurSecondaireFiltre

    Private _oUtilisateurPrimaire As Utilisateur
    Public Property oUtilisateurPrimaire() As Utilisateur
        Get
            Return _oUtilisateurPrimaire
        End Get
        Set(ByVal value As Utilisateur)
            _oUtilisateurPrimaire = value
        End Set
    End Property

    Private _oUtilisateurSecondaire As UtilisateurSecondaire
    Public Property oUtilisateurSecondaire() As UtilisateurSecondaire
        Get
            Return _oUtilisateurSecondaire
        End Get
        Set(ByVal value As UtilisateurSecondaire)
            _oUtilisateurSecondaire = value
        End Set
    End Property

    Private _oFiltre As Filtre
    Public Property oFiltre() As Filtre
        Get
            Return _oFiltre
        End Get
        Set(ByVal value As Filtre)
            _oFiltre = value
        End Set
    End Property

    Private _sValeurFiltre As String
    Public Property sValeurFiltre() As String
        Get
            Return _sValeurFiltre
        End Get
        Set(ByVal value As String)
            _sValeurFiltre = value
        End Set
    End Property

    Private _bEstRestriction As Boolean
    Public Property bEstRestriction() As Boolean
        Get
            Return _bEstRestriction
        End Get
        Set(ByVal value As Boolean)
            _bEstRestriction = value
        End Set
    End Property


    Private _bEstTous As Boolean
    Public Property bEstTous() As Boolean
        Get
            Return _bEstTous
        End Get
        Set(ByVal value As Boolean)
            _bEstTous = value
        End Set
    End Property

End Class
