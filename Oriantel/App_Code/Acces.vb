﻿Public Class Acces

    Private _oModule As Modul
    Public Property oModule() As Modul
        Get
            Return _oModule
        End Get
        Set(ByVal value As Modul)
            _oModule = value
        End Set
    End Property

    Private _oUtilisateur As Utilisateur
    Public Property oUtilisateur() As Utilisateur
        Get
            Return _oUtilisateur
        End Get
        Set(ByVal value As Utilisateur)
            _oUtilisateur = value
        End Set
    End Property

    Private _nOrdre As UInt16
    Public Property nOrdre() As UInt16
        Get
            Return _nOrdre
        End Get
        Set(ByVal value As UInt16)
            _nOrdre = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal oModule As Modul, ByVal oUtilisateur As Utilisateur, ByVal ordre As UInt16)
        Me.oModule = oModule
        Me.oUtilisateur = oUtilisateur
        Me.nOrdre = ordre
    End Sub

End Class
