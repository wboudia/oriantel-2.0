﻿Public Class TypeGraphique

    Private _nIdTypeGraphique As Integer
    Public Property nIdTypeGraphique() As Integer
        Get
            Return _nIdTypeGraphique
        End Get
        Set(ByVal value As Integer)
            _nIdTypeGraphique = value
        End Set
    End Property

    Private _stypeGraphique As String
    Public Property stypeGraphique() As String
        Get
            Return _stypeGraphique
        End Get
        Set(ByVal value As String)
            _stypeGraphique = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal idTypeGraphique As Integer)
        Me.nIdTypeGraphique = idTypeGraphique
    End Sub

    Public Sub recuperationTypeGraphique(ByRef oRapport As Rapport)
        cDal.recuperationTypeGraphique(Me, oRapport)
    End Sub

    Public Sub recupererTypeGraphiqueParId()
        cDal.recupererTypeGraphiqueParId(Me)
    End Sub

End Class
