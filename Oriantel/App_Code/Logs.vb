﻿Public Class Logs

    Private _nIdLogs As Integer
    Public Property nIdLogs() As Integer
        Get
            Return _nIdLogs
        End Get
        Set(ByVal value As Integer)
            _nIdLogs = value
        End Set
    End Property

    Private _oUtilisateur As Utilisateur
    Public Property oUtilisateur() As Utilisateur
        Get
            Return _oUtilisateur
        End Get
        Set(ByVal value As Utilisateur)
            _oUtilisateur = value
        End Set
    End Property

    Private _oUtilisateurSecondaire As UtilisateurSecondaire
    Public Property oUtilisateurSecondaire() As UtilisateurSecondaire
        Get
            Return _oUtilisateurSecondaire
        End Get
        Set(ByVal value As UtilisateurSecondaire)
            _oUtilisateurSecondaire = value
        End Set
    End Property

    Private _oModule As Modul
    Public Property oModule() As Modul
        Get
            Return _oModule
        End Get
        Set(ByVal value As Modul)
            _oModule = value
        End Set
    End Property

    Private _sIp As String
    Public Property sIp() As String
        Get
            Return _sIp
        End Get
        Set(ByVal value As String)
            _sIp = value
        End Set
    End Property

    Private _dDateHeure As Date
    Public Property dDateHeure() As Date
        Get
            Return _dDateHeure
        End Get
        Set(ByVal value As Date)
            _dDateHeure = value
        End Set
    End Property

    Private _sResolution As String
    Public Property sResolution() As String
        Get
            Return _sResolution
        End Get
        Set(ByVal value As String)
            _sResolution = value
        End Set
    End Property

    Private _sNavigateur As String
    Public Property sNavigateur() As String
        Get
            Return _sNavigateur
        End Get
        Set(ByVal value As String)
            _sNavigateur = value
        End Set
    End Property

    Private _nNavigateurVersion As UInt16
    Public Property nNavigateurVersion() As UInt16
        Get
            Return _nNavigateurVersion
        End Get
        Set(ByVal value As UInt16)
            _nNavigateurVersion = value
        End Set
    End Property

    Private _oRapport As Rapport
    Public Property oRapport() As Rapport
        Get
            Return _oRapport
        End Get
        Set(ByVal value As Rapport)
            _oRapport = value
        End Set
    End Property

    Public Sub insertionInformationsUtilisateurDansLog(ByVal idClient As Integer, ByVal idUtilisateur As Integer, ByVal idModule As Integer, ByVal sBrowserName As String, ByVal sBrowserVersion As UInt16, ByVal sResolution As String, ByVal sIp As String, nIdUtilisateurSecondaire As Integer)
        cDal.insertionInformationsUtilisateurDansLog(idClient, idUtilisateur, idModule, sBrowserName, sBrowserVersion, sResolution, sIp, nIdUtilisateurSecondaire)
    End Sub

    Public Sub recuperationDerniereConnexion()
        cDal.Req_Logs_Utilisateur(Me)
    End Sub

    Public Sub recuperationDerniereConnexionPourUtilisateurSecondaire()
        cDal.Req_Logs_Utilisateur_Secondaire(Me)
    End Sub

    Public Sub insertionInformationsUtilisateurDansLog(ByVal idClient As Integer, ByVal idUtilisateur As Integer, ByVal idModule As Integer, ByVal sBrowserName As String, ByVal sBrowserVersion As UInt16, ByVal sResolution As String, ByVal sIp As String, ByVal idRapport As Integer, ByVal nIdUtilisateurSecondaire As Integer)
        cDal.insertionInformationsUtilisateurDansLog(idClient, idUtilisateur, idModule, sBrowserName, sBrowserVersion, sResolution, sIp, idRapport, nIdUtilisateurSecondaire)
    End Sub

End Class
