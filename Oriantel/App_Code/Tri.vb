﻿Public Class Tri
    Private _sIdGrid As String
    Public Property sIdGrid() As String
        Get
            Return _sIdGrid
        End Get
        Set(ByVal value As String)
            _sIdGrid = value
        End Set
    End Property

    Private _sdatafield As String
    Public Property sdatafield() As String
        Get
            Return _sdatafield
        End Get
        Set(ByVal value As String)
            _sdatafield = value
        End Set
    End Property

    Private _sTri As String
    Public Property sTri() As String
        Get
            Return _sTri
        End Get
        Set(ByVal value As String)
            _sTri = value
        End Set
    End Property


    Public Sub New(ByVal _sIdGrid As String, ByVal _sdatafield As String, ByVal _sTri As String)
        sIdGrid = _sIdGrid
        sdatafield = _sdatafield
        sTri = _sTri
    End Sub
End Class
