﻿Public Class ParamSerieColonne

    Private _oClient As Client
    Public Property oClient() As Client
        Get
            Return _oClient
        End Get
        Set(ByVal value As Client)
            _oClient = value
        End Set
    End Property

    Private _oSerie As Serie
    Public Property oSerie() As Serie
        Get
            Return _oSerie
        End Get
        Set(ByVal value As Serie)
            _oSerie = value
        End Set
    End Property


    Private _oColonne As Colonne
    Public Property oColonne() As Colonne
        Get
            Return _oColonne
        End Get
        Set(ByVal value As Colonne)
            _oColonne = value
        End Set
    End Property

End Class
