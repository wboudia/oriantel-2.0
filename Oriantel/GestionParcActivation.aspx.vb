﻿Imports System.Text.RegularExpressions

Public Class GestionParcActivation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If


        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim nIdLigne As Integer
        Dim oLigne As New Ligne

        If Request.Item("idLigne") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            nIdLigne = CInt(Request.Item("idLigne").ToString)
        End If

        oLigne.chargerLigneParId(nIdLigne)
        If Not Page.IsPostBack Then
            If oLigne.dDateActivation = Nothing Then
                Me.TxtDateActivation.Text = Now.ToString("dd/MM/yyyy")
            Else
                Me.TxtDateActivation.Text = oLigne.dDateActivation.ToString("dd/MM/yyyy")
            End If
        End If

    End Sub




    Private Sub BtnActiverValider_Click(sender As Object, e As EventArgs) Handles BtnActiverValider.Click


        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If


        Dim nIdClient As Integer = CInt(User.Identity.Name.Split(CChar("_"))(1))
        Dim nIdLigne As Integer
        Dim oLigne As New Ligne

        If Request.Item("idLigne") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            nIdLigne = CInt(Request.Item("idLigne").ToString)
        End If

        oLigne.chargerLigneParId(nIdClient)


        Dim regex As Regex = New Regex("^(06|07)[0-9]{8}$")
        Dim match As Match = regex.Match(Me.TxtNumeroGSM.Text)
        If Not match.Success Then
            Me.LblInformation.Text = "Le format du numéro n'est pas valide"
            Exit Sub
        Else
            Me.LblInformation.Text = ""
        End If

        If cDal.TesterFiltrePresentRapportSiNumeroExiste(Me.TxtNumeroGSM.Text, nIdClient) = True Then
            Me.LblInformation.Text = "Le numéro saisi existe déjà dans le parc."
            Exit Sub
        Else
            Me.LblInformation.Text = ""
        End If




        If Not IsDate(Me.TxtDateActivation.Text) Then
            Me.LblInformation.Text = "La date n'est pas valide"
            Exit Sub
        Else
            Me.LblInformation.Text = ""
        End If

        Dim oFonction As New Fonction
        Dim oCompteFacturant As New CompteFacturant
        Dim oDiscrimination As New Discrimination
        Dim oForfait As New Forfait
        Dim oHierarchie As New Hierarchie

        oLigne.nIdLigne = nIdLigne
        oLigne.sNumeroGSM = Me.TxtNumeroGSM.Text
        oLigne.dDateActivation = CDate(Me.TxtDateActivation.Text)

        oLigne.activerLigne()

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub
    Private Sub BtnActiverAnnuler_Click(sender As Object, e As EventArgs) Handles BtnActiverAnnuler.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CloseKey", "parent.$.colorbox.close();", True)
    End Sub



End Class