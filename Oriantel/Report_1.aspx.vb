﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.IO
Imports System.Runtime.InteropServices.Marshal
Imports Microsoft.Office.Interop
Imports PDFCreator
Imports System.Net.Mail

Public Class Report_1
    Inherits System.Web.UI.Page

    Public PremierPassageSansHistorique As Boolean
    Public SessionUtilisateur As String
    Public oGridDataBoundLists As New GridDataBoundLists()
    Public oGridRowFooter As GridViewRow


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        '---Test de la connexion de l'utilisateur

        If User.Identity.IsAuthenticated = False Then
            Response.Write("<script>window.open('login.aspx','_parent');<" & Chr(47) & "script>")
        End If



        If Request.Item("module") = Nothing Then
            Response.Redirect("\default.aspx")
            Exit Sub
        Else
            Me.idModule.Value = Request.Item("module").ToString
        End If

        '---Masquage de certains éléments si on est en redirection popup
        Dim estPopup As String = "false"
        If Not Request.Item("masquer") = Nothing Then
            If Request.Item("masquer").ToString = "false" Then
                estPopup = "true"
                Me.UpdatePanelAriane.Attributes.Add("style", "display:none;")
                Me.UpdatePanelFooter.Attributes.Add("style", "display:none;")
                Me.UpdatePanelSommaire.Attributes.Add("style", "display:none;")
                Me.blocCorps.Attributes.Add("style", "margin-left:1px;")
                Me.PageBody.Attributes.Add("style", "background: url(images/bkgdsansfond.png) left bottom;")
                Me.EstPopup.Value = "oui"

            End If
        End If


        '---Gestion du cookie pour la sauvegarde des étapes de navigation
        If (Request.Cookies("sessionOria") IsNot Nothing) Then
            SessionUtilisateur = Request.Cookies("sessionOria")("Identifiant")
        Else
            Dim myCookie As HttpCookie
            myCookie = New HttpCookie("sessionOria")
            SessionUtilisateur = Session.SessionID
            Response.Cookies("sessionOria")("Identifiant") = SessionUtilisateur
        End If
        SessionUtilisateur += estPopup.Substring(0, 1)

        '---Assignation d'un Idrapport si il est passé en paramètre
        If Not Request.Item("rapport") = Nothing Then
            Me.idRapport.Value = Request.Item("rapport").ToString
        End If

        '---Gestion des filtres de redirection lors d'un popup
        If Not Request.Item("chainefiltre") = Nothing Then
            If Me.ChaineFiltreRedirection.Value <> Request.Item("chainefiltre").ToString Then
                If Page.IsPostBack = False Or Me.ChaineFiltreRedirection.Value <> "Efface" Then
                    Me.ChaineFiltreRedirection.Value = Request.Item("chainefiltre").ToString
                End If
            End If
        End If

        Dim oUtilisateur As New Utilisateur()
        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))
        Dim idModule As Integer
        Dim oModule As New Modul
        idModule = CInt(Request.Item("module").ToString)
        oModule.chargerModulesParId(idModule, oUtilisateur.oClient)

        '---Gestion de la cloture d'incidents
        If Me.ReferenceCloture.Value <> "" Then
            Dim sInfos() As String = Me.ReferenceCloture.Value.Split(CChar("|"))
            If sInfos.Length = 4 Then
                cDal.CloturerTicket(sInfos(0), sInfos(1), sInfos(2), sInfos(3))
            End If
            Me.ReferenceCloture.Value = ""
        End If

        '---Gestion de la Supprssion de gestionparc
        If Me.ReferenceSuppressionLigne.Value <> "" Then
            Dim sInfos() As String = Me.ReferenceSuppressionLigne.Value.Split(CChar("|"))
            If sInfos.Length = 3 Then
                cDal.SupprimerParc(sInfos(0), sInfos(1), sInfos(2))
            End If
            If sInfos.Length = 4 Then
                If cDal.SupprimerParc(sInfos(0), sInfos(1), sInfos(2), sInfos(3)) = False Then
                    Me.Notification.Value = "Vous ne pouvez pas supprimer cet enregistrement car il est utilisé dans une autre table."
                End If
            End If
            Me.ReferenceSuppressionLigne.Value = ""
        End If

        '---Gestion de la Supprssion d'incidents
        If Me.ReferenceSuppression.Value <> "" Then
            Dim sInfos() As String = Me.ReferenceSuppression.Value.Split(CChar("|"))
            If sInfos.Length = 4 Then
                cDal.SupprimerTicket(sInfos(0), sInfos(1), sInfos(2), sInfos(3))
            End If
            Me.ReferenceSuppression.Value = ""
        End If

        '---Gestion de la Supprssion d'incidents (Détail)
        If Me.ReferenceSuppressionDetail.Value <> "" Then
            Dim sInfos() As String = Me.ReferenceSuppressionDetail.Value.Split(CChar("|"))
            If sInfos.Length = 3 Then
                cDal.SupprimerTicketDetail(sInfos(0), sInfos(1), sInfos(2))
            End If
            Me.ReferenceSuppressionDetail.Value = ""
        End If

        Dim oNavigation As New Navigation
        Dim sChainePagination As String = ""
        Dim oListeRapports As New Rapports

        '---Ecriture de l'étape de navigation si le hiddenfield autorisationEnregistrementNavigation est à true (il est alimenté par les liens du sommaire)
        If Page.IsPostBack Then

            If Me.retourActive.Value = "true" And Me.listeEtapeNavigation.Value <> "0" Then
                oNavigation.SupprimerDerniereNavigation(SessionUtilisateur, oUtilisateur.nIdUtilisateur, oModule.nIdModule, oUtilisateur.oClient.nIdClient)
            End If

            If Me.autorisationEnregistrementNavigation.Value = "true" Then
                oNavigation.ecrireNavigationParOrdre(SessionUtilisateur, CInt(Me.etapeNavigation.Value), oUtilisateur.nIdUtilisateur, oModule.nIdModule, oUtilisateur.oClient.nIdClient, CInt(Me.idRapport.Value), Me.chaineFiltreRetour.Value, Me.chaineArianeRetour.Value, Me.ValeurOngletNiv1Selectionne.Value, Me.ValeurOngletNiv2Selectionne.Value)
                'Me.chaineFiltreRetour.Value = ""
                Me.autorisationEnregistrementNavigation.Value = "false"
                Charger_Ariane()
                UpdatePanelAriane.Update()
            End If

            Charger_Footer()
            UpdatePanelFooter.Update()

            '---Gestion du retour arriere si le hiddenField retourActive est à true (il est alimenté par le bouton retour)
            If Me.retourActive.Value = "true" And Me.listeEtapeNavigation.Value <> "0" Then
                Dim sListeEtapeNavigation() As String = Me.listeEtapeNavigation.Value.Split(CChar("|"))
                Dim nEtapeNavigation As Integer = CInt(sListeEtapeNavigation(sListeEtapeNavigation.Count - 2))
                Dim sNouvelleValeurListeetapeNavigation As String = ""
                For i = 0 To sListeEtapeNavigation.Count - 2
                    sNouvelleValeurListeetapeNavigation &= sListeEtapeNavigation(i) & "|"
                Next
                If sNouvelleValeurListeetapeNavigation <> "" Then sNouvelleValeurListeetapeNavigation = sNouvelleValeurListeetapeNavigation.Remove(sNouvelleValeurListeetapeNavigation.Length - 1, 1)
                Me.listeEtapeNavigation.Value = sNouvelleValeurListeetapeNavigation
                Me.etapeNavigation.Value = CStr(CInt(Me.etapeNavigation.Value) - 1)
                Me.retourActive.Value = "false"

                oNavigation.chargerNavigationParOrdre(SessionUtilisateur, nEtapeNavigation, oUtilisateur.nIdUtilisateur, oModule.nIdModule, oUtilisateur.oClient.nIdClient)
                If CStr(oNavigation.nIdRapport) <> "0" Then
                    Me.idRapport.Value = CStr(oNavigation.nIdRapport)
                Else
                    Me.idRapport.Value = ""
                End If
                Me.ChaineFiltreRedirection.Value = oNavigation.sChaineFiltre
                Me.ValeurOngletNiv1Selectionne.Value = oNavigation.sValeurOngletNiv1Selectionne
                Me.ValeurOngletNiv2Selectionne.Value = oNavigation.sValeurOngletNiv2Selectionne

                '---Gestion Fil Ariane Redirection + sommaire
                If Me.ChaineFiltreRedirection.Value <> "" Then
                    Dim oRapport As New Rapport
                    oRapport.chargerPremierRapportRedirection(idModule, oUtilisateur.nIdUtilisateur, CInt(Me.idRapport.Value), CInt(User.Identity.Name.Split(CChar("_"))(1)))

                    Dim idRapportTemporaire As String = ""
                    idRapportTemporaire = Me.idRapport.Value
                    Me.idRapport.Value = CStr(oRapport.nIdRapport)
                    Me.idRapportTemporaireSommaire.Value = CStr(oRapport.nIdRapport)

                    Charger_Ariane(oNavigation)
                    UpdatePanelAriane.Update()
                    UpdatePanelSommaire.Update()

                    Me.idRapport.Value = idRapportTemporaire
                Else
                    Charger_Ariane()
                    UpdatePanelAriane.Update()
                    UpdatePanelSommaire.Update()
                End If
            Else
                UpdatePanelSommaire.Update()
            End If
        Else
            Charger_Ariane()
            UpdatePanelAriane.Update()
            Charger_Footer()
            UpdatePanelFooter.Update()
            UpdatePanelCorps.Update()
            UpdatePanelSommaire.Update()
            Dim LienNavigation As String = oModule.sLien & "?module=" & oModule.nIdModule & "&masquer=false&navigation=0"
            Me.etapeNavigation.Value = "0"
            oNavigation.ecrireNavigationParOrdre(SessionUtilisateur, 0, oUtilisateur.nIdUtilisateur, oModule.nIdModule, oUtilisateur.oClient.nIdClient, 0, "", "", Me.ValeurOngletNiv1Selectionne.Value, Me.ValeurOngletNiv2Selectionne.Value)
        End If

    End Sub

    Protected Sub UpdatePanelSommaire_Load(sender As Object, e As EventArgs) Handles UpdatePanelSommaire.Load
        Charger_Sommaire()
    End Sub

    Private Sub Charger_Sommaire()
        Dim idModule As String = ""
        Dim oUtilisateur As New Utilisateur()
        Dim oModule As New Modul()
        Dim oRapports As New Rapports()
        Dim oliteral As New Literal
        Dim sTexteControl As String = ""
        Dim oClient As New Client
        oClient.chargerClientParId(CInt(User.Identity.Name.Split(CChar("_"))(1)))

        idModule = CStr(CInt(Request.Item("module").ToString))
        oModule.chargerModulesParId(CInt(idModule), oClient)
        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))
        oRapports.chargerRappportsParModuleUtilisateur(CInt(idModule), oUtilisateur.nIdUtilisateur, oUtilisateur.oClient.nIdClient)

        Me.PlaceHolderTitreSommaire.Controls.Clear()
        Me.PlaceHolderElementSommaire.Controls.Clear()

        '---Logo client
        Me.logoClient.ImageUrl = "images/logo/" & oUtilisateur.oClient.sLogo
        'Me.logoClient.Width = New Unit(120, UnitType.Pixel)
        Me.logoClient.Height = New Unit(120, UnitType.Pixel)
        Me.logoClient.AlternateText = "logo " & oUtilisateur.oClient.sNomClient

        '---Titre sommaire
        Dim PTitreSommaire As New HtmlGenericControl("p")
        PTitreSommaire.Attributes.Add("id", "section-name")
        PTitreSommaire.Attributes.Add("class", oModule.sLibelleClasseSommaire)
        PTitreSommaire.InnerText = oModule.sTitre
        Me.PlaceHolderTitreSommaire.Controls.Add(PTitreSommaire)

        Dim sNiveau1 As String = ""
        Dim sNiveau2 As String = ""
        Dim PremierPassage As Boolean = True
        Dim UL As New HtmlGenericControl
        Dim DIV As New HtmlGenericControl
        Dim LiNiveau1 As New HtmlGenericControl("li")

        Dim oRapportActuel As New Rapport
        Dim idRapportActuel As String = Me.idRapport.Value
        If Me.idRapportTemporaireSommaire.Value <> "" Then
            idRapportActuel = Me.idRapportTemporaireSommaire.Value
        End If
        If idRapportActuel <> "" Then oRapportActuel = oRapports.Find(Function(p) p.nIdRapport = CInt(idRapportActuel))

        Me.idRapportTemporaireSommaire.Value = ""
        Dim sIdLienTitre1Selectionne As String = ""

        Dim sNiveau1Ancien As String = ""


        Dim i As Integer

        For i = 0 To oRapports.Count - 1
            If oRapports.Item(i).bEstVisible = True Then
                If oRapports.Item(i).sSommaireNiv1 <> sNiveau1 Then
                    If UL.Controls.Count > 0 Then
                        DIV.Controls.Add(UL)
                        Me.PlaceHolderElementSommaire.Controls.Add(DIV)
                    End If

                    UL = New HtmlGenericControl("UL")
                    DIV = New HtmlGenericControl("DIV")

                    Dim nCompteurNotification As Integer = 0
                    Dim sListeRapport = From oRapport In oRapports Where oRapport.sSommaireNiv1 = oRapports.Item(i).sSommaireNiv1 And oRapport.snotificationRequete <> "" And oRapport.bNotificationNiveau1 = True
                    For Each oRapport In sListeRapport
                        nCompteurNotification += cDal.RecupererNombreNotification(oRapport.snotificationRequete.Split(CChar("|"))(0), oRapport.snotificationRequete.Split(CChar("|"))(1))
                    Next

                    '---Niveau1
                    sNiveau1 = oRapports.Item(i).sSommaireNiv1
                    LiNiveau1 = New HtmlGenericControl("li")
                    LiNiveau1.Style.Add("line-height", "20px")
                    LiNiveau1.Attributes.Add("class", "titre")
                    Dim lienTitreNiveau1 As New LinkButton
                    LiNiveau1.Attributes.Add("id", "LiNiveau1_" & i)
                    lienTitreNiveau1.CausesValidation = False
                    lienTitreNiveau1.OnClientClick = ""
                    sIdLienTitre1Selectionne = "LiNiveau1_" & i

                    sc1.RegisterAsyncPostBackControl(lienTitreNiveau1)
                    lienTitreNiveau1.Text = sNiveau1
                    LiNiveau1.Controls.Add(lienTitreNiveau1)

                    If nCompteurNotification <> 0 Then
                        Dim oLiteralNotification As New Literal
                        oLiteralNotification.Text = "<span class=""notification_menu"">" & CStr(nCompteurNotification) & "</span>"
                        LiNiveau1.Controls.Add(oLiteralNotification)
                    End If



                    Me.PlaceHolderElementSommaire.Controls.Add(LiNiveau1)
                End If

                '---Niveau2
                If oRapports.Item(i).sSommaireNiv2 <> sNiveau2 Or oRapports.Item(i).sSommaireNiv1 <> sNiveau1Ancien Then
                    sNiveau2 = oRapports.Item(i).sSommaireNiv2

                    Dim nCompteurNotification As Integer = 0
                    Dim sListeRapport = From oRapport In oRapports Where oRapport.sSommaireNiv1 = oRapports.Item(i).sSommaireNiv1 And oRapport.sSommaireNiv2 = oRapports.Item(i).sSommaireNiv2 And oRapport.snotificationRequete <> "" And oRapport.bNotificationNiveau2 = True
                    For Each oRapport In sListeRapport
                        nCompteurNotification += cDal.RecupererNombreNotification(oRapport.snotificationRequete.Split(CChar("|"))(0), oRapport.snotificationRequete.Split(CChar("|"))(1))
                    Next

                    Dim LiNiveau2 As New HtmlGenericControl("li")
                    LiNiveau2.Style.Add("line-height", "1.7em")
                    Dim lienTitreNiveau2 As New LinkButton
                    lienTitreNiveau2.ID = "LienTitre2_Rapport_" & oRapports.Item(i).nIdRapport
                    lienTitreNiveau2.Text = sNiveau2
                    If nCompteurNotification <> 0 Then
                        lienTitreNiveau2.Text = lienTitreNiveau2.Text + "<span class=""notification_menu"">" & CStr(nCompteurNotification) & "</span>"
                    End If

                    lienTitreNiveau2.OnClientClick = "ValorisationEtat(" & oRapports.Item(i).oModule.nIdModule & "," & oRapports.Item(i).nIdRapport & ");ValoriserMemoireRetour('');"
                    lienTitreNiveau2.CausesValidation = False
                    lienTitreNiveau2.CssClass = " "
                    If PremierPassage = True And idRapportActuel = "" Then
                        lienTitreNiveau2.CssClass = "active"
                        Me.LienTritre1Selectionne.Value = CStr(oRapports.Item(i).nSommaireOrdreNiv1)
                    End If
                    If idRapportActuel <> "" And idRapportActuel <> "0" Then
                        If oRapports.Item(i).sSommaireNiv1 = oRapportActuel.sSommaireNiv1 And oRapports.Item(i).sSommaireNiv2 = oRapportActuel.sSommaireNiv2 Then
                            lienTitreNiveau2.CssClass = "active"
                            Me.LienTritre1Selectionne.Value = CStr(oRapports.Item(i).nSommaireOrdreNiv1)
                            Me.LienTritre2Selectionne.Value = CStr(oRapports.Item(i).nSommaireOrdreNiv2)
                        End If
                    End If

                    LiNiveau2.Controls.Add(lienTitreNiveau2)

                    sc1.RegisterAsyncPostBackControl(lienTitreNiveau2)
                    UL.Controls.Add(LiNiveau2)
                    sNiveau1Ancien = sNiveau1
                End If
                PremierPassage = False
            End If
        Next

        DIV.Controls.Add(UL)
        Me.PlaceHolderElementSommaire.Controls.Add(DIV)
    End Sub

    Private Sub Charger_Ariane(Optional ByVal oNavigation As Navigation = Nothing)
        Dim idModule As Integer
        Dim oModule As New Modul
        Dim oUtilisateur As New Utilisateur()
        Dim bRetourArriere As Boolean = False
        Dim oClient As New Client

        oClient.chargerClientParId(CInt(User.Identity.Name.Split(CChar("_"))(1)))
        If oClient.sLangueClient = "US" Then
            Me.imgLnkClose.Attributes.Add("alt", "Link to sign out")
            Me.imgLnkClose.Attributes.Add("title", "Click to sign out")
        Else
            Me.imgLnkClose.Attributes.Add("alt", "Lien pour se déconnecter")
            Me.imgLnkClose.Attributes.Add("title", "Cliquer pour se déconnecter")
        End If

        If Not oNavigation Is Nothing Then bRetourArriere = True

        idModule = CInt(Request.Item("module").ToString)
        oModule.chargerModulesParId(idModule, oClient)
        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))

        Me.PlaceHolderAriane.Controls.Clear()

        '--- Ajout du lien du module actuel
        Dim LI As New HtmlGenericControl("LI")
        Dim lienModule As New LinkButton
        lienModule.CausesValidation = False
        lienModule.OnClientClick = "RedirectionAdresse('" & oModule.sLien & "?module=" & oModule.nIdModule & "');"
        sc1.RegisterAsyncPostBackControl(lienModule)
        lienModule.Text = oModule.sTitre
        LI.Controls.Add(lienModule)
        Me.PlaceHolderAriane.Controls.Add(LI)

        '---Ajout des liens mémorisés lors d'un retour arrière
        If bRetourArriere = True Then
            Dim sChaineArianneTemp = ""
            For Each sLienAriane In oNavigation.sChaineAriane.Split(CChar("µ"))
                If sChaineArianneTemp = "" Then
                    sChaineArianneTemp = sLienAriane
                Else
                    sChaineArianneTemp += "µ" + sLienAriane
                End If

                Dim oNavigationActuel As New Navigation
                oNavigationActuel.chargerNavigationParChaineAriane(SessionUtilisateur, sLienAriane, oUtilisateur.nIdUtilisateur, oModule.nIdModule, oUtilisateur.oClient.nIdClient)

                LI = New HtmlGenericControl("LI")
                Dim lienArriareRedirection As New LinkButton
                lienArriareRedirection.CausesValidation = True
                lienArriareRedirection.OnClientClick = "document.getElementById(""chaineArianeRetour"").value="""";PreparerRedirection('" & Request.Item("module").ToString & "','" & CStr(oNavigationActuel.nIdRapport) & "','" & Toolbox.FormaterJavascript(oNavigationActuel.sChaineFiltre) & "','" & Toolbox.FormaterJavascript(oNavigationActuel.sChaineAriane) & "');"
                lienArriareRedirection.Text = sLienAriane
                LI.Controls.Add(lienArriareRedirection)
                Me.PlaceHolderAriane.Controls.Add(LI)
            Next
        Else
            '---Ajout des liens mémorisés lors d'un click sur une redirection
            If Me.chaineArianeRetour.Value <> "" Then
                Dim sChaineArianneTemp = ""
                For Each sLienAriane In Me.chaineArianeRetour.Value.Split(CChar("µ"))
                    If sChaineArianneTemp = "" Then
                        sChaineArianneTemp = sLienAriane
                    Else
                        sChaineArianneTemp += "µ" + sLienAriane
                    End If
                    Dim oNavigationActuel As New Navigation
                    oNavigationActuel.chargerNavigationParChaineAriane(SessionUtilisateur, sChaineArianneTemp, oUtilisateur.nIdUtilisateur, oModule.nIdModule, oUtilisateur.oClient.nIdClient)

                    LI = New HtmlGenericControl("LI")
                    Dim lienArriareRedirection As New LinkButton
                    lienArriareRedirection.CausesValidation = True
                    lienArriareRedirection.OnClientClick = "document.getElementById(""chaineArianeRetour"").value="""";PreparerRedirection('" & Request.Item("module").ToString & "','" & CStr(oNavigationActuel.nIdRapport) & "','" & Toolbox.FormaterJavascript(oNavigationActuel.sChaineFiltre) & "','" & Toolbox.FormaterJavascript(oNavigationActuel.sChaineAriane) & "');"
                    lienArriareRedirection.Text = sLienAriane
                    LI.Controls.Add(lienArriareRedirection)
                    Me.PlaceHolderAriane.Controls.Add(LI)
                Next
            End If
        End If
    End Sub
    Protected Sub UpdatePanelCorps_Load(sender As Object, e As EventArgs) Handles UpdatePanelCorps.Load
        ltlJsFiltres.Text = ""
        Dim sInstructionJsPlacementFiltres As String = ""

        Dim oUtilisateurSecondaireFiltres As New UtilisateurSecondaireFiltres()
        If Session.Item("idUtilisateurSecondaire") IsNot Nothing And Session.Item("idUtilisateurSecondaire").ToString <> "0" Then
            Dim oUtilisateurSecondaire As New UtilisateurSecondaire()
            oUtilisateurSecondaire.Req_Infos_Utilisateur_Secondaire(CInt(Session.Item("idUtilisateurSecondaire").ToString), CInt(User.Identity.Name.Split(CChar("_"))(1)))
            oUtilisateurSecondaireFiltres.chargerUtilisateurSecondaireFiltres(oUtilisateurSecondaire)
        End If

        Charger_Corps(sInstructionJsPlacementFiltres, oUtilisateurSecondaireFiltres)
        ltlJsFiltres.Text = ""
        Dim downloadButtonExcel As LinkButton = CType(Page.FindControl(Me.idDownloadButtonExcel.Value), LinkButton)
        Dim downloadButtonPDF As LinkButton = CType(Page.FindControl(Me.idDownloadButtonPDF.Value), LinkButton)

        If Not Page.IsPostBack Then
            If Not downloadButtonExcel Is Nothing Then
                Dim downloadTrigger As UpdatePanelControlTrigger = New PostBackTrigger()
                downloadTrigger.ControlID = downloadButtonExcel.ID
                UpdatePanelCorps.Triggers.Add(downloadTrigger)
            End If
            If Not downloadButtonPDF Is Nothing Then
                Dim downloadTrigger As UpdatePanelControlTrigger = New PostBackTrigger()
                downloadTrigger.ControlID = downloadButtonPDF.ID
                UpdatePanelCorps.Triggers.Add(downloadTrigger)
            End If
        End If
    End Sub
    Private Sub Charger_Corps(ByRef sInstructionJsPlacementFiltres As String, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres)
        Dim oRapportsSurEtat As New Rapports
        Dim oRapportsSurOnglet As New Rapports

        Dim idModule As Integer
        Dim idUtilisateur As Integer
        Dim idRapportActuel As Integer
        Dim sListeIdRapports As String = ""

        Dim ocheckBoxFictive As New CheckBox
        ocheckBoxFictive.AutoPostBack = True
        ocheckBoxFictive.CausesValidation = True
        ocheckBoxFictive.ID = "checkboxpostback"
        ocheckBoxFictive.Attributes.Add("name", "checkboxpostback")
        ocheckBoxFictive.Attributes.Add("style", "display:none;")
        sc1.RegisterAsyncPostBackControl(ocheckBoxFictive)
        PlaceHolderBoutonArriere.Controls.Add(ocheckBoxFictive)

        Me.PossedePagination.Value = "non"
        Me.NombreNiveauxOnglets.Value = "1"
        Me.TitreTableauPresent.Value = "non"
        Me.UpdatePanelEntetes.Update()

        oGridDataBoundLists.Clear()

        idUtilisateur = CInt(User.Identity.Name.Split(CChar("_"))(0))
        idModule = CInt(Request.Item("module").ToString)

        If Me.idRapport.Value = "" Then
            oRapportsSurEtat.chargerRappportsParModuleUtilisateur(idModule, idUtilisateur, CInt(User.Identity.Name.Split(CChar("_"))(1)))
            idRapportActuel = oRapportsSurEtat.Item(0).nIdRapport
            Me.idRapport.Value = CStr(idRapportActuel)
        Else
            idRapportActuel = CInt(Me.idRapport.Value)
        End If

        Dim oRapportActuel As New Rapport
        oRapportActuel.chargerRappportParModuleUtilisateurIdRapport(idModule, idUtilisateur, idRapportActuel, CInt(User.Identity.Name.Split(CChar("_"))(1)))

        oRapportsSurEtat.Clear()
        oRapportsSurEtat.chargerRappportsParModuleUtilisateurIdRapport(idModule, idUtilisateur, idRapportActuel, CInt(User.Identity.Name.Split(CChar("_"))(1)))

        '---Eriture des logs
        If Me.idRapport.Value <> Me.IdRapportLog.Value Then
            Dim oLog As New Logs
            Dim sIp As String = Request.ServerVariables("REMOTE_ADDR")
            oLog.insertionInformationsUtilisateurDansLog(oRapportActuel.oClient.nIdClient, idUtilisateur, idModule, "", 0, "", sIp, CInt(Me.idRapport.Value), CInt(Session.Item("idUtilisateurSecondaire")))
            Me.IdRapportLog.Value = Me.idRapport.Value
        End If

        Dim iOngletOrdreNiv1 As Integer = Nothing

        Dim oBoutonArriere As New LinkButton
        oBoutonArriere.CausesValidation = True
        oBoutonArriere.Text = "Allez à la page précédente"
        oBoutonArriere.CssClass = "historyback"
        oBoutonArriere.OnClientClick = "retourArriere()"
        AddHandler oBoutonArriere.Click, AddressOf oBoutonArriere_Click
        Me.PlaceHolderBoutonArriere.Controls.Add(oBoutonArriere)

        Me.PlaceHolderTitreOnglets.Controls.Clear()
        Me.PlaceHolderDivOnglets.Controls.Clear()

        Me.ListeGridId.Value = SessionUtilisateur

        If Me.ValeurOngletNiv1Selectionne.Value = "" Then
            'Me.ValeurOngletNiv1Selectionne.Value = "tabs-Niv1-" & oRapportsSurEtat(0).nOngletOrdreNiv1 & "_" & oRapportsSurEtat(0).sOngletNiv1.Replace(" ", "")
        End If

        For Each oRapport In oRapportsSurEtat
            If oRapport.nOngletOrdreNiv1 <> iOngletOrdreNiv1 Then
                Dim oLiteral As New Literal
                Dim idDiv As String = "tabs-Niv1-" & oRapport.nOngletOrdreNiv1 & "_" & oRapport.sOngletNiv1.Replace(" ", "")

                sListeIdRapports = ""
                oRapportsSurOnglet.Clear()
                For Each oRapportTemp As Rapport In oRapportsSurEtat
                    If oRapportTemp.nSommaireOrdreNiv1 = oRapport.nSommaireOrdreNiv1 And oRapportTemp.nSommaireOrdreNiv2 = oRapport.nSommaireOrdreNiv2 And oRapportTemp.bEstActif = True And oRapportTemp.nOngletOrdreNiv1 = oRapport.nOngletOrdreNiv1 And oRapportTemp.nOngletOrdreNiv2 = oRapport.nOngletOrdreNiv2 Then
                        oRapportsSurOnglet.Add(oRapportTemp)
                        sListeIdRapports += CStr(oRapportTemp.nIdRapport) & "|"
                    End If
                Next
                sListeIdRapports = sListeIdRapports.Remove(sListeIdRapports.Length - 1, 1)

                Dim oHiddenField As New HtmlGenericControl("input")
                oHiddenField.ID = "memoryNiv1_" & oRapport.sOngletNiv1.Replace(" ", "")
                oHiddenField.Attributes.Add("type", "hidden")
                oHiddenField.Attributes.Add("value", " ")

                Me.PlaceHolderHidden.Controls.Add(oHiddenField)
                oLiteral.Text = "<li><a href=""#" & idDiv & """ onclick=""GererMemoireOnglets('" & idDiv & "','0','','oui');"">" & oRapport.sOngletNiv1 & "</a></li>"
                Me.PlaceHolderTitreOnglets.Controls.Add(oLiteral)

                Dim DIV As New HtmlGenericControl("div")
                DIV.ID = idDiv
                DIV.Attributes.Add("style", "min-height:150Px;")
                DIV.Attributes.Add("class", "ContainerGeneral")

                Me.PlaceHolderDivOnglets.Controls.Add(DIV)

                Dim bOngletNiveau2Existe As Boolean = False
                Creer_Onglets_Niveau_2(oRapport, oRapportsSurEtat, oRapportsSurOnglet, DIV, bOngletNiveau2Existe, oHiddenField, sListeIdRapports, sInstructionJsPlacementFiltres, oUtilisateurSecondaireFiltres)

                '---Ajout du contenu du rapport
                If bOngletNiveau2Existe = False Then
                    If (CType(Page.FindControl("ValeurOngletNiv1Selectionne"), HiddenField).Value = "" And oRapport.nOngletOrdreNiv1 = 1) _
                        Or CType(Page.FindControl("ValeurOngletNiv1Selectionne"), HiddenField).Value = idDiv Then
                        ChargerOnglet(oRapport, oRapportsSurEtat, oRapportsSurOnglet, DIV, sListeIdRapports, sInstructionJsPlacementFiltres, oUtilisateurSecondaireFiltres)
                    End If
                Else
                    Me.NombreNiveauxOnglets.Value = "2"
                End If

                iOngletOrdreNiv1 = oRapport.nOngletOrdreNiv1

            End If
        Next

        If Me.TriActif.Value <> "" Then
            oGrid_Trier()
            Me.TriActif.Value = ""
            Me.UpdatePanelEntetes.Update()
        End If


        oGridDataBoundLists.Clear()

        Dim myScriptMgr As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        Dim downloadButtonExcel As LinkButton = CType(Page.FindControl(Me.idDownloadButtonExcel.Value), LinkButton)
        Dim downloadButtonPDF As LinkButton = CType(Page.FindControl(Me.idDownloadButtonPDF.Value), LinkButton)



        If Not downloadButtonExcel Is Nothing Then myScriptMgr.RegisterPostBackControl(downloadButtonExcel)
        If Not downloadButtonPDF Is Nothing Then myScriptMgr.RegisterPostBackControl(downloadButtonPDF)

    End Sub
    Protected Sub oBoutonArriere_Click(ByVal sender As Object, ByVal e As EventArgs)
    End Sub
    Private Sub Creer_Onglets_Niveau_2(ByVal OrapportNiv1 As Rapport, ByVal oRapportsSurEtat As Rapports, ByVal oRapportsSurOnglet As Rapports, ByRef Div As HtmlGenericControl, ByRef bOngletNiveau2Existe As Boolean, ByRef oHiddenField As HtmlGenericControl, ByVal sListeIdRapports As String, ByRef sInstructionJsPlacementFiltres As String, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres)
        Dim bAutorisationNiveau1 As Boolean = False
        Dim bAutorisationNiveau2 As Boolean = False
        Dim DivNiv2 As New HtmlGenericControl("Div")
        DivNiv2.ID = "tabs-Niv2-" & OrapportNiv1.nOngletOrdreNiv1
        Dim Ul As New HtmlGenericControl("ul")
        Ul.Attributes.Add("class", "ulonglets")
        Div.Controls.Add(DivNiv2)
        DivNiv2.Controls.Add(Ul)
        Dim iOngletOrdreNiv2 As Integer = Nothing
        Dim TableauDiv As List(Of HtmlGenericControl) = New List(Of HtmlGenericControl)

        For Each oRapport In oRapportsSurEtat
            If OrapportNiv1.nOngletOrdreNiv1 = oRapport.nOngletOrdreNiv1 And oRapport.nOngletOrdreNiv2 <> iOngletOrdreNiv2 Then

                sListeIdRapports = ""
                oRapportsSurOnglet.Clear()
                For Each oRapportTemp As Rapport In oRapportsSurEtat
                    If oRapportTemp.nSommaireOrdreNiv1 = oRapport.nSommaireOrdreNiv1 And oRapportTemp.nSommaireOrdreNiv2 = oRapport.nSommaireOrdreNiv2 And oRapportTemp.bEstActif = True And oRapportTemp.nOngletOrdreNiv1 = oRapport.nOngletOrdreNiv1 And oRapportTemp.nOngletOrdreNiv2 = oRapport.nOngletOrdreNiv2 Then
                        oRapportsSurOnglet.Add(oRapportTemp)
                        sListeIdRapports += CStr(oRapportTemp.nIdRapport) & "|"
                    End If
                Next
                sListeIdRapports = sListeIdRapports.Remove(sListeIdRapports.Length - 1, 1)

                '---Ajout du titre
                Dim idDiv1 As String = "tabs-Niv1-" & oRapport.nOngletOrdreNiv1 & "_" & oRapport.sOngletNiv1.Replace(" ", "")
                Dim idDiv2 As String = "tabs-Niv2-" & oRapport.nOngletOrdreNiv1 & "-" & oRapport.nOngletOrdreNiv2 & "_" & oRapport.sOngletNiv2.Replace(" ", "")

                If oHiddenField.Attributes("value").ToString = " " Then
                    oHiddenField.Attributes("value") = idDiv2
                End If

                Dim oLiteral As New Literal
                oLiteral.Text = "<li><a href=""#" & idDiv2 & """ onclick=""GererMemoireOnglets('" & idDiv1 & "','" & idDiv2 & "','" & DivNiv2.ID & "')"">" & oRapport.sOngletNiv2 & "</a></li>"
                Ul.Controls.Add(oLiteral)

                Dim DIVOnglet2 As New HtmlGenericControl("div")
                DIVOnglet2.ID = idDiv2
                DivNiv2.Controls.Add(DIVOnglet2)

                Dim sValeurOngletNiv1Selectionne As String = CType(Page.FindControl("ValeurOngletNiv1Selectionne"), HiddenField).Value
                Dim sValeurOngletNiv2Selectionne As String = CType(Page.FindControl("ValeurOngletNiv2Selectionne"), HiddenField).Value
                Dim iOrdreOngletNiv1 As Integer = oRapport.nOngletOrdreNiv1
                Dim iOrdreOngletNiv2 As Integer = oRapport.nOngletOrdreNiv2

                bAutorisationNiveau1 = False
                bAutorisationNiveau2 = False
                Dim bCorrespondanceNiv1Niv2 As Boolean = True
                If (sValeurOngletNiv1Selectionne = "" And iOrdreOngletNiv1 = 1) Or sValeurOngletNiv1Selectionne = Div.ID Then
                    bAutorisationNiveau1 = True

                    If sValeurOngletNiv1Selectionne = "" Or sValeurOngletNiv2Selectionne = "" Then
                        bCorrespondanceNiv1Niv2 = False
                    Else
                        If sValeurOngletNiv2Selectionne.Split(CChar("-"))(2) <> sValeurOngletNiv1Selectionne.Split(CChar("_"))(0).Split(CChar("-"))(2) Then
                            bCorrespondanceNiv1Niv2 = False
                        End If
                    End If

                    If ((sValeurOngletNiv2Selectionne = "" Or bCorrespondanceNiv1Niv2 = False) And iOrdreOngletNiv2 = 1) Or sValeurOngletNiv2Selectionne = idDiv2 Then
                        bAutorisationNiveau2 = True
                    End If
                End If

                If bAutorisationNiveau1 = True And bAutorisationNiveau2 = True Then
                    ChargerOnglet(oRapport, oRapportsSurEtat, oRapportsSurOnglet, DIVOnglet2, sListeIdRapports, sInstructionJsPlacementFiltres, oUtilisateurSecondaireFiltres)
                End If
                iOngletOrdreNiv2 = oRapport.nOngletOrdreNiv2
                bOngletNiveau2Existe = True
            End If
        Next

        If bOngletNiveau2Existe = True Then
            Div.Controls.Add(DivNiv2)
        Else
            Div.Controls.Remove(DivNiv2)
        End If
    End Sub
    Private Sub ChargerOnglet(ByVal OrapportNiv1 As Rapport, ByVal oRapportsSurEtat As Rapports, ByVal oRapportsSurOnglet As Rapports, ByRef Div As HtmlGenericControl, ByVal sListeIdRapports As String, ByRef sInstructionJsPlacementFiltres As String, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres)
        Dim iOngletOrdreRapport As Integer = Nothing
        Dim iTailleLigne As Integer = 0
        Dim idConteneur As Integer = 0
        Dim bEstGraphique As Boolean = cDal.estGraphique(OrapportNiv1)

        Dim DivConTable As HtmlGenericControl = Nothing

        Dim DivTable As HtmlGenericControl = Nothing
        Dim DivTr As HtmlGenericControl = Nothing
        Dim DivTrPagination As HtmlGenericControl = Nothing

        Me.Title = "Oria Extranet - " & OrapportNiv1.sSommaireNiv1 & " - " & OrapportNiv1.sSommaireNiv2

        If PremierChargementOnglet.Value = "" Then
            Me.PremierChargementOnglet.Value = CStr(OrapportNiv1.nIdRapport) & ";oui"
        Else
            If Me.PremierChargementOnglet.Value.Split(CChar(";"))(0) = CStr(OrapportNiv1.nIdRapport) Then
                Me.PremierChargementOnglet.Value = CStr(OrapportNiv1.nIdRapport) & ";non"
            Else
                Me.PremierChargementOnglet.Value = CStr(OrapportNiv1.nIdRapport) & ";oui"
            End If
        End If
        Me.UpdatePanelEntetes.Update()

        Dim ListeControles As New List(Of Object)
        Dim divZoneFiltre As New HtmlGenericControl("div")
        divZoneFiltre.Attributes.Add("id", "ZoneFiltre_" & sListeIdRapports)
        divZoneFiltre.Attributes.Add("class", "ZoneFiltre")
        Div.Controls.Add(divZoneFiltre)
        Creer_Filtres(OrapportNiv1, oRapportsSurOnglet, divZoneFiltre, ListeControles, Div, sListeIdRapports, oUtilisateurSecondaireFiltres)
        Creer_Bandeau_Outils(divZoneFiltre, OrapportNiv1, bEstGraphique, oRapportsSurOnglet, Div, sListeIdRapports)
        Dim oFiltres As New Filtres
        oFiltres.chargerFiltresParRapports(oRapportsSurOnglet, OrapportNiv1, oUtilisateurSecondaireFiltres)

        Dim oCheckboxMoisGlissant As CheckBox = CType(FindControl("checkbox_MoisGlissant" & "_" & Div.ID & "--" & sListeIdRapports), CheckBox)
        UpdatePanelCorps.Update()

        Dim listeControlesIndice As UShort = 0
        ListeControles.Clear()
        For Each ofiltre In oFiltres
            If Not IsNothing(Page.FindControl("Filtre_" & Div.ID & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports)) Then
                If TypeOf Page.FindControl("Filtre_" & Div.ID & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.DropDownList Then
                    ListeControles.Add(CType(Page.FindControl("Filtre_" & Div.ID & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), DropDownList))
                ElseIf TypeOf Page.FindControl("Filtre_" & Div.ID & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.ListBox Then
                    ListeControles.Add(CType(Page.FindControl("Filtre_" & Div.ID & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), ListBox))
                End If
            End If
        Next

        '---Création des boites pour les tableaux ou graphiques
        For Each oRapport In oRapportsSurOnglet
            If OrapportNiv1.nOngletOrdreNiv1 = oRapport.nOngletOrdreNiv1 And OrapportNiv1.nOngletOrdreNiv2 = oRapport.nOngletOrdreNiv2 Then
                If iTailleLigne + oRapport.ntailleBoite > 100 Then
                    iTailleLigne = 0
                End If
                If iTailleLigne = 0 Then
                    If idConteneur > 0 Then
                        DivTable.Controls.Add(DivTr)
                        DivConTable.Controls.Add(DivTable)
                        Div.Controls.Add(DivConTable)
                    End If
                    idConteneur += 1
                    DivConTable = New HtmlGenericControl("div")
                    DivTable = New HtmlGenericControl("table")
                    DivTr = New HtmlGenericControl("tr")
                    DivTrPagination = New HtmlGenericControl("tr")

                    DivConTable.Attributes.Add("class", "ConTable")
                    DivTable.Attributes.Add("width", "100%")
                    DivTable.Attributes.Add("style", "table-layout:fixed;margin-top:15Px;")
                End If
                iTailleLigne += oRapport.ntailleBoite
                Dim DivTd As New HtmlGenericControl("td")
                DivTd.ID = "DivBoite-" & oRapport.nOngletOrdreNiv1 & "-" & oRapport.nOngletOrdreNiv2 & "-" & oRapport.nOngletOrdreRapport
                DivTd.Attributes.Add("class", "divTableau")
                DivTd.Attributes.Add("style", "width:" & oRapport.ntailleBoite & "%;")
                DivTr.Controls.Add(DivTd)

                Dim DivTdPagination As New HtmlGenericControl("td")
                DivTdPagination.ID = "DivBoitePagination-" & oRapport.nOngletOrdreNiv1 & "-" & oRapport.nOngletOrdreNiv2 & "-" & oRapport.nOngletOrdreRapport
                DivTdPagination.Attributes.Add("class", "C_divTableauPagination")
                DivTdPagination.Attributes.Add("style", "width:" & oRapport.ntailleBoite & "%;")
                DivTrPagination.Controls.Add(DivTdPagination)

                iOngletOrdreRapport = oRapport.nOngletOrdreRapport
            End If
        Next
        DivTable.Controls.Add(DivTr)
        DivTable.Controls.Add(DivTrPagination)

        If Not DivTable Is Nothing Then
            DivConTable.Controls.Add(DivTable)
            Div.Controls.Add(DivConTable)

            For Each oRapport In oRapportsSurOnglet
                If OrapportNiv1.nOngletOrdreNiv1 = oRapport.nOngletOrdreNiv1 And OrapportNiv1.nOngletOrdreNiv2 = oRapport.nOngletOrdreNiv2 Then
                    Dim divTD As HtmlGenericControl = CType(Page.FindControl("DivBoite-" & oRapport.nOngletOrdreNiv1 & "-" & oRapport.nOngletOrdreNiv2 & "-" & oRapport.nOngletOrdreRapport), HtmlGenericControl)
                    Dim DivTdPagination As HtmlGenericControl = CType(Page.FindControl("DivBoitePagination-" & oRapport.nOngletOrdreNiv1 & "-" & oRapport.nOngletOrdreNiv2 & "-" & oRapport.nOngletOrdreRapport), HtmlGenericControl)
                    If cDal.estGraphique(oRapport) = False Then
                        If oRapport.sTitreTableau <> "" Then
                            'Me.TitreTableauPresent.Value = "oui"
                            'Dim oP As New HtmlGenericControl("P")
                            'Dim oPLiteral As New Literal()
                            'oPLiteral.Text = oRapport.sTitreTableau
                            'oP.Attributes.Add("class", "titreTableau")
                            'oP.Controls.Add(oPLiteral)
                            'divTD.Controls.Add(oP)
                            Me.TitreTableauPresent.Value = "oui"
                            Dim oDiv As New HtmlGenericControl("DIV")
                            If oRapport.sTitreTableau = "BonCommandeMobileCreation" Or oRapport.sTitreTableau = "BonCommandeMobileModification" Or oRapport.sTitreTableau = "BonCommandeMobileSuppression" Then
                                Dim oBoutonTelechargement As New LinkButton
                                oBoutonTelechargement.CausesValidation = False
                                oBoutonTelechargement.ID = oRapport.sTitreTableau & sListeIdRapports
                                oBoutonTelechargement.CausesValidation = True
                                oBoutonTelechargement.Attributes.Add("idClient", oRapport.oClient.nIdClient.ToString)
                                oBoutonTelechargement.Attributes.Add("typeAction", oRapport.sTitreTableau.Replace("BonCommandeMobile", "").ToLower)
                                oBoutonTelechargement.Attributes.Add("nIdRapport", oRapport.nIdRapport.ToString)
                                oBoutonTelechargement.Text = "Envoyer le bon de commande"
                                oBoutonTelechargement.Attributes.Add("class", "export-commande")
                                AddHandler oBoutonTelechargement.Click, AddressOf TelechargerBonDeCommandeMobile
                                oDiv.Controls.Add(oBoutonTelechargement)
                                divTD.Controls.Add(oDiv)
                            ElseIf oRapport.sTitreTableau = "BonCommandeFixeCreation" Or oRapport.sTitreTableau = "BonCommandeFixeModification" Or oRapport.sTitreTableau = "BonCommandeFixeSuppression" Then
                                Dim oBoutonTelechargement As New LinkButton
                                oBoutonTelechargement.CausesValidation = False
                                oBoutonTelechargement.ID = oRapport.sTitreTableau & sListeIdRapports
                                oBoutonTelechargement.CausesValidation = True
                                oBoutonTelechargement.Attributes.Add("idClient", oRapport.oClient.nIdClient.ToString)
                                oBoutonTelechargement.Attributes.Add("typeAction", oRapport.sTitreTableau.Replace("BonCommandeFixe", "").ToLower)
                                oBoutonTelechargement.Attributes.Add("nIdRapport", oRapport.nIdRapport.ToString)
                                oBoutonTelechargement.Text = "Envoyer le bon de commande"
                                oBoutonTelechargement.Attributes.Add("class", "export-commande")
                                AddHandler oBoutonTelechargement.Click, AddressOf TelechargerBonDeCommandeFixe
                                oDiv.Controls.Add(oBoutonTelechargement)
                                divTD.Controls.Add(oDiv)
                            Else
                                Dim oPLiteral As New Literal()
                                oPLiteral.Text = oRapport.sTitreTableau
                                oDiv.Attributes.Add("class", "titreTableau")
                                oDiv.Controls.Add(oPLiteral)
                                divTD.Controls.Add(oDiv)
                            End If

                        End If
                        Creer_Gridview(oRapport, divTD, ListeControles, Div, DivTdPagination, oUtilisateurSecondaireFiltres)
                    Else
                        divTD.Attributes.Add("class", "elementGraphique")
                        Creer_Graphique(oRapport, divTD, ListeControles, Div, oUtilisateurSecondaireFiltres)
                    End If
                End If
            Next

        End If

        If Request.Item("masquer") Is Nothing Or (Not Request.Item("masquer") Is Nothing AndAlso Request.Item("masquer") <> "false") Then

            Dim oDivFictiveFooter As New HtmlGenericControl("div")
            oDivFictiveFooter.Attributes.Add("class", "DivFictiveFooter")
            Div.Controls.Add(oDivFictiveFooter)
        End If
    End Sub

    'Private Sub oGrid_Trier(sender As Object, e As EventArgs)
    '    Dim oGrid As GridView = CType(Page.FindControl(CType(sender, ImageButton).Attributes("Grid_Id")), GridView)
    '    Dim sDatafield As String = CType(sender, ImageButton).Attributes("DataField")
    '    Dim sTri As String = CType(sender, ImageButton).Attributes("stri")

    '    Recuperer_Expression_Trie(oGrid, sDatafield, sTri)

    '    Dim oDropDownListPagination As DropDownList = CType(Page.FindControl("ddl_Pagination_" & oGrid.Attributes("IDRAPPORT").ToString), DropDownList)
    '    If Not oDropDownListPagination Is Nothing Then
    '        oDropDownListPagination.SelectedValue = "1"
    '    End If

    'End Sub

    Private Sub oGrid_Trier()
        Dim oGrid As GridView = CType(Page.FindControl(Me.TriActif.Value.Split(CChar("|"))(0)), GridView)
        Dim sDatafield As String = (Me.TriActif.Value.Split(CChar("|"))(1))

        If Not oGrid Is Nothing Then
            Recuperer_Expression_Trie(oGrid, sDatafield)
            Dim oDropDownListPagination As DropDownList = CType(Page.FindControl("ddl_Pagination_" & oGrid.Attributes("IDRAPPORT").ToString), DropDownList)
            If Not oDropDownListPagination Is Nothing Then
                oDropDownListPagination.SelectedValue = "1"
            End If
        End If
    End Sub

    Private Sub Recuperer_Expression_Trie(ByVal oGrid As GridView, Optional ByVal sDatafield As String = "", Optional ByVal sTriClick As String = "", Optional ByVal bInverser As Boolean = True)
        Dim sChaineReicriture As String = ""
        Dim oTris As New Tris
        Dim sTable() As String = Me.ListeTriTableau.Value.Split(CChar("\"))
        Dim oTriDirection As New System.Web.UI.WebControls.SortDirection

        If Me.ListeTriTableau.Value <> "" Then
            For i = 0 To sTable.Length - 1 Step 3
                If i + 2 <= sTable.Length - 1 Then
                    If (oGrid.ID = sTable(i) And sDatafield = sTable(i + 1)) Or oGrid.ID <> sTable(i) Or sDatafield = "" Then
                        oTris.Add(New Tri(sTable(i), sTable(i + 1), sTable(i + 2)))
                    End If
                End If
            Next
        End If
        Dim oTriTrouve As Tri = Nothing
        Dim oTrisAnciens As New Tris
        For i = 0 To oTris.Count - 1 Step 1
            Dim otri As Tri = oTris(i)
            If otri.sIdGrid = oGrid.ID Then
                If otri.sdatafield = sDatafield Then
                    If bInverser = True Then
                        Select Case otri.sTri
                            Case "asc"
                                otri.sTri = "desc"
                            Case "desc"
                                otri.sTri = "asc"
                        End Select
                    End If
                    If otri.sTri <> "" Then
                        If Not sChaineReicriture.Contains(otri.sIdGrid + "\" + otri.sdatafield) Then
                            oTriTrouve = otri
                            sChaineReicriture += oTriTrouve.sIdGrid + "\" + oTriTrouve.sdatafield + "\" + oTriTrouve.sTri + "\"
                        End If
                    End If
                Else
                    If sDatafield = "" Then
                        If Not sChaineReicriture.Contains(otri.sIdGrid + "\" + otri.sdatafield) Then
                            oTrisAnciens.Add(New Tri(otri.sIdGrid, otri.sdatafield, otri.sTri))
                            sChaineReicriture += otri.sIdGrid + "\" + otri.sdatafield + "\" + otri.sTri + "\"
                        End If
                    End If
                End If
            Else
                If Not sChaineReicriture.Contains(otri.sIdGrid + "\" + otri.sdatafield + "\" + otri.sTri) Then
                    sChaineReicriture += otri.sIdGrid + "\" + otri.sdatafield + "\" + otri.sTri + "\"
                End If
            End If
        Next
        If oTriTrouve Is Nothing And sDatafield <> "" Then
            oTriTrouve = New Tri(oGrid.ID, sDatafield, "desc")
            oTris.Add(oTriTrouve)
            If Not sChaineReicriture.Contains(oTriTrouve.sIdGrid + "\" + oTriTrouve.sdatafield + "\" + oTriTrouve.sTri) Then
                sChaineReicriture += oTriTrouve.sIdGrid + "\" + oTriTrouve.sdatafield + "\" + oTriTrouve.sTri + "\"
            End If
        End If

        If sChaineReicriture <> "" Then sChaineReicriture = sChaineReicriture.Remove(sChaineReicriture.Length - 1, 1)
        Me.ListeTriTableau.Value = sChaineReicriture
        Me.UpdatePanelEntetes.Update()

        Dim sExpressionTri As String = ""
        If oTrisAnciens.Count > 0 Then
            For i = 0 To oTrisAnciens.Count - 2
                sExpressionTri += oTrisAnciens.Item(i).sdatafield + " " + oTrisAnciens.Item(i).sTri.ToUpper + ","
            Next
        End If

        If Not oTriTrouve Is Nothing And sDatafield <> "" Then

            oTriDirection = New System.Web.UI.WebControls.SortDirection
            Select Case oTriTrouve.sTri
                Case "asc"
                    oTriDirection = SortDirection.Ascending
                Case "desc"
                    oTriDirection = SortDirection.Descending
            End Select


            If oTrisAnciens.Count > 0 Then
                sExpressionTri += oTrisAnciens.Item(oTrisAnciens.Count() - 1).sdatafield + " " + oTrisAnciens.Item(oTrisAnciens.Count() - 1).sTri.ToUpper + ","
            End If
            sExpressionTri = oTriTrouve.sdatafield
            Me.Tri.Value = Me.ListeTriTableau.Value
            oGrid.Sort(sExpressionTri, oTriDirection)
        Else
            If oTrisAnciens.Count > 0 Then
                sExpressionTri += oTrisAnciens.Item(oTrisAnciens.Count() - 1).sdatafield
                Select Case oTrisAnciens.Item(oTrisAnciens.Count() - 1).sTri.ToUpper
                    Case "ASC"
                        oTriDirection = SortDirection.Ascending
                    Case "DESC"
                        oTriDirection = SortDirection.Descending
                End Select
                Me.Tri.Value = Me.ListeTriTableau.Value
                oGrid.Sort(sExpressionTri, oTriDirection)
            End If
        End If
    End Sub

    Private Sub Creer_Gridview(ByVal oRapport As Rapport, ByRef DivTd As HtmlGenericControl, ByRef ListeControles As List(Of Object), ByRef Div As HtmlGenericControl, ByRef DivTdPagination As HtmlGenericControl, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres)
        Dim sListeIdRapports As String = Toolbox.RecupererListeIdRapports(oRapport)
        Dim bAutorisationFooter As Boolean = False
        Dim nReferenceAnnee As Integer

        Dim oColonnes As New Colonnes
        Dim oCheckboxMoisGlissant As CheckBox = CType(FindControl("checkbox_MoisGlissant" & "_" & Div.ID & "--" & sListeIdRapports), CheckBox)
        Dim oCheckboxDecimales As CheckBox = CType(FindControl("checkboxAfficherDecimale" & "_" & Div.ID & "--" & sListeIdRapports), CheckBox)
        Dim iLargeurTableau As Integer = 0
        Dim iNombreColonnesVisibles As Integer = 0

        If Not oCheckboxMoisGlissant Is Nothing Then
            If Not Request.Form("checkbox_MoisGlissant" & "_" & Div.ID & "--" & sListeIdRapports) Is Nothing Then
                If Request.Form("checkbox_MoisGlissant" & "_" & Div.ID & "--" & sListeIdRapports) = "on" Then
                    oCheckboxMoisGlissant.Checked = True
                Else
                    oCheckboxMoisGlissant.Checked = False
                End If
            End If
        End If

        Dim sTableTri() As String = Nothing
        If Not oRapport.sTriDonnees Is Nothing Then
            sTableTri = oRapport.sTriDonnees.Split(CChar("-"))
        End If

        oColonnes.ChargerListeColonnes(oRapport, oCheckboxMoisGlissant, Div.ID, Me.BloquerColoneAction)
        If oColonnes.Count = 0 Then Exit Sub

        Dim TabNiv1(oColonnes.Count - 1) As String
        Dim TabNiv2(oColonnes.Count - 1) As String
        Dim TabNiv3(oColonnes.Count - 1) As String
        Dim TabNiv4(oColonnes.Count - 1) As String
        Dim NumColonne As Integer
        Dim iPrioriteOrdre As Integer = 1
        Dim sChaineTri As String = ""

        Dim idGrid As String = Replace("Grid_" & oRapport.sSommaireNiv1 & "_" & oRapport.sSommaireNiv2 & "_Rapport" & CStr(oRapport.nIdRapport), " ", "__").Replace("-", "ù").Replace("'", "")
        Me.ListeGridId.Value += "|" + idGrid



        'Gestion Pagination
        Dim opPagination As New HtmlGenericControl("p")
        Dim oDivPagination As New HtmlGenericControl("Div")
        oDivPagination.Controls.Add(opPagination)
        oDivPagination.Attributes.Add("class", "c_Pagination")
        Dim oDropDownListPagination As New DropDownList
        Dim oLiteralPageTotal As New Literal
        Dim oLiteralNombreEnregistrements As New Literal
        If oRapport.bAfficherPagination = True Then
            Dim oLiteral As New Literal()
            Dim sLibelleAfficherLaPage As String = ""
            If oRapport.oClient.sLangueClient = "US" Then
                sLibelleAfficherLaPage = "Show the page"
            Else
                sLibelleAfficherLaPage = "Afficher la page"
            End If
            oLiteral.Text = "<div style='float:left;margin-right:10Px;font-size:12Px;'><span style='line-height:18Px;vertical-align:top;font-size:13Px;margin:2Px 0Px;'>" & sLibelleAfficherLaPage & "&nbsp;&nbsp;</span>"
            opPagination.Controls.Add(oLiteral)
            oDropDownListPagination.AutoPostBack = True
            oDropDownListPagination.ID = "ddl_Pagination_" & CStr(oRapport.nIdRapport)
            oDropDownListPagination.Attributes.Add("onchange", "ValoriserPagination('Pagination','" & oDropDownListPagination.ID & "');")
            oDropDownListPagination.Attributes.Add("idGrid", idGrid)
            oDropDownListPagination.Width = New Unit(40, UnitType.Pixel)
            oDropDownListPagination.Attributes.Add("class", "styled")
            oDropDownListPagination.Attributes.Add("name", "ddl_Pagination_" & CStr(oRapport.nIdRapport))
            sc1.RegisterAsyncPostBackControl(oDropDownListPagination)
            AddHandler oDropDownListPagination.SelectedIndexChanged, AddressOf ddl_Pagination_SelectedIndexChanged
            opPagination.Controls.Add(oDropDownListPagination)
            opPagination.Controls.Add(oLiteralPageTotal)
        End If
        If oRapport.bAfficherNombreEnregistrements = True Then
            opPagination.Controls.Add(oLiteralNombreEnregistrements)
        End If

        Dim sBrowserName As String = ""
        Dim sBrowserVersion As String = ""
        With Request.Browser
            sBrowserName = .Browser
            sBrowserVersion &= .Version
        End With

        Dim sClasseTableau = "C_Tableau"
        If ((sBrowserName = "IE" Or sBrowserName = "InternetExplorer") And (sBrowserVersion = "6.0" Or sBrowserVersion = "7.0" Or sBrowserVersion = "8.0" Or sBrowserVersion = "9.0")) Then
            sClasseTableau = "C_Tableau CssOldIE"
        End If

        Dim oGrid As New GridView
        With oGrid
            .ID = idGrid
            .AutoGenerateColumns = False
            .EnableSortingAndPagingCallbacks = False
            .CssClass = sClasseTableau
            .ShowHeader = True
            If oRapport.bAfficherTotal = True Then bAutorisationFooter = True
            If oRapport.bAutorisationAjout = True Then bAutorisationFooter = True

            .ShowFooter = bAutorisationFooter

            If oRapport.bAfficherPagination = True Then
                .AllowPaging = True
                .PagerSettings.Visible = False
                .PageSize = 50
                Me.PossedePagination.Value = "oui"
            End If

        End With
        Me.UpdatePanelEntetes.Update()

        AddHandler oGrid.RowDataBound, AddressOf oGrid_OnRowDataBound
        AddHandler oGrid.RowCreated, AddressOf oGrid_OnRowCreated
        AddHandler oGrid.PreRender, AddressOf oGrid_PreRender
        AddHandler oGrid.PageIndexChanging, AddressOf oGrid_PageIndexChanging


        oGrid.Attributes.Add("NomID", Div.ID)
        oGrid.Attributes.Add("NomIDTD", DivTd.ID)
        oGrid.Attributes.Add("IDRAPPORT", CStr(oRapport.nIdRapport))

        NumColonne = 0
        For Each oColonne In oColonnes
            TabNiv1(NumColonne) = oColonne.sEnteteNiveau1
            TabNiv2(NumColonne) = oColonne.sEnteteNiveau2
            TabNiv3(NumColonne) = oColonne.sEnteteNiveau3
            TabNiv4(NumColonne) = oColonne.sEnteteNiveau4
            NumColonne += 1

            Dim Colonne As New BoundField
            Dim sNomColonneRetraire As String = ""
            If oColonne.sNomColonne.Contains("|") Then
                sNomColonneRetraire = oColonne.sNomColonne.Split(CChar("|"))(0)
            Else
                sNomColonneRetraire = oColonne.sNomColonne
            End If

            If oColonne.sDetailListeMois <> "" Then
                nReferenceAnnee = CInt(oColonne.sDetailListeMois.Split(CChar(":"))(0))
                Colonne.DataField = "A" & CStr(nReferenceAnnee) & "M" & Toolbox.ConvertirValeurFiltre(oColonne.sEnteteNiveau1, "Mois", oRapport.oClient)
            Else
                Colonne.DataField = sNomColonneRetraire
            End If
            Colonne.HeaderText = oColonne.sEnteteNiveau1
            Colonne.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

            Colonne.HtmlEncode = False

            '---Tri
            '6;asc-7;asc-8;desc-9;desc
            'Colonne.SortExpression = oColonne.sNomColonne
            If Not sTableTri Is Nothing Then
                For Each sCoupleTri In sTableTri
                    If CInt(sCoupleTri.Split(CChar(";"))(0)) = oColonne.nIdColonne Then
                        Select Case sCoupleTri.Split(CChar(";"))(1).ToUpper
                            Case "ASC"
                                sChaineTri += oGrid.ID + "\" + sNomColonneRetraire + "\" + "asc" + "\"
                            Case "DESC"
                                sChaineTri += oGrid.ID + "\" + sNomColonneRetraire + "\" + "desc" + "\"
                        End Select
                        iPrioriteOrdre += 1
                    End If
                Next
            End If

            '---Gestion des Colonnes Visibles
            Colonne.Visible = oColonne.bEstVisible
            If Colonne.Visible = True Then
                Dim oParamRapportColonne As New ParamRapportColonne(oRapport, oColonne)
                oParamRapportColonne.affichageParamRapportColonne()
                Colonne.Visible = oParamRapportColonne.bAfficher
            End If
            If Colonne.Visible = True Then
                iLargeurTableau += oColonne.nLargeurColonne
                iNombreColonnesVisibles += 1
            End If

            '---Gestion du Format
            If oColonne.sMefDonnees <> "" Then
                Dim sTable() As String = oColonne.sMefDonnees.Split(CChar("|"))
                Dim sCritere As String = ""
                Dim sValeur As String = ""
                For i = 0 To sTable.Length - 1
                    sCritere = sTable(i).ToString.Split(CChar(";"))(0)
                    sValeur = sTable(i).ToString.Split(CChar(";"))(1)
                    Select Case sCritere.ToUpper
                        Case "DATE"
                            Colonne.DataFormatString = sValeur
                        Case "NOMBRE", "POURCENTAGE", "PROGRESSION"
                            Colonne.DataFormatString = sValeur
                            Dim EtatCocheDecimale As String = Request.Form("checkboxAfficherDecimale" & "_" & Div.ID & "--" & sListeIdRapports)
                            If Not oCheckboxDecimales Is Nothing Then
                                If EtatCocheDecimale = "on" Or oCheckboxDecimales.Checked = True And oRapport.AfficherCaseDecimale = True Then
                                    Colonne.DataFormatString = sValeur
                                Else
                                    Colonne.DataFormatString = "{0:N0}"
                                End If
                            End If
                    End Select
                Next
            End If
            Colonne.NullDisplayText = oColonne.sSiNull

            oGrid.Columns.Add(Colonne)
        Next

        If Me.Tri.Value <> "" Then
            Me.ListeTriTableau.Value = Me.Tri.Value
        End If

        If sChaineTri <> "" Then sChaineTri = sChaineTri.Remove(sChaineTri.Length - 1, 1)
        If Not Me.ListeTriTableau.Value.Contains(oGrid.ID) Then
            If Me.ListeTriTableau.Value = "" Then
                Me.ListeTriTableau.Value = sChaineTri
            Else
                Me.ListeTriTableau.Value += "\" + sChaineTri
            End If
        End If

        Me.UpdatePanelEntetes.Update()

        oGrid.Width = New Unit(iLargeurTableau + 20, UnitType.Pixel)
        oDivPagination.Attributes.Add("style", "width:" + CStr(iLargeurTableau + 3 + iNombreColonnesVisibles) + "px;")
        DivTd.Controls.Add(oGrid)

        If oRapport.bAfficherPagination = True Or oRapport.bAfficherNombreEnregistrements = True Then
            DivTdPagination.Controls.Add(oDivPagination)
        End If


        Dim oSqlDataSource As New SqlDataSource
        Dim oCsql As New cSQL(oRapport.oClient.sBaseClient)
        oSqlDataSource.ID = "SqlDataSource" & oRapport.nIdRapport
        oSqlDataSource.ConnectionString = "Data Source=" & oCsql.serveur & ";Initial Catalog=" & oCsql.base & ";Persist Security Info=True;User ID=" & oCsql.utilisateur & ";Password=" & oCsql.password
        DivTd.Controls.Add(oSqlDataSource)
        oGrid.DataSourceID = oSqlDataSource.ID
        oGrid.Attributes.Add("oSqlDataSource", oSqlDataSource.ID)

        Binder_Gridview(oRapport, oGrid, oColonnes, ListeControles, DivTd, Div, oUtilisateurSecondaireFiltres)
        Recuperer_Expression_Trie(oGrid, "", "", False)

        If oRapport.bAfficherPagination = True Then
            Dim iNbPagesGrid As Integer = oGrid.PageCount
            For i = 1 To iNbPagesGrid
                oDropDownListPagination.Items.Add(CStr(i))
            Next
            oDropDownListPagination.SelectedValue = "1"
            'oGrid.PageIndex = 1
            For Each sTexte In Me.Pagination.Value.ToString.Split(CChar("|"))
                If sTexte.Split(CChar("="))(0) = oDropDownListPagination.ID Then
                    oDropDownListPagination.SelectedValue = sTexte.Split(CChar("="))(1)
                    oGrid.PageIndex = CInt(sTexte.Split(CChar("="))(1)) - 1
                End If
            Next
            If oRapport.oClient.sLangueClient = "US" Then
                oLiteralPageTotal.Text = "<span style='line-height:18Px;vertical-align:top;font-size:13Px;'>&nbsp;&nbsp;of&nbsp;&nbsp;" & CStr(iNbPagesGrid) & "</span></div>"
            Else
                oLiteralPageTotal.Text = "<span style='line-height:18Px;vertical-align:top;font-size:13Px;'>&nbsp;&nbsp;sur&nbsp;&nbsp;" & CStr(iNbPagesGrid) & "</span></div>"
            End If
        End If
        If oRapport.bAfficherNombreEnregistrements = True Then
            Dim oDataview As DataView = CType(oSqlDataSource.Select(DataSourceSelectArguments.Empty), DataView)

            Dim iNbEnregistrementsGrid As Integer = oDataview.Count
            oLiteralNombreEnregistrements.Text = ""
            Dim sMargin As String = ""
            'If oRapport.bAfficherPagination = True Then
            '    sMargin = "margin-left:30px;"
            'End If
            'oLiteralNombreEnregistrements.Text = "<br/><span style='" + sMargin + "line-height:18Px;font-weight:bold;font-size:12px;vertical-align:top;'>" & CStr(iNbEnregistrementsGrid)
            oLiteralNombreEnregistrements.Text = "<div style='float:left;'><div class='arrondi' style='float:left;'><span style='" + sMargin + "line-height:18Px;font-weight:bold;font-size:12px;vertical-align:top;color:#FFFFFF !important;color:#000000;'>" & CStr(iNbEnregistrementsGrid)
            If iNbEnregistrementsGrid > 1 Then
                oLiteralNombreEnregistrements.Text += " </span></div><span style='line-height:18Px;vertical-align:top;font-size:13Px;'>&nbsp;enregistrements</span></div>"
            Else
                oLiteralNombreEnregistrements.Text += " </span></div><span style='line-height:18Px;vertical-align:top;font-size:13Px;'>&nbsp;enregistrement</span></div>"
            End If
        End If

        If oRapport.bAutorisationAjout = True Then
            ChargerLigneAjout(oRapport, oColonnes, oGrid)
        End If


    End Sub

    Private Sub ChargerLigneAjout(ByVal oRapport As Rapport, ByVal oColonnes As Colonnes, ByRef oGrid As GridView)
        '---Gestion Ligne Ajout
        If oRapport.bAutorisationAjout = True And Me.BloquerLigneAjout.Value = "" And Me.ReferenceModificationLigne.Value = "" Then

            Dim TableVisibilite(oColonnes.Count - 1) As Boolean
            Dim iIndexColonne As Integer = 0
            Dim bEstVisiblle As Boolean
            For Each oColonne In oColonnes
                bEstVisiblle = oColonne.bEstVisible
                If bEstVisiblle = True Then
                    Dim oParamRapportColonne As New ParamRapportColonne(oRapport, oColonne)
                    oParamRapportColonne.affichageParamRapportColonne()
                    bEstVisiblle = oParamRapportColonne.bAfficher
                End If
                TableVisibilite(iIndexColonne) = bEstVisiblle
                iIndexColonne += 1
            Next

            Dim iDerniereColonneVisible As Integer = 0
            For i = TableVisibilite.Count - 1 To 0 Step -1
                If TableVisibilite(i) = True Then
                    iDerniereColonneVisible = i
                    Exit For
                End If
            Next


            oGrid.ShowFooter = True

            Try
                oGrid.FooterRow.TableSection = TableRowSection.TableFooter
            Catch
            End Try
            Dim sListeControleInsert As String = ""
            Dim sListeColonnesInsert As String = ""

            oGrid.FooterStyle.CssClass = "LigneInsert"


            For i = 0 To oGrid.Columns.Count - 1
                Select Case oColonnes(i).sControleModification.ToUpper
                    Case "LISTE"
                        Dim oControle As New DropDownList
                        oControle.ID = "DropdownList_Insert" & "_" & oRapport.nIdRapport & "-" & oColonnes(i).nIdColonne
                        'Dim sListeValeurs As New List(Of String)
                        cDal.RecupererListeValeursTable(oControle, oRapport.oClient.sBaseClient, oColonnes(i).sTableModification, oColonnes(i).sChampModification, oColonnes(i).sFiltreModification)
                        'For Each sTexte In sListeValeurs
                        '    oControle.Items.Add(sTexte)
                        'Next
                        oControle.Attributes.Add("class", "styled")
                        oControle.Attributes.Add("name", oControle.ID)
                        oControle.Width = New Unit(80, UnitType.Percentage)
                        oControle.Height = New Unit(20, UnitType.Pixel)
                        Dim oP As New HtmlGenericControl("p")
                        oP.Controls.Add(oControle)
                        oGrid.FooterRow.Cells(i).Controls.Add(oP)
                        sListeControleInsert += oControle.ID + "|"
                        sListeColonnesInsert += oColonnes(i).sNomColonne + "|"
                    Case "TEXTE"
                        Dim oControle As New TextBox
                        oControle.ID = "Textbox_Insert" & "_" & oRapport.nIdRapport & "-" & oColonnes(i).nIdColonne
                        oControle.TextMode = TextBoxMode.SingleLine
                        oControle.Width = New Unit(96, UnitType.Percentage)
                        oControle.Attributes.Add("onChange", "Supprimer_Balises_Input(this)")
                        oControle.Attributes.Add("style", "font-size:12Px;color:#206797;")
                        oControle.Height = New Unit(20, UnitType.Pixel)
                        oControle.CssClass = "InsertText"
                        oGrid.FooterRow.Cells(i).Controls.Add(oControle)
                        sListeControleInsert += oControle.ID + "|"
                        sListeColonnesInsert += oColonnes(i).sNomColonne + "|"
                        If oColonnes(i).sTypeDonnees.ToLower = "nombre" Or oColonnes(i).sTypeDonnees.ToLower = "nombrebalise" Then
                            oControle.Text = "0"
                        End If
                    Case "MULTITEXTE"
                        Dim oControle As New TextBox
                        oControle.ID = "Textbox_Insert" & "_" & oRapport.nIdRapport & "-" & oColonnes(i).nIdColonne
                        oControle.TextMode = TextBoxMode.MultiLine
                        oControle.Width = New Unit(96, UnitType.Percentage)
                        oControle.Attributes.Add("onChange", "Supprimer_Balises_Input(this)")
                        oControle.Attributes.Add("style", "font-size:12Px;color:#206797;")
                        oControle.Height = New Unit(60, UnitType.Pixel)
                        oControle.CssClass = "InsertText"
                        oGrid.FooterRow.Cells(i).Controls.Add(oControle)
                        sListeControleInsert += oControle.ID + "|"
                        sListeColonnesInsert += oColonnes(i).sNomColonne + "|"
                        If oColonnes(i).sTypeDonnees.ToLower = "nombre" Or oColonnes(i).sTypeDonnees.ToLower = "nombrebalise" Then
                            oControle.Text = "0"
                        End If
                    Case "CHECKBOX"
                        Dim oControle As New CheckBox
                        oControle.ID = "Checkbox_Insert" & "_" & oRapport.nIdRapport & "-" & oColonnes(i).nIdColonne
                        oControle.Height = New Unit(20, UnitType.Pixel)
                        'oControle.CssClass = "InsertText"
                        oGrid.FooterRow.Cells(i).Controls.Add(oControle)
                        sListeControleInsert += oControle.ID + "|"
                        sListeColonnesInsert += oColonnes(i).sNomColonne + "|"
                End Select


                If oColonnes(i).sTypeDonnees.ToUpper = "ACTION" Then
                    If sListeControleInsert <> "" Then sListeControleInsert = sListeControleInsert.Remove(sListeControleInsert.Length - 1, 1)
                    If sListeColonnesInsert <> "" Then sListeColonnesInsert = sListeColonnesInsert.Remove(sListeColonnesInsert.Length - 1, 1)


                    Dim oBoutonImage As New ImageButton
                    oBoutonImage.ID = "BtnValidationAjout" & "_" & CStr(oRapport.nIdRapport)
                    oBoutonImage.CssClass = "boutonValidationAjout"
                    oBoutonImage.CausesValidation = True
                    oBoutonImage.Attributes.Add("sGridId", oGrid.ID)
                    oBoutonImage.Attributes.Add("sListeControleInsert", sListeControleInsert)
                    oBoutonImage.Attributes.Add("sRapport", CStr(oRapport.nIdRapport))
                    oBoutonImage.Attributes.Add("sListeColonnesInsert", sListeColonnesInsert)
                    oBoutonImage.Attributes.Add("sBase", oRapport.oClient.sBaseClient)
                    oBoutonImage.Attributes.Add("sTable", oRapport.sTableAction)
                    oBoutonImage.ToolTip = "Ajouter"
                    oBoutonImage.ImageUrl = "images/icones/Validation.png"
                    AddHandler oBoutonImage.Click, AddressOf BtnValidationAjout_Click
                    sc1.RegisterAsyncPostBackControl(oBoutonImage)
                    oGrid.FooterRow.Cells(i).Controls.Add(oBoutonImage)
                End If


                If iDerniereColonneVisible = i Then
                    oGrid.FooterRow.Cells(i).Attributes.Add("style", "min-width:" & CStr(oColonnes(i).nLargeurColonne + 20) & "Px;")
                Else
                    oGrid.FooterRow.Cells(i).Attributes.Add("style", "min-width:" & (oColonnes(i).nLargeurColonne).ToString & "Px;")

                End If

            Next

            oGridRowFooter = oGrid.FooterRow
        Else
            Try
                oGrid.FooterRow.Visible = False
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub BtnValidationEdit_Click(sender As Object, e As EventArgs)
        Dim oBoutonImage As ImageButton
        oBoutonImage = CType(sender, ImageButton)
        Dim sListeControle As String = oBoutonImage.Attributes("sListeControleEdit").ToString
        Dim sListeColonnes As String = oBoutonImage.Attributes("sListeColonnesEdit").ToString
        Dim sGridId As String = oBoutonImage.Attributes("sGridId").ToString
        Dim sBase As String = oBoutonImage.Attributes("sBase").ToString
        Dim sTable As String = oBoutonImage.Attributes("sTable").ToString
        Dim sRapport = oBoutonImage.Attributes("sRapport").ToString
        Dim sCritere As String = oBoutonImage.Attributes("sCritere").ToString
        Dim nNumeroLigne As Integer = CInt(oBoutonImage.Attributes("sNumeroLigne").ToString)

        Dim oUtilisateurSecondaireFiltres As New UtilisateurSecondaireFiltres()
        If Session.Item("idUtilisateurSecondaire") IsNot Nothing And Session.Item("idUtilisateurSecondaire").ToString <> "0" Then
            Dim oUtilisateurSecondaire As New UtilisateurSecondaire()
            oUtilisateurSecondaire.Req_Infos_Utilisateur_Secondaire(CInt(Session.Item("idUtilisateurSecondaire").ToString), CInt(User.Identity.Name.Split(CChar("_"))(1)))
            oUtilisateurSecondaireFiltres.chargerUtilisateurSecondaireFiltres(oUtilisateurSecondaire)
        End If


        InsertEdit(sGridId, sBase, sRapport, sTable, sListeControle, sListeColonnes, sCritere, "Edition", nNumeroLigne, oUtilisateurSecondaireFiltres)

    End Sub

    Private Sub BtnValidationAjout_Click(sender As Object, e As EventArgs)
        Dim oBoutonImage As ImageButton
        oBoutonImage = CType(sender, ImageButton)
        Dim sListeControle As String = oBoutonImage.Attributes("sListeControleInsert").ToString
        Dim sListeColonnes As String = oBoutonImage.Attributes("sListeColonnesInsert").ToString
        Dim sGridId As String = oBoutonImage.Attributes("sGridId").ToString
        Dim sBase As String = oBoutonImage.Attributes("sBase").ToString
        Dim sTable As String = oBoutonImage.Attributes("sTable").ToString
        Dim sRapport = oBoutonImage.Attributes("sRapport").ToString

        Dim oUtilisateurSecondaireFiltres As New UtilisateurSecondaireFiltres()
        If Session.Item("idUtilisateurSecondaire") IsNot Nothing And Session.Item("idUtilisateurSecondaire").ToString <> "0" Then
            Dim oUtilisateurSecondaire As New UtilisateurSecondaire()
            oUtilisateurSecondaire.Req_Infos_Utilisateur_Secondaire(CInt(Session.Item("idUtilisateurSecondaire").ToString), CInt(User.Identity.Name.Split(CChar("_"))(1)))
            oUtilisateurSecondaireFiltres.chargerUtilisateurSecondaireFiltres(oUtilisateurSecondaire)
        End If


        InsertEdit(sGridId, sBase, sRapport, sTable, sListeControle, sListeColonnes, "", "Insertion", 0, oUtilisateurSecondaireFiltres)

    End Sub

    Private Sub InsertEdit(ByVal sGridId As String, ByVal sBase As String, ByVal sRapport As String, ByVal sTable As String, ByVal sListeControle As String, ByVal sListeColonnes As String, ByVal sCritere As String, ByVal sTypeAction As String, ByVal nNumeroLigne As Integer, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres)
        Dim ogrid As GridView = CType(FindControl(sGridId), GridView)
        Dim idModule As Integer
        Dim oModule As New Modul
        Dim oUtilisateur As New Utilisateur()
        Dim oRapport As New Rapport
        Dim oColonnes As New Colonnes


        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))
        idModule = CInt(Request.Item("module").ToString)
        oModule.chargerModulesParId(idModule, oUtilisateur.oClient)
        oRapport.chargerRappportParModuleUtilisateurIdRapport(oModule.nIdModule, oUtilisateur.nIdUtilisateur, CInt(sRapport), oUtilisateur.oClient.nIdClient)

        Dim Id_Div As String = ogrid.Attributes("NomID").ToString
        Dim sListeIdRapports As String = ""
        Dim oRapports As New Rapports
        oRapports.chargerRappportsParModuleUtilisateurIdRapport(oRapport.oModule.nIdModule, oRapport.oUtilisateur.nIdUtilisateur, oRapport.nIdRapport, oUtilisateur.oClient.nIdClient)

        sListeIdRapports = ""
        For Each oRapportTemp In oRapports
            If oRapport.nOngletOrdreNiv1 = oRapportTemp.nOngletOrdreNiv1 And oRapport.nOngletOrdreNiv2 = oRapportTemp.nOngletOrdreNiv2 Then
                sListeIdRapports += CStr(oRapportTemp.nIdRapport) & "|"
            End If
        Next
        sListeIdRapports = sListeIdRapports.Remove(sListeIdRapports.Length - 1, 1)

        Dim oCheckboxMoisGlissant As CheckBox = CType(FindControl("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports), CheckBox)
        If Not Page.Request("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports) Is Nothing Then
            If Page.Request("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports) = "on" Then
                oCheckboxMoisGlissant.Checked = True
            Else
                oCheckboxMoisGlissant.Checked = False
            End If
        End If

        oColonnes.ChargerListeColonnes(oRapport, oCheckboxMoisGlissant, Id_Div, Me.BloquerColoneAction)

        Dim sListeValeurs As String = ""

        Dim iCompteur As Integer = 0
        For Each sLibelle In sListeColonnes.Split(CChar("|"))
            For j As Integer = 0 To ogrid.Columns.Count - 1
                If CType(ogrid.Columns(j), BoundField).DataField = sLibelle Then
                    If sLibelle = "idClient" Then
                        sListeValeurs += oUtilisateur.oClient.nIdClient.ToString + "|"
                    Else
                        Select Case sListeControle.Split(CChar("|"))(iCompteur).Split(CChar("_"))(0)
                            Case "DropdownList"
                                If sTypeAction = "Insertion" Then
                                    sListeValeurs += CType(ogrid.FooterRow.Cells(j).Controls(0).Controls(0), DropDownList).SelectedValue.Replace("|", "_") + "|"
                                Else
                                    sListeValeurs += CType(ogrid.Rows(nNumeroLigne).Cells(j).Controls(0).Controls(0), DropDownList).SelectedValue.Replace("|", "_") + "|"
                                End If
                            Case "Textbox"
                                Dim oColonneTemp = From oColonne In oColonnes Where oColonne.sNomColonne = sLibelle
                                Select Case CType(oColonneTemp(0), Colonne).sTypeDonnees
                                    Case "nombre", "nombrebalise"
                                        If sTypeAction = "Insertion" Then
                                            If Not IsNumeric(CType(ogrid.FooterRow.Cells(j).Controls(0), TextBox).Text.Replace("|", "_").Replace(".", ",")) Then
                                                Me.Notification.Value = "Le champ " & CType(oColonneTemp(0), Colonne).sEnteteNiveau1 & " doit contenir une valeur numérique"
                                            End If
                                        Else
                                            If Not IsNumeric(CType(ogrid.Rows(nNumeroLigne).Cells(j).Controls(0), TextBox).Text.Replace("|", "_").Replace(".", ",")) Then
                                                Me.Notification.Value = "Le champ " & CType(oColonneTemp(0), Colonne).sEnteteNiveau1 & " doit contenir une valeur numérique"
                                            End If
                                        End If
                                End Select
                                If sTypeAction = "Insertion" Then
                                    sListeValeurs += CType(ogrid.FooterRow.Cells(j).Controls(0), TextBox).Text.Replace("|", "_") + "|"
                                Else
                                    sListeValeurs += CType(ogrid.Rows(nNumeroLigne).Cells(j).Controls(0), TextBox).Text.Replace("|", "_") + "|"
                                End If
                            Case "Checkbox"
                                If sTypeAction = "Insertion" Then
                                    sListeValeurs += CType(ogrid.FooterRow.Cells(j).Controls(0), CheckBox).Checked.ToString + "|"
                                Else
                                    sListeValeurs += CType(ogrid.Rows(nNumeroLigne).Cells(j).Controls(0), CheckBox).Checked.ToString + "|"
                                End If
                        End Select
                    End If
                End If
            Next
            iCompteur += 1
        Next
        If sListeValeurs <> "" Then sListeValeurs = sListeValeurs.Remove(sListeValeurs.Length - 1, 1)


        If Me.ReferenceInsert.Value <> "" Then
            If Me.ReferenceInsert.Value.Contains("=") Then
                If sListeColonnes <> "" Then
                    sListeColonnes += "|" + Me.ReferenceInsert.Value.Split(CChar("="))(0)
                Else
                    sListeColonnes += Me.ReferenceInsert.Value.Split(CChar("="))(0)
                End If
                If sListeValeurs <> "" Then
                    sListeValeurs += "|" + Me.ReferenceInsert.Value.Split(CChar("="))(1)
                Else
                    sListeValeurs += Me.ReferenceInsert.Value.Split(CChar("="))(1)
                End If
            End If
        End If

        If Me.Notification.Value = "" Then
            Dim TestModification As Boolean = False
            If Not oRapport.sTestModification Is Nothing Then
                Select Case sTypeAction
                    Case "Edition"
                        TestModification = cDal.testerModification(sBase, oRapport.sTestModification, sListeColonnes, sListeValeurs, sCritere)
                    Case "Insertion"
                        TestModification = cDal.testerModification(sBase, oRapport.sTestInsertion, sListeColonnes, sListeValeurs, "")
                End Select
            End If

            If TestModification = True Then
                '---On gère les avertissements

            End If



            Select Case sTypeAction
                Case "Edition"
                    If TestModification = False Then
                        cDal.editionValeurs(sBase, sTable, sListeColonnes, sListeValeurs, sCritere)
                        Me.ReferenceModificationLigne.Value = ""
                    Else
                        Me.Notification.Value = "Vous ne pouvez pas appliquer cette modification car un enregistrement identique existe."
                    End If
                Case "Insertion"
                    If TestModification = False Then
                        cDal.insertionValeurs(sBase, sTable, sListeColonnes, sListeValeurs)
                    Else
                        Me.Notification.Value = "Vous ne pouvez pas ajouter cet enregistrement car il existe déjà."
                    End If

            End Select

            If oRapport.sObjetMailing <> "" Then
                Dim sObjet As String = oRapport.oClient.sNomClient & " : " & "Creation " & oRapport.sObjetMailing
                Dim sCorps As String = ""

                Dim sTableColonnes() As String = sListeColonnes.Split(CChar("|"))
                Dim sTableValeurs() As String = sListeValeurs.Split(CChar("|"))

                Dim i As Integer

                For i = 0 To sTableColonnes.Count - 1
                    Dim oColonne As Colonne = oColonnes.Find(Function(p) p.sNomColonne = sTableColonnes(i))
                    If oColonne Is Nothing Then
                        sCorps += sTableColonnes(i) & ": " & sTableValeurs(i) + CStr(Chr(10))
                    Else
                        sCorps += oColonne.sEnteteNiveau1 & ": " & sTableValeurs(i) + CStr(Chr(10))
                    End If
                Next

                sCorps += CStr(Chr(10)) + "Merci de ne pas répondre à ce message. Les mails envoyés à cette adresse ne recevront pas de réponse."
                Dim oEmail As New Email
                For Each oMailing In oUtilisateur.oClient.oMailings
                    Email.envoiEmail(sObjet, sCorps, oMailing.mail)
                Next
            End If
        End If

        Dim oRapportsTraitement As New Rapports
        For Each oRapportActuel In oRapports
            If oRapport.nOngletOrdreNiv1 = oRapportActuel.nOngletOrdreNiv1 And oRapport.nOngletOrdreNiv2 = oRapportActuel.nOngletOrdreNiv2 Then
                oRapportsTraitement.Add(oRapport)
            End If
        Next
        Dim oFiltres As New Filtres
        oFiltres.chargerFiltresParRapports(oRapportsTraitement, oRapport, oUtilisateurSecondaireFiltres)
        Dim ListeControles As New List(Of Object)
        Dim listeControlesIndice As UShort = 0
        For Each ofiltre In oFiltres
            If Not IsNothing(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports)) Then
                If TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.DropDownList Then
                    ListeControles.Add(CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), DropDownList))
                ElseIf TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.ListBox Then
                    ListeControles.Add(CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), ListBox))
                End If
            End If
        Next



        Binder_Gridview(oRapport, ogrid, oColonnes, ListeControles, CType(Page.FindControl(ogrid.Attributes("NomIDTD").ToString), HtmlGenericControl), CType(Page.FindControl(ogrid.Attributes("NomID").ToString), HtmlGenericControl), oUtilisateurSecondaireFiltres)
        'ogrid.DataBind()
        ChargerLigneAjout(oRapport, oColonnes, ogrid)
    End Sub

    Protected Sub UpdatePanel_Unload(ByVal sender As Object, ByVal e As EventArgs)
        Me.RegisterUpdatePanel(TryCast(sender, UpdatePanel))
    End Sub

    Public Sub RegisterUpdatePanel(ByVal upnl As UpdatePanel)
        Dim arrObj() As Object = {upnl}
        For Each methInfo As System.Reflection.MethodInfo In GetType(ScriptManager).GetMethods(System.Reflection.BindingFlags.NonPublic Or System.Reflection.BindingFlags.Instance)
            If methInfo.Name.Equals("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel") Then
                If methInfo.IsStatic = True Then
                    methInfo.Invoke(ScriptManager.GetCurrent(upnl.Page), arrObj)
                End If
            End If
        Next
    End Sub

    Private Sub ddl_Pagination_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        'Dim oGrid As GridView = CType(Page.FindControl(CType(sender, DropDownList).Attributes("idGrid").ToString), GridView)
        'oGrid.PageIndex = CInt(CType(sender, DropDownList).SelectedValue)
    End Sub

    Private Sub oGrid_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        CType(sender, GridView).PageIndex = e.NewPageIndex
        CType(sender, GridView).DataBind()
    End Sub

    Private Sub Creer_Graphique(ByRef oRapport As Rapport, ByRef divTD As HtmlGenericControl, ByRef ListeControles As List(Of Object), ByRef Div As HtmlGenericControl, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres)
        Try
            Dim sListeIdRapports As String = Toolbox.RecupererListeIdRapports(oRapport)

            Dim oCheckboxTTC As CheckBox = CType(Div.FindControl("checkbox_TTC_" & Div.ID & "--" & sListeIdRapports), CheckBox)
            Dim oCheckboxMoisGlissant As CheckBox = CType(FindControl("checkbox_MoisGlissant" & "_" & Div.ID & "--" & sListeIdRapports), CheckBox)
            If Not Page.Request("checkbox_MoisGlissant" & "_" & Div.ID & "--" & sListeIdRapports) Is Nothing Then
                If Page.Request("checkbox_MoisGlissant" & "_" & Div.ID & "--" & sListeIdRapports) = "on" Then
                    oCheckboxMoisGlissant.Checked = True
                Else
                    oCheckboxMoisGlissant.Checked = False
                End If
            End If

            Dim oParamRapportAffichageGraphiques As New ParamRapportAffichageGraphiques()
            oParamRapportAffichageGraphiques.recupererListeParamRapportAffichageGraphique(oRapport)

            If oParamRapportAffichageGraphiques.Count <> 0 Then
                Dim oChart As New Chart()
                oChart.Visible = True
                Dim oChartArea As New ChartArea("ChartArea_" & oParamRapportAffichageGraphiques(0).oRapport.ToString)
                'oChartArea.Position.X = 0
                'oChartArea.Position.Y = 0
                'oChartArea.Position.Height = 100
                'oChartArea.Position.Width = 100
                oChart.ChartAreas.Add(oChartArea)

                Dim nTailleChart As Integer = 0

                ' POUR CHAQUE IDSERIE (1 AFFICHAGEGRAPHIQUE = 1 SERIE), JE BOUCLE POUR POUVOIR RAJOUTER LA SERIE DANS LE GRAPHIQUE CORRESPONDANT AU RAPPORT
                For Each el As ParamRapportAffichageGraphique In oParamRapportAffichageGraphiques
                    Dim bPresenceListeMois As Boolean = False
                    Dim sIndiceMois(0) As String
                    Dim nTempIndiceMois As UShort = 0
                    Dim sformatDonneeOrdonnee As String = ""

                    Dim oColonnes As New Colonnes()
                    oColonnes.ChargerListeColonnesGraphique(oRapport, oCheckboxMoisGlissant, Div.ID, el.oAffichageGraphique.oSerie)

                    'TENIR COMPTE DE LISTE MOIS
                    For Each Col As Colonne In oColonnes
                        If Col.nIdColonne > 100000 Then
                            bPresenceListeMois = True
                            If nTempIndiceMois = sIndiceMois.Length Then
                                ReDim Preserve sIndiceMois(nTempIndiceMois)
                            End If

                            sIndiceMois(nTempIndiceMois) = Col.sNomColonne
                            nTempIndiceMois += CUShort(1)
                        End If
                    Next

                    'UNIQUEMENT POUR LA RECUPERATION DE LA LEGENDE EN ABSCISSE
                    Dim sTmpAbscisseLegende() As String = Split(el.oAffichageGraphique.sAbscisse, ";")
                    Dim sLegendeAbscisse(0) As String
                    Dim sLegendeClaireAbscisse(0) As String
                    Dim j As UShort = 0
                    For Each sIndexColonne As String In sTmpAbscisseLegende
                        Dim oColTmp As Colonne = oColonnes.Find(Function(p) p.nIdColonne = CInt(sIndexColonne))
                        If Not oColTmp Is Nothing Then
                            If sLegendeAbscisse.Length = j Then
                                ReDim Preserve sLegendeAbscisse(sLegendeAbscisse.Length)
                            End If
                            If sLegendeClaireAbscisse.Length = j Then
                                ReDim Preserve sLegendeClaireAbscisse(sLegendeClaireAbscisse.Length)
                            End If
                            sLegendeAbscisse(j) = oColTmp.sNomColonne.Split(CChar("|"))(0)
                            sLegendeClaireAbscisse(j) = oColTmp.sEnteteNiveau1.Split(CChar("|"))(0)
                            j = CUShort(j + 1)
                        End If
                    Next

                    Dim sNomSerie As String = ""
                    For Each sNomCol As String In sLegendeClaireAbscisse
                        If sNomSerie <> "" Then
                            sNomSerie += " "
                        End If
                        sNomSerie += sNomCol
                    Next

                    Dim sOrdonneeRecherchee As String = ""
                    Dim oCheckboxDecimales As CheckBox = CType(FindControl("checkboxAfficherDecimale" & "_" & Div.ID & "--" & sListeIdRapports), CheckBox)
                    If bPresenceListeMois = False Then
                        Dim oColTmp As Colonne = oColonnes.Find(Function(p) p.nIdColonne = CInt(el.oAffichageGraphique.sOrdonnee))
                        sOrdonneeRecherchee = oColTmp.sNomColonne.Split(CChar("|"))(0)
                        If oColTmp.sMefDonnees.Split(CChar(";")).Count > 0 Then
                            If Not oCheckboxDecimales Is Nothing Then
                                If Request.Form("checkboxAfficherDecimale" & "_" & Div.ID & "--" & sListeIdRapports) = "on" Or oCheckboxDecimales.Checked = True And oRapport.AfficherCaseDecimale = True Then
                                    sformatDonneeOrdonnee = oColTmp.sMefDonnees.Split(CChar(":"))(1).Split(CChar("}"))(0)
                                Else
                                    sformatDonneeOrdonnee = "N0"
                                End If
                            End If
                        End If

                        oColTmp = Nothing
                    Else
                        'Récupérer le format pour les listes mois
                        Dim oColonne As New Colonne()
                        oColonne = oColonne.RecupererFormatListeMois(el.oAffichageGraphique.oSerie)
                        If Not oCheckboxDecimales Is Nothing Then
                            If Request.Form("checkboxAfficherDecimale" & "_" & Div.ID & "--" & sListeIdRapports) = "on" Or oCheckboxDecimales.Checked = True And oRapport.AfficherCaseDecimale = True Then
                                sformatDonneeOrdonnee = oColonne.sMefDonnees.Split(CChar(":"))(1).Split(CChar("}"))(0)
                            Else
                                sformatDonneeOrdonnee = "N0"
                            End If
                        End If
                    End If

                    Dim oSqlDataSource As New SqlDataSource
                    Dim oCsql As New cSQL(oRapport.oClient.sBaseClient)
                    oSqlDataSource.ID = "SqlDataSource" & oRapport.nIdRapport
                    oSqlDataSource.ConnectionString = "Data Source=" & oCsql.serveur & ";Initial Catalog=" & oCsql.base & ";Persist Security Info=True;User ID=" & oCsql.utilisateur & ";Password=" & oCsql.password
                    oSqlDataSource.SelectCommand = cDal.Creer_Requete_Tableau(oRapport, oColonnes, ListeControles, Div, oCheckboxTTC, oCheckboxMoisGlissant, Me.OutilTop.Value, Me.ListeTriTableau.Value, Me.TriActif.Value, Me.BloquerColoneAction, oUtilisateurSecondaireFiltres)(0)

                    Dim dt As New DataTable()
                    Dim cmd As New SqlCommand(oSqlDataSource.SelectCommand)

                    cmd.Connection = oCsql.connexion
                    cmd.CommandType = CommandType.Text

                    Dim sda As New SqlDataAdapter()
                    sda.SelectCommand = cmd
                    sda.Fill(dt)

                    Dim i As UShort = 0
                    Dim nPointReferenceDetachement As Integer = Nothing
                    Dim nValeurReferenceDetachement As Double = Nothing
                    Dim sXaxes() As String
                    Dim nYaxes() As Object

                    If bPresenceListeMois = True Then
                        'ReDim sXaxes(sIndiceMois.Length - 1)
                        'ReDim nYaxes(sIndiceMois.Length - 1)
                        ReDim sXaxes(0)
                        ReDim nYaxes(0)
                    Else
                        ReDim sXaxes(dt.Rows.Count - 1)
                        ReDim nYaxes(dt.Rows.Count - 1)
                    End If

                    'PERMETTRA DE CALCULER LES 10% POUR LES GESTIONS SPECIFIQUES DES VALEURS FAIBLES
                    Dim nValeurTotaleOrdonnees As Double = 0
                    Dim bValeurInferieur10Pourcent As Boolean = False
                    Dim nValeurSeuil As Double = 0
                    If sOrdonneeRecherchee <> "" Then
                        For Each dr As DataRow In dt.Rows
                            nValeurTotaleOrdonnees += Convert.ToDouble(dr(sOrdonneeRecherchee).ToString)
                        Next
                    End If
                    If el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.bChangementCasValeurFaible = True Then
                        nValeurSeuil = CDbl(nValeurTotaleOrdonnees * 10 / 100)
                    End If

                    If dt.Rows.Count > 0 Then
                        For Each dr As DataRow In dt.Rows

                            Dim sValeurLegendeAbscisse As String = ""
                            Dim nValeurOrdonne As Object = Nothing
                            Dim sLegendeSeriePourListeMois As String = ""
                            'If Not sNomSerie Is Nothing And sNomSerie <> "" Then
                            '    sLegendeSeriePourListeMois = Convert.ToString(dr(sNomSerie))
                            'End If

                            If Not sLegendeAbscisse(0) Is Nothing And sLegendeAbscisse(0) <> "" And el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "ColonneMultiserie" Then
                                For nIndiceLegendeAbscisse As Integer = 0 To (sLegendeAbscisse.Count - 1)
                                    If nIndiceLegendeAbscisse > 0 Then
                                        sLegendeSeriePourListeMois += " "
                                    End If
                                    sLegendeSeriePourListeMois += Convert.ToString(dr(sLegendeAbscisse(nIndiceLegendeAbscisse)))
                                Next
                            ElseIf Not sLegendeAbscisse(0) Is Nothing And sLegendeAbscisse(0) <> "" Then
                                sLegendeSeriePourListeMois = Convert.ToString(dr(sLegendeAbscisse(0)))
                            End If

                            If bPresenceListeMois = True Then
                                ReDim nYaxes(sIndiceMois.Length - 1)
                            End If

                            For nIndiceMois = 0 To (sIndiceMois.Length - 1) Step 1

                                ' Si Liste de mois, je prend dans le tableau d'indice mois
                                If bPresenceListeMois = True Then
                                    sValeurLegendeAbscisse = Toolbox.VerifierEnteteLibelleMois("LISTEMOIS", CInt(sIndiceMois(nIndiceMois).Split(CChar("M"))(1)), oRapport.oClient)
                                    'If Not dr(sIndiceMois(nIndiceMois)) Is DBNull.Value Then
                                    If nIndiceMois = sXaxes.Length Then
                                        ReDim Preserve sXaxes(nIndiceMois)
                                        ReDim Preserve nYaxes(nIndiceMois)
                                    End If
                                    If dr(sIndiceMois(nIndiceMois)) Is DBNull.Value Then
                                        nValeurOrdonne = Nothing
                                    Else
                                        nValeurOrdonne = CDec(dr(sIndiceMois(nIndiceMois)))
                                        nYaxes.SetValue(nValeurOrdonne, nIndiceMois)
                                    End If

                                    sXaxes.SetValue(sValeurLegendeAbscisse, nIndiceMois)


                                    oChartArea.AxisX.Interval = 1

                                    nTailleChart = CInt(nTailleChart + ((oParamRapportAffichageGraphiques(0).oAffichageGraphique.oGraphique.nLargeurParPoint) / dt.Rows.Count))
                                    'End If
                                Else
                                    If Not dr(sLegendeAbscisse(0)) Is DBNull.Value Then
                                        For Each elAbscisse In sLegendeAbscisse
                                            If sValeurLegendeAbscisse <> "" Then
                                                sValeurLegendeAbscisse = sValeurLegendeAbscisse & " / "
                                            End If
                                            sValeurLegendeAbscisse += CStr(dr(elAbscisse))
                                        Next
                                    Else
                                        sValeurLegendeAbscisse = ""
                                    End If

                                    nValeurOrdonne = dr(sOrdonneeRecherchee)

                                    'Test Niveau Inferieur A 10%
                                    If el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.bChangementCasValeurFaible = True AndAlso bValeurInferieur10Pourcent = False AndAlso CDbl(nValeurOrdonne) <= nValeurSeuil Then
                                        bValeurInferieur10Pourcent = True
                                    End If

                                    sXaxes.SetValue(sValeurLegendeAbscisse, i)
                                    nYaxes.SetValue(nValeurOrdonne, i)

                                    If Not el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurDetachee Is Nothing Then
                                        If el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurDetachee.ToUpper = "MAX" Then
                                            If nValeurReferenceDetachement = Nothing OrElse CDbl(nValeurOrdonne) > nValeurReferenceDetachement Then
                                                nValeurReferenceDetachement = CDbl(nValeurOrdonne)
                                                nPointReferenceDetachement = i
                                            End If
                                        ElseIf el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurDetachee.ToUpper = "MIN" Then
                                            If nValeurReferenceDetachement = Nothing OrElse CDbl(nValeurOrdonne) < nValeurReferenceDetachement Then
                                                nValeurReferenceDetachement = CDbl(nValeurOrdonne)
                                                nPointReferenceDetachement = i
                                            End If
                                        End If
                                    End If
                                    i = CUShort(i + 1)

                                    nTailleChart += oParamRapportAffichageGraphiques(0).oAffichageGraphique.oGraphique.nLargeurParPoint
                                End If
                            Next

                            If bPresenceListeMois = True Then
                                If oParamRapportAffichageGraphiques.Count > 1 Then
                                    sLegendeSeriePourListeMois = el.oAffichageGraphique.oSerie.sLegendeTexte
                                ElseIf sLegendeSeriePourListeMois = "" Then
                                    sLegendeSeriePourListeMois = "Serie_" & el.oRapport.nIdRapport & "_" & el.oAffichageGraphique.oSerie.nIdSerie.ToString
                                End If
                                Dim oSeries As New Series(sLegendeSeriePourListeMois)
                                oChart.Series.Add(oSeries.Name)
                                oChart.Series(oSeries.Name).Points.DataBindXY(sXaxes, nYaxes)
                                oChart.Series(oSeries.Name).ToolTip = sLegendeSeriePourListeMois & " : #VALY{" & sformatDonneeOrdonnee & "}"
                                oSeries.EmptyPointStyle.IsVisibleInLegend = False
                                oSeries.EmptyPointStyle.IsVisibleInLegend = False

                                DefinitionTypeGraphique(el, oSeries, oChart, bValeurInferieur10Pourcent, oChartArea, nPointReferenceDetachement)

                                If el.oAffichageGraphique.oSerie.bLabelSurGraphique = True Then
                                    MiseEnFormeValeursPoints(oChart.Series(oSeries.Name).Points, sformatDonneeOrdonnee, el.oAffichageGraphique.oSerie)
                                Else
                                    oChart.Series(oSeries.Name).IsValueShownAsLabel = False
                                End If
                                oChart.Series(oSeries.Name).ToolTip = sLegendeClaireAbscisse(0) & " #VALX : #VALY{" & sformatDonneeOrdonnee & "}"
                            End If

                        Next

                        If bPresenceListeMois = True And el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "ColonneMultiserie" Then
                            CreationLegendeGraphique(el, oChart, oChartArea, bPresenceListeMois, Split(el.oAffichageGraphique.sAbscisse, ";"), oParamRapportAffichageGraphiques.Count, oRapport.oClient.nIdClient)
                        ElseIf bPresenceListeMois = False Then
                            If el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "ColonneMultiserie" Then

                                CreationLegendeGraphique(el, oChart, oChartArea, bPresenceListeMois, Split(el.oAffichageGraphique.sAbscisse, ";"), oParamRapportAffichageGraphiques.Count, oRapport.oClient.nIdClient)

                                For i = 0 To CUShort(sXaxes.Count - 1) Step 1
                                    Dim oSeries As New Series(sXaxes(i).ToString)
                                    oChart.Series.Add(oSeries.Name)
                                    oSeries.Legend = sXaxes(i).ToString

                                    Dim sXaxesTemp(0) As String
                                    sXaxesTemp.SetValue(oRapport.sSommaireNiv2, 0)
                                    Dim nYaxesTemp(0) As Double
                                    nYaxesTemp.SetValue(CDbl(Replace(nYaxes(i).ToString, ".", ",")), 0)

                                    oChart.Series(oSeries.Name).Points.DataBindXY(sXaxesTemp, nYaxesTemp)
                                    oChart.Series(oSeries.Name).ChartType = SeriesChartType.Column
                                    oChart.Series(oSeries.Name).IsXValueIndexed = True

                                    oChart.Series(oSeries.Name).ToolTip = sLegendeClaireAbscisse(0) & " : #VALY{" & sformatDonneeOrdonnee & "}"
                                Next

                            Else
                                Dim oSeries As New Series(sNomSerie)
                                oChart.Series.Add(oSeries.Name)
                                oChart.Series(oSeries.Name).Points.DataBindXY(sXaxes, nYaxes)
                                oChart.Series(oSeries.Name).IsXValueIndexed = True

                                MiseEnFormeValeursPoints(oChart.Series(oSeries.Name).Points, sformatDonneeOrdonnee, el.oAffichageGraphique.oSerie)
                                DefinitionTypeGraphique(el, oSeries, oChart, bValeurInferieur10Pourcent, oChartArea, nPointReferenceDetachement)
                                oChart.Series(oSeries.Name).ToolTip = sLegendeClaireAbscisse(0) & " #VALX : #VALY{" & sformatDonneeOrdonnee & "}"
                                If el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "Camembert" Then
                                    oChart.Series(oSeries.Name)("CollectedThreshold") = "5"
                                    oChart.Series(oSeries.Name)("CollectedThresholdUsePercent") = "true"
                                    If el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurLabel.Contains("#PERCENT") Then
                                        oChart.Series(oSeries.Name)("CollectedLabel") = el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurLabel
                                    Else
                                        oChart.Series(oSeries.Name)("CollectedLabel") = "#VALY{" & sformatDonneeOrdonnee & "}"
                                    End If
                                    oChart.Series(oSeries.Name)("CollectedColor") = "#D0D0D0"
                                    oChart.Series(oSeries.Name)("CollectedLegendText") = "Autres"
                                End If
                            End If
                        End If

                        ' *** TITRE POUR L'AXE DES ABSCISSES ET ORDONNEES *** '
                        If el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique <> "Camembert" Then
                            If el.oAffichageGraphique.sTitreAbscisse <> Nothing Then
                                oChart.ChartAreas(oChartArea.Name).AxisX.Title = el.oAffichageGraphique.sTitreAbscisse
                                oChart.ChartAreas(oChartArea.Name).AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 12)
                                oChart.ChartAreas(oChartArea.Name).AxisX.TitleAlignment = Drawing.StringAlignment.Center
                            End If

                            If el.oAffichageGraphique.sTitreOrdonnee <> Nothing Then
                                oChart.ChartAreas(oChartArea.Name).AxisY.Title = el.oAffichageGraphique.sTitreOrdonnee
                                oChart.ChartAreas(oChartArea.Name).AxisY.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 12)
                                oChart.ChartAreas(oChartArea.Name).AxisY.TitleAlignment = Drawing.StringAlignment.Center
                            End If
                        Else
                            Dim title As New Title(el.oAffichageGraphique.sTitreOrdonnee, Docking.Left, New System.Drawing.Font("Microsoft Sans Serif", 12), Drawing.Color.Black)
                            oChart.Titles.Add(title)
                        End If

                        oChartArea.AxisX.Interval = 1
                        'oChart.BorderlineWidth = 1
                        oChart.BackColor = Drawing.ColorTranslator.FromHtml("#F1")
                        oChart.TextAntiAliasingQuality = TextAntiAliasingQuality.Normal
                        oChart.ChartAreas(0).BackColor = Drawing.Color.Transparent
                        oChart.ChartAreas(0).AxisX.MajorGrid.LineColor = Drawing.ColorTranslator.FromHtml("#999")
                        oChart.ChartAreas(0).AxisY.MajorGrid.LineColor = Drawing.ColorTranslator.FromHtml("#999")
                        'oChart.ChartAreas(0).AxisX.MajorGrid.Enabled = False
                        'oChart.ChartAreas(0).AxisY.MajorGrid.Enabled = False

                        If el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "Camembert" Or el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "StackedColumn" Or oParamRapportAffichageGraphiques.Count > 1 Then
                            CreationLegendeGraphique(el, oChart, oChartArea, bPresenceListeMois, Split(el.oAffichageGraphique.sAbscisse, ";"), oParamRapportAffichageGraphiques.Count, oRapport.oClient.nIdClient)
                        End If

                        Dim AjoutTailleLegendeGraphique As Integer = oParamRapportAffichageGraphiques(0).oAffichageGraphique.oGraphique.nTailleLegende
                        If bValeurInferieur10Pourcent = False Then
                            oChart.ChartAreas(oChartArea.Name).Area3DStyle.Enable3D = oParamRapportAffichageGraphiques(0).oAffichageGraphique.oGraphique.bAffichage3d
                        Else
                            oChart.ChartAreas(oChartArea.Name).Area3DStyle.Enable3D = True
                            AjoutTailleLegendeGraphique = CInt(AjoutTailleLegendeGraphique + (AjoutTailleLegendeGraphique / 2))
                        End If

                        If oChart.ChartAreas(oChartArea.Name).Area3DStyle.Enable3D = True Or bValeurInferieur10Pourcent = True Then
                            oChart.ChartAreas(oChartArea.Name).Area3DStyle.Inclination = oParamRapportAffichageGraphiques(0).oAffichageGraphique.oGraphique.nInclination3D
                            oChart.ChartAreas(oChartArea.Name).Area3DStyle.Rotation = oParamRapportAffichageGraphiques(0).oAffichageGraphique.oGraphique.nRotation3D
                        End If

                        oChart.Width = CType((nTailleChart + AjoutTailleLegendeGraphique) / oParamRapportAffichageGraphiques.Count, Unit)
                        If el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "Camembert" Then
                            If (nTailleChart + AjoutTailleLegendeGraphique) / oParamRapportAffichageGraphiques.Count < 400 Then
                                oChart.Width = 400
                            End If
                        End If
                    End If
                Next
                If oChart.Series.Count > 0 Then

                    Dim sCheminImage As String = Server.MapPath("\tempimg\U" & User.Identity.Name.Split(CChar("_"))(0) & "_M" & CStr(oRapport.oModule.nIdModule) & "_R" & CStr(oRapport.nIdRapport) & ".png")
                    If File.Exists(sCheminImage) Then
                        File.Delete(sCheminImage)
                    End If

                    oChart.ImageType = ChartImageType.Png
                    oChart.SaveImage(sCheminImage)

                    divTD.Controls.Add(oChart)
                End If
            End If
            'cDal.insertionTestOptimisation(System.Reflection.MethodBase.GetCurrentMethod.Name & "_Fin")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub MiseEnFormeValeursPoints(ByRef dpCollection As DataPointCollection, ByVal sformatDonneeOrdonnee As String, ByVal oSerie As Serie)
        Try
            Dim dpDeletePoints As New List(Of DataPoint)
            Dim bCountainsPercent As Boolean = False
            If Not oSerie.oAffichagePersonnaliseGraphique.sValeurLabel Is Nothing Then
                bCountainsPercent = oSerie.oAffichagePersonnaliseGraphique.sValeurLabel.Contains("#PERCENT")
            End If

            For Each point As DataPoint In dpCollection
                If oSerie.oTypeGraphique.stypeGraphique = "Camembert" And point.YValues(point.YValues.Count - 1) = 0 Then
                    dpDeletePoints.Add(point)
                Else
                    If point.IsEmpty = False And bCountainsPercent = False Then
                        point.Label = point.YValues(0).ToString(sformatDonneeOrdonnee)
                        point.LegendText = Convert.ToString(point.AxisLabel)
                    End If
                End If
            Next

            For Each deletePoints In dpDeletePoints
                dpCollection.Remove(deletePoints)
            Next

            dpDeletePoints = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DefinitionTypeGraphique(ByRef el As ParamRapportAffichageGraphique, ByRef oSeries As Series, ByRef oChart As Chart, ByVal bValeurInferieur10Pourcent As Boolean, ByRef oChartArea As ChartArea, ByVal nPointReferenceDetachement As Integer)
        If el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "Camembert" Then
            oChart.ChartAreas(0).BackColor = Drawing.ColorTranslator.FromHtml("#F1")
            oChart.Series(oSeries.Name).ChartType = SeriesChartType.Pie

            'If ((Not el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurExterieure = Nothing And el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurExterieure = True)) _
            '    Or (bValeurInferieur10Pourcent = True) Then
            '    oChart.Series(oSeries.Name)("PieLabelStyle") = "Outside"
            'End If
            If Not el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurDetachee Is Nothing Then
                oChart.Series(oSeries.Name).Points(nPointReferenceDetachement)("Exploded") = "true"
            End If
            If Not el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurLegende Is Nothing Then
                oChart.Series(oSeries.Name).LegendText = el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurLegende
            End If
            If Not el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurLabel Is Nothing Then
                oChart.Series(oSeries.Name).Label = el.oAffichageGraphique.oSerie.oAffichagePersonnaliseGraphique.sValeurLabel
            Else
                oChart.Series(oSeries.Name)("PieLabelStyle") = "Disabled"
            End If
            oChart.ChartAreas(oChartArea.Name).BackColor = Drawing.ColorTranslator.FromHtml("#F1")
        ElseIf el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "StackedColumn" Then
            oChart.Series(oSeries.Name).ChartType = SeriesChartType.StackedColumn
        ElseIf el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "ColonneSimple" Then
            oChart.Series(oSeries.Name).ChartType = SeriesChartType.Column
        ElseIf el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "Linéaire" Then
            oChart.Series(oSeries.Name).ChartType = SeriesChartType.Line
        ElseIf el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique = "Courbe" Then
            oChart.Series(oSeries.Name).ChartType = SeriesChartType.Spline
            oChart.Series(oSeries.Name).BorderWidth = 4
        End If
    End Sub

    Private Sub CreationLegendeGraphique(ByRef el As ParamRapportAffichageGraphique, ByRef oChart As Chart, ByRef oChartArea As ChartArea, ByVal bPresenceListeMois As Boolean, ByVal sTabAbscisse As String(), ByVal nNbSeries As Integer, ByVal nIdClient As Integer)
        'Je détermine le nom de la légende
        Dim oColonne As New Colonne(Convert.ToInt32(sTabAbscisse(0)))
        Dim sLegendeName As String = el.oAffichageGraphique.oLegende.sNomLegende & "_" & el.oAffichageGraphique.oSerie.nIdSerie.ToString
        oColonne.RecupererEnteteNiv1(nIdClient)

        oChart.Legends.Add(sLegendeName)

        If nNbSeries > 1 Then
            oChart.Legends(sLegendeName).Title = el.oAffichageGraphique.oLegende.sNomLegende
        Else
            oChart.Legends(sLegendeName).Title = oColonne.sEnteteNiveau1
        End If

        Select Case el.oAffichageGraphique.oLegende.sPosition.ToUpper
            Case "BOTTOM"
                oChart.Legends(sLegendeName).Docking = Docking.Bottom
                oChart.Legends(sLegendeName).Alignment = Drawing.StringAlignment.Center
            Case "LEFT"
                oChart.Legends(sLegendeName).Docking = Docking.Left
            Case "TOP"
                oChart.Legends(sLegendeName).Docking = Docking.Top
                oChart.Legends(sLegendeName).Alignment = Drawing.StringAlignment.Center
            Case Else
                oChart.Legends(sLegendeName).Docking = Docking.Right
        End Select
        'If (bPresenceListeMois) = True Or (el.oAffichageGraphique.oSerie.oTypeGraphique.stypeGraphique) = "Camembert" Then
        '    oChart.Legends(sLegendeName).Title = oColonne.sEnteteNiveau1
        'End If

        oChart.Legends(sLegendeName).DockedToChartArea = oChartArea.Name
        oChart.Legends(sLegendeName).IsDockedInsideChartArea = False
        oChart.Legends(sLegendeName).InterlacedRows = False

        oColonne = Nothing
    End Sub


    Private Sub Binder_Gridview(ByVal oRapport As Rapport, ByRef oGrid As GridView, ByVal oColonnes As Colonnes, ByRef ListeControles As List(Of Object), ByVal divtd As HtmlGenericControl, ByVal Div As HtmlGenericControl, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres)
        Dim sListeIdRapports As String = Toolbox.RecupererListeIdRapports(oRapport)

        Dim oCheckboxTTC As CheckBox = CType(Div.FindControl("checkbox_TTC_" & Div.ID & "--" & sListeIdRapports), CheckBox)
        Dim oCheckboxMoisGlissant As CheckBox = CType(FindControl("checkbox_MoisGlissant" & "_" & Div.ID & "--" & sListeIdRapports), CheckBox)

        Dim oSqlDataSource As SqlDataSource = CType(Page.FindControl("SqlDataSource" & oRapport.nIdRapport), SqlDataSource)




        Dim sRequete As String = cDal.Creer_Requete_Tableau(oRapport, oColonnes, ListeControles, Div, oCheckboxTTC, oCheckboxMoisGlissant, Me.OutilTop.Value, Me.ListeTriTableau.Value, Me.TriActif.Value, Me.BloquerColoneAction, oUtilisateurSecondaireFiltres)(0)
        If oRapport.bAutorisationAjout = True Then
            If cDal.CompterEnregistrements(oRapport, sRequete) = True Then
                oSqlDataSource.SelectCommand = sRequete
            Else
                oSqlDataSource.SelectCommand = cDal.Creer_Requete_Tableau(oRapport, oColonnes, ListeControles, Div, oCheckboxTTC, oCheckboxMoisGlissant, Me.OutilTop.Value, Me.ListeTriTableau.Value, Me.TriActif.Value, Me.BloquerColoneAction, oUtilisateurSecondaireFiltres)(2)
            End If
        Else
            oSqlDataSource.SelectCommand = sRequete
        End If

        'oSqlDataSource.SelectCommand = cDal.Creer_Requete_Tableau(oRapport, oColonnes, ListeControles, Div, oCheckboxTTC, oCheckboxMoisGlissant)(0)
        oSqlDataSource.DataBind()
        oGrid.DataBind()

    End Sub

    Private Sub oGrid_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        Dim oGrid As GridView = CType(sender, GridView)

        Dim oRapport As New Rapport()
        Dim oColonnes As New Colonnes
        Dim idModule As String = ""
        Dim oUtilisateur As New Utilisateur()
        Dim Table_Id() As String = CType(sender, GridView).ID.Split(CChar("t"))
        Dim idRapport As Integer = CInt(Table_Id(Table_Id.Length - 1))

        Dim i As Integer

        idModule = CStr(CInt(Request.Item("module").ToString))
        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))
        oRapport.chargerRappportParModuleUtilisateurIdRapport(CInt(idModule), oUtilisateur.nIdUtilisateur, idRapport, oUtilisateur.oClient.nIdClient)

        Dim Id_Div As String = CType(sender, GridView).Attributes("NomID").ToString

        Dim sListeIdRapports As String = ""
        Dim oRapports As New Rapports
        oRapports.chargerRappportsParModuleUtilisateurIdRapport(oRapport.oModule.nIdModule, oRapport.oUtilisateur.nIdUtilisateur, oRapport.nIdRapport, oUtilisateur.oClient.nIdClient)

        sListeIdRapports = ""
        For Each oRapportTemp In oRapports
            If oRapport.nOngletOrdreNiv1 = oRapportTemp.nOngletOrdreNiv1 And oRapport.nOngletOrdreNiv2 = oRapportTemp.nOngletOrdreNiv2 Then
                sListeIdRapports += CStr(oRapportTemp.nIdRapport) & "|"
            End If
        Next
        sListeIdRapports = sListeIdRapports.Remove(sListeIdRapports.Length - 1, 1)

        Dim oCheckboxMoisGlissant As CheckBox = CType(FindControl("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports), CheckBox)
        If Not Page.Request("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports) Is Nothing Then
            If Page.Request("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports) = "on" Then
                oCheckboxMoisGlissant.Checked = True
            Else
                oCheckboxMoisGlissant.Checked = False
            End If
        End If

        oColonnes.ChargerListeColonnes(oRapport, oCheckboxMoisGlissant, Id_Div, Me.BloquerColoneAction)
        '---Gestion des mois glissants
        Dim LibelleRetraite As String = ""
        For i = 0 To oGrid.Columns.Count - 1

            If oColonnes(i).sNomColonne.Contains("|") Then
                LibelleRetraite = oColonnes(i).sNomColonne.Split(CChar("|"))(0)
            Else
                LibelleRetraite = oColonnes(i).sNomColonne
            End If
            oGrid.Columns(i).HeaderText = oColonnes(i).sEnteteNiveau1
            CType(oGrid.Columns(i), BoundField).DataField = LibelleRetraite
        Next

        Toolbox.Fusionner_Gridview(oGrid, oColonnes, oRapport.bAutoriserFusionColonnes)

        oGrid.UseAccessibleHeader = True
        If Not oGrid.HeaderRow Is Nothing Then
            oGrid.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

        If Not oGridRowFooter Is Nothing Then
            oGrid.FooterRow.TableSection = TableRowSection.TableFooter
            For i = 0 To oGridRowFooter.Cells.Count - 1
                If oGridRowFooter.Cells(i).Controls.Count > 0 Then
                    oGrid.FooterRow.Cells(i).Controls.Add(oGridRowFooter.Cells(i).Controls(0))
                End If

                Dim iLargeurColonne As Integer = CInt(oColonnes.Find(Function(p) p.sNomColonne = CType(oGrid.Columns(i), BoundField).DataField).nLargeurColonne)
                If i = oGrid.Columns.Count - 1 Then
                    iLargeurColonne += 20
                End If
                oGrid.FooterRow.Cells(i).Width = New Unit(iLargeurColonne, UnitType.Pixel)
            Next

        End If


    End Sub

    Private Sub oGrid_OnRowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then
            Creer_Entetes(sender, e)
        End If
    End Sub

    Private Sub Creer_Entetes(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        Dim oGrid As GridView = CType(sender, GridView)
        Dim Table_Id() As String = CType(sender, GridView).ID.Split(CChar("t"))
        Dim idRapport As Integer = CInt(Table_Id(Table_Id.Length - 1))
        Dim idModule As String = ""
        Dim oRapport As New Rapport()
        Dim oColonnes As New Colonnes
        Dim sAffichage As String = ""
        Dim oListeRapports As New Rapports
        Dim sListeIdRapports As String = ""

        Dim i As Integer

        idModule = CStr(CInt(Request.Item("module").ToString))
        oListeRapports.chargerRappportsParModuleUtilisateur(CInt(Request.Item("module")), CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))
        oRapport = oListeRapports.Find(Function(p) p.nIdRapport = idRapport)

        sListeIdRapports = ""
        For Each oRapportTemp As Rapport In oListeRapports
            If oRapportTemp.nSommaireOrdreNiv1 = oRapport.nSommaireOrdreNiv1 And oRapportTemp.nSommaireOrdreNiv2 = oRapport.nSommaireOrdreNiv2 And oRapportTemp.bEstActif = True And oRapportTemp.nOngletOrdreNiv1 = oRapport.nOngletOrdreNiv1 And oRapportTemp.nOngletOrdreNiv2 = oRapport.nOngletOrdreNiv2 Then
                sListeIdRapports += CStr(oRapportTemp.nIdRapport) & "|"
            End If
        Next
        sListeIdRapports = sListeIdRapports.Remove(sListeIdRapports.Length - 1, 1)

        Dim Id_Div As String = CType(sender, GridView).Attributes("NomID").ToString
        Dim oCheckboxMoisGlissant As CheckBox = CType(FindControl("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports), CheckBox)
        oColonnes.ChargerListeColonnes(oRapport, oCheckboxMoisGlissant, Id_Div, Me.BloquerColoneAction)

        Dim LigneHeader As GridViewRow = Nothing
        Dim TableVisibilite(oColonnes.Count - 1) As Boolean
        Dim iIndexColonne As Integer = 0
        Dim bEstVisiblle As Boolean
        For Each oColonne In oColonnes
            bEstVisiblle = oColonne.bEstVisible
            If bEstVisiblle = True Then
                Dim oParamRapportColonne As New ParamRapportColonne(oRapport, oColonne)
                oParamRapportColonne.affichageParamRapportColonne()
                bEstVisiblle = oParamRapportColonne.bAfficher
            End If
            TableVisibilite(iIndexColonne) = bEstVisiblle
            iIndexColonne += 1
        Next

        Dim iDerniereColonneVisible As Integer = 0
        For i = TableVisibilite.Count - 1 To 0 Step -1
            If TableVisibilite(i) = True Then
                iDerniereColonneVisible = i
                Exit For
            End If
        Next

        iIndexColonne = 0
        For iNiveau = 2 To 4
            LigneHeader = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            LigneHeader.TableSection = TableRowSection.TableHeader
            iIndexColonne = 0
            For Each ocolonne In oColonnes
                Dim oCellule As New TableCell()
                oCellule.Text = ""
                Select Case iNiveau
                    Case 2
                        oCellule.Text = ocolonne.sEnteteNiveau2
                        If oCellule.Text = "" Then oCellule.Text = ocolonne.sEnteteNiveau1
                    Case 3
                        oCellule.Text = ocolonne.sEnteteNiveau3
                        If oCellule.Text = "" Then oCellule.Text = ocolonne.sEnteteNiveau2
                        If oCellule.Text = "" Then oCellule.Text = ocolonne.sEnteteNiveau1
                    Case 4
                        oCellule.Text = ocolonne.sEnteteNiveau4
                        If oCellule.Text = "" Then oCellule.Text = ocolonne.sEnteteNiveau3
                        If oCellule.Text = "" Then oCellule.Text = ocolonne.sEnteteNiveau2
                        If oCellule.Text = "" Then oCellule.Text = ocolonne.sEnteteNiveau1
                        e.Row.Cells(iIndexColonne).CssClass = "C_entete_" + ocolonne.sclasseCellule
                        e.Row.Cells(iIndexColonne).Width = New Unit(ocolonne.nLargeurColonne, UnitType.Pixel)
                End Select
                oCellule.HorizontalAlign = HorizontalAlign.Center
                oCellule.ColumnSpan = 1
                oCellule.RowSpan = 1
                oCellule.CssClass = "C_entete_" + ocolonne.sclasseCellule

                If iDerniereColonneVisible = iIndexColonne Then
                    oCellule.Attributes.Add("style", "min-width:" & CStr(ocolonne.nLargeurColonne + 20) & "Px;")
                Else
                    oCellule.Attributes.Add("style", "min-width:" & (ocolonne.nLargeurColonne).ToString & "Px;")
                End If

                If TableVisibilite(iIndexColonne) = False Then
                    oCellule.Visible = False
                End If
                LigneHeader.Cells.Add(oCellule)
                iIndexColonne += 1

            Next
            oGrid.Controls(0).Controls.AddAt(0, LigneHeader)
        Next
        Me.UpdatePanelEntetes.Update()

        '---Fusion Horizontale
        For iNiveau = 2 To 0 Step -1
            Dim oLigne As GridViewRow = CType(oGrid.Controls(0).Controls(iNiveau), GridViewRow)
            For i = oLigne.Cells.Count() - 2 To 0 Step -1
                If oLigne.Cells(i).Text = oLigne.Cells(i + 1).Text Then
                    oLigne.Cells(i).ColumnSpan = oLigne.Cells(i + 1).ColumnSpan + 1
                    oLigne.Cells(i + 1).Visible = False
                End If
            Next
        Next

        '---Fusion Verticale
        Dim sTexteCompareLigne As String = ""
        Dim sTexteCompareLigneSuivante As String = ""
        For iNiveau = 2 To 0 Step -1
            Dim oLigne As GridViewRow = CType(oGrid.Controls(0).Controls(iNiveau), GridViewRow)
            Dim oLigneSuivante As GridViewRow

            If iNiveau = 2 Then
                oLigneSuivante = e.Row
            Else
                oLigneSuivante = CType(oGrid.Controls(0).Controls(iNiveau + 1), GridViewRow)
            End If

            For i = 0 To CType(oGrid.Controls(0).Controls(0), GridViewRow).Cells.Count - 1
                If Toolbox.ConvertirHtml(oLigne.Cells(i).Text) = Toolbox.ConvertirHtml(oLigneSuivante.Cells(i).Text) Then
                    oLigne.Cells(i).RowSpan = If(oLigneSuivante.Cells(i).RowSpan < 2, 2, oLigneSuivante.Cells(i).RowSpan + 1)
                    oLigneSuivante.Cells(i).Visible = False
                    Dim sEnteteNiveau1 As String = oLigne.Cells(i).Text
                    Dim bAjouterLien As Boolean = False
                    For Each oColonne In oColonnes
                        If oColonne.sEnteteNiveau1 = sEnteteNiveau1 And oColonne.sNomColonne.Split(CChar("|"))(0) = CType(oGrid.Columns(i), BoundField).DataField Then
                            bAjouterLien = True
                        End If
                    Next
                    Dim sNomColonne As String = CType(oGrid.Columns(i), BoundField).DataField.ToString
                    Dim oColonneTemp As Colonne = oColonnes.Find(Function(p) p.sNomColonne.Split(CChar("|"))(0) = sNomColonne)
                    If bAjouterLien = True And oColonneTemp.bEstTriAutorise Then
                        Dim oliteral As New Literal
                        oLigne.Cells(i).Attributes.Add("sNomColonne", CType(oGrid.Columns(i), BoundField).DataField)
                        oLigne.Cells(i).Attributes.Add("nIdColonne", CStr(oColonneTemp.nIdColonne))
                        oliteral.Text = oLigne.Cells(i).Text
                        oLigne.Cells(i).Controls.Add(oliteral)

                        Dim oTris As New Tris
                        Dim sTable() As String = Me.ListeTriTableau.Value.Split(CChar("\"))
                        If Me.ListeTriTableau.Value <> "" Then
                            For j = 0 To sTable.Length - 1 Step 3
                                If j + 2 <= sTable.Length - 1 Then
                                    oTris.Add(New Tri(sTable(j), sTable(j + 1), sTable(j + 2)))
                                End If
                            Next
                        End If
                        Dim sTri As String = ""
                        For Each oTri In oTris
                            If oTri.sIdGrid = oGrid.ID And oTri.sdatafield = CType(oGrid.Columns(i), BoundField).DataField Then
                                Select Case oTri.sTri
                                    Case "asc"
                                        sTri = "desc"
                                    Case "desc"
                                        sTri = "asc"
                                End Select
                            End If
                        Next

                        Dim oImageButton As New ImageButton

                        Select Case sTri
                            Case ""
                                oImageButton.ImageUrl = "~/images/placeholder.gif"
                            Case "asc"
                                oImageButton.ImageUrl = "~/images/asc.gif"
                            Case "desc"
                                oImageButton.ImageUrl = "~/images/desc.gif"
                        End Select

                        oImageButton.ID = "imagetri_" + oGrid.ID + "_" + CStr(i)
                        oImageButton.Attributes.Add("name", oImageButton.ID)
                        oImageButton.Attributes.Add("Grid_Id", oGrid.ID)
                        oImageButton.Attributes.Add("DataField", CType(oGrid.Columns(i), BoundField).DataField)
                        oImageButton.Attributes.Add("SensTri", "")
                        oImageButton.Attributes.Add("UpdatePanelId", oGrid.Attributes("NomIDUPDATEPANEL"))
                        oImageButton.CssClass = "C_image_tri"
                        'AddHandler oImageButton.Click, AddressOf oGrid_Trier
                        oImageButton.Attributes.Add("onclick", "NotifierTri('" & oGrid.ID & "','" & CType(oGrid.Columns(i), BoundField).DataField & "');")
                        oLigne.Cells(i).Controls.Add(oImageButton)
                    End If

                End If

                If iNiveau = 2 And oLigneSuivante.Cells(i).Visible = True Then
                    Dim oliteral As New Literal
                    Dim oColonneTemp As Colonne = oColonnes.Find(Function(p) p.sNomColonne.Split(CChar("|"))(0) = CType(oGrid.Columns(i), BoundField).DataField)
                    oLigneSuivante.Cells(i).Attributes.Add("sNomColonne", CType(oGrid.Columns(i), BoundField).DataField)
                    oLigne.Cells(i).Attributes.Add("nIdColonne", CStr(oColonneTemp.nIdColonne))
                    oliteral.Text = oLigneSuivante.Cells(i).Text
                    oLigneSuivante.Cells(i).Controls.Add(oliteral)

                    Dim oTris As New Tris
                    Dim sTable() As String = Me.ListeTriTableau.Value.Split(CChar("\"))
                    If Me.ListeTriTableau.Value <> "" Then
                        For j = 0 To sTable.Length - 1 Step 3
                            Try
                                oTris.Add(New Tri(sTable(j), sTable(j + 1), sTable(j + 2)))
                            Catch
                            End Try
                        Next
                    End If
                    Dim sTri As String = ""
                    For Each oTri In oTris
                        If oTri.sIdGrid = oGrid.ID And oTri.sdatafield = CType(oGrid.Columns(i), BoundField).DataField Then
                            Select Case oTri.sTri
                                Case "asc"
                                    sTri = "desc"
                                Case "desc"
                                    sTri = "asc"
                            End Select
                        End If
                    Next

                    Dim oImageButton As New ImageButton
                    Select Case sTri
                        Case ""
                            oImageButton.ImageUrl = "~/images/placeholder.gif"
                        Case "asc"
                            oImageButton.ImageUrl = "~/images/asc.gif"
                        Case "desc"
                            oImageButton.ImageUrl = "~/images/desc.gif"
                    End Select

                    oImageButton.ID = "imagetri_" + oGrid.ID + "_" + CStr(i)
                    oImageButton.Attributes.Add("name", oImageButton.ID)
                    oImageButton.Attributes.Add("Grid_Id", oGrid.ID)
                    oImageButton.Attributes.Add("DataField", CType(oGrid.Columns(i), BoundField).DataField)
                    oImageButton.Attributes.Add("SensTri", "")
                    oImageButton.Attributes.Add("UpdatePanelId", oGrid.Attributes("NomIDUPDATEPANEL"))
                    oImageButton.CssClass = "C_image_tri"
                    oImageButton.Attributes.Add("onclick", "NotifierTri('" & oGrid.ID & "','" & CType(oGrid.Columns(i), BoundField).DataField & "');")
                    oLigneSuivante.Cells(i).Controls.Add(oImageButton)
                End If
            Next
        Next

    End Sub

    Private Function CopierCellule(ByVal oCellule As TableCell) As TableCell
        Dim oTablecell As New TableCell()
        oTablecell.Text = oCellule.Text
        Return oTablecell
    End Function

    Private Sub oGrid_OnRowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        Try
            Dim Ligne As GridViewRow = CType(e.Row, GridViewRow)
            Dim oGrid As GridView = CType(sender, GridView)
            Dim Table_Id() As String = CType(sender, GridView).ID.Split(CChar("t"))
            Dim idRapport As Integer = CInt(Table_Id(Table_Id.Length - 1))
            Dim sTexteCelluleNonModifiee As String = ""
            Dim iIndiceColonneSource As Integer = Nothing

            Dim idModule As String = ""
            Dim oRapport As New Rapport()
            Dim oColonnes As New Colonnes
            Dim sAffichage As String = ""
            Dim oListeRapports As New Rapports
            Dim sListeIdRapports As String = ""

            Dim oRedirections As New Redirections
            Dim oFiltresRapportSource As New Filtres
            Dim oRapportsSource As New Rapports

            Dim sColonneEdition As String = ""
            Dim sValeurEdition As String = ""
            Dim bModeEdition As Boolean = False

            Dim bClasseGeneraleTrouvee As Boolean = False
            Dim oGridDataBoundList As GridDataBoundList = Nothing
            Dim oGridDataBoundListNouveau As New GridDataBoundList
            oGridDataBoundList = oGridDataBoundLists.Find(Function(p) p.sIdGridview = oGrid.ID)
            If Not oGridDataBoundList Is Nothing Then
                bClasseGeneraleTrouvee = True
            Else
                oGridDataBoundListNouveau.sIdGridview = oGrid.ID
            End If


            If Me.ReferenceModificationLigne.Value <> "" Then
                sColonneEdition = Me.ReferenceModificationLigne.Value.ToString.Split(CChar("|"))(2).Split(CChar("="))(0)
                sValeurEdition = Me.ReferenceModificationLigne.Value.ToString.Split(CChar("|"))(2).Split(CChar("="))(1)
            End If



            Dim oUtilisateurSecondaireFiltres As New UtilisateurSecondaireFiltres()
            If Session.Item("idUtilisateurSecondaire") IsNot Nothing And Session.Item("idUtilisateurSecondaire").ToString <> "0" Then
                Dim oUtilisateurSecondaire As New UtilisateurSecondaire()
                oUtilisateurSecondaire.Req_Infos_Utilisateur_Secondaire(CInt(Session.Item("idUtilisateurSecondaire").ToString), CInt(User.Identity.Name.Split(CChar("_"))(1)))
                oUtilisateurSecondaireFiltres.chargerUtilisateurSecondaireFiltres(oUtilisateurSecondaire)
            End If




            idModule = CStr(CInt(Request.Item("module").ToString))
            If bClasseGeneraleTrouvee = False Then
                oListeRapports.chargerRappportsParModuleUtilisateur(CInt(Request.Item("module")), CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))
                oRapport = oListeRapports.Find(Function(p) p.nIdRapport = idRapport)

                sListeIdRapports = ""
                For Each oRapportTemp As Rapport In oListeRapports
                    If oRapportTemp.nSommaireOrdreNiv1 = oRapport.nSommaireOrdreNiv1 And oRapportTemp.nSommaireOrdreNiv2 = oRapport.nSommaireOrdreNiv2 And oRapportTemp.bEstActif = True And oRapportTemp.nOngletOrdreNiv1 = oRapport.nOngletOrdreNiv1 And oRapportTemp.nOngletOrdreNiv2 = oRapport.nOngletOrdreNiv2 Then
                        sListeIdRapports += CStr(oRapportTemp.nIdRapport) & "|"
                        oRapportsSource.Add(oRapportTemp)
                    End If
                Next
                If sListeIdRapports <> "" Then
                    sListeIdRapports = sListeIdRapports.Remove(sListeIdRapports.Length - 1, 1)
                End If
                oGridDataBoundListNouveau.oListeRapports = oListeRapports
                oGridDataBoundListNouveau.oRapportsSource = oRapportsSource
                oGridDataBoundListNouveau.sListeIdRapports = sListeIdRapports
            Else
                oListeRapports = oGridDataBoundList.oListeRapports
                oRapport = oListeRapports.Find(Function(p) p.nIdRapport = idRapport)
                oRapportsSource = oGridDataBoundList.oRapportsSource
                sListeIdRapports = oGridDataBoundList.sListeIdRapports
            End If

            oRapport = oListeRapports.Find(Function(p) p.nIdRapport = idRapport)

            Dim Id_Div As String = CType(sender, GridView).Attributes("NomID").ToString
            Dim oCheckboxMoisGlissant As CheckBox = CType(FindControl("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports), CheckBox)
            Dim oCheckBoxDecimales As CheckBox = CType(FindControl("checkboxAfficherDecimale" & "_" & Id_Div & "--" & sListeIdRapports), CheckBox)
            Dim ocheckboxTTC As CheckBox = CType(FindControl("checkbox_TTC" & "_" & Id_Div & "--" & sListeIdRapports), CheckBox)
            Dim otextboxSeuil As TextBox = CType(FindControl("seuil" & "_" & Id_Div & "_" & sListeIdRapports), TextBox)

            If bClasseGeneraleTrouvee = False Then
                oColonnes.ChargerListeColonnes(oRapport, oCheckboxMoisGlissant, Id_Div, Me.BloquerColoneAction)
                oRedirections.chargerRedirectionsParRapport(oRapport, oCheckboxMoisGlissant, Id_Div, Me.BloquerColoneAction)

                oGridDataBoundListNouveau.oColonnes = oColonnes
                oGridDataBoundListNouveau.oRedirections = oRedirections
            Else
                oColonnes = oGridDataBoundList.oColonnes
                oRedirections = oGridDataBoundList.oRedirections
            End If

            If Not oRedirections Is Nothing Then
                If bClasseGeneraleTrouvee = False Then
                    oFiltresRapportSource.chargerFiltresParRapports(oRapportsSource, oRapport, oUtilisateurSecondaireFiltres)
                    oGridDataBoundListNouveau.oFiltresRapportSource = oFiltresRapportSource
                Else
                    oFiltresRapportSource = oGridDataBoundList.oFiltresRapportSource
                End If
            End If

            If bClasseGeneraleTrouvee = False Then
                oGridDataBoundLists.Add(oGridDataBoundListNouveau)
            End If

            Dim Div As HtmlGenericControl = CType(Page.FindControl(Id_Div), HtmlGenericControl)
            Dim TableValeurs(oGrid.Columns.Count - 1) As String
            Dim iIndexLigne As Integer = 0
            Dim NumColonne As Integer = 0
            Dim TabNiv2(oColonnes.Count() - 1) As String
            Dim TabNiv3(oColonnes.Count() - 1) As String
            Dim TabNiv4(oColonnes.Count() - 1) As String
            Dim bPresenceNiv2 As Boolean = False
            Dim bPresenceNiv3 As Boolean = False
            Dim bPresenceNiv4 As Boolean = False
            Dim iDerniereColonneVisible As Integer = 0
            Dim bSeuilEstDepasse As Boolean
            Dim sNumeroPostit As String = ""

            For i = oGrid.Columns.Count - 1 To 0 Step -1
                If oGrid.Columns(i).Visible = True Then
                    iDerniereColonneVisible = i
                    Exit For
                End If
            Next

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim sClasseHover As String = ""
                If oRapport.bEstHover = True Then
                    Ligne.CssClass = "C_Hover"
                End If

                Dim nLargeurTest As Integer = 0

                For Each oColonne In oColonnes
                    Dim oPostItLiaison As New PostItLiaison()
                    oPostItLiaison.oRapport = oRapport
                    oPostItLiaison.ChargerPostItLiaisonSelonNomTableAvecPostIt()

                    If Not oPostItLiaison.sNomColonneNumTel Is Nothing AndAlso oColonne.sNomColonne = oPostItLiaison.sNomColonneNumTel Then
                        sNumeroPostit = Ligne.Cells(iIndexLigne).Text
                    End If


                    iIndexLigne += 1
                Next


                iIndexLigne = 0
                For Each oColonne In oColonnes

                    If oColonne.sNomColonne = "postit" Then
                        'Dim sAppelFonction As String = "<span class=""lienPostIt"" onclick=""OuvrirPopupPostIt('" & sNumeroPostit & "');"">" & Ligne.Cells(iIndexLigne).Text & "</span>"
                        Dim oPostItLigne As New PostItLigne()
                        oRapport.oClient.chargerClientParId(oRapport.oClient.nIdClient)
                        oPostItLigne.sNumLigne = sNumeroPostit
                        oPostItLigne.chargerPostItLigne(oRapport.oClient)
                        Dim bPresencePointSuspension As Boolean = True
                        Dim sPointSuspension As String = ""
                        Dim sAffichageInfoBulle As String = ""
                        If oPostItLigne.nIdPostIt = 0 OrElse oPostItLigne.sInfoPostIt.Length > 15 OrElse Trim(oPostItLigne.sInfoPostIt).Length = 0 Then
                            sPointSuspension = "..."
                        End If
                        If oPostItLigne.nIdPostIt > 0 Then
                            sAffichageInfoBulle = "title=""" & oPostItLigne.sInfoPostIt & """ "
                        Else
                            sAffichageInfoBulle = ""
                        End If
                        Dim sAppelFonction As String = " <span Class=""lienPostIt"" " & sAffichageInfoBulle & "onclick=""OuvrirPopupPostIt('" & sNumeroPostit & "');"">" & Left(oPostItLigne.sInfoPostIt, 15) & sPointSuspension & "</span>"
                        Ligne.Cells(iIndexLigne).Text = "<div style""width:100%;height:100%;cursor:hand;"">" & sAppelFonction & "</div>"
                    End If

                    If sValeurEdition <> "" And oColonne.sNomColonne.ToUpper = "ACTION" And Ligne.Cells(iIndexLigne).Text.Contains(sColonneEdition & "=" & sValeurEdition & "'") Then
                        bModeEdition = True
                    End If


                    TableValeurs(iIndexLigne) = Ligne.Cells(iIndexLigne).Text

                    If iDerniereColonneVisible = iIndexLigne Then
                        Ligne.Cells(iIndexLigne).Attributes.Add("style", "min-width:" & CStr(oColonne.nLargeurColonne + 20) & "Px;")
                    Else
                        Ligne.Cells(iIndexLigne).Attributes.Add("style", "min-width:" & (oColonne.nLargeurColonne).ToString & "Px;")
                    End If
                    If oColonne.sTypeDonnees.ToLower = "nombre" Or oColonne.sTypeDonnees.ToLower = "nombrebalise" Then
                        Dim sNomColonneRetraire As String = ""
                        Dim sBalises As New List(Of String)
                        If oColonne.sNomColonne.Contains("|") Then
                            sNomColonneRetraire = oColonne.sNomColonne.Split(CChar("|"))(0)
                        Else
                            sNomColonneRetraire = oColonne.sNomColonne
                        End If

                        sAffichage = TableValeurs(iIndexLigne)

                        If oColonne.sMefDonnees <> "" Then
                            Dim sTable() As String = oColonne.sMefDonnees.Split(CChar("|"))
                            Dim sCritere As String = ""
                            Dim sValeur As String = ""
                            Dim stmpNombreDecimale As String = ""


                            If oColonne.sTypeDonnees = "nombrebalise" Then
                                sBalises = Toolbox.ScinderBalisesHtml(sAffichage)
                                sAffichage = sBalises(1).Replace(".", ",")
                            End If

                            For iIndiceTable = 0 To sTable.Length - 1
                                sCritere = sTable(iIndiceTable).ToString.Split(CChar(";"))(0)
                                sValeur = sTable(iIndiceTable).ToString.Split(CChar(";"))(1)
                                Select Case sCritere.ToUpper
                                    Case "NOMBRE", "POURCENTAGE", "PROGRESSION"
                                        sAffichage = sAffichage.Replace("&nbsp;", "")
                                        sAffichage = sAffichage.Replace("&#160;", "")
                                        Dim sFormat As String = "# ### ##0"
                                        If sAffichage = "" Then sAffichage = "0"
                                        ' Je récupère la partie après le N, dans le cas où le N ne serait pas renseigné, je renseigne 0 à la place
                                        If Split(sValeur, "N").Count = 0 Then
                                            stmpNombreDecimale = "0}"
                                        Else
                                            stmpNombreDecimale = Split(sValeur, "N")(1)
                                        End If
                                        If oCheckBoxDecimales Is Nothing OrElse oCheckBoxDecimales.Checked = False Then
                                            stmpNombreDecimale = "0}"
                                        End If

                                        Dim iNombreDecimale As UShort = CUShort(Split(stmpNombreDecimale, "}")(0))

                                        If iNombreDecimale > 0 Then
                                            sFormat += "."
                                            For j = 0 To (iNombreDecimale - 1)
                                                sFormat += "0"
                                            Next
                                        End If
                                        If sAffichage <> "-" Then
                                            sAffichage = Format(CDbl(sAffichage), sFormat)
                                        End If
                                    Case "TIME"

                                End Select
                            Next
                        End If
                        If Not sAffichage Is Nothing AndAlso sAffichage <> "-" AndAlso sAffichage <> "" AndAlso Not oColonne.sMefDonnees.ToUpper.Contains("TIME") Then
                            If CDbl(sAffichage.Replace(" ", "")) = 0 Then sAffichage = oColonne.sSiZero
                            If sBalises.Count > 0 Then
                                sAffichage = sBalises(0) + sAffichage + sBalises(2)
                                Ligne.Cells(iIndexLigne).Text = sAffichage
                            End If
                            TableValeurs(iIndexLigne) = sAffichage

                        End If

                    End If

                    If oColonne.sStyleDonnees <> "" Then
                        Toolbox.AppliquerFormatCellule(oColonne.sStyleDonnees, Ligne.Cells(iIndexLigne))
                    End If
                    If oColonne.sTypeDonnees.ToUpper = "NOMBRE" AndAlso sAffichage <> "-" AndAlso Not oColonne.sMefDonnees.ToUpper.Contains("TIME") Then
                        If Toolbox.ConvertirNombreFormate(Ligne.Cells(iIndexLigne).Text) <> "" Then
                            If CDbl(Toolbox.ConvertirNombreFormate(Ligne.Cells(iIndexLigne).Text)) = 0 Then Ligne.Cells(iIndexLigne).Text = oColonne.sSiZero
                        End If
                        If Toolbox.ConvertirNombreFormate(Ligne.Cells(iIndexLigne).Text) = "" Then Ligne.Cells(iIndexLigne).Text = oColonne.sSiNull
                        If oColonne.sValeurSeuil <> "" And Toolbox.ConvertirNombreFormate(Ligne.Cells(iIndexLigne).Text) <> "" Then
                            If CDbl(Toolbox.ConvertirNombreFormate(Ligne.Cells(iIndexLigne).Text)) > CDbl(oColonne.sValeurSeuil) Then
                                Toolbox.AppliquerFormatCellule(oColonne.sStyleSeuil, Ligne.Cells(iIndexLigne))
                            End If
                        End If
                        If oColonne.sMefDonnees.Contains("pourcentage") Then
                            Toolbox.AppliquerFormatPourcentage(Ligne.Cells(iIndexLigne))
                        End If
                        If oColonne.sMefDonnees.Contains("progression") Then
                            Toolbox.AppliquerFormatProgression(Ligne.Cells(iIndexLigne))
                        End If

                    ElseIf oColonne.sTypeDonnees = "booleen" Then
                        Ligne.Cells(iIndexLigne).Text = Toolbox.ReplaceBooleenTexte(Ligne.Cells(iIndexLigne).Text)
                    Else
                        If e.Row.RowType <> DataControlRowType.Footer Then
                            If oColonne.sMefDonnees.ToUpper.Contains("TIME") Then
                                If Toolbox.ConvertirNombreFormate(Ligne.Cells(iIndexLigne).Text) = "00:00:00" Then Ligne.Cells(iIndexLigne).Text = oColonne.sSiNull
                            Else
                                If Toolbox.ConvertirNombreFormate(Ligne.Cells(iIndexLigne).Text) = "" Then Ligne.Cells(iIndexLigne).Text = oColonne.sSiNull
                            End If
                        End If
                    End If

                    sTexteCelluleNonModifiee = Ligne.Cells(iIndexLigne).Text

                    Dim sClasseFusion = ""
                    If oColonne.bEstFusionnable = True Then
                        sClasseFusion = "C_Fusion"
                    End If
                    Dim sClasse As String = "C_donnees_" + oColonne.sclasseCellule
                    If sClasseFusion <> "" Then sClasse += " " + sClasseFusion

                    Ligne.Cells(iIndexLigne).CssClass = sClasse

                    If Not FindControl("checkBoxAfficherCaseNumComplet_" & Id_Div & "--" & sListeIdRapports) Is Nothing Then
                        If (oColonne.bEstDigitsMasquables = True And CType(FindControl("checkBoxAfficherCaseNumComplet_" & Id_Div & "--" & sListeIdRapports), CheckBox).Checked = False) Then
                            If Not (Ligne.Cells(iIndexLigne).Text Is Nothing) Then
                                If (Ligne.Cells(iIndexLigne).Text.Length > 0) Then
                                    Ligne.Cells(iIndexLigne).Text = MasquerChaine(Ligne.Cells(iIndexLigne).Text, oColonne)
                                End If
                            End If
                        End If
                    End If
                    If FindControl("checkBoxAfficherCaseNumComplet_" & Id_Div & "--" & sListeIdRapports) Is Nothing And oRapport.bAfficherNumeroEntier = False Then
                        If oColonne.bEstDigitsMasquables = True And oRapport.bAfficherCaseNumComplet = False And oRapport.bAfficherNumeroEntier = False Then
                            If Not (Ligne.Cells(iIndexLigne).Text Is Nothing) Then
                                If (Ligne.Cells(iIndexLigne).Text.Length > 0) Then
                                    Ligne.Cells(iIndexLigne).Text = MasquerChaine(Ligne.Cells(iIndexLigne).Text, oColonne)
                                End If
                            End If
                        End If
                    End If

                    bSeuilEstDepasse = False
                    If oRapport.bAfficherSeuil = True And oColonne.bEstSeuil = True Then
                        If Not IsNothing(otextboxSeuil) AndAlso otextboxSeuil.Text <> "" Then
                            If IsNumeric(otextboxSeuil.Text.Replace(".", ",")) And IsNumeric(TableValeurs(iIndexLigne).Replace(".", ",").Replace(" ", "")) Then
                                If CDbl(TableValeurs(iIndexLigne).Replace(".", ",").Replace(" ", "")) > CDbl(otextboxSeuil.Text.Replace(".", ",")) Then
                                    bSeuilEstDepasse = True
                                End If
                            End If
                        End If
                    End If

                    '---Gestion des redirections
                    If e.Row.RowType <> DataControlRowType.Footer Then
                        Dim idColonneActive As Integer = oColonnes(iIndexLigne).nIdColonne
                        Dim oRedirection As Redirection = oRedirections.Find(Function(p) p.nidColonne = idColonneActive)
                        If Not oRedirection Is Nothing Then
                            '---Il y a une redirection
                            Dim sChaineArianeRetour As String = sTexteCelluleNonModifiee
                            Dim TableChaineRedirection() As String = oRedirection.sChaineFiltre.Split(CChar(";"))
                            Dim sRedirectionSource As String = ""
                            Dim sRedirectionCible As String = ""
                            Dim ChaineFinaleRedirection As String = ""
                            For Each sChaineRedirection As String In TableChaineRedirection
                                iIndiceColonneSource = Nothing
                                sRedirectionSource = sChaineRedirection.Split(CChar(">"))(0)
                                sRedirectionCible = sChaineRedirection.Split(CChar(">"))(1)
                                Select Case sRedirectionSource.Substring(0, 2)
                                    Case "Cs"
                                        '---De type Cs5>Fd10
                                        Dim idColonneSource As String = sRedirectionSource.Substring(2, sRedirectionSource.Length - 2)
                                        '---Chercher_Numero_Colonne_Source
                                        Dim i As Integer = 0
                                        For Each oColonneTemp As Colonne In oColonnes
                                            If oColonneTemp.nIdColonne = CInt(idColonneSource) Then
                                                iIndiceColonneSource = i
                                            End If
                                            i += 1
                                        Next
                                        sChaineArianeRetour = TableValeurs(iIndiceColonneSource)
                                        ChaineFinaleRedirection += sRedirectionCible.Substring(2, sRedirectionCible.Length - 2) & "=" & TableValeurs(iIndiceColonneSource) & "µ"
                                    Case "Fs"
                                        '---De type Fs10>Fd4
                                        Dim IdFiltreSource As String = sRedirectionSource.Substring(2, sRedirectionSource.Length - 2)
                                        Dim oFiltre As Filtre = oFiltresRapportSource.Find(Function(p) p.nIdFiltre = CInt(IdFiltreSource))
                                        Dim oControleObject As Object = Page.FindControl("Filtre_" & Id_Div & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports)
                                        If TypeOf oControleObject Is DropDownList Then
                                            ChaineFinaleRedirection += sRedirectionCible.Substring(2, sRedirectionCible.Length - 2) & "=" & Toolbox.ReplaceQuote(CType(oControleObject, DropDownList).SelectedValue) & "µ"
                                        End If
                                        If TypeOf oControleObject Is ListBox Then
                                            Dim ChaineFinaleRedirectionIntermediare As String = ""
                                            ChaineFinaleRedirectionIntermediare += sRedirectionCible.Substring(2, sRedirectionCible.Length - 2) & "="
                                            For Each item In CType(oControleObject, ListBox).GetSelectedIndices
                                                ChaineFinaleRedirectionIntermediare &= Toolbox.ReplaceQuote(CType(oControleObject, ListBox).Items(item).Value) & "|"
                                            Next
                                            If ChaineFinaleRedirectionIntermediare <> "" Then ChaineFinaleRedirectionIntermediare = ChaineFinaleRedirectionIntermediare.Remove(ChaineFinaleRedirectionIntermediare.Length - 1, 1)
                                            ChaineFinaleRedirection &= ChaineFinaleRedirectionIntermediare
                                        End If
                                    Case "Ms"
                                        '---De type MS>Fd12 (Gestion des mois en colonne)
                                        ChaineFinaleRedirection += sRedirectionCible.Substring(2, sRedirectionCible.Length - 2) & "=" & Toolbox.ConvertirValeurFiltre(oColonnes(iIndexLigne).sNomColonne, "Mois", oRapport.oClient) & "µ"
                                    Case "Ps"
                                        '---De type Ps|Texte personalise|>Fd12 (Filtre avec un texte personalise)
                                        ChaineFinaleRedirection += sRedirectionCible.Substring(2, sRedirectionCible.Length - 2) & "=" & sRedirectionSource.Split(CChar("|"))(1) & "µ"
                                    Case Else
                                        ChaineFinaleRedirection += sRedirectionCible & "=" & sRedirectionSource
                                End Select
                            Next
                            If ChaineFinaleRedirection <> "" Then
                                ChaineFinaleRedirection = ChaineFinaleRedirection.Remove(ChaineFinaleRedirection.Length - 1, 1)
                            End If

                            Dim oRaportDestination As New Rapport
                            oRaportDestination = oListeRapports.Find(Function(p) p.nIdRapport = oRedirection.nIdRapportCible)

                            If sChaineArianeRetour = "" Then
                                sChaineArianeRetour = sTexteCelluleNonModifiee
                            End If

                            If iIndiceColonneSource = Nothing Then iIndiceColonneSource = iIndexLigne

                            If oRedirection.bEstPopup = True Then
                                Dim Lien_Redirection As String = oRedirection.sPageRapportCible & "?module=" & idModule & "&rapport=" & oRaportDestination.nIdRapport & "&masquer=false&chainefiltre=" & ChaineFinaleRedirection
                                Dim oDiv As New HtmlGenericControl("DIV")
                                Dim sRaffraichissement As String
                                If oRedirection.bRafraichissementFermeturePopup = True Then
                                    sRaffraichissement = "1"
                                Else
                                    sRaffraichissement = "0"
                                End If


                                'oDiv.Attributes.Add("onclick", "OuvrirPopup('" & Lien_Redirection & "','" & oRaportDestination.sSommaireNiv2 & " - " & Toolbox.FormaterJavascript(sChaineArianeRetour) & "','" & oRedirection.sLargueurPopup & "','" & oRedirection.sHauteurPopup & "'," & sRaffraichissement & ");__doPostBack('__Page', 'MyCustomArgument');")
                                oDiv.Attributes.Add("onclick", "OuvrirPopup('" & Lien_Redirection & "','" & oRedirection.sLargueurPopup & "','" & oRedirection.sHauteurPopup & "'," & sRaffraichissement & ");")
                                oDiv.Attributes.Add("style", "width:100%;height:100%;")
                                Dim oLiteral As New Literal
                                'oLiteral.Text = "<span onclick=""__doPostBack('__Page', 'MyCustomArgument');"" style=""color:#37a8e7;text-decoration:underline;cursor:pointer;cursor:hand;"" >" & TableValeurs(iIndexLigne) & "</span>"
                                If bSeuilEstDepasse = False Then
                                    oLiteral.Text = "<span style=""color:#37a8e7;text-decoration:underline;cursor:pointer;cursor:hand;"" >" & TableValeurs(iIndexLigne) & "</span>"
                                Else
                                    oLiteral.Text = "<span style=""color:#B22222;text-decoration:underline;cursor:pointer;cursor:hand;"" >" & TableValeurs(iIndexLigne) & "</span>"
                                End If
                                oDiv.Controls.Add(oLiteral)
                                Ligne.Cells(iIndexLigne).Text = ""
                                Ligne.Cells(iIndexLigne).Controls.Add(oDiv)
                                Ligne.Cells(iIndexLigne).Attributes("style") = Ligne.Cells(iIndexLigne).Attributes("style").ToString & "cursor:pointer;cursor:hand"
                            Else
                                Dim oDiv As New HtmlGenericControl("DIV")
                                oDiv.Attributes.Add("style", "width:100%;height:100%;")
                                Dim oLiteral As New Literal
                                If bSeuilEstDepasse = False Then
                                    oLiteral.Text = "<span onclick=""PreparerRedirection('" & Request.Item("module").ToString & "','" & CStr(oRedirection.nIdRapportCible) & "','" & Toolbox.FormaterJavascript(ChaineFinaleRedirection) & "','" & Toolbox.FormaterJavascript(sChaineArianeRetour) & "');__doPostBack('__Page', 'MyCustomArgument');""  style=""color:#37a8e7;text-decoration:underline;display:block;width:100%"" >" & TableValeurs(iIndexLigne) & "</span>"
                                Else
                                    oLiteral.Text = "<span onclick=""PreparerRedirection('" & Request.Item("module").ToString & "','" & CStr(oRedirection.nIdRapportCible) & "','" & Toolbox.FormaterJavascript(ChaineFinaleRedirection) & "','" & Toolbox.FormaterJavascript(sChaineArianeRetour) & "');__doPostBack('__Page', 'MyCustomArgument');""  style=""color:#B22222;text-decoration:underline;display:block;width:100%"" >" & TableValeurs(iIndexLigne) & "</span>"
                                End If
                                oDiv.Controls.Add(oLiteral)

                                Ligne.Cells(iIndexLigne).Text = ""
                                Ligne.Cells(iIndexLigne).Controls.Add(oDiv)
                                Ligne.Cells(iIndexLigne).Attributes("style") = Ligne.Cells(iIndexLigne).Attributes("style").ToString & "cursor:pointer;cursor:hand"
                            End If
                        End If
                    End If

                    If bSeuilEstDepasse = True Then
                        Ligne.Cells(iIndexLigne).CssClass = Ligne.Cells(iIndexLigne).CssClass & "_Seuil"
                    End If

                    'If oRapport.bAfficherSeuil = True And oColonne.bEstSeuil = True Then
                    '    If Not IsNothing(otextboxSeuil) AndAlso otextboxSeuil.Text <> "" Then
                    '        If IsNumeric(otextboxSeuil.Text.Replace(".", ",")) And IsNumeric(TableValeurs(iIndexLigne).Replace(".", ",").Replace(" ", "")) Then
                    '            If CDbl(TableValeurs(iIndexLigne).Replace(".", ",").Replace(" ", "")) > CDbl(otextboxSeuil.Text.Replace(".", ",")) Then
                    '                'Ligne.Cells(iIndexLigne).ForeColor = System.Drawing.ColorTranslator.FromHtml("#B22222")
                    '                Ligne.Cells(iIndexLigne).CssClass = Ligne.Cells(iIndexLigne).CssClass & "_Seuil"
                    '            End If
                    '        End If
                    '    End If
                    'End If

                    iIndexLigne += 1
                Next

                '---Gestion ligne edition
                If bModeEdition = True Then
                    iIndexLigne = 0
                    Dim sListeControleEdit As String = ""
                    Dim sListeColonnesEdit As String = ""
                    For Each ocolonne In oColonnes
                        Select Case ocolonne.sControleModification.ToUpper
                            Case "LISTE"
                                Dim oControle As New DropDownList
                                oControle.ID = "DropdownList_Edit" & "_" & oRapport.nIdRapport & "-" & ocolonne.nIdColonne
                                cDal.RecupererListeValeursTable(oControle, oRapport.oClient.sBaseClient, ocolonne.sTableModification, ocolonne.sChampModification, ocolonne.sFiltreModification)

                                Dim i As Integer = 0
                                For Each oItem In oControle.Items
                                    If CType(oItem, ListItem).Text = Ligne.Cells(iIndexLigne).Text Then
                                        oControle.SelectedIndex = i
                                    End If
                                    i += 1
                                Next
                                oControle.Attributes.Add("class", "styled")
                                oControle.Attributes.Add("name", oControle.ID)
                                oControle.Width = New Unit(80, UnitType.Percentage)
                                oControle.Height = New Unit(20, UnitType.Pixel)
                                Dim oP As New HtmlGenericControl("p")
                                oP.Controls.Add(oControle)
                                Ligne.Cells(iIndexLigne).Text = ""
                                Ligne.Cells(iIndexLigne).Controls.Add(oP)
                                sListeControleEdit += oControle.ID + "|"
                                sListeColonnesEdit += ocolonne.sNomColonne + "|"
                            Case "TEXTE"
                                Dim oControle As New TextBox
                                oControle.ID = "Textbox_Edit" & "_" & oRapport.nIdRapport & "-" & ocolonne.nIdColonne
                                oControle.TextMode = TextBoxMode.SingleLine
                                oControle.Text = Ligne.Cells(iIndexLigne).Text
                                oControle.Width = New Unit(96, UnitType.Percentage)
                                oControle.Attributes.Add("onChange", "Supprimer_Balises_Input(this)")
                                oControle.Attributes.Add("style", "font-size:12Px;color:#206797;")
                                oControle.Height = New Unit(20, UnitType.Pixel)
                                oControle.CssClass = "InsertText"

                                Ligne.Cells(iIndexLigne).Text = ""
                                Ligne.Cells(iIndexLigne).Controls.Add(oControle)

                                sListeControleEdit += oControle.ID + "|"
                                sListeColonnesEdit += ocolonne.sNomColonne + "|"
                            Case "MULTITEXTE"
                                Dim oControle As New TextBox
                                oControle.ID = "Textbox_Edit" & "_" & oRapport.nIdRapport & "-" & ocolonne.nIdColonne
                                oControle.TextMode = TextBoxMode.MultiLine
                                oControle.Text = Ligne.Cells(iIndexLigne).Text
                                oControle.Width = New Unit(96, UnitType.Percentage)
                                oControle.Attributes.Add("onChange", "Supprimer_Balises_Input(this)")
                                oControle.Attributes.Add("style", "font-size:12Px;color:#206797;")
                                oControle.Height = New Unit(60, UnitType.Pixel)
                                oControle.CssClass = "InsertText"

                                Ligne.Cells(iIndexLigne).Text = ""
                                Ligne.Cells(iIndexLigne).Controls.Add(oControle)

                                sListeControleEdit += oControle.ID + "|"
                                sListeColonnesEdit += ocolonne.sNomColonne + "|"
                            Case "CHECKBOX"
                                Dim oControle As New CheckBox
                                oControle.ID = "Checkbox_Edit" & "_" & oRapport.nIdRapport & "-" & ocolonne.nIdColonne
                                If Ligne.Cells(iIndexLigne).Text.ToUpper = "TRUE" Or Ligne.Cells(iIndexLigne).Text.ToUpper = "OUI" Then
                                    oControle.Checked = True
                                Else
                                    oControle.Checked = False
                                End If

                                Ligne.Cells(iIndexLigne).Text = ""
                                Ligne.Cells(iIndexLigne).Controls.Add(oControle)

                                sListeControleEdit += oControle.ID + "|"
                                sListeColonnesEdit += ocolonne.sNomColonne + "|"
                        End Select


                        If ocolonne.sTypeDonnees.ToUpper = "ACTION" Then
                            If sListeControleEdit <> "" Then sListeControleEdit = sListeControleEdit.Remove(sListeControleEdit.Length - 1, 1)
                            If sListeColonnesEdit <> "" Then sListeColonnesEdit = sListeColonnesEdit.Remove(sListeColonnesEdit.Length - 1, 1)

                            Ligne.Cells(iIndexLigne).Controls.Clear()

                            Dim oBoutonImage As New ImageButton
                            oBoutonImage.ID = "BtnValidationEdit" & "_" & CStr(oRapport.nIdRapport)
                            oBoutonImage.CssClass = "boutonValidationEdit"
                            oBoutonImage.CausesValidation = True
                            oBoutonImage.Attributes.Add("sGridId", oGrid.ID)
                            oBoutonImage.Attributes.Add("sListeControleEdit", sListeControleEdit)
                            oBoutonImage.Attributes.Add("sRapport", CStr(oRapport.nIdRapport))
                            oBoutonImage.Attributes.Add("sListeColonnesEdit", sListeColonnesEdit)
                            oBoutonImage.Attributes.Add("sNumeroLigne", Ligne.RowIndex.ToString)
                            oBoutonImage.Attributes.Add("sBase", oRapport.oClient.sBaseClient)
                            oBoutonImage.Attributes.Add("sTable", oRapport.sTableAction)
                            oBoutonImage.Attributes.Add("sCritere", sColonneEdition & "=" & sValeurEdition)
                            oBoutonImage.ToolTip = "Modifier"
                            oBoutonImage.ImageUrl = "images/icones/Validation.png"
                            AddHandler oBoutonImage.Click, AddressOf BtnValidationEdit_Click
                            sc1.RegisterAsyncPostBackControl(oBoutonImage)
                            Ligne.Cells(iIndexLigne).Controls.Add(oBoutonImage)

                            oBoutonImage = New ImageButton
                            oBoutonImage.ID = "BtnAnulationEdit" & "_" & CStr(oRapport.nIdRapport)
                            oBoutonImage.OnClientClick = "AnnulerEdition();"
                            oBoutonImage.CssClass = "boutonAnulationEdit"
                            oBoutonImage.CausesValidation = True
                            oBoutonImage.ToolTip = "Annuler"
                            oBoutonImage.ImageUrl = "images/icones/ico_supprimer.png"
                            Ligne.Cells(iIndexLigne).Controls.Add(oBoutonImage)
                        End If


                        iIndexLigne += 1
                    Next
                End If


            End If

            If e.Row.RowType = DataControlRowType.Footer And oRapport.bAfficherTotal = True Then
                Dim oRapports As New Rapports
                Dim oRapportsTraitement As New Rapports
                oRapports = oRapportsSource
                For Each oRapportActuel In oRapports
                    If oRapport.nOngletOrdreNiv1 = oRapportActuel.nOngletOrdreNiv1 And oRapport.nOngletOrdreNiv2 = oRapportActuel.nOngletOrdreNiv2 Then
                        oRapportsTraitement.Add(oRapport)
                    End If
                Next

                Dim oFiltres As New Filtres
                oFiltres.chargerFiltresParRapports(oRapportsTraitement, oRapport, oUtilisateurSecondaireFiltres)

                Dim ListeControles As New List(Of Object)
                Dim listeControlesIndice As UShort = 0
                For Each ofiltre In oFiltres
                    If Not IsNothing(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports)) Then
                        If TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.DropDownList Then
                            ListeControles.Add(CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), DropDownList))
                        ElseIf TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.ListBox Then
                            ListeControles.Add(CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), ListBox))
                        End If
                    End If
                Next

                Dim idGrid As String = Replace("Grid_" & oRapport.sSommaireNiv1 & "_" & oRapport.sSommaireNiv2 & "_Rapport" & CStr(oRapport.nIdRapport), " ", "__").Replace("-", "ù").Replace("'", "")

                cDal.ChargerLigneTotal(oColonnes, oRapport, e, ListeControles, Div, oCheckboxMoisGlissant, ocheckboxTTC, oCheckBoxDecimales, Me.OutilTop.Value, Me.ListeTriTableau.Value, Me.TriActif.Value, oGrid, iDerniereColonneVisible, Me.BloquerColoneAction, oUtilisateurSecondaireFiltres)
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Function MasquerChaine(ByVal sTexte As String, ByVal oColonne As Colonne) As String


        Dim nTailleTexte As Short = CShort(sTexte.Length)
        Dim nRetraitTexte As Short = oColonne.nNombreDigitsMasques
        If nRetraitTexte > nTailleTexte Then
            nRetraitTexte = nTailleTexte
        End If

        Dim sChaineModifiée As String = Left(sTexte, (nTailleTexte - nRetraitTexte))
        For i = 1 To nRetraitTexte Step 1
            sChaineModifiée += "●"
        Next

        Return sChaineModifiée


    End Function

    Private Sub Creer_Filtres(ByVal OrapportNiv1 As Rapport, ByVal oRapportsSurOnglet As Rapports, ByRef Div As HtmlGenericControl, ByRef ListeControles As List(Of Object), ByRef DivContainerParente As HtmlGenericControl, ByVal sListeIdRapports As String, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres)
        Dim oFiltres As New Filtres()
        Dim bFiltreVisible As Boolean = False

        Dim DivFiltres As HtmlGenericControl
        Dim DivCol1 As HtmlGenericControl
        Dim DivCol2 As HtmlGenericControl
        Dim DivCol3 As HtmlGenericControl
        Dim DivCol4 As HtmlGenericControl
        Dim DivCol5 As HtmlGenericControl
        Dim P As HtmlGenericControl

        Dim oLabel As HtmlGenericControl

        oFiltres.chargerFiltresParRapports(oRapportsSurOnglet, OrapportNiv1, oUtilisateurSecondaireFiltres)

        '---Mise a jour Chaine Redirection
        Dim sChaineFiltreRedirection As String = ""
        Dim sFiltre As String = ""
        If Me.Filtre.Value <> "" Then
            For Each sTexte As String In Me.ChaineFiltreRedirection.Value.Split(CChar("µ"))
                If Me.Filtre.Value.Contains(sTexte.Split(CChar("="))(0) & "=") Then
                    For Each sTexteFiltre As String In Me.Filtre.Value.Split(CChar("µ"))
                        If sTexteFiltre.Split(CChar("="))(0) = sTexte.Split(CChar("="))(0) Then
                            Dim oFiltre_Temp As Filtre = oFiltres.Find(Function(q) q.nIdFiltre = CInt(sTexteFiltre.Split(CChar("="))(0)))
                            'If Not oFiltre_Temp Is Nothing AndAlso oFiltre_Temp.bEstVisible = True Then
                            'sChaineFiltreRedirection += sTexte.Split(CChar("="))(0) + "=" + sTexteFiltre.Split(CChar("="))(1) + "µ"
                            'Else
                            sChaineFiltreRedirection += sTexte.Split(CChar("="))(0) + "=" + sTexte.Split(CChar("="))(1) + "µ"
                            'End If
                        End If
                    Next
                Else
                    sChaineFiltreRedirection += sTexte.Split(CChar("="))(0) + "=" + sTexte.Split(CChar("="))(1) + "µ"
                End If
            Next
            For Each sTexteFiltre As String In Me.Filtre.Value.Split(CChar("µ"))
                If Not Me.ChaineFiltreRedirection.Value.Contains(sTexteFiltre.Split(CChar("="))(0) & "=") Then
                    Dim oFiltre_Temp As Filtre = oFiltres.Find(Function(q) q.nIdFiltre = CInt(sTexteFiltre.Split(CChar("="))(0)))
                    If Not oFiltre_Temp Is Nothing AndAlso oFiltre_Temp.bEstVisible = True Then
                        sChaineFiltreRedirection += sTexteFiltre.Split(CChar("="))(0) + "=" + sTexteFiltre.Split(CChar("="))(1) + "µ"
                    End If
                End If
            Next
            If sChaineFiltreRedirection <> "" Then sChaineFiltreRedirection = sChaineFiltreRedirection.Remove(sChaineFiltreRedirection.Length - 1, 1)
            Me.ChaineFiltreRedirection.Value = sChaineFiltreRedirection
        End If

        If oFiltres.Count <> 0 Then
            DivFiltres = New HtmlGenericControl("div")
            DivFiltres.Attributes.Add("class", "clearfix")
            DivFiltres.ID = "idDivColsFiltres" & sListeIdRapports

            DivCol1 = New HtmlGenericControl("div")
            DivCol1.Attributes.Add("class", "col1")
            DivCol2 = New HtmlGenericControl("div")
            DivCol2.Attributes.Add("class", "col2")
            DivCol3 = New HtmlGenericControl("div")
            DivCol3.Attributes.Add("class", "col3")
            DivCol4 = New HtmlGenericControl("div")
            DivCol4.Attributes.Add("class", "col4")
            DivCol5 = New HtmlGenericControl("div")
            DivCol5.Attributes.Add("style", "display:none")

            P = New HtmlGenericControl("p")

            DivFiltres.Controls.Add(DivCol4)
            DivFiltres.Controls.Add(DivCol1)
            DivFiltres.Controls.Add(DivCol2)
            DivFiltres.Controls.Add(DivCol3)
            DivFiltres.Controls.Add(DivCol5)



            If OrapportNiv1.bEstFiltreBoite = True Then
                Dim DivBoiteOutils As New HtmlGenericControl("div")
                Dim sNomIdDivOutils As String = "DivBoiteFiltre" & "-" & OrapportNiv1.sOngletNiv1.Replace(" ", "") & "-" & OrapportNiv1.sOngletNiv2.Replace(" ", "")
                DivBoiteOutils.Attributes.Add("class", "DivColonnes clearfix")
                DivBoiteOutils.ID = "DivBoiteFiltre" & sListeIdRapports
                Dim sAffichageBandeau As String = "none"
                Dim aLienAfficheMasqueColonnesVisibles As New HtmlGenericControl("a")
                aLienAfficheMasqueColonnesVisibles.Attributes.Add("onclick", "toggle_visibility_filters('" & DivBoiteOutils.ID & "');")
                If Me.BoiteFiltre.Value.ToString.Contains(DivBoiteOutils.ID) Then
                    Me.BandeauMasqueFiltre.Value = DivBoiteOutils.ID
                    Me.UpdatePanelEntetes.Update()
                End If
                If Me.BandeauMasqueFiltre.Value = DivBoiteOutils.ID Then
                    sAffichageBandeau = "block"
                    aLienAfficheMasqueColonnesVisibles.Attributes.Add("class", "LinkFiltres highlight")
                Else
                    aLienAfficheMasqueColonnesVisibles.Attributes.Add("class", "LinkFiltres")
                End If
                aLienAfficheMasqueColonnesVisibles.InnerText = "FILTRES"
                DivBoiteOutils.Controls.Add(aLienAfficheMasqueColonnesVisibles)
                Dim oLiteral As New Literal
                oLiteral.Text = "<br />"
                DivBoiteOutils.Controls.Add(oLiteral)
                DivBoiteOutils.Controls.Add(DivFiltres)
                DivFiltres.Style.Add("display", sAffichageBandeau & ";margin-right:10px;margin-left:10px;margin-top:10px;max-height:350px;overflow-y:overlay;")
                Div.Controls.Add(DivBoiteOutils)
            Else
                Div.Controls.Add(DivFiltres)
            End If


            Dim TabPlacement1(0) As Boolean
            Dim TabPlacement2(0) As Boolean
            Dim TabPlacement3(0) As Boolean
            Dim ColonneARemplir As Integer

            For Each oFiltre In oFiltres

                ColonneARemplir = 0

                If TabPlacement3.Length < TabPlacement1.Length And TabPlacement3.Length < TabPlacement2.Length Then ColonneARemplir = 3
                If TabPlacement2.Length <= TabPlacement1.Length And TabPlacement2.Length <= TabPlacement3.Length Then ColonneARemplir = 2
                If TabPlacement1.Length <= TabPlacement2.Length And TabPlacement1.Length <= TabPlacement3.Length Then ColonneARemplir = 1


                If oFiltre.bEstVisible = False Then ColonneARemplir = 5

                P = New HtmlGenericControl("p")
                If oFiltre.bEstVisible = True Then
                    oLabel = New HtmlGenericControl("label")
                    oLabel.Attributes.Add("for", "Filtre_" & DivContainerParente.ID & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports)
                    oLabel.InnerText = oFiltre.sLibelleFiltre
                    P.Controls.Add(oLabel)
                    bFiltreVisible = True
                    P.Attributes.Add("style", "overflow:hidden;")
                End If

                Select Case oFiltre.bEstMultiselection
                    Case True
                        Dim oControle As New ListBox
                        oControle.Attributes.Add("class", "scrollbox")
                        oControle.Attributes.Add("size", "5")
                        oControle.Attributes.Add("multiple", "multiple")

                        oControle.Attributes.Add("name", "Filtre_" & DivContainerParente.ID & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports)
                        oControle.ID = "Filtre_" & DivContainerParente.ID & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports
                        oControle.SelectionMode = ListSelectionMode.Multiple
                        sc1.RegisterAsyncPostBackControl(oControle)

                        oControle.EnableViewState = False
                        P.Controls.Add(oControle)
                        Select Case ColonneARemplir
                            Case 1
                                ReDim Preserve TabPlacement1(TabPlacement1.Length + 2)
                                TabPlacement1(TabPlacement1.Length - 1) = True
                                TabPlacement1(TabPlacement1.Length - 2) = True
                                TabPlacement1(TabPlacement1.Length - 3) = True
                                DivCol1.Controls.Add(P)
                            Case 2
                                ReDim Preserve TabPlacement2(TabPlacement2.Length + 2)
                                TabPlacement2(TabPlacement2.Length - 1) = True
                                TabPlacement2(TabPlacement2.Length - 2) = True
                                TabPlacement2(TabPlacement2.Length - 3) = True
                                DivCol2.Controls.Add(P)
                            Case 3
                                ReDim Preserve TabPlacement3(TabPlacement3.Length + 2)
                                TabPlacement3(TabPlacement3.Length - 1) = True
                                TabPlacement3(TabPlacement3.Length - 2) = True
                                TabPlacement3(TabPlacement3.Length - 3) = True
                                DivCol3.Controls.Add(P)
                            Case 5
                                DivCol5.Controls.Add(P)
                        End Select

                    Case False
                        Dim oControle As New DropDownList
                        oControle.Attributes.Add("class", "styled")
                        oControle.Attributes.Add("name", "Filtre_" & DivContainerParente.ID & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports)
                        oControle.ID = "Filtre_" & DivContainerParente.ID & "_" & oFiltre.nIdFiltre & "_" & sListeIdRapports
                        oControle.Attributes.Add("onchange", "ValoriserFiltre(" & oFiltre.nIdFiltre & ",'" & oControle.ID & "');")
                        oControle.EnableViewState = False
                        sc1.RegisterAsyncPostBackControl(oControle)
                        P.Controls.Add(oControle)
                        Select Case ColonneARemplir
                            Case 1
                                ReDim Preserve TabPlacement1(TabPlacement1.Length)
                                TabPlacement1(TabPlacement1.Length - 1) = True
                                DivCol1.Controls.Add(P)
                            Case 2
                                ReDim Preserve TabPlacement2(TabPlacement2.Length)
                                TabPlacement2(TabPlacement2.Length - 1) = True
                                DivCol2.Controls.Add(P)
                            Case 3
                                ReDim Preserve TabPlacement3(TabPlacement3.Length)
                                TabPlacement3(TabPlacement3.Length - 1) = True
                                DivCol3.Controls.Add(P)
                            Case 5
                                DivCol5.Controls.Add(P)
                        End Select
                End Select
            Next

            Dim scheckBoxMoisGlissantCol3Ligne2 As String = "checkbox_MoisGlissant" & "_" & DivContainerParente.ID & "--" & sListeIdRapports
            Dim oCheckBoxMoisGlissant As CheckBox = CType(Page.FindControl(scheckBoxMoisGlissantCol3Ligne2), CheckBox)

            Charger_Filtres(sListeIdRapports, oFiltres, OrapportNiv1, oRapportsSurOnglet, DivContainerParente.ID, CStr(OrapportNiv1.nIdRapport), oCheckBoxMoisGlissant, ListeControles)

            If bFiltreVisible = True Then
                Dim oBouton As New Button
                oBouton.ID = "btn_refresh" & "_" & DivContainerParente.ID
                oBouton.Attributes.Add("name", "btn_refresh" & "|" & DivContainerParente.ID & "|" & OrapportNiv1.nIdRapport)
                oBouton.CssClass = "btn_refresh"
                oBouton.Text = ""
                oBouton.CausesValidation = True
                sc1.RegisterAsyncPostBackControl(oBouton)
                Dim oDivBtn As New HtmlGenericControl("div")
                oDivBtn.Attributes.Add("class", "BtnActualiser")
                oDivBtn.Controls.Add(oBouton)
                DivCol4.Controls.Add(oDivBtn)
                'Placer actualiser (3=nb filtre par ligne - 30=hauteur par filtre

                Dim oFiltresCalculHauteur As New Filtres
                For Each oFiltreTemp In oFiltres
                    If oFiltreTemp.bEstVisible = True Then
                        oFiltresCalculHauteur.Add(oFiltreTemp)
                    End If
                Next

                If oFiltresCalculHauteur.Count > 3 Then
                    Dim nBaseCalcul As Integer = 1
                    If oFiltresCalculHauteur.Count >= 4 And oFiltresCalculHauteur.Count <= 6 Then
                        nBaseCalcul = 4
                    ElseIf oFiltresCalculHauteur.Count >= 7 And oFiltresCalculHauteur.Count <= 9 Then
                        nBaseCalcul = 8
                    ElseIf oFiltresCalculHauteur.Count >= 10 And oFiltresCalculHauteur.Count <= 12 Then
                        nBaseCalcul = 11
                    ElseIf oFiltresCalculHauteur.Count >= 13 And oFiltresCalculHauteur.Count <= 15 Then
                        nBaseCalcul = 14
                    ElseIf oFiltresCalculHauteur.Count >= 16 And oFiltresCalculHauteur.Count <= 18 Then
                        nBaseCalcul = 17
                    ElseIf oFiltresCalculHauteur.Count >= 19 And oFiltresCalculHauteur.Count <= 21 Then
                        nBaseCalcul = 20
                    ElseIf oFiltresCalculHauteur.Count >= 22 And oFiltresCalculHauteur.Count <= 24 Then
                        nBaseCalcul = 23
                    ElseIf oFiltresCalculHauteur.Count > 24 Then
                        nBaseCalcul = 27
                    End If
                    DivCol4.Attributes.Add("style", "height:" & CInt(nBaseCalcul / 3 * 35) & "Px")
                    oDivBtn.Attributes.Add("style", "top:30%;")
                End If
                If OrapportNiv1.bEstFiltreBoite = False Then DivFiltres.Attributes.Add("style", "float:left;width:100%;margin-bottom:10Px;margin-top:10Px;")
            Else
                If OrapportNiv1.bEstFiltreBoite = False Then DivFiltres.Attributes.Add("style", "float:left;width:100%;margin-bottom:10Px;margin-top:10Px;display: none !important")
            End If
        End If
    End Sub

    Private Sub Creer_Bandeau_Outils(ByRef Div As HtmlGenericControl, ByRef oRapportNiv1 As Rapport, ByVal bEstGraphique As Boolean, ByVal oRapportsSurOnglet As Rapports, ByRef DivContainerParente As HtmlGenericControl, ByVal sListeIdRapports As String)
        Dim DivBoiteOutils As HtmlGenericControl
        Dim sNomIdDivOutils As String = "DivOutils" & "-" & oRapportNiv1.sOngletNiv1.Replace(" ", "") & "-" & oRapportNiv1.sOngletNiv2.Replace(" ", "")
        DivBoiteOutils = New HtmlGenericControl("div")
        DivBoiteOutils.Attributes.Add("class", "DivColonnes clearfix")
        DivBoiteOutils.ID = "idDivBoiteOutils" & sListeIdRapports

        ' PARTIE TRADUCTION DE LA BARRE D'OUTILS
        Dim sLibelleBoiteOutils As String = ""
        Dim sLibelleColonnesAffichees As String = ""
        Dim sLibelleRechercherDansLeTableau As String = ""
        Dim sLibelleSeuil As String = ""
        Dim sLibelleMoisGlissant As String = ""
        Dim sLibelleTTC As String = ""
        Dim sLibelleDecimales As String = ""
        Dim sLibelleNumeroComplet As String = ""
        If oRapportNiv1.oClient.sLangueClient = "US" Then
            sLibelleBoiteOutils = "Tools"
            sLibelleColonnesAffichees = "Columns shown"
            sLibelleRechercherDansLeTableau = "Search in the table"
            sLibelleSeuil = "Threshold"
            sLibelleMoisGlissant = "Rolling months"
            sLibelleTTC = "	including VAT"
            sLibelleDecimales = "Decimal"
            sLibelleNumeroComplet = "Complete phone number"
        Else
            sLibelleBoiteOutils = "Outils"
            sLibelleColonnesAffichees = "Colonnes visibles"
            sLibelleRechercherDansLeTableau = "Rechercher dans le tableau"
            sLibelleSeuil = "Seuil"
            sLibelleMoisGlissant = "Mois glissant"
            sLibelleTTC = "TTC"
            sLibelleDecimales = "Décimales"
            sLibelleNumeroComplet = "Numéro Complet"
        End If

        ' *** Afficher/Masquer la liste *** '
        '<a href="#" onclick="toggle_visibility('DivColonnes');" class="LinkFiltres">Afficher / Masquer les filtres</a>
        Dim sAffichageBandeau As String = "none"
        Dim aLienAfficheMasqueColonnesVisibles As New HtmlGenericControl("a")
        aLienAfficheMasqueColonnesVisibles.Attributes.Add("onclick", "toggle_visibility_tools('" & DivBoiteOutils.ID & "');")


        If Me.Outil.Value = "" Then
            For Each oRapportTest In oRapportsSurOnglet
                If oRapportTest.bEstBoiteOutilOuverte = True Then
                    If Me.Outil.Value = "none" Then
                        Me.Outil.Value &= DivBoiteOutils.ID
                    Else
                        If Not Me.Outil.Value.ToString.Contains(DivBoiteOutils.ID) Then
                            Me.Outil.Value &= "|" & DivBoiteOutils.ID
                        End If
                    End If
                End If
            Next
        End If

        If Me.Outil.Value.ToString.Contains(DivBoiteOutils.ID) Then
            Me.BandeauMasque.Value = DivBoiteOutils.ID
            Me.UpdatePanelEntetes.Update()
        End If

        If Me.BandeauMasque.Value = DivBoiteOutils.ID Then
            sAffichageBandeau = "block"
            aLienAfficheMasqueColonnesVisibles.Attributes.Add("class", "LinkFiltres highlight")
        Else
            aLienAfficheMasqueColonnesVisibles.Attributes.Add("class", "LinkFiltres")
        End If

        aLienAfficheMasqueColonnesVisibles.InnerText = sLibelleBoiteOutils
        DivBoiteOutils.Controls.Add(aLienAfficheMasqueColonnesVisibles)
        Dim oLiteral As New Literal
        oLiteral.Text = "<br />"
        DivBoiteOutils.Controls.Add(oLiteral)

        ' *** Ligne Filtres "Choix des colonnes visibles" ***'
        Dim DivFiltresChoixColsVisibles As HtmlGenericControl
        DivFiltresChoixColsVisibles = New HtmlGenericControl("div")
        DivFiltresChoixColsVisibles.Attributes.Add("class", "clearfix")
        DivFiltresChoixColsVisibles.Attributes.Add("id", sNomIdDivOutils)

        DivFiltresChoixColsVisibles.Style.Add("display", sAffichageBandeau & ";margin-right:10px;margin-left:10px;margin-top:10px;")

        Dim bAfficherExport As Boolean = False
        Dim bAfficherRecherche As Boolean = False
        Dim bAfficherCaseMoisGlissant As Boolean = False
        Dim bAfficherCaseTTC As Boolean = False
        Dim AfficherCaseDecimale As Boolean = False
        Dim bAfficherEnMoisGlissant As Boolean = False
        Dim bAfficherEnTTC As Boolean = False
        Dim bAfficherAvecDecimales As Boolean = False
        Dim bAfficherNumeroEntier As Boolean = False
        Dim bAfficherCaseNumComplet As Boolean = False
        Dim sOutilTop As String = ""
        Dim bAfficherSeuil As Boolean = False

        For Each oRapport In oRapportsSurOnglet
            If oRapport.bAfficherExport = True Then bAfficherExport = True
            If oRapport.bAfficherRecherche = True Then bAfficherRecherche = True
            If oRapport.bAfficherCaseMoisGlissant = True Then bAfficherCaseMoisGlissant = True
            If oRapport.bAfficherCaseTTC = True Then bAfficherCaseTTC = True
            If oRapport.AfficherCaseDecimale = True Then AfficherCaseDecimale = True
            If oRapport.bAfficherEnMoisGlissant = True Then bAfficherEnMoisGlissant = True
            If oRapport.bAfficherEnTTC = True Then bAfficherEnTTC = True
            If oRapport.bAfficherAvecDecimales = True Then bAfficherAvecDecimales = True
            If oRapport.bAfficherNumeroEntier = True Then bAfficherNumeroEntier = True
            If oRapport.bAfficherCaseNumComplet = True Then bAfficherCaseNumComplet = True
            If oRapport.sOutilTop <> "" Then sOutilTop = oRapport.sOutilTop
            If oRapport.bAfficherSeuil = True Then bAfficherSeuil = True
        Next

        ' *** Ligne 2 Filtres ***'
        Dim DivFiltresLigne2 As HtmlGenericControl
        Dim DivCol1Ligne2 As HtmlGenericControl
        Dim DivCol2Ligne2 As HtmlGenericControl
        Dim DivCol3Ligne2 As HtmlGenericControl
        Dim DivCol4Ligne2 As HtmlGenericControl
        Dim DivCol5Ligne2 As HtmlGenericControl
        Dim DivCol6Ligne2 As HtmlGenericControl
        Dim DivCol7Ligne2 As HtmlGenericControl

        DivFiltresLigne2 = New HtmlGenericControl("div")
        DivFiltresLigne2.Attributes.Add("class", "SearchToolsBox clearfix")
        DivFiltresLigne2.Attributes.Add("style", "float:left;width:100%;")

        DivCol1Ligne2 = New HtmlGenericControl("div")
        DivCol1Ligne2.Attributes.Add("class", "col1")
        DivCol2Ligne2 = New HtmlGenericControl("div")
        DivCol2Ligne2.Attributes.Add("class", "col2")
        DivCol3Ligne2 = New HtmlGenericControl("div")
        DivCol3Ligne2.Attributes.Add("class", "col3")
        DivCol4Ligne2 = New HtmlGenericControl("div")
        DivCol4Ligne2.Attributes.Add("class", "col4")
        DivCol5Ligne2 = New HtmlGenericControl("div")
        DivCol5Ligne2.Attributes.Add("class", "col5")
        DivCol6Ligne2 = New HtmlGenericControl("div")
        DivCol6Ligne2.Attributes.Add("class", "col6")
        DivCol7Ligne2 = New HtmlGenericControl("div")
        DivCol7Ligne2.Attributes.Add("class", "col7")

        Dim pCol1Ligne2 As New HtmlGenericControl("p")
        Dim pCol2Ligne2 As New HtmlGenericControl("p")
        Dim pCol3Ligne2 As New HtmlGenericControl("p")
        Dim ulCol4Ligne2 As New HtmlGenericControl("ul")
        Dim pCol5Ligne2 As New HtmlGenericControl("p")
        Dim pCol6Ligne2 As New HtmlGenericControl("p")
        Dim pCol7Ligne2 As New HtmlGenericControl("p")

        ' *** Element enfants direct (<p> et <ul>) ainsi que leurs attributs *** '

        pCol1Ligne2.Attributes.Add("class", "export")
        pCol2Ligne2.Attributes.Add("class", "recherche-tableau")
        pCol3Ligne2.Attributes.Add("class", "checkbox")
        ulCol4Ligne2.Attributes.Add("class", "navigation")
        pCol5Ligne2.Attributes.Add("class", "aller-page")
        pCol6Ligne2.Attributes.Add("class", "filtre-top")
        pCol7Ligne2.Attributes.Add("class", "seuil")
        ' *** Insertion des éléments suivants *** '
        ' Col1


        Dim sIdaCol1Ligne2_2 As String = "idDownloadButtonPdf-" & Toolbox.RecupererListeIdRapports(oRapportNiv1)
        Dim saCol1Ligne2_1 As String = "idDownloadButtonExcel-" & Toolbox.RecupererListeIdRapports(oRapportNiv1)
        If bAfficherExport = True And Page.FindControl(sIdaCol1Ligne2_2) Is Nothing Then
            pCol1Ligne2.InnerText = "Export "
            Dim aCol1Ligne2_1 As New LinkButton
            Dim aCol1Ligne2_2 As New LinkButton

            '--------------
            aCol1Ligne2_1.CausesValidation = False
            aCol1Ligne2_1.ID = saCol1Ligne2_1
            aCol1Ligne2_1.CausesValidation = True
            aCol1Ligne2_1.Attributes.Add("idmodule", oRapportNiv1.oModule.nIdModule.ToString)
            aCol1Ligne2_1.Attributes.Add("idutilisateur", oRapportNiv1.oUtilisateur.nIdUtilisateur.ToString)
            aCol1Ligne2_1.Attributes.Add("idclient", oRapportNiv1.oClient.nIdClient.ToString)
            aCol1Ligne2_1.Attributes.Add("idcontainerParent", DivContainerParente.ID.ToString)
            aCol1Ligne2_1.Attributes.Add("estExcel", "1")
            Me.idDownloadButtonExcel.Value = aCol1Ligne2_1.ID

            aCol1Ligne2_2.CausesValidation = False
            aCol1Ligne2_2.ID = sIdaCol1Ligne2_2
            aCol1Ligne2_2.CausesValidation = True
            aCol1Ligne2_2.Attributes.Add("idmodule", oRapportNiv1.oModule.nIdModule.ToString)
            aCol1Ligne2_2.Attributes.Add("idutilisateur", oRapportNiv1.oUtilisateur.nIdUtilisateur.ToString)
            aCol1Ligne2_2.Attributes.Add("idclient", oRapportNiv1.oClient.nIdClient.ToString)
            aCol1Ligne2_2.Attributes.Add("idcontainerParent", DivContainerParente.ID.ToString)
            aCol1Ligne2_2.Attributes.Add("estExcel", "0")
            Me.idDownloadButtonPDF.Value = aCol1Ligne2_2.ID

            AddHandler aCol1Ligne2_1.Click, AddressOf oDownloadButtonExcel_Click
            AddHandler aCol1Ligne2_2.Click, AddressOf oDownloadButtonExcel_Click
            '----------------------------
            aCol1Ligne2_2.OnClientClick = ""
            aCol1Ligne2_2.ID = sIdaCol1Ligne2_2
            aCol1Ligne2_1.ID = saCol1Ligne2_1
            aCol1Ligne2_2.OnClientClick = ""
            aCol1Ligne2_1.OnClientClick = ""
            aCol1Ligne2_1.CssClass = "export-excel"
            aCol1Ligne2_2.CssClass = "export-pdf"
            aCol1Ligne2_1.Text = "EXCEL"
            aCol1Ligne2_2.Text = "PDF"
            pCol1Ligne2.Controls.Add(aCol1Ligne2_1)
            pCol1Ligne2.Controls.Add(aCol1Ligne2_2)
            DivCol1Ligne2.Controls.Add(pCol1Ligne2)
        End If


        Dim sinputCol2Ligne2 As String = "recherche" & "_" & DivContainerParente.ID & "_" & sListeIdRapports
        If bAfficherRecherche = True And Page.FindControl(sinputCol2Ligne2) Is Nothing Then
            'Col2
            Dim inputCol2Ligne2 As New TextBox
            inputCol2Ligne2.ID = sinputCol2Ligne2
            inputCol2Ligne2.Attributes.Add("name", "recherche" & "_" & DivContainerParente.ID & "_" & sListeIdRapports)
            inputCol2Ligne2.Attributes.Add("onkeydown", "ValoriserRecherche('Recherche','" & inputCol2Ligne2.ID & "')")
            inputCol2Ligne2.Attributes.Add("onpaste", "ValoriserRecherche('Recherche','" & inputCol2Ligne2.ID & "')")
            inputCol2Ligne2.Attributes.Add("oninput", "ValoriserRecherche('Recherche','" & inputCol2Ligne2.ID & "')")
            inputCol2Ligne2.CssClass = "recherche"
            inputCol2Ligne2.Attributes.Add("placeholder", sLibelleRechercherDansLeTableau)

            If UpdatePanelCorps.Page.IsPostBack Then
                Dim sRecherche As String = ""
                If Request.Form("recherche" & "_" & DivContainerParente.ID & "_" & sListeIdRapports) <> Nothing Then
                    inputCol2Ligne2.Text = Request.Form("recherche" & "_" & DivContainerParente.ID & "_" & sListeIdRapports)
                End If
                If Me.Recherche.Value <> "" Then
                    For Each sTexte In Me.Recherche.Value.ToString.Split(CChar("|"))
                        If sTexte.Split(CChar("="))(0) = inputCol2Ligne2.ID Then
                            inputCol2Ligne2.Text = sTexte.Split(CChar("="))(1)

                        End If
                    Next
                End If
            End If

            Dim input2Col2Ligne2 As New ImageButton
            input2Col2Ligne2.ImageUrl = "images/icon-loupe.png"
            input2Col2Ligne2.ID = "loupe" & "_" & DivContainerParente.ID & "--" & sListeIdRapports
            input2Col2Ligne2.AlternateText = "Rechercher"
            pCol2Ligne2.Controls.Add(inputCol2Ligne2)
            pCol2Ligne2.Controls.Add(input2Col2Ligne2)
            DivCol2Ligne2.Controls.Add(pCol2Ligne2)
        End If


        Dim sinputCol7Ligne2 As String = "seuil" & "_" & DivContainerParente.ID & "_" & sListeIdRapports
        If bAfficherSeuil = True And Page.FindControl(sinputCol7Ligne2) Is Nothing Then
            'Col2
            Dim inputCol7Ligne2 As New TextBox
            inputCol7Ligne2.ID = sinputCol7Ligne2
            inputCol7Ligne2.Attributes.Add("name", "seuil" & "_" & DivContainerParente.ID & "_" & sListeIdRapports)
            inputCol7Ligne2.Attributes.Add("onkeyup", "ValoriserSeuil('Seuil','" & inputCol7Ligne2.ID & "')")
            inputCol7Ligne2.Attributes.Add("onpaste", "ValoriserSeuil('Seuil','" & inputCol7Ligne2.ID & "')")
            inputCol7Ligne2.Attributes.Add("oninput", "ValoriserSeuil('Seuil','" & inputCol7Ligne2.ID & "')")
            inputCol7Ligne2.CssClass = "seuil"
            inputCol7Ligne2.Attributes.Add("placeholder", sLibelleSeuil)



            If Request.Form("seuil" & "_" & DivContainerParente.ID & "_" & sListeIdRapports) <> Nothing Then
                Dim oAncienMomoireSeuil As New MemoireSeuil
                oAncienMomoireSeuil.nIdClient = oRapportsSurOnglet(0).oClient.nIdClient
                oAncienMomoireSeuil.nIdModule = oRapportsSurOnglet(0).oModule.nIdModule
                oAncienMomoireSeuil.nIdRapport = oRapportsSurOnglet(0).nIdRapport
                oAncienMomoireSeuil.nIdUtilisateur = oRapportsSurOnglet(0).oUtilisateur.nIdUtilisateur
                oAncienMomoireSeuil.nIdUtilisateurSecondaire = CInt(Session.Item("idUtilisateurSecondaire"))
                oAncienMomoireSeuil.chargerValeurMemoireSeuil()

                Dim oNouveauMemoireSeuil As New MemoireSeuil
                oNouveauMemoireSeuil.nIdClient = oRapportsSurOnglet(0).oClient.nIdClient
                oNouveauMemoireSeuil.nIdModule = oRapportsSurOnglet(0).oModule.nIdModule
                oNouveauMemoireSeuil.nIdRapport = oRapportsSurOnglet(0).nIdRapport
                oNouveauMemoireSeuil.nIdUtilisateur = oRapportsSurOnglet(0).oUtilisateur.nIdUtilisateur
                oNouveauMemoireSeuil.nIdUtilisateurSecondaire = CInt(Session.Item("idUtilisateurSecondaire"))
                oNouveauMemoireSeuil.sValeur = Request.Form("seuil" & "_" & DivContainerParente.ID & "_" & sListeIdRapports)


                If oNouveauMemoireSeuil.sValeur <> oAncienMomoireSeuil.sValeur Then
                    If oNouveauMemoireSeuil.sValeur = "" Then
                        oNouveauMemoireSeuil.supprimerValeurMemoireSeuil()
                    Else
                        oNouveauMemoireSeuil.ecrireValeurMemoireSeuil()
                    End If

                End If
            Else
                If Me.Seuil.Value = "VIDE" Then
                    Dim oNouveauMemoireSeuil As New MemoireSeuil
                    oNouveauMemoireSeuil.nIdClient = oRapportsSurOnglet(0).oClient.nIdClient
                    oNouveauMemoireSeuil.nIdModule = oRapportsSurOnglet(0).oModule.nIdModule
                    oNouveauMemoireSeuil.nIdRapport = oRapportsSurOnglet(0).nIdRapport
                    oNouveauMemoireSeuil.nIdUtilisateur = oRapportsSurOnglet(0).oUtilisateur.nIdUtilisateur
                    oNouveauMemoireSeuil.nIdUtilisateurSecondaire = CInt(Session.Item("idUtilisateurSecondaire"))
                    oNouveauMemoireSeuil.supprimerValeurMemoireSeuil()
                    Me.Seuil.Value = ""
                End If
            End If




            'If UpdatePanelCorps.Page.IsPostBack Then
            Dim oMemoireSeuil As New MemoireSeuil
            oMemoireSeuil.nIdClient = oRapportsSurOnglet(0).oClient.nIdClient
            oMemoireSeuil.nIdModule = oRapportsSurOnglet(0).oModule.nIdModule
            oMemoireSeuil.nIdRapport = oRapportsSurOnglet(0).nIdRapport
            oMemoireSeuil.nIdUtilisateur = oRapportsSurOnglet(0).oUtilisateur.nIdUtilisateur
            oMemoireSeuil.nIdUtilisateurSecondaire = CInt(Session.Item("idUtilisateurSecondaire"))
            oMemoireSeuil.chargerValeurMemoireSeuil()

            inputCol7Ligne2.Text = oMemoireSeuil.sValeur
            'Dim sSeuil As String = ""
            'If Request.Form("seuil" & "_" & DivContainerParente.ID & "_" & sListeIdRapports) <> Nothing Then
            '    inputCol7Ligne2.Text = Request.Form("seuil" & "_" & DivContainerParente.ID & "_" & sListeIdRapports)
            'End If
            'If Me.Seuil.Value <> "" Then
            '    For Each sTexte In Me.Seuil.Value.ToString.Split(CChar("|"))
            '        If sTexte.Split(CChar("="))(0) = inputCol7Ligne2.ID Then
            '            inputCol7Ligne2.Text = sTexte.Split(CChar("="))(1)

            '        End If
            '    Next
            'End If
            'End If

            Dim input2Col7Ligne2 As New ImageButton
            input2Col7Ligne2.ImageUrl = "images/icon-seuil.png"
            input2Col7Ligne2.ID = "iconeseuil" & "_" & DivContainerParente.ID & "--" & sListeIdRapports
            input2Col7Ligne2.AlternateText = "Fixer le seuil"
            pCol7Ligne2.Controls.Add(inputCol7Ligne2)
            pCol7Ligne2.Controls.Add(input2Col7Ligne2)
            DivCol7Ligne2.Controls.Add(pCol7Ligne2)
        End If



        Dim sDropDownListOutilTop As String = "dropdownlist_OutilTop" & "_" & DivContainerParente.ID & "--" & sListeIdRapports
        If sOutilTop <> "" And Page.FindControl(sDropDownListOutilTop) Is Nothing Then
            Dim spanOutilTop As New HtmlGenericControl("span")
            ' LA DropdownList
            Dim dropdownListOutilTop As New DropDownList
            dropdownListOutilTop.ID = sDropDownListOutilTop

            dropdownListOutilTop.Attributes.Add("class", "styled")
            dropdownListOutilTop.Attributes.Add("style", "width:80px")
            dropdownListOutilTop.Attributes.Add("name", "dropdownlist_OutilTop" & "_" & DivContainerParente.ID & "--" & sListeIdRapports)

            dropdownListOutilTop.CausesValidation = True
            dropdownListOutilTop.AutoPostBack = True
            dropdownListOutilTop.EnableViewState = False

            For Each sLibelle In sOutilTop.Split(CChar("|"))(0).Split(CChar(";"))
                dropdownListOutilTop.Items.Add(Toolbox.FormaterFiltreTop(sLibelle, "Ajouter", oRapportNiv1.oClient))
            Next

            If Not UpdatePanelCorps.Page.IsPostBack Then
                If sOutilTop <> "" Then
                    dropdownListOutilTop.SelectedValue = Toolbox.FormaterFiltreTop(sOutilTop.Split(CChar("|"))(1), "Ajouter", oRapportNiv1.oClient)
                    Me.OutilTop.Value = Toolbox.FormaterFiltreTop(sOutilTop.Split(CChar("|"))(1), "Ajouter", oRapportNiv1.oClient)
                End If
            Else
                If Me.OutilTop.Value.ToUpper <> "" Then
                    dropdownListOutilTop.SelectedValue = Me.OutilTop.Value
                Else
                    dropdownListOutilTop.SelectedValue = Toolbox.FormaterFiltreTop(sOutilTop.Split(CChar("|"))(1), "Ajouter", oRapportNiv1.oClient)
                    Me.OutilTop.Value = Toolbox.FormaterFiltreTop(sOutilTop.Split(CChar("|"))(1), "Ajouter", oRapportNiv1.oClient)
                End If
            End If
            dropdownListOutilTop.Attributes.Add("onclick", "ValoriserDropdownList('OutilTop','" & dropdownListOutilTop.ID & "');")
            spanOutilTop.Controls.Add(dropdownListOutilTop)


            DivCol6Ligne2.Controls.Add(spanOutilTop)
        End If

        'Col3 (Mois Glissant)
        Dim scheckBoxMoisGlissantCol3Ligne2 As String = "checkbox_MoisGlissant" & "_" & DivContainerParente.ID & "--" & sListeIdRapports
        If bAfficherCaseMoisGlissant = True And Page.FindControl(scheckBoxMoisGlissantCol3Ligne2) Is Nothing Then
            Dim spanMoisGlissant As New HtmlGenericControl("span")
            ' LA CHECKBOX
            Dim checkBoxMoisGlissantCol3Ligne2 As New CheckBox
            checkBoxMoisGlissantCol3Ligne2.ID = scheckBoxMoisGlissantCol3Ligne2
            ' LE LIBELLE
            Dim olblMoisGlissantCol3Ligne2 As New HtmlGenericControl("label")
            olblMoisGlissantCol3Ligne2.Attributes.Add("for", "checkbox_MoisGlissant" & "_" & DivContainerParente.ID & "--" & sListeIdRapports)
            olblMoisGlissantCol3Ligne2.InnerText = sLibelleMoisGlissant

            checkBoxMoisGlissantCol3Ligne2.CausesValidation = True
            checkBoxMoisGlissantCol3Ligne2.AutoPostBack = True
            checkBoxMoisGlissantCol3Ligne2.EnableViewState = False

            If Not UpdatePanelCorps.Page.IsPostBack Then
                If bAfficherEnMoisGlissant = True Then
                    checkBoxMoisGlissantCol3Ligne2.Checked = True
                End If
            Else
                If Me.MoisGlissant.Value.ToUpper = "TRUE" Then
                    checkBoxMoisGlissantCol3Ligne2.Checked = True
                ElseIf Me.MoisGlissant.Value.ToUpper = "FALSE" Then
                    checkBoxMoisGlissantCol3Ligne2.Checked = False
                End If
            End If
            checkBoxMoisGlissantCol3Ligne2.Attributes.Add("onclick", "ValoriserCheckbox('MoisGlissant','" & checkBoxMoisGlissantCol3Ligne2.ID & "');")

            spanMoisGlissant.Controls.Add(checkBoxMoisGlissantCol3Ligne2)
            spanMoisGlissant.Controls.Add(olblMoisGlissantCol3Ligne2)

            pCol3Ligne2.Controls.Add(spanMoisGlissant)
        End If
        '----TTC
        Dim scheckBoxTTCCol3Ligne2 As String = "checkbox_TTC" & "_" & DivContainerParente.ID & "--" & sListeIdRapports
        If bAfficherCaseTTC = True And Page.FindControl(scheckBoxTTCCol3Ligne2) Is Nothing Then
            Dim spanTTC As New HtmlGenericControl("span")
            Dim checkBoxTTCCol3Ligne2 As New CheckBox()
            checkBoxTTCCol3Ligne2.ID = scheckBoxTTCCol3Ligne2
            checkBoxTTCCol3Ligne2.AutoPostBack = True
            checkBoxTTCCol3Ligne2.CausesValidation = True

            checkBoxTTCCol3Ligne2.Attributes.Add("onclick", "ValoriserCheckbox('TTC','" & checkBoxTTCCol3Ligne2.ID & "');")

            Dim olblTTCCol3Ligne2 As New HtmlGenericControl("label")
            olblTTCCol3Ligne2.Attributes.Add("for", "checkbox_TTC" & "_" & DivContainerParente.ID & "--" & sListeIdRapports)
            olblTTCCol3Ligne2.InnerText = sLibelleTTC

            If Not UpdatePanelCorps.Page.IsPostBack Then
                If bAfficherEnTTC = True Then
                    checkBoxTTCCol3Ligne2.Checked = True
                End If
            Else
                'If Request.Form("checkbox_TTC" & "_" & DivContainerParente.ID & "--" & sListeIdRapports) = "on" Then
                '    checkBoxTTCCol3Ligne2.Checked = True
                'Else
                '    checkBoxTTCCol3Ligne2.Checked = False
                'End If
                'If Me.TTC.Value.ToUpper = "TRUE" Then
                '    checkBoxTTCCol3Ligne2.Checked = True
                'ElseIf Me.TTC.Value.ToUpper = "FALSE" Then
                '    checkBoxTTCCol3Ligne2.Checked = False
                'End If
                If Request.Form("checkbox_TTC" & "_" & DivContainerParente.ID & "--" & sListeIdRapports) = "on" Then
                    checkBoxTTCCol3Ligne2.Checked = True
                Else
                    If Request.Form("checkbox_TTC" & "_" & DivContainerParente.ID & "--" & sListeIdRapports) = Nothing Then
                        If bAfficherEnTTC = True Then
                            checkBoxTTCCol3Ligne2.Checked = True
                        End If
                    Else
                        checkBoxTTCCol3Ligne2.Checked = False
                    End If

                End If
                If Me.TTC.Value.ToUpper = "TRUE" Then
                    checkBoxTTCCol3Ligne2.Checked = True
                ElseIf Me.TTC.Value.ToUpper = "FALSE" Then
                    checkBoxTTCCol3Ligne2.Checked = False
                End If
            End If

            spanTTC.Controls.Add(checkBoxTTCCol3Ligne2)
            spanTTC.Controls.Add(olblTTCCol3Ligne2)

            pCol3Ligne2.Controls.Add(spanTTC)
        End If

        '---Decimale
        Dim scheckBoxDecimalesCol3Ligne2 As String = "checkboxAfficherDecimale" & "_" & DivContainerParente.ID & "--" & sListeIdRapports
        If AfficherCaseDecimale = True And Page.FindControl(scheckBoxDecimalesCol3Ligne2) Is Nothing Then
            Dim spanDecimales As New HtmlGenericControl("span")
            Dim checkBoxDecimalesCol3Ligne2 As New CheckBox
            checkBoxDecimalesCol3Ligne2.ID = scheckBoxDecimalesCol3Ligne2
            checkBoxDecimalesCol3Ligne2.AutoPostBack = True
            checkBoxDecimalesCol3Ligne2.CausesValidation = True

            Dim olblDecimalesCol3Ligne2 As New HtmlGenericControl("label")
            olblDecimalesCol3Ligne2.Attributes.Add("for", "checkboxAfficherDecimale" & "_" & DivContainerParente.ID & "--" & sListeIdRapports)
            olblDecimalesCol3Ligne2.InnerText = sLibelleDecimales

            checkBoxDecimalesCol3Ligne2.Attributes.Add("onclick", "ValoriserCheckbox('Decimale','" & checkBoxDecimalesCol3Ligne2.ID & "');")

            If Not UpdatePanelCorps.Page.IsPostBack Then
                If bAfficherAvecDecimales = True Then
                    checkBoxDecimalesCol3Ligne2.Checked = True
                End If
            Else
                If Request.Form("checkboxAfficherDecimale" & "_" & DivContainerParente.ID & "--" & sListeIdRapports) = "on" Then
                    checkBoxDecimalesCol3Ligne2.Checked = True
                Else

                    If Request.Form("checkboxAfficherDecimale" & "_" & DivContainerParente.ID & "--" & sListeIdRapports) = Nothing Then
                        If bAfficherAvecDecimales = True Then
                            checkBoxDecimalesCol3Ligne2.Checked = True
                        End If
                    Else
                        checkBoxDecimalesCol3Ligne2.Checked = False
                    End If
                End If
                If Me.Decimale.Value.ToUpper = "TRUE" Then
                    checkBoxDecimalesCol3Ligne2.Checked = True
                ElseIf Me.Decimale.Value.ToUpper = "FALSE" Then
                    checkBoxDecimalesCol3Ligne2.Checked = False
                End If
            End If

            spanDecimales.Controls.Add(checkBoxDecimalesCol3Ligne2)
            spanDecimales.Controls.Add(olblDecimalesCol3Ligne2)

            pCol3Ligne2.Controls.Add(spanDecimales)
        End If

        '---Num complet
        Dim scheckBoxAfficherCaseNumComplet As String = "checkBoxAfficherCaseNumComplet" & "_" & DivContainerParente.ID & "--" & sListeIdRapports
        If bAfficherCaseNumComplet = True And Page.FindControl(scheckBoxAfficherCaseNumComplet) Is Nothing Then
            Dim spanNumComplet As New HtmlGenericControl("span")
            Dim checkBoxAfficherCaseNumComplet As New CheckBox
            checkBoxAfficherCaseNumComplet.ID = scheckBoxAfficherCaseNumComplet
            checkBoxAfficherCaseNumComplet.AutoPostBack = True
            checkBoxAfficherCaseNumComplet.CausesValidation = True

            Dim olblDecimalesCol3Ligne2 As New HtmlGenericControl("label")
            olblDecimalesCol3Ligne2.Attributes.Add("for", "checkBoxAfficherCaseNumComplet" & "_" & DivContainerParente.ID & "--" & sListeIdRapports)
            olblDecimalesCol3Ligne2.InnerText = sLibelleNumeroComplet

            checkBoxAfficherCaseNumComplet.Attributes.Add("onclick", "ValoriserCheckbox('NumeroEntier','" & checkBoxAfficherCaseNumComplet.ID & "');")

            spanNumComplet.Controls.Add(checkBoxAfficherCaseNumComplet)
            spanNumComplet.Controls.Add(olblDecimalesCol3Ligne2)

            pCol3Ligne2.Controls.Add(spanNumComplet)

            If Not UpdatePanelCorps.Page.IsPostBack Then
                If bAfficherNumeroEntier = True Then
                    checkBoxAfficherCaseNumComplet.Checked = True
                End If
            Else
                If Request.Form("checkBoxAfficherCaseNumComplet" & "_" & DivContainerParente.ID & "--" & sListeIdRapports) = "on" Then
                    checkBoxAfficherCaseNumComplet.Checked = True
                Else
                    If Request.Form("checkBoxAfficherCaseNumComplet" & "_" & DivContainerParente.ID & "--" & sListeIdRapports) = Nothing Then
                        If bAfficherNumeroEntier = True Then
                            checkBoxAfficherCaseNumComplet.Checked = True
                        End If
                    Else
                        checkBoxAfficherCaseNumComplet.Checked = False
                    End If

                End If
                If Me.NumeroEntier.Value.ToUpper = "TRUE" Then
                    checkBoxAfficherCaseNumComplet.Checked = True
                ElseIf Me.NumeroEntier.Value.ToUpper = "FALSE" Then
                    checkBoxAfficherCaseNumComplet.Checked = False
                End If
            End If
        End If

        If bAfficherCaseMoisGlissant = True Or bAfficherCaseTTC = True Or AfficherCaseDecimale = True Or bAfficherCaseNumComplet = True Then
            DivCol3Ligne2.Controls.Add(pCol3Ligne2)
        End If

        ' *** Insertion des éléments parents dans la DIV *** '
        If DivCol1Ligne2.Controls.Count > 0 Then DivFiltresLigne2.Controls.Add(DivCol1Ligne2)
        If DivCol2Ligne2.Controls.Count > 0 Then DivFiltresLigne2.Controls.Add(DivCol2Ligne2)
        If DivCol7Ligne2.Controls.Count > 0 Then DivFiltresLigne2.Controls.Add(DivCol7Ligne2)
        If DivCol6Ligne2.Controls.Count > 0 Then DivFiltresLigne2.Controls.Add(DivCol6Ligne2)
        If DivCol3Ligne2.Controls.Count > 0 Then DivFiltresLigne2.Controls.Add(DivCol3Ligne2)
        If DivCol4Ligne2.Controls.Count > 0 Then DivFiltresLigne2.Controls.Add(DivCol4Ligne2)
        If DivCol5Ligne2.Controls.Count > 0 Then DivFiltresLigne2.Controls.Add(DivCol5Ligne2)

        DivFiltresChoixColsVisibles.Controls.Add(DivFiltresLigne2)

        '-------------------------------------------

        Dim DivCvLeft As New HtmlGenericControl("div")
        Dim DivCvRight As New HtmlGenericControl("div")

        DivCvLeft.Attributes.Add("class", "cv-left")
        DivCvRight.Attributes.Add("class", "cv-right")

        Dim pDivCvLeft As New HtmlGenericControl("p")
        Dim pDivCvRight As New HtmlGenericControl("p")

        DivCvLeft.Controls.Add(pDivCvLeft)
        DivCvRight.Controls.Add(pDivCvRight)
        pDivCvLeft.Attributes.Add("class", "checkbox")
        pDivCvRight.Attributes.Add("class", "checkbox")

        Dim spanLbl2 As New HtmlGenericControl("span")
        spanLbl2.Attributes.Add("style", "line-height:20Px;")
        spanLbl2.InnerText = sLibelleColonnesAffichees + " : "
        If bEstGraphique <> True Then
            pDivCvLeft.Controls.Add(spanLbl2)
        End If

        'Récupérer la liste des colonnes à afficher pour cet utilisateur dans la bdd
        Dim oColonnes As New Colonnes()
        Dim oCheckboxMoisGlissant As CheckBox = CType(FindControl("checkbox_MoisGlissant" & "_" & "_" & DivContainerParente.ID & "--" & sListeIdRapports), CheckBox)
        oColonnes.ChargerListeColonnesParRapports(oRapportsSurOnglet, oCheckboxMoisGlissant, DivContainerParente.ID, Me.BloquerColoneAction)

        For Each el As Colonne In oColonnes
            Dim oSpan As New HtmlGenericControl("span")
            Dim soCheckBoxFiltreVisible As String = "CkList" & "_" & DivContainerParente.ID & "--" & sListeIdRapports & "_" & oRapportNiv1.oClient.nIdClient & "_" &
                oRapportNiv1.oUtilisateur.nIdUtilisateur & "_" & oRapportNiv1.oModule.nIdModule & "_" & el.nIdColonne.ToString
            If el.bEstMasquable = True And Page.FindControl(soCheckBoxFiltreVisible) Is Nothing Then

                ' LA CHECKBOX
                Dim oCheckBoxFiltreVisible As New CheckBox()
                oCheckBoxFiltreVisible.ID = soCheckBoxFiltreVisible

                ' LE LIBELLE
                Dim oLabel As New HtmlGenericControl("label")
                oLabel.Attributes.Add("for", oCheckBoxFiltreVisible.ID)
                oLabel.InnerText = el.sEnteteNiveau1

                AddHandler oCheckBoxFiltreVisible.CheckedChanged, AddressOf oCheckBoxFiltreVisible_CheckedChanged
                oCheckBoxFiltreVisible.CausesValidation = True
                oCheckBoxFiltreVisible.AutoPostBack = True
                oCheckBoxFiltreVisible.EnableViewState = False

                For Each oRapport In el.oRapports
                    Dim oParamRapportColonne As ParamRapportColonne = New ParamRapportColonne(oRapport, el)
                    oParamRapportColonne.affichageParamRapportColonne()
                    If oParamRapportColonne.bAfficher <> Nothing Then
                        oCheckBoxFiltreVisible.Checked = oParamRapportColonne.bAfficher
                    End If
                Next
                oSpan.Controls.Add(oCheckBoxFiltreVisible)
                oSpan.Controls.Add(oLabel)

                pDivCvRight.Controls.Add(oSpan)
            End If
        Next

        If pDivCvRight.Controls.Count > 0 Then
            DivFiltresChoixColsVisibles.Controls.Add(DivCvLeft)
            DivFiltresChoixColsVisibles.Controls.Add(DivCvRight)
        End If

        'DivFiltresChoixColsVisibles.Controls.Add(divExport)

        If pDivCvLeft.Controls.Count > 1 Or DivFiltresLigne2.Controls.Count > 0 Then
            DivBoiteOutils.Controls.Add(DivFiltresChoixColsVisibles)
            Div.Controls.Add(DivBoiteOutils)
        End If

    End Sub

    Private Sub oDownloadButtonExcel_Click(ByVal sender As Object, ByVal e As EventArgs)

        'Récupération des éléments permettant la création de la requête
        Dim oDownloadButton As LinkButton = CType(sender, LinkButton)
        'Split des IDs afin de les parcourir pour récupérer toutes les requêtes et afficher leurs résultats dans le fichier excel
        Dim sIdRapport() As String = oDownloadButton.ID.Substring(oDownloadButton.ID.IndexOf(CChar("-")) + 1).Split(CChar("|"))
        Dim oListeRequetes As New List(Of KeyValuePair(Of Rapport, List(Of String)))
        Dim oListeColonnes As New List(Of KeyValuePair(Of Rapport, Colonnes))
        Dim oListeKeyPairFiltreValeur As New List(Of KeyValuePair(Of String, String))
        Dim bEstExcel As Boolean = CBool(oDownloadButton.Attributes("estExcel"))

        Dim oCheckboxMoisGlissant As CheckBox
        Dim oCheckboxTTC As CheckBox = Nothing
        Dim oCheckboxDecimales As CheckBox = Nothing


        Dim idModule As Integer
        Dim oModule As New Modul
        Dim oUtilisateur As New Utilisateur()
        Dim oClient As New Client

        oClient.chargerClientParId(CInt(User.Identity.Name.Split(CChar("_"))(1)))

        idModule = CInt(oDownloadButton.Attributes("idmodule"))
        oModule.chargerModulesParId(idModule, oClient)
        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))

        Dim oUtilisateurSecondaireFiltres As New UtilisateurSecondaireFiltres()
        If Session.Item("idUtilisateurSecondaire") IsNot Nothing And Session.Item("idUtilisateurSecondaire").ToString <> "0" Then
            Dim oUtilisateurSecondaire As New UtilisateurSecondaire()
            oUtilisateurSecondaire.Req_Infos_Utilisateur_Secondaire(CInt(Session.Item("idUtilisateurSecondaire").ToString), CInt(User.Identity.Name.Split(CChar("_"))(1)))
            oUtilisateurSecondaireFiltres.chargerUtilisateurSecondaireFiltres(oUtilisateurSecondaire)
        End If



        'Récupérer l'id de la div
        Dim Id_Div As String = oDownloadButton.Attributes("idcontainerparent")
        Dim sListeIdRapports As String = ""
        Dim oRapports As New Rapports
        Dim oRapport As New Rapport
        oRapport.chargerRappportParModuleUtilisateurIdRapport(oModule.nIdModule, oUtilisateur.nIdUtilisateur, CInt(sIdRapport(0)), oUtilisateur.oClient.nIdClient)
        oRapports.chargerRappportsParModuleUtilisateurIdRapport(oRapport.oModule.nIdModule, oRapport.oUtilisateur.nIdUtilisateur, oRapport.nIdRapport, oUtilisateur.oClient.nIdClient)

        oRapport = Nothing

        For i As Integer = 0 To (sIdRapport.Count - 1)

            'Dim sGridId As String
            'Dim sRapport = oDownloadButtonExcel.Attributes("sRapport").ToString

            'Création de la requête
            Dim oColonnes As New Colonnes
            oRapport = New Rapport()

            oRapport.chargerRappportParModuleUtilisateurIdRapport(oModule.nIdModule, oUtilisateur.nIdUtilisateur, CInt(sIdRapport(i)), oUtilisateur.oClient.nIdClient)



            Dim oRapportsTraitement As New Rapports
            For Each oRapportActuel In oRapports
                If oRapport.nOngletOrdreNiv1 = oRapportActuel.nOngletOrdreNiv1 And oRapport.nOngletOrdreNiv2 = oRapportActuel.nOngletOrdreNiv2 Then
                    oRapportsTraitement.Add(oRapport)
                End If
            Next



            sListeIdRapports = Toolbox.RecupererListeIdRapports(oRapportsTraitement(0))


            'Récupérer les valeurs des checkbox
            oCheckboxMoisGlissant = CType(FindControl("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports), CheckBox)
            If Not Page.Request("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports) Is Nothing Then
                If Page.Request("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports) = "on" Then
                    oCheckboxMoisGlissant.Checked = True
                Else
                    oCheckboxMoisGlissant.Checked = False
                End If
            End If

            oCheckboxTTC = CType(FindControl("checkbox_TTC" & "_" & Id_Div & "--" & sListeIdRapports), CheckBox)
            If Not Page.Request("checkbox_TTC" & "_" & Id_Div & "--" & sListeIdRapports) Is Nothing Then
                If Page.Request("checkbox_TTC" & "_" & Id_Div & "--" & sListeIdRapports) = "on" Then
                    oCheckboxTTC.Checked = True
                Else
                    oCheckboxTTC.Checked = False
                End If
            End If

            oCheckboxDecimales = CType(FindControl("checkboxAfficherDecimale" & "_" & Id_Div & "--" & sListeIdRapports), CheckBox)
            If Not Page.Request("checkboxAfficherDecimale" & "_" & Id_Div & "--" & sListeIdRapports) Is Nothing Then
                If Page.Request("checkboxAfficherDecimale" & "_" & Id_Div & "--" & sListeIdRapports) = "on" Then
                    oCheckboxDecimales.Checked = True
                Else
                    oCheckboxDecimales.Checked = False
                End If
            End If

            oColonnes.ChargerListeColonnes(oRapport, oCheckboxMoisGlissant, Id_Div, Me.BloquerColoneAction)

            Dim oFiltres As New Filtres
            oFiltres.chargerFiltresParRapports(oRapportsTraitement, oRapport, oUtilisateurSecondaireFiltres)
            Dim ListeControles As New List(Of Object)
            Dim listeControlesIndice As UShort = 0
            For Each ofiltre In oFiltres
                If Not IsNothing(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports)) Then
                    If TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.DropDownList Then
                        ListeControles.Add(CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), DropDownList))
                    ElseIf TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.ListBox Then
                        ListeControles.Add(CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), ListBox))
                    End If
                End If
            Next

            If i = 0 Then
                For nDepartFiltre = 0 To (oFiltres.Count - 1) Step 1
                    Dim sValeurFiltre As String = ""
                    If (TypeOf ListeControles(nDepartFiltre) Is System.Web.UI.WebControls.DropDownList) Then
                        sValeurFiltre = CType(ListeControles(nDepartFiltre), DropDownList).SelectedValue
                    ElseIf (TypeOf ListeControles(nDepartFiltre) Is System.Web.UI.WebControls.ListBox) Then
                        sValeurFiltre = CType(ListeControles(nDepartFiltre), ListBox).SelectedValue
                    End If
                    oListeKeyPairFiltreValeur.Add(New KeyValuePair(Of String, String)(oFiltres(nDepartFiltre).sLibelleFiltre, sValeurFiltre))
                Next
            End If

            'Dim oSqlDataSource As SqlDataSource = CType(Page.FindControl("SqlDataSource" & oRapport.nIdRapport), SqlDataSource)
            Dim Div As HtmlGenericControl = CType(FindControl(Id_Div), HtmlGenericControl)


            '---Creation OrderBy
            Dim sOrder As String = " ORDER BY "
            If Me.ListeTriTableau.Value <> "" Then
                'Grid_Dépenses__générales_Dépenses__générales_Rapport1\Type_Prestation\asc
                Dim idGrid As String = Replace("Grid_" & oRapport.sSommaireNiv1 & "_" & oRapport.sSommaireNiv2 & "_Rapport" & CStr(oRapport.nIdRapport), " ", "__").Replace("-", "ù").Replace("'", "")
                Dim sTable() As String = Me.ListeTriTableau.Value.Split(CChar("\"))
                For j = 0 To sTable.Length - 1 Step 3
                    If sTable(j) = idGrid Then
                        sOrder += "[" & sTable(j + 1) & "] " & sTable(j + 2) & ","
                    End If
                Next
            End If
            If sOrder <> " ORDER BY " Then
                sOrder = sOrder.Remove(sOrder.Length - 1, 1)
            Else
                sOrder = ""
            End If


            'TODO - AVEC LE TRAITEMENT A REALISER SUR LE FICHIER EXCEL A PARTIR DES ELEMENTS DE CHAQUE REQUETE RETOURNEE
            Dim sRequete As New List(Of String)

            For j = 0 To 2
                If cDal.estGraphique(oRapport) Then
                    sRequete.Add(Server.MapPath("\tempimg\U" & User.Identity.Name.Split(CChar("_"))(0) & "_M" & CStr(oRapport.oModule.nIdModule) & "_R" & CStr(oRapport.nIdRapport) & ".png"))
                Else
                    If j = 0 Then
                        sRequete.Add(cDal.Creer_Requete_Tableau(oRapport, oColonnes, ListeControles, Div, oCheckboxTTC, oCheckboxMoisGlissant, Me.OutilTop.Value, Me.ListeTriTableau.Value, Me.TriActif.Value, Me.BloquerColoneAction, oUtilisateurSecondaireFiltres)(j) & sOrder)
                    Else
                        sRequete.Add(cDal.Creer_Requete_Tableau(oRapport, oColonnes, ListeControles, Div, oCheckboxTTC, oCheckboxMoisGlissant, Me.OutilTop.Value, Me.ListeTriTableau.Value, Me.TriActif.Value, Me.BloquerColoneAction, oUtilisateurSecondaireFiltres)(j))
                    End If

                End If

            Next


            For Each sRequeteCdal In cDal.Creer_Requete_Tableau(oRapport, oColonnes, ListeControles, Div, oCheckboxTTC, oCheckboxMoisGlissant, Me.OutilTop.Value, Me.ListeTriTableau.Value, Me.TriActif.Value, Me.BloquerColoneAction, oUtilisateurSecondaireFiltres)
                If cDal.estGraphique(oRapport) Then
                    sRequete.Add(Server.MapPath("\tempimg\U" & User.Identity.Name.Split(CChar("_"))(0) & "_M" & CStr(oRapport.oModule.nIdModule) & "_R" & CStr(oRapport.nIdRapport) & ".png"))
                Else
                    sRequete.Add(sRequeteCdal)
                End If

            Next

            'Ajout de la requete dans le tableau de string
            oListeRequetes.Add(New KeyValuePair(Of Rapport, List(Of String))(oRapport, sRequete))
            oListeColonnes.Add(New KeyValuePair(Of Rapport, Colonnes)(oRapport, oColonnes))

        Next

        CreerExcel(oListeRequetes, oListeColonnes, oCheckboxDecimales, oListeKeyPairFiltreValeur, oCheckboxTTC, bEstExcel)

    End Sub



    Private Sub oCheckBoxFiltreVisible_CheckedChanged(sender As Object, e As EventArgs)
        Dim oCheckBoxFiltreVisible As CheckBox = CType(sender, CheckBox)
        ' Récupération des informations selon les id concaténées dans l'id de la checkbox
        Dim nTest As Integer = oCheckBoxFiltreVisible.ID.IndexOf("--")
        Dim sTest As String = oCheckBoxFiltreVisible.ID.Substring(CType(nTest + 2, Integer))

        'Liste des éléments du tableau de string : 0->idRapport - 1->idClient - 2-> idUtilisateur - 3->idModule - 4->idColonne
        Dim str() As String = Split(sTest, "_")

        Dim ListeRapport As String = str(0)
        For Each Id_Rapport_Niv1 In ListeRapport.Split(CChar("|"))

            Dim oParamRapportColonne As New ParamRapportColonne(New Rapport(CInt(Id_Rapport_Niv1), New Client(CInt(str(1))), New Utilisateur(CInt(str(2))), New Modul(CShort(str(3)))), New Colonne(CInt(str(4))), oCheckBoxFiltreVisible.Checked)
            oParamRapportColonne.SauvegardeEtatCheckboxChoixVisible()

            Dim Id_Div As String = CType(sender, CheckBox).ID.Replace("--", "*").Split(CChar("*"))(0).Replace("CkList_", "")
            Dim idModule As String = ""
            Dim OrapportNiv1 As New Rapport
            idModule = CStr(Request.Item("module"))
            OrapportNiv1.chargerRappportParModuleUtilisateurIdRapport(CInt(idModule), CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(Id_Rapport_Niv1), CInt(User.Identity.Name.Split(CChar("_"))(1)))
            Dim Div As HtmlGenericControl = CType(Page.FindControl(Id_Div), HtmlGenericControl)
            Dim oGrid As GridView = CType(Page.FindControl(Replace("Grid_" & OrapportNiv1.sSommaireNiv1 & "_" & OrapportNiv1.sSommaireNiv2 & "_Rapport" & CStr(OrapportNiv1.nIdRapport), " ", "__").Replace("-", "ù").Replace("'", "")), GridView)

            If Not oGrid Is Nothing Then
                'Liste des éléments du tableau de string : 0->idRapport - 1->idClient - 2-> idUtilisateur - 3->idModule - 4->idColonne
                Dim oCheckbox As CheckBox = CType(Page.FindControl("CkList_" & Div.ID & "--" & str(0) & "_" & str(1) & "_" &
                                                                       str(2) & "_" & str(3) & "_" & str(4)), CheckBox)
                Dim oColonnes As New Colonnes
                Dim sNomColonneRetrait As String = ""
                Dim oCheckboxMoisGlissant As CheckBox = CType(FindControl("checkbox_MoisGlissant" & "_" & Div.ID & "--" & CStr(OrapportNiv1.nIdRapport)), CheckBox)
                oColonnes.ChargerListeColonnes(OrapportNiv1, oCheckboxMoisGlissant, Id_Div, Me.BloquerColoneAction)

                Dim iLargeurTableau As Integer = 0

                Dim i As Integer = 0
                For Each oColonne In oColonnes
                    If oColonne.nIdColonne = CInt(str(4)) Then
                        If oColonne.sNomColonne.Contains("|") Then
                            sNomColonneRetrait = oColonne.sNomColonne.Split(CChar("|"))(0)
                        Else
                            sNomColonneRetrait = oColonne.sNomColonne
                        End If
                        If Not oCheckbox Is Nothing Then
                            oGrid.Columns(i).Visible = oCheckbox.Checked
                        End If
                        'Exit For
                    End If
                    If oGrid.Columns(i).Visible = True Then
                        iLargeurTableau += CInt(oGrid.Columns(i).ControlStyle.Width.Value)
                    End If
                    i += 1
                Next
                oGrid.Width = New Unit(iLargeurTableau + 20, UnitType.Pixel)

            End If
        Next
    End Sub

    Private Sub Charger_Footer()
        If Request.Item("masquer") Is Nothing Or (Not Request.Item("masquer") Is Nothing AndAlso Request.Item("masquer") <> "false") Then
            Dim oUtilisateur As New Utilisateur()
            Dim oModules As New Moduls()
            Dim oDocuments As New Documents

            oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))
            oModules.chargerListeModulesUnUtilisateur(oUtilisateur.nIdUtilisateur, CInt(User.Identity.Name.Split(CChar("_"))(1)))
            oDocuments.chargerListeDocument(oUtilisateur)

            Me.PlaceHolderFooterModules.Controls.Clear()
            Me.PlaceHolderFooterNotice.Controls.Clear()



            For Each oModule In oModules
                Dim LI As New HtmlGenericControl("LI")
                Dim lienModule As New LinkButton
                lienModule.CausesValidation = False
                lienModule.OnClientClick = "RedirectionAdresse('" & oModule.sLien & "?module=" & oModule.nIdModule & "');"

                sc1.RegisterAsyncPostBackControl(lienModule)
                lienModule.Text = "<span>" & oModule.sTitre & "</span>"
                lienModule.CssClass = oModule.sLibelleClasseFooter
                LI.Controls.Add(lienModule)
                Me.PlaceHolderFooterModules.Controls.Add(LI)
            Next

            For Each oDocument In oDocuments
                If oDocument.slibelleClasse = "notice" Then
                    Dim oLiteral As New Literal
                    oLiteral.Text = "<li><a href=""\" & oDocument.sLien & """>Notice d'utilisation</a></li>"
                    Me.PlaceHolderFooterNotice.Controls.Add(oLiteral)
                    Exit For
                End If
            Next

            Dim oHiddenField As New HiddenField
            oHiddenField.ID = "HiddenFooter"
            PlaceHolderFooterModules.Controls.Add(oHiddenField)

        End If
    End Sub

    Protected Sub lnkClose_Click(sender As Object, e As EventArgs) Handles lnkClose.Click
        FormsAuthentication.SignOut()
        Response.Redirect("login.aspx")
    End Sub

    Private Sub Charger_Filtres(ByVal sListeIdRapports As String, ByVal oFiltres As Filtres, ByVal OrapportNiv1 As Rapport, ByVal oRapportsSurOnglet As Rapports, ByVal Id_Div As String, ByVal Id_Rapport_Niv1 As String, ByVal oCheckboxMoisGlissant As CheckBox, ByRef ListeControles As List(Of Object))
        Dim idModule As String = ""

        Dim ListeValeurs As New List(Of ArrayList)
        Dim ControleTrouve As Object
        Dim IndexListeValeur As Integer
        idModule = CStr(Request.Item("module"))
        Me.listeIdRapports.Value = sListeIdRapports

        Dim sTous As String = "Tous"
        If OrapportNiv1.oClient.sLangueClient = "US" Then sTous = "All"

        Dim listeControlesIndice As UShort = 0
        ListeControles.Clear()
        For Each ofiltre In oFiltres
            If Not IsNothing(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports)) Then
                If TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.DropDownList Then
                    ListeControles.Add(CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), DropDownList))
                ElseIf TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.ListBox Then
                    ListeControles.Add(CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), ListBox))
                End If
            End If



        Next

        Dim bEstMoisGlissant As Boolean = False
        If Not oCheckboxMoisGlissant Is Nothing Then
            If oCheckboxMoisGlissant.Checked = True Then
                bEstMoisGlissant = True
            End If
        Else
            If Not Page.Request("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports) Is Nothing Then
                If Page.Request("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports) = "on" Then
                    bEstMoisGlissant = True
                End If
            End If
        End If

        cDal.ChargerValeursFiltre(sListeIdRapports, oFiltres, OrapportNiv1, oRapportsSurOnglet, Id_Div, ListeControles, ListeValeurs, bEstMoisGlissant, Me.ChaineFiltreRedirection.Value)

        For Each oFiltreActuel In oFiltres
            Dim nIdRapportTrouve As Integer = oFiltreActuel.TesterFiltrePresentRapport(oFiltreActuel.nIdFiltre, OrapportNiv1, oRapportsSurOnglet, False)
            If nIdRapportTrouve > 0 Then
                ControleTrouve = Nothing

                For i = 0 To ListeControles.Count - 1
                    If TypeOf ListeControles(i) Is System.Web.UI.WebControls.DropDownList Then
                        Dim ddlControle As DropDownList = CType(ListeControles(i), DropDownList)
                        If ddlControle.ID = "Filtre_" & Id_Div & "_" & oFiltreActuel.nIdFiltre & "_" & sListeIdRapports Then
                            ControleTrouve = ListeControles(i)
                            IndexListeValeur = i
                        End If
                    ElseIf TypeOf ListeControles(i) Is System.Web.UI.WebControls.ListBox Then
                        Dim ddlControle As ListBox = CType(ListeControles(i), ListBox)
                        If ddlControle.ID = "Filtre_" & Id_Div & "_" & oFiltreActuel.nIdFiltre & "_" & sListeIdRapports Then
                            ControleTrouve = ListeControles(i)
                            IndexListeValeur = i
                        End If
                    End If
                Next

                Select Case oFiltreActuel.bEstMultiselection

                    Case True
                        Dim oControle As ListBox = CType(ControleTrouve, ListBox)

                        Dim Valeur_Selectionnee As New List(Of String)
                        If Request.Form(oControle.ID) <> Nothing Then
                            For Each item As String In Request.Form(oControle.ID).ToString.Split(CChar(","))
                                Valeur_Selectionnee.Add(item)
                            Next
                        End If

                        oControle.Items.Clear()
                        oControle.SelectedValue = Nothing
                        If oFiltreActuel.bEstTous Then oControle.Items.Add(sTous)
                        For Each Item In ListeValeurs(IndexListeValeur)
                            If Not IsDBNull(Item) Then
                                oControle.Items.Add(Toolbox.ConvertirValeurFiltre(CStr(Item), oFiltreActuel.sNomColonne, OrapportNiv1.oClient))
                                oControle.Items(oControle.Items.Count() - 1).Attributes.Add("title", Toolbox.ConvertirValeurFiltre(CStr(Item), oFiltreActuel.sNomColonne, OrapportNiv1.oClient))
                            End If
                        Next
                        For Each sValeur_Selectionnee As String In Valeur_Selectionnee
                            If sValeur_Selectionnee <> Nothing And Not oControle.Items.Contains(New ListItem(sValeur_Selectionnee)) Then
                                oControle.Items.Add(sValeur_Selectionnee)
                                oControle.Items(oControle.Items.Count() - 1).Attributes.Add("title", sValeur_Selectionnee)
                            End If
                        Next
                        For Each Item As ListItem In oControle.Items
                            If Valeur_Selectionnee.Contains(Item.Value) Then
                                Item.Selected = True
                            End If
                        Next
                        If Valeur_Selectionnee.Count = 0 Then
                            For Each Item As ListItem In oControle.Items
                                If oFiltreActuel.sValeurFiltre.Split(CChar(";")).Contains(Item.Value) Then
                                    Item.Selected = True
                                End If

                            Next
                        End If

                    Case False
                        Dim oControle As DropDownList = CType(ControleTrouve, DropDownList)
                        Dim Valeur_Selectionnee As String = Request.Form(oControle.ID)
                        oControle.Items.Clear()
                        oControle.SelectedValue = Nothing
                        If oFiltreActuel.bEstTous Then oControle.Items.Add(sTous)
                        If oFiltreActuel.sNomColonne.ToUpper = "ANNEE" And bEstMoisGlissant = True Then
                            oControle.Items.Add(Toolbox.ConvertirValeurFiltre(CStr(ListeValeurs(IndexListeValeur).Item(ListeValeurs(IndexListeValeur).Count - 1)), oFiltreActuel.sNomColonne, OrapportNiv1.oClient))
                            Valeur_Selectionnee = ""
                        Else
                            For Each Item In ListeValeurs(IndexListeValeur)
                                If IsDBNull(Item) Then
                                    Item = ""
                                End If
                                oControle.Items.Add(Toolbox.ConvertirValeurFiltre(CStr(Item), oFiltreActuel.sNomColonne, OrapportNiv1.oClient))
                            Next
                        End If
                        If Valeur_Selectionnee <> Nothing And Not oControle.Items.Contains(New ListItem(Valeur_Selectionnee)) Then
                            oControle.Items.Add(Valeur_Selectionnee)
                        End If

                        If Valeur_Selectionnee <> "" And oControle.Items.Contains(New ListItem(Valeur_Selectionnee)) Then
                            oControle.SelectedValue = Valeur_Selectionnee
                        End If
                        If Valeur_Selectionnee = "" Then
                            If oControle.Items.Contains(New ListItem(oFiltreActuel.sValeurFiltre)) Then
                                oControle.SelectedValue = oFiltreActuel.sValeurFiltre
                            End If
                            If oFiltreActuel.sValeurFiltre = "#Derniere valeur" Then
                                If oControle.Items.Count <> 0 Then
                                    oControle.SelectedValue = oControle.Items(oControle.Items.Count - 1).Value
                                End If
                            End If
                        End If
                End Select
            End If
        Next

        '---Gestion des redirections
        Dim sTestRedirection As String = ""
        If Me.ChaineFiltreRedirection.Value <> "" And Me.ChaineFiltreRedirection.Value <> "Efface" Then
            sTestRedirection = Me.ChaineFiltreRedirection.Value
        End If
        If Me.PremierChargementOnglet.Value <> "" Then
            If Me.PremierChargementOnglet.Value.Split(CChar(";"))(1) = "non" And CInt(Me.PremierChargementOnglet.Value.Split(CChar(";"))(0)) = OrapportNiv1.nIdRapport Then
                sTestRedirection = ""
            End If
        End If

        If sTestRedirection <> "" Then

            Dim TableFiltresRedirections() As String = sTestRedirection.Split(CChar("µ"))
            For Each FiltreRedirection As String In TableFiltresRedirections
                Dim Id_Filtre As Integer = CInt(FiltreRedirection.Split(CChar("="))(0))
                Dim ChaineFiltre As String = FiltreRedirection.Split(CChar("="))(1)


                For Each ofiltre In oFiltres
                    If ofiltre.nIdFiltre = Id_Filtre Then
                        If Not IsNothing(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports)) Then
                            If TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.DropDownList Then
                                Dim oControle As DropDownList = CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), DropDownList)
                                If Not oControle.Items.Contains(New ListItem(ChaineFiltre)) Then
                                    oControle.Items.Add(New ListItem(ChaineFiltre))
                                End If
                                oControle.SelectedValue = ChaineFiltre
                            ElseIf TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.ListBox Then
                                Dim oControle As ListBox = CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), ListBox)
                                Dim ListeValeursFiltre() As String = ChaineFiltre.Split(CChar("|"))
                                For Each ValeursFiltre As String In ListeValeursFiltre
                                    If Not oControle.Items.Contains(New ListItem(ValeursFiltre)) Then
                                        oControle.Items.Add(New ListItem(ValeursFiltre))
                                    End If
                                Next
                                For Each item As ListItem In oControle.Items
                                    If ListeValeursFiltre.Contains(item.Value) Then
                                        item.Selected = True
                                    Else
                                        item.Selected = False
                                    End If
                                Next
                            End If
                        End If
                    End If
                Next

            Next
        End If

    End Sub

    Private Function CreerExcel(ByRef oListeRequetes As List(Of KeyValuePair(Of Rapport, List(Of String))), ByRef oListeColonnes As List(Of KeyValuePair(Of Rapport, Colonnes)), ByRef oCheckboxDecimales As CheckBox, ByRef oListeKeyPairFiltreValeur As List(Of KeyValuePair(Of String, String)), ByVal oCheckboxTTC As CheckBox, ByVal bEstExcel As Boolean) As Boolean

        Dim sFile As String = ""
        Dim sNomFichier As String = ""
        Dim strFile As FileInfo
        Dim nSheet As Integer = 0

        Dim Process1() As Process = Process.GetProcesses()

        Dim oExcel As New Excel.Application
        oExcel.Workbooks.Add()
        Dim nouveauClasseur As Excel.Workbook
        nouveauClasseur = oExcel.ActiveWorkbook
        Dim idProcExcel As Integer

        Dim sTous As String = "Tous"
        If oListeRequetes(0).Key.oClient.sLangueClient = "US" Then sTous = "All"

        ' -------- Liste des processus après celui d'Excel

        Dim Process2() As Process = Process.GetProcesses()

        ' -------- Recherche de mon Id de processus
        Dim bMonProcessXL As Boolean
        For j = 0 To Process2.GetUpperBound(0)
            If Process2(j).ProcessName = "EXCEL" Then
                bMonProcessXL = True
                ' Parcours des processus avant le mien
                For i = 0 To Process1.GetUpperBound(0)
                    If Process1(i).ProcessName = "EXCEL" Then
                        If Process2(j).Id = Process1(i).Id Then
                            ' S'il existait avant, c
                            bMonProcessXL = False
                            Exit For
                        End If
                    End If
                Next i

                If bMonProcessXL = True Then
                    ' Maintenant que j'ai son Id, je pourrai le tuer
                    ' xlApp.Hinstance ne fonctionne pas avec Excel 2000
                    ' alors que cette méthode marche toujours !
                    idProcExcel = Process2(j).Id
                    Exit For
                End If
            End If
        Next j

        'oExcel.Visible = True
        '' Execution en tache de fond
        oExcel.Visible = False : oExcel.DisplayAlerts = False
        Do Until nouveauClasseur.Sheets.Count = 1
            nouveauClasseur.Sheets(nouveauClasseur.Sheets.Count).delete()
        Loop

        sNomFichier = oListeRequetes(0).Key.sSommaireNiv1 & " - " & oListeRequetes(0).Key.sSommaireNiv2
        If bEstExcel = True Then
            sNomFichier = sNomFichier & ".xlsx"
        Else
            sNomFichier = sNomFichier & ".pdf"
        End If
        strFile = New FileInfo(Server.MapPath("\export\" & User.Identity.Name.Split(CChar("_"))(0) & "\" & sNomFichier))

        For Each el In oListeRequetes
            Dim estGraphique As Boolean = cDal.estGraphique(el.Key)
            If estGraphique = False Then

                If nSheet > 0 Then
                    nouveauClasseur.Sheets.Add(, nouveauClasseur.Sheets.Item(nSheet))
                End If

                'Rechercher les colonnes correspondantes dans oListeColonnes afin de remonter les noms de colonnes permettant de définir les entêtes dans Excel pour ce rapport
                ' /!\ Attention aux colonnes qui ne sont pas visibles et qui reviennent dans la liste
                Dim col As KeyValuePair(Of Rapport, Colonnes) = oListeColonnes.Find(Function(x) x.Key.Equals(el.Key))

                'test.Sheets.Add()
                Dim oSheet As Excel.Worksheet
                'oSheet = DirectCast(oExcel.ActiveWorkbook.Sheets(i + 1), Excel.Worksheet)
                oSheet = DirectCast(nouveauClasseur.Sheets(nouveauClasseur.Sheets.Count), Excel.Worksheet)

                Dim sNomFeuille As String = el.Key.sNomFichierExport
                Dim nIncrement As Integer = 0
                Dim bTestNomTrouve As Boolean = True
                Do Until bTestNomTrouve = False
                    bTestNomTrouve = False
                    For Each oFeuille In nouveauClasseur.Sheets
                        If oFeuille.name = sNomFeuille Then
                            nIncrement += 1
                            sNomFeuille = el.Key.sNomFichierExport & "_" & CStr(nIncrement)
                            bTestNomTrouve = True
                        End If
                    Next
                Loop

                Do Until sNomFeuille.Length < 31
                    sNomFeuille = sNomFeuille.Substring(0, sNomFeuille.Length - 1)
                Loop

                oSheet.Name = sNomFeuille

                Dim sqlString As New cSQL
                With oSheet.ListObjects.Add(SourceType:=0, Source:=
        "ODBC;DRIVER=SQL Server;SERVER=" + sqlString.connexionString() + ";UID=user_extranet;PWD=Oria31;APP=Microsoft Office 2010;DATABASE=" + el.Key.oClient.sBaseClient, Destination:=oSheet.Range("$A$5")).QueryTable
                    .CommandText = el.Value(0)
                    .RowNumbers = False
                    .FillAdjacentFormulas = False
                    .PreserveFormatting = False
                    .RefreshOnFileOpen = False
                    .BackgroundQuery = False
                    .SavePassword = False
                    .SaveData = True
                    .AdjustColumnWidth = False
                    .RefreshPeriod = 0
                    .PreserveColumnInfo = False
                    Try
                        .Refresh(False)
                    Catch ex As Exception
                    End Try

                End With

                oSheet.ListObjects("Tableau_DonnéesExternes_1").TableStyle = ""
                oSheet.ListObjects("Tableau_DonnéesExternes_1").ShowHeaders = False
                oSheet.ListObjects("Tableau_DonnéesExternes_1").Unlink()
                oSheet.ListObjects("Tableau_DonnéesExternes_1").Unlist()

                Dim nbLigne As Long = lastRowInExcel(oSheet)


                Dim nListeColSuppr As New List(Of Integer)
                Dim j As Integer = 0
                For Each oColo In col.Value

                    If oColo.bEstVisible = False Or oColo.bEstExtractExcel = False Then
                        nListeColSuppr.Add(j)
                    End If
                    j += 1
                Next
                For j = nListeColSuppr.Count - 1 To 0 Step -1
                    oSheet.Columns(nListeColSuppr(j) + 1).delete()
                    col.Value.RemoveAt(nListeColSuppr(j))
                Next

                'FUSION HORIZONTALE DU CONTENU SEULEMENT SI C'EST AUTORISE
                If el.Key.bAutoriserFusionColonnes = True Then
                    Dim iNombreColSpan As Integer
                    For iIndexLigne As Long = 6 To nbLigne
                        For iColonneActuelle = 1 To col.Value.Count
                            Dim sCellule As String = oSheet.Cells(iIndexLigne, iColonneActuelle).Text.ToString
                            If sCellule.Contains("columnspan") Then
                                iNombreColSpan = CInt(sCellule.Split(CChar(">"))(0).Split(CChar("="))(1).Split(CChar("µ"))(0))
                                If iColonneActuelle + iNombreColSpan - 1 > col.Value.Count Then iNombreColSpan = col.Value.Count - iColonneActuelle
                                oSheet.Range(Convert_Colonne(iColonneActuelle) & CStr(iIndexLigne) & ":" & Convert_Colonne(iColonneActuelle + iNombreColSpan - 1) & CStr(iIndexLigne)).MergeCells = True
                            End If
                        Next
                    Next
                End If

                Dim sTableRange As Excel.Range
                For j = 1 To col.Value.Count
                    If col.Value(j - 1).bEstComposeeBalises = True Or cDal.testerSiColonneRedirection(col.Value(j - 1), CInt(User.Identity.Name.Split(CChar("_"))(1))) Then
                        sTableRange = oSheet.Range(Convert_Colonne(j) & "6:" & Convert_Colonne(j) & CStr(nbLigne))
                        For i = 1 To nbLigne
                            sTableRange(i, 1).value = Toolbox.RemoveTagHTML(CType(sTableRange(i, 1).value, String))
                        Next
                        oSheet.Range(Convert_Colonne(j) & "6:" & Convert_Colonne(j) & CStr(nbLigne)).Value2 = sTableRange.Value
                    End If
                Next

                'FAIRE LES FILTRES POUR LES ENTETES ET REALISER LES FUSIONS
                Dim k As Integer = 5
                For j = 1 To col.Value.Count
                    If col.Value.Item(j - 1).bEstVisible = True Then
                        Dim sEnteteLePlusEleve As String = ""

                        If Not col.Value.Item(j - 1).sEnteteNiveau4 Is Nothing AndAlso col.Value.Item(j - 1).sEnteteNiveau4 <> "" Then
                            sEnteteLePlusEleve = col.Value.Item(j - 1).sEnteteNiveau4
                        ElseIf Not col.Value.Item(j - 1).sEnteteNiveau3 Is Nothing AndAlso col.Value.Item(j - 1).sEnteteNiveau3 <> "" Then
                            sEnteteLePlusEleve = col.Value.Item(j - 1).sEnteteNiveau3
                        ElseIf Not col.Value.Item(j - 1).sEnteteNiveau2 Is Nothing AndAlso col.Value.Item(j - 1).sEnteteNiveau2 <> "" Then
                            sEnteteLePlusEleve = col.Value.Item(j - 1).sEnteteNiveau2
                        Else
                            sEnteteLePlusEleve = col.Value.Item(j - 1).sEnteteNiveau1
                        End If

                        If Not col.Value.Item(j - 1).sEnteteNiveau4 Is Nothing AndAlso col.Value.Item(j - 1).sEnteteNiveau4 <> "" Then
                            DirectCast(oSheet.Cells(2, j), Excel.Range).Value = DirectCast(col.Value.Item(j - 1).sEnteteNiveau4, String)
                        Else
                            DirectCast(oSheet.Cells(2, j), Excel.Range).Value = sEnteteLePlusEleve
                        End If

                        If Not col.Value.Item(j - 1).sEnteteNiveau3 Is Nothing AndAlso col.Value.Item(j - 1).sEnteteNiveau3 <> "" Then
                            DirectCast(oSheet.Cells(3, j), Excel.Range).Value = DirectCast(col.Value.Item(j - 1).sEnteteNiveau3, String)
                        Else
                            DirectCast(oSheet.Cells(3, j), Excel.Range).Value = sEnteteLePlusEleve
                        End If

                        If Not col.Value.Item(j - 1).sEnteteNiveau2 Is Nothing AndAlso col.Value.Item(j - 1).sEnteteNiveau2 <> "" Then
                            DirectCast(oSheet.Cells(4, j), Excel.Range).Value = DirectCast(col.Value.Item(j - 1).sEnteteNiveau2, String)
                        Else
                            DirectCast(oSheet.Cells(4, j), Excel.Range).Value = sEnteteLePlusEleve
                        End If

                        DirectCast(oSheet.Cells(5, j), Excel.Range).Value = DirectCast(col.Value.Item(j - 1).sEnteteNiveau1, String)

                    End If
                Next

                oSheet.Rows(5).AutoFilter()

                'FUSION VERTICAL DES ENTÊTES
                'Les lignes d'entêtes, dans le fichier excel, vont de 2 à 5
                For i = 2 To 5 Step 1
                    'Parcourir toutes les colonnes d'entêtes
                    For j = 1 To col.Value.Count
                        'La variable nLimiteHauteFusion représente la valeur de la dernière cellule qui doit être fusionnée avec les autres
                        Dim nLimiteHauteFusion As Integer = 1
                        Dim bFusionPresent As Boolean = False
                        Do Until (CStr(oSheet.Cells(i, j).text) <> CStr(oSheet.Cells(i + nLimiteHauteFusion, j).text)) OrElse CStr(oSheet.Cells(i + nLimiteHauteFusion, j).text) = ""

                            If (i + nLimiteHauteFusion) <= 5 Then
                                nLimiteHauteFusion += 1
                            End If
                            bFusionPresent = True

                            If (i + nLimiteHauteFusion > 5) Then
                                Exit Do
                            End If
                        Loop

                        If bFusionPresent = True Then
                            'Le -1 s'appliquant à nLimiteHauteFusion est dû au fait que nLimiteHauteFusion commence à 1 et non 0
                            oSheet.Range(Convert_Colonne(j) + CStr(i) + ":" + Convert_Colonne(j) + CStr(i + (nLimiteHauteFusion - 1))).MergeCells = True
                        End If

                    Next
                Next

                'FUSION HORIZONTAL DES ENTÊTES
                For i = 2 To 5 Step 1
                    For j = 1 To col.Value.Count
                        Dim nLimiteHauteFusion As Integer = 1
                        Dim bFusionPresent As Boolean = False
                        Do Until (CStr(oSheet.Cells(i, j).text) <> CStr(oSheet.Cells(i, j + nLimiteHauteFusion).text)) OrElse (CStr(oSheet.Cells(i, j + nLimiteHauteFusion).text) = "")

                            If (i + nLimiteHauteFusion) <= 5 Then
                                nLimiteHauteFusion += 1
                            End If
                            bFusionPresent = True

                            If (i + nLimiteHauteFusion > 5) Then
                                Exit Do
                            End If
                        Loop

                        If bFusionPresent = True Then
                            'Le -1 s'appliquant à nLimiteHauteFusion est dû au fait que nLimiteHauteFusion commence à 1 et non 0
                            oSheet.Range(Convert_Colonne(j) + CStr(i) + ":" + Convert_Colonne(j + (nLimiteHauteFusion - 1)) + CStr(i)).MergeCells = True
                        End If
                    Next
                Next



                'FUSION VERTICALE DU CONTENU
                'Parcourir toutes les colonnes d'entêtes



                For j = 1 To col.Value.Count
                    If col.Value(j - 1).bEstFusionnable = True Then
                        For i = 6 To nbLigne Step 1
                            'La variable nLimiteHauteFusion représente la valeur de la dernière cellule qui doit être fusionnée avec les autres
                            Dim nLimiteHauteFusion As Integer = 1
                            Dim bFusionPresent As Boolean = False
                            Do Until (CStr(oSheet.Cells(i, j).text) <> CStr(oSheet.Cells(i + nLimiteHauteFusion, j).text)) OrElse (CStr(oSheet.Cells(i + nLimiteHauteFusion, j).text) = "")

                                If (i + nLimiteHauteFusion) <= nbLigne Then
                                    nLimiteHauteFusion += 1
                                End If
                                bFusionPresent = True

                                If (i + nLimiteHauteFusion > nbLigne) Then
                                    Exit Do
                                End If
                            Loop

                            If bFusionPresent = True Then
                                'Le -1 s'appliquant à nLimiteHauteFusion est dû au fait que nLimiteHauteFusion commence à 1 et non 0
                                oSheet.Range(Convert_Colonne(j) + CStr(i) + ":" + Convert_Colonne(j) + CStr(i + (nLimiteHauteFusion - 1))).MergeCells = True
                            End If

                        Next
                    End If
                Next



                '---Total
                If el.Key.bAfficherTotal = True Then
                    Dim nBDebutColonneTotal As Integer = 2
                    For Each oColo In col.Value
                        If oColo.sTypeDonnees.ToUpper <> "NOMBRE" And oColo.sTypeDonnees.ToUpper <> "NOMBREBALISE" Then
                            nBDebutColonneTotal += 1
                        End If
                    Next
                    With oSheet.ListObjects.Add(SourceType:=0, Source:="ODBC;DRIVER=SQL Server;SERVER=" + sqlString.connexionString() + ";UID=user_extranet;PWD=Oria31;APP=Microsoft Office 2010;DATABASE=" + el.Key.oClient.sBaseClient, Destination:=oSheet.Range("$" & Convert_Colonne(nBDebutColonneTotal - 1) & "$" & CStr(nbLigne + 1))).QueryTable
                        .CommandText = el.Value(1).Replace("OPERATION", el.Key.sTypeTotal.ToUpper)
                        .RowNumbers = False
                        .FillAdjacentFormulas = False
                        .PreserveFormatting = False
                        .RefreshOnFileOpen = False
                        .BackgroundQuery = False
                        .SavePassword = False
                        .SaveData = True
                        .AdjustColumnWidth = False
                        .RefreshPeriod = 0
                        .PreserveColumnInfo = False
                        Try
                            .Refresh(False)
                        Catch ex As Exception
                        End Try
                    End With

                    oSheet.ListObjects("Tableau_DonnéesExternes_1").TableStyle = ""
                    oSheet.ListObjects("Tableau_DonnéesExternes_1").ShowHeaders = False
                    oSheet.ListObjects("Tableau_DonnéesExternes_1").Unlink()
                    oSheet.ListObjects("Tableau_DonnéesExternes_1").Unlist()

                    nbLigne += 1
                    oSheet.Rows(CStr(nbLigne) & ":" & CStr(nbLigne)).delete(Shift:=Excel.XlDirection.xlUp)
                    oSheet.Cells(nbLigne, 1).value = el.Key.sTitreTotal
                    If el.Key.bAfficherHtTtcDansLibelleTotal = True And el.Key.bAfficherCaseTTC = True Then
                        Dim bAfficherTTC As Boolean = oCheckboxTTC.Checked

                        Select Case el.Key.oClient.sMonnaieClient
                            Case "euro"
                                Select Case bAfficherTTC
                                    Case True
                                        oSheet.Cells(nbLigne, 1).value += " (€ TTC)"
                                    Case False
                                        oSheet.Cells(nbLigne, 1).value += " (€ HT)"
                                End Select
                            Case "dollar"
                                Select Case bAfficherTTC
                                    Case True
                                        oSheet.Cells(nbLigne, 1).value += " (€ TTC)"
                                    Case False
                                        oSheet.Cells(nbLigne, 1).value += " ($)"
                                End Select
                        End Select
                    End If
                    oSheet.Range("A" & CStr(nbLigne) & ":" & Convert_Colonne(nBDebutColonneTotal - 2) & CStr(nbLigne)).MergeCells = True
                End If

                oSheet.Columns(Convert_Colonne(col.Value.Count + 1) & ":BO").Delete(Shift:=Excel.XlDirection.xlToLeft)

                'Mise en forme
                excelEnTete(oSheet, col.Value)
                excelContenu(oSheet, col.Value, oCheckboxDecimales, nbLigne)
                If el.Key.bAfficherTotal = True Then excelTotal(oSheet, col.Value, nbLigne)

                oSheet.Columns("A:" & Convert_Colonne(col.Value.Count)).ColumnWidth = 150
                oSheet.Cells.EntireRow.AutoFit()
                oSheet.Cells.EntireColumn.AutoFit()

                'Mise en page
                oSheet.PageSetup.PrintTitleRows = "$2:$5"
                oSheet.PageSetup.PrintTitleColumns = ""

                oSheet.PageSetup.PrintArea = "$A$2:$" & Convert_Colonne(col.Value.Count) & "$" & CStr(nbLigne)

                oSheet.PageSetup.LeftHeader = "Société ORIA"
                oSheet.PageSetup.CenterHeader = el.Key.sSommaireNiv2 & " - " & el.Key.sOngletNiv1
                oSheet.PageSetup.RightHeader = el.Key.oClient.sEnteteExport
                oSheet.PageSetup.LeftFooter = "Filtres : "
                For nCompteurFiltre = 0 To (oListeKeyPairFiltreValeur.Count - 1)
                    If oListeKeyPairFiltreValeur(nCompteurFiltre).Value <> sTous Then
                        oSheet.PageSetup.LeftFooter += oListeKeyPairFiltreValeur(nCompteurFiltre).Key + " " + oListeKeyPairFiltreValeur(nCompteurFiltre).Value + " - "
                    End If
                    If nCompteurFiltre = oListeKeyPairFiltreValeur.Count - 1 Then
                        oSheet.PageSetup.LeftFooter = oSheet.PageSetup.LeftFooter.Substring(0, oSheet.PageSetup.LeftFooter.Length - 3)
                    End If
                Next
                oSheet.PageSetup.RightFooter = "Edité le &D"

                oSheet.PageSetup.Zoom = False
                oSheet.PageSetup.FitToPagesWide = 1
                oSheet.PageSetup.FitToPagesTall = False
                oSheet.PageSetup.AlignMarginsHeaderFooter = True

                oSheet = Nothing
                sqlString = Nothing

            Else
                '---Gestion Graphiques
                If nSheet > 0 Then
                    nouveauClasseur.Sheets.Add(, nouveauClasseur.Sheets.Item(nSheet))
                End If

                Dim oSheet As Excel.Worksheet
                oSheet = DirectCast(nouveauClasseur.Sheets(nouveauClasseur.Sheets.Count), Excel.Worksheet)


                Dim sNomFeuille As String = el.Key.sNomFichierExport
                Dim nIncrement As Integer = 0
                Dim bTestNomTrouve As Boolean = True
                Do Until bTestNomTrouve = False
                    bTestNomTrouve = False
                    For Each oFeuille In nouveauClasseur.Sheets
                        If oFeuille.name = sNomFeuille Then
                            nIncrement += 1
                            sNomFeuille = el.Key.sNomFichierExport & "_" & CStr(nIncrement)
                            bTestNomTrouve = True
                        End If
                    Next
                Loop

                oSheet.Name = sNomFeuille

                '
                Dim sCheminImage As String = el.Value(0)
                Dim srcBitmap As New Bitmap(sCheminImage)
                Dim nWith As Integer = srcBitmap.Size.Width
                Dim nHeight As Integer = srcBitmap.Size.Height
                Dim nPourcentage As Double = 0

                If nWith > 800 Then
                    nPourcentage = 800 / nWith
                    nWith = 800
                    nHeight = CInt(nHeight * nPourcentage)
                End If
                srcBitmap.Dispose()
                srcBitmap = Nothing

                oSheet.Cells(2, 2).select()
                oSheet.Shapes.AddPicture(sCheminImage, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoTrue, 30, 30, nWith, nHeight)





                oSheet.PageSetup.PrintArea = "$A$2:$O$40"

                oSheet.PageSetup.LeftHeader = "Société ORIA"
                oSheet.PageSetup.CenterHeader = el.Key.sSommaireNiv2 & " - " & el.Key.sOngletNiv1
                oSheet.PageSetup.RightHeader = el.Key.oClient.sEnteteExport
                oSheet.PageSetup.LeftFooter = "Filtres : "
                For nCompteurFiltre = 0 To (oListeKeyPairFiltreValeur.Count - 1)
                    If oListeKeyPairFiltreValeur(nCompteurFiltre).Value <> sTous Then
                        oSheet.PageSetup.LeftFooter += oListeKeyPairFiltreValeur(nCompteurFiltre).Key + " " + oListeKeyPairFiltreValeur(nCompteurFiltre).Value + " - "
                    End If
                    If nCompteurFiltre = oListeKeyPairFiltreValeur.Count - 1 Then
                        oSheet.PageSetup.LeftFooter = oSheet.PageSetup.LeftFooter.Substring(0, oSheet.PageSetup.LeftFooter.Length - 3)
                    End If
                Next
                oSheet.PageSetup.RightFooter = "Edité le &D"

                oSheet.PageSetup.Zoom = False
                oSheet.PageSetup.FitToPagesWide = 1
                oSheet.PageSetup.FitToPagesTall = False
                oSheet.PageSetup.AlignMarginsHeaderFooter = True

                oSheet = Nothing

            End If

            'CHANGEMENT DE FEUILLE POUR LE RAPPORT SUIVANT
            nSheet += 1
        Next

        ' Suppresion du fichier
        If strFile.Exists = True Then
            strFile.Delete()
        End If

        sFile = Server.MapPath("\export\" & User.Identity.Name.Split(CChar("_"))(0) & "\" & sNomFichier)
        ' Création du dossier spécifique s'il n'existe pas
        ' ---- Vérification de l'existence du dossier
        If Not Directory.Exists(Server.MapPath("\export\" & User.Identity.Name.Split(CChar("_"))(0))) Then
            ' Création du dossier
            Directory.CreateDirectory(Server.MapPath("\export\" & User.Identity.Name.Split(CChar("_"))(0)))
        End If

        If bEstExcel = False Then
            nouveauClasseur.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, Server.MapPath("\export\" & User.Identity.Name.Split(CChar("_"))(0) & "\" & sNomFichier), Excel.XlFixedFormatQuality.xlQualityStandard, True, True, , , False)
        Else
            nouveauClasseur.SaveAs(Server.MapPath("\export\" & User.Identity.Name.Split(CChar("_"))(0) & "\" & sNomFichier), Excel.XlFileFormat.xlOpenXMLWorkbook)
        End If

        ' Sauvegarde du fichier
        nouveauClasseur.Close()
        oExcel.Workbooks.Close()
        oExcel.Quit()
        ReleaseComObject(oExcel)
        oExcel = Nothing

        Try
            If idProcExcel <> 0 Then
                Process.GetProcessById(idProcExcel).Kill()
            Else
                For Each el In Process.GetProcessesByName("EXCEL")
                    el.Kill()
                Next
            End If
        Catch ex As Exception
            For Each el In Process.GetProcessesByName("EXCEL")
                el.Kill()
            Next
        End Try

        SendFile(sFile, sFile.Split(CChar("\"))(sFile.Split(CChar("\")).Length - 1))

        Return True

    End Function

    Private Sub SendFile(ByVal strPath As System.String, ByVal strSuggestedName As System.String)
        Try
            Dim objSourceFileInfo As System.IO.FileInfo
            objSourceFileInfo = New System.IO.FileInfo(strPath)
            If objSourceFileInfo.Exists Then

                With Me.Response
                    .ContentType = "application/octet-stream"
                    .AddHeader("Content-Disposition", "attachment; filename=" & Replace(strSuggestedName, " ", "_"))
                    .AddHeader("Content-Length", objSourceFileInfo.Length.ToString)
                    .WriteFile(objSourceFileInfo.FullName)
                    .Flush()
                    .End()
                End With
            Else


            End If

        Catch
            Response.End()
        End Try
    End Sub

    Public Sub excelEnTete(ByRef oSheet As Excel.Worksheet, ByRef oColonnes As Colonnes)
        Dim oRange As Excel.Range
        oRange = oSheet.Range("A2:" & Convert_Colonne(oColonnes.Count) & "5")

        oRange.Interior.Pattern = Excel.Constants.xlSolid
        oRange.Interior.PatternColorIndex = Excel.Constants.xlAutomatic
        oRange.Interior.Color = 15132390
        oRange.Font.Color = -2386688

        oRange.HorizontalAlignment = Excel.Constants.xlCenter
        oRange.VerticalAlignment = Excel.Constants.xlCenter
        oRange.WrapText = True

        excelBordures(oRange)

    End Sub

    Public Sub excelTotal(ByRef oSheet As Excel.Worksheet, ByRef oColonnes As Colonnes, ByVal nBLigne As Long)
        Dim oRange As Excel.Range
        oRange = oSheet.Range("A" & CStr(nBLigne) & ":" & Convert_Colonne(oColonnes.Count) & CStr(nBLigne))

        oRange.Interior.Pattern = Excel.Constants.xlSolid
        oRange.Interior.PatternColorIndex = Excel.Constants.xlAutomatic
        oRange.Interior.Color = 15132390
        oRange.Font.Color = -2386688

        oRange.HorizontalAlignment = Excel.Constants.xlCenter
        oRange.VerticalAlignment = Excel.Constants.xlCenter
        oRange.WrapText = True

        excelBordures(oRange)

    End Sub

    Public Sub excelContenu(ByRef oSheet As Excel.Worksheet, ByRef oColonnes As Colonnes, ByRef oCheckboxDecimales As CheckBox, ByVal nLastRow As Long)
        Dim oRange As Excel.Range
        Dim nCompteurColonne As Integer = 1

        For Each el In oColonnes
            oRange = oSheet.Range(Convert_Colonne(nCompteurColonne) & "6:" & Convert_Colonne(nCompteurColonne) & nLastRow)
            oRange.Interior.Pattern = Excel.Constants.xlSolid
            oRange.Interior.PatternColorIndex = Excel.Constants.xlAutomatic
            Select Case el.sclasseCellule.ToUpper
                Case "INFORMATIONS"
                    oRange.Interior.Color = 15527148
                    oRange.Font.Color = -6854880
                    oRange.Font.Bold = True
                Case "VALEURS"
                    oRange.Interior.Color = 15856113
                    oRange.Font.Color = -6854880
                Case "TOTAL"
                    oRange.Interior.Color = 15856113
                    oRange.Font.Color = -2386688
                    oRange.Font.Bold = True
            End Select

            If el.sMefDonnees.ToUpper.Contains("DATE") Then
                Dim str As String = el.sMefDonnees.Split(CChar(":"))(1).Replace("}", "")
                str.Replace("dd", "d")
                str.Replace("MMMM", "mm")
                str.Replace("MM", "m")

                oRange.Replace(What:="-", Replacement:="/", LookAt:=Excel.XlLookAt.xlPart, SearchOrder:=Excel.XlSearchOrder.xlByRows, MatchCase:=False, SearchFormat:=False, ReplaceFormat:=False)

                oRange.NumberFormat = str
            ElseIf el.sMefDonnees.ToUpper.Contains("NOMBRE") Or el.sMefDonnees.ToUpper.Contains("PROGRESSION") Then
                Dim str As String = "#,##0"
                Dim nDecimale As Integer = CInt(el.sMefDonnees.Split(CChar(":"))(1).Replace("}", "").Replace("N", ""))
                If Not oCheckboxDecimales Is Nothing Then
                    If nDecimale > 0 And oCheckboxDecimales.Checked = True Then
                        str = str & "."
                        For i = 0 To nDecimale - 1
                            str = str & "0"
                        Next
                    End If
                End If

                oRange.NumberFormat = str
            End If



            nCompteurColonne += 1
        Next
        oRange = oSheet.Range("A6:" & Convert_Colonne(nCompteurColonne - 1) & nLastRow)

        oRange.HorizontalAlignment = Excel.Constants.xlCenter
        oRange.VerticalAlignment = Excel.Constants.xlCenter

        oRange.WrapText = True

        excelBordures(oRange)

    End Sub

    Public Sub excelBordures(ByVal oRange As Excel.Range)

        oRange.Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        oRange.Borders(Excel.XlBordersIndex.xlInsideVertical).ThemeColor = 1
        oRange.Borders(Excel.XlBordersIndex.xlInsideVertical).TintAndShade = 0
        oRange.Borders(Excel.XlBordersIndex.xlInsideVertical).Weight = Excel.XlBorderWeight.xlThin

        oRange.Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        oRange.Borders(Excel.XlBordersIndex.xlInsideHorizontal).ThemeColor = 1
        oRange.Borders(Excel.XlBordersIndex.xlInsideHorizontal).TintAndShade = 0
        oRange.Borders(Excel.XlBordersIndex.xlInsideHorizontal).Weight = Excel.XlBorderWeight.xlThin

        oRange.Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        oRange.Borders(Excel.XlBordersIndex.xlEdgeLeft).ThemeColor = 1
        oRange.Borders(Excel.XlBordersIndex.xlEdgeLeft).TintAndShade = -0.499984740745262
        oRange.Borders(Excel.XlBordersIndex.xlEdgeLeft).Weight = Excel.XlBorderWeight.xlMedium

        oRange.Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
        oRange.Borders(Excel.XlBordersIndex.xlEdgeRight).ThemeColor = 1
        oRange.Borders(Excel.XlBordersIndex.xlEdgeRight).TintAndShade = -0.499984740745262
        oRange.Borders(Excel.XlBordersIndex.xlEdgeRight).Weight = Excel.XlBorderWeight.xlMedium

        oRange.Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        oRange.Borders(Excel.XlBordersIndex.xlEdgeTop).ThemeColor = 1
        oRange.Borders(Excel.XlBordersIndex.xlEdgeTop).TintAndShade = -0.499984740745262
        oRange.Borders(Excel.XlBordersIndex.xlEdgeTop).Weight = Excel.XlBorderWeight.xlMedium

        oRange.Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        oRange.Borders(Excel.XlBordersIndex.xlEdgeBottom).ThemeColor = 1
        oRange.Borders(Excel.XlBordersIndex.xlEdgeBottom).TintAndShade = -0.499984740745262
        oRange.Borders(Excel.XlBordersIndex.xlEdgeBottom).Weight = Excel.XlBorderWeight.xlMedium

    End Sub

    Public Function Convert_Colonne(ByVal Entree As Integer) As String

        '---Fonction permettant de convertir un numero ou lettre de colonne excel en lettre ou numero
        Dim i As Integer, x As Integer
        Dim Sortie As String
        Sortie = ""
        For i = 6 To 0 Step -1
            x = CInt((26 ^ (i + 1) - 1) / 25 - 1)
            If Entree > x Then
                Sortie = Sortie & Chr(((Entree - x - 1) \ CInt(26 ^ i)) Mod 26 + 65)
            End If
        Next i
        Convert_Colonne = Sortie
    End Function

    Public Function Convert_Colonne(ByVal Entree As String) As Integer

        '---Fonction permettant de convertir un numero ou lettre de colonne excel en lettre ou numero
        Dim i As Integer, ValeurCh As Integer
        Dim Sortie As Integer
        Const ChaineAlpha As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        Sortie = 0
        For i = 1 To Len(Entree)
            ValeurCh = InStr(1, ChaineAlpha, Mid(UCase(Entree), i, 1))
            Sortie = Sortie * 26 + ValeurCh
        Next
        Convert_Colonne = Sortie
    End Function



    Public Function lastRowInExcel(ByRef ActiveSheet As Excel.Worksheet) As Long
        Dim lastRow As Long = 1
        Dim lastCol As Long
        lastCol = ActiveSheet.Range("iv2").End(Excel.XlDirection.xlToLeft).Column
        'lastCol = CType(ActiveSheet.Cells(ActiveSheet.Columns.Count, "2").End(Excel.XlDirection.xlToLeft).Column, Integer)
        For i = 1 To lastCol
            If CType(ActiveSheet.Cells(ActiveSheet.Rows.Count, "A").End(Excel.XlDirection.xlUp).Row, Integer) > lastRow Then
                lastRow = CType(ActiveSheet.Cells(ActiveSheet.Rows.Count, "A").End(Excel.XlDirection.xlUp).Row, Integer)
            End If
        Next

        Return lastRow
    End Function

    Public Sub TelechargerBonDeCommandeMobile(sender As Object, e As EventArgs)
        Dim oLinkBouton As LinkButton
        oLinkBouton = CType(sender, LinkButton)

        Dim oUtilisateurSecondaireFiltres As New UtilisateurSecondaireFiltres()
        If Session.Item("idUtilisateurSecondaire") IsNot Nothing And Session.Item("idUtilisateurSecondaire").ToString <> "0" Then
            Dim oUtilisateurSecondaire As New UtilisateurSecondaire()
            oUtilisateurSecondaire.Req_Infos_Utilisateur_Secondaire(CInt(Session.Item("idUtilisateurSecondaire").ToString), CInt(User.Identity.Name.Split(CChar("_"))(1)))
            oUtilisateurSecondaireFiltres.chargerUtilisateurSecondaireFiltres(oUtilisateurSecondaire)
        End If



        Dim nIdClient As Integer = CInt(oLinkBouton.Attributes("idClient").ToString)
        Dim sType As String = oLinkBouton.Attributes("typeAction").ToString
        Dim oUtilisateur As New Utilisateur()
        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))


        Dim oCommandes As New Commandes
        oCommandes.ChargerCommandesATelecharger(sType, oUtilisateur.oClient)
        If oCommandes.Count = 0 Then
            Me.Notification.Value = "Il n'y a aucune commande en attente d'envoi."
        Else


            Dim nNumeroCommande As Long = cDal.RecupererNumeroCommandeEnCoursClient(oUtilisateur.oClient.nIdClient) + 1
            Dim sNumeroCommande As String = Year(Now) & "-" & Month(Now) & "-" & Day(Now) & "-" & Toolbox.VerifierEnteteLibelleMois("LISTEMOIS", Month(Now), New Client(nIdClient)).ToUpper & "-" & Toolbox.ConstruireNumCommande(5, nNumeroCommande)

            Dim sCheminFichierModele As String = Nothing

            Select Case sType
                Case "creation"
                    sCheminFichierModele = Server.MapPath("\documents\" & oUtilisateur.oClient.sNomClient & "\GestionParc\Modeles\Modele Creation.xls")
                Case "modification"
                    sCheminFichierModele = Server.MapPath("\documents\" & oUtilisateur.oClient.sNomClient & "\GestionParc\Modeles\Modele Modification.xls")
                Case "suppression"
                    sCheminFichierModele = Server.MapPath("\documents\" & oUtilisateur.oClient.sNomClient & "\GestionParc\Modeles\Modele Suppression.xls")
            End Select


            Dim sCheminFichierEnregistrement = Server.MapPath("\documents\" & oUtilisateur.oClient.sNomClient & "\GestionParc\Bonsdecommandes\" & sNumeroCommande & ".xls")
            Dim sCheminTelechargement = "documents/CG31/GestionParc/Bonsdecommandes/" & sNumeroCommande & ".xls"

            Dim Process1() As Process = Process.GetProcesses()
            Dim oExcel As New Excel.Application
            Dim oClasseur As Excel.Workbook = oExcel.Workbooks.Open(sCheminFichierModele)
            Dim oFichier As FileInfo
            oFichier = New FileInfo(sCheminFichierEnregistrement)
            If oFichier.Exists Then
                oFichier.Delete()
            End If

            oClasseur.SaveAs(sCheminFichierEnregistrement)
            Dim oFeuille As Excel.Worksheet = CType(oClasseur.Sheets(1), Excel.Worksheet)
            Dim idProcExcel As Integer
            Dim Process2() As Process = Process.GetProcesses()
            ' -------- Recherche de mon Id de processus
            Dim bMonProcessXL As Boolean
            For j = 0 To Process2.GetUpperBound(0)
                If Process2(j).ProcessName = "EXCEL" Then
                    bMonProcessXL = True
                    ' Parcours des processus avant le mien
                    For i = 0 To Process1.GetUpperBound(0)
                        If Process1(i).ProcessName = "EXCEL" Then
                            If Process2(j).Id = Process1(i).Id Then
                                ' S'il existait avant, c
                                bMonProcessXL = False
                                Exit For
                            End If
                        End If
                    Next i
                    If bMonProcessXL = True Then
                        idProcExcel = Process2(j).Id
                        Exit For
                    End If
                End If
            Next j
            oExcel.Visible = False : oExcel.DisplayAlerts = False

            Dim nLigne As Integer = 0
            Dim nCompteurCommande As Integer = 0
            Dim bPremierPassage As Boolean = True

            Select Case sType
                Case "creation"
                    oFeuille.Range("A3").Value = sNumeroCommande
                    For Each oCommande In oCommandes
                        oFeuille.Rows(nLigne + 5 & ":" & nLigne + 5).copy()
                        oFeuille.Rows(nLigne + 5 & ":" & nLigne + 5).Insert(Shift:=Excel.XlDirection.xlDown)
                        If bPremierPassage = False Then
                            oFeuille.Range("A" & nLigne + 5 & ":" & "P" & nLigne + 5).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
                            oFeuille.Range("A" & nLigne + 5 & ":" & "P" & nLigne + 5).Borders(Excel.XlBordersIndex.xlEdgeTop).Weight = 2
                        Else
                            bPremierPassage = False
                        End If
                        oFeuille.Range("A" & nLigne + 5).Value = "ACCES N° " & CStr(nCompteurCommande + 1)
                        oFeuille.Range("B" & nLigne + 5).Value = oCommande.oNouvelleLigne.oCompteFacturant.sLibelle
                        oFeuille.Range("C" & nLigne + 5).Value = oCommande.oNouvelleLigne.sNom & " " & oCommande.oNouvelleLigne.sPrenom
                        For Each oStock In oCommande.oStockAjoutes
                            If oStock.sTypeLiaison.ToUpper = "PRINCIPALE" Then
                                oFeuille.Range("D" & nLigne + 5).Value = oStock.sNumSerie
                            End If
                        Next
                        Dim oLigne As New Ligne
                        oLigne.chargerLigneParId(oCommande.oNouvelleLigne.nIdLigne)

                        oFeuille.Range("E" & nLigne + 5).Value = oCommande.oNouvelleLigne.oForfait.oTypeForfait.sLibelle & " " & oCommande.oNouvelleLigne.oForfait.sLibelle & " à " & CStr(oCommande.oNouvelleLigne.oForfait.nPrixAbonnement)
                        Select Case oUtilisateur.oClient.sMonnaieClient
                            Case "euro"
                                oFeuille.Range("E" & nLigne + 5).Value += "€"
                            Case "dollar"
                                oFeuille.Range("E" & nLigne + 5).Value += "$"
                        End Select
                        oFeuille.Range("F" & nLigne + 5).Value = oLigne.dDateActivation.ToString("dd/MM/yyyy")
                        oFeuille.Range("G" & nLigne + 5).Value = oCommande.oNouvelleLigne.oDiscrimination.sLibelle
                        Dim nCompteurOption As Integer = 1
                        For j = 0 To oCommande.oOptionsAjoutees.Count - 1
                            If nCompteurOption > 4 Then
                                nCompteurOption = 1
                                nLigne = nLigne + 1
                                oFeuille.Rows(nLigne + 5 & ":" & nLigne + 5).copy()
                                oFeuille.Rows(nLigne + 5 & ":" & nLigne + 5).Insert(Shift:=Excel.XlDirection.xlDown)
                            End If
                            oFeuille.Range(Chr(Asc("G") + nCompteurOption) & nLigne + 5).Value = oCommande.oOptionsAjoutees(j).sLibelle
                            nCompteurOption = nCompteurOption + 1
                        Next
                        nCompteurCommande += 1
                        nLigne += 1
                    Next
                    oFeuille.Rows(nLigne + 5 & ":" & nLigne + 5).delete(Shift:=Excel.XlDirection.xlUp)
                    oFeuille.Cells.EntireColumn.AutoFit()
                Case "modification"
                    oFeuille.Range("A3").Value = sNumeroCommande
                    For Each oCommande In oCommandes
                        oFeuille.Rows(nLigne + 5 & ":" & nLigne + 5).copy()
                        oFeuille.Rows(nLigne + 5 & ":" & nLigne + 5).Insert(Shift:=Excel.XlDirection.xlDown)
                        If bPremierPassage = False Then
                            oFeuille.Range("A" & nLigne + 5 & ":" & "N" & nLigne + 5).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
                            oFeuille.Range("A" & nLigne + 5 & ":" & "N" & nLigne + 5).Borders(Excel.XlBordersIndex.xlEdgeTop).Weight = 2
                        Else
                            bPremierPassage = False
                        End If
                        Dim oLigne As New Ligne
                        oLigne.chargerLigneParId(oCommande.oNouvelleLigne.nIdLigne)

                        oFeuille.Range("A" & nLigne + 5).Value = oLigne.sNumeroGSM
                        If oCommande.oAncienneLigne.sNom <> oCommande.oNouvelleLigne.sNom Or oCommande.oAncienneLigne.sPrenom <> oCommande.oNouvelleLigne.sPrenom Then
                            oFeuille.Range("B" & nLigne + 5).Value = oCommande.oNouvelleLigne.sNom & " " & oCommande.oNouvelleLigne.sPrenom
                        End If

                        For Each oStock In oCommande.oStockAjoutes
                            If oStock.sTypeLiaison.ToUpper = "PRINCIPALE" Then
                                oFeuille.Range("C" & nLigne + 5).Value = oStock.sNumSerie
                            End If
                        Next
                        If oCommande.oAncienneLigne.oForfait.sLibelle <> oCommande.oNouvelleLigne.oForfait.sLibelle Then
                            oFeuille.Range("D" & nLigne + 5).Value = oCommande.oNouvelleLigne.oForfait.oTypeForfait.sLibelle & " " & oCommande.oNouvelleLigne.oForfait.sLibelle & " à " & CStr(oCommande.oNouvelleLigne.oForfait.nPrixAbonnement)
                            Select Case oUtilisateur.oClient.sMonnaieClient
                                Case "euro"
                                    oFeuille.Range("D" & nLigne + 5).Value += "€"
                                Case "dollar"
                                    oFeuille.Range("D" & nLigne + 5).Value += "$"
                            End Select
                        End If
                        If oCommande.oAncienneLigne.oCompteFacturant.sLibelle <> oCommande.oNouvelleLigne.oCompteFacturant.sLibelle Then oFeuille.Range("E" & nLigne + 5).Value = oCommande.oNouvelleLigne.oCompteFacturant.sLibelle
                        If oCommande.oAncienneLigne.oDiscrimination.sLibelle <> oCommande.oNouvelleLigne.oDiscrimination.sLibelle Then oFeuille.Range("F" & nLigne + 5).Value = oCommande.oNouvelleLigne.oDiscrimination.sLibelle
                        Dim nCompteurOption As Integer = 1
                        Dim nLigneAAjouterOption As Integer = 0
                        Dim nLigneFinaleAAjouter As Integer = 0
                        For j = 0 To oCommande.oOptionsRetirees.Count - 1
                            If nCompteurOption > 4 Then
                                nCompteurOption = 1
                                nLigneAAjouterOption = nLigneAAjouterOption + 1
                                oFeuille.Rows(nLigne + nLigneAAjouterOption + 5 & ":" & nLigne + nLigneAAjouterOption + 5).copy()
                                oFeuille.Rows(nLigne + nLigneAAjouterOption + 5 & ":" & nLigne + nLigneAAjouterOption + 5).Insert(Shift:=Excel.XlDirection.xlDown)
                            End If
                            oFeuille.Range(Chr(Asc("F") + nCompteurOption) & nLigne + nLigneAAjouterOption + 4).Value = oCommande.oOptionsRetirees(j).sLibelle
                            nCompteurOption = nCompteurOption + 1
                        Next
                        If nLigneAAjouterOption > nLigneFinaleAAjouter Then nLigneFinaleAAjouter = nLigneAAjouterOption
                        nCompteurOption = 1
                        nLigneAAjouterOption = 0
                        For j = 0 To oCommande.oOptionsAjoutees.Count - 1
                            If nCompteurOption > 4 Then
                                nCompteurOption = 1
                                nLigneAAjouterOption = nLigneAAjouterOption + 1
                                oFeuille.Rows(nLigne + nLigneAAjouterOption + 5 & ":" & nLigne + nLigneAAjouterOption + 5).copy()
                                oFeuille.Rows(nLigne + nLigneAAjouterOption + 5 & ":" & nLigne + nLigneAAjouterOption + 5).Insert(Shift:=Excel.XlDirection.xlDown)
                            End If
                            oFeuille.Range(Chr(Asc("J") + nCompteurOption) & nLigne + nLigneAAjouterOption + 5).Value = oCommande.oOptionsAjoutees(j).sLibelle
                            nCompteurOption = nCompteurOption + 1
                        Next
                        If nLigneAAjouterOption > nLigneFinaleAAjouter Then nLigneFinaleAAjouter = nLigneAAjouterOption
                        nLigne = nLigne + nLigneFinaleAAjouter + 1
                    Next
                    oFeuille.Rows(nLigne + 5 & ":" & nLigne + 5).delete(Shift:=Excel.XlDirection.xlUp)
                    oFeuille.Cells.EntireColumn.AutoFit()
                Case "suppression"
                    oFeuille.Range("A3").Value = sNumeroCommande
                    For Each oCommande In oCommandes
                        oFeuille.Rows(nLigne + 5 & ":" & nLigne + 5).copy()
                        oFeuille.Rows(nLigne + 5 & ":" & nLigne + 5).Insert(Shift:=Excel.XlDirection.xlDown)
                        If bPremierPassage = False Then
                            oFeuille.Range("A" & nLigne + 5 & ":" & "D" & nLigne + 5).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
                            oFeuille.Range("A" & nLigne + 5 & ":" & "D" & nLigne + 5).Borders(Excel.XlBordersIndex.xlEdgeTop).Weight = 2
                        Else
                            bPremierPassage = False
                        End If
                        Dim oLigne As New Ligne
                        oLigne.chargerLigneParId(oCommande.oNouvelleLigne.nIdLigne)
                        oFeuille.Range("A" & nLigne + 5).Value = "'" + oLigne.sNumeroGSM
                        oFeuille.Range("B" & nLigne + 5).Value = oCommande.oAncienneLigne.sNom & " " & oCommande.oAncienneLigne.sPrenom
                        oFeuille.Range("C" & nLigne + 5).Value = oCommande.oAncienneLigne.oCompteFacturant.sLibelle
                        If Not oLigne.dDateDesactivation = Nothing Then oFeuille.Range("D" & nLigne + 5).Value = oLigne.dDateDesactivation.ToString("dd/MM/yyyy")
                        nLigne += 1
                    Next
                    oFeuille.Rows(nLigne + 5 & ":" & nLigne + 5).delete(Shift:=Excel.XlDirection.xlUp)
                    oFeuille.Cells.EntireColumn.AutoFit()
            End Select

            oClasseur.Save()
            oClasseur.Close()
            oFeuille = Nothing
            oClasseur = Nothing
            oExcel = Nothing

            Try
                If idProcExcel <> 0 Then
                    Process.GetProcessById(idProcExcel).Kill()
                Else
                    For Each el In Process.GetProcessesByName("EXCEL")
                        el.Kill()
                    Next
                End If
            Catch ex As Exception
                For Each el In Process.GetProcessesByName("EXCEL")
                    el.Kill()
                Next
            End Try

            '---ENVOI DE MAIL

            Dim sNbLignes As String = ""
            If oCommandes.Count = 1 Then
                sNbLignes = "1 ligne"
            Else
                sNbLignes = oCommandes.Count & " lignes"
            End If
            Dim sNature As String
            Select Case sType
                Case "creation"
                    sNature = "création"
                Case Else
                    sNature = sType
            End Select

            Dim sdestinataireCommandeFrom As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "destinataireCommandeFrom")
            Dim sdestinataireCommandeTo As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "destinatairesCommandeTo")
            Dim sdestinataireCommandeCC As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "destinatairesCommandeCc")
            Dim sdestinataireCommandeCCI As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "destinatairesCommandeCci")
            Dim sSubject As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "objetCommande")
            Dim sCorps As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "corpsCommande")
            Dim sNotification As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "destinatairesCommandeaccuse")

            Dim oMail As New MailMessage
            If sdestinataireCommandeFrom <> "" Then oMail.From = New MailAddress(sdestinataireCommandeFrom.Split(CChar(";"))(0), sdestinataireCommandeFrom.Split(CChar(";"))(1))
            If sdestinataireCommandeTo <> "" Then
                For Each smail In sdestinataireCommandeTo.Split(CChar("|"))
                    oMail.To.Add(New MailAddress(smail.Split(CChar(";"))(0), smail.Split(CChar(";"))(1)))
                Next
            End If
            If sdestinataireCommandeCC <> "" Then
                For Each smail In sdestinataireCommandeCC.Split(CChar("|"))
                    oMail.CC.Add(New MailAddress(smail.Split(CChar(";"))(0), smail.Split(CChar(";"))(1)))
                Next
            End If
            If sdestinataireCommandeCCI <> "" Then
                For Each smail In sdestinataireCommandeCCI.Split(CChar("|"))
                    oMail.Bcc.Add(New MailAddress(smail.Split(CChar(";"))(0), smail.Split(CChar(";"))(1)))
                Next
            End If
            If sNotification <> "" Then oMail.Headers.Add("Disposition-Notification-To", sNotification)

            oMail.Subject = sSubject.Replace("***NUMCOMMANDE***", sNumeroCommande).Replace("***NATURE***", sNature)
            oMail.Body = sCorps.Replace("***NUMCOMMANDE***", sNumeroCommande).Replace("***NBRLIGNE***", sNbLignes).Replace("***NATURE***", sNature)
            oMail.IsBodyHtml = True

            oMail.Attachments.Add(New Attachment(sCheminFichierEnregistrement))

            Dim oSmtp As New SmtpClient("smtp.office365.com")
            oSmtp.Port = 587
            oSmtp.EnableSsl = True
            'Dim oCredential As New System.Net.NetworkCredential("sebastien.lamarque@oria.fr", "Zuxo0260")
            'Dim oCredential As New System.Net.NetworkCredential("wilfrid.boudia@oria.fr", "2AssassiNOria")
            Dim oCredential2 As New System.Net.NetworkCredential("fabien.daries@oria.fr", "oPoq8*Fc ")
            oSmtp.Credentials = oCredential2
            'oSmtp.Credentials = oCredential2

            Try
                oSmtp.Send(oMail)
            Catch ex As Exception
                MsgBox("Le message n'a pas pu être délivré. Erreur :" & ex.Message, vbOKOnly)
            End Try
            '---

            cDal.MettreAJourNumeroCommande(oUtilisateur.oClient.nIdClient, nNumeroCommande)
            For Each oCommande In oCommandes
                oCommande.PasserEnTelechargeCommande(sNumeroCommande)
            Next


            Me.UpdatePanelEntetes.Update()



            binderGridViewParIdRapport(CInt(oLinkBouton.Attributes("nIdRapport").ToString), oUtilisateurSecondaireFiltres)
            Charger_Sommaire()
            UpdatePanelSommaire.Update()

        End If
    End Sub

    Public Sub TelechargerBonDeCommandeFixe(sender As Object, e As EventArgs)
        Dim oLinkBouton As LinkButton
        oLinkBouton = CType(sender, LinkButton)

        Dim nIdClient As Integer = CInt(oLinkBouton.Attributes("idClient").ToString)
        Dim sType As String = oLinkBouton.Attributes("typeAction").ToString
        Dim oUtilisateur As New Utilisateur()
        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))


        Dim oUtilisateurSecondaireFiltres As New UtilisateurSecondaireFiltres()
        If Session.Item("idUtilisateurSecondaire") IsNot Nothing And Session.Item("idUtilisateurSecondaire").ToString <> "0" Then
            Dim oUtilisateurSecondaire As New UtilisateurSecondaire()
            oUtilisateurSecondaire.Req_Infos_Utilisateur_Secondaire(CInt(Session.Item("idUtilisateurSecondaire").ToString), CInt(User.Identity.Name.Split(CChar("_"))(1)))
            oUtilisateurSecondaireFiltres.chargerUtilisateurSecondaireFiltres(oUtilisateurSecondaire)
        End If


        Dim oCommandes As New CommandeFixes
        oCommandes.ChargerCommandesFixeATelecharger(sType, oUtilisateur.oClient)
        If oCommandes.Count = 0 Then
            Me.Notification.Value = "Il n'y a aucune commande en attente d'envoi."
        Else


            Dim nNumeroCommande As Long = cDal.RecupererNumeroCommandeEnCoursClient(oUtilisateur.oClient.nIdClient) + 1
            Dim sNumeroCommande As String = Year(Now) & "-" & Month(Now) & "-" & Day(Now) & "-" & Toolbox.VerifierEnteteLibelleMois("LISTEMOIS", Month(Now), New Client(nIdClient)).ToUpper & "-" & Toolbox.ConstruireNumCommande(5, nNumeroCommande)

            '    Dim sCheminFichierModele As String

            '    Select Case sType
            '        Case "creation"
            '            sCheminFichierModele = Server.MapPath("\documents\" & oUtilisateur.oClient.sNomClient & "\GestionParc\Modeles\Modele Creation.xls")
            '        Case "modification"
            '            sCheminFichierModele = Server.MapPath("\documents\" & oUtilisateur.oClient.sNomClient & "\GestionParc\Modeles\Modele Modification.xls")
            '        Case "suppression"
            '            sCheminFichierModele = Server.MapPath("\documents\" & oUtilisateur.oClient.sNomClient & "\GestionParc\Modeles\Modele Suppression.xls")
            '    End Select


            '    Dim sCheminFichierEnregistrement = Server.MapPath("\documents\" & oUtilisateur.oClient.sNomClient & "\GestionParc\Bonsdecommandes\" & sNumeroCommande & ".xls")
            '    Dim sCheminTelechargement = "documents/CG31/GestionParc/Bonsdecommandes/" & sNumeroCommande & ".xls"

            '    Dim Process1() As Process = Process.GetProcesses()
            '    Dim oExcel As New Excel.Application
            '    Dim oClasseur As Excel.Workbook = oExcel.Workbooks.Open(sCheminFichierModele)
            '    Dim oFichier As FileInfo
            '    oFichier = New FileInfo(sCheminFichierEnregistrement)
            '    If oFichier.Exists Then
            '        oFichier.Delete()
            '    End If

            '    oClasseur.SaveAs(sCheminFichierEnregistrement)
            '    Dim oFeuille As Excel.Worksheet = CType(oClasseur.Sheets(1), Excel.Worksheet)
            '    Dim idProcExcel As Integer
            '    Dim Process2() As Process = Process.GetProcesses()
            '    ' -------- Recherche de mon Id de processus
            '    Dim bMonProcessXL As Boolean
            '    For j = 0 To Process2.GetUpperBound(0)
            '        If Process2(j).ProcessName = "EXCEL" Then
            '            bMonProcessXL = True
            '            ' Parcours des processus avant le mien
            '            For i = 0 To Process1.GetUpperBound(0)
            '                If Process1(i).ProcessName = "EXCEL" Then
            '                    If Process2(j).Id = Process1(i).Id Then
            '                        ' S'il existait avant, c
            '                        bMonProcessXL = False
            '                        Exit For
            '                    End If
            '                End If
            '            Next i
            '            If bMonProcessXL = True Then
            '                idProcExcel = Process2(j).Id
            '                Exit For
            '            End If
            '        End If
            '    Next j
            '    oExcel.Visible = False : oExcel.DisplayAlerts = False

            '    Dim nLigne As Integer = 0
            '    Dim nCompteurCommande As Integer = 0
            '    Dim bPremierPassage As Boolean = True

            '    Select Case sType
            '        Case "creation"
            '            oFeuille.Range("A3").Value = sNumeroCommande
            '            For Each oCommande In oCommandes
            '                oFeuille.Rows(nLigne + 4 & ":" & nLigne + 4).copy()
            '                oFeuille.Rows(nLigne + 4 & ":" & nLigne + 4).Insert(Shift:=Excel.XlDirection.xlDown)
            '                If bPremierPassage = False Then
            '                    oFeuille.Range("A" & nLigne + 4 & ":" & "P" & nLigne + 4).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
            '                    oFeuille.Range("A" & nLigne + 4 & ":" & "P" & nLigne + 4).Borders(Excel.XlBordersIndex.xlEdgeTop).Weight = 2
            '                Else
            '                    bPremierPassage = False
            '                End If
            '                oFeuille.Range("A" & nLigne + 4).Value = "ACCES N° " & CStr(nCompteurCommande + 1)
            '                oFeuille.Range("B" & nLigne + 4).Value = oCommande.oNouvelleLigne.oCompteFacturant.sLibelle
            '                oFeuille.Range("C" & nLigne + 4).Value = oCommande.oNouvelleLigne.sNom & " " & oCommande.oNouvelleLigne.sPrenom
            '                For Each oStock In oCommande.oStockAjoutes
            '                    If oStock.sTypeLiaison.ToUpper = "PRINCIPALE" Then
            '                        oFeuille.Range("D" & nLigne + 4).Value = oStock.sNumSerie
            '                    End If
            '                Next
            '                Dim oLigne As New Ligne
            '                oLigne.chargerLigneParId(oCommande.oNouvelleLigne.nIdLigne)

            '                oFeuille.Range("E" & nLigne + 4).Value = oCommande.oNouvelleLigne.oForfait.oTypeForfait.sLibelle & " " & oCommande.oNouvelleLigne.oForfait.sLibelle & " à " & CStr(oCommande.oNouvelleLigne.oForfait.nPrixAbonnement) & "€"
            '                oFeuille.Range("F" & nLigne + 4).Value = oLigne.dDateActivation.ToString("dd/MM/yyyy")
            '                oFeuille.Range("G" & nLigne + 4).Value = oCommande.oNouvelleLigne.oDiscrimination.sLibelle
            '                Dim nCompteurOption As Integer = 1
            '                For j = 0 To oCommande.oOptionsAjoutees.Count - 1
            '                    If nCompteurOption > 4 Then
            '                        nCompteurOption = 1
            '                        nLigne = nLigne + 1
            '                        oFeuille.Rows(nLigne + 4 & ":" & nLigne + 4).copy()
            '                        oFeuille.Rows(nLigne + 4 & ":" & nLigne + 4).Insert(Shift:=Excel.XlDirection.xlDown)
            '                    End If
            '                    oFeuille.Range(Chr(Asc("G") + nCompteurOption) & nLigne + 4).Value = oCommande.oOptionsAjoutees(j).sLibelle
            '                    nCompteurOption = nCompteurOption + 1
            '                Next
            '                nCompteurCommande += 1
            '                nLigne += 1
            '            Next
            '            oFeuille.Rows(nLigne + 4 & ":" & nLigne + 4).delete(Shift:=Excel.XlDirection.xlUp)
            '            oFeuille.Cells.EntireColumn.AutoFit()
            '        Case "modification"
            '            oFeuille.Range("A2").Value = sNumeroCommande
            '            For Each oCommande In oCommandes
            '                oFeuille.Rows(nLigne + 4 & ":" & nLigne + 4).copy()
            '                oFeuille.Rows(nLigne + 4 & ":" & nLigne + 4).Insert(Shift:=Excel.XlDirection.xlDown)
            '                If bPremierPassage = False Then
            '                    oFeuille.Range("A" & nLigne + 4 & ":" & "N" & nLigne + 4).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
            '                    oFeuille.Range("A" & nLigne + 4 & ":" & "N" & nLigne + 4).Borders(Excel.XlBordersIndex.xlEdgeTop).Weight = 2
            '                Else
            '                    bPremierPassage = False
            '                End If
            '                Dim oLigne As New Ligne
            '                oLigne.chargerLigneParId(oCommande.oNouvelleLigne.nIdLigne)

            '                oFeuille.Range("A" & nLigne + 4).Value = oLigne.sNumeroGSM
            '                If oCommande.oAncienneLigne.sNom <> oCommande.oNouvelleLigne.sNom Or oCommande.oAncienneLigne.sPrenom <> oCommande.oNouvelleLigne.sPrenom Then
            '                    oFeuille.Range("B" & nLigne + 4).Value = oCommande.oNouvelleLigne.sNom & " " & oCommande.oNouvelleLigne.sPrenom
            '                End If

            '                For Each oStock In oCommande.oStockAjoutes
            '                    If oStock.sTypeLiaison.ToUpper = "PRINCIPALE" Then
            '                        oFeuille.Range("C" & nLigne + 4).Value = oStock.sNumSerie
            '                    End If
            '                Next
            '                If oCommande.oAncienneLigne.oForfait.sLibelle <> oCommande.oNouvelleLigne.oForfait.sLibelle Then
            '                    oFeuille.Range("D" & nLigne + 4).Value = oCommande.oNouvelleLigne.oForfait.oTypeForfait.sLibelle & " " & oCommande.oNouvelleLigne.oForfait.sLibelle & " à " & CStr(oCommande.oNouvelleLigne.oForfait.nPrixAbonnement) & "€"
            '                End If
            '                If oCommande.oAncienneLigne.oCompteFacturant.sLibelle <> oCommande.oNouvelleLigne.oCompteFacturant.sLibelle Then oFeuille.Range("E" & nLigne + 4).Value = oCommande.oNouvelleLigne.oCompteFacturant.sLibelle
            '                If oCommande.oAncienneLigne.oDiscrimination.sLibelle <> oCommande.oNouvelleLigne.oDiscrimination.sLibelle Then oFeuille.Range("F" & nLigne + 4).Value = oCommande.oNouvelleLigne.oDiscrimination.sLibelle
            '                Dim nCompteurOption As Integer = 1
            '                Dim nLigneAAjouterOption As Integer = 0
            '                Dim nLigneFinaleAAjouter As Integer = 0
            '                For j = 0 To oCommande.oOptionsRetirees.Count - 1
            '                    If nCompteurOption > 4 Then
            '                        nCompteurOption = 1
            '                        nLigneAAjouterOption = nLigneAAjouterOption + 1
            '                        oFeuille.Rows(nLigne + nLigneAAjouterOption + 4 & ":" & nLigne + nLigneAAjouterOption + 4).copy()
            '                        oFeuille.Rows(nLigne + nLigneAAjouterOption + 4 & ":" & nLigne + nLigneAAjouterOption + 4).Insert(Shift:=Excel.XlDirection.xlDown)
            '                    End If
            '                    oFeuille.Range(Chr(Asc("F") + nCompteurOption) & nLigne + nLigneAAjouterOption + 4).Value = oCommande.oOptionsRetirees(j).sLibelle
            '                    nCompteurOption = nCompteurOption + 1
            '                Next
            '                If nLigneAAjouterOption > nLigneFinaleAAjouter Then nLigneFinaleAAjouter = nLigneAAjouterOption
            '                nCompteurOption = 1
            '                nLigneAAjouterOption = 0
            '                For j = 0 To oCommande.oOptionsAjoutees.Count - 1
            '                    If nCompteurOption > 4 Then
            '                        nCompteurOption = 1
            '                        nLigneAAjouterOption = nLigneAAjouterOption + 1
            '                        oFeuille.Rows(nLigne + nLigneAAjouterOption + 4 & ":" & nLigne + nLigneAAjouterOption + 4).copy()
            '                        oFeuille.Rows(nLigne + nLigneAAjouterOption + 4 & ":" & nLigne + nLigneAAjouterOption + 4).Insert(Shift:=Excel.XlDirection.xlDown)
            '                    End If
            '                    oFeuille.Range(Chr(Asc("J") + nCompteurOption) & nLigne + nLigneAAjouterOption + 4).Value = oCommande.oOptionsAjoutees(j).sLibelle
            '                    nCompteurOption = nCompteurOption + 1
            '                Next
            '                If nLigneAAjouterOption > nLigneFinaleAAjouter Then nLigneFinaleAAjouter = nLigneAAjouterOption
            '                nLigne = nLigne + nLigneFinaleAAjouter + 1
            '            Next
            '            oFeuille.Rows(nLigne + 4 & ":" & nLigne + 4).delete(Shift:=Excel.XlDirection.xlUp)
            '            oFeuille.Cells.EntireColumn.AutoFit()
            '        Case "suppression"
            '            oFeuille.Range("A2").Value = sNumeroCommande
            '            For Each oCommande In oCommandes
            '                oFeuille.Rows(nLigne + 4 & ":" & nLigne + 4).copy()
            '                oFeuille.Rows(nLigne + 4 & ":" & nLigne + 4).Insert(Shift:=Excel.XlDirection.xlDown)
            '                If bPremierPassage = False Then
            '                    oFeuille.Range("A" & nLigne + 4 & ":" & "D" & nLigne + 4).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
            '                    oFeuille.Range("A" & nLigne + 4 & ":" & "D" & nLigne + 4).Borders(Excel.XlBordersIndex.xlEdgeTop).Weight = 2
            '                Else
            '                    bPremierPassage = False
            '                End If
            '                Dim oLigne As New Ligne
            '                oLigne.chargerLigneParId(oCommande.oNouvelleLigne.nIdLigne)
            '                oFeuille.Range("A" & nLigne + 4).Value = "'" + oLigne.sNumeroGSM
            '                oFeuille.Range("B" & nLigne + 4).Value = oCommande.oAncienneLigne.sNom & " " & oCommande.oAncienneLigne.sPrenom
            '                oFeuille.Range("C" & nLigne + 4).Value = oCommande.oAncienneLigne.oCompteFacturant.sLibelle
            '                If Not oLigne.dDateDesactivation = Nothing Then oFeuille.Range("D" & nLigne + 4).Value = oLigne.dDateDesactivation.ToString("dd/MM/yyyy")
            '                nLigne += 1
            '            Next
            '            oFeuille.Rows(nLigne + 4 & ":" & nLigne + 4).delete(Shift:=Excel.XlDirection.xlUp)
            '            oFeuille.Cells.EntireColumn.AutoFit()
            '    End Select

            '    oClasseur.Save()
            '    oClasseur.Close()
            '    oFeuille = Nothing
            '    oClasseur = Nothing
            '    oExcel = Nothing

            '    Try
            '        If idProcExcel <> 0 Then
            '            Process.GetProcessById(idProcExcel).Kill()
            '        Else
            '            For Each el In Process.GetProcessesByName("EXCEL")
            '                el.Kill()
            '            Next
            '        End If
            '    Catch ex As Exception
            '        For Each el In Process.GetProcessesByName("EXCEL")
            '            el.Kill()
            '        Next
            '    End Try

            '    '---ENVOI DE MAIL

            '    Dim sNbLignes As String = ""
            '    If oCommandes.Count = 1 Then
            '        sNbLignes = "1 ligne"
            '    Else
            '        sNbLignes = oCommandes.Count & " lignes"
            '    End If
            '    Dim sNature As String
            '    Select Case sType
            '        Case "creation"
            '            sNature = "création"
            '        Case Else
            '            sNature = sType
            '    End Select

            '    Dim sdestinataireCommandeFrom As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "destinataireCommandeFrom")
            '    Dim sdestinataireCommandeTo As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "destinatairesCommandeTo")
            '    Dim sdestinataireCommandeCC As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "destinatairesCommandeCc")
            '    Dim sdestinataireCommandeCCI As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "destinatairesCommandeCci")
            '    Dim sSubject As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "objetCommande")
            '    Dim sCorps As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "corpsCommande")
            '    Dim sNotification As String = cDal.RecupererChampFromClientGestionParc(oUtilisateur.oClient.nIdClient, "destinatairesCommandeaccuse")

            '    Dim oMail As New MailMessage
            '    If sdestinataireCommandeFrom <> "" Then oMail.From = New MailAddress(sdestinataireCommandeFrom.Split(CChar(";"))(0), sdestinataireCommandeFrom.Split(CChar(";"))(1))
            '    If sdestinataireCommandeTo <> "" Then
            '        For Each smail In sdestinataireCommandeTo.Split(CChar("|"))
            '            oMail.To.Add(New MailAddress(smail.Split(CChar(";"))(0), smail.Split(CChar(";"))(1)))
            '        Next
            '    End If
            '    If sdestinataireCommandeCC <> "" Then
            '        For Each smail In sdestinataireCommandeCC.Split(CChar("|"))
            '            oMail.CC.Add(New MailAddress(smail.Split(CChar(";"))(0), smail.Split(CChar(";"))(1)))
            '        Next
            '    End If
            '    If sdestinataireCommandeCCI <> "" Then
            '        For Each smail In sdestinataireCommandeCCI.Split(CChar("|"))
            '            oMail.Bcc.Add(New MailAddress(smail.Split(CChar(";"))(0), smail.Split(CChar(";"))(1)))
            '        Next
            '    End If
            '    If sNotification <> "" Then oMail.Headers.Add("Disposition-Notification-To", sNotification)

            '    oMail.Subject = sSubject.Replace("***NUMCOMMANDE***", sNumeroCommande).Replace("***NATURE***", sNature)
            '    oMail.Body = sCorps.Replace("***NUMCOMMANDE***", sNumeroCommande).Replace("***NBRLIGNE***", sNbLignes).Replace("***NATURE***", sNature)
            '    oMail.IsBodyHtml = True

            '    oMail.Attachments.Add(New Attachment(sCheminFichierEnregistrement))

            '    Dim oSmtp As New SmtpClient("smtp.office365.com")
            '    oSmtp.Port = 587
            '    oSmtp.EnableSsl = True
            '    Dim oCredential As New System.Net.NetworkCredential("sebastien.lamarque@oria.fr", "Office_Oria31")
            '    oSmtp.Credentials = oCredential

            '    Try
            '        oSmtp.Send(oMail)
            '    Catch ex As Exception
            '    End Try
            '    '---


            For Each oCommande In oCommandes
                oCommande.PasserEnTelechargeCommandeFixe()
            Next


            Me.UpdatePanelEntetes.Update()

            binderGridViewParIdRapport(CInt(oLinkBouton.Attributes("nIdRapport").ToString), oUtilisateurSecondaireFiltres)
            Charger_Sommaire()
            UpdatePanelSommaire.Update()

        End If
    End Sub


    Private Sub binderGridViewParIdRapport(ByVal nIdRapport As Integer, ByRef oUtilisateurSecondaireFiltres As UtilisateurSecondaireFiltres)
        Dim oUtilisateur As New Utilisateur
        oUtilisateur.Req_Infos_Utilisateur(CInt(User.Identity.Name.Split(CChar("_"))(0)), CInt(User.Identity.Name.Split(CChar("_"))(1)))
        Dim idModule As Integer = CInt(Request.Item("module").ToString)
        Dim oModule As New Modul
        oModule.chargerModulesParId(idModule, oUtilisateur.oClient)
        Dim oRapport As New Rapport
        oRapport.chargerRappportParModuleUtilisateurIdRapport(oModule.nIdModule, oUtilisateur.nIdUtilisateur, nIdRapport, oUtilisateur.oClient.nIdClient)
        Dim ogrid As GridView = CType(FindControl(Replace("Grid_" & oRapport.sSommaireNiv1 & "_" & oRapport.sSommaireNiv2 & "_Rapport" & CStr(oRapport.nIdRapport), " ", "__").Replace("-", "ù").Replace("'", "")), GridView)

        Dim Id_Div As String = ogrid.Attributes("NomID").ToString
        Dim sListeIdRapports As String = ""
        Dim oRapports As New Rapports
        oRapports.chargerRappportsParModuleUtilisateurIdRapport(oRapport.oModule.nIdModule, oRapport.oUtilisateur.nIdUtilisateur, oRapport.nIdRapport, oUtilisateur.oClient.nIdClient)
        sListeIdRapports = ""
        For Each oRapportTemp In oRapports
            If oRapport.nOngletOrdreNiv1 = oRapportTemp.nOngletOrdreNiv1 And oRapport.nOngletOrdreNiv2 = oRapportTemp.nOngletOrdreNiv2 Then
                sListeIdRapports += CStr(oRapportTemp.nIdRapport) & "|"
            End If
        Next
        sListeIdRapports = sListeIdRapports.Remove(sListeIdRapports.Length - 1, 1)

        Dim oCheckboxMoisGlissant As CheckBox = CType(FindControl("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports), CheckBox)
        If Not Page.Request("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports) Is Nothing Then
            If Page.Request("checkbox_MoisGlissant" & "_" & Id_Div & "--" & sListeIdRapports) = "on" Then
                oCheckboxMoisGlissant.Checked = True
            Else
                oCheckboxMoisGlissant.Checked = False
            End If
        End If
        Dim ocolonnes As New Colonnes
        ocolonnes.ChargerListeColonnes(oRapport, oCheckboxMoisGlissant, Id_Div, Me.BloquerColoneAction)


        Dim oRapportsTraitement As New Rapports
        For Each oRapportActuel In oRapports
            If oRapport.nOngletOrdreNiv1 = oRapportActuel.nOngletOrdreNiv1 And oRapport.nOngletOrdreNiv2 = oRapportActuel.nOngletOrdreNiv2 Then
                oRapportsTraitement.Add(oRapport)
            End If
        Next
        Dim oFiltres As New Filtres
        oFiltres.chargerFiltresParRapports(oRapportsTraitement, oRapport, oUtilisateurSecondaireFiltres)
        Dim ListeControles As New List(Of Object)
        Dim listeControlesIndice As UShort = 0
        For Each ofiltre In oFiltres
            If Not IsNothing(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports)) Then
                If TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.DropDownList Then
                    ListeControles.Add(CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), DropDownList))
                ElseIf TypeOf Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports) Is System.Web.UI.WebControls.ListBox Then
                    ListeControles.Add(CType(Page.FindControl("Filtre_" & Id_Div & "_" & ofiltre.nIdFiltre & "_" & sListeIdRapports), ListBox))
                End If
            End If
        Next

        Binder_Gridview(oRapport, ogrid, ocolonnes, ListeControles, CType(Page.FindControl(ogrid.Attributes("NomIDTD").ToString), HtmlGenericControl), CType(Page.FindControl(ogrid.Attributes("NomID").ToString), HtmlGenericControl), oUtilisateurSecondaireFiltres)

    End Sub


End Class
