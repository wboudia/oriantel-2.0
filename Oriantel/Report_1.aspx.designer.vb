﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Report_1
    
    '''<summary>
    '''Contrôle PageBody.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PageBody As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Contrôle form1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''Contrôle sc1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents sc1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''Contrôle UpdatePanelEntetes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelEntetes As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle InstructionsEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InstructionsEntete As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle ListeGridId.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeGridId As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle ListeTriTableau.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeTriTableau As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle PremierChargementOnglet.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PremierChargementOnglet As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle PossedePagination.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PossedePagination As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle NombreNiveauxOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NombreNiveauxOnglets As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle TitreTableauPresent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreTableauPresent As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle BandeauMasque.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BandeauMasque As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle BandeauMasqueFiltre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BandeauMasqueFiltre As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle TriActif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TriActif As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle ChaineFiltreRedirection.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ChaineFiltreRedirection As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle MoisGlissant.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MoisGlissant As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle OutilTop.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents OutilTop As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle Decimale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Decimale As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle TTC.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TTC As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle NumeroEntier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumeroEntier As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle Pagination.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Pagination As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle Recherche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Recherche As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle Seuil.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Seuil As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle Outil.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Outil As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle Tri.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Tri As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle Filtre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Filtre As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle BoiteFiltre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BoiteFiltre As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle ReferenceInsert.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ReferenceInsert As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle ReferenceCloture.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ReferenceCloture As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle ReferenceSuppressionLigne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ReferenceSuppressionLigne As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle ReferenceModificationLigne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ReferenceModificationLigne As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle EstPopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EstPopup As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle TelechargementBonCommandeMobile.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TelechargementBonCommandeMobile As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle ReferenceSuppression.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ReferenceSuppression As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle ReferenceSuppressionDetail.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ReferenceSuppressionDetail As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle BloquerLigneAjout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BloquerLigneAjout As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle BloquerColoneAction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BloquerColoneAction As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle Notification.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle FichierTelechargement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents FichierTelechargement As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle LienTritre1Selectionne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LienTritre1Selectionne As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle LienTritre2Selectionne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LienTritre2Selectionne As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle EtatAffichageSommaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtatAffichageSommaire As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle UpdatePanelAriane.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelAriane As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle PlaceHolderAriane.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderAriane As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle lnkClose.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents lnkClose As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Contrôle imgLnkClose.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents imgLnkClose As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''Contrôle UpdatePanelSommaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelSommaire As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle hfaccordion.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents hfaccordion As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle logoClient.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents logoClient As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''Contrôle PlaceHolderTitreSommaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderTitreSommaire As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolderElementSommaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderElementSommaire As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle ValeurOngletNiv1Selectionne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ValeurOngletNiv1Selectionne As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle ValeurOngletNiv2Selectionne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ValeurOngletNiv2Selectionne As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle IdOngletNiv2Selectionne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IdOngletNiv2Selectionne As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle etapeNavigation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents etapeNavigation As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle listeEtapeNavigation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents listeEtapeNavigation As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle retourActive.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents retourActive As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle chaineFiltreRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents chaineFiltreRetour As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle chaineArianeRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents chaineArianeRetour As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle autorisationEnregistrementNavigation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents autorisationEnregistrementNavigation As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle idRapportTemporaireSommaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents idRapportTemporaireSommaire As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle idModule.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents idModule As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle idRapport.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents idRapport As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle listeIdRapports.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents listeIdRapports As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle idcontainerParent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents idcontainerParent As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle IdRapportLog.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents IdRapportLog As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle UpdatePanelCorps.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelCorps As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle blocCorps.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents blocCorps As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''Contrôle PlaceHolderEntetes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderEntetes As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolderBoutonArriere.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderBoutonArriere As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolderHidden.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderHidden As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolderTitreOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderTitreOnglets As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolderDivOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderDivOnglets As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle UpdateProgress1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateProgress1 As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''Contrôle UpdateProgress2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateProgress2 As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''Contrôle ltlJsFiltres.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ltlJsFiltres As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Contrôle idDownloadButtonExcel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents idDownloadButtonExcel As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle idDownloadButtonPDF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents idDownloadButtonPDF As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle UpdatePanelFooter.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelFooter As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle PlaceHolderFooterModules.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderFooterModules As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Contrôle PlaceHolderFooterNotice.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlaceHolderFooterNotice As Global.System.Web.UI.WebControls.PlaceHolder
End Class
