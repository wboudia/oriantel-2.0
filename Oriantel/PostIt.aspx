﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PostIt.aspx.vb" Inherits="Oriantel.PostIt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64 128x128" href="../images/icones/favicon.ico" />

    <title>Oria Extranet</title>

    <script src="js/jquery-1.10.2.js"></script>
    <link href="css/colorbox.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="text-align: center; width: 580Px; height: 20px; float: left;color:#206797;">
            <span style="padding-top: 10px; font-size: 14pt; text-transform: uppercase;">
                Post-It
            </span>
        </div>
        <div style="text-align:center;width:580Px;height:20Px;float:left;color:#206797;">
            <asp:Label ID="lblNumLigne" runat="server" style="padding-top: 10px; font-size: 12pt;" Text=""></asp:Label>
        </div>
        <div style="text-align:center;">
            <asp:TextBox id="idTextAreaInfoPostIt" TextMode="multiline" Columns="75" Rows="15" runat="server" />
        </div>
        <div style="text-align:center;">
            <asp:Button ID="btnValider" runat="server" Text="Valider" />
        </div>
    </div>
    </form>
    
</body>
</html>
